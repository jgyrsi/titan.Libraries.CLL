---
Author: István Falusi
Version: 13/198 17-CNL 113 512, Rev. A
Date: 2007-11-12

---
= EPTF CLL Ring Buffer Support, User Guide
:author: István Falusi
:revnumber: 13/198 17-CNL 113 512, Rev. A
:revdate: 2007-11-12
:toc:

== About This Document

=== How to Read This Document

This is the User Guide for the Ring Buffer Support of the Ericsson Performance Test Framework (TitanSim), Core Load Library (CLL). TitanSim CLL is developed for the TTCN-3 ‎<<_1, [1]>> Toolset with TITAN <<_2, [2]>>. This document should be read together with the Function Description of the Ring Buffer Support feature <<_5, ‎[5]>>. For more information on the TitanSim CLL, consult the the User's Guide <<_4, ‎[4]>> and the Function Specification <<_3, ‎[3]>> of the TitanSim.

== System Requirements

In order to use the Ring Buffer Support feature the system requirements listed in TitanSim CLL User Guide <<_4, ‎[4]>> should be fulfilled.

= Ring Buffer Support

== Overview

The EPTF CLL Ring Buffer component is a fundamental component providing an implementation for ring buffers in a load test environment.

Ring buffer (or circular buffer) is a http://en.wikipedia.org/wiki/Data_structure[data structure] that uses a single, fixed-size http://en.wikipedia.org/wiki/Buffer_%28computer_science%29[buffer] as if it were connected end-to-end. This structure can be easily used for buffering http://en.wikipedia.org/wiki/Data_stream[data streams].

TitanSim Ring Buffer is a sequence of elements with maximized size, called capacity. It allows random access to elements and provides constant time insertion and removal at both front-end and back-end of the sequence. If you insert a new element to a full buffer (i.e. number of elements is equal with buffer capacity), the element at the other end is being overwritten.

TitanSim Ring Buffer component provides ring buffer implementation both for integer and generic types. Generic (type independent) ring buffer support is implemented through macro-preprocessor of C++ compiler.

[[desc_files_this_feat]]
== Description of Files In This Feature

The EPTF CLL Ring Buffer Support API includes the following files:

* __EPTF_CLL_IntegerRingBuffer_Functions.ttcn__: This TTCN-3 module contains the public interface of ring buffer for integer values.
* __EPTF_CLL_GenericRingBuffer_Functions.ttcnin__: This TTCN-3 include file contains the public interface of generic ring buffer implementation can be used for any user defined TTCN3 data type.
* __EPTF_CLL_RingBuffer_Definitions.ttcn__: This module contains common type definitions for ring buffer support.
* __EPTF_CLL_GenericRingBuffer_Definitions.ttcnin__: This include file contains type definitions for generic ring buffer support.
* __EPTF_CLL_RingBuffer_PrivateFunctions.ttcn__: This module contains common private functions are used by integer ring buffer and generic ring buffer.

NOTE: TitanSim Ring Buffer component consists of two _.ttcnin_ files. These support the type independent, generic ring buffer management with the usage of C++ macros. The _Makefile_ Generator treats the _.ttcnin_ files as include files. They will be added to the _Makefile_ as special include file which will not be translated by the compiler, but will be checked for modification when building the test suite.

== Description of Required Files From Other Features

The TitanSim Ring Buffer does not have any dependencies to other features of TitanSim CLL.

== Installation

Since `EPTF_CLL_RingBuffer` is used as a part of the TTCN-3 test environment this requires TTCN-3 Test Executor to be installed before any operation of these functions. For more details on the installation of TTCN-3 Test Executor see the relevant section of <<_2, ‎[2]>>.

If not otherwise noted in the respective sections, the following are needed to use `EPTF_CLL_RingBuffer`:

* Copy the files listed in section <<desc_files_this_feat, Description of Files In This Feature>> to the directory of the test suite or create symbolic links to them.
* Import the Ring Buffer demo or write your own application using Ring Buffer Support.
* Create _Makefile_ or modify the existing one. For more details see the relevant section of <<_2, ‎[2]>>.
* Edit the config file according to your needs, see section <<config, Configuration>>.

[[config]]
== Configuration

The TitanSim Ring Buffer Support does not require any configuration setting.

= Examples

The "demo" directory of the deliverable contains the following files:

* __RingBuffer_demo.ttcn__ - This module contains the demo use cases.
* __MyMessageTypeRingBuffer_Functions.ttcnpp__ - This module contains the type definitions needed by the demo use cases. It illustrates how to instantiate the code for supporting a ring buffer built from a user defined type.

== Demo Module

The demo module (__RingBuffer_demo.ttcn__) illustrates a typical usage of the ring buffer. It defines a special message type and generates instances of it. The generated messages are stored in a ring buffer until a certain message arrives and then all the messages are dumped from the ring buffer. It is possible to traverse the ring buffer from both begin-to-end and end-to-begin.

* Step 1 - Preparation: define an arbitrary TTCN3 type and instantiate the code for managing ring buffer of this new defined type
+
[source]
----
module MyMessageTypeRingBuffer_Functions
{
  type record MyMessageType {
    enumerated {OK, ERROR} severity,
    charstring data
  }
#define EPTF_BASE_TYPE MyMessageType
#include "EPTF_CLL_GenericRingBuffer_Functions.ttcnin"
#undef EPTF_BASE_TYPE
}
----

* Step 2 - Initialization: define and initialise a ring buffer
+
[source]
----
import from MyMessageTypeRingBuffer_Functions all;
var MyMessageTypeRingBuffer v_buffer;
f_EPTF_MyMessageType_RB_init(v_buffer, 50);
----

* Step 3 - Collect Data: store messages in ring buffer (assume, that `f_GenerateMessage()` generates a new message)
+
[source]
----
var MyMessageType v_myMessage;
do
{
  f_GenerateMessage(v_myMessage);
  f_EPTF_MyMessageType_RB_push_back(v_buffer, v_myMessage);
}
while ( v_myMessage.severity != ERROR);
----

* Step 4 - Dump: empty ring buffer from begin-to-end,
+
[source]
----
while (not f_EPTF_MyMessageType_RB_empty(v_buffer))
{
  log( f_EPTF_MyMessageType_RB_front(v_buffer) );
  f_EPTF_MyMessageType_RB_pop_front(v_buffer);
}
----

Dump from reverse direction:

[source]
----
while (not f_EPTF_MyMessageType_RB_empty(v_buffer))
{
  log( f_EPTF_MyMessageType_RB_back(v_buffer) );
  f_EPTF_MyMessageType_RB_pop_back(v_buffer);
}
----

= Terminology

*TITANSim Core (Load) Library(CLL)* +
It is that part of the TITANSim software that is totally project independent. (I.e., which is not protocol-, or application-dependent). The TITANSim CLL is to be supplied and supported by the TCC organization. Any TITANSim CLL development is to be funded centrally by Ericsson.

= Abbreviations

CLL:: Core Load Library

EPTF:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TITANSim:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TTCN-3:: Testing and Test Control Notation version 3 <<_1, [1]>>.

= References

[[_1]]
[1] ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2] User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3] TitanSim CLL for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4] TitanSim CLL for TTCN-3 toolset with TITAN, User Guide

[[_5]]
[5] EPTF CLL Ring Buffer Support, Function Description

[[_6]]
[6] TitanSim CLL for TTCN-3 toolset with TITAN +
http://ttcn.ericsson.se/products/libraries.shtml[Reference Guide]
