---
Author: József Gyürüsi
Version: 22/198 17-CNL 113 512, Rev. A
Date: 2007-12-06

---
= EPTF CLL Semaphore, User Guide
:author: József Gyürüsi
:revnumber: 22/198 17-CNL 113 512, Rev. A
:revdate: 2007-12-06
:toc:

== About This Document

=== How to Read This Document

This is the User Guide for the Semaphore of the Ericsson Performance Test Framework (TitanSim), Core Load Library (CLL). TitanSim CLL is developed for the TTCN-3 <<_1, ‎[1]>> Toolset with TITAN <<_2, ‎[2]>>. This document should be read together with the Function Description of the Semaphore feature ‎<<_5, [5]>>. For more information on the TitanSim CLL consult the User's Guide ‎<<_4, [4]>> and the Function Specification ‎<<_3, [3]>> of the TitanSim.

== System Requirements

In order to use the Semaphore feature the system requirements listed in TitanSim CLL User's Guide <<_4, ‎[4]>> should be fulfilled.

= Semaphore

== Overview

The EPTF CLL Semaphore component is a fundamental component providing an implementation for Semaphore in a load test environment.

[[description_of_files_in_this_feature]]
== Description of Files in This Feature

The EPTF CLL Semaphore API includes the following files:

* Semaphore
** __EPTF_CLL_Semaphore_Definitions.ttcn__ - This TTCN-3 module contains common type definitions that should be used in all Semaphore Components.
** __EPTF_CLL_Semaphore_Functions.ttcn__ - This TTCN-3 module contains the implementation of Semaphore main functions.

[[description_of_required_files_from_other_features]]
== Description of Required Files From Other Features

The Semaphore feature is part of the TitanSim EPTF Core Load Library (CLL). It relies on several features of the CLL. The user has to obtain the products/files to be found under the respective feature names:

* `Base`
* `FreeBusyQueue`

== Installation

Since EPTF CLL Semaphore is used as a part of the TTCN-3 test environment this requires TTCN-3 Test Executor to be installed before any operation of these functions. For more details on the installation of TTCN-3 Test Executor see the relevant section of ‎<<_2, [2]>>.

If not otherwise noted in the respective sections, the following are needed to use EPTF CLL Semaphore:

* Copy the files listed in section <<description_of_files_in_this_feature, Description of Files in This Feature>> and <<description_of_required_files_from_other_features,  Description of Required Files From Other Features>> to the directory of the test suite or create symbolic links to them.
* Import the `NameService` demo or write your own application using `NameService`.
* Create _Makefile_ or modify the existing one. For more details see the relevant section of ‎<<_2, [2]>>.
* Edit the config file according to your needs, see following section <<configuration, Configuration>>.

[[configuration]]
== Configuration

The executable test program behavior is determined via the run-time configuration file. This is a simple text file, which contains various sections. The usual suffix of configuration files is _.cfg_. For further information on the configuration file see ‎‎<<_2, [2]>>.

The EPTF Semaphore feature defines TTCN-3 module parameters as defined in ‎<<_2, ‎[2]>>, clause 4. Actual values of these parameters – when there is no default value or a different from the default actual value to be used – shall be given in the `[MODULE_PARAMETERS]` section of the configuration file.

The EPTF Semaphore feature defines the following module parameters:

`boolean EPTF_CLL_Semaphore_Definitions.tsp_debugVerbose_PoolMgmt`

The debug messages of the semaphore pool can be enabled by this module parameter.

Default: `_false_`.

== Usage

To use the EPTF Semaphore feature

* Extend one of your component with the `EPTF_Semaphore_CT`
* Call the init function of the Semaphore to initialize the feature
* Use its public functions to create/wait for/unlock a semaphore

NOTE: The init function registers the cleanup function of the Semaphore, so it is not necessary to call it explicitly.

Do not access the component variables in `EPTF_Semaphore_CT` directly! Use the API functions instead.

When the `f_EPTF_Semaphore_waitForUnlock` function is called on a locked semaphore it blocks execution until the semaphore is unlocked. To be able to unlock the Semaphore the altstep that handles the unlock event and calls the unlock function should be activated as default before calling the wait function.

The basic steps you have to do to use the Semaphore when you have to wait the response after the request was sent:

* Create a semaphore and store its id (or send with the message, and put it into the response)
* Send the message of which response should be received
* In the default altstep write the handler function of the response so that it unlocks the semaphore
* In the place where you want to be sure, that the response is received call the `waitForUnlock` function.

= Warning Messages

NOTE: Besides the below described warning messages, warning messages shown in ‎<<_2, ‎[2]>> or those of other used features or product may also appear.

`*Semaphore is already locked: <id>*`

The lock function is called for a semaphore that is already locked.

`*Semaphore <idx> was not unlocked before max waiting time expired. Semaphore unlocked*`

The wait function reached the max waiting time but the Semaphore was not unlocked. The semaphore is unlocked automatically to let the wait function run.

= Examples

The "demo" directory of the deliverable contains the following examples:

* __EPTF_Semaphore_test.ttcn__

== Configuration File

The used configuration file (_main.cfg_) sets only the parameters in the `[LOGGING]` section. No special configuration is needed.

== Demo Module

The demo module _EPTF_Semaphore_test.ttcn_ illustrates a typical usage of the Semaphore feature.

The usage of create/lock/unlock/wait functions are shown.

=== Detailed Description

In the demo two semaphores are created. The first semaphore will be unlocked and the default `maxWait` time (infinity) is used in the wait function. The second semaphore is not unlocked, but the `maxWait` time is set the 1 second. After the semaphores are created the default altstep is activated and the wait function of the first semaphore is called with infinity wait time.

The event that corresponds to a received message is `t_wait` timer’s timeout event. When this event handled in the activated default altstep it will wait for the second semaphore to be unlocked. This never happens, so its wait function will exit because the `maxWait` time elapses. Then it unlocks the first semaphore and calls repeat. At this point as the semaphore is unlocked its waiting function continues and the verdict is set to pass.

= Terminology

*TitanSim Core (Load) Library(CLL):* +
It is that part of the TitanSim software that is totally project independent. (I.e., which is not protocol-, or application-dependent). The TitanSim CLL is to be supplied and supported by the TCC organization. Any TitanSim CLL development is to be funded centrally by Ericsson.

*Semaphore:* +
The TTCN-3 way to solve blocking scenarios and perform synchronization.

= Abbreviations

CLL:: Core Load Library

EPTF:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TitanSim:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TTCN-3:: Testing and Test Control Notation version 3 <<_1, [1]>>

= References

[[_1]]
[1] ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2] User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3] TitanSim CLL for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4] TitanSim CLL for TTCN-3 toolset with TITAN, User Guide

[[_5]]
[5] EPTF CLL Semaphore Function Description

[[_6]]
[6] TitanSim CLL for TTCN-3 toolset with TITAN +
http://ttcn.ericsson.se/products/libraries.shtml[Reference Guide]
