---
Author: József Gyürüsi
Version: 15/155 16-CNL 113 512, Rev. D
Date: 2014-12-03

---
= EPTF CLL Statistics Capture Control, Function Description
:author: József Gyürüsi
:revnumber: 15/155 16-CNL 113 512, Rev. D
:revdate: 2014-12-03
:toc:

== How to Read This Document

This is the Function Description for the Statistics Capture Control of the Ericsson Performance Test Framework (TitanSim), Core Library (CLL). EPTF Core Library is developed for the TTCN-3 <<_1, ‎[1]>> Toolset with TITAN <<_2, ‎[2]>>.
Additionally, to understand the functionality of this feature, read the documentation of the Variable <<_5, ‎[5]>> and the Statistics Measurement ‎<<_6, [6]>> features.

== Scope

This document is to specify the content and functionality of the Statistics Capture Control feature of the EPTF Core Library.

== Recommended Way of Reading

The readers are supposed to get familiar with the concept and functionalities of EPTF Core Library <<_3, ‎[3]>> in general, and the Variable ‎<<_5, [5]>> and Statistics Measure <<_6, ‎[6]>> features. They should get familiar with the list of acronyms and the glossary in the Terminology section.

== Release Information

This document contains actualized information on the TitanSim Release R4A. The new functionalities compared to the release R7A01 concerning the EPTF Statistics Capture Control feature are the following:

* The feature is extended with the client component. The existing component behaves like before and adds server functionalities.
* The changes are backward-compatible to the initial release R2A01 and tested according to this requirement.

= General Description

This document specifies the Statistics Capture Control feature of the EPTF Core Library.

The EPTF Statistics Capture Control feature makes it possible to

* Group Statistics into Capture Groups with a specified Sample Time
* Define output log files per Capture Group
* Print the value of Statistics in the Group to the log files periodically, at Sample Time intervals
* Modify Capture Group parameters
* Create/Delete Capture Group(s)
* Start capturing per group, stop capturing a group, and restart it
* Optionally: to automatically control the statistics capture according to the selected Capture Control Mode
* Capture on remote component

The statistics data is calculated by the EPTF Statistics Measure feature of the EPTF CLL based on EPTF Variable values. The EPTF CLL Statistics Measure feature <<_6, ‎[6]>> makes it possible to create several Statistics belonging to the same EPTF Variable. These Statistics can have different types (content, delta, min, max, etc.) and also several Statistics of the same type can be defined to each Variable (for example min, delta, deltaSum).

In the EPTF Statistics Capture Control feature, for all Statistics, it can be defined, whether they should be reset periodically. The reset period is the same as the timer period of the capture group, in which the Statistics is inserted.

The aim of the EPTF Statistics Capture Control feature is to group certain Statistics into Capture Groups and to print Capture Group contents regularly at sampling intervals to predefined capture files.

Apart from controlling (starting/stopping/restarting) Statistics Capture with the functions of the API belonging to the `EPTF_StatCapture_CT` component, an optional component, the `EPTF_StatCaptureControl_CT` can be used to achieve automatic capture control. Note that this is an optional, extended functionality.

If the user application includes the `EPTF_StatCaptureControl_CT` component as well, then the starting and stopping of the Statistics Capture can be done automatically according to the selected Capture Control Mode.

To be able to use EPTF Statistics Capture, the user component should extend the `EPTF_StatCapture_CT` component. If the automatic Capture Control functionality is also needed, the user component should extend the `EPTF_StatCapture_Control_CT` component. In order to capture statistics on a remote component, the generating components should extend the `EPTF_StatCaptureClient_CT`, the statistic logger component should extend the `EPTF_StatCapture_CT`.

= Functional Interface

Apart from this description, a cross-linked reference guide for the EPTF Core Library Functions can be reached for online reading <<_4, [4]>>.

In this function description, the functions of the main functionality (`EPTF_StatCapture_CT`) and the optional, automatic capture control functionality (`EPTF_StatCaptureControl_CT`) are described separately in the following chapters.

[[eptf-statcapture-ct-component-api]]
= EPTF_StatCapture_CT Component API

== Naming Conventions

All functions have the prefix `f_EPTF_StatCapture_`.

== Public Functions

=== Initialization

Before using the EPTF Statistics Capture Control functions the

`f_EPTF_StatCapture_init(componentname)`

function should be called. This initializes the EPTF Statistics Capture Control feature.

=== Creating a Logfile for a Capture Group

`f_EPTF_StatCapture_openNewStatLogFile (userfilename)`

This function should be called instead of `FileIO` functions to create a statistics `logfile`. It creates a file with the defined `userfilename`, appends self-name and component ID, and then the actual time and date to its filename, and immediately writes the `TimeStampBase` data and capture file version into the file.

The created file should have the following format:

For example: _FirstFile_main_mtc_2007-11-16_14.00.02.txt_ or _FirstFile_ptc1_3_2007-11-16_14.00.02.txt_

corresponding to the `<username>_<selfname>_<component ID>_<y-m-d>_<h.m.s>` format.

The `selfname` is the test component self-name and the component ID is the component identifier. The `TimeStampBase` means not only that the actual date and time is printed to the file, but also that a capture timer is started for that time. Later on, when data is printed to this capture file, it always has a relative time value compared to the `TimeStampBase`.

=== Creating Statistics for Capture

`f_EPTF_StatCapture_createStatforCapture(statID, statname, periodicreset := false, activestat := true)`

In order to enable Statistics settings for the capture, this function must be called. The user first creates a Variable with (`EPTF_Variable` ‎<<_5, [5]>>), then creates a Statistics for this Variable (with `EPTF_StatMeasure` ‎<<_6, [6]>>). Then, to define capture data for this Statistics, the `createStatforCapture` function should be called.

The `statID` is the ID returned by `StatMeasure`. It is important, that it is not allowed to create more Statistics for capture with the same `statID`. The `statname` can be set by the user. The last two parameters are optional, by default, the created Statistics is not reset on Sampling Time intervals (`periodicrese`_), and they are set to active (`activestat`), i.e. their values are printed out to the capture files.

If the function is called with more than two parameters (`statID`, `statname`, `_true_`, `_false_`), the reset functionality and the statistics state can be changed. If the Statistics should be reset periodically after capture, then the `periodicreset` parameter should be set to `_true_`. This means, that after having added this Statistics to a capture group (or several capture groups), its value is reset at sampling intervals of the capture group.

NOTE: These Statistics with `periodicreset` set to `_true_` can be added to _one and only one_ capture group.

=== Capture Group Creation

[[f-eptf-statcapture-addnewgroup-groupname-timerperiod-logfiles-statisticslist]]
==== `f_EPTF_StatCapture_addNewGroup(groupname,timerperiod,logfiles, statisticsList,groupidx, groupstatidxList)`

This function creates a capture group with a unique group name; a sampling period (`timerperiod`) and a list of `logfiles` are assigned as output `logfiles` for the capture group. If no `logfiles` are passed to this function, the capture group is printed to the log file specified by the Module Parameter `tsp_EPTF_StatCapture_def_capture_file` in the config file.

Additionally, if the `statisticsList` parameter is set (by default it is empty), the function adds a list of statistics to the capture group. In this case, the created statistics indices are also returned.

NOTE: There is no restriction for adding one statistics to several capture groups if it is a Statistics with periodic reset set to `_true_`. Also, logging several capture groups into the same `logfile` is possible.

The index of the created group is returned in the `groupidx` argument. This index can be used later to access this capture group. Any EPTF Capture Group is uniquely identified by the component reference where it is created and by this index.

[[f-eptf-statcapture-parseconfiggroup]]
==== `f_EPTF_StatCapture_parseConfigGroup()`

Apart from calling API functions, it is also possible to create Capture Groups from the configuration file with module parameters. Then, in the demo application, this function should be called after the local Variables (which we would like to capture) are created. This function then reads in the configuration parameters and creates Capture Groups with the defined parameters (for example `groupname`, `timer period`, `Statistics`.)

NOTE: Statistics are created on-the-fly from the configuration file according to Variable Name (on the demo component, local Variable which we would like to capture), Statistics type and Name, for example:

[source]
----
varName := "var1",
statType := delta,   //content, delta, deltaSum, min, max,
		          // mean, standardDev
statName := "MyDelta"
----

NOTE: As a best practice, it is advised not to use both options (API capture group creation and group creation from configuration file) at the same time in order to avoid conflicts such as group name or variable name collision.

=== Adding Statistics to Capture Group

A capture group consists of a unique group name, a sampling period, a list of file descriptors to print the group and a list of Statistics (references to Statistics) each containing statistics content or statistical data.

Statistics can be either active or inactive in the capture group. By default, they are active, that means their content is printed out to the log files belonging to the capture group. When switched to inactive, a "-" sign is printed to the log files instead of their value. Activity state of Statistics can be modified during run-time.

The reset of Statistics is carried out at each sampling interval of the capture group after they are captured for all Statistics, which have been created with the `periodicreset` field set to `_true_`.

Adding one or more statistics triggers the writing of a statistics header line on the next write of the statistics to the capture file.

Statistics can be added to the group one by one or as a list.

==== Adding a Single Statistics

`f_EPTF_StatCapture_addStatistics(groupindex, stat, groupstatindex)`

The input parameters are the `groupindex`, i.e. the unique identifier of the capture group where we would like to add the new Statistics, and the Statistics descriptor `stat` which contains the Statistics reference, its name, the active flag, which is set to true by default and the `periodicreset` flag, which is set to false by default. On successful insertion, the index of the Statistics inside the capture group is returned in `_groupstatindex`.

==== Adding a List of Statistics

`f_EPTF_StatCapture_addListOfStatistics(groupindex, statList, groupstatidxList)`

A batch alternative of the previous function. The identifier of the capture group, and the list of Statistics descriptors `statList` are the input parameters, and a list of created Statistics indexes inside the capture group `groupstatidxList` is returned.

[[get-set-functions]]
=== Get/Set Functions

[[setting-modifying-the-sampling-time-of-a-capture-group]]
==== Setting/Modifying the Sampling Time of a Capture Group:

`f_EPTF_StatCapture_setTimerPeriod(groupindex, timerperiod)`

The sampling time period of the capture group specified by the `groupindex` parameter can be set during run-time to the float `timerperiod` input parameter (expressed in seconds). Every time the timer period is changed successfully, the new timer period is printed to the log files assigned to the capture group. From then on, the active Statistics of the capture group is captured and printed to file at these intervals.

==== Modifying the Capture State of a Statistics

[source]
----
f_EPTF_StatCapture_InactivateStatistics(groupindex, groupstatidx)

f_EPTF_StatCapture_ActivateStatistics(groupindex, groupstatidx)
----

These functions set the capture state of a Statistics specified by its index `groupstatidx` and the capture group `groupindex` it belongs to. By default, the state of the Statistics is active, that means, at each sampling period, when the capture group is captured, the content of the Statistics is printed out to the log files belonging to the capture group. During run-time, the capture state of the Statistics can be changed. If the state is set to inactive, instead of printing out Statistics content, a "-" sign is printed out to the log files.

NOTE: If this Statistics is included in other capture groups, then its state can be set independently for each capture group.

==== Finding a Statistics in a Capture Group

`f_EPTF_StatCapture_getStatisticsID (stat, groupindex, groupstatidx) returns boolean`

This function returns the Statistics index of the input parameter `stat` inside the Capture group specified by the input parameter `groupindex`.

If the Statistics is not contained in the capture group, `_false_` is returned.

=== Starting/Stopping Capture, Capture Control by API

Starting and stopping capture can be done anytime during runtime. Only deleted capture groups cannot be restarted again.

==== Start Capturing a Capture Group

`f_EPTF_StatCapture_startGroupCapture(groupindex)`

This function starts capturing a capture group defined by `groupindex`. First, it checks whether the group is not deleted, then prints the actual sampling time, groupname and Statistics value headers for the group to each `logfile` specified at capture group creation. At the end of each sampling interval, the contents of the capture group Statistics are printed out, until the capturing is stopped.

==== Start Capturing All Capture Groups

`f_EPTF_StatCapture_startCaptureAll()`

This function starts capturing all (not deleted) capture groups. It calls the `startGroupCapture` function for each capture group.

==== Stop Capturing a Capture Group

`f_EPTF_StatCapture_stopGroupCapture(groupindex)`

This functions stops capturing a capture group defined by `groupindex`. As an effect, the content of the capture group is not printed out to the `logfiles`.

==== Stop Capturing All Capture Groups

`f_EPTF_StatCapture_stopCaptureAll()`

This function stops the capture of all capture groups.

=== Deleting a Capture Group

`f_EPTF_StatCapture_deleteGroup(groupindex)`

Capture groups can be deleted during run-time. This means, that their content is not printed to file any more. Note that field contents of the group are deleted and the group cannot be restarted again.

== Summary Table of the Public Functions for EPTF Statistics Capture Control feature

See Summary of Statistics Capture Control functions belonging to the EPTF_StatCapture_CT component in the table below:

[width="100%",cols="50%,50%",options="header",]
|==============================================================================================================================================================================
|Function name |Description
|`f_EPTF_StatCapture_init_CT` |Initializes the StatCapture component.
|`f_EPTF_StatCapture_openNewStatLogFile` |Opens a new statistics logfile with the actual time and date appended to the filename and prints the TimeStampBase value into the file.
|`f_EPTF_StatCapture_createStatforCapture` |Creates Statistics for Capture.
|`f_EPTF_StatCapture_addNewGroup` |Adds a new Capture Group.
|`f_EPTF_StatCapture_parseConfigGroup()`` |Creates Statistics and adds new Capture Groups from parameters specified in the configuration file.
|`f_EPTF_StatCapture_addStatistics` |Adds Statistics to the Capture Group.
|`f_EPTF_StatCapture_addListOfStatistics` |Adds a list of Statistics to the Capture Group.
|f `_EPTF_StatCapture_setTimerPeriod` |Sets the Timer Period (sampling time) of the Capture Group.
|`f_EPTF_StatCapture_setLogFiles` |Sets capture files belonging to the Capture Group.
|`f_EPTF_StatCapture_InactivateStatistics` |Inactivates Statistics in a given Capture Group.
|`f_EPTF_StatCapture_ActivateStatistics` |Activates Statistics in a given Capture Group.
|`f_EPTF_StatCapture_getStatisticsID` |Looks up a Statistics in a Capture Group according to the Statistics ID.
|`f_EPTF_StatCapture_startGroupCapture` |Starts the Capture of a Capture Group.
|`f_EPTF_StatCapture_startCaptureAll` |Start Capturing all Capture Groups.
|`f_EPTF_StatCapture_stopGroupCapture` |Stops the Capture of a Capture Group.
|`f_EPTF_StatCapture_stopCaptureAll` |Stops Capturing all Capture Groups.
|`f_EPTF_StatCapture_deleteGroup` |Deletes a Capture Group.
|==============================================================================================================================================================================

== Private Functions

These functions _should not be called_ by the user of the EPTF Statistics Capture feature.

=== Logging Mechanism at Sampling Intervals

[[f-eptf-statcapture-handleevent-actionid-eventindex]]
==== `f_EPTF_StatCapture_handleEvent(actionID, eventindex)`

is a private function, it serves for logging the content of the capture group to the log files belonging to the capture group at sampling intervals. Inactive Statistics are printed out with "-".

NOTE: This function should not be called by the user, the mechanism is automatic.

=== Cleanup Function

[[f-eptf-statcapture-cleanup]]
==== `f_EPTF_StatCapture_cleanup()`

This function is responsible for a peaceful cleanup after termination of the program module or in case of errors.

NOTE: This is a private function; it should not be called by the user. On the other hand, it is important that the user application should _register this cleanup function_ in its init function with

`f_EPTF_Base_RegisterCleanup(refers(f_EPTF_StatCapture_cleanup())`

and make sure that you call the `f_EPTF_Base_cleanup_CT()` function (not the private `StatCapture_cleanup` function!) before terminating the component execution. See more details on the usage of the EPTF Base functions in its Function Description ‎<<_7, [7]>>.

NOTE: Other private functions not mentioned in the documentation can be found in the source code after the //Private Functions comment. These functions should not be called by the user.

[[eptf-statcapturecontrol-ct-component-api]]
= `EPTF_StatCaptureControl_CT` Component API

== Naming Conventions

All functions have the prefix `f_EPTF_StatCaptureControl_`.

== Public Functions

=== Initialization

In order to use the (optional) EPTF Statistics Capture Control functionality the

[source]
----
f_EPTF_StatCaptureControl_init(componentname, nameserverRef,
			         autoControlEnabled)
----

function should be called. The `EPTF_StatCaptureControl_CT` component extends the `EPTF_NS_Client_CT` Name Server Client component to use the Name Service functionality. Therefore, in the `nameserverRef` parameter, the component reference to the belonging Name Service Server Component is needed. This initializes the EPTF Statistics Capture Control component, processes the configuration parameters (for example capture control mode), registers component references to the Name Service Server and adds `PostProc` functions (see <<automatic_capture_control_functionality, Automatic Capture Control Functionality>> for capture control. These `PostProc` functions automatically control the statistics capture according to the changes of the capture control Variables.

In case the function is called with the optional `autoControlEnabled` parameter set to `_false_`, the component behaves exaclty like the `EPTF_StatCapture_CT` component (see <<eptf-statcapture-ct-component-api, EPTF StatCaptureControl_component_API>> ). (Automatic capture control is switched off). By default, this parameter is set to `_true_`.

== Private Functions

These functions _should not be called_ by the user of the EPTF Statistics Capture Control feature.

[[automatic_capture_control_functionality]]
=== Automatic Capture Control Functionality

The capture control functionality is implemented with `PostProc` functions belonging to the control Variables of the Capture Control Modes respectively.

`f_EPTF_StatCaptureControl_User_PostProc (idx, argList)`

`f_EPTF_StatCaptureControl_Execution_PostProc (idx, argList)`

`f_EPTF_StatCaptureControl_Load_PostProc (idx, argList)`

If certain guard conditions are fulfilled, they call the statistics starting/stopping functions (`f_EPTF_StatCapture_startCaptureAll()`, `f_EPTF_StatCapture_stopCaptureAll()`) of the `EPTF_StatCapture_CT` API.

=== Cleanup Function

`f_EPTF_StatCaptureControl_cleanup()`

This function is responsible for a peaceful cleanup after termination of the program module or in case of errors.

NOTE: This is a private function; it should not be called by the user. On the other hand, it is important that the user application should _register this cleanup function_ in its init function with

`f_EPTF_Base_RegisterCleanup(refers(f_EPTF_StatCaptureControl _cleanup())`

and make sure that you call the `f_EPTF_Base_cleanup_CT()` function (not the private `StatCaptureControl_cleanup function!)` before terminating the component execution. See more details on the usage of the EPTF Base functions in its Function Description <<_7, ‎[7]>>.

NOTE: Other private functions not mentioned in the documentation can be found in the source code after the //Private Functions comment. These functions should not be called by the user.

== Summary Table of the Public Functions for EPTF Statistics Capture Control

See Summary of Statistics Capture Control functions belonging to the `EPTF_StatCaptureControl_CT` component in the table below:

[width="100%",cols="50%,50%",options="header",]
|=============================================================================================================
|Function name |Description
|`f_EPTF_StatCaptureControl_init_CT` |Initializes the `StatCaptureControl` component and activates event handling.
|=============================================================================================================

[[eptf-statcaptureclient-ct-component-api]]
= `EPTF_StatCaptureClient_CT` Component API

== Naming Conventions

All functions have the prefix `f_EPTF_StatCaptureClient_`.

== Public Functions

=== Initializing the Client Component

If the statistic generating and the logging should be on another component, the generating side should extend the `StatCaptureClient_CT` feature component. Before using the EPTF Statistics Capture Client functions the client should call the

`f_EPTF_StatCaptureClient_init(componentname)`

function. This initializes the EPTF Statistics Capture Client feature.

=== Subscribe Statistics to a Group

In order to access the statistics on the client component, the client should subscribe the statistics on the server grouped by a `groupName` with the function `f_EPTF_StatCaptureClient_addStatsToGroup`( `pl_groupName`, `pl_stats`, `pl_subscriptionMode`). The subscription mode is defined by the `pl_subscriptionMode`.

=== Starting/Stopping Capture from Client

Starting and stopping capture can be done anytime during runtime.

==== Start Capturing a Capture Group

`f_EPTF_StatCaptureClient_startGroup(pl_groupName)`

This function starts capturing a capture group defined by `pl_groupName`.

==== Start Capturing All Capture Groups

`f_EPTF_StatCaptureClient_startGroupAll()`

This function starts capturing all (not deleted) capture groups.

==== Stop Capturing a Capture Group

`f_EPTF_StatCaptureClient_stopGroup(pl_groupName)`

This function stops capturing a capture group defined by `pl_groupName`. As an effect, the content of the capture group is not printed out to the `logfiles`.

==== Stop Capturing All Capture Groups

`f_EPTF_StatCaptureClient_stopGroupAll()`

This function stops the capture of all capture groups on the server.

==== Delete Capturing a Capture Group

`f_EPTF_StatCaptureClient_deleteGroup(pl_groupName)`

This function deletes a capture group defined by `pl_groupName`. As an effect, the content of the capture group is not printed out to the `logfiles` and cannot be started again until the `f_EPTF_StatCaptureClient_addStatsToGroup()` function is called to that group.

== Summary Table of the Public Functions for EPTF Statistics Capture Client Feature

See Summary of Statistics Capture Control functions belonging to the `EPTF_StatCaptureClient_CT` component in the table below:

[width="100%",cols="50%,50%",options="header",]
|==========================================================================================
|Function name |Description
|`f_EPTF_StatCaptureClient_init_CT` |Initializes the StatCaptureClient component.
|`f_EPTF_StatCaptureClient_addStatsToGroup` |Add statistics to a Capture Group on the server.
|`f_EPTF_StatCaptureClient_startGroup` |Starts the Capture of a Capture Group on the server.
|`f_EPTF_StatCaptureClient_startGroupAll` |Start Capturing all Capture Groups on the server.
|`f_EPTF_StatCaptureClient_stopGroup` |Stops the Capture of a Capture Group on the server.
|`f_EPTF_StatCaptureClient_stopGroupAll` |Stops Capturing all Capture Groups on the server.
|`f_EPTF_StatCaptureClient_deleteGroup` |Deletes a Capture Group on the server.
|==========================================================================================

= Capture File Format Specification

Using the `TCCFile_IO` functionality, the Capture Control feature defines two different file formats (the user can select the required file format from config file). The default file format (_readable_) is easy to process, filter, sort, etc. with standard POSIX text processing utilities (grep, sort, awk, sed. etc.). The second file format (_gnuplot_) is compatible with the input format of `Gnuplot`, a simple command-line data plotting utility.

For efficiency reasons statistics shall be sampled, that is they shall be dumped to the file at regular intervals only.

In the _gnuplot_ format, output files can contain comment lines. A comment line must start with a "#" character.

The actual version of the capture file is printed out in a header line (for example #Capture File Version: 2.2).

== Readable Format

In the default file format, each line has the following general syntax:

1. Empty lines are ignored. Whitespaces between fields are ignored.

2. Non-empty lines start with a heading declaring the type of the line. The heading is separated by a colon from the fields.

3. The heading might be structured, this is described at each heading

4. String values are to be given as TTCN-3 charstring literals surrounded by double quotes

The following line types are defined:

[width="100%",cols="50%,50%",options="header",]
|=====================================================================================================================================================================================================================================================================================================================================================================================================================================================================================
|*Line heading* |*Line fields and meaning*
|`TimeStampBase` |The actual snapshot time when the dumping has been started
|`CaptureFileVersion: x.x` |The actual version of the capture file format.
|`CaptureGroup["GroupName"`] |Defines a `CaptureGroup` with the given `GroupName`. It also means that the capture of this `CaptureGroup` has been started.
|`SampleTime["GroupName"`] |`<aa.bbbbbb>:` Sampling time in seconds
|`ValueHeader["GroupName"`] |Each field defines the identifier of the respective variable in the corresponding `CaptureGroup` line
|`["GroupName", Relative Time]` |Each field defines the value of the respective variable in the corresponding `ValueHeader` line.
|`Capture_Started["GroupName", ID, user/execution/load-based/programmed]` |This text is printed out in all Capture Control Modes (in case of the programmed API control as well). It signals that the capture measurement period is started for the given Capture Group and an ID is assigned to the measurement period. Eg. the ID 2_12 signifies the 12^th^ measurement period belonging to the Capture Group with ID 2. This ID identifies the measurement period when it is stopped.
|`Capture_Finished ["GroupName", ID]` |This text is printed out when the capture measurement period is stopped. It is noted, whether the measurement period (with belonging Capture Group Name and measurement period ID) is finished with success or it is aborted before its expected duration.
|=====================================================================================================================================================================================================================================================================================================================================================================================================================================================================================

Example:

[source]
----
TimeStampBase 2007-11-23-15:52:06.712812 0.578000
CaptureFileVersion: 2.2
CaptureGroup["Stat1"]
Capture_Started[“Stat1”, 1_1, programmed]
SampleTime["Stat1"]: 1.000000 sec
ValueHeader["Stat1"]: TestCaseId respRecOK respRecNOK respTimeout
["Stat1",0.578000]: “User” 1 0 0.005000
["Stat1",1.578000]: “130.100.72.230” 1 0 0.020000
Capture_Finished[“Stat1”, 1_1]
CaptureGroup[“ErrorCodes“]
Capture_Started[“ErrorCodes”, 2_1, programmed]
SampleTime[“ErrorCodes“]: 2.000000 sec
ValueHeader[“ErrorCodes“]: TestCaseId respCode numReceived description
[“ErrorCodes“, 13.650000]: “User” 503 5 “User does not exist”
SampleTime[“Stat1“]: 3.000000 sec
----

== _Gnuplot_ Format

In the _Gnuplot_ format, all headers are commented out with the "#" character. The line containing groupname, relative time and actual variable values is changed to the following format:

`GroupName RelativeTime value1 value2 … valueN.`

String values and the `GroupName` are charstring literals, surrounded by double quotes.

That means, all non-commented lines are of this format, the separator between fields is the whitespace character. Thus, the file can be easily processed by _Gnuplot_. An example for this format can be the following:

[source]
----
# TimeStampBase 2007-11-23-15:52:06.712812 0.578000
# Capture File Version: 2.2
# CaptureGroup["Stat1"]
# Capture_Started["Stat1", 1_1, programmed]
# SampleTime["Stat1"]: 1.000000 sec
# ValueHeader["Stat1"]:TestCaseId respRecOK respRecNOK respTimeout
"Stat1" 0.578000 "User" 1 0 0.005000
"Stat1" 1.578000 "130.100.72.230" 1 0 0.020000
# Capture_Finished["Stat1", 1_1]
# CaptureGroup["ErrorCodes"]
# Capture_Started["ErrorCodes", 2_1, programmed]
# SampleTime["ErrorCodes"]: 2.000000 sec
# ValueHeader["ErrorCodes"]: TestCaseId respCode numReceived description
"ErrorCodes" 13.650000 "User" 503 5 "User does not exist"
# SampleTime["Stat1"]: 3.000000 sec
----

= Terminology

*TitanSim Core Library (CLL):* +
It is that part of the TitanSim software that is totally project independent. (i.e., which is not protocol-, or application-dependent). The EPTF Core Library is to be supplied and supported by the TCC organization. Any EPTF Core Library development is to be funded centrally by Ericsson.

*TitanSim Variables:* +
They contain values, from which _TitanSim Statistics_ can be calculated automatically on changes or periodically.

*TitanSim Statistics Measure:* +
It is a feature responsible for creating _TitanSim Statistics_ and updating their value when required.

*TitanSim Statistics:* +
They are Statistics of TTCN-3 Variables. Possible Statistics are among others minimum, maximum, mean, standard deviation, etc. and also the content of a Variable can be a Statistics itself. The values of such Statistics are automatically updated in the background.

*Capture Group:* +
It is a concept for grouping Statistics references with the same _Sampling Time_ together, capturing referenced Statistics at sampling intervals and printing contents of the group to the log files assigned to the group.

*Sampling Time:* +
It is defined per _Capture Group_, in seconds. It is the time period, at which the Statistics contained by a capture group are sampled and printed out to the log files.

*Capture Control Mode:* +
It is a functionality which allows choosing between several capture control modes for starting and stopping statistics capture, such as: _Manual_ _Load-based_ and _Complete_ _Capture Control_ modes.

*Manual Capture Control:* +
It is a _Capture Control Mode_ that starts/stops the statistics capture according to a manual user interaction (for example a button pressed on the TitanSim GUI).

*Load-based Capture Control:* +
It is a _Capture Control Mode_, which makes it possible to start/stop capture according to load stability measured in the system. The load Variable can be any user-defined load-related parameter supported by the TitanSim tool (for example the cps generated by the tool, CPU load of the IUT, etc.).

*Complete Capture Control:* +
It is a _Capture Control Mode_, which starts statistics capture automatically when the test execution is started and automatically stops it when the test execution is finished.

= Abbreviations

CLL:: Core Library

EPTF:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TitanSim:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TTCN-3:: Testing and Test Control Notation version 3 <<_1, ‎[1]>>.

= References

[[_1]]
[1] ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. +
http://www.etsi.org/deliver/etsi_es/201800_201899/20187301/03.02.01_60/es_20187301v030201p.pdf[Part 1: Core Language]

[[_2]]
[2] User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3] EPTF Core Library for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4] EPTF Core Library for TTCN-3 toolset with TITAN, +
http://ttcn.ericsson.se/TCC_Releases/Libraries/EPTF_Core_Library_CNL113512/doc/apidoc/html/index.html[Reference Guide]

[[_5]]
[5] EPTF Core Library Variable Function Description

[[_6]]
[6] EPTF Core Library Statistics Measure Function Description

[[_7]]
[7] EPTF Core Library Base Function Description
