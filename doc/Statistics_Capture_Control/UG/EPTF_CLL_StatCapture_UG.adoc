---
Author: József Gyürüsi
Version: 15/198 17-CNL 113 512, Rev. E
Date: 2015-06-22

---
= EPTF CLL Statistics Capture Control, User Guide
:author: József Gyürüsi
:revnumber: 15/198 17-CNL 113 512, Rev. E
:revdate: 2015-06-22
:toc:

== About This Document

=== How to Read This Document

This is the User Guide for the Statistics Capture Control of the Ericsson Performance Test Framework (TitanSim), Core Library (CLL). EPTF Core Library is developed for the TTCN-3 <<_1, [1]>> Toolset with TITAN <<_2, [2]>>. This document should be read together with the Function Description of the Statistics Capture Control feature <<_5, [5]>> . For more information on the EPTF Core Library, see the User Guide <<_4, [4]>> and the Function Specification <<_3, [3]>> of the TitanSim. Additionally, to understand the functionality of this feature, read the documentation of the Variable <<_7, [7]>> and the Statistics Measure <<_8, [8]>> features.

== System Requirements

In order to use the Statistics Capture Control feature the system requirements listed in EPTF Core Library User Guide <<_4, [4]>> should be fulfilled.

Additionally, this feature also uses the functionalities of the TCC Useful Function product <<_7, [7]>>.

== Release Information

This document contains actualized information on the TitanSim Release R7A01. The new functionalities compared to the release R2A02 concerning the EPTF Statistics Capture Control feature are the following:

* The feature is extended with the client component. The existing component behaves like before and adds server functionalities.
* The changes are backward-compatible to the initial release R2A01 and tested according to this requirement.

= Statistics Capture Control

== Overview

The EPTF CLL Statistics Capture Control feature consists of two fundamental components providing an implementation for statistics capture control in a load test environment.

Statistics are calculated belonging to EPTF Variables <<_7, ‎[7]>> by the EPTF Statistics Measure Feature <<_8, [8]>>. For each EPTF Variable, several statistics can be defined containing different type of values: for example variable content, minimum, maximum, delta, etc. Statistics can be reset independently, without changing the original Variable value.

These Statistics are captured by the EPTF Statistics Capture Control feature. Statistics can be grouped to capture groups, defining a certain sampling period, when the capture group is printed to file. Each capture group can be printed to `logfiles` of a format defined by the user. The feature implements a mechanism to handle large `logfiles` for meeting the requirements of a typical load test use case.

[[basic_usage]]
== Basic Usage

The EPTF Statistics Capture Control feature is implemented by more components. The previously described functionality is implemented on the `EPTF_StatCapture_CT` component that provides the statistics capture functionality and offers an API for starting/stopping statistics capture. This component itself can be used as a stand-alone application for statistics capture and statistics capture control. The following figure (see ‎<<basic_configuration, Basic Configuration>>) shows the test configuration. In this case the demo application is based on the `EPTF_StatCapture_CT` and the two belonging modules only (i.e. the optional modules are not needed).

[[basic_configuration]]
=== Basic Configuration

image:images/EPTF_stat_capture_demo_CT.png[alt]

The `EPTF_StatCapture_demo_CT` is the user application, it extends the `EPTF_StatCapture_CT` component. Statistics Capture is carried out by the functionality of the `EPTF_StatCapture_CT` component and it is controlled by function calls according to its API. That means, that the statistics capture can be started or stopped by calling the belonging API functions in the user application.

== Extended Usage

The second component implemented in the EPTF Statistics Capture Control feature is the `EPTF_StatCaptureControl_CT,` it extends the `EPTF_StatCapture_CT` component and it is an optional, add-on component, that provides extended functionality by implementing _automatic_ statistics capture control based on external variables (see <<extended_configuration, Extended Configuration>>), for example defining certain capture start/stop criteria based on load stability in the system or user interaction.

=== Capture Control Modes

There are three different Capture Control Modes offered by the `EPTF_StatCaptureControl_CT_`(they can be selected by the configuration file): Manual, Load-based and Complete Capture Control Mode.

==== Manual (or User) Capture Control Mode

If this capture control mode is selected by the user, statistics capture can be started and stopped any time (after the execution has been started) by user interaction (for example a button is pressed on the TitanSim GUI). An EPTF Variable of type boolean should be defined in the configuration file, which shows user interaction (value change from false->true initiates start capture). By default, this control mode is selected.

==== Load-Based Capture Control Mode

If this capture control mode is chosen by the user, statistics capture is controlled according to load stability and user-specified timer intervals (module parameters: `tsp_EPTF_StatCaptureControl_loadStableToStart`, `tsp_EPTF_StatCaptureControl_loadStableCapturePeriod` and the `tsp_EPTF_StatCaptureControl_loadCaptureDelay` in the config file, see Section ‎<<modules-eptf-cll-statcapturecontrol, Modules `EPTF_CLL_StatCaptureControl`>>.

For convenience reasons, in the further description the "*" abbreviation is used instead of the string `tsp_EPTF_StatCaptureControl`.

The Load-based mode continuously monitors an EPTF Variable which signals whether the load is stable in the system. Note that this Variable is a boolean variable (specified by the user with the `loadVariable` module parameter) and it can be any regulated load-related parameter supported by the TitanSim tool, for example cps generated by the tool, or CPU load of the IUT, etc.

The Statistics Capture is started after the load has been stable for the time specified by the user in the `loadStableToStart` module parameter. If the load remains stable for a time specified by the `loadStableCapturePeriod` module parameter, then the captured data during this period are considered valid and the capture of information is stopped.

After a successful capture, the user can define a delay to start a new capture by the `loadCaptureDelay` module parameter.

NOTE: Only a fixed delay interval can be specified via this parameter. By default, it is set to `_0.0._`

After the delay period is over, the `loadStableToStart` timer is started again to examine the load stability and to initiate a new capture.

Otherwise, if the load becomes unstable during the `loadStableCapturePeriod` the capture is stopped and the captured data so far during this capture data collection period becomes obsolete and have to be discarded (this is signaled in the capture file as well). In this case, the `loadStableToStart` timer is started again to initiate a new capture.

The maximum number of capture periods (i.e. the number of start capture-stop capture sequences) initiated (in Load-based mode) can be defined via the `nrOfCaptures` module parameter. This counter is increased independently of the success of capture periods. Once the maximum number is reached, no more capture periods can be initiated. The load generation is allowed to be stopped before the maximum number of capture periods is reached (see Figure below).

The example scenario depicted in the figure shows the timers and the statistics capture control according to the load Variable.

See Example of Load-based Capture Control in the figure below:

image:images/load_based_capture_control.png[alt]

==== Complete (or Execution) Capture Control Mode

If this capture control mode is selected, the capture of statistics is automatically started as soon as the test execution is started and automatically stopped when the test execution has been finished.

[[extended_configuration]]
=== Extended Configuration

image:images/extended_configuration.png[alt]

In this configuration, the user application runs on the `EPTF_StatCaptureControl_demo_CT` which extends the `EPTF_StatCaptureControl_CT` component. This latter contains additional functionality to the `EPTF_StatCapture_CT` component (extends it), for example the automatic capture control functionality. Thus the starting or stopping of statistics can be bound to the fulfillment of certain conditions for values of Variables defined by the user, and the statistics capture can be controlled automatically.

The Capture Control Mode can be selected via the configuration file and the Variables belonging to the Capture Control Modes can be described here as well with the owner component name and their variable name. Note that all these Control Variables must be of type Boolean.

For example, the `Execution_CT` component contains a boolean Variable which signals that the load test execution has been started or stopped. The `User_CT` component contains a boolean Variable which shows whether the user has pressed the `Start Capture`/`Stop Capture` button on the TitanSim GUI. The `Load_CT` component contains a boolean Variable which shows whether load is stable or not. The demo application automatically controls statistics capture according to the selected Capture Control Mode and the values of these Variables.

For accessing these Variables in the user application, an `EPTF Name Service Server` must be running in the system, and the demo application must extend a belonging EPTF Name Service Client `EPTF_NS_Client_CT` component.

[[description_of_files_in_this_feature]]
== Description of Files in This Feature

The EPTF CLL Statistics Capture Control API includes the following files:

* StatCapture
** __EPTF_CLL_StatCapture_Definitions.ttcnpp__ - This TTCN-3 module contains common type definitions that should be used in all Statistics Capture Controlling Components.
** __EPTF_CLL_StatCapture_Functions.ttcnpp__ - This TTCN-3 module contains the implementation of Statistics Capture Control functions.
** __EPTF_CLL_StatCapture_External.cc__ - This TTCN-3 module contains external functions for Statistics Capture Control.
* `StatCaptureControl` (extends `StatCapture`)
** __EPTF_CLL_StatCaptureControl_Definitions.ttcnpp__ - This TTCN-3 module is optional, it contains common type definitions that should be used in all Statistics Capture Controlling Components if _automatic_ statistics capture control is needed.
** __EPTF_CLL_StatCaptureControl_Functions.ttcnpp__ - This TTCN-3 module is optional, it contains the implementation of Statistics Capture Control functions for _automatic_ statistics capture control.

NOTE: The `StatCaptureControl` component and the belonging modules are optional. If the user application only needs programmable statistics capture control, these files are not necessary to include. This case the user application should extend the `EPTF_StatCapture_CT` component and include the files listed in the `StatCapture` bullet point. On the other hand, if automatic statistics capture control functionality is needed, the user application should include all files of the EPTF CLL Statistics Capture Control feature (both `StatCapture` and `StatCaptureControl` bullet points), and extend the `EPTF_StatCaptureControl_CT` component.

[[description_of_required_files_from_other_features]]
== Description of Required Files From Other Features

The Statistics Capture Control feature is part of the TitanSim EPTF Core Library (CLL). It relies on several features of the CLL and the TCC Useful Functions. The user has to obtain the products/files to be found under the respective feature names:

* `Base`
* `Common`
* `FBQScheduler`
* `FreeBusyQueue`
* `HashMap`
* `Scheduler`
* `StatMeasure`
* `Variable`
* `NameService` (for the optional `StatCaptureControl`)

From the TCC Useful Functions <<_8, ‎[8]>>:

** _TCCFileIO.cc_
** __TCCFileIO_Functions.ttcn__
** _TCCMaths.cc_
** __TCCMaths_Functions.ttcn__
** __TCCMaths_GenericTypes.ttcn__

== Installation

Since EPTF CLL Statistics Capture Control is used as a part of the TTCN-3 test environment this requires TTCN-3 Test Executor to be installed before any operation of these functions. For more details on the installation of TTCN-3 Test Executor see the relevant section of <<_2, [2]>>.

If not otherwise noted in the respective sections, the following are needed to use `EPTF_CLL_StatCapture`:

* Copy the files listed in section <<description_of_files_in_this_feature, Description of Files in This Feature>> and <<description_of_required_files_from_other_features, Description of Required Files From Other Features>> to the directory of the test suite or create symbolic links to them.
* Import the Statistics Capture Control demo or write your own application using `StatCapture`.
* Create _Makefile_ or modify the existing one. For more details see the relevant section of <<_2, [2]>>.
* Edit the config file according to your needs, see following section <<configuration, Configuration>>.

[[configuration]]
== Configuration

The executable test program behavior is determined via the run-time configuration file. This is a simple text file, which contains various sections. The usual suffix of configuration files is _.cfg_. For further information on the configuration file see <<_2, [2]>>.

This feature defines TTCN-3 module parameters as defined in <<_2, [2]>>, clause 4. Actual values of these parameters – when there is no default value or a different from the default actual value to be used – shall be given in the `[MODULE_PARAMETERS]` section of the configuration file.

[[module-parameters-of-the-eptf-cll-statcapture]]
=== Module Parameters of the `EPTF_CLL_StatCapture`

* `tsp_EPTF_StatCapture_def_capture file`
+
This charstring type module parameter is defined in module `EPTF_CLL_StatCapture_Functions`. It is used to define the name of the default capture file, where those capture groups, which are created without any assigned `logfiles` are dumped. The default value for this parameter is set to `DefaultFile_<selfname>__<component id>__<date>_<time>.txt` where `selfname` is the test component self name and the component id the component identifier.

* `tsp_EPTF_StatCapture_max_file_size`
+
This integer type module parameter is defined in module `EPTF_CLL_StatCapture_Functions`. It is used to define the maximum file size of the `logfiles`, where the capture is dumped. By default, this file size is set to 1 GB (`1000000000` is the default value).
+
NOTE: 2 GB can be a critical size for some OS.

* `tsp_EPTF_StatCapture_max_nrof_files`
+
This integer type module parameter is defined in module `EPTF_CLL_StatCapture_Functions`. It defines the maximum number of files that can be opened for one data stream. That means, if a `logfile` is full, how many new files can be opened after to continue dumping. By default, this parameter is set to `_100_`.

* `tsp_EPTF_StatCapture_fileFormat`
+
This enumerated type module parameter is defined in module `EPTF_CLL_StatCapture_Functions`. It defines the output file format for the `logfiles`. Possible values are: _readable_ or _gnuplot_. By default, this parameter is set to the _readable_ format. Both formats are aimed to be post-processed easily. The _readable_ format is easier to read and it is inherited from a previous project and the _gnuplot_ format is aimed for use with Gnuplot.

* `tsp_EPTF_StatCapture_captureGroups`
+
This module parameter is of type `EPTF_StatCapture_CaptureGroupsConfig`, and it is defined in module `EPTF_CLL_StatCapture_Functions`. With this module parameter it is possible to define Capture Groups (and belonging statistics) from the configuration file. In this case, to process this parameter, the function `f_EPTF_StatCapture_parseConfigGroup()` should be called _after_ the local Variables (for which Statistics are captured) are available/created/subscribed for on the user application component. By default, this parameter is set to an empty record.

* `tsp_EPTF_StatCapture_headerSeparator`
+
This module parameter specifies the separator character (or string) that the `StatCapture` is use when writing a statistics header line containing the name of the stats. The default value is a single space.

* `tsp_EPTF_StatCapture_statNameSeparatorSubstitute`
+
This module parameter specifies the character or string that is to be substituted instead of the separator character in statistics names when writing a header line. The default value is a single underscore ("_").

[[modules-eptf-cll-statcapturecontrol]]
=== Modules `EPTF_CLL_StatCaptureControl`

* `tsp_EPTF_StatCaptureControl_captureControlMode`
+
This enumerated type module parameter is defined in module `EPTF_CLL_StatCaptureControl_Functions`. It defines the desired Capture Control Mode for automatic statistics capture control. Possible values are: `_manual_`, `_loadBased_`, `_complete_`. By default, the parameter is set to `_manual_` control mode.

* `tsp_EPTF_StatCaptureControl_nrOfCaptures`
+
This integer type module parameter is defined in module `EPTF_CLL_StatCaptureControl_Functions`. It specifies the number of periods of starting-stopping capture, and its value is only considered if the `loadBased` Capture Control Mode has been selected. Its default value is set to `_100_`.

* `tsp_EPTF_StatCaptureControl_loadStableToStart`
+
This float type module parameter is defined in module `EPTF_CLL_StatCaptureControl_Functions`. It specifies the time interval, for which the load Variable must be stable in order to start capture. By default, it is set to `_10.0_` (seconds).

* `tsp_EPTF_StatCaptureControl_loadStableCapturePeriod`
+
This float type module parameter is defined in module `EPTF_CLL_StatCaptureControl_Functions`. It specifies the time interval, for which statistics capture is carried out. By default, it is set to `_600.0_` (seconds).

* `tsp_EPTF_StatCaptureControl_loadCaptureDelay`
+
This float type module parameter is defined in module `EPTF_CLL_StatCaptureControl_Functions`. It defines the time delay, which is started after a successful capture. By default, its value is set to `_0.0_` (seconds).

* `tsp_EPTF_StatCaptureControl_userVariable`
+
This module parameter is defined in module `EPTF_CLL_StatCaptureControl_Functions`. It is an internal record type containing the following fields of type charstring: `{componentName, varName}`. In the `componentName` field the owner component's name should be specified and to the `varName` field the name of the capture controlling EPTF Variable should be assigned.
+
For example: `tsp_EPTF_StatCaptureControl_userVariable := {componentName := "User_CT", varName := "UserVar"}.`

* `tsp_EPTF_StatCaptureControl_loadVariable`
+
This module parameter is defined in module `EPTF_CLL_StatCaptureControl_Functions`. It is an internal record type containing the following fields of type charstring: `{componentName, varName}.` In the `componentName` field the owner component's name should be specified and to the `varName` field the name of the capture controlling EPTF Variable should be assigned.
+
For example: `tsp_EPTF_StatCaptureControl_loadVariable := {componentName := "Load_CT", varName := "LoadStableVar"}.`

* `tsp_EPTF_StatCaptureControl_executionVariable`
+
This module parameter is defined in module `EPTF_CLL_StatCaptureControl_Functions`. It is an internal record type containing the following fields of type charstring: `{componentName, varName}`. In the `componentName` field the owner component's name should be specified and to the `varName` field the name of the capture controlling EPTF Variable should be assigned.
+
For example: `tsp_EPTF_StatCaptureControl_executionVariable := {componentName := "Execution_CT", varName := "ExecutionVar"}`.

= Error Messages

NOTE: Besides the below described error messages, error messages shown in ‎<<_2, [2]>> or those of other used features or product may also appear.

`*New StatCapture group name is not unique.*`

The group name assigned to create a new capture group is already in use. Group names should be unique for each capture group.

`*No output file defined for Capture Group.*`

This error message is printed out, when the capture group is created with no `logfile` parameter assigned and also no default `logfile` is set in the config file.

`*Group does not exist.*`

This error message states that the input parameter of a function refers to a non-existent capture group.

`*Statistics index inside the group does not exist*`

This error message states that the input parameter of a function refers to a non-existing statistics inside a capture group.

`*Cannot dump to file*`

This error message occurs, when writing or appending to a file is not possible.

`*Cannot open logfile*`

This error message occurs, when opening a `logfile` is not possible.

`*Cannot create Statistics, StatID already exists!*`

This error message occurs, when the statistics ID to be created already exists.

`*The same statistics already added to the group!*`

This error message states that the statistics with the same `statID` is already added to the group.

`*Statistics with periodic reset cannot be added to more capture groups.*`

This error message occurs, when a statistics with periodic reset enabled is added to more than one capture group. This is not allowed as it would lead to side effects when resetting the statistics.

= Warning Messages

NOTE: Besides the below described warning messages shown in <<_2, [2]>> or those of other used features or product may also appear.

`*Group to be deleted does not exist.*`

This warning message states that the input parameter of the group deletion function refers to a non-existent capture group.

`*Group is not found for name <groupname>*`

This warning message states that the input parameter (`groupname`) is not valid.

`*No capture data defined for this capture group*`

This warning message occurs, when the capture of an empty capture group should be started.

`*Group does not exist.*`

This warning message states that the input parameter of a function refers to a non-existent capture group. In this case, it is only a warning, as it does not have any harmful effect (for example when deleting a non-existent capture group).

`*File size too low. Logstring cannot be dumped.*`

This warning message states that the config file parameters regarding maximum file size and maximum number of files are set too low.

`*The last file <file descriptor> is full, capture finished. Data is not logged to file.*`

This warning message occurs, when dumping a capture stream has filled the maximum number of files (defined in the config file). From this timepoint, no more capture is possible for that stream.

= Examples

The "demo" directory of the deliverable contains the following examples:

* _main.cfg_
* __EPTF_StatCapture_demo.ttcn__
* __EPTF_StatCaptureControl_demo.ttcn__

== Configuration File

The used configuration file (_main.cfg_) is for the Statistics Capture Control example is placed in the demo directory.

In case the `tsp_EPTF_StatCapture_def_capture_file` module parameter is left empty, each capture group created with an empty log file list is captured to a file called __Default_Capture_File.txt__. By setting the module parameter to a filepath, the default capture file can be changed.

[source]
----
[MODULE_PARAMETERS]

//StatCapture config parameters
tsp_EPTF_StatCapture_def_capture_file := "DefaultFile"
tsp_EPTF_StatCapture_max_file_size := 1000000 //1 MByte
tsp_EPTF_StatCapture_max_nrof_files := 100  //maximum 100 files for each // stream
tsp_EPTF_StatCapture_fileFormat := gnuplot   //gnuplot/readable

tsp_EPTF_StatCapture_captureGroups :=
{{groupName :=  "configgroup1",
  timerPeriod := 1.0,
  logFileNames := {"configFirstfile"},
  statistics := { { varName := "var1",
                                         statType := content,
			         statName := "content",
                    periodicReset := false,
                    activeStat := true
                   },
	       { varName := "var1",
                    statType := delta,
                    statName := "delta",
	         periodicReset := false,
	         activeStat := true
	        }
                 }
 },
 {{groupName :=  "configgroup2",
  timerPeriod := 1.0,
  logFileNames := {"configSecondfile"},
  statistics := { { varName := "var2",
                                         statType := deltaSum,
			         statName := "deltaSumReset",
                    periodicReset := true,
                    activeStat := true
                   },
	       { varName := "var2",
                    statType := EPS,
                    statName := "EPS",
	         periodicReset := false,
	         activeStat := true
	        }
                 }
 }
}

//StatCaptureControl config parameters
tsp_EPTF_StatCaptureControl_captureControlMode := loadBased; //manual/loadBased/complete
tsp_EPTF_StatCaptureControl_nrOfCaptures := 4; // 4 periods of //start-stop capture
tsp_EPTF_StatCaptureControl_loadStableToStart := 5.0;//the load //Variable must be stable for this long to start capture
tsp_EPTF_StatCaptureControl_loadStableCapturePeriod:= 10.0; //10 //seconds long measurement
tsp_EPTF_StatCaptureControl_loadCaptureDelay := 5.0; //5 //seconds long delay after successful capture
tsp_EPTF_StatCaptureControl_userVariable := {componentName := "User", varName := "UserVar"};
tsp_EPTF_StatCaptureControl_loadVariable := {componentName := "Load", varName := "LoadVar"};
tsp_EPTF_StatCaptureControl_executionVariable := {componentName := "Execution", varName := "ExecutionVar"};

//StatCaptureControl Demo config parameters
tsp_StatCaptureControl_demo_user_modify_timer := 20.0;
tsp_StatCaptureControl_demo_load_modify_timer := 30.0;
tsp_StatCaptureControl_demo_execution_start_timer := 1.0;
tsp_StatCaptureControl_demo_execution_stop_timer := 149.0;     //relative time from execution start
----

== Demo Modules

=== Basic Usage

The demo module __EPTF_StatCapture_demo.ttcn__ illustrates the basic usage of the EPTF Statistics Capture Control feature (see ‎<<basic_usage, Basic Usage>>.

Two variables are created by the user (`EPTF_Var`). For the first variable, two statistics are defined, one representing the content value, the second one representing the delta of that variable (`EPTF_StatMeasure`). The second variable is a float variable, its `deltaSum` is measured in a third statistics.

NOTE: This statistics is set to `periodicreset = true`, which means its value is reset at sampling intervals of the capture group it belongs to.

[source]
----
var integer vl_var1ID, vl_var2ID:= -1;
var integer vl_stat1ID, vl_stat2ID, vl_stat3ID := -1;

f_EPTF_Var_newInt("var1",10,vl_var1ID);
vl_stat1ID := f_EPTF_StatMeasure_newStat(vl_var1ID, content);
var EPTF_StatCapture_tStatistics stat1 := f_EPTF_StatCapture_createStatforCapture(vl_stat1ID, "mystat1name");
vl_stat2ID := f_EPTF_StatMeasure_newStat(vl_var1ID, delta);
var EPTF_StatCapture_tStatistics stat2 := f_EPTF_StatCapture_createStatforCapture(vl_stat2ID, "mystat2name");
f_EPTF_Var_newFloat("var2",10.0,vl_var2ID);
vl_stat3ID := f_EPTF_StatMeasure_newStat(vl_var2ID, deltaSum, true);
var EPTF_StatCapture_tStatistics stat3 := f_EPTF_StatCapture_createStatforCapture(vl_stat3ID, "mystat3name");
----

The Statistics references are created for Capture. Then, they are added to Capture groups, for example:

`f_EPTF_StatCapture_addNewGroup"`group2", 1.0,\{fd2}, \{stat3}, v_groupidx2, group2statidxList);`

The capture is started, and in the altstep statement, the underlying Variable values are modified during runtime. The Statistics values are refreshed automatically, and captured by the `StatCapture` component. Their values are printed to file at regular sampling intervals.

[source]
----
f_EPTF_StatCapture_startCaptureAll();
…
alt {
    …
    [ ] t_adjust.timeout{
         f_EPTF_Var_adjustContent(vl_var1ID, {intVal:= vl_now});
         f_EPTF_Var_adjustContent(vl_var2ID, {floatVal:= int2float(vl_now)}); vl_now := vl_now * 2; t_adjust.start; repeat;}

    [ ] t_wait.timeout {}
  }
  setverdict(pass);
  f_EPTF_StatCapture_stopCaptureAll();
  f_EPTF_Base_cleanup();
----

NOTE: At the end of the demo application, the Base cleanup function is called (not the `StatCapture` cleanup function!). As we have registered the `StatCapture` cleanup function to the Base component in our `EPTF_StatCapture_init` function with the command `f_EPTF_Base_RegisterCleanup(refers(f_EPTF_StatCaptureControl_cleanup));`, the Base component looks after the clean-up of all components in the demo system.

=== Extended Usage

The demo module __EPTF_StatCaptureControl_demo.ttcn__ shows the extended usage of the EPTF Statistics Capture Control feature (see ‎<<basic_usage, Basic Usage>>) and illustrates the automatic capture control functionality with Capture Control Modes.

The demo testcase runs on the `mtc_test` component, which first creates the components as described in the `extendeD` configuration section (see ‎‎<<extended_configuration, Extended Configuration>>), and starts them with their behavior.

[source]
----
function f_EPTF_mtc_test_Behaviour_1(in charstring pl_selfName) runs on mtc_test_CT {

  f_init_mtc_test_CT(pl_selfName);
  var NS_test_CT vl_NS_Server := NS_test_CT.create;
  vl_NS_Server.start(f_EPTF_NS_main_CT("NameServiceServer"));
  var User_test_CT vl_User_test := User_test_CT.create;
  vl_User_test.start(f_EPTF_User_test_Behaviour_1("User",
                                           vl_NS_Server));
  …
 }
----

The `StatCaptureControl` component calls its init function, registers itself into the Name Service Server and creates Statistics, groups them into Capture Groups and defines belonging `logfiles` to the groups. Additionally, the values of Statistics are regularly updated for simulating a working system. Note that this behavior code is almost identical to the basic usage demo. The important difference is that in the init function of the `StatCaptureControl_test_CT` we call the `EPTF_StatCaptureControl_init` function instead of the `EPTF_StatCapture_init` function. This means, that we prepare for listening to the controls such as execution, load stability and user interaction.

[source]
----
function f_init_StatCaptureControl_test_CT(in charstring pl_selfName, in EPTF_NS_CT pl_NSCompRef) runs on StatCaptureControl_test_CT {

  if (v_StatCaptureControl_test_initialized) {
    return; // already initialized
  }
  f_EPTF_StatCaptureControl_init(pl_selfName, pl_NSCompRef);
  v_StatCaptureControl_test_initialized := true;
  log("----StatCaptureControl test INIT DONE----");
}
----

In the meanwhile, the control components (`User_CT`, `Load_CT`, `Execution_CT`) are also started. In the demo application, these components simulate a real system, where the User Variable is set according to a real button click event, etc. Therefore, with belonging configuration parameters, the timers for changing control Variable values on components are read in from the configuration file (for example when to start/stop test execution, when the load should become stable/unstable, when the user should press the `start`/`stop` button). These components read in the configuration file parameters and change control Variable values accordingly. They all call the Base cleanup function when terminated, except the Execution component; this one calls the `f_EPTF_Base_stopRemote(mtc)` function with `mtc` as parameter. This means, if the test execution has finished, all components are stopped, which is the simulated and expected behavior of any TitanSim application with automatic Statistics Capture Control.

= Terminology

*Core Library (CLL):* +
It is that part of the TitanSim software that is totally project independent. (i.e., which is not protocol-, or application-dependent). The EPTF Core Library is to be supplied and supported by the TCC organization. Any EPTF Core Library development is to be funded centrally by Ericsson

*Variables:* +
They contain values, from which _TitanSim Statistics_ can be calculated automatically on changes or periodically.

*Statistics Measure:* +
It is a feature, which is responsible for creating _TitanSim Statistics_ and updating their value when required.

*Statistics:* +
They are TTCN-3 variables. The values of such variables are automatically and periodically updated in the background.

*Capture Group:* +
It is a concept for grouping Statistics with the same _Sampling Time_ together, capturing referenced variables at sampling intervals and printing contents of the group to the log files assigned to the group.

*Sampling Time:* +
It is defined per _Capture Group_, in seconds. It is the time period, at which the Statistics contained by a capture group are sampled and printed out to the log files.

*Capture Control Mode:* +
It is a functionality which allows choosing between several capture control modes for starting and stopping statistics capture, such as: _Manual_, _Load-based_ and _Complete_ _Capture Control_ modes.

*Manual Capture Control:* +
It is a _Capture Control Mode that_ starts/stops the statistics capture according to a manual user interaction (for example a button pressed on the TitanSim GUI).

*Load-based Capture Control:* +
It is a _Capture Control Mode_, which makes it possible to start/stop capture according to load stability measured in the system. The load Variable can be any user-defined load-related parameter supported by the TitanSim tool (for example the cps generated by the tool, CPU load of the IUT, etc.).

*Complete Capture Control:* +
It is a _Capture Control Mode_, which starts statistics capture automatically when the test execution is started and automatically stops it when the test execution is finished.

= Abbreviations

CLL:: Core Library

EPTF:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TitanSim:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TTCN-3:: Testing and Test Control Notation version 3 <<_1, [1]>>.

= References

[[_1]]
[1] ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. http://www.etsi.org/deliver/etsi_es/201800_201899/20187301/03.02.01_60/es_20187301v030201p.pdf[Part 1: Core Language]

[[_2]]
[2] User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3] EPTF Core Library for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4] EPTF Core Library for TTCN-3 toolset with TITAN, User Guide

[[_5]]
[5] EPTF Core Library Statistics Capture Control Function Description

[[_6]]
[6] EPTF Core Library for TTCN-3 toolset with TITAN +
http://ttcn.ericsson.se/TCC_Releases/Libraries/EPTF_Core_Library_CNL113512/doc/apidoc/html/index.html[Reference Guide]

[[_7]]
[7] EPTF Core Library Variable User Guide

[[_8]]
[8] EPTF Core Library Statistics Measure User Guide
