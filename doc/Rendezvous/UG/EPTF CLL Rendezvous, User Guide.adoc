---
Author: Zsolt Szalai
Version: 23/198 17-CNL 113 512, Rev. B
Date: 2009-06-08

---
= EPTF CLL Rendezvous, User Guide
:author: Zsolt Szalai
:revnumber: 23/198 17-CNL 113 512, Rev. B
:revdate: 2009-06-08
:toc:

== About This Document

=== How to Read This Document

This is the User Guide for the EPTF Rendezvous of the Ericsson Performance Test Framework (TitanSim), Core Load Library (CLL). TitanSim CLL is developed for the TTCN-3 ‎<<_1, [1]>> Toolset with TITAN ‎<<_2, [2]>>. This document should be read together with the Function Description of the EPTF Logging feature ‎<<_5, [5]>>. For more information on the TitanSim CLL, consult the User's Guide <<_4, [4]>> and the Function Specification <<_3, ‎[3]>> of the TitanSim.

== System Requirements

In order to use the EPTF Rendezvous feature the system requirements listed in TitanSim CLL User Guide ‎<<_4, [4]>> should be fulfilled.

= EPTF Rendezvous

== Overview

The EPTF CLL Rendezvous component is a fundamental component providing _Rendezvous service_ for other entities. Rendezvous service is a generic solution for synchronization among/between various entities either locally and remotely.

The feature provides a server component which stores the rendezvous requests. Clients can send initial rendezvous requests of specific rendezvous types to this server. Upon receiving the requests the server checks whether the rendezvous service of the given type exists. If not, then it creates the rendezvous service instance. If the given type exists, then the server notifies the requestors with a rendezvous response. (See figure below)

See a typical Rendezvous service below:

image:images/timediagramrendezvous.jpeg[timediagram_rendezvous]

In the State-Trigger type of Rendezvous, the clients subscribe for a an ID, and when it is triggered by one of the Clients, all the others subscribed will be notified. (See figure below)

See `StateTrigger` type Rendezvous below:

image:images/rendezvous.jpeg[rendezvous]

To be able to use EPTF Rendezvous, the user should extend one `EPTF_Rendezvous_CT` component and every user components should extend an `EPTF_RendezvousClient_CT`. Their `init` functions must be run. After initialization several rendezvous types can be started.

== Supported Rendezvous Types

Supported rendezvous types are:

* "Wait For A Trigger" type rendezvous. The rendezvous ID is an integer number. There are two requestors. The success trigger is to receive the requests from the two requestors. Upon receiving the requests, the server notifies the requestors with a rendezvous response.
* "Wait For N Trigger" type rendezvous. The rendezvous ID is an integer number. There are N requestors. The success trigger is to receive request from all of the requestors. Upon receiving all the requests, the server notifies the requestors with a rendezvous response.
* "State Trigger" type rendezvous. The rendezvous ID is a charstring in this type of rendezvous. There is unlimited number of requestors, who subscribes for a rendezvous ID. The success trigger is a State Change from a Rendezvous Client to that particular rendezvous ID. At that event, the clients subscribed will execute a pre-defined call-back function specified at the subscription note.

== Description of Files in This Feature

The EPTF CLL Rendezvous API includes the following files:

* EPTF Rendezvous
** __EPTF_CLL_Rendezvous_Definitions.ttcnpp__ - This TTCN-3 module contains common type definitions that should be used in all EPTF Rendezvous Components.
** __EPTF_CLL_Rendezvous_Functions.ttcn__ - This TTCN-3 module contains the implementation of EPTF Rendezvous server functions.
** __EPTF_CLL_RendezvousClient_Functions.ttcn__ - This TTCN-3 module contains the implementation of EPTF Rendezvous client functions.

[[desc_req_files_other_feat]]
== Description of Required Files From Other Features

The EPTF Rendezvous feature is part of the TitanSim EPTF Core Load Library (CLL). It relies on several features of the CLL. To use the EPTF Rendezvous, the user has to obtain the respective files from the following features:

* `Common`
* `Base`
* `HashMap`
* `FreeBusyQueue`
* `Semaphore`
* `Logging`

== Installation

Since `EPTF_CLL_Rendezvous` is used as a part of the TTCN-3 test environment this requires TTCN-3 Test Executor to be installed before any operation of these functions. For more details on the installation of TTCN-3 Test Executor see the relevant section of ‎<<_2, [2]>>.

If not otherwise noted in the respective sections, the following are needed to use `EPTF_CLL_Rendezvous`:

* Copy the files listed in section <<desc_req_files_other_feat, Description of Required Files From Other Features>> to the directory of the test suite or create symbolic links to them.
* Import the Rendezvous demo or write your own application using EPTF Rendezvous
* Create _Makefile_ or modify the existing one. For more details see the relevant section of ‎‎<<_2, [2]>>.
* Edit the config file according to your needs, see following section <<config, Configuration>>.

[[config]]
== Configuration

The executable test program behavior is determined via the run-time configuration file. This is a simple text file, which contains various sections. The usual suffix of configuration files is _.cfg_. For further information on the configuration file see ‎<<_2, ‎[2]>>.

This EPTF Rendezvous feature does not define module parameters.

= Warning Messages

NOTE: Warning messages shown in ‎‎<<_2, [2]>> or those of other used features or product may also appear.

`*Unexpected message has arrived from sender, discarded*`

An unexpected message has been arrived to the EPTF Rendezvous port, which cannot be processed. The message will be discarded.

= Examples

The "demo" directory of the deliverable contains the following examples:

* __EPTF_ Rendezvous_Demo.ttcn__

== Demo Module

The demo module (__EPTF_ Rendezvous_Demo.ttcn__) illustrates a typical usage of the EPTF Rendezvous feature.

Starting a "Wait For A Trigger" type rendezvous:

[source]
----
function f_RendezvousClient_Behaviour(in charstring pl_selfName, in EPTF_Rendezvous_CT pl_server) runs on Client_CT{
  f_EPTF_RendezvousClient_init_CT(pl_selfName,pl_server);

  var integer vl_Idx;
  vl_Idx:=f_EPTF_RendezvousClient_WaitForATrigger(100);

  if(f_EPTF_RendezvousClient_WaitForResponse(vl_Idx))
  {
    log("---Trigger received---");
  }
  setverdict(pass);
  f_EPTF_Base_cleanup_CT();
}
----

Starting a "Wait For N Trigger" type rendezvous:

[source]
----
f_EPTF_RendezvousClient_init_CT(pl_selfName,pl_server);
  var integer vl_Idx;
  vl_Idx:=f_EPTF_RendezvousClient_WaitForNTrigger(100,3);
  // rendezvous ID=100, number of requestors=3
  if(f_EPTF_RendezvousClient_WaitForResponse(vl_Idx))
  {
    log("---Trigger received---");
  }
  setverdict(pass);
  f_EPTF_Base_cleanup_CT();
}
----

= Terminology

*TitanSim Core (Load) Library(CLL):* +
It is that part of the TitanSim software that is totally project independent. (I.e., which is not protocol-, or application-dependent). The TitanSim CLL is to be supplied and supported by the TCC organization. Any TitanSim CLL development is to be funded centrally by Ericsson.

*Rendezvous service:* +
It provides a generic solution for synchronization among/between various entities either locally and remotely with the help of a server component.

= Abbreviations

CLL:: Core Load Library

EPTF:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TitanSim:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TTCN-3:: Testing and Test Control Notation version 3 ‎<<_1, [1]>>.

= References

[[_1]]
[1] ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2] User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3] TitanSim CLL for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4] TitanSim CLL for TTCN-3 toolset with TITAN, User Guide

[[_5]]
[5] EPTF CLL EPTF Rendezvous, Function Description

[[_6]]
[6] TitanSim CLL for TTCN-3 toolset with TITAN +
http://ttcn.ericsson.se/products/libraries.shtml[Reference Guide]
