---
Author: Zsolt Szalai
Version: 23/155 16-CNL 113 512, Rev. B
Date: 2009-06-08

---
= EPTF CLL Rendezvous, Function Description
:author: Zsolt Szalai
:revnumber: 23/155 16-CNL 113 512, Rev. B
:revdate: 2009-06-08
:toc:

== How to Read This Document

This is the Function Description for the EPTF Rendezvous of the Ericsson Performance Test Framework (TitanSim), Core Load Library (CLL). TitanSim CLL is developed for the TTCN-3 <<_1, ‎[1]>> Toolset with TITAN <<_2, ‎[2]>>.

== Scope

This document is to specify the content and functionality of the EPTF Rendezvous feature of the TitanSim CLL.

== Recommended Way of Reading

The readers are supposed to get familiar with the concept and functionalities of TitanSim CLL ‎<<_3, [3]>>. They should get familiar with the list of acronyms and the glossary in the Terminology section.

= General Description

This document specifies the EPTF Rendezvous feature of the TitanSim CLL.

The EPTF Rendezvous feature makes it possible to

* Allow synchronization between various entities.
* Support a set of Rendezvous types for components
* Provide a server to store the requests

The aim of the EPTF Rendezvous feature is, to make a _Rendezvous service_. Rendezvous service provides a generic solution for synchronization among/between various entities either locally and remotely.

The feature provides a server component which stores the rendezvous requests. Clients can send initial rendezvous requests of specific rendezvous types to this server. Upon receiving the requests the server checks whether the rendezvous service of the given type exists. If not, then it creates the rendezvous service instance. If the given type exists, then the server notifies the requestors with a rendezvous response.

To be able to use EPTF Rendezvous, the user should extend one `EPTF_Rendezvous_CT` component and every user components should extend an `EPTF_RendezvousClient_CT`. Their `init` functions must be run.

= Supported Rendezvous Types

Supported rendezvous types are:

* "Wait For A Trigger" type rendezvous. The rendezvous ID is an integer number. There are two requestors. The success trigger is to receive the requests from the two requestors. Upon receiving the requests, the server notifies the requestors with a rendezvous response.
* "Wait For N Trigger" type rendezvous. The rendezvous ID is an integer number. There are N requestors. The success trigger is to receive request from all of the requestors. Upon receiving all the requests, the server notifies the requestors with a rendezvous response.
* "State Trigger" type rendezvous. The rendezvous ID is a charstring in this type of rendezvous. There is unlimited number of requestors, who subscribes for a rendezvous ID. The success trigger is a State Change from a Rendezvous Client to that particular rendezvous ID. At that event, the clients subscribed will execute a pre-defined call-back function specified at the subscription note.

= Functional Interface

Apart from this description a cross-linked reference guide for the TitanSim CLL Functions can be reached for on-line reading ‎<<_4, [4]>>.

== Naming Conventions

All functions have the prefix `f_EPTF_Rendezvous_` or `f_EPTF_RendezvousClient_`.

== Public Functions

=== Initialization of Rendezvous

Before using the EPTF Rendezvous functions the

`f_EPTF_Rendezvous_init_CT()`

function should be called. This initializes the EPTF Rendezvous feature and starts the Rendezvous server component. There should be one server component.

=== Initialization of `RendezvousClient`

Before using the EPTF `RendezvousClient` functions the

`f_EPTF_RendezvousClient_init_CT()`

function should be called. This initializes the EPTF `RendezvousClient` feature. Every client who would like to use Rendezvous server should extend `EPTF_RendezvousClient_CT` and should call this `init` function.

=== Start a Wait For A Trigger Type Rendezvous

`f_EPTF_RendezvousClient_StartWaitForATrigger (pl_rendezvousID)`

With the call of this function a client can start a 'Wait For A Trigger' type rendezvous. The parameter `pl_rendezvousID` is the ID of the rendezvous type. The function returns an ID which can be used for the waiting of the response.

=== Start a Wait For N Trigger Type Rendezvous

`f_EPTF_RendezvousClient_StartWaitForNTrigger (pl_rendezvousID, pl_numberOfClients)`

With the call of this function a client can start a 'Wait For N Trigger' type rendezvous. The parameter `pl_rendezvousID` is the ID of the rendezvous type, `pl_numberOfClients` are the number of requestors. The function returns an ID which can be used for the waiting of the response. It does not have to add the number of requestors. In that case the requestors haven’t been added the rendezvous starts with 2 requestors. The maximum number of requestors will be the maximum value `pl_numberOfClients` reaches. A client can start this type of rendezvous more than one time. In that case a client will get more than one response.

=== Subscribe for a State-Change Rendezvous

[source]
----
function f_EPTF_RendezvousClient_SubscribeToState(
in charstring pl_rendezvousID,
in ft_EPTF_Rendezvous_NotifyAction pl_action,
in boolean pl_permanentsubs := false)
----

This function will register the call-back function named `pl_action` provided and send a subscription note to the server, registering the ID the client is waiting for. Setting the `pl_permanentsubs` parameter will make the subscription permanent, subscriptions will not be erased after a state-change what is the default. There is no need to wait for the trigger to come; the call-back function will be automatically called at the triggering.

Calling

`function f_EPTF_RendezvousClient_Unsubscribe(in charstring pl_rendezvousID)`

will result in the deletion of a previously taken subscription.

=== Triggering a New State

[source]
----
function f_EPTF_RendezvousClient_NewState(
in charstring pl_rendezvousID,
in integer pl_rendezvousID_int := -1)
----

With this function a new state can be published to the clients subscribed to Rendezvous ID as the state. This will trigger the state-change and every client subscribed to this state will be notified and every call-back functions will be called by the clients automatically. An additional integer can be specified if necessary.

=== Wait the trigger of the Wait For A Trigger or a Wait For N Trigger Type Rendezvous

`f_EPTF_RendezvousClient_WaitForResponse (pl_semaphore)`

This function is used for waiting the response for the Wait For A Trigger type or a Wait For N Trigger type message. The variable returned by the function `f_EPTF_RendezvousClient_StartWaitForATrigger()` or `f_EPTF_RendezvousClient_StartWaitForNTrigger()` can be used as a parameter to this function (`pl_semaphore`). The function is blocking the execution until the response arrives. If the response arrives the function returns true. If the function called after the response has arrived, the execution won’t be blocked.

== Summary Table of All Public Functions for EPTF Rendezvous

See summary of EPTF Rendezvous functions in the table below:

[width="100%",cols="50%,50%",options="header",]
|===============================================================================================================================
|Function name |Description
|`f_EPTF_Rendezvous_init_CT` |Initializes the EPTF `Rendezvous_CT` component
|`f_EPTF_RendezvousClient_init_CT` |Initializes the EPTF `RendezvousClient_CT` component
|`f_EPTF_RendezvousClient_StartWaitForATrigger` |Function to send a `WaitForATrigger` type request
|`f_EPTF_RendezvousClient_StartWaitForNTrigger` |Function to send a `WaitForNTrigger` type request
|`f_EPTF_RendezvousClient_WaitForResponse` |Function to wait for the `WaitForATrigger` type or `WaitForNTrigger` type response message
|`f_EPTF_RendezvousClient_SubscribeToState` |Sends a subscription note for a `StateTrigger` type of rendezvous
|`f_EPTF_RendezvousClient_Unsubscribe` |Sends an unsubscription note for a `StateTrigger` type of rendezvous
|`_EPTF_RendezvousClient_NewState` |Changes the client’s state to the ID, triggering subscribed clients
|===============================================================================================================================

= Terminology

*TitanSim Core (Load) Library(CLL):* +
It is that part of the TitanSim software that is totally project independent. (I.e., which is not protocol-, or application-dependent). The TitanSim CLL is to be supplied and supported by the TCC organization. Any TitanSim CLL development is to be funded centrally by Ericsson.

*Rendezvous service:* +
It provides a generic solution for synchronization among/between various entities either locally and remotely with the help of a server component.

= Abbreviations

CLL:: Core Load Library

EPTF:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TitanSim:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TTCN-3:: Testing and Test Control Notation version 3 ‎<<_1, [1]>>

= References

[[_1]]
[1] ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2] User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3] TitanSim CLL for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4] TitanSim CLL for TTCN-3 toolset with TITAN +
http://ttcn.ericsson.se/products/libraries.shtml[Reference Guide]
