---
Author: Gábor Tatárka
Version: 5/198 17-CNL 113 512, Rev. C
Date: 2010-02-09

---
= EPTF CLL Common, User Guide
:author: Gábor Tatárka
:revnumber: 5/198 17-CNL 113 512, Rev. C
:revdate: 2010-02-09
:toc:

== About This Document

=== How to Read This Document

This is the User Guide for the Common of the Ericsson Performance Test Framework (TitanSim), Core Load Library (CLL). TitanSim CLL is developed for the TTCN-3 <<_1, ‎[1]>> Toolset with TITAN <<_2, ‎[2]>>. This document should be read together with the Function Description of the Common feature <<_5, ‎[5]>>. For more information on the TitanSim CLL, consult the Users Guide <<_4, ‎[4]>> and the Function Specification <<_3, ‎[3]>> of the TitanSim.

== System Requirements

In order to use the Common feature the system requirements listed in TitanSim CLL User Guide <<_4, ‎[4]>> should be fulfilled.

= Common

== Overview

The EPTF Common feature contains type and function definitions commonly used by other EPTF features.

[[description_of_files_in_this_feature]]
== Description of Files in This Feature

The EPTF CLL Common API includes the following files:

* Common
** __EPTF_CLL_Common_Definitions.ttcn__: +
This TTCN-3 module contains type definitions commonly used by other EPTF features.
** __EPTF_CLL_Common_Functions.ttcn__: +
This TTCN-3 module contains function definitions commonly used by other EPTF features.
** __EPTF_CLL_Common_PrivateDebugDefinitions.ttcnpp__: +
This file contains TTCN-3 constant, its value depends on whether the preprocessor flag EPTF_DEBUG is specified or not.
** __EPTF_CLL_Common_RndDefinitions.ttcn__: +
This TTCN-3 module contains type definitions of the RndValues sub-feature.
** __EPTF_CLL_Common_RndFunctions.ttcn__: +
This TTCN-3 module contains function definitions of the RndValues sub-feature.
** __EPTF_CLL_Common_IndexArrayDefinitions.ttcn__: +
This TTCN-3 module contains type definitions of the IndexArray record sub-feature.
** __EPTF_CLL_Common_IndexArrayFunctions.ttcn__: +
This TTCN-3 module contains function definitions of the IndexArray record sub-feature.

[[description_of_required_files_from_other_features]]
== Description of Required Files from Other Features

None.

== Installation

Since `EPTF_CLL_Common` is used as a part of the TTCN-3 test environment this requires TTCN-3 Test Executor to be installed before any operation of these functions. For more details on the installation of TTCN-3 Test Executor see the relevant section of <<_2, ‎[2]>>.

If not otherwise noted in the respective sections, the following are needed to use `EPTF_CLL_Common`:

1. Copy the files listed in section <<description_of_files_in_this_feature, Description of Files in This Feature>> and <<description_of_required_files_from_other_features, Description of Required Files from Other Features>> to the directory of the test suite or create symbolic links to them.
2. Write your application using EPTF Common.
3. Create _Makefile_ or modify the existing one. For more details see the relevant section of <<_2, ‎[2]>>.
4. Edit the config file according to your needs; see section <<configuration, Configuration>>.

[[configuration]]
== Configuration

The executable test program behavior is determined via the run-time configuration file. This is a simple text file, which contains various sections. The usual suffix of configuration files is _.cfg_. For further information on the configuration file see ‎<<_2, [2]>>.

The Common feature defines TTCN-3 module parameters as defined in <<_2, ‎[2]>>, clause 4. Actual values of these parameters – when no default value or a different from the default actual value wished to be used – shall be given in the `[MODULE_PARAMETERS]` section of the configuration file.

The Common feature defines the following module parameters:

* `tsp_EPTF_maxRunningTime`
+
This float type module parameter is defined in module `EPTF_CLL_Definitions`. It is used to define the maximum running time in seconds after which the application should exit.
+
Its default value is `_2147483.0_`, approximately 24 days.

* `tsp_EPTF_Common_RndValues_behaviour`
+
This float type module parameter is defined in module `EPTF_Common_RndValues_Functions`. It is used to define the behavior of the random number generation. The default value of this parameter is `_-1.0_`, it results in the rnd() built-in TTCN-3 function to be called without parameters so the random number sequence cannot be re-created. If its value is between `_0.0_` inclusive and `_1.0_` exclusive, that value is passed to rnd() the first time it is called. The number will be used as seed to the random number generator resulting in the same sequence of random numbers if the feature is started again with the same value. Other values are not allowed and will result in an error message.

* `tsp_EPTF_Common_RndValues_numberOfRndValues`
+
This integer type module parameter is defined in module `EPTF_Common_RndValues_Functions`. It is used to define the number of the generated random numbers.

= Error Messages

`*The component has been not initialized yet*`

The component is not initialized, but used.

`*Faulty input value in f_EPTF_Common_RndValues_init_CT*`

The given input value is out of range.

= Terminology

*TitanSim Core (Load) Library(CLL):* +
It is that part of the TitanSim software that is totally project independent. (I.e., which is not protocol-, or application-dependent). The TitanSim CLL is to be supplied and supported by the TCC organization. Any TitanSim CLL development is to be funded centrally by Ericsson

= Abbreviations

CLL:: Core Load Library

EPTF:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TitanSim:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TTCN-3:: Testing and Test Control Notation version 3 ‎<<_1, [1]>>.

= References

[[_1]]
[1] ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2] User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3] TitanSim CLL for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4] TitanSim CLL for TTCN-3 toolset with TITAN, User Guide

[[_5]]
[5] EPTF CLL Common, Function Description

[[_6]]
[6] TitanSim CLL for TTCN-3 toolset with TITAN +
http://ttcn.ericsson.se/products/libraries.shtml[Reference Guide]
