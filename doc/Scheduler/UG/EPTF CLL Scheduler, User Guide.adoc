---
Author: Zoltán Zsichla
Version: 14/198 17-CNL 113 512, Rev. D
Date: 2015-12-04

---
= EPTF CLL Scheduler, User Guide
:author: Zoltán Zsichla
:revnumber: 14/198 17-CNL 113 512, Rev. D
:revdate: 2015-12-04
:toc:

== How to Read This Document

This is the User Guide for the Scheduler of the Ericsson Performance Test Framework (EPTF), Core Library (CLL). EPTF CLL is developed for the TTCN-3 <<_1, [1]>> Toolset with TITAN <<_2, [2]>>. This document should be read together with the Function Description of the Scheduler feature <<_5, [5]>>. For more information on the EPTF CLL, consult the User Guide <<_4, [4]>> and the Function Specification <<_3, [3]>> of the TitanSim.

== System Requirements

In order to use the Scheduler feature the system requirements listed in EPTF CLL User Guide <<_4, [4]>> should be fulfilled.

= Scheduler

== Overview

The EPTF CLL Scheduler component is a fundamental component providing an implementation for scheduling in a load test environment.

The EPTF Scheduler feature makes it possible to schedule events (actions) that should happen at a given time measured from the start of the component or relative to the schedule of the event. The Scheduler uses only two TTCN-3 timers to implement scheduling. These are the component clock – which is started at initialization – and the next event timer.

== Red Black Tree Scheduler

The EPTF Scheduler feature contains an event queue which is based on Red Black Tree <<_6, [6]>>.

The Red Black Tree can insert, delete and search elements in `O(log n)` time.

[[description_of_files_in_this_feature]]
== Description of Files in This Feature

The EPTF CLL Scheduler API includes the following files:

* Scheduler
** __EPTF_CLL_Scheduler_Definitions.ttcn__ - This TTCN-3 module contains common type definitions of the Scheduler Component.
** __EPTF_CLL_RBTScheduler_Definitions.ttcn__ - This TTCN-3 module contains type definitions for the Red Black Tree based Scheduler
** __EPTF_CLL_RBTScheduler_Functions.ttcn__ - This TTCN-3 modlue contains function definitions for the Red Black Tree based Scheduler.

[[description_of_required_files_from_other_features]]
== Description of Required Files from Other Features

The Scheduler feature is part of the TitanSim EPTF Core Library (CLL). It relies on several features of the CLL. To use the Scheduler the user has to obtain the respective files from the following features:

* `Base`
* `Common`
* `Logging`
* `Red Black Tree`

== Installation

Since `EPTF_CLL_Scheduler` is used as a part of the TTCN-3 test environment this requires TTCN-3 Test Executor to be installed before any operation of these functions. For more details on the installation of TTCN-3 Test Executor see the relevant section of <<_2, [2]>>.

If not noted otherwise, in the respective sections, the following are needed to use `EPTF_CLL_Scheduler`:

* Copy the files listed in sections <<description_of_files_in_this_feature, Description of Files in This Feature>> and <<description_of_required_files_from_other_features, Description of Required Files from Other Features>> to the directory of the test suite or create symbolic links to them.
* Import the Scheduler demo or write your own application using Scheduler.
* Create _Makefile_ or modify the existing one. For more details see the relevant section of <<_2, [2]>>.
* Edit the config file according to your needs, see the following section <<configuration, Configuration>>].

[[configuration]]
== Configuration

The executable test program behavior is determined via the run-time configuration file. This is a simple text file, which contains various sections. The usual suffix of configuration files is _.cfg_. For further information on the configuration file see <<_2, [2]>>.

This Scheduler feature defines TTCN-3 module parameters as defined in <<_2, [2]>>, clause 4. Actual values of these parameters – when no default value or a different from the default actual value wished to be used – shall be given in the `[MODULE_PARAMETERS]` section of the configuration file.

This Scheduler feature defines the following module parameters:

* `tsp_debug_EPTF_SchedulerFunctions`
+
This boolean type module parameter is defined in module `EPTF_CLL_Scheduler_Definitions`. It is used to enable debug logging. By default, its value is false.

* `tsp_debugVerbose_EPTF_SchedulerFunctions`
+
This boolean type module parameter is defined in module `EPTF_CLL_Scheduler_Definitions`. It is used to enable debug logging. By default, its value is false.

* `tsp_EPTF_ELEMENTARY_TIMESTEP_PARAM`
+
This float type module parameter defines the granularity of the time-axis of the event queue.

* `tsp_EPTF_Scheduler_enableRounding`
+
This boolean type module parameter enables/disables rounding the desired time of the scheduled event.

* `tsp_EPTF_Scheduler_enableLoadMeasurement`
+
This boolean type module parameter enables/disables the load measurement functionality.

== DTE Handling

When the user wants his own function to be called at the time, when during the execution of his action a DTE happens, then this function can be given in the `f_EPTF_SchedulerComp_scheduleAction` `pl_dteHandler` parameter.

The given function, after the DTE intervention, will be called by the system and the action’s data just as the message of DTE will be transmitted in a parameter.

By default DTE handling is disabled.

== Load Measurement

Load measurement functionality can be used to detect/prevent overload situation. It measures the load based on the cumulative sum of lag of events during the measurement period (`LoadMeasurementPeriod`). When the measured load level exceeds the threshold (`MaxLoadThreshold`), the events to be scheduled into the past are scheduled in the future. This way the overload situation can be prevented.

By default the load measurement is enabled.

= Error Messages

NOTE: Besides the below described error messages, error messages shown in <<_2, [2]>> or those of other used features or product may also appear.

EPTF CLL Scheduler defines the following error messages:

`*Error: f_EPTF_SchedulerComp_CancelEvent: clearing invalid_chain or non-empty event at qidx: <index>*`

`*Error: f_EPTF_SchedulerComp_performActions has failed to freeze event: <event>*`

`*Error: f_EPTF_SchedulerComp_performActions has failed with event: <event>*`

`*Error: cannot remove current event from queue*`

`*ERROR: f_EPTF_SchedulerComp_performActions() has been failed*`

`*[f_EPTF_Scheduler_schedulerIsNotLate] Error: Cannot get smallest element, maybe tree is empty*`

`*[f_EPTF_Scheduler_eventIsValid] Error: checking non-existent event at pl_qidx: <index>*`

`*[f_EPTF_Scheduler_removeValidEvent] Error: clearing invalid, or non-empty event at pl_qidx: <index>*`

`*[f_EPTF_Scheduler_freezeBusyEventHead] Error: Cannot get smalledst element index. Tree might be empty.*`

`*[f_EPTF_Scheduler_freezeBustEventHead] Error: freezing non-existent event.*`

`*Error: f_EPTF_Scheduler_removeInvalidEvent: clearing non-invalid, or non-existent event at qidx: <index>*`

`*Error: getNewTimerVal: list is empty*`

`*Error: f_EPTF_Scheduler_schedulerIsNotLate: list is empty*`

`*Error: f_EPTF_Scheduler_eventIsValid: checking non-existent event at pl_qidx: <index>, pl_NextEventQueue.order.itemList[pl_qidx]: <value>*`

`*Error: f_EPTF_Scheduler_eventIsInvalid: checking non-existent event at pl_qidx: <index>, pl_NextEventQueue.order.itemList[pl_qidx]: <value>*`

`*Error: f_SetSchedulerActionRoundable: invalid not-busy event during appending to!: <value>*`

`*Error: f_EPTF_Scheduler_removeValidEvent: clearing invalid, or non-empty event at pl_qidx: <index>, pl_nextEventQueue.order.itemList[pl_qidx]: <value>*`

`*Error: f_EPTF_Scheduler_freezeBusyEventHead: freezing non-existent event: <event>, pl_NextEventQueue.order.itemList[pl_NextEventQueue.order.status.busyStatus.head]: <value>*`

`*Error: f_EPTF_Scheduler_removeInvalidEvent: clearing not-invalid, or non-existent event at qidx: <index>, pl_NextEventQueue.order.itemList[qidx]: <value>*`

`*Error: f_EPTF_Scheduler_rescheduleEvent: invalid not-busy event during appending to!: <value>*`

= Warning Messages

NOTE: Besides the below described warning messages, warning messages shown in <<_2, [2]>> or those of other used features or product may also appear.

The EPTF CLL Scheduler defines the following warning messages:

`*Warning: getNewTimerVal: scheduler is behind clock, timerval: <value>, event: <event>*`

= Examples

The "demo" directory of the deliverable contains the following examples:

* __EPTF_Scheduler_test.prj__
* __EPTF_Scheduler_test.ttcn__

== Demo Module

The demo module (__EPTF_Scheduler_test.ttcn__) illustrates a typical usage of the Scheduler feature.

= Terminology

*EPTF Core Library (CLL):* +
It is that part of the TitanSim software that is totally project independent. (That is, which is not protocol-, or application-dependent). The EPTF CLL is to be supplied and supported by the TCC organization. Any EPTF CLL development is to be funded centrally by Ericsson.

*Red Black Tree:* +
The EPTF Red Black Tree feature is a self-balancing binary search tree. It can be used to store indexes into an associated array.

= Abbreviations

API:: Application Programming Interface

CLL:: Core Library

EPTF:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TitanSim:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TTCN-3:: Testing and Test Control Notation version 3 <<_1, [1]>>.

= References

[[_1]]
[1] ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. +
http://www.etsi.org/deliver/etsi_es/201800_201899/20187301/03.02.01_60/es_20187301v030201p.pdf[Part 1: Core Language]

[[_2]]
[2] User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3] EPTF CLL for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4] EPTF CLL for TTCN-3 toolset with TITAN, User Guide

[[_5]]
[5] EPTF CLL Scheduler, Function Description

[[_6]]
[6] EPTF CLL Red Black Tree, User Guide

[[_7]]
[7] EPTF CLL for TTCN-3 toolset with TITAN +
http://ttcn.ericsson.se/TCC_Releases/Libraries/EPTF_Core_Library_CNL113512/doc/apidoc/html/index.html[Reference Guide]
