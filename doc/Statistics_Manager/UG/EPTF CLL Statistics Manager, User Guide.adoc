---
Author: Balázs Lugossy
Version: 34/198 17-CNL 113 512, Rev. B
Date: 2012-09-10

---
= EPTF CLL Statistics Manager, User Guide
:author: Balázs Lugossy
:revnumber: 34/198 17-CNL 113 512, Rev. B
:revdate: 2012-09-10
:toc:

== How To Read This Document

This is the User Guide for the Statistics Manager of the Ericsson Performance Test Framework (TitanSim), Core Load Library (CLL). TitanSim CLL is developed for the TTCN-3 <<_1, ‎[1]>> Toolset with TITAN ‎<<_2, [2]>>. This document should be read together with the Function Description of the Statistics Manager feature ‎<<_5, [5]>>. For more information on the TitanSim CLL consult the Users Guide <<_4, ‎[4]>> and the Function Specification ‎<<_3, [3]>> of the TitanSim. Additionally, to understand the functionality of this feature, read the documentation of the `StatMeasure` ‎<<_7, [7]>> feature.

== System Requirements

In order to use the Statistics Manager feature the system requirements listed in TitanSim CLL User Guide <<_4, ‎[4]>> should be fulfilled.

= Statistics Manager

== Overview

The EPTF CLL Statistics Manager component provides a way to declare statistics based on published data elements and to publish these statistics so that they can be used to visualize congestion.

[[descripton_of_files_in_this_feature]]
== Description of Files in This Feature

The EPTF CLL Statistics Manager API includes the following files:

* StatManager
** __EPTF_CLL_StatManager_Definitions.ttcn__ - This TTCN-3 module contains common type definitions that should be used in all Statistics Manager Components.
** __EPTF_CLL_StatManager_Functions.ttcn__ - This TTCN-3 module contains the implementation of Statistics Manager functions.

[[description_of_required_files_from_other_features]]
== Description of Required Files From Other Features

The Statistics Manager feature is part of the TitanSim EPTF Core Load Library (CLL). It relies on several features of the CLL. The user has to obtain the products/files to be found under the respective feature names:

* `Base`
* `Common`
* `DataSource`
* `HashMap`
* `Logging`
* `StatMeasure`
* `Variable`

== Installation

Since EPTF CLL Statistics Manager is used as a part of the TTCN-3 test environment this requires TTCN-3 Test Executor to be installed before any operation of these functions. For more details on the installation of TTCN-3 Test Executor see the relevant section of ‎<<_2, [2]>>.

If not otherwise noted in the respective sections, the following are needed to use `EPTF_CLL_StatManager`:

*	Copy the files listed in section <<descripton_of_files_in_this_feature, Description of Files in This Feature>> and <<description_of_required_files_from_other_features, Description of Required Files From Other Features>> to the directory of the test suite or create symbolic links to them.
*	Import the Statistics Manager demo or write your own application using `StatManager`.
*	Create _Makefile_ or modify the existing one. For more details see the relevant section of ‎‎<<_2, [2]>>.
*	Edit the config file according to your needs, see following section <<configuration, Configuration>>.

[[configuration]]
== Configuration

The executable test program behavior is determined via the run-time configuration file. This is a simple text file, which contains various sections. The usual suffix of configuration files is _.cfg._ For further information on the configuration file see <<_2, ‎[2]>>.

The Statistics Manager feature defines TTCN-3 module parameters as defined in <<_2, ‎[2]>>, clause 4. Actual values of these parameters – when there is no default value or a different from the default actual value to be used – shall be given in the `[MODULE_PARAMETERS]` section of the configuration file.

This feature does not contain any Statistics Manager–specific module parameters.

= Usage

To use the `StatManager` feature you have to extend `EPTF_StatManager_CT` and call the `f_EPTF_StatManager_init_CT` function.

= Warning Messages

NOTE: Besides the below described warning messages, warning messages shown in <<_2, ‎[2]>>  or those of other used features or product may also appear.

`*"<string> is not a legal float number!"*`

Limit value shall be specified as a legal float number.

`*Setting 'defaultColor' was unsuccessful! Using default color: black"*`

There were some errors in `defaultColor` parameter.

`*"<string> is not a legal LED color!"*`

Legal LED colors are: black, blue, red, yellow and green.

`*"Setting 'enableValueInLEDText' was unsuccessful! Setting it to false"*`

Legal parameter values are: `_yes_` or `_no_`.

`*"<string> is not a legal parameter value for: 'enableValueInLEDText'"*`

Legal parameter values are: `_yes_` or `_no_`.

`*"Wrong VarId parameter was given! No limit statistic will be returned!"*`

Incorrect `VarId` parameter was specified.

`*"No VarId parameter was given! No limit statistic will be returned!"*`

You must specify a source variable to retrieve the corresponding data.

`*"Wrong refVarId parameter was given! Setting to -1!"*`

The specified reference variable is incorrect. There will be no reference variables used.

`*"unhandled element: <string>"*`

Wrong data element name was given.

= Examples

The "demo" directory of the deliverable contains the following examples:

* _main.cfg_
* _EPTF_StatManager_demo.ttcn_

== Configuration File

The used configuration file (_main.cfg_) is for the Statistics Manager example is placed in the demo directory.

== Demo Module

The demo module __EPTF_StatManager_demo.ttcn__ illustrates a typical usage of the Statistics Manager feature. It is placed in the demo directory of the released feature.

= Terminoogy

*TitanSim Core (Load) Library(CLL):* +
It is that part of the TitanSim software that is totally project independent. (I.e., which is not protocol-, or application-dependent). The TitanSim CLL is to be supplied and supported by the TCC organization. Any TitanSim CLL development is to be funded centrally by Ericsson

*TitanSim Variables:* +
They contain values, from which TitanSim Statistics can be calculated automatically on changes or periodically.

*TitanSim Statistics Measure:* +
It is a feature, which is responsible for creating TitanSim Statistics and updating their value when required.

*TitanSim Statistics:* +
They are TTCN-3 variables. The values of such variables are automatically and periodically updated in the background.

= Abbreviations

CLL::	Core Load Library

EPTF::	Ericsson Load Test Framework, formerly TITAN Load Test Framework

TitanSim::	Ericsson Load Test Framework, formerly TITAN Load Test Framework

TTCN-3:: 	Testing and Test Control Notation version 3 ‎<<_1, [1]>>.

= References

[[_1]]
[1]	ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2]	User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3]	TitanSim CLL for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4]	TitanSim CLL  for TTCN-3 toolset with TITAN, User Guide

[[_5]]
[5]	EPTF CLL Statistics Manager Function Description

[[_6]]
[6]	TitanSim CLL  for TTCN-3 toolset with TITAN +
http://ttcn.ericsson.se/products/libraries.shtml[Reference Guide]

[[_7]]
[7]	EPTF CLL StatMeasure User Guide
