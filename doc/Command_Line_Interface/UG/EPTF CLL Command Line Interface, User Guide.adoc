---
Author: Balázs Lugossy
Version: 29/198 17-CNL 113 512, Rev. B
Date: 2012-12-01

---
= EPTF CLL Command Line Interface, User Guide
:author: Balázs Lugossy
:revnumber: 29/198 17-CNL 113 512, Rev. B
:revdate: 2012-12-01
:toc:

== How to Read This Document

This is the User Guide for the Command Line Interface of the Ericsson Performance Test Framework (TitanSim), Core Load Library (CLL). TitanSim CLL is developed for the TTCN-3 <<_1, ‎[1]>> Toolset with TITAN ‎<<_2, [2]>>. This document should be read together with the Function Description of the Command Line Interface feature <<_5, ‎[5]>>. For more information on the TitanSim CLL, consult the User's Guide <<_4, ‎[4]>> and the Function Specification <<_3, [3]>> of the TitanSim.

== System Requirements

In order to use the Command Line Interface feature the system requirements listed in TitanSim CLL User's Guide <<_4, ‎[4]>> should be fulfiled.

= The Command Line Interface

== Overview

The EPTF CLI implements the functionality to be able to control TTCN3 code via a telnet interface by issuing commands on the connected terminal.

[[description_of_files_in_this_feature]]
== Description of Files in This Feature

The EPTF CLL CLI includes the following files:

*	__EPTF_CLL_CLI_Definitions.ttcn__ - This TTCN-3 module contains common type definitions used by the CLI feature.
*	__EPTF_CLL_CLI_Functions.ttcn__ - This TTCN-3 module contains the implementation of CLI functions.

[[description_of_required_files_in_other_CLL_features]]
== Description of Required Files from Other CLL Features

The EPTF CLI feature depends on the following features of EPTF:

*	__EPTF_CLL_Base_Definitions__
*	__EPTF_CLL_Variable_Definitions__
*	__EPTF_CLL_Semaphore_Definitions__
*	__EPTF_CLL_Common_Definitions__
*	__EPTF_CLL_Logging_Definitions__

== Description of Required External Features Outside CLL

*	`TCCUsefulFunctions_CNL113472`
*	`TELNETasp_CNL113320` test port

== Installation

Since `EPTF_CLL_CLI` is used as a part of the TTCN-3 test environment this requires TTCN-3 Test Executor to be installed before any operation of these functions. For more details on the installation of TTCN-3 Test Executor see the relevant section of <<_2, ‎[2]>>‎.

If not otherwise noted in the respective sections, the following are needed to use `EPTF_CLL_CLI`:

*	Copy the files listed in section <<description_of_files_in_this_feature, Description of Files in This Feature>> and <<description_of_required_files_in_other_CLL_features, Description of Required Files from Other CLL Features>> to the directory of the test suite or create symbolic links to them.
*	Import the CLI demo or write your own application using EPTF CLI.
*	Create _Makefile_ or modify the existing one. For more details see the relevant section of ‎<<_2, [2]>>.
*	Edit the config file according to your needs, see following section <<configuration, Configuration>>.

[[configuration]]
== Configuration

The executable test program behavior is determined via the run-time configuration file. This is a simple text file, which contains various sections. The usual suffix of configuration files is _.cfg_. For further information on the configuration file see ‎<<_2, [2]>>.

The CLI feature defines TTCN-3 module parameters as defined in <<_2, ‎[2]>>, clause 4. Actual values of these parameters – when no default value or a different from the default actual value wished to be used – shall be given in the `[MODULE_PARAMETERS]` section of the configuration file.

The EPTF CLI feature uses the Telnet test port. This test port requires test port parameters to configure it. See the Telnet test port user guide <<_7, ‎[7]>> for more details.

NOTE: The `CTRL_MODE` parameter of the telnet test ports used by the CLI should be set to ‘server’ to be able to connect to them via a telnet client.

The EPTF CLI feature defines the following module parameters:

=== `tsp_EPTF_CLL_CLI_ShowWelcome`

This is a boolean parameter which can be used to enable a welcome message when connected to the CLI terminals.

Default value: `_false_` (disabled).

=== `tsp_EPTF_CLI_Functions_debug`

Module parameter to switch on debug logging in CLI.

Default: `_false_` (disabled).

=== `tsp_EPTF_CLI_Client_Functions_debug`

Module parameter to switch on debug logging in `CLI_Client`.

Default: `_false_` (disabled).

= Usage

== Using the Functional Interface

To be able to use the CLI feature at least one component in the system has to extend the `EPTF_CLI_CT` component. This component creates the telnet ports for the normal and the display terminal. In addition to the CLI component the CLI command can be registered in a component that extends the `EPTF_CLI_Client_CT` component.

=== Initialization

Before using the functions of these components they have to be initialized by the init functions of the corresponding CLI components.

To be able to use the command line interface terminal in case of error, it is recommended to place the CLI component to the MTC since this component terminates last.

=== Registering a Command

New command can be registered for a given terminal (i.e. CLI component) from CLI Clients (see ‎[6]). The command names has to be unique on the CLI used and should not contain spaces.

The help message given during registration should be a short description of the command because this help string will be printed when the `"help <command>"` is issued on the terminal. More detailed help can be printed by implementing the handling of the "help" argument for the command. In this case the detailed help is returned by executing `"<command> help"` on the terminal.

If the CLI Client init function is called with CLI component set to `_"null"_` and no CLI was given in the register function, or the CLI Client init is not called at all or cleanup has been called, it is not possible to register commands, the CLI is disabled.

=== The Help of a Command

Detailed help about a command can be printed by `"<command> help"` if the handler function of the command handles the "help" as argument.

=== The Command Handler

The command handler function registered for a command can modify the parameter containing the result of the command. This result will be printed on the terminal on which the command was executed. If the result is not changed, `"<command> done."` will be printed. Also the function has to return an error code. This error code notifies the CLI about the success of the command. If the command was handled successfully the function should return `_zero_`, otherwise a positive number. Positive numbers are treated as erroneous exit codes and will appear on the CLI terminal automatically.

If the handler function of a command has to wait for a message on a test port, it is not recommended to call altsteps that contain test port receive statements in the handler function. Instead the Semaphore feature of the CLL can be used to block the execution until the response arrives. When the response is handled in a default altstep it should also unlock the semaphore. This has to be done to avoid deadlocks on the test port.

=== Sending Messages to the Display Terminal

If messages should be sent to the terminal periodically, there is a display terminal provided by the CLI. Messages to the display terminal can be sent from the CLI Client. The display terminal can be used to prevent flooding the normal terminal. It is possible to enter the same commands on the display terminal as on the normal terminal.

== Using the Terminals

The CLI provides two terminals: a normal terminal, and a display terminal to show periodic printouts. The terminals can be accessed by a telnet client on the ports configured. Commands defined in the CLI can be executed on these terminals.

If the execution of the commands take some time, it is possible that the result of a command issued later will appear before the results of commands issued earlier are printed on the terminal.

=== CLI Built-In Commands

The CLI defines built-in commands to get basic help, exit from the terminal and stop the test execution. The built-in commands cannot be redefined i.e. to register commands with the same names on the CLI client.

==== The `help` Command

The `help` command can be used to print a general help message and list all available commands. To get help about a specific command, the name of the command should be passed to the help command: `'help <command>'`.

The `help` command will show the basic help message specified in the command registration function. To get more detailed help, the command handle should handle an argument like "help" and print out a more detailed information about the usage of the command.

==== The `exit` and `quit` Commands

The `exit` and `quit` commands can be used to close the terminal session. The test is not stopped. It is possible to reconnect the terminal later.

The `quit` command name is not case sensitive.

==== The `stop` and `stopAll` Commands

The `stop` and `stopAll` commands stop the execution of the test case. All components (that uses the Base feature of the CLL) will be stopped and the test is terminated if the `stop` or `stopAll` commands are executed on the terminal.

The stop command name is not case sensitive.

==== The `alias` Command

To create an alias to a command the `alias` command can be used. There is an example for simple usage in the first line below. `help` command can be substituted with 'h' from then on. Command parts (ex. arguments) can also be aliased, but must be signed with special character tags ($) in case of usage. In the third line we have an alias both for `help` (h) and `stop` (s). As 's' is signed to be substituted before command execution, thus the line will retrieve help message of command stop.

NOTE: There is no need of encapsulating 'h' between '$' tags, because the first word of the command is auto substituted if it is an alias.

Example:

[source]
----
alias help h
alias stop s
h $s$
----

==== The `unalias` Command

`unalias` removes a registered alias.

Example:

`unalias h`

==== The `Isalias` Command

`Isalias` prints information about a registered alias.

Example:

`lsalias h`

==== The `allalias` Command

`allalias` lists all registered aliases in the system.

= Warning Messages

NOTE: Besides the below described warning messages, warning messages shown in ‎[2] or those of other used features or product may also appear.

`*WARNING: Invalid ASP received from port <telnetPort>. Ignored.*`

An unexpected. message arrived on the telnet port, and it is ignored. This warning is printed when two or more clients connect to the same terminal.

`*WARNING: Invalid message received on CLI telnet port. Ignored.*`

An unexpected message arrived on the telnet port. It is ignored.

`*WARNING: f_EPTF_CLI_Client_sendCommandDisplay: Cannot send text to display: command not found: <commandName>*`

The display message could not be shown. The command name does not registered, it has no display.

= Examples

== Example Configuration

The following shows example test port configuration settings to configure the Command Line Interface telnet ports:

[source]
----
*.EPTF_CLI_TELNET_PCO.CTRL_PORTNUM := "17100"
*.EPTF_CLI_TELNET_PCO.CTRL_LOGIN_SKIPPED := "yes"
*.EPTF_CLI_TELNET_PCO.CTRL_USERNAME_CLIENT := "ttcn"
*.EPTF_CLI_TELNET_PCO.CTRL_PASSWORD_CLIENT := "ttcn"
*.EPTF_CLI_TELNET_PCO.CTRL_SERVER_PROMPT := "TTCN> "
*.EPTF_CLI_TELNET_PCO.CTRL_MODE := "server"
Similar settings with different CTRL_PORTNUM value can be used to configure the display terminal:
*.EPTF_CLI_displayTELNET_PCO.CTRL_PORTNUM := "17101"
*.EPTF_CLI_displayTELNET_PCO.CTRL_LOGIN_SKIPPED := "yes"
*.EPTF_CLI_displayTELNET_PCO.CTRL_USERNAME_CLIENT := "ttcn"
*.EPTF_CLI_displayTELNET_PCO.CTRL_PASSWORD_CLIENT := "ttcn"
*.EPTF_CLI_displayTELNET_PCO.CTRL_SERVER_PROMPT := "TTCN> "
*.EPTF_CLI_displayTELNET_PCO.CTRL_MODE := "server"
----

If the TELNET test port is used to connect to these ports, the `CTRL_MODE` has to be set to `_"client"_`, CTRL_PORTNUM and `CTRL_SERVER_PROMPT` has to be exactly the same as given here and the `CTRL_DETECT_SERVER_DISCONNECTED` has to be set to `_"yes"_`.

== Demo Module

The demo module in the demo directory illustrates a typical usage of the EPTF CLI feature. It demonstrates how to register a command, how a command handler should work, how to use the display terminal etc.

= Terminology

*TitanSim Core (Load) Library(CLL):* +
It is that part of the TitanSim software that is totally project independent. (I.e., which is not protocol-, or application-dependent). The TitanSim CLL is to be supplied and supported by the TCC organization. Any TitanSim CLL development is to be funded centrally by Ericsson.

*Command Line Interface:* +
It is a user interface accessible via telnet protocol where the behavior of TitanSim can be changed using commands.

= Abbreviations

CLI::	Command Line Interface

CLL::	Core Load Library

EPTF::	Ericsson Performance Test Framework, formerly TITAN Load Test Framework

TitanSim::	Ericsson Performance Test Framework, formerly TITAN Load Test Framework

TTCN-3:: 	Testing and Test Control Notation version 3 <<_1, [1]>>.

= References

[[_1]]
[1]	ETSI ES 201 873-1 v4.1.2 (2009-07) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2]	User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3]	TitanSim CLL for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4]	TitanSim CLL for TTCN-3 toolset with TITAN, User Guide

[[_5]]
[5]	EPTF CLL Command Line Interface, Function Description

[[_6]]
[6]	TitanSim CLL  for TTCN-3 toolset with TITAN +
http://ttcn.ericsson.se/products/libraries.shtml[Reference Guide]

[[_7]]
[7]	Telnet Test Port for TTCN-3 Toolset with TITAN, User Guide
