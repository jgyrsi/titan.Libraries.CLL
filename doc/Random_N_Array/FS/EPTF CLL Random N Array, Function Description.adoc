---
Author: Bence Molnár
Version: 28/155 16-CNL 113 512, Rev. A
Date: 2009-02-20

---
= EPTF CLL Random N Array, Function Description
:author: Bence Molnár
:revnumber: 28/155 16-CNL 113 512, Rev. A
:revdate: 2009-02-20
:toc:

== How to Read This Document

This is the Function Description for the Random N Array of the Ericsson Performance Test Framework (TitanSim), Core Load Library (CLL). TitanSim CLL is developed for the TTCN-3 <<_1, ‎[1]>> Toolset with TITAN <<_2, ‎[2]>>.

== Scope

This document is to specify the content and functionality of the Random N Array feature of the TitanSim CLL.

== Recommended Way of Reading

The readers are supposed to get familiar with the concept and functionalities of TitanSim CLL ‎<<_3, [3]>>. They should get familiar with the list of acronyms and the glossary in the Terminology section.

= General Description

This document specifies the Random N Array feature of the TitanSim CLL.

The EPTF Random N Array feature makes it possible to

* Manage lists over 'record of' data structures easily using index-arithmetics
** Provide efficient iterators over the elements linked into the same list
** Provide a number of lists containing elements, each list representing a different state
* Choosing random elements
** Provide a method from choosing a random element from a list.
* Using an interface similar to FBQ architecture
** Provide a free and busy list similar to FBQ, called Random Free Busy Array (RFBA)
** Provide functions with similar interface as defined in FBQ for managing RFBA

The aim of the EPTF Random N Array feature is to provide dynamic memory allocation for the TTCN-3 language in an efficient way.

RNA contains N `IntegerList`. An `IntegerList` can represent a state (free, busy, invalid etc.). Each `IntegerList` stores a specified number of elements. There is an `IdxMap`, that contains Integer pairs (`IntegerLists`) for each elements where the first integer of the pairs points to the List where the element is stored and the second stores the index of the element inside that `IntegerList`.

The advantage of this structure is that an element can be selected randomly from the lists.

RFBA is a specific RNA. It supports two `IntegerLists` or two states a Free and a Busy state, that can contain elements. The advantage of RFBA is that it integrates well into the current code, since the FBQ has a similar interface. RNA on the other hand is more generic with its N states.

Figure below shows a simple RFBA:

image:images/RFBA.png[alt]

= Functional Interface

Apart from this description a cross-linked reference guide for the TitanSim CLL Functions can be reached for on-line reading ‎<<_4, [4]>>.

== Naming Conventions

All functions have the prefix `f_EPTF_RNA`.

== Public Functions

=== Initialization

Before using an EPTF Random N Array the

`f_EPTF_RNA_initRNA(pl_size,_ _pl_numOfLists, pl_queue)`

function should be called with the queue as the parameter. This initializes the RNA containing `pl_numOfLists` number of lists or states. List number 0 will contain `pl_size` elements.

The RFBA can be initialized with the function

`f_EPTF_RNA_initRFBA(pl_size,_ _pl_queue)`

It will have only two lists or states can be called Free and Busy.

=== Getting or Creating Slots

The function `f_EPTF_RNA_getRndOrCreateSlot(pl_listNum, pl_queue)` can be used to get an element from a random slot from a list specified by `pl_listNum`. The function resizes the list if there are no slots available there.

Function `f_EPTF_RNA_createSlots (pl_number, pl_listNum, pl_queue)` can be used to create several new slot items at once in the list specified by `pl_listNum`.

Function `f_EPTF_RNA_getRndSlot(pl_listNum, pl_queue)` can be used to get an element from a random slot from a list specified by `pl_listNum`. This function cannot resize the list, and returns with a negative index if there are no free slots.

The function `f_EPTF_RNA_getRndOrCreateFreeSlot( pl_queue)` can be used to get an element from a random slot from the free list in an RFBA. The function resizes the list if there are no slots available there.

Function `f_EPTF_RNA_createFreeSlots (pl_number, pl_queue)` can be used to create several new slot items at once in the free list in an RFBA.

Function `f_EPTF_RNA_getRndFreeSlot(pl_queue)` can be used to get an element from a random slot from the free list in an RFBA. This function cannot resize the list, and returns with a negative index if there are no free slots.

=== Moving a Slot

The following functions can be used to move a slot:

`f_EPTF_RNA_moveSlot(pl_elementIdx, pl_listTo, pl_queue)`

can be used to move an element to a list specified by the parameter `pl_listTo`.

`f_EPTF_FBQ_moveFromFreeToBusySlot(pl_elementIdx, pl_queue)` can be used to move an element from the free list into the busy list in an RFBA.

`f_EPTF_FBQ_moveFromBusyToFreeSlot(pl_elementIdx, pl_queue)` can be used to move an element from the busy list into the free list in an RFBA.

=== Checking the State of an Element

The following functions can be used to check if an element is in a given state (or list) in an RFBA:

`f_EPTF_RNA_itemIsBusy(pl_elementIdx, pl_queue)` returns true if the element is in the busy list,

`f_EPTF_RNA_itemIsFree(pl_elementIdx, pl_queue)` returns true if the element is in the free list.

=== Getting the Location of an Element

The function

`f_EPTF_RNA_getList(pl_elementIdx, pl_queue)`

can be used to get the index of the list where `pl_elementIdx` located.

=== Getting the Length of a List

The function `f_EPTF_RNA_getLengthOfList(pl_listNum, pl_queue)` can be used To get a length of the list specified by `pl_listNum.`

The functions `f_EPTF_RNA_getLengthOfBusyList(pl_queue)` and `f_EPTF_RNA_getLengthOfFreeList(pl_queue)` can be used to get the length of the busy and the free list respectively in an RFBA.

== Summary Table of All Public Functions for EPTF Random N Array

See summary of Random N Array functions in the table below:

[width="100%",cols="50%,50%",options="header",]
|==============================================================================================================
|Function name |Description
|`f_EPTF_RNA_initRNA` |Initializes a Random N Array
|`f_EPTF_RNA_getRndOrCreateSlot` |Returns an index to an existing or a newly created element
|`f_EPTF_RNA_createSlots` |Creates slots in a list
|`f_EPTF_RNA_getRndSlot` |Returns an element from a random slot
|`f_EPTF_RNA_moveSlot` |Moves a slot to an other list
|`f_EPTF_RNA_getLengthOfList` |Returns the length of a specified list
|`f_EPTF_RNA_getList` |Gets the location of an element
|`f_EPTF_RNA_initRFBA` |Initializes a Random Free Busy Array
|`f_EPTF_RNA_getRndOrCreateFreeSlot` |Returns an index to an existing or a newly created element in the free list
|`f_EPTF_RNA_createFreeSlots` |Creates slots in the free list
|`f_EPTF_RNA_getRndFreeSlot` |Returns an element from a random slot in the free list
|`f_EPTF_RNA_moveFromFreeToBusySlot` |Moves a slot from the free list to the busy list
|`f_EPTF_RNA_moveFromBusyToFreeSlot` |Moves a slot from the busy list to the free list
|`f_EPTF_RNA_getLengthOfFreeList` |Returns the length of the free list
|`f_EPTF_RNA_getLengthOfBusyList` |Returns the length of the busy list
|`f_EPTF_RNA_itemIsBusy` |Returns true if the item is in the busy list
|`f_EPTF_RNA_itemIsFree` |Returns true if the item is in the free list
|==============================================================================================================

= Terminology

*TitanSim Core (Load) Library(CLL):* +
It is that part of the TitanSim software that is totally project independent. (I.e., which is not protocol-, or application-dependent). The TitanSim CLL is to be supplied and supported by the TCC organization. Any TitanSim CLL development is to be funded centrally by Ericsson.

= Abbreviations

CLL:: Core Load Library

EPTF:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TitanSim:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TTCN-3:: Testing and Test Control Notation version 3 <<_1, [1]>>.

FBQ:: Free Busy Queue

RNA:: Random N Array

RFBA:: Random Free Busy Array

= References

[[_1]]
[1] ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2] User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3] TitanSim CLL for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4] TitanSim CLL for TTCN-3 toolset with TITAN +
http://ttcn.ericsson.se/products/libraries.shtml[Reference Guide]
