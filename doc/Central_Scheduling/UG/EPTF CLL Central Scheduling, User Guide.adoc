---
Author: Mihány Mikó
Version: 24/198 17-CNL 113 512, Rev. C
Date: 2011-09-06

---
= EPTF CLL Central Scheduling, User Guide
:author: Mihány Mikó
:revnumber: 24/198 17-CNL 113 512, Rev. C
:revdate: 2011-09-06
:toc:

== About This Document

=== How to Read This Document

This is the User Guide for the Central Scheduling of the Ericsson Performance Test Framework (TitanSim), Core Load Library (CLL). TitanSim CLL is developed for the TTCN-3 <<_1, [1]>> Toolset with TITAN <<_2, [2]>>. This document should be read together with the Function Description of the Central Scheduling feature <<_5, [5]>>. For more information on the TitanSim CLL consult the User's Guide <<_4, [4]>> and the Function Specification <<_3, [3]>> of the TitanSim.

== System Requirements

In order to use the Central Scheduling feature the system requirements listed in TitanSim CLL User's Guide <<_4, [4]>> should be fulfilled.

= Central Scheduling

== Overview

The EPTF Central Scheduling feature makes it possible to write load test cases using similar methodology than in function test. Load testing means executing several traffic cases in parallel.

Scheduling of the traffic case triggering is performed by the Central Scheduling Application Admin. The triggering is done by sending execution burst messages to load generators which will execute the selected traffic case(s). The load generator and the interface between the admin and the load generator must be implemented by the user.

[[description_of_files_in_this_feature]]
== Description of Files in This Feature

The EPTF CLL Central Scheduling API includes the following files:

* Central Scheduling
** __EPTF_CLL_CS_Definitions.ttcnpp__ - This TTCN-3 module contains common type definitions that should be used in all Central Scheduling Components.
** __EPTF_CLL_CS_Admin_Functions.ttcnpp__ - This TTCN-3 module contains the implementation of the Central Scheduling Application Admin Base functions.
** __EPTF_CLL_CS_LB_Functions.ttcnpp__ - This TTCN-3 module contains the implementation of the Load Balancing for Central Scheduling.
** __EPTF_CLL_CS_UIHandler_Definitions.ttcnpp__ - This module contains common type definitions that should be used if a GUI is needed for Central Scheduling.
** __EPTF_CLL_CS_UIHandler_Functions.ttcnpp__ - This module contains the implementation of the Central Scheduling with GUI support.

[[description_of_required_files_from_other_features]]
== Description of Required Files from Other Features

The Central Scheduling feature is part of the TitanSim EPTF Core Load Library (CLL). It relies on several features of the CLL. To use the Central Scheduling, the user has to obtain the respective files from the following features:

* `Common`
* `Base`
* `ExecCtrl`
* `LGenBase`
* `FreeBusyQueue`
* `UIHandler`
* `Variable`

== Installation

Since `EPTF_CLL_CentralScheduling` is used as a part of the TTCN-3 test environment this requires TTCN-3 Test Executor to be installed before any operation of these functions. For more details on the installation of TTCN-3 Test Executor see the relevant section of <<_2, [2]>>.

If not otherwise noted in the respective sections, the following are needed to use `EPTF_CLL_CentralScheduling`:

* Copy the files listed in sections <<description_of_files_in_this_feature, Description of Files in This Feature>> and <<description_of_required_files_from_other_features,  Description of Required Files from Other Features>> to the directory of the test suite or create symbolic links to them.
* Import the Central Scheduling demo or write your own application using Central Scheduling.
* Create _Makefile_ or modify the existing one. For more details see the relevant section of <<_2, [2]>>.
* Edit the config file according to your needs, see following section <<configuration, Configuration>>.

[[configuration]]
== Configuration

The executable test program behavior is determined via the run-time configuration file. This is a simple text file, which contains various sections. The usual suffix of configuration files is _.cfg_. For further information on the configuration file see <<_2, [2]>>.

This Central Scheduling feature defines TTCN-3 module parameters as defined in <<_2, [2]>>, clause 4. Actual values of these parameters – when no default value or a different from the default actual value wished to be used – shall be given in the `[MODULE_PARAMETERS]` section of the configuration file.

=== Module Parameters of the Central Scheduling Feature

* `tsp_EPTF_CS_LGenHostnameList`
+
This `EPTF_CharstringList` type module parameter is defined in module `EPTF_CLL_CS_Definitions`. It is used to define the Hostnames on which load generators are to be created.
+
Its default value is `_{}_`.

* `tsp_debug_EPTF_CLL_CS_LB_Functions`
+
This boolean type module parameter is defined in module `EPTF_CLL_CS_LB_Functions`. It can be used for debugging purposes.
+
Its default value is `_false_`.

* `tsp_debug_EPTF_CS_Admin_Functions`
+
This boolean type module parameter is defined in module `EPTF_CLL_CS_Admin_Functions`. It can be used for debugging purposes.
+
Its default value is `_false_`.

* `tsp_EPTF_CLL_CS_Admin_LgenStatusGuard`
+
This float type module parameter is defined in module `EPTF_CLL_CS_Admin_Functions`. It defines the maximum time a load generator can take to initialize and send its initial status message. After this time expires with no status received, the Admin stops with an error message.
+
The default value of this parameter is `_10.0_` for 10 seconds.

* `tsp_EPTF_CLL_CS_Admin_LGenCreationDelay`
+
This float type module parameter is defined in module `EPTF_CLL_CS_Admin_Functions`. It defines a delay between successive creation of load generators. Its default value is `_0.1_`.

=== Configuring Traffic Cases

Entity group for traffic cases can be created via the LGenBase module parameter `tsp_LGenBase_EntityGrpDeclarators` with the entity name `CS_Entity_Tc`<suffix> where <suffix> is defined by `v_EPTF_CS_EntityName_Suffix` in the user init function.

Traffic cases and scenarios can be configured via the following LGenBase module parameters:

* `tsp_LGenBase_TcMgmt_tcTypeDeclarators2`
* `tsp_LgenBase_TcMgmt_WeightedScenarioDeclarators`
* `tsp_LGenBase_Scenarios2Grps`

For details, see <<_7, [7]>>. For an example, see the demo in the demo directory.

=== Configuring Load Generators

The Central Scheduler Application Admin declares entities of type `CS_Entity_LGen<suffix>` where `<suffix>` is defined by calling the function `f_EPTF_CS_Admin_setEntityNameSuffix` in the user init function. These entities can be used to define the number of load generators used by a specific type of Central Scheduling. This can be done by creating an entity group of this entity type via the LGenBase module parameter `tsp_LGenBase_EntityGrpDeclarators`.

No scenario must be stared on the load generator entities. Load generator entities are handled automatically without a scenario and their entities and entity group are only needed to be able to split load generators between multiple Admins using the ExecCtrl feature.

=== DataSource function of Central Scheduling

See the natural documentation of the CentralScheduling feature.

= Error Messages

NOTE: Besides the below described error messages, error messages shown in <<_2, [2]>> or those of other used features or product may also appear.

`*f_EPTF_CS_Admin_declareTrafficCase: redeclaration of traffic case <traffic-case-name>*`

The traffic case with `<traffic-case-name>` was declared more than once in the user defined initialization of the Central Scheduling Application Admin.

`*f_EPTF_CS_Admin_declareTrafficCase: traffic cases should be declared in user defined init function.*`

The function `f_EPTF_CS_Admin_declareTrafficCase` was called after initialization and creation of the internal traffic case type/selector lookup table.

`*f_EPTF_CS_Admin_sanityCheck: missing initialization of mandatory function reference `<function-reference>'*`

A mandatory function reference was not initialized in the user defined initialization function.

`*f_EPTF_CS_Admin_initFSM: Error while declaring FSM events*`

The admin could not initialize the FSM.

`*f_EPTF_CS_handleLGenStatusMsg: problem reported by LGen during initialization*`

The load generator sent disabled status after its creation and start.

`*f_EPTF_CS_lookupBySenderAddress: component reference not found in LGen database*`

Invalid component reference was passed to `f_EPTF_CS_lookupBySenderAddress`.

`*f_EPTF_CS_Admin_buildTcTypeIdx2tcSelector: undefined traffic case <traffic-case-name> for entity type <entity-type>*`

The configuration file contains a traffic case that was not declared in the user initialization function of the Central Scheduling Application Admin.

`*f_EPTF_CS_Admin_scenarioCreated: No scenario should be started on LGen entities.*`

The configuration file tried to assign a scenario to the load generator entities.

`*f_EPTF_CS_Admin_createLGens: timeout when waiting for LGen status message*`

The created load generator did not send a status message within the status guard time.

= Warning Messages

NOTE: Besides the below described warning messages, warning messages shown in <<_2, [2]>> or those of other used features or product may also appear.

`*f_EPTF_LB_selectLoadGen: WARNING: no free LGens found.*`

The load balancing feature did not find a free load generator.

`*f_EPTF_CS_Admin_declareTrafficCase: WARNING: traffic case selector <selector> assigned to traffic case <traffic-case> is already used by <another-traffic-case>*`

The traffic case selector `<selector>` was assigned to more than one traffic case.

`*f_EPTF_CS_Admin_sanityCheck: WARNING: no traffic case was declared. Traffic case selectors will be auto-assigned.*`

No traffic case was declared from the user init function. Traffic cases will be assigned a selector automatically, in the order of definition in the configuration file.

`*WARNING: f_EPTF_CS_SelectAndTriggerLGen: no trigger message is buffered.*`

`f_EPTF_CS_SelectAndTriggerLGen` was called without traffic cases in the buffer.

`*WARNING: f_EPTF_CS_SelectAndTriggerLGen: no free LGen: saturation!!!*`

No free load generators are available to send the trigger to.

=== DataSource Warning Messages

`*f_EPTF_CS_DSProcessData: Invalid iterator or externalData or parameter:*`

This warning message is reported, when the DataSource functionality is used and something with the request was wrong. Wrong iterator or externalData or parameter(s) were used.

`*f_EPTF_CS_DSProcessData: Unhandled element:*`

This warning message is reported, when the DataSource functionality is used and the current data element is unknown for the feature.

`*f_EPTF_CS_getParams: Parameters are not correct:*`

This warning message is reported, when the DataSource functionality is used and the parameters of the current data element are not correct. For example the name of the parameter was incorrect.

`*f_EPTF_CS_getParams: Too few parameter is given:*`

This warning message is reported, when the DataSource functionality is used and the parameters of the current data element are less than required.

`*f_EPTF_CS_getParams: Too many parameters are given:*`

This warning message is reported, when the DataSource functionality is used and the parameters of the current data element are more than required.

= Examples

The "demo" directory of the deliverable contains the following examples:

* __Common/Common_Definitions.ttcn__
* __EPTF_CS_demo.cfg__
* __EPTF_CS_demo.prj__
* __LoadRegulator/LoadRegulatro_Definitions.ttcn__
* __LoadRegulator/LoadRegulatro_Functions.ttcn__
* _Main/MainModule.ttcn_
* __protocolA/A_defs.ttcn__
* __protocolA/CS_A_Definitions.ttcn__
* __protocolA/CS_A_Functions.ttcn__
* __protocolA/LGen_A_Functions.ttcn__
* __protocolB/B_defs.ttcn__
* __protocolB/CS_B_Definitions.ttcn__
* __protocolB/CS_B_Functions.ttcn__
* __protocolB/LGen_B_Functions.ttcn__

== _Makefile_

The demo uses the `HostAdmin` feature of the CLL which needs the _Makefile_ script demo/HostAdmin/__makefile_patch.sh__ to be executed after _Makefile_ generation.

= Terminology

*TitanSim Core (Load) Library(CLL):* +
It is that part of the TitanSim software that is totally project independent. (I.e., which is not protocol-, or application-dependent). The TitanSim CLL is to be supplied and supported by the TCC organization. Any TitanSim CLL development is to be funded centrally by Ericsson.


= Abbreviations

CLL:: Core Load Library

EPTF:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

GUI:: Graphics User Interface

TitanSim:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TTCN-3:: Testing and Test Control Notation version 3 <<_1, [1]>>.

= References

[[_1]]
[1] ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2] User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3] TitanSim CLL for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4] TitanSim CLL for TTCN-3 toolset with TITAN, User Guide

[[_5]]
[5] EPTF CLL Central Scheduling, Function Description

[[_6]]
[6] TitanSim CLL for TTCN-3 toolset with TITAN +
http://ttcn.ericsson.se/products/libraries.shtml[Reference Guide]

[[_7]]
[7] EPTF CLL LGenBase, User Guide
