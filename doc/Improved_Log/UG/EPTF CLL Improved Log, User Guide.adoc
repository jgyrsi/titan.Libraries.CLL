---
Author: Ildikó Váncsa
Version: 35/198 17-CNL 113 512, Rev. A
Date: 2012-09-10

---
= EPTF CLL Improved Log, User Guide
:author: Ildikó Váncsa
:revnumber: 35/198 17-CNL 113 512, Rev. A
:revdate: 2012-09-10
:toc:

== About This Document

=== How to Read This Document

This is the User Guide for the `ILog` feature of the Ericsson Performance Test Framework (TitanSim), Core Load Library (CLL). TitanSim CLL is developed for the TTCN-3 <<_1, [1]>> Toolset with TITAN <<_2, [2]>>. This document should be read together with the Function Description of the `ILog` feature <<_5, [5]>>. For more information on the TitanSim CLL, consult the Function Specification <<_4, [4]>> and the Users Guide <<_3, [3]>> of the TitanSim.

== System Requirements

In order to use the `ILog` feature the system requirements listed in TitanSim CLL User Guide <<_4, [4]>> should be fulfilled.

= `ILog`

== Overview

Logging every traffic case not only can result in unnecessarily huge log files, but it even makes logs less useable thus harder to find the cause of the failure. The EPTF CLL `ILog` feature makes it possible not to log successful traffic cases, only the failed ones.

[[descrip_files_this_feature]]
== Description of Files in This Feature

The EPTF CLL Improved Logging API includes the following files:

* ILogBase
** __EPTF_CLL_ILogBase_Definitions.ttcn__ - This TTCN-3 module contains common type definitions that should be used in all `ILog` components.
** __EPTF_CLL_ILogBase_Functions.ttcn__ - This TTCN-3 module contains the implementation of `ILog` functions.
* `ILogString`:
** __EPTF_CLL_ILogString_Definitions.ttcn__ - This TTCN-3 module contains the type definitions of the `ILogString` feature.
** __EPTF_CLL_ILogString_Functions.ttcn__ - This TTCN-3 module contains the function implementations of the `ILogString` feature.
* `ILog`:
** __EPTF_CLL_ILog_Definitions.ttcn__ - This TTCN-3 module contains the type definitions of the `ILog` feature.
** __EPTF_CLL_ILog_Functions.ttcn__ - This TTCN-3 module contains the function implementations of the `ILog` feature.


[[descr_req_files_other_feat]]
== Description of Required Files From Other Features

The `ILog` feature is part of the TitanSim EPTF Core Load Library (CLL). It relies on several features of the CLL. The user has to obtain the products/files to be found under the respective feature names:

* `Base`
* `Common`
* `FreeBusyQueue`
* `HashMap`
* `LGenBase`
* `NQueue`
* `TCCUsefulFunctions`

== Installation

Since EPTF CLL ILog is used as a part of the TTCN-3 test environment this requires TTCN-3 Test Executor to be installed before any operation of these functions. For more details on the installation of TTCN-3 Test Executor see the relevant section of <<_2, [2]>>.

If not otherwise noted in the respective sections, the following are needed to use `EPTF_CLL_ILog`:

* Copy the files listed in section <<descrip_files_this_feature, Description of Files in This Feature>> and <<descr_req_files_other_feat, Description of Required Files From Other Features>> to the directory of the test suite or create symbolic links to them.
* Import the Improved Logging demo or write your own application using `ILog`.
* Create _Makefile_ or modify the existing one. For more details see the relevant section of <<_2, [2]>>.
* Edit the config file according to your needs. See the following section <<configuration, Configuration>>.

[[configuration]]
== Configuration

The executable test program behavior is determined via the run-time configuration file. This is a simple text file, which contains various sections. The usual suffix of configuration files is _.cfg_. For further information on the configuration file see <<_2, [2]>>.

The `ILog` feature defines TTCN-3 module parameters as defined in <<_2, [2]>>, clause 4. Actual values of these parameters – when there is no default value or a different from the default actual value to be used – shall be given in the `[MODULE_PARAMETERS]` section of the configuration file.

This feature does not contain any `ILog`–specific module parameters.

= Usage

The ILog feature consists of three modules; each of them can be used independently.

== `ILogBase`

The `ILogBase` is the common layer. It is provides the base functionality and can be used by the user or by other modules as well.

In order to use the EPTF CLL `ILogBase` feature the user component should extend the `EPTF_ILogBase_CT` component.

This component should be initialized by the function `f_EPTF_ILogBase_init_CT` before other functions of `ILogBase` can be used.

In order to use this feature, the user has to create one or more `LogHeader` and a `LogItem` functions, which create the log based on the chains that contain the log items.

There has to be at least one user side database, which contains the log items. The record type, used for this database, depends on the user. The database can be registered in the `ILogBase` component by using the `f_EPTF_ILogBase_register_DB` function. This function returns with an integer value, which is the Id of that database. This Id has to be used, when a new element is added to a chain.

The chain can be created by using the `f_EPTF_ILogBase_newChain` function. This function also returns an integer value that is the Id of that chain. When a new log item is joined to a chain with the `f_EPTF_ILogBase_addToChains` function, the Ids returned by the previous functions have to be used. This function returns with the element index of the new log item. This index should be use to store the item in the user side database.

The chains can be logged out or deleted at the end of the operation.

== `ILog`

In the first step, shall be implemented the `ILog` feature in application libraries handling session-based protocols.

A supported possible use case can be the following:

* The application writes a detailed log, and a short summary. In case of successful message flow, the short summary is written down. In case of failed message flow, the short summary is written down (like a header), and then the detailed log.

* The test steps (provided by the application and the application libraries) and the transport layer always log into the detailed log.

* The test steps mentioned above can log into the short summary too. This logging can be turned on/off by test steps. This way the users can manage from the FSM, which steps to write into the short description.

Several test steps can be used to start a traffic flow in a scenario, and in another scenario they are inside a traffic flow. Only the business logic implemented in the Application level can decide when to start a new chain. Therefore, the chains can be started by an FSM test step and obviously logging out the chains can be initiated by an FSM test step too.

The user can log out both the detailed and the short chain in case of failed traffic with using the `f_EPTF_ILog_step_logSuccess` function, and only the short chain in case of successful traffic by using the `f_EPTF_ILog_step_logFailed` function. It is also possible to delete both chains, when the traffic is successfully ended; this functionality is implemented in the `f_EPTF_ILog_step_deleteLog` function.

This layer of the feature can be used on the application and the `AppLib` levels. All the functions are accessible that are provided by the `ILogBase` layer, which means that the user can register new databases, create new detailed and short chain pairs, add elements to the log chains and log out and delete the log chains by using only this upper layer.

== `ILogString`

The `ILogString` feature provides charstring logging functionality. It has a built-in database that has records, which consist of a charstring and a float value that contains a timestamp.

To use this feature, it has to be initialized with the `f_EPTF_ILogString_init_CT` function. The initialization creates the built-in database as well. The functionalities of this feature are the same as those ones provided by the `ILogBase`. The difference between these two layers is that in case of `ILogString`, the user has to store only the Ids of the chains that were created; everything else is administered by the feature. The log item and the log header functions are also provided by the `ILogString` layer, which means that these functions are not customizable by the user.

= Warning Messages

NOTE: Besides the below described warning messages, warning messages shown in <<_2, [2]>> or those of other used features or product may also appear.

The `ILog`, `ILogBase` and `ILogString` feature may log one or more of the following warning messages:

`*f_EPTF_ILog_newChains: There is unprocessed chain data! Please log or delete it previously!*`

`*f_EPTF_ILog_addToChains: No valid chain was given in the list, which is currently in use!*`

`*f_EPTF_ILog_step_logSuccess: No valid chain was given in the list, which is currently in use!*`

`*f_EPTF_ILog_step_logFailed: No valid chain was given in the list, which is currently in use!*`

`*f_EPTF_ILog_step_deleteLog: No valid chain was given in the list, which is currently in use!*`

`*f_EPTF_ILogBase_newChain: Chain id inconsistency!*`

`*f_EPTF_ILogBase_addToChains: Invalid database id: "<pl_dbId>"*`

`*f_EPTF_ILogBase_addToChains: No valid chain was given in the list, which is currently in use!*`

`*f_EPTF_ILogBase_addToChains: There is no detailed chain in use!*`

`*f_EPTF_ILogBase_getName: Invalid chain, id: "<pl_chainId>"*`

`*f_EPTF_ILogBase_deleteChain: Invalid chain, id: "<pl_chainId>"*`

`*f_EPTF_ILogBase_logChain: Invalid chain, id: "<pl_chainId>"*`

`*f_EPTF_ILogBase_logAll: No chain to log!*`

`*f_EPTF_ILogString_addToChains: Could not add the log item to any of the chains in the chain list! Save of the log item is failed!*`

= Examples

The "demo" directory of the deliverable contains the following examples:

* __EPTF_ILog_demo.cfg__
* __EPTF_ILog_demo.ttcn__

== Configuration file

The used configuration file (__EPTF_ILog_demo.cfg__) is for the `ILog` example, it is placed in the demo directory.

== Demo Module

The demo module __EPTF_ILog_demo.ttcn__ illustrates a typical usage of the `ILog` feature. It is placed in the demo directory of the released feature.

= Terminology

*TitanSim Core (Load) Library(CLL):* +
It is that part of the TitanSim software that is totally project independent. (I.e., which is not protocol-, or application-dependent). The TitanSim CLL is to be supplied and supported by the TCC organization. Any TitanSim CLL development is to be funded centrally by Ericsson.

= Abbreviations

CLL:: Core Load Library

EPTF:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TitanSim:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TTCN-3:: Testing and Test Control Notation version 3 <<_1, [1]>>.

ILog:: Improved Log

= References

[[_1]]
[1] ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2] User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3] TitanSim CLL for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4] TitanSim CLL for TTCN-3 toolset with TITAN, User Guide

[[_5]]
[5] EPTF CLL Improved Logging Function Description

[[_6]]
[6] TitanSim CLL for TTCN-3 toolset with TITAN, +
http://ttcn.ericsson.se/products/libraries.shtml[Reference Guide]
