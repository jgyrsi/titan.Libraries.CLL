---
Author: Gábor Szalai
Version: 198 17-CNL 113 512, Rev. D
Date: 2009-02-09

---
= EPTF CLL for TTCN-3 toolset with TITAN, User Guide
:author: Gábor Szalai
:revnumber: 198 17-CNL 113 512, Rev. D
:revdate: 2009-02-09
:toc:

== How to Read This Document

This is Users Guide for the Ericsson Performance Test Framework (TitanSim), Core Load Library (CLL). TitanSim CLL is developed for the TTCN-3 Toolset with TITAN <<_2, ‎[2]>>. This document should be read together with the Functional Specification ‎<<_3, [3]>>.

== Scope

This document is the User Guide for the TitanSim Core Load Library (CLL). Under the collective term "TitanSim" we mean several products:

* "Turn-key applications", like "TitanSim for IMS I&V"
* Building blocks of such applications, that is, Application Libraries, such as SIP Application Library
* The application independent CLL.

The _technology_ under the hood of TitanSim has been called historically the _Ericsson Performance Test Framework_ (EPTF), hence the acronym "EPTF" in many of the identifiers of the API.

This document focuses on understanding the CLL. It contains the description of CLL features and references to their detailed User Guides. The demo applications are located in the product releases VOB along the CLL itself.

== Recommended Way of Reading

The readers are supposed to get familiar with the scope and goals of the project from <<_5, ‎[5]>> and also the glossary therein. The next step is to read the Quick Study ‎<<_6, [6]>> and its glossary. Finally they should get familiar with the list of acronyms and the glossary in Section ‎1.5 and ‎1.6 respectively.

= TitanSim Core Load Library (TitanSim CLL)

== Introduction

TitanSim CLL is a framework containing set of generic features and functions needed when implementing TTCN-3 traffic generators. It provides built-in functionality/libraries for:

* handling a generic, customizable graphical user interface
* infrastructure for handling of parameters and statistics along the test configuration (including the visualization of parameters/statistics and functions/altsteps for handling user interactions)
* ready-to-use functions and methods for effective handling of:
** Pool/queue variables
** Scheduling (mass timer infrastructure)
* customizable, ready-to-use TTCN-3 components (and related functions/altsteps) covering common scenarios, like execution control, load regulation, central scheduling, etc.
* API to extend the library with project-specific features

=== Detailed Feature Documentation

The CLL features are shortly explained further in this section. The detailed documentation for each feature can be found under the delivered directory _EPTF_Core_Library_CNL113512/doc_ with the following naming convention: __EPTF_CLL__<FeatureName>_FD_ for the Function Description and __EPTF_CLL__<FeatureName>_UG_ for the User Guide, respectively.

== `Base`

The EPTF Base component is a fundamental component that handles common tasks (e.g. managing cleanup functions, graceful termination, etc.) that should be implemented in all EPTF components. All EPTF components should extend this component (i.e. explicitly) or a component that extends this component (i.e. implicitly).

See ‎<<_7, ‎[7]>> for more details.

== `Central Scheduling`

The EPTF Central Scheduling feature makes it possible to write load test cases using similar methodology than in function test. Load testing means executing several traffic cases in parallel.

See ‎<<_30, [30]>> for more details.

== `Common`

The EPTF Common feature contains common type and function definitions used by other EPTF features.

See ‎‎<<_8, [8]>> for more details.

== `Execution Control`

The aim of the EPTF ExecCtrl feature is to provide common framework for configuring and handling multiple Load Generators in one test scenario.

See ‎‎<<_9, [9]>> for more details.

== `FreeBusyQueue`

The EPTF Free Busy Queue feature provides dynamic memory allocation for the TTCN-3 language in an efficient way.

See ‎‎<<_10, [10]>>for more details.

== `HashMap`

The EPTF HashMap feature provides access to a well-tested, industry standard _GCC_ HashMap implementation. HashMap is a hashed associate container that associates object of type key with object of type data.

See ‎<<_11, [11]>> for more details.

== `HostAdmin`

The aim of the EPTF Host Admin feature is to provide processor and memory usage measurement for a given host it is started on. Remote measurements are also possible using the feature by subscribing to EPTF Variables (see ‎2.27).

See ‎<<_12, [12]>> for more details.

== `LGenBase`

The EPTF LGenBase feature is the basis for all load generators (LGens) components of TitanSim, i.e. this feature should be used to write efficient load generators.

See ‎<<_13, [13]>> for more details.

== `LoadRegulator`

The EPTF Load Regulator feature makes it possible to regulate the CPS of a load generator based on the load of the SUT.

See ‎‎<<_14, [14]>>for more details.

== `Logging`

The aim of the EPTF Logging feature is, to use the library provided logging framework for event-class based per-PTC log control. The feature manages the logging database, defines default log classes and functions without the extension of GUI.

See ‎<<_15, [15]>> for more details.

== `NameService`

The EPTF Name Service feature makes it possible to register and query component references by name.

See ‎<<_17, [17]>> for more details.

== `PTCDeployment`

The EPTF `PTCDeployment` implements the functionality that allows components to select a host, where new PTCs can be deployed according to specified tasks (called roles) in the PTCD component.

See ‎‎<<_18, [18] >>for more details.

== `RandomNArray`

The aim of the EPTF Random N Array feature is to provide dynamic memory allocation for the TTCN-3 language in an efficient way. The EPTF Random N Array feature makes it possible to manage lists over `record of' data structures easily using index-arithmetics.

See ‎<<_33, [33]>> for more details.

== `RedBlackTree`

The EPTF Red Black Tree feature is a self-balancing binary search tree. It can be used to store indexes into an associated array, similar to the EPTF Free Busy Queue (see ‎2.6).

See ‎‎<<_19, [19]>>for more details.

== `Rendezvous`

The aim of the EPTF Rendezvous feature is, to make a _Rendezvous service_. `Rendezvous` service provides a generic solution for synchronization among/between various entities either locally and remotely.

See ‎‎<<_29, [29]>>for more details.

== `RingBuffer`

The EPTF Ring Buffer feature provides a general ring buffer (or circular buffer) implementation. Ring buffer is a http://en.wikipedia.org/wiki/Data_structure[data structure] that uses a single, fixed-size http://en.wikipedia.org/wiki/Buffer_%28computer_science%29[buffer] as if it were connected end-to-end. This structure can be easily used for buffering http://en.wikipedia.org/wiki/Data_stream[data streams].

See ‎<<_20, [20]>> for more details.

== `Scheduler`

The EPTF `Scheduler` feature makes it possible to schedule events (actions) that should happen at a given time measured from the start of the component or relative to the schedule of the event. The Scheduler is efficient, it uses only two TTCN-3 timers to implement scheduling.

See ‎<<_21, [21]>> for more details.

== `Semaphore`

The EPTF Semaphore feature makes it possible to implement blocking functions and to perform synchronization in TTCN-3.

See ‎<<_22, [22] >>for more details.

== `StatCapture`

The aim of the EPTF Statistics Capture feature is to group Statistics into capture groups and to print capture group contents regularly at sampling intervals to predefined capture files.

See ‎<<_23, [23]>> for more details.

== `StatHandler`

StatHandler feature makes it possible to collect global, aggregated statistics via the existing EPTF Variable interface.

See ‎<<_32, [32]>> for more details.

== `StatMeasure`

The aim of the EPTF Statistics Measure feature is to create Statistics (stand-alone or belonging to a Variable) and update their value when required.

See ‎‎<<_24, [24]>>for more details.

== `StatReplay`

The aim of the EPTF Statistics Replay feature is to display a given Statistics from a given capture group on the chart in a slider window in the GUI.

See ‎<<_25, [25]>> for more details.

== `Time Profile Editor`

The EPTF CLL Time Profile Editor is an application which allows the user to create and edit time profiles used by the EPTF CLL Execution Control ‎[10] component.

See ‎<<_31, [31]>> for more details.

== `Transport`

The EPTF Transport Control feature makes it possible to

Route the incoming and outgoing messages between the components

Store messages and information in a FreeBusyQueue

Handle the UDP and IPL4 testports dynamically.

See ‎<<_28, [28]>> for more details.

== `UIHandler`

With the EPTF `UIHandler` feature, the user can manage display and manipulation of subscribed Variables on the runtime GUI. Apart from this, it is even possible to change the runtime GUI. The user can add, remove, enable or disable GUI elements.

See ‎<<_26, [26]>> for more details.

== `Variable`

The EPTF `Variable` feature makes it possible to access component variables in remote components and to create automatically calculated Variables triggered by the refresh of other Variables.

See ‎‎<<_27, [27] >>for more details.

= Installation and Configuration

Since the EPTF CLL is used as a part of the TTCN-3 test environment this requires TTCN-3 Test Executor to be installed before any operation of these functions. For more details on the installation of TTCN-3 Test Executor see the relevant section of ‎<<_2, ‎[2]>>.

If not otherwise noted in the respective sections, the following are needed to use the EPTF CLL features:

* Copy the files of the specific feature from the released _EPTF_Core_Library_CNL113512/src_ directory of the test suite or create symbolic links to them.
* Import the released demo application from _EPTF_Core_Library_CNL113512/demo_ or write your own application using the given CLL feature.
* Import required files from _EPTF_Core_Library_CNL113512/src_ from other EPTF CLL features as explained in the User Guide of the feature (see _EPTF_Core_Library_CNL113512/doc)._
* Create _Makefile_ or modify the existing one. For more details see the relevant section of the User Guide of the feature from _EPTF_Core_Library_CNL113512/doc._
* Edit the configuration file according to your needs, see the User Guide of the specific _EPTF_Core_Library_CNL113512/doc_ for configuration and module parameters.

Some of the features of the Core Library are using constants defined during compilation. These constants can be defined with the EPTF_DEBUG switch which can be set in the CPPFLAGS section of the _Makefile_. If this macro is defined the application will run in debug mode otherwise not.

= Terminology

*TitanSim Application Library:* +
It is that part of the TitanSim software that is (at-least partially) project dependent. (I.e., which is protocol-, and/or application-dependent). Any TitanSim application library is to be supplied and supported _either_ by the customer project _only_; or by the customer project _together_ with the TCC organization. Any TitanSim application library development is to be funded by the ordering projects.

*TitanSim Core (Load) Library(CLL):* +
It is that part of the TitanSim software that is totally project independent. (I.e., which is not protocol-, or application-dependent). The TitanSim CLL is to be supplied and supported by the TCC organization. Any TitanSim CLL development is to be funded centrally by Ericsson.

*Application Library (AppLib):* +
It is that part of the TitanSim software that is although protocol, or application-area dependent, but can be _reused_ across many TitanSim applications. `AppLibs` are dependent on the CLL.

*Control Logic:* +
It is that part of the TitanSim software that is specific to a particular TitanSim application program. It is usually not directly reusable and is built upon the CLL and several `AppLibs`.

*Entities:* +
They are "things/objects" that are simulated by the LGen. Such "things" are usually the "users", "calls", "servers", "terminations", or anything whose external behavior is to be simulated by the `LGen`. Usually an `LGen` simulates more than one entity.

*Entity context:* +
It is an index-able generic data record describing an entity. It serves as a hub connecting all information related to the entity together by referring the respective associated _behavior contexts_ and _FSM contexts_ via their indices. The entity contexts are identified via their indices with respect to the entity context database stored in the `v_LGenBase_entities` component variable.

*Behavior context:* +
It is an index-able (set of) data record(s) that stores data and state information of a given entity with respect to a given _behavior type._ Storage for behavior contexts is to be provided by the respective AppLibs.

*Behavior type:* +
It is a concept for collectively referring a set of _behavior contexts, FSM inputs, test steps,_ and functions realizing methods conforming to given function signatures prescribed by the `LGenBase` feature. Behavior types are to be declared dynamically during run-time to the `LGenBase` by the component-type initialization function of some `AppLib`. Usually an `AppLib` product has only one behavior type, but it is permitted for `AppLibs` to declare more than one behavior types, if necessary. Behavior types are identified by their indices determined by the order of declaration and the declarations are stored in `v_LGenBase_behaviorTypes`

*Entity type:* +
It is a named list of references to declared _behavior types_. As such, it declares what type of behavior contexts are available on entities of this type.

*Entity group:* +
A group of entities with homogenous entity type. The indices of the entities of the group make a continuous interval that does not overlap with the indices range of any other entity group.

*Test step:* +
Test steps are functions whose signature conforms to a given function type. They _must_ not contain TTCN-3 statement with blocking semantics.

*FSM table:* +
It is a formal specification of the communication rules of Finite State Machines. These indexable records are stored in `v_LGenBase_fsmTables`. FSM tables realizing _traffic cases_ must obey certain rules.

*Traffic case type:* +
It is a prototype used as an input for instantiating traffic case instances on a specific entity group. It defines what _FSM table_ to use, what _entity type_ must be used by the _entity group_ where the _traffic case_ is to be deployed

*Traffic case:* +
It is a behavior, defined by an FSM table, that is executed on a given entity group. All entities of the entity group participate in executing the the traffic case. An entity group may have more than one concurrent traffic cases.

*Scenario:* +
It is a collection of traffic cases deployed on an entity group.

*TitanSim Variables:* +
They are enhanced component variables that make it possible to access the values of other Variables in remote components.

*TitanSim Statistics:* +
They are Statistics of TitanSim Variables. Possible Statistics are among others minimum, maximum, mean, standard deviation, etc. and also the content of a Variable can be a Statistics itself. The values of such Statistics are automatically and periodically updated in the background.

*Generated load of a traffic case:* +
It is defined as the _aggregate_ load generated by _traffic case instances_ of the given _traffic case_

*Generated load of the traffic mix:* +
It is defined as the _aggregate_ load generated by the individual _traffic cases_ of the given _traffic mix_

*Programming style, event driven:* +
A programming style not-so ``natural'' for most people. It is atypical for function tests. It focuses on one or more threads of execution, however, _not_ in a sequential manner. Rather it defines states for execution where the program code wait for an event (any event) to happen, then based on the event it executes a state transition to a new state and continues to wait for another event. The flow of events _cannot be_ understood easily just by reading the source code sequentially, therefore it is less natural for many programmer.

*Programming style, procedural:* +
It is typical for functions tests. It focuses on a single thread of execution in a sequential manner. The "natural" programming style for most programmers, since the flow of events can be understood easily just by reading the source code sequentially.

*Traffic case instance:* +
It is a specific execution instance of the behavior (message sequence) defined by the _traffic case_

*Traffic generator PTC:* +
It is a PTC that actually generate the messages toward the SUT during the performance test campaign

*Traffic group:* +
It is a named instance of a _traffic mix_. Such traffic group may have _ TitanSim Parameters_ that are common to each traffic cases within the group that realize the _traffic mix_. Such traffic group may have (potentially aggregate) _ TitanSim Statistics_ that provides (potentially aggregate) information about the execution of the _ traffic cases_ within the group. Note, however, that the _ traffic cases_ may have individual parameters and statistics, as well.

*Traffic mix:* +
It is a specific combination of _traffic cases_ that are to be executed concurrently by TitanSim. Each traffic case has a corresponding _traffic weight assigned._ The traffic mix has a load-level target that is an aggregate of the individual load-levels of the individual _traffic cases_, whose load levels are the result of the execution of their _traffic case instances_

= Abbreviations

API:: Application Programming Interface

EPTF:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

FOA:: First Office Application

IUT:: Implementation Under Test

LGen:: Load Generator PTC

PTC:: Parallel Test Component

SUT:: System Under Test

TLTF:: See EPTF

= References

See Section ‎2.1.1 for the location of feature documentations in the delivery VOB.

[[_1]]
[1] ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2] User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3] TitanSim CLL for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4] TitanSim CLL for TTCN-3 toolset with TITAN +
http://ttcn.ericsson.se/products/libraries.shtml[Reference Guide]

[[_5]]
[5] Project Specification for TitanSim R2

[[_6]]
[6] Quick Study on TitanSim R2 development

[[_7]]
[7] EPTF CLL Base, User Guide

[[_8]]
[8] EPTF CLL Common, User Guide

[[_9]]
[9] EPTF CLL ExecCtrl, User Guide

[[_10]]
[10] EPTF CLL Free Busy Queue, User Guide

[[_11]]
[11] EPTF CLL HashMap, User Guide

[[_12]]
[12] EPTF HostAdmin, User Guide

[[_13]]
[13] EPTF CLL LGenBase, User Guide

[[_14]]
[14] EPTF CLL LoadRegulator, User Guide

[[_15]]
[15] EPTF CLL Logging, User Guide

[[_16]]
[16] EPTF CLL LoggingUI, User Guide

[[_17]]
[17] EPTF CLL NameService, User Guide

[[_18]]
[18] EPTF CLL PTCDeployment, User Guide

[[_19]]
[19] EPTF CLL RedBlackTree, User Guide

[[_20]]
[20] EPTF CLL RingBuffer, User Guide

[[_21]]
[21] EPTF CLL Scheduler, User Guide

[[_22]]
[22] EPTF CLL Semaphore, User Guide

[[_23]]
[23] EPTF CLL StatCapture, User Guide

[[_24]]
[24] EPTF CLL StatMeasure, User Guide

[[_25]]
[25] EPTF CLL StatReplay, User Guide

[[_26]]
[26] EPTF CLL UIHandler, User Guide

[[_27]]
[27] EPTF CLL Variable, User Guide

[[_28]]
[28] EPTF CLL Transport, User Guide

[[_29]]
[29] EPTF CLL Rendezvous, User Guide

[[_30]]
[30] EPTF CLL Central Scheduling, User Guide

[[_31]]
[31] EPTF CLL Time Profile Editor, User Guide

[[_32]]
[32] EPTF CLL Stat Handler, User Guide

[[_33]]
[33] EPTF CLL Random N Array, User Guide
