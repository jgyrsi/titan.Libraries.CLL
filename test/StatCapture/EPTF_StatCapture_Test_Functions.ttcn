///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_StatCapture_Test_Testcases
// 
//  Purpose:
//    This module contains a test application for the Statistics Capture Control feature.
//
/////////////////////////////////////////////////////////////// 
module EPTF_StatCapture_Test_Functions { 

import from EPTF_CLL_HashMapInt2Int_Functions all;
import from EPTF_CLL_Variable_Definitions all;
import from EPTF_CLL_Variable_Functions all;
import from EPTF_CLL_StatMeasure_Definitions all;
import from EPTF_CLL_StatMeasure_Functions all;
import from EPTF_CLL_Scheduler_Definitions all;
import from EPTF_CLL_RBTScheduler_Functions all;
import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_StatCapture_Definitions all;
import from EPTF_CLL_StatCapture_Functions all;
import from TCCFileIO_Functions all;
import from EPTF_CLL_Logging_Functions all;
import from EPTF_CLL_Common_Definitions all;
import from EPTF_StatCapture_Test_Definitions all;

modulepar boolean tsp_EPTF_StatCapture_Test_debug :=true;
///////////////////////////////////////////////////////////
//  Function: f_StatCapture_Test_init
//
//  Purpose: initializes component EPTF_StatCapture_Test_CT
//     
//
//  Detailed Comments: -
// 
///////////////////////////////////////////////////////////
function f_StatCapture_Test_init(in charstring pl_selfName) runs on EPTF_StatCapture_Test_CT {

  if (v_StatCapture_Test_initialized) {
    return; // already initialized
  }

  f_EPTF_StatCapture_init(pl_selfName); 
  f_EPTF_Logging_init_CT(pl_selfName);
  v_StatCapture_Test_initialized := true;
  v_StatCapture_Test_captureGroups := {};
  v_StatCapture_Test_selfName := pl_selfName;
  //v_StatCapture_Test_def := //activate(as_EPTF_SchedulerComp_ActionHandler());
  f_EPTF_Base_registerCleanup(refers(f_EPTF_StatCapture_Test_cleanup));
  log("----StatCapture INIT DONE----");
}

function f_EPTF_StatCapture_Test_cleanup() runs on EPTF_StatCapture_Test_CT {
  v_StatCapture_Test_initialized:=false;
  v_StatCapture_Test_selfName:="";
  //hack cleanup for hashtable:
  var integer vl_id:=-1;
  if(f_EPTF_int2int_HashMap_GetID("fd2nameHashmap",vl_id)) {
    f_EPTF_int2int_HashMap_Delete("fd2nameHashmap");
    f_EPTF_Logging_warning(true, "Hash Map deleted by test program but it should be deleted by EPTF_StatCapture_CT.It shall be fixed!!!")
  };
}


function f_EPTF_StatCaptureClient_Test_cleanup() runs on EPTF_StatCaptureClient_Test_CT {
  v_StatCapture_Test_initialized:=false;
  v_StatCapture_Test_selfName:="";
  //hack cleanup for hashtable:
  var integer vl_id:=-1;
  if(f_EPTF_int2int_HashMap_GetID("fd2nameHashmap",vl_id)) {
    f_EPTF_int2int_HashMap_Delete("fd2nameHashmap");
    f_EPTF_Logging_warning(true, "Hash Map deleted by test program but it should be deleted by EPTF_StatCapture_CT.It shall be fixed!!!")
  };
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_StatCapture_Test_setVerdict
//
//  Purpose:  To set verdict according to the input arg
//     
//
//  Detailed Comments: 
// 
///////////////////////////////////////////////////////////
function f_EPTF_StatCapture_Test_setVerdict(in boolean pl_bool) {
  if(pl_bool) { setverdict(pass) } else { setverdict(fail) } 
}
///////////////////////////////////////////////////////////
//  Function: f_StatCapture_Test_checkInit
//
//  Purpose:  checks if f_EPTF_StatCapture_init() works correctly
//     
//
//  Detailed Comments: -
// 
///////////////////////////////////////////////////////////
function f_StatCapture_Test_checkInit(in charstring pl_selfName) runs on EPTF_StatCapture_Test_CT return boolean {
  var boolean vl_retval := true;
// FIXME: USES PRIVATE MEMBERS:
//   if( not( v_StatCapture_initialized and v_EPTF_StatMeasure_initialized and v_Scheduler_initialized))
//   {
//     log(v_StatCapture_initialized, v_EPTF_StatMeasure_initialized, v_Scheduler_initialized);
//     vl_retval:= false;
//   }
//   if(v_StatCapture_captureGroups!={} ){
//     log(v_StatCapture_captureGroups);
//     vl_retval:= false;
//   }
//   if( v_StatCapture_FileDataList!={}) {
//     log(v_StatCapture_FileDataList)
//     vl_retval:= false;
//   }
//   if( v_StatCapture_eventHandler==null ){
//     log(v_StatCapture_eventHandler);
//     vl_retval:= false;
//   }
//   if(v_StatCapture_inthashmap_id <0) {
//     log(v_StatCapture_inthashmap_id);
//     vl_retval:=false
//   }  
//   if( v_StatCapture_logfilenames == {}) {
//     log(v_StatCapture_logfilenames); //at least the default file name is stored
//     vl_retval:=false;
//   }
//   if( v_StatCapture_resetStatList!= {} ){
//     log(v_StatCapture_resetStatList);
//     vl_retval:= false;
//   }
  if( f_EPTF_Base_selfName()!= pl_selfName) {
    log(f_EPTF_Base_selfName());
    vl_retval:= false;
  } 
  //"files closed" hasn't tested yet 
  return vl_retval;
}//f_
///////////////////////////////////////////////////////////
//  Function: f_StatCapture_Test_checkCleanup
//
//  Purpose:  checks if f_EPTF_StatCapture_cleanup() works correctly
//     
//
//  Detailed Comments: -
// 
///////////////////////////////////////////////////////////
function f_StatCapture_Test_checkCleanup(in charstring pl_selfName) runs on EPTF_StatCapture_Test_CT return boolean {
  var boolean vl_retval := true;
// FIXME: USES PRIVATE data:
//   if(v_StatCapture_initialized)
//   {
//     log(v_StatCapture_initialized);
//     vl_retval:= false;
//   }
  var integer vl_id;
  if(f_EPTF_int2int_HashMap_GetID("fd2nameHashmap", vl_id)==true) {
    vl_retval := false;
    log("fd2nameHashmap hashmap not deleted");
  }
  return vl_retval;
}//f_

///////////////////////////////////////////////////////////
//  Function: f_EPTF_StatCapture_Test_genarateStat
//
//  Purpose:  generate a very special stat by adjusting pl_var1ID and pl_var2ID
//     
//
//  Detailed Comments:
//   1. Starts the capture by f_EPTF_StatCapture_startCaptureAll
//   2. Generate statistics by calling f_EPTF_Var_adjustContent for pl_var1ID and pl_var2ID
//   3. Stops the statistics by f_EPTF_StatCapture_stopCaptureAll
//
///////////////////////////////////////////////////////////
function f_EPTF_StatCapture_Test_genarateStat(in integer pl_var1ID, in integer pl_var2ID, in float pl_execTime:=10.0, in float pl_adjTime := 1.0) 
runs on EPTF_StatCapture_Test_CT 
{

  timer t_wait := pl_execTime;
  t_wait.start;

  timer t_adjust := pl_adjTime;
  t_adjust.start;

  // add initial value to the stats:
  f_EPTF_Var_refreshContent(pl_var1ID);
  f_EPTF_Var_refreshContent(pl_var2ID);

  timer t_1:=0.2; t_1.start; t_1.timeout;
  f_EPTF_StatCapture_startCaptureAll();

  var integer vl_now := 1;

  alt {
    [] t_adjust.timeout{
        //log(">>>>Timeout at t_wait: ", t_wait.read);
        //log(">>>>=============Stat vars before adjust:==========",statList);
        f_EPTF_Var_adjustContent(pl_var1ID, {intVal:= vl_now});
        f_EPTF_Var_adjustContent(pl_var2ID, {floatVal:= int2float(vl_now)}); vl_now := vl_now * 2; t_adjust.start; 
        //log(">>>>============Stat vars after adjust:========= ",statList);
        repeat;
      }
    [] t_wait.timeout {}
  }
  f_EPTF_StatCapture_stopCaptureAll();
}//f_


///////////////////////////////////////////////////////////
//  Function: f_EPTF_StatCaptureStat_Test_readFiles
//
//  Purpose:   To read all log files into one string
//     
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////
function f_EPTF_StatCaptureStat_Test_readFiles( 
  in integer pl_fileIdx, out charstring pl_fileContent)
runs on EPTF_StatCapture_Test_CT 
return boolean
{  
  var charstring vl_fileName :="";
  var integer vl_retval:=-1;
  var integer vl_suffix :=0;
  var charstring vl_strSuffix:="";
  var charstring vl_actContent:="";
  pl_fileContent := ""
  while(true) {
    vl_suffix:=vl_suffix+1;
    if(vl_suffix>1) { vl_strSuffix:= "-" & int2str(vl_suffix) }
    if( f_EPTF_StatCapture_getFileName(pl_fileIdx, vl_strSuffix, vl_fileName)==false) {
      return false;
    }
    action("f_EPTF_StatCaptureStat_Test_readFiles >> ",vl_fileName);
    var integer vl_fd:= f_FIO_open_rdonly(vl_fileName)

    if(vl_fd==-1){
      f_EPTF_Logging_debug(tsp_EPTF_StatCapture_Test_debug, "Couldn't open the file "&vl_fileName&".");//perhaps 
      return true;
    }

    var integer vl_from:=f_FIO_seek_home(vl_fd);
    var integer vl_to:=f_FIO_seek_end(vl_fd);
    log("DEBUG>>>from: ", vl_from, " To: ",vl_to);
    log("Size: ",  vl_to-vl_from);
    if( f_FIO_seek_home(vl_fd)==-1  or vl_to==-1) { 
      return false;
    }

    vl_retval:=f_FIO_read_text(vl_fd, vl_actContent, vl_to-vl_from)
      pl_fileContent:=pl_fileContent & vl_actContent;
    if(-1 == vl_retval){
      f_FIO_close(vl_fd);
      f_EPTF_Logging_error(true, "Couldn't read the file "&vl_fileName&".");
      return false;
    }

    if( f_FIO_close(vl_fd)==-1) {
      f_EPTF_Logging_error(true, "Could not close the file!");
      return false;
    }
    log("Content now: ", pl_fileContent);
  }//while
  return true;
}//f_

///////////////////////////////////////////////////////////
//  Function: f_EPTF_StatCatureStat_Test_split
//
//  Purpose:  To split a string into two or more chunks at a given delimiter
//     
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////
function f_EPTF_StatCapture_Test_split(in charstring pl_strToSplit, in charstring pl_delimiter) return EPTF_CharstringList
{
  var EPTF_CharstringList vl_outputStringList;
  var integer vl_size := lengthof(pl_strToSplit);
  var integer vl_delSize:= lengthof(pl_delimiter);
  var integer vl_listIndex :=0;
  var integer vl_startPos:=0;
  var integer i:=0;

  while( i<vl_size ) {
    if( substr(pl_strToSplit,i,vl_delSize)==pl_delimiter ) {
      vl_outputStringList[vl_listIndex]:= substr(pl_strToSplit,vl_startPos,i-vl_startPos);
      vl_listIndex:=vl_listIndex+1;
      i:=i+vl_delSize;
      vl_startPos:=i;
    } else {
      i:=i+1;
    }
  }

  //Last chunk:
  if(vl_startPos<vl_size) {
    vl_outputStringList[vl_listIndex]:= substr(pl_strToSplit,vl_startPos, vl_size-vl_startPos)
  }
  return vl_outputStringList;
}//f_


///////////////////////////////////////////////////////////
//  Function: f_EPTF_StatCapture_Test_getConditionalHashmark
//
//  Purpose: To privide different beginning for the different file type header lines
//     
//
//  Detailed Comments: for gnuplot it returns with"#", otherwise with empty string
//
///////////////////////////////////////////////////////////
function f_EPTF_StatCapture_Test_getConditionalHashmark() 
return charstring {
  if(tsp_EPTF_StatCapture_fileFormat==gnuplot) {
    return "#"
  }else{ return ""}

}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_StatCapture_Test_checkFirstLine
//
//  Purpose: checks the first line of the log file
//     
//
//  Detailed Comments:  
//   Example for acceptable line "TimeStampBase: 2008-12-05-18:20:49.770204 0.015099" for readable
//   Example for acceptable line "TimeStampBase: #2008-12-05-18:20:49.770204 0.015099" for gnuplot
//
///////////////////////////////////////////////////////////
function f_EPTF_StatCapture_Test_checkFirstLine(in charstring pl_line) 
return boolean {
  var template charstring tl_expectedFirstLine;

  if(tsp_EPTF_StatCapture_fileFormat==gnuplot) {
    tl_expectedFirstLine:=  pattern "\#TimeStampBase: [0-9]#(4,4)\-[0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9]#(2,6) [0-9].[0-9]#(2,6)"
  } else {
      //                             TimeStampBase:  2008       - 12        -  05      - 18       : 20       :  49       .770204           0.015099
    tl_expectedFirstLine := pattern "TimeStampBase: [0-9]#(4,4)\-[0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9]#(2,6) [0-9].[0-9]#(2,6)";
  }
  log( match(pl_line,tl_expectedFirstLine) );
  return match(pl_line,tl_expectedFirstLine);
}//f_

///////////////////////////////////////////////////////////
//  Function: f_EPTF_StatCapture_Test_checkSecondLine
//
//  Purpose: checks the second line of the log file
//     
//
//  Detailed Comments:  
//   Example for acceptable line "CaptureFileVersion: 2.2" for readable
//   Example for acceptable line "#CaptureFileVersion: 2.2" for gnuplot
//
///////////////////////////////////////////////////////////
function f_EPTF_StatCapture_Test_checkSecondLine(in charstring pl_line) 
return boolean {
  var charstring vl_expectedLine:="CaptureFileVersion: 2.2";
  if(tsp_EPTF_StatCapture_fileFormat==gnuplot) {
    vl_expectedLine:="#" & vl_expectedLine;
  }
  log( match(pl_line, vl_expectedLine));
  return match(pl_line, vl_expectedLine );
}//f_

///////////////////////////////////////////////////////////
//  Function: f_EPTF_StatCapture_Test_checkThirdLine
//
//  Purpose: checks the third line of the log file
//     
//
//  Detailed Comments:  
//   Example for acceptable line "CaptureGroup[\"group1\"]" for readable
//   Example for acceptable line "#CaptureGroup[\"group1\"]" for gnuplot
//
///////////////////////////////////////////////////////////
function f_EPTF_StatCapture_Test_checkThirdLine(in charstring pl_line, in charstring pl_groupName) return boolean {
  var charstring vl_expectedLine:= "CaptureGroup[\"" & pl_groupName & "\"]";
  if(tsp_EPTF_StatCapture_fileFormat==gnuplot) {
    vl_expectedLine:="#" & vl_expectedLine;
  }
  log( match(pl_line, vl_expectedLine));
  return match(pl_line, vl_expectedLine);
}
///////////////////////////////////////////////////////////
//  Function: f_EPTF_StatCapture_Test_checkFourthLine
//
//  Purpose: checks the third line of the log file
//     
//
//  Detailed Comments:  
//   Example for acceptable line "Capture_Started[\"group1\", 0_0, programmed]" for readable
//   Example for acceptable line "#Capture_Started[\"group1\", 0_0, programmed]" for gnuplot
//
///////////////////////////////////////////////////////////
//e.g:"Capture_Started[\"group1\", 0_0, programmed]",
function f_EPTF_StatCapture_Test_checkFourthLine(in charstring pl_line,in charstring pl_groupName,in charstring pl_capturemode := "programmed") 
return boolean {
  var charstring vl_expectedLine := "Capture_Started[\"" & pl_groupName & "\", 0_0, " & pl_capturemode & "]"
  if(tsp_EPTF_StatCapture_fileFormat==gnuplot) {
    vl_expectedLine:="#" & vl_expectedLine;
  }
  log( match(pl_line, vl_expectedLine));
  return match(pl_line, vl_expectedLine);
}
///////////////////////////////////////////////////////////
//  Function: f_EPTF_StatCapture_Test_checkFifthLine
//
//  Purpose: checks the third line of the log file
//     
//
//  Detailed Comments:  
//   Example for acceptable line "SampleTime[\"group1\"]: 1.000000 sec" for readable
//   Example for acceptable line "#SampleTime[\"group1\"]: 1.000000 sec" for gnuplot
//
///////////////////////////////////////////////////////////
function f_EPTF_StatCapture_Test_checkFifthLine(in charstring pl_line,in charstring pl_groupName, in float pl_sampleTime) 
return boolean {
  var charstring vl_expectedLine := "SampleTime[\"" & pl_groupName & "\"]: " & float2str(pl_sampleTime) & " sec";
    if(tsp_EPTF_StatCapture_fileFormat==gnuplot) {
    vl_expectedLine:="#" & vl_expectedLine;
  }
  log( match(pl_line, vl_expectedLine));
  return match(pl_line, vl_expectedLine);
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_StatCapture_Test_checkSixthLine
//
//  Purpose: checks the 6th line of the log file
//     
//
//  Detailed Comments:  
//   Example for acceptable line "ValueHeader[\"group1\"]: Int1Content Int1Delta Float2deltaSum " for readable
//   Example for acceptable line "#ValueHeader[\"group1\"]: Int1Content Int1Delta Float2deltaSum " for gnuplot
//
///////////////////////////////////////////////////////////
function f_EPTF_StatCapture_Test_checkSixthLine(in charstring pl_line,in charstring pl_groupName, in EPTF_CharstringList vl_statNames) 
return boolean {
  var charstring vl_expectedLine := "ValueHeader[\"" & pl_groupName & "\"]: ";
  for( var integer i:=0; i<sizeof(vl_statNames); i:=i+1) {
    vl_expectedLine:= vl_expectedLine & vl_statNames[i] & " ";
  }
  if(tsp_EPTF_StatCapture_fileFormat==gnuplot) {
    vl_expectedLine:="#" & vl_expectedLine;
  }
  log( match(pl_line, vl_expectedLine));
  return match(pl_line, vl_expectedLine);
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_StatCapture_dc2Str
//
//  Purpose: To convert direct content to string
//     
//
//  Detailed Comments:  
//   Example for acceptable line "ValueHeader[\"group1\"]: Int1Content Int1Delta Float2deltaSum " for readable
//   Example for acceptable line "#ValueHeader[\"group1\"]: Int1Content Int1Delta Float2deltaSum " for gnuplot
//
///////////////////////////////////////////////////////////
function f_EPTF_StatCapture_Test_dc2Str(in EPTF_Var_DirectContent pl_dc, out charstring pl_str) {
  if(ischosen(pl_dc.intVal)){
    pl_str:= int2str(pl_dc.intVal)
  } else if(ischosen(pl_dc.floatVal)){
    pl_str:=float2str(pl_dc.floatVal)
  } else if(ischosen(pl_dc.charstringVal)) {
    pl_str:= pl_dc.charstringVal;
  } else {
    log(">>>WARING: unsupported data type!!!");
    pl_str:="-"
  }
}
///////////////////////////////////////////////////////////
//  Function: f_EPTF_StatCapture_Test_checkLastLine
//
//  Purpose: checks the last line of the log file(s)
//     
//
//  Detailed Comments:  
//   Example for acceptable line "Capture_Finished[\"group1\", 0_0]" for readable
//   Example for acceptable line "#Capture_Finished[\"group1\", 0_0]" for gnuplot
//
///////////////////////////////////////////////////////////Capture_Finished["group1", 0_0]
function f_EPTF_StatCapture_Test_LastLine(in charstring pl_line,in charstring pl_groupName) 
return boolean {
  var charstring vl_expectedLine := "Capture_Finished[\"" & pl_groupName & "\", 0_0]";
  if(tsp_EPTF_StatCapture_fileFormat==gnuplot) {
    vl_expectedLine:="#" & vl_expectedLine;
  }
  log( match(pl_line, vl_expectedLine));
  return match(pl_line, vl_expectedLine);
}

function f_EPTF_StatCapture_Test_checkBody(in EPTF_CharstringList pl_lines,in charstring pl_groupName, in EPTF_StatCapture_Test_DcArray pl_expectedResults) 
return boolean {
  var EPTF_CharstringList vl_words, vl_firstTwo;
  var charstring vl_str:="";
  var EPTF_CharstringList vl_prevstrlist := {"", "", "", ""};
  var integer vl_outofSyncCount:=0;
  var integer j:=0;
  var template charstring tl_num :=  pattern "[0-9]#(1,).[0-9]#(1,)\]:";

  for(var integer i:=6;i<sizeof(pl_lines)-1; i:=i+1) {
    //log(">>>Expected results[", i,"]: ",pl_expectedResults[i-6]);
    vl_words:= f_EPTF_StatCapture_Test_split(pl_lines[i], " ");
    //log("i= ",i, " vl_words: ",vl_words);
    if(tsp_EPTF_StatCapture_fileFormat==readable) {

      vl_firstTwo:=f_EPTF_StatCapture_Test_split( vl_words[0],",");
      if( (vl_firstTwo[0]=="[\"" & pl_groupName & "\"") and 
        match( vl_firstTwo[1],tl_num) )
      {
        for(j:=1;j<sizeof(vl_words); j:=j+1) {
          f_EPTF_StatCapture_Test_dc2Str(pl_expectedResults[i-6][j-1], vl_str);
          // if out of sync one time no problem
          if(vl_prevstrlist[j] != "" and vl_words[j] == vl_prevstrlist[j] and vl_outofSyncCount<(sizeof(vl_words)-1))
          {
             action("f_EPTF_StatCapture_Test_checkBody out of sync: ", vl_words[j], "-", vl_str);
             vl_outofSyncCount := vl_outofSyncCount+1;
          }
          else
          if(vl_words[j]!=vl_str) {
            log(match(vl_words[j],vl_str));
            return false;
          }
          vl_prevstrlist[j] := vl_words[j];
        }
      } else {
        log(">>>Failed at i: ", i);
        return false;
      }

    }else {//gnuplot:

      if(vl_words[0]=="\"" & pl_groupName & "\"" and match( vl_words[1] , pattern "[0-9]#(1,).[0-9]#(1,)") ) {
        for(j:=2;j<sizeof(vl_words); j:=j+1) {
          //log(">>>Expected results[", i,"][",j,"]: ",pl_expectedResults[i-6][j-2]);
          f_EPTF_StatCapture_Test_dc2Str(pl_expectedResults[i-6][j-2], vl_str);
          //log(">>>2.Expected results[", i,"][",j,"]: ",vl_str);
          // if out of sync one time no problem
          if(vl_prevstrlist[j] != "" and vl_words[j] == vl_prevstrlist[j] and vl_outofSyncCount<(sizeof(vl_words)-1))
          {
             action("f_EPTF_StatCapture_Test_checkBody out of sync: ", vl_words[j], "-", vl_str);
             vl_outofSyncCount := vl_outofSyncCount+1;
          }
          else
          if(vl_words[j]!=vl_str) {
            log(match(vl_words[j],vl_str));
            return false;
          }
          vl_prevstrlist[j] := vl_words[j];
        }
      } else {
        log(">>>Failed at i: ", i);
        return false;
      }

    }//if

  }//for
  return true;
}//f_
///////////////////////////////////////////////////////////
//  Function: f_EPTF_StatCapture_Test_checkCapturedFile
//
//  Purpose: to check the content of the captured file(s). This content is an input string
//   1. Split the content for lines
//   2. Check the first 6 lines as header lines
//   3. Check the body
//   4. Check the last line as footer
//   Set the verdict in each step
//  Detailed Comments: 
// 
///////////////////////////////////////////////////////////
function f_EPTF_StatCapture_Test_checkCapturedFile(
  in integer pl_fileIdx,
  in charstring pl_groupName,
  in float pl_sampleTime,
  in EPTF_CharstringList pl_statNames,
  in EPTF_StatCapture_Test_DcArray pl_expectedResults)
runs on EPTF_StatCapture_Test_CT {
  var charstring vl_fileName:="";
  var charstring vl_fileContent :="";
  f_EPTF_StatCapture_Test_setVerdict(f_EPTF_StatCaptureStat_Test_readFiles(pl_fileIdx,vl_fileContent));
  log(">>>Fetched file content: ", vl_fileContent);
  var EPTF_CharstringList vl_lines := f_EPTF_StatCapture_Test_split(vl_fileContent, "\n");
  log("lines:",vl_lines);
  if( sizeof(vl_lines) >=8 ) {
    f_EPTF_StatCapture_Test_setVerdict(f_EPTF_StatCapture_Test_checkFirstLine(vl_lines[0]));//test for f_EPTF_StatCapture_openNewStatLogFile
    f_EPTF_StatCapture_Test_setVerdict(f_EPTF_StatCapture_Test_checkSecondLine(vl_lines[1]));//test for f_EPTF_StatCapture_openNewStatLogFile
    f_EPTF_StatCapture_Test_setVerdict(f_EPTF_StatCapture_Test_checkThirdLine(vl_lines[2], pl_groupName)); //test for f_EPTF_StatCapture_addNewGroup,
    //f_EPTF_StatCapture_startCaptureAll, f_EPTF_StatCapture_dumpStringforGroup
    f_EPTF_StatCapture_Test_setVerdict(f_EPTF_StatCapture_Test_checkFourthLine(vl_lines[3], pl_groupName));
    f_EPTF_StatCapture_Test_setVerdict(f_EPTF_StatCapture_Test_checkFifthLine(vl_lines[4],pl_groupName,pl_sampleTime));
    f_EPTF_StatCapture_Test_setVerdict(f_EPTF_StatCapture_Test_checkSixthLine(vl_lines[5],pl_groupName,pl_statNames));

    f_EPTF_StatCapture_Test_setVerdict(f_EPTF_StatCapture_Test_checkBody(vl_lines, pl_groupName, pl_expectedResults));
    f_EPTF_StatCapture_Test_setVerdict(f_EPTF_StatCapture_Test_LastLine(vl_lines[sizeof(vl_lines)-1],pl_groupName));
  } else {
    setverdict(fail);
  }

}//f_


///////////////////////////////////////////////////////////
//  Function: f_EPTF_StatCapture_Test_clientServer_clientBehavior
//
//  Purpose: 
//      Realises the StatCapture client behavior - creates variables and modifies their value constantly while telling the server component
//      to subsribe to the variables and capture the statistics. 
//  Detailed Comments: 
// 
///////////////////////////////////////////////////////////
function f_EPTF_StatCapture_Test_clientServer_clientBehavior(in EPTF_StatCapture_CT pl_server, in charstring pl_groupName := "", 
                                                             in integer pl_NrOfRestart := 1, in boolean pl_deleteGroup := false) runs on EPTF_StatCaptureClient_Test_CT{
  action("=========StatCapture client started=========")
  timer T_1s:=0.5;T_1s.start;T_1s.timeout; //just to get a different timestamp for our files
  var charstring vl_name:="EPTF_StatCapture_Test_client";
  f_EPTF_StatCaptureClient_init(vl_name, pl_server); 
  f_EPTF_StatMeasure_init_CT(vl_name); 

  v_StatCapture_Test_initialized := true;
  v_StatCapture_Test_captureGroups := {};
  v_StatCapture_Test_selfName := vl_name;
  f_EPTF_Base_registerCleanup(refers(f_EPTF_StatCaptureClient_Test_cleanup));

  var integer vl_var1ID:=-1, vl_var2ID:= -1;
  var integer vl_stat3ID:=-1;

  f_EPTF_Var_newInt("var1",10,vl_var1ID);
  f_EPTF_Var_newFloat("var2",10.0,vl_var2ID);
  vl_stat3ID := f_EPTF_StatMeasure_newStat(vl_var2ID, deltaSum);
  f_EPTF_StatMeasure_createVarFromStat(vl_stat3ID, "varstat3");  
  var EPTF_CharstringList vl_statNames:= {"var1","varstat3"};
  f_EPTF_StatCaptureClient_addStatsToGroup( "group1",vl_statNames, sampledAtSync)

  f_EPTF_StatCaptureClient_Test_genarateStat(vl_var1ID,vl_var2ID, 10.0, 0.901, pl_groupName, pl_NrOfRestart, pl_deleteGroup);	// the smallest but enought pl_adjTime the shure thing [K.I.]
  timer T_2s:=3.0;T_2s.start;T_2s.timeout; //let the variables created
  sync_PCO.send( " ready " );
  f_EPTF_Base_wait4Shutdown();
}

function f_EPTF_StatCapture_Test_clientServer_maxWaitTime_wait(in float pl_waitTime) runs on EPTF_StatCaptureClient_Test_CT{
  var float vl_startTime := f_EPTF_Base_getRelTimeInSecs();
  while (f_EPTF_Base_getRelTimeInSecs() < vl_startTime + pl_waitTime) {
  }
}

function f_EPTF_StatCapture_Test_clientServer_maxWaitTime_behaviour(in EPTF_StatCapture_CT pl_server, in float pl_maxWaitTime, in charstring pl_groupName := "", 
                                                             in integer pl_NrOfRestart := 1, in boolean pl_deleteGroup := false) runs on EPTF_StatCaptureClient_Test_CT{
  action("=========StatCapture client started=========")
  timer T_1s:=0.5;T_1s.start;T_1s.timeout; //just to get a different timestamp for our files
  var charstring vl_name:="EPTF_StatCapture_Test_client";
  f_EPTF_StatCaptureClient_init(vl_name, pl_server); 
  f_EPTF_StatMeasure_init_CT(vl_name); 

  v_StatCapture_Test_initialized := true;
  v_StatCapture_Test_captureGroups := {};
  v_StatCapture_Test_selfName := vl_name;
  f_EPTF_Base_registerCleanup(refers(f_EPTF_StatCaptureClient_Test_cleanup));

  for(var integer vl_idx := 0; vl_idx < 100; vl_idx := vl_idx + 1) {
    var integer vl_var1ID:=-1, vl_var2ID:= -1;
    var integer vl_stat3ID:=-1;
    var charstring vl_var1 := "varI" & int2str(vl_idx);
    var charstring vl_var2 := "varF" & int2str(vl_idx);
    var charstring vl_var3 := "varstat" & int2str(vl_idx);
    
    f_EPTF_Var_newInt(vl_var1,10,vl_var1ID);
    f_EPTF_Var_newFloat(vl_var2,10.0 ,vl_var2ID);
    vl_stat3ID := f_EPTF_StatMeasure_newStat(vl_var2ID, deltaSum);
    f_EPTF_StatMeasure_createVarFromStat(vl_stat3ID, vl_var3);  
    var EPTF_CharstringList vl_statNames:= {vl_var1, vl_var2, vl_var3};
    f_EPTF_StatCaptureClient_addStatsToGroup( "group" & int2str(vl_idx),vl_statNames, sampledAtSync)
    //timer T_2s:=0.001;T_2s.start;T_2s.timeout; 

//    f_EPTF_StatCaptureClient_Test_genarateStat(vl_var1ID,vl_var2ID, 10.0, 0.901, pl_groupName, pl_NrOfRestart, pl_deleteGroup);   // the smallest but enought pl_adjTime the shure thing [K.I.]
  }
  f_EPTF_StatCapture_Test_clientServer_maxWaitTime_wait(pl_maxWaitTime);
  
//  timer T_2s:=3.0;T_2s.start;T_2s.timeout; //let the variables created
//  sync_PCO.send( " ready " );
  f_EPTF_Base_wait4Shutdown();
}

type component f_EPTF_StatCapture_Test_CheckWarning_CT extends EPTF_StatCapture_Test_CT { 
    var boolean v_StatCapture_Test_warningArrived := false;
    var charstring v_StatCapture_Test_warningMsg:="";
  }

  function f_EPTF_StatCapture_Test_CheckWarning_init(in charstring pl_name) runs on f_EPTF_StatCapture_Test_CheckWarning_CT {
    f_StatCapture_Test_init(pl_name);
    v_StatCapture_Test_warningArrived := false;
    v_StatCapture_Test_warningMsg:="";
    f_EPTF_Base_registerCleanup(refers(f_EPTF_StatCapture_Test_CheckWarning_cleanup));
    f_EPTF_Logging_registerPreambleFn(refers(f_EPTF_StatCapture_Test_CheckWarning_preamble_FT));
  }

  function f_EPTF_StatCapture_Test_CheckWarning_cleanup() runs on f_EPTF_StatCapture_Test_CheckWarning_CT {
    if (v_StatCapture_Test_warningArrived) {
      setverdict(fail,"Warning message ",v_StatCapture_Test_warningMsg," was logged!");
    } else {
      setverdict(pass);
    }
  }

  function f_EPTF_StatCapture_Test_CheckWarning_setWarning(in charstring pl_warningMsg) runs on f_EPTF_StatCapture_Test_CheckWarning_CT {
    v_StatCapture_Test_warningMsg := pl_warningMsg;
  }

  function f_EPTF_StatCapture_Test_CheckWarning_preamble_FT(in charstring pl_message) runs on f_EPTF_StatCapture_Test_CheckWarning_CT {
    if (match(pl_message,pattern v_StatCapture_Test_warningMsg)) {
      v_StatCapture_Test_warningArrived := true;
    }
  }

///////////////////////////////////////////////////////////
//  Function: f_EPTF_StatCaptureClient_Test_genarateStat
//
//  Purpose:  generate a very special stat by adjusting pl_var1ID and pl_var2ID
//     
//
//  Detailed Comments:
//   1. Starts the capture by f_EPTF_StatCaptureClient_startGroupAll() if pl_groupName is not filled, f_EPTF_StatCaptureClient_startGroup(pl_groupName) else.
//   2. Generate statistics by calling f_EPTF_Var_adjustContent for pl_var1ID and pl_var2ID
//   3. Stops the statistics by f_EPTF_StatCaptureClient_stopGroupAll() if pl_groupName is not filled, f_EPTF_StatCaptureClient_stopGroup(pl_groupName) else.
//
///////////////////////////////////////////////////////////
function f_EPTF_StatCaptureClient_Test_genarateStat(in integer pl_var1ID, in integer pl_var2ID, 
                                                    in float pl_execTime:=10.0, in float pl_adjTime := 1.0,
                                                    in charstring pl_groupName := "", in integer pl_NrOfRestart, in boolean pl_deleteGroup) 
runs on EPTF_StatCaptureClient_Test_CT 
{
action("=========f_EPTF_StatCaptureClient_Test_genarateStat started=========")
  timer t_wait := pl_execTime;
  t_wait.start;

  timer t_adjust := pl_adjTime;
  t_adjust.start;

  // add initial value to the stats:
  f_EPTF_Var_refreshContent(pl_var1ID);
  f_EPTF_Var_refreshContent(pl_var2ID);
  var integer vl_now := 1;
  for(var integer vl_i := 0; vl_i < pl_NrOfRestart; vl_i := vl_i + 1){
      if (t_wait.running){
        timer t_1:=0.2; t_1.start; t_1.timeout;
        if (pl_groupName == "") {
          f_EPTF_StatCaptureClient_startGroupAll();
        } else {
          f_EPTF_StatCaptureClient_startGroup(pl_groupName);
        }
        alt {
          [] t_adjust.timeout{
              f_EPTF_Var_adjustContent(pl_var1ID, {intVal:= vl_now});
              f_EPTF_Var_adjustContent(pl_var2ID, {floatVal:= int2float(vl_now)}); vl_now := vl_now * 2; t_adjust.start; 
              repeat;
            }
          [] t_wait.timeout {}
        }
  
        if (pl_groupName == "") {
          f_EPTF_StatCaptureClient_stopGroupAll();
        } else {
          f_EPTF_StatCaptureClient_stopGroup(pl_groupName);
        }
        if(pl_deleteGroup == true){
          f_EPTF_StatCaptureClient_deleteGroup(pl_groupName);
        }
      }
  }    
  setverdict(pass);
}

}//module
