///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_DsRestAPI_Test_Functions
//
// Purpose:
//   This module contains functions for testing EPTF DsRestAPI.
//
// Module Parameters:
//    -
//  Module depends on:
//    <EPTF_DsRestAPI_Test_Definitions>
//    <EPTF_CLL_DsRestAPI_Definitions>
//    <EPTF_CLL_DsRestAPI_Functions>
//    <EPTF_CLL_Base_Functions>
//    <EPTF_CLL_Common_Definitions>
//    <EPTF_CLL_Variable_Definitions>
//    <EPTF_CLL_DsRestAPI_DSServer_Functions>
//    <EPTF_CLL_DataSource_Definitions>
//    <EPTF_CLL_DataSourceClient_Functions>
//    <EPTF_CLL_Logging_Functions>
//    <EPTF_CLL_Variable_Functions>
//    <TCCFileIO_Functions>
//
// Current Owner:
//    Zoltan Zsichla (ezolzsi)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////
module EPTF_DsRestAPI_Test_Functions {

//=========================================================================
// Import Part
//=========================================================================
import from EPTF_DsRestAPI_Test_Definitions all;
import from EPTF_CLL_DsRestAPI_Definitions all;
import from EPTF_CLL_DsRestAPI_DSServer_Definitions all;
import from EPTF_DsRestAPI_Test_Definitions all;
import from EPTF_CLL_DsRestAPI_Functions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_Variable_Definitions all
import from EPTF_CLL_DsRestAPI_DSServer_Functions all;
import from EPTF_CLL_DataSource_Definitions all;
import from EPTF_CLL_DataSourceClient_Functions all;
import from EPTF_CLL_Logging_Functions all;
import from EPTF_CLL_Variable_Functions all;
import from TCCFileIO_Functions all;
import from TCCConversion_Functions all;

//=========================================================================
// Module parameters
//=========================================================================
modulepar charstring tsp_EPTF_DsRestAPI_DSMockFilePath;

//=========================================================================
// Functions
//=========================================================================

function f_EPTF_DsRestAPI_Test_init_CT(
  in charstring pl_selfName
) runs on DsRestAPI_Test_CT {
  
  f_EPTF_Base_init_CT(pl_selfName);
  f_EPTF_DsRestAPI_init_CT(pl_selfName);
  
}

function f_EPTF_DsRestAPI_Test_getFileContent(
  in charstring pl_path
) runs on DsRestAPI_Test_CT return charstring{

  var charstring vl_retVal := "";
  var integer vl_numOfReadedBytes := -1;
  var integer vl_endOfFile := -1;
  var integer vl_fd := -1;

  vl_fd := f_FIO_open_rdonly(pl_path);
  vl_endOfFile := f_FIO_seek_end(vl_fd);
  f_FIO_seek_home(vl_fd);

  if(vl_fd < 0){
    log("File: ", pl_path," cannot be opened, ignored.");
    log (f_FIO_get_error_string());
  }
  else{
    vl_numOfReadedBytes := f_FIO_read_text(vl_fd, vl_retVal, vl_endOfFile);
    if(vl_numOfReadedBytes < 0)
    {
      log("File: ", pl_path," cannot be readed");
      log (f_FIO_get_error_string ());
    }
  }

  return vl_retVal;
}

group DsRestAPI_Test_DataSourceClient {

function f_EPTF_DsRestAPI_Test_getDSfromFile() runs on DsRestAPI_Test_CT {
  var charstring vl_fileContent := f_EPTF_DsRestAPI_Test_getFileContent(tsp_EPTF_DsRestAPI_DSMockFilePath);
  var charstring vl_JSONString := ef_EPTF_DsRestAPI_Test_getJSON(vl_fileContent);
  var octetstring vl_JSONOct := char2oct(vl_JSONString);
  v_DsRestAPI_Test_DSMock := ef_DsRestAPI_Test_dec_DSMock(vl_JSONOct);
}

function f_EPTF_DsRestAPI_Test_DS_init_CT(
  in charstring pl_selfName,
  in EPTF_DataSource_CT pl_dataSource_compRef := null
) runs on DsRestAPI_Test_CT{
  f_EPTF_DsRestAPI_DSServer_init_CT(pl_selfName); 

  if(pl_dataSource_compRef != null){
    f_EPTF_DsRestAPI_Test_getDSfromFile();
    f_EPTF_DataSourceClient_init_CT(f_EPTF_Base_selfName(), pl_dataSource_compRef);
    f_EPTF_DataSourceClient_registerData(v_DsRestAPI_Test_DSMock.DSClientName, f_EPTF_Base_selfName(), refers(f_EPTF_DsRestAPI_Test_DSProcessGetData), pl_dataSource_compRef);
    f_EPTF_DataSourceClient_registerDataValue(v_DsRestAPI_Test_DSMock.DSClientName, f_EPTF_Base_selfName(), refers(f_EPTF_DsRestAPI_Test_DSProcessDataValue), pl_dataSource_compRef);
    f_EPTF_DataSourceClient_registerSetDataValue(v_DsRestAPI_Test_DSMock.DSClientName, f_EPTF_Base_selfName(), refers(f_EPTF_DsRestAPI_Test_DSProcessSetDataValue), pl_dataSource_compRef);
    f_EPTF_DataSourceClient_sendReady(v_DsRestAPI_Test_DSMock.DSClientName, f_EPTF_Base_selfName());
  }
}

function f_EPTF_DsRestAPI_Test_DSProcessSetDataValue(inout EPTF_Var_DirectContent pl_dataValue,
                                              in charstring pl_source,
                                              in charstring pl_ptcName := "",
                                              in charstring pl_element,
                                              in EPTF_DataSource_Params pl_params := {},
                                              in EPTF_IntegerList pl_indexList := {})
runs on DsRestAPI_Test_CT return integer{ 
  var EPTF_CharstringList pl_result := {};

  select (pl_element) {
    case (c_DsRestAPI_Test_dataElementEGrpType) 
    {
    if (0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params,{c_DsRestAPI_Test_paramNameEntityGroup},pl_result)){
        for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
          if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
            v_DsRestAPI_Test_DSMock.EntityGroups[j].EGrpType.Value := pl_dataValue.charstringVal;
            return 0;
          }
        }
      }
    }
    case (c_DsRestAPI_Test_dataElementEGrpSize) 
    {
    if (0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params,{c_DsRestAPI_Test_paramNameEntityGroup},pl_result)){
          for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
        if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
        v_DsRestAPI_Test_DSMock.EntityGroups[j].EGrpSize.Value := int2str(pl_dataValue.intVal);
        return 0;
        }
      }
        }
    }
    case (c_DsRestAPI_Test_dataElementScStart) 
    {
    if (0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params,{c_DsRestAPI_Test_paramNameEntityGroup, c_DsRestAPI_Test_paramNameScenario},pl_result)){
      for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
        if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
        for(var integer i := 0; i < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios); i := i + 1) {
        if (pl_result[1] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].Value) {
          if (pl_dataValue.boolVal) { 
                  v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].ScStart.Value := "true"; 
                } else {
                  v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].ScStart.Value := "false";
                }
          return 0;
            }
        }
        }
          }
      }
    }
    case (c_DsRestAPI_Test_dataElementScStop) 
    {
    if (0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params,{c_DsRestAPI_Test_paramNameEntityGroup, c_DsRestAPI_Test_paramNameScenario},pl_result)){
      for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
        if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
        for(var integer i := 0; i < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios); i := i + 1) {
        if (pl_result[1] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].Value) {
          if (pl_dataValue.boolVal) { 
                  v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].ScStop.Value := "true"; 
                } else {
                  v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].ScStop.Value := "false";
                }
          return 0;
            }
        }
        }
          }
      }
    }
    case (c_DsRestAPI_Test_dataElementScStatusLED) 
    {
    if (0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params,{c_DsRestAPI_Test_paramNameEntityGroup, c_DsRestAPI_Test_paramNameScenario},pl_result)){
      for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
        if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
        for(var integer i := 0; i < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios); i := i + 1) {
        if (pl_result[1] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].Value) {
          var charstring vl_color; 
                f_EPTF_DsRestAPI_Test_Color2Str(pl_dataValue.statusLEDVal.color, vl_color);
                v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].ScStatusLED.Value := vl_color & pl_dataValue.statusLEDVal.text;
          return 0;
            }
        }
        }
          }
      }
    }
    case (c_DsRestAPI_Test_dataElementTcStart) 
    {
    if(0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params,{c_DsRestAPI_Test_paramNameEntityGroup, c_DsRestAPI_Test_paramNameScenario, c_DsRestAPI_Test_paramNameTrafficCase}, pl_result)){
          for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
        if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
        for(var integer i := 0; i < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios); i := i + 1) {
        if (pl_result[1] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].Value) {
          for(var integer k := 0; k < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases); k := k + 1) {
            if (pl_result[2] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases[k].Value) {
              if (pl_dataValue.boolVal) { 
              v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases[k].TcStart.Value := "true"; 
              } else {
              v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases[k].TcStart.Value := "false";
              }
              return 0;
              }
          }
            }
        }
        }
          }
    }
    }
    case (c_DsRestAPI_Test_dataElementTcStop) 
    {
    if(0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params,{c_DsRestAPI_Test_paramNameEntityGroup, c_DsRestAPI_Test_paramNameScenario, c_DsRestAPI_Test_paramNameTrafficCase}, pl_result)){
          for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
        if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
        for(var integer i := 0; i < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios); i := i + 1) {
        if (pl_result[1] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].Value) {
          for(var integer k := 0; k < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases); k := k + 1) {
            if (pl_result[2] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases[k].Value) {
              if (pl_dataValue.boolVal) { 
              v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases[k].TcStop.Value := "true"; 
              } else {
              v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases[k].TcStop.Value := "false";
              }
              return 0;
              }
          }
            }
        }
        }
          }
    }
    }
    case else {
      return -1;
    }
  }
  return -2;
}

function f_EPTF_DsRestAPI_Test_str2Color (in charstring pl_value, out EPTF_LEDColors pl_color) return integer {
    if (pl_value == "led:blue") {
      pl_color := led_blue;
    }
    else if (pl_value == "led:black") {
      pl_color := led_black;
    }
    else if (pl_value == "led:yellow") {
      pl_color := led_yellow;
    }
    else if (pl_value == "led:green") {
      pl_color := led_green;
    }
    else if (pl_value == "led:red") {
      pl_color := led_red;
    }
    else {
      log(%definitionId, ": Undefined EPTF_LEDColors Input: ", pl_value);
      action("ERROR: ", pl_value);
      return -1;
    }
    return 0;
}

function f_EPTF_DsRestAPI_Test_Color2Str (in EPTF_LEDColors pl_color, out charstring pl_value) return integer {
    if (pl_color == led_blue) {
      pl_value := "[led:blue]";
    }
    else if (pl_color == led_black) {
      pl_value := "[led:black]";
    }
    else if (pl_color == led_yellow) {
      pl_value := "[led:yellow]";
    }
    else if (pl_color == led_green) {
      pl_value := "[led:green]";
    }
    else if (pl_color == led_red) {
      pl_value := "[led:red]";
    }
    else {
      log(%definitionId, ": Undefined EPTF_LEDColors Input: ", pl_color);
      action("ERROR: ", pl_color);
      return -1;
    }
    return 0;
}

function f_EPTF_DsRestAPI_Test_tp2DirectContent(in integer pl_tp, in charstring pl_value, inout EPTF_Var_DirectContent pl_dataValue) return integer{
  @try {
    if (pl_tp == 1) {
      pl_dataValue := {intVal := str2int(pl_value)};
    }
    else if (pl_tp == 2) {
      pl_dataValue := {floatVal := str2float(pl_value)};
    }
    else if (pl_tp == 3) {
      pl_dataValue := {boolVal := (pl_value == "true")};
    }
    else if (pl_tp == 4) {
      pl_dataValue := {charstringVal := pl_value};
    }
    else if (pl_tp == 5) {
      pl_dataValue := {octetstringVal := char2oct(pl_value)};
    }
    else if (pl_tp == 6) {
      pl_dataValue := {hexstringVal := str2hex(pl_value)};
    }
    else if (pl_tp == 7) {
      pl_dataValue := {bitstringVal := str2bit(pl_value)};
    }
    else if (pl_tp == 8) {
      if (pl_dataValue == {unknownVal := {omit}}) {
        pl_dataValue := {integerlistVal := {str2int(pl_value)}};
      } else {
        pl_dataValue.integerlistVal[sizeof(pl_dataValue.integerlistVal)] := str2int(pl_value);
      }
    }
    else if (pl_tp == 9) {
      if (pl_dataValue == {unknownVal := {omit}}) {
        pl_dataValue := {floatlistVal := {str2float(pl_value)}};
      } else {
        pl_dataValue.floatlistVal[sizeof(pl_dataValue.floatlistVal)] := str2float(pl_value);
      }
    }
    else if (pl_tp == 10) {
      if (pl_dataValue == {unknownVal := {omit}}) {
        pl_dataValue := {charstringlistVal := {pl_value}};
      } else {
        pl_dataValue.charstringlistVal[sizeof(pl_dataValue.charstringlistVal)] := pl_value;
      }
    }
    else if (pl_tp == 11) {
      var charstring vl_searchStr := "]";
      var integer vl_strPos := f_strstr(pl_value, vl_searchStr);
      var EPTF_LEDColors vl_color;
      if ( 0 == f_EPTF_DsRestAPI_Test_str2Color(substr(pl_value, 1, vl_strPos-1), vl_color)) {
        pl_dataValue := {statusLEDVal := {
          vl_color,
          substr(pl_value, vl_strPos+1, lengthof(pl_value)-vl_strPos-1)
        }};
      }
    } else {
      log(%definitionId, ": Undefined tp Input: ", pl_tp);
      action("ERROR: ", pl_dataValue);
      return -2;
    }
    return 0;
  } @catch(dte_str) {
    log(%definitionId, ": Dynamic Test Error: ", dte_str);
    action("DTE_STR: ", dte_str);
    return -1;
  }
}

function f_EPTF_DsRestAPI_Test_DSProcessGetData(
  out charstring pl_dataVarName,
  in charstring pl_source,
  in charstring pl_ptcName,
  in charstring pl_element,
  in EPTF_DataSource_Params pl_params
) runs on DsRestAPI_Test_CT return integer {
  var integer vl_errorCode := -1;
  var EPTF_CharstringList pl_result := {};
  pl_dataVarName := log2str("Error: variable not found for dataSource: source: ", pl_source, " element: ", pl_element, " params: ", pl_params); // set it to error message
  
  select(pl_element) {
    case (c_DsRestAPI_Test_dataElementTcCPS) {
      if(0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params,{c_DsRestAPI_Test_paramNameEntityGroup, c_DsRestAPI_Test_paramNameScenario, c_DsRestAPI_Test_paramNameTrafficCase}, pl_result)){
        for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
          if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
            for(var integer i := 0; i < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios); i := i + 1) {
              if (pl_result[1] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].Value) {
                for(var integer k := 0; k < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases); k := k + 1) {
                  if (pl_result[2] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases[k].Value) {
                    var integer vl_idx := f_EPTF_Var_getId(c_DsRestAPI_Test_varNameTcCPS);
                    if (-1 == vl_idx) {
                      f_EPTF_Var_newFloat(c_DsRestAPI_Test_varNameTcCPS, 10.0, vl_idx);
                    }
                    pl_dataVarName := c_DsRestAPI_Test_varNameTcCPS;
                    vl_errorCode := 0;
                    return vl_errorCode;
                  }
                }
              }
            }
          }
        }
      }
    }
    case else {

    }
  }
  return vl_errorCode;
}


function f_EPTF_DsRestAPI_Test_DSProcessDataValue(out EPTF_Var_DirectContent pl_dataValue,
                                              in charstring pl_source,
                                              in charstring pl_ptcName := "",
                                              in charstring pl_element,
                                              in EPTF_DataSource_Params pl_params := {})
runs on DsRestAPI_Test_CT return integer{ 
  var integer vl_errorCode := -1;
  var EPTF_CharstringList pl_result := {};
  pl_dataValue := {unknownVal := {omit}}; // set it to invalid

  select(pl_element)
  {
    case (c_DsRestAPI_Test_iteratorEntityGroups)
    {
      if(sizeof(pl_params) == 0){
        for(var integer i := 0; i < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); i := i + 1) {
          vl_errorCode := f_EPTF_DsRestAPI_Test_tp2DirectContent(v_DsRestAPI_Test_DSMock.EntityGroups[i].tp, v_DsRestAPI_Test_DSMock.EntityGroups[i].Value, pl_dataValue);
        }
      } else {
        log(%definitionId, ": Datasource Parameter is not needed.");
      }
    }
    case (c_DsRestAPI_Test_iteratorScenarios)
    {
      if(0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params, {c_DsRestAPI_Test_paramNameEntityGroup}, pl_result)){
        for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
          if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
            for(var integer i := 0; i < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios); i := i + 1) {
              vl_errorCode := f_EPTF_DsRestAPI_Test_tp2DirectContent(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].tp, v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].Value, pl_dataValue);
            }
            return vl_errorCode;
          }
        }
      } else {
        log(%definitionId& ": Datasource Parameter is wrong. The iterator " & c_DsRestAPI_Test_iteratorScenarios & " requires an EntityGroup.");
      }
    }
    case (c_DsRestAPI_Test_iteratorTrafficCases)
    {
      if(0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params,{c_DsRestAPI_Test_paramNameEntityGroup,c_DsRestAPI_Test_paramNameScenario}, pl_result)){
        for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
          if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
            for(var integer i := 0; i < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios); i := i + 1) {
              if (pl_result[1] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].Value) {
                for(var integer k := 0; k < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases); k := k + 1) {
                  vl_errorCode := f_EPTF_DsRestAPI_Test_tp2DirectContent(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases[k].tp, v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases[k].Value, pl_dataValue);
                }
                return vl_errorCode;
              }
            }
          }
        }
      } else if (0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params,{c_DsRestAPI_Test_paramNameScenarioInstance}, pl_result)){
        for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
          if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
            for(var integer i := 0; i < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios); i := i + 1) {
              if (pl_result[1] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].Value) {
                for(var integer k := 0; k < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases); k := k + 1) {
                  vl_errorCode := f_EPTF_DsRestAPI_Test_tp2DirectContent(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases[k].tp, v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases[k].Value, pl_dataValue);
                }
                return vl_errorCode;
              }
            }
          }
        }
      } else {
        log(%definitionId& ": Datasource Parameter(s) are wrong. The iterator " & c_DsRestAPI_Test_iteratorTrafficCases & " requires an EntityGroup and a Scenario pair or a ScenarioInstance.");
      }
    }
    case (c_DsRestAPI_Test_iteratorDummy)
    {
      if(0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params,{c_DsRestAPI_Test_paramNameEntityGroup,c_DsRestAPI_Test_paramNameScenario}, pl_result)){
        for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
          if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
            for(var integer i := 0; i < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios); i := i + 1) {
              if (pl_result[1] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].Value) {
                for(var integer k := 0; k < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].Dummy); k := k + 1) {
                  vl_errorCode := f_EPTF_DsRestAPI_Test_tp2DirectContent(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].Dummy[k].tp, v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].Dummy[k].Value, pl_dataValue);
                }
                return vl_errorCode;
              }
            }
          }
        }
      } else {
        log(%definitionId& ": Datasource Parameter(s) are wrong. The iterator " & c_DsRestAPI_Test_iteratorDummy & " requires an EntityGroup and a Scenario.");
      }
    }
    case (c_DsRestAPI_Test_dataElementEGrpType)
    {
      if (0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params,{c_DsRestAPI_Test_paramNameEntityGroup},pl_result)){
        for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
          if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
            vl_errorCode := f_EPTF_DsRestAPI_Test_tp2DirectContent(v_DsRestAPI_Test_DSMock.EntityGroups[j].EGrpType.tp, v_DsRestAPI_Test_DSMock.EntityGroups[j].EGrpType.Value, pl_dataValue);
            return vl_errorCode;
          }
        }
      }
    }
    case (c_DsRestAPI_Test_dataElementEGrpSize)
    {
      if (0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params,{c_DsRestAPI_Test_paramNameEntityGroup},pl_result)){
        for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
          if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
            vl_errorCode := f_EPTF_DsRestAPI_Test_tp2DirectContent(v_DsRestAPI_Test_DSMock.EntityGroups[j].EGrpSize.tp, v_DsRestAPI_Test_DSMock.EntityGroups[j].EGrpSize.Value, pl_dataValue);
            return vl_errorCode;
          }
        }
      }
    }
    case (c_DsRestAPI_Test_dataElementScStart)
    {
    if (0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params,{c_DsRestAPI_Test_paramNameEntityGroup, c_DsRestAPI_Test_paramNameScenario},pl_result)){
      for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
        if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
        for(var integer i := 0; i < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios); i := i + 1) {
        if (pl_result[1] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].Value) {
          vl_errorCode := f_EPTF_DsRestAPI_Test_tp2DirectContent(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].ScStart.tp, v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].ScStart.Value, pl_dataValue);
          return vl_errorCode;
            }
        }
        }
          }
      }
    }
    case (c_DsRestAPI_Test_dataElementScStop)
    {
      if (0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params,{c_DsRestAPI_Test_paramNameEntityGroup, c_DsRestAPI_Test_paramNameScenario},pl_result)){
        for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
          if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
            for(var integer i := 0; i < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios); i := i + 1) {
              if (pl_result[1] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].Value) {
                vl_errorCode := f_EPTF_DsRestAPI_Test_tp2DirectContent(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].ScStop.tp, v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].ScStop.Value, pl_dataValue);
                return vl_errorCode;
              }
            }
          }
        }
      }
    }
    case (c_DsRestAPI_Test_dataElementScStatusLED)
    {
      if (0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params,{c_DsRestAPI_Test_paramNameEntityGroup, c_DsRestAPI_Test_paramNameScenario},pl_result)){
        for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
          if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
            for(var integer i := 0; i < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios); i := i + 1) {
              if (pl_result[1] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].Value) {
                vl_errorCode := f_EPTF_DsRestAPI_Test_tp2DirectContent(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].ScStatusLED.tp, v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].ScStatusLED.Value, pl_dataValue);
                return vl_errorCode;
              }
            }
          }
        }
      }
    }
    case (c_DsRestAPI_Test_dataElementTcStart)
    {
      if(0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params,{c_DsRestAPI_Test_paramNameEntityGroup, c_DsRestAPI_Test_paramNameScenario, c_DsRestAPI_Test_paramNameTrafficCase}, pl_result)){
        for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
          if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
            for(var integer i := 0; i < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios); i := i + 1) {
              if (pl_result[1] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].Value) {
                for(var integer k := 0; k < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases); k := k + 1) {
                  if (pl_result[2] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases[k].Value) {
                    vl_errorCode := f_EPTF_DsRestAPI_Test_tp2DirectContent(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases[k].TcStart.tp, v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases[k].TcStart.Value, pl_dataValue);
                    return vl_errorCode;
                  }
                }
              }
            }
          }
        }
      }
    }
    case (c_DsRestAPI_Test_dataElementTcStop)
    {
      if(0 == f_EPTF_DsRestAPI_Test_DSgetParams(pl_params,{c_DsRestAPI_Test_paramNameEntityGroup, c_DsRestAPI_Test_paramNameScenario, c_DsRestAPI_Test_paramNameTrafficCase}, pl_result)){
        for(var integer j := 0; j < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups); j := j + 1) {
          if (pl_result[0] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Value) {
            for(var integer i := 0; i < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios); i := i + 1) {
              if (pl_result[1] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].Value) {
                for(var integer k := 0; k < sizeof(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases); k := k + 1) {
                  if (pl_result[2] == v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases[k].Value) {
                    vl_errorCode := f_EPTF_DsRestAPI_Test_tp2DirectContent(v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases[k].TcStop.tp, v_DsRestAPI_Test_DSMock.EntityGroups[j].Scenarios[i].TrafficCases[k].TcStop.Value, pl_dataValue);
                    return vl_errorCode;
                  }
                }
              }
            }
          }
        }
      }
    }
    case else {

    }
    }
  return vl_errorCode;
}

function f_EPTF_DsRestAPI_Test_DSgetParams(in EPTF_DataSource_Params pl_params,
                                         in EPTF_CharstringList pl_needed,
                                         out EPTF_CharstringList pl_result,
                                         in boolean pl_noWarning := false) runs on DsRestAPI_Test_CT return integer {
  pl_result := {};
  if(sizeof(pl_params) == sizeof(pl_needed)){
     for(var integer j := 0; j < sizeof(pl_needed); j := j + 1){
       for(var integer i := 0; i < sizeof(pl_params); i:= i + 1){
         if(pl_params[i].paramName == pl_needed[j]){
           pl_result[sizeof(pl_result)] := pl_params[i].paramValue;
         }
       }
     }
     if(sizeof(pl_result) != sizeof(pl_needed)){
       if(not pl_noWarning){
         var EPTF_CharstringList vl_error := {};
         for(var integer i := 0; i < sizeof(pl_params); i:= i + 1){
           vl_error[i] := pl_params[i].paramName;
         }
         log(%definitionId& ": Parameters are not correct: "&
         "\nReceived: "& log2str(vl_error)&
         "\nNeeded  : "& log2str(pl_needed) );
       }
       return -1;
     }
   var EPTF_CharstringList pl_res := {};
   var charstring vl_searchStr := ".";
     var integer vl_strPos := f_strstr(pl_result[0], vl_searchStr);
     if (vl_strPos != -1) {
     pl_res[0] := substr(pl_result[0], 0, vl_strPos);
       pl_res[1] := substr(pl_result[0], vl_strPos+1, lengthof(pl_result[0])-vl_strPos-1);
     pl_result := pl_res;
     }
     return 0;
  } else{
    if(not pl_noWarning){
        var EPTF_CharstringList vl_error := {};
        for(var integer i := 0; i < sizeof(pl_params); i:= i + 1){
          vl_error[i] := pl_params[i].paramName;
        }
        if(sizeof(pl_params) < sizeof(pl_needed)){
           log(%definitionId& ": Too few parameter is given: "&
           "\nReceived: "& log2str(vl_error)&
           "\nNeeded  : "& log2str(pl_needed));
        } else {
           log(%definitionId& ": Too many parameters are given: "&
           "\nReceived: "& log2str(vl_error)&
           "\nNeeded  : "& log2str(pl_needed));
        }
    }
    return -1;
  }
}

external function ef_DsRestAPI_Test_enc_Requests(in EPTF_DsRestAPI_RqWrapper pl_request) return octetstring with { extension "prototype(convert) encode(JSON) errorbehavior(ALL:WARNING)" };

external function ef_EPTF_DsRestAPI_Test_getDirContent(in charstring pl_pathOfDir) return EPTF_CharstringList;
external function ef_EPTF_DsRestAPI_Test_getJSON(in charstring pl_fileContent) return charstring;
external function ef_DsRestAPI_Test_dec_ContentList(in octetstring pl_JSONoct) return EPTF_DsRestAPI_ContentAndChildrenListWrapper with { extension "prototype(convert) decode(JSON) errorbehavior(ALL:WARNING)" }
external function ef_DsRestAPI_Test_dec_DSMock(in octetstring pl_JSONoct) return EPTF_DsRestAPI_Wrapper with { extension "prototype(convert) decode(JSON) errorbehavior(ALL:WARNING)" }
external function ef_DsRestAPI_Test_dec_Request(in octetstring pl_JSONoct) return EPTF_DsRestAPI_RqWrapper with { extension "prototype(convert) decode(JSON) errorbehavior(ALL:WARNING)" }

} // group DsRestAPI_Test_DataSourceClient
} //module
