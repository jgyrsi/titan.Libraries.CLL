///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_NameService_Test_Testcases
// 
//  Purpose:
//    This module contains the main testcases of generic EPTF Nameservice.
// 
//  Module depends on:
//   <EPTF_NameService_Test_Definitions> 
//   <EPTF_NameService_Test_Functions> 
//   <EPTF_CLL_NameService_Definitions>
//   <EPTF_CLL_NameService_Functions>
//   <EPTF_CLL_NameServiceClient_Functions> 
//   <EPTF_CLL_Base_Functions>
//   <EPTF_CLL_Base_Definitions>
//
// 
//  Current Owner:
//    Balazs Barcsik (EBALBAR)
// 
//  Last Review Date:
//    2007-xx-xx
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////////

module EPTF_NameService_Test_Testcases {

//=========================================================================
// Import Part
//=========================================================================
import from EPTF_NameService_Test_Definitions all;
import from EPTF_NameService_Test_Functions all;
import from EPTF_CLL_NameService_Definitions all;
import from EPTF_CLL_NameService_Functions all;
import from EPTF_CLL_NameServiceClient_Functions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_Base_Definitions all;


testcase tc_EPTF_NS_test() runs on EPTF_NS_Client_Test {
  var EPTF_NS_CT vl_NS_Server := EPTF_NS_CT.create;
  vl_NS_Server.start(f_EPTF_NS_main_CT("NameServiceServer"));

  var EPTF_NS_Client_Test vl_NS_Client := EPTF_NS_Client_Test.create;
  vl_NS_Client.start(f_EPTF_NS_Client_Behaviour_1("NS_Client#1",vl_NS_Server));

  // I am another client:
  f_EPTF_NS_Client_init_CT("NS_Client#2",vl_NS_Server);

  f_EPTF_NS_RegisterName("Name2",1);
  f_EPTF_NS_RegisterName("Name2",2); // try to register the same name with different id

  f_EPTF_NS_DeregisterName("Name2");

  f_EPTF_NS_RegisterName("Name3",3); // register a new name
  f_EPTF_NS_RegisterName("Name4"); // register a new name with id omit

  var EPTF_NS_QueryResp vl_queryResp;
  f_EPTF_NS_Query("Name3",vl_queryResp);
  //log("QueryResp Received: ", vl_queryResp);
  if (vl_queryResp != {
                 name := "Name3",
                 ownerComp := mtc,
                 id := 3 
                }) {
    setverdict(fail);
  }
  timer t_guard
  t_guard.start( 3.0 )
  alt
  {
    [] vl_NS_Client.done{t_guard.stop}
    [] t_guard.timeout{
      setverdict(inconc,"Component NS_Client#1 should have been terminated...")
    }
  };
  
  f_EPTF_NS_RegisterName("Name2",2); // try to register the same name after deregistration
  f_EPTF_NS_Query("Name2",vl_queryResp);
  const EPTF_NS_QueryResp c_queryResp2MTC :={
                 name := "Name2",
                 ownerComp := mtc,
                 id := 2 
                } 
  if (vl_queryResp != c_queryResp2MTC) {
    setverdict(fail,"Register after deregistration failed: ",match(vl_queryResp, c_queryResp2MTC));
  }

  f_EPTF_NS_Query("Name1",vl_queryResp);        // query item registered in another component, but that component exited
  //log("QueryResp Received: ", vl_queryResp);
  if (vl_queryResp != {
                 name := "Name1",
                 ownerComp := omit,
                 id := omit 
                }) {
    setverdict(fail);
  }
  f_EPTF_NS_Query("Name0",vl_queryResp);        // query an unregistered item
  //log("QueryResp Received: ", vl_queryResp);
  if (vl_queryResp != {
                 name := "Name0",
                 ownerComp := omit,
                 id := omit 
                }) {
    setverdict(fail);
  }
  f_EPTF_NS_Query("Name4",vl_queryResp);        // query an registered item with id omit
  //log("QueryResp Received: ", vl_queryResp);
  if (vl_queryResp != {
                 name := "Name4",
                 ownerComp := mtc,
                 id := omit 
                }) {
    setverdict(fail);
  }

  f_EPTF_NS_DeregisterName("Name2"); // deregister
  f_EPTF_NS_DeregisterName("Name2"); // try to deregister the same name after deregistration

  setverdict(pass);

   t_wait.start(2.0); t_wait.timeout;

  // NS_Client must be cleaned up explicitly, because Base_cleanup stops the other
  // components (i.e. the NS master) before calling the component cleanup fns, so
  // the NS_Client_cleanup would send a bye message via a closed port
  // resulting in dynamic testcase error.
  f_EPTF_NS_Client_cleanup_CT();

  f_EPTF_Base_cleanup_CT();
   
//  all component.done;
}

///////////////////////////////////////////////////////////
// Testcase: tc_EPTF_NS_Test_init_CT
// 
// Purpose:
//   to test f_EPTF_NS_init_CT() function
// 
// Requirement:
//   -
//
// Action:
//   - creates an EPTF_NS_CT component and starts f_EPTF_NS_main_CT function on it
//   - creates an EPTF_NS_Client_CT component and starts a test function
//
// Expected Result:
//  components stop properly after several minutes
///////////////////////////////////////////////////////////
testcase tc_EPTF_NS_Test_init_CT() runs on EPTF_NS_Client_Test 
{
 f_EPTF_Base_init_CT("tc_EPTF_NS_Test_init_CT");
  var EPTF_NS_CT vl_NS_Server := EPTF_NS_CT.create("NS_Server");
  vl_NS_Server.start(f_EPTF_NS_main_CT("NS_Server"));
  var EPTF_NS_Client_CT vl_NS_Client := EPTF_NS_Client_CT.create("NS_Client");
  vl_NS_Client.start(f_EPTF_NS_Client_Test_init_CT("NS_Client", vl_NS_Server));
  t_wait.start(2.0); 
  t_wait.timeout;
// f_EPTF_Base_stopRemote(mtc);
  f_EPTF_Base_cleanup_CT();
// all component.done;
}

///////////////////////////////////////////////////////////
// Testcase: tc_EPTF_NS_Client_Test_upcast
// 
// Purpose:
//   to test f_EPTF_NS_Client_upcast() function
// 
// Requirement:
//   -
//
// Action:
//   - creates an EPTF_NS_CT component and starts f_EPTF_NS_main_CT function on it
//   - creates an MyCT3 component
//   - creates MyCT4 component
//   - calls f_EPTF_NS_Client_upcast function
//
// Expected Result:
//  the upcast method works properly
///////////////////////////////////////////////////////////
testcase tc_EPTF_NS_Client_Test_upcast() runs on EPTF_NS_Client_Test 
{
 f_EPTF_Base_init_CT("tc_EPTF_NS_Client_Test_upcast");
  var EPTF_NS_CT vl_NS_Server := EPTF_NS_CT.create("NS_Server");
  vl_NS_Server.start(f_EPTF_NS_main_CT("NS_Server"));
  var EPTF_Base_CT vl_base;

  var MyCT3 vl_NS_Client := MyCT3.create("NS_Client");
  var MyCT4 vl_client := vl_NS_Client;
 vl_base := vl_client;

  vl_base := f_EPTF_NS_Client_upcast(vl_client);

 vl_base.start(f_EPTF_NS_Client_Test_upcast("NS_Client"));
  t_wait.start(2.0); 
  t_wait.timeout;
// f_EPTF_Base_stopRemote(mtc);
  f_EPTF_Base_cleanup_CT();
// all component.done;
}

///////////////////////////////////////////////////////////
// Testcase: tc_EPTF_NS_Client_Test_downcast
// 
// Purpose:
//   to test f_EPTF_NS_Client_downcast() function
// 
// Requirement:
//   -
//
// Action:
//   - creates an EPTF_NS_CT component and starts f_EPTF_NS_main_CT function on it
//   - creates an MyCT component
//   - creates MyCT2 component
//   - calls f_EPTF_NS_Client_downcast function with MyCT2 component reference
//
// Expected Result:
//  the downcast method works properly
///////////////////////////////////////////////////////////
testcase tc_EPTF_NS_Client_Test_downcast() runs on EPTF_NS_Client_Test 
{
 f_EPTF_Base_init_CT("tc_EPTF_NS_Client_Test_upcast");
  var EPTF_NS_CT vl_NS_Server := EPTF_NS_CT.create("NS_Server");
  vl_NS_Server.start(f_EPTF_NS_main_CT("NS_Server"));
  var MyCT vl_my := MyCT.create("Base_Client");
  var MyCT2 vl_my2 := vl_my;

  var EPTF_NS_Client_CT vl_nsClient;
 //vl_nsClient := vl_my2; // syntax error
 vl_nsClient := f_EPTF_NS_Client_downcast(vl_my2);
 setverdict(pass);
  t_wait.start(2.0); 
  t_wait.timeout;
// f_EPTF_Base_stopRemote(mtc);
  f_EPTF_Base_cleanup_CT();
// all component.done;
}


control {
  execute(tc_EPTF_NS_test());
  execute(tc_EPTF_NS_Test_init_CT());
  execute(tc_EPTF_NS_Client_Test_upcast());
  execute(tc_EPTF_NS_Client_Test_downcast());
}
} // end of module
