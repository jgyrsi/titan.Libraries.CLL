///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_NameService_Test_Functions
// 
//  Purpose:
//    This module contains the test functions of generic EPTF Nameservice.
// 
//  Module depends on:
//   <EPTF_CLL_NameServiceClient_Functions> 
//   <EPTF_CLL_NameService_Definitions> 
//   <EPTF_CLL_Base_Functions> 
//   <EPTF_CLL_Base_Definitions> 
//
// 
//  Current Owner:
//    Balazs Barcsik (EBALBAR)
// 
//  Last Review Date:
//    2007-xx-xx
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////////
module EPTF_NameService_Test_Functions {
//=========================================================================
// Import Part
//=========================================================================
import from EPTF_NameService_Test_Definitions all;
import from EPTF_CLL_NameServiceClient_Functions all;
import from EPTF_CLL_NameService_Definitions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_Base_Definitions all;
//=========================================================================
// Functions
//=========================================================================

function f_EPTF_NS_Client_Behaviour_1(in charstring pl_selfName, in EPTF_NS_CT pl_NS_ServerCompRef) runs on EPTF_NS_Client_Test {
  f_EPTF_NS_Client_init_CT(pl_selfName,pl_NS_ServerCompRef);

  f_EPTF_NS_RegisterName("Name1",1);
  var EPTF_NS_QueryResp vl_queryResp;
  t_wait.start; t_wait.timeout;
  f_EPTF_NS_Query("Name2",vl_queryResp);        // query item registered and then deregistered in another component
  log("QueryResp Received: ", vl_queryResp);
  if (vl_queryResp != {
                 name := "Name2",
                 ownerComp := omit,
                 id := omit 
                }) {
    setverdict(fail);
  }
  f_EPTF_NS_Query("Name3",vl_queryResp);        // query item registered in another component
  log("QueryResp Received: ", vl_queryResp);
  if (vl_queryResp != {
                 name := "Name3",
                 ownerComp := mtc,
                 id := 3 
                }) {
    setverdict(fail);
  }
   f_EPTF_Base_cleanup_CT();
}


function f_EPTF_NS_Client_Test_init_CT(
  in charstring pl_name, 
  in EPTF_NS_CT pl_serverRef)
runs on EPTF_NS_Client_CT 
{
  var EPTF_NS_QueryResp vl_queryResp;

  f_EPTF_NS_Client_init_CT(pl_name, pl_serverRef);
  f_EPTF_NS_RegisterName("Name1",1);

  f_EPTF_NS_Query("Name1",vl_queryResp);
  log("QueryResp Received: ", vl_queryResp);
  if (vl_queryResp != {
                 name := "Name1",
                 ownerComp := self,
                 id := 1 
                }) {
    setverdict(fail);
  }
  f_EPTF_NS_DeregisterName("Name1");
  setverdict(pass);
  f_EPTF_Base_cleanup_CT();
}

function f_EPTF_NS_Client_Test_upcast(
  in charstring pl_name)
runs on EPTF_Base_CT 
{
  f_EPTF_Base_init_CT(pl_name);
  setverdict(pass);
  f_EPTF_Base_cleanup_CT();
}

function f_EPTF_NS_Client_Test_downcast(
  in charstring pl_name, 
  in EPTF_NS_CT pl_serverRef)
runs on EPTF_NS_Client_CT 
{
  var EPTF_NS_QueryResp vl_queryResp;
 v_NS_Client_initialized := false;
  f_EPTF_NS_Client_init_CT(pl_name, pl_serverRef);
  f_EPTF_NS_RegisterName("Name1",1);
  f_EPTF_NS_DeregisterName("Name1");
  setverdict(pass);
  f_EPTF_Base_cleanup_CT();
}

}
