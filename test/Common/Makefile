#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

# The following make commands are available:
# - make, make all     Builds the executable test suite.
# - make archive       Archives all source files.
# - make check         Checks the semantics of TTCN-3 and ASN.1 modules.
# - make clean         Removes all generated files.
# - make compile       Translates TTCN-3 and ASN.1 modules to C++.
# - make dep           Creates/updates dependency list.
# - make objects       Builds the object files without linking the executable.
# - make tags          Creates/updates tags file using ctags.

#
# Set these variables...
#

# The path of your TTCN-3 Test Executor installation:
# Uncomment this line to override the environment variable.
# TTCN3_DIR =

# Your platform: (SOLARIS, SOLARIS8, LINUX, FREEBSD or WIN32)
PLATFORM = SOLARIS8

# Your C++ compiler:
CXX = g++

# Flags for the C++ preprocessor (and makedepend as well):
CPPFLAGS = -D$(PLATFORM) -I$(TTCN3_DIR)/include

# Flags for the C++ compiler:
CXXFLAGS = -Wall

# Flags for the linker:
LDFLAGS =

# Flags for the TTCN-3 and ASN.1 compiler:
COMPILER_FLAGS = -L

# Execution mode: (either ttcn3 or ttcn3-parallel)
TTCN3_LIB = ttcn3

# The path of your OpenSSL installation:
# If you do not have your own one, leave it unchanged.
OPENSSL_DIR = $(TTCN3_DIR)

# Directory to store the archived source files:
# Note: you can set any directory except ./archive
ARCHIVE_DIR = backup

#
# You may change these variables. Add your files if necessary...
#

# TTCN-3 modules of this project:
TTCN3_MODULES = RndValues_Demo.ttcn EPTF_CLL_Common_RndFunctions.ttcn EPTF_CLL_Common_RndDefinitions.ttcn EPTF_CLL_Common_Definitions.ttcn

# ASN.1 modules of this project:
ASN1_MODULES =

# C++ source & header files generated from the TTCN-3 & ASN.1 modules of
# this project:
GENERATED_SOURCES = RndValues_Demo.cc EPTF_Common_RndValues_Functions.cc EPTF_Common_RndValues_Definitions.cc EPTF_CLL_Definitions.cc
GENERATED_HEADERS = RndValues_Demo.hh EPTF_Common_RndValues_Functions.hh EPTF_Common_RndValues_Definitions.hh EPTF_CLL_Definitions.hh

# C/C++ Source & header files of Test Ports, external functions and
# other modules:
USER_SOURCES =
USER_HEADERS =

# Object files of this project that are needed for the executable test suite:
OBJECTS = RndValues_Demo.o EPTF_Common_RndValues_Functions.o EPTF_Common_RndValues_Definitions.o EPTF_CLL_Definitions.o

# Other files of the project (Makefile, configuration files, etc.)
# that will be added to the archived source files:
OTHER_FILES = ../RndValues_Demo.cfg Makefile

# The name of the executable test suite:
TARGET = RndValues_Demo

#
# Do not modify these unless you know what you are doing...
# Platform specific additional libraries:
#
SOLARIS_LIBS = -lsocket -lnsl
SOLARIS8_LIBS = -lsocket -lnsl
LINUX_LIBS =
FREEBSD_LIBS =
WIN32_LIBS =

#
# Rules for building the executable...
#

all: $(TARGET) ;

objects: $(OBJECTS) ;

$(TARGET): $(OBJECTS)
	$(CXX) $(LDFLAGS) -o $@ $(OBJECTS) \
	-L$(TTCN3_DIR)/lib -l$(TTCN3_LIB) \
	-L$(OPENSSL_DIR)/lib -lcrypto $($(PLATFORM)_LIBS)

.cc.o .c.o:
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) -o $@ $<

$(GENERATED_SOURCES) $(GENERATED_HEADERS): compile
	@if [ ! -f $@ ]; then rm -f compile; $(MAKE) compile; fi

check: $(TTCN3_MODULES) $(ASN1_MODULES)
	$(TTCN3_DIR)/bin/compiler -s $(COMPILER_FLAGS) \
	$(TTCN3_MODULES) $(PREPROCESSED_TTCN3_MODULES) $(ASN1_MODULES)

compile: $(TTCN3_MODULES) $(ASN1_MODULES)
	$(TTCN3_DIR)/bin/compiler $(COMPILER_FLAGS) \
	$(TTCN3_MODULES) $(ASN1_MODULES) - $?
	touch $@

browserdata.dat: $(TTCN3_MODULES) $(ASN1_MODULES)
	$(TTCN3_DIR)/bin/compiler -B -s $(COMPILER_FLAGS) \
	$(TTCN3_MODULES) $(ASN1_MODULES)

tags: $(TTCN3_MODULES) $(ASN1_MODULES) \
$(USER_HEADERS) $(USER_SOURCES)
	$(TTCN3_DIR)/bin/ctags_ttcn3 --line-directives=yes \
	$(TTCN3_MODULES) $(ASN1_MODULES) \
	$(USER_HEADERS) $(USER_SOURCES)

clean:
	-rm -f $(TARGET) $(OBJECTS) $(GENERATED_HEADERS) \
	$(GENERATED_SOURCES) compile \
	browserdata.dat tags *.log

dep: $(GENERATED_SOURCES) $(USER_SOURCES)
	makedepend $(CPPFLAGS) $(GENERATED_SOURCES) $(USER_SOURCES)

archive:
	mkdir -p $(ARCHIVE_DIR)
	tar -cvhf - $(TTCN3_MODULES) $(ASN1_MODULES) \
	$(USER_HEADERS) $(USER_SOURCES) $(OTHER_FILES) \
	| gzip >$(ARCHIVE_DIR)/`basename $(TARGET) .exe`-`date '+%y%m%d-%H%M'`.tgz

#
# Add your rules here if necessary...
#

