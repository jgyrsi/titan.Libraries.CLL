///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: RendezvousServer_test
// 
//  Purpose:
//    Creates and manages a RendezvousServer to provide synchronization among various entities.
// 
///////////////////////////////////////////////////////////

module EPTF_Rendezvous_test
{

//=========================================================================
// Import Part
//=========================================================================

import from EPTF_CLL_RendezvousClient_Functions all;
import from EPTF_CLL_Rendezvous_Functions all;
import from EPTF_CLL_Rendezvous_Definitions all;

import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_Semaphore_Functions all;

//=========================================================================
//Component Types
//========================================================================

type component SYSTEM_CT{
}

type component MAIN_CT extends EPTF_Base_CT{
}

type component Client_CT extends EPTF_RendezvousClient_CT{
}

//=========================================================================
//Functions
//=========================================================================

function f_RendezvousClient_Behaviour(in charstring pl_selfName, in
  EPTF_Rendezvous_CT pl_server) runs on Client_CT{
  log("----- Client Behaviour START -------");
  f_EPTF_RendezvousClient_init_CT(pl_selfName,pl_server);

  var integer vl_Idx1,vl_Idx2;
  vl_Idx1:=f_EPTF_RendezvousClient_StartWaitForATrigger(10);
  vl_Idx2:=f_EPTF_RendezvousClient_StartWaitForNTrigger(11,3);

  if(f_EPTF_RendezvousClient_WaitForResponse(vl_Idx2))
  {
    log("---Trigger received---");
  }
  
  if(f_EPTF_RendezvousClient_WaitForResponse(vl_Idx1))
  {
    log("---Trigger received---");
  }
  
  setverdict(pass); 
  log("----- Client Behaviour stop -------");
  f_EPTF_Base_cleanup_CT(); 
}

function f_RendezvousClient_Behaviour2(in charstring pl_selfName, in
  EPTF_Rendezvous_CT pl_server) runs on Client_CT{
  log("----- Client Behaviour START -------");
  f_EPTF_RendezvousClient_init_CT(pl_selfName,pl_server);

  // create a semaphore to confuse the rendezvous server:
  var integer vl_dummySemaphore := f_EPTF_Semaphore_new();
  
  var integer vl_Idx1,vl_Idx2;
  vl_Idx1:=f_EPTF_RendezvousClient_StartWaitForATrigger(10);
  vl_Idx2:=f_EPTF_RendezvousClient_StartWaitForNTrigger(11,2);

  if(f_EPTF_RendezvousClient_WaitForResponse(vl_Idx2))
  {
    log("---Trigger received---");
  }
  

  if(f_EPTF_RendezvousClient_WaitForResponse(vl_Idx1))
  {
    log("---Trigger received---");
  }
  
  setverdict(pass); 
  log("----- Client Behaviour stop -------");
  f_EPTF_Base_stopAll(pass); 
}

function f_RendezvousClient_Behaviour3(in charstring pl_selfName, in
  EPTF_Rendezvous_CT pl_server) runs on Client_CT{
  log("----- Client Behaviour START -------");
  f_EPTF_RendezvousClient_init_CT(pl_selfName,pl_server);
  
  timer t_wait := 0.5;
  t_wait.start; t_wait.timeout; // makes sure that this component starts slowest

  
  var integer vl_Idx1,vl_Idx2;
  vl_Idx1:=f_EPTF_RendezvousClient_StartWaitForATrigger(10);
  vl_Idx2:=f_EPTF_RendezvousClient_StartWaitForNTrigger(11,2);

  if(f_EPTF_RendezvousClient_WaitForResponse(vl_Idx2))
  {
    log("---Trigger received---");
  }
  

  if(f_EPTF_RendezvousClient_WaitForResponse(vl_Idx1))
  {
    log("---Trigger received---");
  }
  
  setverdict(pass); 
  log("----- Client Behaviour stop -------");
  f_EPTF_Base_cleanup_CT(); 
}



function f_RendezvousClient_WaitForATrigger_Behaviour1(in charstring pl_selfName, in
  EPTF_Rendezvous_CT pl_server) runs on Client_CT{
  log("----- Client Behaviour START -------");
  f_EPTF_RendezvousClient_init_CT(pl_selfName,pl_server);
    
  var integer vl_Idx;
  vl_Idx:=f_EPTF_RendezvousClient_StartWaitForATrigger(10);

  if(f_EPTF_RendezvousClient_WaitForResponse(vl_Idx))
  {
    log("---Trigger received---");
  }
  
  setverdict(pass); 
  log("----- Client Behaviour stop -------");
  f_EPTF_Base_cleanup_CT(); 
}


function f_RendezvousClient_WaitForATrigger_Behaviour2(in charstring pl_selfName, in
  EPTF_Rendezvous_CT pl_server) runs on Client_CT{
  log("----- Client Behaviour START -------");
  f_EPTF_RendezvousClient_init_CT(pl_selfName,pl_server);

  var EPTF_IntegerList vl_IdxList;
  for (var integer i:=0;i<100;i:=i+1){
    vl_IdxList[i]:=f_EPTF_RendezvousClient_StartWaitForATrigger(i);
  }

  for (var integer j:=0;j<100;j:=j+1){
    if(f_EPTF_RendezvousClient_WaitForResponse(vl_IdxList[j]))
    {
      log("---Trigger received---");
    }
  }

  setverdict(pass); 
  log("----- Client Behaviour stop -------");
  f_EPTF_Base_cleanup_CT(); 
}

function f_RendezvousClient_WaitForNTrigger_Behaviour1(in charstring pl_selfName, in
  EPTF_Rendezvous_CT pl_server) runs on Client_CT{
  log("----- Client Behaviour START -------");
  f_EPTF_RendezvousClient_init_CT(pl_selfName,pl_server);

  var integer vl_Idx;
  vl_Idx:=f_EPTF_RendezvousClient_StartWaitForNTrigger(10,100);

  if(f_EPTF_RendezvousClient_WaitForResponse(vl_Idx))
  {
    log("---Trigger received---");
  }

  setverdict(pass); 
  log("----- Client Behaviour stop -------");
  f_EPTF_Base_cleanup_CT(); 
}

function f_RendezvousClient_WaitForNTrigger_Behaviour2(in charstring pl_selfName, in
  EPTF_Rendezvous_CT pl_server) runs on Client_CT{
  log("----- Client Behaviour START -------");
  f_EPTF_RendezvousClient_init_CT(pl_selfName,pl_server);
    
  var EPTF_IntegerList vl_IdxList;
  for (var integer i:=0;i<100;i:=i+1){
    vl_IdxList[i]:=f_EPTF_RendezvousClient_StartWaitForNTrigger(i,3);
  }

  for (var integer j:=0;j<100;j:=j+1){
    if(f_EPTF_RendezvousClient_WaitForResponse(vl_IdxList[j]))
    {
      log("---Trigger received---");
    }
  }
  
  setverdict(pass); 
  log("----- Client Behaviour stop -------");
  f_EPTF_Base_cleanup_CT(); 
}


function f_RendezvousServer_behaviour(charstring pl_selfName) runs on EPTF_Rendezvous_CT {
  log("----- RendezvousServer START -------");
  f_EPTF_Rendezvous_init_CT(pl_selfName);

  f_EPTF_Base_wait4Shutdown();
  f_EPTF_Base_cleanup_CT();
  log("----- RendezvousServer shutdown -------");  
}

//=========================================================================
// Testcases
//=========================================================================

//=========================================================================
// Testcase: tc_EPTF_Rendezvous_BaseTest
//=========================================================================

testcase tc_EPTF_Rendezvous_BaseTest() runs on MAIN_CT system SYSTEM_CT{

  //Server
  var EPTF_Rendezvous_CT ct_server := EPTF_Rendezvous_CT.create;

  //Starting Server
  ct_server.start(f_RendezvousServer_behaviour("RendezvousServer"));

  //Client1
  var Client_CT ct_client1 := Client_CT.create;
  //Starting Client
  ct_client1.start(f_RendezvousClient_Behaviour("RendezvousClient1", ct_server));

  //Client2
  var Client_CT ct_client2 := Client_CT.create;
  //Starting Client
  ct_client2.start(f_RendezvousClient_Behaviour("RendezvousClient2", ct_server));

  //Client3
  var Client_CT ct_client3 := Client_CT.create;
  //Starting Client
  ct_client3.start(f_RendezvousClient_Behaviour("RendezvousClient3", ct_server));

  timer T_main := 10.0;
  T_main.start;
  alt {
    [] ct_server.done {}
    [] T_main.timeout{}
  }
  //all component.done;  
  f_EPTF_Base_cleanup_CT();

}

//=========================================================================
// Testcase: tc_EPTF_Rendezvous_BaseTestWithSemaphore
//=========================================================================

testcase tc_EPTF_Rendezvous_BaseTestWithSemaphore() runs on MAIN_CT system SYSTEM_CT{

  f_EPTF_Base_init_CT("MTC");
  //Server
  var EPTF_Rendezvous_CT ct_server := EPTF_Rendezvous_CT.create;

  //Starting Server
  ct_server.start(f_RendezvousServer_behaviour("RendezvousServer"));

  //Client1
  var Client_CT ct_client1 := Client_CT.create;
  //Starting Client
  ct_client1.start(f_RendezvousClient_Behaviour2("RendezvousClient1", ct_server));

  //Client2
  var Client_CT ct_client2 := Client_CT.create;
  //Starting Client
  ct_client2.start(f_RendezvousClient_Behaviour3("RendezvousClient2", ct_server));


  timer T_main := 10.0;
  T_main.start;
  alt {
    [] ct_server.done {}
    [] T_main.timeout{
      setverdict(fail);
    }
  }
  //all component.done;  
  f_EPTF_Base_cleanup_CT();

}

//=========================================================================
// Testcase: tc_EPTF_Rendezvous_WaitForATriggerWith100Clients
//=========================================================================

testcase tc_EPTF_Rendezvous_WaitForATriggerWith100Clients() runs on MAIN_CT system SYSTEM_CT{

  //Server
  var EPTF_Rendezvous_CT ct_server := EPTF_Rendezvous_CT.create;

  //Starting Server
  ct_server.start(f_RendezvousServer_behaviour("RendezvousServer"));

  //Starting Clients
  for(var integer i:=1;i<=100;i:=i+1)
  {
    var Client_CT ct_client := Client_CT.create;
    //Starting Client
    ct_client.start(f_RendezvousClient_WaitForATrigger_Behaviour1("RendezvousClient"&int2str(i), ct_server));
  }

  timer T_main := 10.0;
  T_main.start;
  alt {
    [] ct_server.done {}
    [] T_main.timeout{}
  }
  //all component.done; 
  f_EPTF_Base_cleanup_CT();  
}

//=========================================================================
// Testcase: tc_EPTF_Rendezvous_WaitForATriggerWith100Rendezvous
//=========================================================================

testcase tc_EPTF_Rendezvous_WaitForATriggerWith100Rendezvous() runs on MAIN_CT system SYSTEM_CT{

  //Server
  var EPTF_Rendezvous_CT ct_server := EPTF_Rendezvous_CT.create;

  //Starting Server
  ct_server.start(f_RendezvousServer_behaviour("RendezvousServer"));

  //Client1
  var Client_CT ct_client1 := Client_CT.create;
  //Starting Client
  ct_client1.start(f_RendezvousClient_WaitForATrigger_Behaviour2("RendezvousClient1", ct_server));

  //Client2
  var Client_CT ct_client2 := Client_CT.create;
  //Starting Client
  ct_client2.start(f_RendezvousClient_WaitForATrigger_Behaviour2("RendezvousClient2", ct_server));


  timer T_main := 10.0;
  T_main.start;
  alt {
    [] ct_server.done {}
    [] T_main.timeout{}
  }
  //all component.done; 
  f_EPTF_Base_cleanup_CT();  
}

//=========================================================================
// Testcase: tc_EPTF_Rendezvous_WaitForNTriggerWith100Clients
//=========================================================================

testcase tc_EPTF_Rendezvous_WaitForNTriggerWith100Clients() runs on MAIN_CT system SYSTEM_CT{

  //Server
  var EPTF_Rendezvous_CT ct_server := EPTF_Rendezvous_CT.create;

  //Starting Server
  ct_server.start(f_RendezvousServer_behaviour("RendezvousServer"));

  //Starting Clients
  for(var integer i:=1;i<=100;i:=i+1)
  {
    var Client_CT ct_client := Client_CT.create;
    //Starting Client
    ct_client.start(f_RendezvousClient_WaitForNTrigger_Behaviour1("RendezvousClient"&int2str(i), ct_server));
  }

  timer T_main := 10.0;
  T_main.start;
  alt {
    [] ct_server.done {}
    [] T_main.timeout{}
  }
  //all component.done; 
  f_EPTF_Base_cleanup_CT();  
}

//=========================================================================
// Testcase: tc_EPTF_Rendezvous_WaitForNTriggerWith100Rendezvous
//=========================================================================

testcase tc_EPTF_Rendezvous_WaitForNTriggerWith100Rendezvous() runs on MAIN_CT system SYSTEM_CT{

  //Server
  var EPTF_Rendezvous_CT ct_server := EPTF_Rendezvous_CT.create;

  //Starting Server
  ct_server.start(f_RendezvousServer_behaviour("RendezvousServer"));

  //Starting 3 Clients
  for(var integer i:=1;i<=3;i:=i+1)
  {
    var Client_CT ct_client := Client_CT.create;
    //Starting Client
    ct_client.start(f_RendezvousClient_WaitForNTrigger_Behaviour2("RendezvousClient"&int2str(i), ct_server));
  }

  timer T_main := 10.0;
  T_main.start;
  alt {
    [] ct_server.done {}
    [] T_main.timeout{}
  }
  //all component.done; 
  f_EPTF_Base_cleanup_CT();  
}


control
{
  execute(tc_EPTF_Rendezvous_BaseTest());
  execute(tc_EPTF_Rendezvous_BaseTestWithSemaphore());
  execute(tc_EPTF_Rendezvous_WaitForATriggerWith100Clients());
  execute(tc_EPTF_Rendezvous_WaitForATriggerWith100Rendezvous());
  execute(tc_EPTF_Rendezvous_WaitForNTriggerWith100Clients());
  execute(tc_EPTF_Rendezvous_WaitForNTriggerWith100Rendezvous());
}

}  // end of module
