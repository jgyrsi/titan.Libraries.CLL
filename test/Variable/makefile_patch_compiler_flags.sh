#!/bin/bash
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                //
#///////////////////////////////////////////////////////////////////////////////

# if -e or -N compiler flag is supported, it is needed

TTCN_COMPILER_SUPPORTS_E="$(compiler --help 2>&1 | grep "\-e:")"
TTCN_COMPILER_SUPPORTS_N="$(compiler --help 2>&1 | grep "\-N:")"
SED_CMD=""
if [[ "$TTCN_COMPILER_SUPPORTS_E" != "" ]]
then
   # only add if not already there:
   SED_CMD=$(printf "$SED_CMD\n"'/-e/!s/COMPILER_FLAGS =/COMPILER_FLAGS = -e/g')
fi
if [[ "$TTCN_COMPILER_SUPPORTS_N" != "" ]]
then
   # only add if not already there:
   SED_CMD=$(printf "$SED_CMD\n"'/-N/!s/COMPILER_FLAGS =/COMPILER_FLAGS = -N/g')
fi

sed -e "$SED_CMD"
