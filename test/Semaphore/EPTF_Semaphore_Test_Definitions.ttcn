///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_Semaphore_Test_Definitions
// 
//  Purpose:
//    This module contains the type definitions of generic EPTF Semaphore.
// 
//  Module depends on:
//   <EPTF_CLL_Semaphore_Definitions> 
//
// 
//  Current Owner:
//    Balazs Barcsik (EBALBAR)
// 
//  Last Review Date:
//    2007-xx-xx
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////////
module EPTF_Semaphore_Test_Definitions {
//=========================================================================
// Import Part
//=========================================================================
import from EPTF_CLL_Semaphore_Definitions all;

//=========================================================================
// Type Component
//=========================================================================
type component EPTF_Semaphore_Test_CT extends EPTF_Semaphore_CT {
  timer t_lock := 2.0;
  timer t_update := 0.01; // to generate events
  var boolean blockingFlag := false;
  var integer v_semaphoreIdx; // will be unlocked
  var integer v_semaphoreIdx2; // will not be unlocked
}
}
