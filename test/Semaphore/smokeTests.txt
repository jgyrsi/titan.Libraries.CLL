///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
EPTF_Semaphore_Test_Testcases.tc_Semaphore_basicTest
EPTF_Semaphore_Test_Testcases.tc_EPTF_Semaphore_Test_init_CT
EPTF_Semaphore_Test_Testcases.tc_EPTF_Semaphore_Test_new
EPTF_Semaphore_Test_Testcases.tc_EPTF_Semaphore_Test_delete
EPTF_Semaphore_Test_Testcases.tc_EPTF_Semaphore_Test_lock
EPTF_Semaphore_Test_Testcases.tc_EPTF_Semaphore_Test_unlock
EPTF_Semaphore_Test_Testcases.tc_EPTF_Semaphore_Test_multiLockMultiUnlock
EPTF_Semaphore_Test_Testcases.tc_EPTF_Semaphore_Test_multiLockUnlock
EPTF_Semaphore_Test_Testcases.tc_EPTF_Semaphore_Test_waitForUnlockNoDelete
EPTF_Semaphore_Test_Testcases.tc_EPTF_Semaphore_NEG_Test_waitForUnlockNoDelete
EPTF_Semaphore_Test_Testcases.tc_EPTF_Semaphore_NEG_Test_useInvalidSemaphoreInWaitForUnlock
EPTF_Semaphore_Test_Testcases.tc_EPTF_Semaphore_NEG_Test_useInvalidSemaphoreInUnlock
EPTF_Semaphore_Test_Testcases.tc_EPTF_Semaphore_NEG_Test_useInvalidSemaphoreInWaitForIncreaseLock
