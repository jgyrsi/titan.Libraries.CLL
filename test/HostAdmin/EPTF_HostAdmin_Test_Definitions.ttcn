///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_HostAdmin_Test_Definitions
//
// Purpose:
//   This module contains type definitions for testing EPTF Load measurement
//
// Module Parameters:
//    tsp_demoInterval - *float* - interval of testing, default: 30.0
//    tsp_demoRefreshPeriod - *float* - local-measurement refresh period, default: 2.0
//
//  Module depends on:
//    <EPTF_CLL_HostAdmin_Definitions>
//    <EPTF_CLL_HostAdmin_BaseDefinitions>
//    <EPTF_CLL_HostAdmin_BaseFunctions>
//    <EPTF_CLL_Base_Definitions>
//    <EPTF_CLL_Variable_Definitions>
//    <EPTF_CLL_Common_Definitions>
// 
// Current Owner:
//    Balazs Barcsik (ebalbar)
//
// Last Review Date:
//    2008-02-14
//
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////
module EPTF_HostAdmin_Test_Definitions {

import from EPTF_CLL_HostAdmin_Definitions all;
import from EPTF_CLL_HostAdmin_BaseDefinitions all;
import from EPTF_CLL_HostAdmin_BaseFunctions all;
import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_Variable_Definitions all;
import from EPTF_CLL_Common_Definitions all;

type component EPTF_HostAdmin_Test_CT extends EPTF_Var_CT
{
  var EPTF_HostAdmin_CT hostAdm;

  var integer v_testerHostLoad_k := -1;
  var integer v_numCPUs_k := -1;
  var EPTF_IntegerList v_CPULoads_k := {};
  var integer v_physicalMemory_k := -1;
  var integer v_freeMemory_k := -1;
  var integer v_freeMemoryCalled := 0;
  var integer v_hostLoadCalled := 0;
  var integer v_CPUloadCalled := 0;
  
  timer t_test := tsp_EPTF_HostAdmin_loadMeasurementRefreshTime * (int2float(c_called)+0.1);
}

type component EPTF_HostAdmin_Base_Test_CT extends EPTF_HostAdmin_Base_CT, EPTF_Base_CT
{
}

const charstring c_hostAdmin_name := "HostAdmin_test"
const integer c_called := 2;


}
