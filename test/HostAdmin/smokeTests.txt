///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_LocalMeasure
EPTF_HostAdmin_Test_Testcases.tc_EPTF_HostAdmin_Test_behavior
EPTF_HostAdmin_Test_Testcases.tc_EPTF_HostAdmin_Base_Test_InitMeasInterface
EPTF_HostAdmin_Test_Testcases.tc_EPTF_HostAdmin_Base_Test_CleanupMeasInterface
EPTF_HostAdmin_Test_Testcases.tc_EPTF_HostAdmin_Base_Test_update
EPTF_HostAdmin_Test_Testcases.tc_EPTF_HostAdmin_dataSource_test
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongIteratorProcesses
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementHostCPULoad
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementProcName1
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementProcName2
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementProcCPULoad1
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementProcCPULoad2
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementProcMEMUsage1
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementProcMEMUsage2
  
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongIteratorCPUs
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementCPULoad1
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementCPULoad2
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementHostNumCPUs
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementHostPhysicalMemory
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementHostFreeMemory
  
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementHostName
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementIPv4Addr
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementIPv6Addr
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementMinCPULoad
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementMaxCPULoad
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementAvgCPULoad
EPTF_HostAdmin_Test_Testcases.tc_HostAdmin_DS_Neg_WrongDataElementResetMinMaxAvgCPULoad 
   
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DataTestTroughVariables
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DataTestTroughVariables_WithGUI
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DataSourceTest
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongIteratorProcessSortOptionList
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongIteratorHosts
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongIteratorProcessNames
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongIteratorProcessesOnHost1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongIteratorProcessesOnHost2
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongIteratorProcessesOnHost3
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongIteratorProcessesOnHost4    
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementProcessSortOption
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementNofHosts
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPhysicalMem1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPhysicalMem2
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPhysicalMem3
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPhysicalMem4    
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPhysicalMemHRF1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPhysicalMemHRF2
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPhysicalMemHRF3
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPhysicalMemHRF4    
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementFreeMem1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementFreeMem2
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementFreeMem3
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementFreeMem4
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementFreeMemHRF1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementFreeMemHRF2
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementFreeMemHRF3
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementFreeMemHRF4
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongIteratorCpus1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongIteratorCpus2
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongIteratorCpus3
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongIteratorCpus4
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementCpuLoad1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementCpuLoad2
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementCpuLoad3
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementCpuLoad4  
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPerCpuLoad1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPerCpuLoad2
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPerCpuLoad3
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPerCpuLoad4
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPerCpuLoad5
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPerCpuLoad6    
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementSelfName1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementSelfName2
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementSelfName3  
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementSelfName4
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementSelfName5
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementSelfName6
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementSelfName7    
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementHostName1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementHostName2  
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementHostName3
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementHostName4
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementHostName5    
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPID1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPID2
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPID3
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPID4
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementPID5      
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementMemUsage1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementMemUsage2
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementMemUsage3
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementMemUsage4
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementMemUsage5        
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementMemUsageHRF1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementMemUsageHRF2
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementMemUsageHRF3
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementMemUsageHRF4
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementMemUsageHRF5  
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementIPv4Addr1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementIPv4Addr2
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementIPv4Addr3
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementIPv4Addr4
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementIPv6Addr1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementIPv6Addr2
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementIPv6Addr3
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementIPv6Addr4
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementCpuMin1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementCpuMin2
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementCpuMin3
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementCpuMin4
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementCpuMax1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementCpuMax2
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementCpuMax3
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementCpuMax4
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementCpuAvg1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementCpuAvg2
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementCpuAvg3
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementCpuAvg4
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementResetMinMaxAvgCPULoad1
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementResetMinMaxAvgCPULoad2
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementResetMinMaxAvgCPULoad3
EPTF_HostAdmin_Test_Testcases.tc_HostAdminServer_DS_Neg_WrongDataElementResetMinMaxAvgCPULoad4
