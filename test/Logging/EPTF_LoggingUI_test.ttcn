///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: LoggingUI_test
// 
//  Purpose:
//    Test module for LoggingUI feature testing.
// 
///////////////////////////////////////////////////////////

module LoggingUI_test
{

//=========================================================================
// Import Part
//=========================================================================

// if you use EPTF_Generic_Logger_CT you have to import these
import from EPTF_CLL_Logging_Definitions all;
import from EPTF_CLL_Logging_Functions all;

import from EPTF_CLL_LoggingUI_Definitions all;
import from EPTF_CLL_LoggingUI_Functions all;
import from EPTF_CLL_LoggingUIClient_Functions all;

import from EPTF_CLL_UIHandlerClient_Definitions all;
import from EPTF_CLL_UIHandlerClient_Functions all;
import from EPTF_CLL_UIHandler_Definitions all;
import from EPTF_CLL_UIHandler_WidgetFunctions all;
import from EPTF_CLL_UIHandler_MsgDefinitions all;
import from EPTF_CLL_UIHandler_XULFunctions all;
import from EPTF_CLL_Variable_Definitions all;
import from EPTF_CLL_Variable_Functions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_HashMapStr2Int_Functions all;
import from ttcn_ericsson_se_protocolModules_xtdp_xtdl all;

//=========================================================================
//Component Types
//=========================================================================

type component SYSTEM_CT{
//  port XTDPasp_PT EPTF_guiPortIf;
}

type component Server_CT extends EPTF_UIHandler_CT, EPTF_LoggingUI_CT{
}

type component MAIN_CT extends EPTF_Base_CT {
}

//=========================================================================
//Templates
//=========================================================================

// template ASP_XTDP tr_LoggingUI_ASP_XTDP_exitTTCNButtonPressed :=
// {
//   client_id := ?,
//   data := {
//     noNamespaceSchemaLocation := *,
//     xtdp_Message := {
//       xtdp_Requests := {
//         { requestId := ?,
//           widget := { widgetId := "EPTF_exit_ttcn_button", widgetType := ? },
//           action_Field := xtdp_put,
//           argument := ?,
//           argumentList := *,
//           timeout_Field := * 
//         }
//       }
//     }
//   }
// }

//=========================================================================
//Functions
//=========================================================================

function f_EPTF_LoggingUITest_createHBox(in charstring pl_myWidgetID, out Widgets pl_xul)
{
  pl_xul := {
    hbox := {
	customclass   := omit,
	disabled      := omit,
	disabledongui := omit,
	fixedposition := omit,
	flex          := omit,
	id            := pl_myWidgetID,
	orientation   := vertical,
	scrollable    := omit,
	embeddedwidgets := {embeddedwidget_list := {}}
    }
  }
}

function f_EPTF_LoggingUITest_createMainWidgets(in charstring pl_myWidgetID, out Widgets pl_xul)
{
  pl_xul :={
   hbox   := {
    customclass   := omit,
    disabled      := omit,
    disabledongui := omit,
    fixedposition := omit,
    flex          := omit,
    id            := pl_myWidgetID,
    orientation   := horizontal,
    scrollable    := omit,
    embeddedwidgets := { embeddedwidget_list := {
      {
        hbox := {
	  customclass   := omit,
	  disabled      := omit,
	  disabledongui := omit,
	  fixedposition := omit,
	  flex          := omit,
	  id            := "EPTF_Main_hbox_labels",
	  orientation   := vertical,
	  scrollable    := omit,
	  embeddedwidgets := {embeddedwidget_list := {
            {
              label_ := {
	        customclass    := omit,
	        disabled       := omit,
	        disabledongui  := omit,
	        flex           := omit,
	        id             := "status",
	        style          := omit,
	        tooltiptext    := omit,
	        value_         := "This text will be replaced runtime.",
	        externaldata    := omit
             }
            }
          }}
        }
      },
      {
        hbox := {
	  customclass   := omit,
	  disabled      := omit,
	  disabledongui := omit,
	  fixedposition := omit,
	  flex          := omit,
	  id            := "EPTF_Main_hbox_buttons",
	  orientation   := horizontal,
	  scrollable    := omit,
	  embeddedwidgets := {embeddedwidget_list := {
            {
              spacer    :=  {
                flex := 1.0,
                id   := "spacer"
              }
            },
            {
              button    := {
	        checked        := omit,
	        customclass    := omit,
	        disabled       := false,
	        disabledongui  := omit,
	        flex           := omit,
	        id             := "EPTF_exit_ttcn_button",
	        imageid        := omit,
	        label_         := "Exit TTCN",
	        tooltiptext    := omit,
	        type_          := omit,
	        externaldata   := omit
              }
            }
          }}
        }
      }
    }}
  }
  }
}

function f_EPTF_LoggingUITest_createTabBox(in charstring pl_myWidgetID, out Widgets pl_xul)
{
  pl_xul :={
    tabpage := {
      customclass     := omit,
      disabledongui   := omit,
      id              := pl_myWidgetID,
      label_          := pl_myWidgetID,
      maxheight       := omit,
      orientation     := omit,
      tooltiptext     := omit,
      embeddedwidgets := {embeddedwidget_list := {}}
    }
  }
}


function f_EPTF_LoggingUIClient1_Behaviour(
  in charstring pl_selfName, 
  in EPTF_LoggingUI_CT pl_loggingUI_main,
  in EPTF_UIHandler_CT pl_subscriber) runs on EPTF_LoggingUIClient_CT{

  f_EPTF_LoggingUIClient_init_CT(pl_selfName, pl_loggingUI_main, pl_subscriber);
  f_EPTF_Logging_debug(true,"----- LoggingUIClient START -------");

  var integer v_logMaskIdx;
  v_logMaskIdx := f_EPTF_Logging_registerComponentMasks("EPTF_SomeClient", {"Error", "Warning", "Debug"});

  f_EPTF_Logging_log(true,v_logMaskIdx,{2},"----- ComponentMask registered -------");

  timer t_waitProv1 := 3.0;

  t_waitProv1.start;
  alt {
    [] t_waitProv1.timeout{

        f_EPTF_Logging_error(true,"--EPTF_Logging Mask:0 (Error)--");
        f_EPTF_Logging_warning(true,"--EPTF_Logging Mask:1 (Warning)--");
        f_EPTF_Logging_debug(true,"--EPTF_Logging Mask:2 (Debug)--");
        f_EPTF_Logging_debugM(true,"--EPTF_Logging Mask:3 (DebugM)--");
        f_EPTF_Logging_debugV(true,"--EPTF_Logging Mask:4 (DebugV)--");
        f_EPTF_Logging_log(true,v_logMaskIdx,{0},"--EPTF_SomeClient Mask:0 (Error)--");
        f_EPTF_Logging_log(true,v_logMaskIdx,{1},"--EPTF_SomeClient Mask:1 (Warning)--");
        f_EPTF_Logging_log(true,v_logMaskIdx,{2},"--EPTF_SomeClient Mask:2 (Debug)--");       

        t_waitProv1.start;repeat;
    }
  };
  
  f_EPTF_Logging_debug(true,"----- LoggingUIClient shutdown -------");
  f_EPTF_Base_cleanup_CT(); 
}

function f_EPTF_LoggingUIClient2_Behaviour(
  in charstring pl_selfName, 
  in EPTF_LoggingUI_CT pl_loggingUI_main,
  in EPTF_UIHandler_CT pl_subscriber) runs on EPTF_LoggingUIClient_CT{

  f_EPTF_LoggingUIClient_init_CT(pl_selfName, pl_loggingUI_main, pl_subscriber);
  f_EPTF_Logging_debug(true,"----- LoggingUIClient START -------");

  var integer v_logMaskIdx;
  v_logMaskIdx := f_EPTF_Logging_registerComponentMasks("EPTF_SomeClient2", {"Error", "Debug"});
  f_EPTF_Logging_log(true,v_logMaskIdx,{1},"----- ComponentMask registered -------");


  timer t_waitProv1 := 3.0;

  t_waitProv1.start;
  alt {
    [] t_waitProv1.timeout{

        f_EPTF_Logging_error(true,"--EPTF_Logging Mask:0 (Error)--");
        f_EPTF_Logging_warning(true,"--EPTF_Logging Mask:1 (Warning)--");
        f_EPTF_Logging_debug(true,"--EPTF_Logging Mask:2 (Debug)--");
        f_EPTF_Logging_debugM(true,"--EPTF_Logging Mask:3 (DebugM)--");
        f_EPTF_Logging_debugV(true,"--EPTF_Logging Mask:4 (DebugV)--");
        f_EPTF_Logging_log(true,v_logMaskIdx,{0},"--EPTF_SomeClient2 Mask:0 (Error)--");
        f_EPTF_Logging_log(true,v_logMaskIdx,{1},"--EPTF_SomeClient2 Mask:1 (Debug)--");       

        t_waitProv1.start;repeat;
    }
  };

  /*  timer t_waitProv1 := 3.0;
  t_waitProv1.start;
  alt {
    [] t_waitProv1.timeout{
        if(v_EPTF_Logging_ComponentMask_List[0].componentLocalMask[0])
        {
          //disabling mask 0.0 element (error,eptf core)
          f_EPTF_Logging_disableLocalMask(0,0);
          f_EPTF_LoggingUI_disableAllGlobal();
        }
        else 
        {
          //enabling mask 0.0 element (error,eptf core)
          f_EPTF_Logging_enableLocalMask(0,0);
          f_EPTF_LoggingUI_enableAllGlobal();
        }       

        t_waitProv1.start;repeat;
    }
  };*/

  f_EPTF_Logging_debug(true,"----- LoggingUIClient shutdown -------");
  f_EPTF_Base_cleanup_CT(); 
}

function f_EPTF_LoggingUIClient3_Behaviour(
  in charstring pl_selfName, 
  in EPTF_LoggingUI_CT pl_loggingUI_main,
  in EPTF_UIHandler_CT pl_subscriber) runs on EPTF_LoggingUIClient_CT{

  f_EPTF_LoggingUIClient_init_CT(pl_selfName, pl_loggingUI_main, pl_subscriber);
  f_EPTF_Logging_debug(true,"----- LoggingUIClient START -------");

  var integer v_logMaskIdx;
  //TODO the following code has error in it, the same componentmask is to be registered, but with different classes.
  //TODO The src code does not give error on it, TR HQ62119 has been written on it. 
  v_logMaskIdx := f_EPTF_Logging_registerComponentMasks("EPTF_SomeClient", {"Error", "Warning", "Debug"});
  f_EPTF_Logging_log(true,v_logMaskIdx,{1},"----- ComponentMask registered -------");

  timer t_waitProv1 := 3.0;
  t_waitProv1.start;
  alt {
    [] t_waitProv1.timeout{
        //testing global chekbox
        if(v_EPTF_Logging_allComponentGlobalEnable)
        {
          f_EPTF_LoggingUI_disableAllGlobal();
        }
        else 
        {
          f_EPTF_LoggingUI_enableAllGlobal();
        }       
        for (var integer j:=0; j<2; j:=j+1) {
          //testing component type global chekboxes
          if(v_EPTF_Logging_ComponentMask_List[j].componentGlobalEnable)
          {
            f_EPTF_LoggingUI_disableGlobal(j);
          }
          else 
          {
            f_EPTF_LoggingUI_enableGlobal(j);
          } 
          //testing component type mask chekboxes
          for (var integer k:=0; k<sizeof(v_EPTF_Logging_ComponentMask_List[j].componentGlobalMask); k:=k+1) {
            if(v_EPTF_Logging_ComponentMask_List[j].componentGlobalMask[k])
            {
              f_EPTF_LoggingUI_disableGlobalMask(j,k);
            }
            else 
            {
              f_EPTF_LoggingUI_enableGlobalMask(j,k);
            }
          }
        }

        //testing local chekbox
        if(v_EPTF_Logging_allComponentLocalEnable)
        {
          f_EPTF_Logging_disableAllLocal();
        }
        else 
        {
          f_EPTF_Logging_enableAllLocal();
        }       
        for (var integer j:=0; j<2; j:=j+1) {
          //testing component type Local chekboxes
          if(v_EPTF_Logging_ComponentMask_List[j].componentLocalEnable)
          {
            f_EPTF_Logging_disableLocal(j);
          }
          else 
          {
            f_EPTF_Logging_enableLocal(j);
          } 
          //testing component type mask chekboxes
          for (var integer k:=0; k<sizeof(v_EPTF_Logging_ComponentMask_List[j].componentLocalMask); k:=k+1) {
            if(v_EPTF_Logging_ComponentMask_List[j].componentLocalMask[k])
            {
              f_EPTF_Logging_disableLocalMask(j,k);
            }
            else 
            {
              f_EPTF_Logging_enableLocalMask(j,k);
            }
          }
        }

        //testing my chekboxes
        //testing component type global chekboxes
        if(v_EPTF_Logging_ComponentMask_List[v_logMaskIdx].componentGlobalEnable)
        {
          f_EPTF_LoggingUI_disableGlobal(v_logMaskIdx);
        }
        else 
        {
          f_EPTF_LoggingUI_enableGlobal(v_logMaskIdx);
        } 
        //testing component type mask chekboxes
        for (var integer k:=0; k<sizeof(v_EPTF_Logging_ComponentMask_List[v_logMaskIdx].componentGlobalMask); k:=k+1) {
          if(v_EPTF_Logging_ComponentMask_List[v_logMaskIdx].componentGlobalMask[k])
          {
            f_EPTF_LoggingUI_disableGlobalMask(v_logMaskIdx,k);
          }
          else 
          {
            f_EPTF_LoggingUI_enableGlobalMask(v_logMaskIdx,k);
          }
        }
        //testing component type Local chekboxes
        if(v_EPTF_Logging_ComponentMask_List[v_logMaskIdx].componentLocalEnable)
        {
          f_EPTF_Logging_disableLocal(v_logMaskIdx);
        }
        else 
        {
          f_EPTF_Logging_enableLocal(v_logMaskIdx);
        } 
        //testing component type mask chekboxes
        for (var integer k:=0; k<sizeof(v_EPTF_Logging_ComponentMask_List[v_logMaskIdx].componentLocalMask); k:=k+1) {
          if(v_EPTF_Logging_ComponentMask_List[v_logMaskIdx].componentLocalMask[k])
          {
            f_EPTF_Logging_disableLocalMask(v_logMaskIdx,k);
          }
          else 
          {
            f_EPTF_Logging_enableLocalMask(v_logMaskIdx,k);
          }
        }
        t_waitProv1.start;repeat;
    }
  };

  f_EPTF_Logging_debug(true,"----- LoggingUIClient shutdown -------");
  f_EPTF_Base_cleanup_CT(); 
}

// redefines the eventClasses differently than it was in f_EPTF_LoggingUIClient1_Behaviour. This should result in an error message in the LoggingUI component.
function f_EPTF_LoggingUIClient4_Behaviour(
  in charstring pl_selfName, 
  in EPTF_LoggingUI_CT pl_loggingUI_main,
  in EPTF_UIHandler_CT pl_subscriber) runs on EPTF_LoggingUIClient_CT{

  f_EPTF_LoggingUIClient_init_CT(pl_selfName, pl_loggingUI_main, pl_subscriber);
  f_EPTF_Logging_debug(true,"----- LoggingUIClient START -------");

  var integer v_logMaskIdx;
  //TODO the following code has error in it, the same componentmask is to be registered, but with different classes.
  //TODO The src code does not give error on it, TR HQ62119 has been written on it. 
  v_logMaskIdx := f_EPTF_Logging_registerComponentMasks("EPTF_SomeClient", {"Error", "Debug" /*, "Debug2", "Debug3", "Debug4"*/});
  f_EPTF_Logging_log(true,v_logMaskIdx,{1},"----- ComponentMask registered -------");

  f_EPTF_Base_wait4Shutdown();
}

function f_EPTF_LoggingUI_Behaviour(
  in charstring pl_selfName, 
  in EPTF_UIHandler_CT pl_subscriber,
  in charstring pl_tabboxName) runs on EPTF_LoggingUI_CT{

  log("----- LoggingUI START -------"); 
  f_EPTF_LoggingUI_init_CT(pl_selfName, pl_subscriber,pl_tabboxName)
  
  timer t_waitProv := 2.0;

  t_waitProv.start;
  alt {
    [] t_waitProv.timeout{
      t_waitProv.start;repeat;
    }
  }
  f_EPTF_Base_cleanup_CT();
  log("----- LoggingUI shutdown -------");  
}

function f_EPTF_UIHandler_Behaviour(charstring pl_selfName) runs on EPTF_UIHandler_CT{
  log("----- Subscriber START -------");
  f_EPTF_UIHandler_init_CT(pl_selfName);
  f_EPTF_UIHandler_clearGUI();

  f_EPTF_UIHandler_addWindow();

  //Creates Layout:
  var boolean isExists;
  var Widgets vl_xul;
  f_EPTF_LoggingUITest_createHBox("EPTF_Main_hbox",vl_xul);
  f_EPTF_UIHandler_XSD_addElementToGui(vl_xul, "EPTF_Main_Window",isExists);

  f_EPTF_LoggingUITest_createMainWidgets("EPTF_Main_widgets",vl_xul);
  f_EPTF_UIHandler_XSD_addElementToGui(vl_xul, "EPTF_Main_hbox",isExists);

  f_EPTF_LoggingUITest_createTabBox("EPTF_Main_tabbox",vl_xul);
  f_EPTF_UIHandler_XSD_addElementToGui(vl_xul, "EPTF_Main_Window",isExists);  

/*  alt {
    [] EPTF_guiPortIf.receive(tr_LoggingUI_ASP_XTDP_exitTTCNButtonPressed){
      log("----- Exit button had been pressed ----");
    };
  };*/
  f_EPTF_UIHandler_exitButtonPressed(60.0);
  f_EPTF_Base_stopRemote(mtc);
  log("----- Subscriber shutdown -------");
}

function f_Server_behaviour(charstring pl_selfName, 
  in EPTF_UIHandler_CT pl_subscriber,
  in charstring pl_tabboxName) runs on Server_CT {
  log("----- Subscriber START -------");
  f_EPTF_UIHandler_init_CT(pl_selfName);
  f_EPTF_UIHandler_clearGUI();

  f_EPTF_UIHandler_addWindow();

  //Creates Layout:
  var boolean isExists;
  var Widgets vl_xul;
  f_EPTF_LoggingUITest_createHBox("EPTF_Main_hbox",vl_xul);
  f_EPTF_UIHandler_XSD_addElementToGui(vl_xul, "EPTF_Main_Window",isExists);

  f_EPTF_LoggingUITest_createMainWidgets("EPTF_Main_widgets",vl_xul);
  f_EPTF_UIHandler_XSD_addElementToGui(vl_xul, "EPTF_Main_hbox",isExists);

  f_EPTF_LoggingUITest_createTabBox("EPTF_Main_tabbox",vl_xul);
  f_EPTF_UIHandler_XSD_addElementToGui(vl_xul, "EPTF_Main_Window",isExists);  

  log("----- LoggingUI START -------"); 
  f_EPTF_LoggingUI_init_CT(pl_selfName, pl_subscriber,pl_tabboxName)


/*  alt {
    [] as_EPTF_UIHandler_exitButtonPressed();
  };*/
  f_EPTF_UIHandler_exitButtonPressed(60.0);
  f_EPTF_Base_stopRemote(mtc);
  log("----- Server shutdown -------");  
  f_EPTF_Base_cleanup_CT();
}

function f_Server_behaviour_NEG(charstring pl_selfName, 
  in EPTF_UIHandler_CT pl_subscriber,
  in charstring pl_tabboxName) runs on Server_CT {
  log("----- Subscriber START -------");
  f_EPTF_UIHandler_init_CT(pl_selfName);
  f_EPTF_UIHandler_clearGUI();

  f_EPTF_UIHandler_addWindow();
  
  f_EPTF_Base_setExpectedErrorMsg("*Inconsistent eventClassNames found. Event classes for this component type were already defined. Event class names are redefined on component* with compType: EPTF_SomeClient, selection: 1, eventClassNames:*")

  //Creates Layout:
  var boolean isExists;
  var Widgets vl_xul;
  f_EPTF_LoggingUITest_createHBox("EPTF_Main_hbox",vl_xul);
  f_EPTF_UIHandler_XSD_addElementToGui(vl_xul, "EPTF_Main_Window",isExists);

  f_EPTF_LoggingUITest_createMainWidgets("EPTF_Main_widgets",vl_xul);
  f_EPTF_UIHandler_XSD_addElementToGui(vl_xul, "EPTF_Main_hbox",isExists);

  f_EPTF_LoggingUITest_createTabBox("EPTF_Main_tabbox",vl_xul);
  f_EPTF_UIHandler_XSD_addElementToGui(vl_xul, "EPTF_Main_Window",isExists);  

  log("----- LoggingUI START -------"); 
  f_EPTF_LoggingUI_init_CT(pl_selfName, pl_subscriber,pl_tabboxName)


/*  alt {
    [] as_EPTF_UIHandler_exitButtonPressed();
  };*/
  f_EPTF_UIHandler_exitButtonPressed(60.0);
  f_EPTF_Base_stopRemote(mtc);
  log("----- Server shutdown -------");  
  f_EPTF_Base_cleanup_CT();
}

//=========================================================================
// Testcases
//=========================================================================

testcase tc_LoggingUI_logTest() runs on MAIN_CT system SYSTEM_CT{

  f_EPTF_Base_init_CT("MainCT");
  //Handler (subscriber)
//  var EPTF_UIHandler_CT ct_subscriber := EPTF_UIHandler_CT.create;
  var Server_CT ct_subscriber := Server_CT.create;

  //Starting UIHandler
  ct_subscriber.start(f_Server_behaviour("Main", ct_subscriber,"EPTF_Main_tabbox"));
  var EPTF_LoggingUI_CT ct_loggingUI_handler  := ct_subscriber;

  // to prevent deadlock in LoggingUIServer:
  timer t_wait := 1.0;
  t_wait.start; t_wait.timeout;
  
  //LoggingUI Client with HandlerClient & Starting Client
  var EPTF_LoggingUIClient_CT ct_client1 := EPTF_LoggingUIClient_CT.create;
  ct_client1.start(f_EPTF_LoggingUIClient1_Behaviour("Client1",ct_loggingUI_handler, ct_subscriber));

 //LoggingUI Client with HandlerClient & Starting Client
  var EPTF_LoggingUIClient_CT ct_client2 := EPTF_LoggingUIClient_CT.create;
  ct_client2.start(f_EPTF_LoggingUIClient2_Behaviour("Client2",ct_loggingUI_handler, ct_subscriber)); 

 //LoggingUI Client with HandlerClient & Starting Client
  var EPTF_LoggingUIClient_CT ct_client3 := EPTF_LoggingUIClient_CT.create;
  ct_client3.start(f_EPTF_LoggingUIClient3_Behaviour("Client3",ct_loggingUI_handler, ct_subscriber)); 

  timer T_main := 55.0;
  T_main.start;
  alt {
    [] ct_subscriber.done {}
    [] T_main.timeout{
      f_EPTF_Base_stopAll(pass);
    }
  }
  f_EPTF_Base_cleanup_CT();
}

testcase tc_LoggingUI_eventClassRedefinitionTest_NEG() runs on MAIN_CT system SYSTEM_CT{

  f_EPTF_Base_init_CT("MainCT");
  //Handler (subscriber)
//  var EPTF_UIHandler_CT ct_subscriber := EPTF_UIHandler_CT.create;
  var Server_CT ct_subscriber := Server_CT.create;

  //Starting UIHandler
  ct_subscriber.start(f_Server_behaviour_NEG("Main", ct_subscriber,"EPTF_Main_tabbox"));
  var EPTF_LoggingUI_CT ct_loggingUI_handler  := ct_subscriber;

  // to prevent deadlock in LoggingUIServer:
  timer t_wait := 1.0;
  t_wait.start; t_wait.timeout;
  
  //LoggingUI Client with HandlerClient & Starting Client
  var EPTF_LoggingUIClient_CT ct_client1 := EPTF_LoggingUIClient_CT.create;
  ct_client1.start(f_EPTF_LoggingUIClient1_Behaviour("Client1",ct_loggingUI_handler, ct_subscriber));

 //LoggingUI Client with HandlerClient & Starting Client
  var EPTF_LoggingUIClient_CT ct_client2 := EPTF_LoggingUIClient_CT.create;
  ct_client2.start(f_EPTF_LoggingUIClient2_Behaviour("Client2",ct_loggingUI_handler, ct_subscriber)); 

 //LoggingUI Client with HandlerClient & Starting Client
  var EPTF_LoggingUIClient_CT ct_client3 := EPTF_LoggingUIClient_CT.create;
  ct_client3.start(f_EPTF_LoggingUIClient3_Behaviour("Client3",ct_loggingUI_handler, ct_subscriber)); 

 //LoggingUI Client with incorrect event classes
  var EPTF_LoggingUIClient_CT ct_client4 := EPTF_LoggingUIClient_CT.create;
  ct_client4.start(f_EPTF_LoggingUIClient4_Behaviour("Client4",ct_loggingUI_handler, ct_subscriber)); 

  timer T_main := 55.0;
  T_main.start;
  alt {
    [] ct_subscriber.done {}
    [] T_main.timeout{
      f_EPTF_Base_stopAll(pass);
    }
  }
  f_EPTF_Base_cleanup_CT();
}

testcase tc_LoggingUI_functionTest() runs on MAIN_CT system SYSTEM_CT{

  f_EPTF_Base_init_CT("MainCT");
  //Handler (subscriber)
  //var EPTF_UIHandler_CT ct_subscriber := EPTF_UIHandler_CT.create;
  var Server_CT ct_subscriber := Server_CT.create;

  //Starting UIHandler
  ct_subscriber.start(f_Server_behaviour("Main", ct_subscriber,"EPTF_Main_tabbox"));
  var EPTF_LoggingUI_CT ct_loggingUI_handler  := ct_subscriber;

  // to prevent deadlock in LoggingUIServer:
  timer t_wait := 0.5;
  t_wait.start; t_wait.timeout;
  
  //LoggingUI Client with HandlerClient & Starting Client
  var EPTF_LoggingUIClient_CT ct_client := EPTF_LoggingUIClient_CT.create;
  ct_client.start(f_EPTF_LoggingUIClient3_Behaviour("Client1",ct_loggingUI_handler, ct_subscriber));

  timer T_main := 55.0;
  T_main.start;
  alt {
    [] ct_subscriber.done {}
    [] T_main.timeout{
      f_EPTF_Base_stopAll(pass);
    }
  }
  f_EPTF_Base_cleanup_CT();
}


testcase tc_LoggingUI_clientNumber(in integer pl_clients) runs on MAIN_CT system SYSTEM_CT{

  f_EPTF_Base_init_CT("MainCT");
  //Handler (subscriber)
//  var EPTF_UIHandler_CT ct_subscriber := EPTF_UIHandler_CT.create;
  var Server_CT ct_subscriber := Server_CT.create;

  //Starting UIHandler
  ct_subscriber.start(f_Server_behaviour("Main", ct_subscriber,"EPTF_Main_tabbox"));
  var EPTF_LoggingUI_CT ct_loggingUI_handler  := ct_subscriber;


  // to prevent deadlock in LoggingUIServer:
  timer t_wait := 0.5;
  t_wait.start; t_wait.timeout;
  
  //LoggingUI Client with HandlerClient & Starting Client
  for (var integer i:=0; i<pl_clients; i:=i+1) {
    var EPTF_LoggingUIClient_CT ct_client := EPTF_LoggingUIClient_CT.create;
    ct_client.start(f_EPTF_LoggingUIClient1_Behaviour("Client"&int2str(i),ct_loggingUI_handler, ct_subscriber)); 
  }  

  timer T_main := 55.0;
  T_main.start;
  alt {
    [] ct_subscriber.done {}
    [] T_main.timeout{
      f_EPTF_Base_stopAll(pass);
    }
  }
  f_EPTF_Base_cleanup_CT();
}

testcase tc_LoggingUI_cleanUp_CT() runs on Server_CT
{
  f_EPTF_UIHandler_init_CT("Main");
  f_EPTF_UIHandler_clearGUI();
  f_EPTF_UIHandler_addWindow();

  //Creates Layout:
  log("----- Layout creation -------");
  var boolean isExists;
  var Widgets vl_xul;
  f_EPTF_LoggingUITest_createHBox("EPTF_Main_hbox",vl_xul);
  f_EPTF_UIHandler_XSD_addElementToGui(vl_xul, "EPTF_Main_Window",isExists);
  f_EPTF_LoggingUITest_createMainWidgets("EPTF_Main_widgets",vl_xul);
  f_EPTF_UIHandler_XSD_addElementToGui(vl_xul, "EPTF_Main_hbox",isExists);
  f_EPTF_LoggingUITest_createTabBox("EPTF_Main_tabbox",vl_xul);
  f_EPTF_UIHandler_XSD_addElementToGui(vl_xul, "EPTF_Main_Window",isExists);  

  log("----- LoggingUI START -------"); 
  f_EPTF_LoggingUI_init_CT("Main", self ,"EPTF_Main_tabbox")
  f_EPTF_UIHandler_exitButtonPressed(3.0);
  log("----- CleanUp -------");
  f_EPTF_Base_cleanup_CT();

  log("----- Check hashmap -------");
  var integer vl_id := 0;
  if (f_EPTF_str2int_HashMap_GetID(c_EPTF_LoggingUI_typesLookupTable_HashName, vl_id) == false) {
    setverdict(pass);
  } else {
    log(%definitionId,": error: c_EPTF_LoggingUI_typesLookupTable Hashmap was not cleaned up, id: ", vl_id);
    setverdict(fail);
  }

  log("----- Layout creation again -------");
  f_EPTF_UIHandler_init_CT("Main");
  f_EPTF_UIHandler_clearGUI();
  f_EPTF_UIHandler_addWindow();
  f_EPTF_LoggingUITest_createHBox("EPTF_Main_hbox",vl_xul);
  f_EPTF_UIHandler_XSD_addElementToGui(vl_xul, "EPTF_Main_Window",isExists);
  f_EPTF_LoggingUITest_createMainWidgets("EPTF_Main_widgets",vl_xul);
  f_EPTF_UIHandler_XSD_addElementToGui(vl_xul, "EPTF_Main_hbox",isExists);
  f_EPTF_LoggingUITest_createTabBox("EPTF_Main_tabbox",vl_xul);
  f_EPTF_UIHandler_XSD_addElementToGui(vl_xul, "EPTF_Main_Window",isExists);  

  log("----- LoggingUI START again-------");
  f_EPTF_LoggingUI_init_CT("Main", self, "EPTF_Main_tabbox")
  log("----- CleanUp again-------");
  f_EPTF_Base_cleanup_CT();  

}

//=========================================================================
// Control
//=========================================================================

control
{
  execute(tc_LoggingUI_logTest());
  execute(tc_LoggingUI_eventClassRedefinitionTest_NEG())
  execute(tc_LoggingUI_functionTest());
  execute(tc_LoggingUI_clientNumber(10));
  execute(tc_LoggingUI_clientNumber(100));
  execute(tc_LoggingUI_cleanUp_CT());
}

}  // end of module
