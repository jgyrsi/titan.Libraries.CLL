///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
EPTF_Scheduler_Test_Testcases.tc_EPTF_Scheduler_Test_demo
EPTF_Scheduler_Test_Testcases.tc_EPTF_Scheduler_Test_timerReadSpeed
EPTF_Scheduler_Test_Testcases.tc_EPTF_Scheduler_Test_schedulerWritingSpeed_incremental_linear
EPTF_Scheduler_Test_Testcases.tc_EPTF_Scheduler_Test_schedulerWritingSpeed_decremental_linear
EPTF_Scheduler_Test_Testcases.tc_EPTF_Scheduler_Test_schedulerWritingSpeed_random_linear
EPTF_Scheduler_Test_Testcases.tc_EPTF_Scheduler_Test_schedulerWritingSpeed_incremental_exp
EPTF_Scheduler_Test_Testcases.tc_EPTF_Scheduler_Test_schedulerWritingSpeed_decremental_exp
EPTF_Scheduler_Test_Testcases.tc_EPTF_Scheduler_Test_schedulerWritingSpeed_random_exp
EPTF_Scheduler_Test_Testcases.tc_EPTF_Scheduler_Test_schedulerWriteThenReadSpeed_incr_exp
EPTF_Scheduler_Test_Testcases.tc_EPTF_Scheduler_Test_schedulerWriteThenReadSpeed_decr_exp
EPTF_Scheduler_Test_Testcases.tc_EPTF_Scheduler_Test_schedulerWriteThenReadSpeed_rand_exp
