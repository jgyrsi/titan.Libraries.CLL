///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_Scheduler_PerfTest_Functions
//
// Purpose:
//   This module contains functions to test the performance of EPTF Scheduler feature.
//
// Module Parameters:
//    -
//  Module depends on:
//    <EPTF_Scheduler_PerfTest_Definitions>
//    <EPTF_CLL_RBTScheduler_Functions>
//    <EPTF_CLL_Scheduler_Definitions>
//    <EPTF_CLL_Base_Functions>
//    <EPTF_CLL_Common_Definitions>
//    <TestResultGen>
//    <ttcn_ericsson_se_TitanSim_Perftest>
//    <TCCConversion_Functions>
//    <TCCMaths_Functions>
//
// Current Owner:
//    Jozsef Gyurusi (ethjgi)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////
module EPTF_Scheduler_PerfTest_Functions {

//=========================================================================
// Import Part
//=========================================================================
import from EPTF_Scheduler_PerfTest_Definitions all;
import from EPTF_CLL_RBTScheduler_Functions all;
import from EPTF_CLL_Scheduler_Definitions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_Common_Definitions all;
import from TestResultGen all;
import from ttcn_ericsson_se_TitanSim_Perftest all;
import from TCCConversion_Functions all;
import from TCCMaths_Functions all;

//=========================================================================
// Functions
//=========================================================================

function f_EPTF_Scheduler_PerfTest_init_CT(
  in charstring pl_selfName
) runs on Scheduler_PerfTest_CT {
  
  
  f_EPTF_Base_init_CT(pl_selfName);
  f_EPTF_Scheduler_init_CT(pl_selfName);
    
  v_start_time := f_TestResultGen_getCurrentTime();
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_Scheduler_PerfTest_measure_scheduleAction
// 
//  Purpose:
//    measure the execution time of f_EPTF_SchedulerComp_scheduleAction function
//
//  Parameters:
//    pl_numOfIteration - *in integer* - number of events to schedule
//
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////
function f_EPTF_Scheduler_PerfTest_measure_scheduleAction(
  in integer pl_numOfIteration
  ) runs on Scheduler_PerfTest_CT {
  
  
  
  action("Generating random numbers...");
  // generate random times to schedule the events in the interval 0..10.0
  var EPTF_FloatList vl_whenList;
  for(var integer i:=0; i<pl_numOfIteration; i:=i+1) {
    vl_whenList[i] := 10.0*rnd();
  }
  var integer vl_eventIndex;
  
  // shift the events into the future:
  var float vl_currentTime := f_EPTF_Base_getRelTimeInSecs();  
  var float vl_safetyTimeForEventSchedule := 1.0+int2float(pl_numOfIteration)/100000.0;
  for(var integer i:=0; i<pl_numOfIteration; i:=i+1) {
    vl_whenList[i] := vl_currentTime + vl_safetyTimeForEventSchedule + vl_whenList[i];
  }
   
  action("Measurement starts")
  // start measurement
  var float vl_startTime := f_EPTF_Base_getRelTimeInSecs();
  // do task ...
  for(var integer i:=0; i<pl_numOfIteration; i:=i+1) {
    f_EPTF_SchedulerComp_scheduleAction(
      pl_when := vl_whenList[i],
      pl_actionHandler := null,
      pl_action := {},
      pl_eventIndex := vl_eventIndex,
      pl_roundIt := false,
      pl_dteHandler := null
    );
  }
  var float vl_endTime := f_EPTF_Base_getRelTimeInSecs();
  
  action("Measurement finised");
 
  // start control measurement
  var float vl_startTimeControl := f_EPTF_Base_getRelTimeInSecs();
  // do empty task ...
  for(var integer i:=0; i<pl_numOfIteration; i:=i+1) {
  }
  var float vl_endTimeControl := f_EPTF_Base_getRelTimeInSecs();
  action("Corrigation time: ", (vl_endTimeControl-vl_startTimeControl)/int2float(pl_numOfIteration)*1e6);  
   
  v_testResult.result_list[sizeof(v_testResult.result_list)] := {
      name := "Average scheduling time of one event for "&int2str(pl_numOfIteration)&" events",
      unit := "microsec",
      base := log2str((vl_endTime-vl_startTime+vl_endTimeControl-vl_startTimeControl)/int2float(pl_numOfIteration)*1e6)
  };
    
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_Scheduler_PerfTest_measure_cancelEvent
// 
//  Purpose:
//    measure the execution time of f_EPTF_SchedulerComp_CancelEvent function
//
//  Parameters:
//    pl_numOfIteration - *in integer* - number of events to schedule
//
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////
function f_EPTF_Scheduler_PerfTest_measure_cancelEvent(
  in integer pl_numOfIteration
  ) runs on Scheduler_PerfTest_CT {
  
  
  
  action("Generating random numbers...");
  // generate random times to schedule the events in the interval 0..10.0
  var EPTF_FloatList vl_whenList;
  var EPTF_IntegerList vl_eventIdxList;
  for(var integer i:=0; i<pl_numOfIteration; i:=i+1) {
    vl_whenList[i] := 10.0*rnd();
    vl_eventIdxList[i] := i;
  }
  
  // shift the events into the future:
  var float vl_currentTime := f_EPTF_Base_getRelTimeInSecs();  
  var float vl_safetyTimeForEventSchedule := 1.0+int2float(pl_numOfIteration)/100000.0;
  for(var integer i:=0; i<pl_numOfIteration; i:=i+1) {
    vl_whenList[i] := vl_currentTime + vl_safetyTimeForEventSchedule + vl_whenList[i];
  }
   
  action("Scheduling the events...");
  // schedule the events
  for(var integer i:=0; i<pl_numOfIteration; i:=i+1) {
    f_EPTF_SchedulerComp_scheduleAction(
      pl_when := vl_whenList[i],
      pl_actionHandler := null,
      pl_action := {},
      pl_eventIndex := vl_eventIdxList[i],
      pl_roundIt := false,
      pl_dteHandler := null
    );
  }

  action("Measurement starts")
  // start measurement
  var float vl_startTime := f_EPTF_Base_getRelTimeInSecs();
  // do task ...
  for(var integer i:=0; i<pl_numOfIteration; i:=i+1) {
    f_EPTF_SchedulerComp_CancelEvent(
      qidx := vl_eventIdxList[i]
    );
  }
  var float vl_endTime := f_EPTF_Base_getRelTimeInSecs();
  
  action("Measurement finised");
 
  // start control measurement
  var float vl_startTimeControl := f_EPTF_Base_getRelTimeInSecs();
  // do empty task ...
  for(var integer i:=0; i<pl_numOfIteration; i:=i+1) {
  }
  var float vl_endTimeControl := f_EPTF_Base_getRelTimeInSecs();
  action("Corrigation time: ", (vl_endTimeControl-vl_startTimeControl)/int2float(pl_numOfIteration)*1e6);  
   
  v_testResult.result_list[sizeof(v_testResult.result_list)] := {
      name := "Average cancelling time of one event for "&int2str(pl_numOfIteration)&" events",
      unit := "microsec",
      base := log2str((vl_endTime-vl_startTime+vl_endTimeControl-vl_startTimeControl)/int2float(pl_numOfIteration)*1e6)
  };
    
}


function f_EPTF_Scheduler_PerfTest_measure_performActions_dummyHandler(
  in EPTF_ScheduledAction pl_action, in integer pl_eventIndex
) runs on Scheduler_PerfTest_CT return boolean {
  v_sumTLag := v_sumTLag + f_EPTF_Base_getRelTimeInSecs() - pl_action.when
  return true;
}

function f_EPTF_Scheduler_PerfTest_measure_performActions_firstEventHandler(
  in EPTF_ScheduledAction pl_action, in integer pl_eventIndex
) runs on Scheduler_PerfTest_CT return boolean {
  action("First event handled when: ", pl_action.when, " current time: ", f_EPTF_Base_getRelTimeInSecs(), " snapshot time: ", f_EPTF_SchedulerComp_snapshotTime());
  v_measurementStartTime := f_EPTF_Base_getRelTimeInSecs();
  v_sumTLag := v_sumTLag + f_EPTF_Base_getRelTimeInSecs() - pl_action.when
  return true;
}

function f_EPTF_Scheduler_PerfTest_measure_performActions_lastEventHandler(
  in EPTF_ScheduledAction pl_action, in integer pl_eventIndex
) runs on Scheduler_PerfTest_CT return boolean {
  v_sumTLag := v_sumTLag + f_EPTF_Base_getRelTimeInSecs() - pl_action.when
  v_measurementEndTime := f_EPTF_Base_getRelTimeInSecs();
  action("Last event handled when: ", pl_action.when, " current time: ", f_EPTF_Base_getRelTimeInSecs(), " snapshot time: ", f_EPTF_SchedulerComp_snapshotTime());
  return true;
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_Scheduler_PerfTest_measure_performActions
// 
//  Purpose:
//    measure the time lag of events during event processing
//
//  Parameters:
//    pl_numOfIteration - *in integer* - number of events to schedule
//
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////
function f_EPTF_Scheduler_PerfTest_measure_performActions(
  in integer pl_numOfIteration
  ) runs on Scheduler_PerfTest_CT {
  
  v_measurementStartTime := 0.0;
  v_measurementEndTime := -1.0;
  v_sumTLag := 0.0;
    
  action("Generating random numbers...");
  // generate random times to schedule the events in the interval 0..10.0
  var EPTF_FloatList vl_whenList;
  for(var integer i:=0; i<pl_numOfIteration; i:=i+1) {
    vl_whenList[i] := 10.0*rnd();
  }
  var integer vl_eventIdx;
  // modify the first event time to be the latest of all:
  vl_whenList[0] := 0.0;
  // modify the last event time to be the latest of all:
  vl_whenList[pl_numOfIteration-1] := 10.0;

  // shift the events into the future:
  var float vl_currentTime := f_EPTF_Base_getRelTimeInSecs();  
  var float vl_safetyTimeForEventSchedule := 1.0+int2float(pl_numOfIteration)/100000.0;
  for(var integer i:=0; i<pl_numOfIteration; i:=i+1) {
    vl_whenList[i] := vl_currentTime + vl_safetyTimeForEventSchedule + vl_whenList[i];
  }
   
  action("Scheduling the events...");
  // schedule the events
  for(var integer i:=0; i<pl_numOfIteration; i:=i+1) {
    var EPTF_Scheduler_ActionHandler vl_eventHandler;
    if (i == 0) {
      // set the last handler for the first event
      vl_eventHandler := refers(f_EPTF_Scheduler_PerfTest_measure_performActions_firstEventHandler);
    } else if (i == pl_numOfIteration-1) {
      // set the last handler for the last event
      vl_eventHandler := refers(f_EPTF_Scheduler_PerfTest_measure_performActions_lastEventHandler);
    } else {
      // set dummy handler for all the others
      vl_eventHandler := refers(f_EPTF_Scheduler_PerfTest_measure_performActions_dummyHandler);
    }
    f_EPTF_SchedulerComp_scheduleAction(
      pl_when := vl_whenList[i],
      pl_actionHandler := vl_eventHandler,
      pl_action := {},
      pl_eventIndex := vl_eventIdx,
      pl_roundIt := false,
      pl_dteHandler := null
    );
  }

  action("Measurement starts")
  // start measurement
  var float vl_startTime := f_EPTF_Base_getRelTimeInSecs();
  // do task ...
  // wait for the last event:
  timer t_last_wait := 0.0;
  timer t_pinger := 0.5; // trigger alt snapshot
  t_last_wait.start;
  t_pinger.start;
  alt {
    [v_measurementEndTime>0.0] t_last_wait.timeout {
      // last event is processed
    }
    [] t_pinger.timeout {
      t_pinger.start;
      repeat;
    }
  }
  
  action("Measurement finised");

  // start control measurement
  var EPTF_ScheduledAction vl_action;
  vl_action.when := 5.0;
  var float vl_sumTLag := 0.0
  var float vl_startTimeControl := f_EPTF_Base_getRelTimeInSecs();
  // do the same task as in the handler ...
  for(var integer i:=0; i<pl_numOfIteration; i:=i+1) {
    vl_sumTLag := vl_sumTLag + f_EPTF_Base_getRelTimeInSecs() - vl_action.when
  }
  var float vl_endTimeControl := f_EPTF_Base_getRelTimeInSecs();
  action("Corrigation time: ", (vl_endTimeControl-vl_startTimeControl)/int2float(pl_numOfIteration)*1e6);  

  action("Time remained before processing first event: ", vl_currentTime + vl_safetyTimeForEventSchedule - vl_startTime);
  if (vl_currentTime + vl_safetyTimeForEventSchedule - vl_startTime<0.01) {
    setverdict(inconc, "Time is not enough to schedule all events in time. First events are in the past!")
  }
  vl_startTime := v_measurementStartTime;
  var float vl_endTime := v_measurementEndTime;
   
  v_testResult.result_list[sizeof(v_testResult.result_list)] := {
      name := "Average time lag of one event for "&int2str(pl_numOfIteration)&" events",
      unit := "microsec",
      base := log2str((v_sumTLag+(vl_endTimeControl-vl_startTimeControl))/int2float(pl_numOfIteration)*1e6)
  };
    
}

} // module
