///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_Scheduler_Test_Testcases
// 
//  Purpose:
//    This module provides testcases for testing EPTF_CLL_Scheduler
// 
//  Module depends on:
//    -
// 
//  Current Owner:
//    Attila JenÃ¶ BalaskÃ³ (ETHBAAT)
// 
//  Last Review Date:
//    -
//
//  Detailed Comments:
//    Provide definitions to test
//
//
///////////////////////////////////////////////////////////////
module EPTF_Scheduler_Test_Testcases {

import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_Scheduler_Definitions all;
//import from EPTF_CLL_FBQScheduler_Definitions all;
//import from EPTF_CLL_FBQScheduler_Functions all;
// to use Red-black tree scheduler, comment the FBQ lines and uncomment these lines:
import from EPTF_CLL_RBTScheduler_Definitions all;
import from EPTF_CLL_RBTScheduler_Functions all;
import from EPTF_Scheduler_Test_Definitions all;
import from EPTF_Scheduler_Test_Functions all;
import from TCCSystem_Functions all;
import from EPTF_CLL_HostAdmin_BaseFunctions all;
import from TestResultGen all;
import from ttcn_ericsson_se_TitanSim_Perftest all;
import from EPTF_CLL_Logging_Functions all;

//==================================================================
//=========      Testcases   =======================================
//==================================================================
group Testcases {

  //===============================================
  // Name:      tc_EPTF_Scheduler_Test_demo
  // Purpose:   Basic test imported from Demo
  // Status:    Ready, Passed
  //===============================================
  testcase tc_EPTF_Scheduler_Test_demo() runs on EPTF_Scheduler_Test_CT {
    action("Please wait 30 seconds until test case finishes...");
    f_EPTF_Scheduler_Test_init_CT();
    f_startEvent0(1.0,10);
    f_startEvent1(1.0,3.0,10);
    timer t_wait := 30.0;
    t_wait.start;
    alt {
      [] t_wait.timeout {
        }
    }
    // test if the event were executed correct number of times:
    if (v_event0Data.eventCounter != 21 or v_event1Data.eventCounter !=4) {
      setverdict(fail, "Incorrect number of events: v_event0Data.eventCounter: ", v_event0Data.eventCounter, " should be 21, v_event1Data.eventCounter: ",
        v_event1Data.eventCounter, " should be 4");
    }
    setverdict(pass);
    f_EPTF_Base_stop(none);
  }//tc_

  //===============================================
  // Name:      tc_EPTF_Scheduler_Test_parallel
  // Purpose:   To test whether the scheduler can run event handlers in parallel
  // Status:    
  //===============================================
  testcase tc_EPTF_Scheduler_Test_parallel() runs on EPTF_Scheduler_ParallelTest_CT {
    action("Parallel test started...");
    f_EPTF_Scheduler_ParallelTest_init_CT();
    f_EPTF_Scheduler_ParallelTest_startEvents(10.0,0.1,100);
    timer t_wait := v_testcaseDuration;
    timer t_allEvents := 0.0;
    t_wait.start;
    t_allEvents.start;
    alt {
      [v_allEventsProcessed]  t_allEvents.timeout;
      []  t_wait.timeout;
    }
    if (v_allEventsProcessed) {
      setverdict(pass);
    } else {
      setverdict(fail, "Not all scheduled events were processed in time.");
    }
    f_EPTF_Base_stop();
  }//tc_
  
  //===============================================
  // Name:     tc_EPTF_Scheduler_Test_timerReadSpeed
  // Purpose:  To test the scheduler how quick is to read the component clock
  //           Measured value app. 3.5e-07s i.e less than 1e-06s
  // Status:   Ready.
  //===============================================
  testcase tc_EPTF_Scheduler_Test_timerReadSpeed() runs on EPTF_Scheduler_Test_CT {
    f_EPTF_Scheduler_Test_init_CT(); 
    var float vl_startTime:=T_EPTF_componentClock.read;
    var float vl_endTime:=0.0
    for(var integer vl_n:=0;vl_n< tsp_NrOfTimerReading; vl_n:=vl_n+1) {
      vl_endTime:=T_EPTF_componentClock.read;
    }
    log(">>>Timer Reading speed: Elapsed time/NrOfRead/Time/TimePerRead: ", 
      vl_endTime-vl_startTime," ",tsp_NrOfTimerReading," ",(vl_endTime-vl_startTime)/int2float(tsp_NrOfTimerReading));
    if((vl_endTime-vl_startTime)/int2float(tsp_NrOfTimerReading) < 0.000001) {
      setverdict(pass);
    } else {
      setverdict(fail);
    }
    self.stop;
  }//tc_

  //===============================================
  // Name:     tc_EPTF_Scheduler_Test_schedulerWritingSpeed_incremental_linear
  // Purpose:  To test the scheduler how quick is to write into the event queue
  //           It measures the full and average insertion time into the event queue time for N elements,
  //           where N=tsp_deltaNoOfEvents, 2*tsp_deltaNoOfEvents,3*tsp_deltaNoOfEvents,...tsp_noOfEvents,
  //           The timestamp is strictly increasing in case of a certain  N. The Queue is been emptied before each N
  // Parameters: tsp_noOfEvents: max number of events
  //            tsp_deltaNoOfEvents 
  //            tsp_eventGap   : used eventGap between two event timestamp
  //            tsp_delayBetweenSchAndGen: the difference between the time of the first writing and the least timestamp of events scheduled
  //
  // Status:   Ready.
  //===============================================
  testcase tc_EPTF_Scheduler_Test_schedulerWritingSpeed_incremental_linear() runs on EPTF_Scheduler_Test_CT {
    f_EPTF_Scheduler_Test_init_CT(); 
    var float vl_totalAvgTime := 0.0;
    vl_totalAvgTime:=f_EPTF_Scheduler_Test_StartEvent2List_Average(
      tsp_noOfEvents, tsp_deltaNoOfEvents, tsp_eventGap, tsp_delayBetweenSchAndGen, incr,lin) 

    log(">>Incremental Writing: Average Time Spent For One Insertion Into The Queue:",vl_totalAvgTime);
    if( vl_totalAvgTime<0.0005) //not too strict, it means 2000/s
    {
      setverdict(pass);
    } else {
      setverdict(fail);
    }
    self.stop;
  }//tc_

group ParallelTestFuncions {
  //===============================================
  // Name:     tc_EPTF_Scheduler_Test_schedulerWritingSpeed_decremental_linear
  // Purpose:  To test the scheduler how quick is to write into the event queue
  //           It measures the full and average insertion time into the event queue time for N elements,
  //           where N=tsp_deltaNoOfEvents, 2*tsp_deltaNoOfEvents,3*tsp_deltaNoOfEvents...tsp_noOfEvents
  //           The timestamp is strictly decreasing in case of a certain  N. The Queue is been emptied before each N
  // Parameters: tsp_noOfEvents: max number of events
  //            tsp_deltaNoOfEvents 
  //            tsp_eventGap   : used eventGap between two event timestamp (for incremental and decremental case)
  //            tsp_delayBetweenSchAndGen: the difference between the time of the first writing and the least timestamp of events scheduled
  //
  // Status:   Ready.
  //===============================================
  testcase tc_EPTF_Scheduler_Test_schedulerWritingSpeed_decremental_linear() runs on EPTF_Scheduler_Test_CT {
    f_EPTF_Scheduler_Test_init_CT(); 
    var float vl_totalAvgTime := 0.0;
    vl_totalAvgTime:=f_EPTF_Scheduler_Test_StartEvent2List_Average(
      tsp_noOfEvents, tsp_deltaNoOfEvents, tsp_eventGap, tsp_delayBetweenSchAndGen, decr,lin) 

    log(">>Decremental Writing: Average Time Spent For One Insertion Into The Queue:",vl_totalAvgTime);
    if( vl_totalAvgTime<0.0005) //not too strict, it means 2000/s
    {
      setverdict(pass);
    } else {
      setverdict(fail);
    }
    self.stop;
  }//tc_
}//group ParallelTestFunctions

  //===============================================
  // Name:     tc_EPTF_Scheduler_Test_schedulerWritingSpeed_random_linear
  // Purpose:  To test the scheduler how quick is to write into the event queue
  //           It measures the full and average insertion time into the event queue time for N elements,
  //           where N=tsp_deltaNoOfEvents, 2*tsp_deltaNoOfEvents,...tsp_noOfEvents
  // Parameters: tsp_noOfEvents: max number of events
  //            tsp_deltaNoOfEvents 
  //            tsp_eventGap   : used eventGap between two event timestamp (average;used only to calc the min and max timestamp)
  //            tsp_delayBetweenSchAndGen: the difference between the time of the first writing and the least timestamp of events scheduled
  //
  // Status:   Ready.
  //===============================================
  testcase tc_EPTF_Scheduler_Test_schedulerWritingSpeed_random_linear() runs on EPTF_Scheduler_Test_CT {
    f_EPTF_Scheduler_Test_init_CT(); 
    var float vl_totalAvgTime := 0.0;
    vl_totalAvgTime:=f_EPTF_Scheduler_Test_StartEvent2List_Average(
      tsp_noOfEvents, tsp_deltaNoOfEvents, tsp_eventGap, tsp_delayBetweenSchAndGen, rand,lin) 

    log(">>Random Writing: Average Time Spent For One Insertion Into The Queue:",vl_totalAvgTime);
    if( vl_totalAvgTime<0.0005) //not too strict, it means 2000/s
    {
      setverdict(pass);
    } else {
      setverdict(fail);
    }
    self.stop;
  }//tc_


    //===============================================
  // Name:     tc_EPTF_Scheduler_Test_schedulerWritingSpeed_incremental_exp
  // Purpose:  To test the scheduler how quick is to write into the event queue
  //           It measures the full and average insertion time into the event queue time for N elements,
  //           where N=tsp_deltaNoOfEvents, 2*tsp_deltaNoOfEvents,...tsp_noOfEvents,
  //           The timestamp is strictly increasing in case of a certain  N. The Queue is been emptied before each N
  // Parameters: tsp_noOfEvents: max number of events
  //            tsp_deltaNoOfEvents 
  //            tsp_eventGap   : used eventGap between two event timestamp 
  //            tsp_delayBetweenSchAndGen: the difference between the time of the first writing and the least timestamp of events scheduled
  //
  // Status:   Ready.
  //===============================================
  testcase tc_EPTF_Scheduler_Test_schedulerWritingSpeed_incremental_exp() runs on EPTF_Scheduler_Test_CT {
    f_EPTF_Scheduler_Test_init_CT(); 
    var float vl_totalAvgTime := 0.0;
    vl_totalAvgTime:=f_EPTF_Scheduler_Test_StartEvent2List_Average(
      tsp_noOfEvents, tsp_qNoOfEvents, tsp_eventGap, tsp_delayBetweenSchAndGen, incr,exp) 

    log(">>Incremental Writing: Average Time Spent For One Insertion Into The Queue:",vl_totalAvgTime);
    if( vl_totalAvgTime<0.0005) //not too strict, it means 2000/s
    {
      setverdict(pass);
    } else {
      setverdict(fail);
    }
    self.stop;
  }//tc_

  //===============================================
  // Name:     tc_EPTF_Scheduler_Test_schedulerWritingSpeed_decremental_exp
  // Purpose:  To test the scheduler how quick is to write into the event queue
  //           It measures the full and average insertion time into the event queue time for N elements,
  //           where N=tsp_deltaNoOfEvents, 2*tsp_deltaNoOfEvents,...tsp_noOfEvents
  // Parameters: tsp_noOfEvents: max number of events
  //            tsp_deltaNoOfEvents 
  //            tsp_eventGap   : used eventGap between two event timestamp (for incremental and decremental case)
  //            tsp_delayBetweenSchAndGen: the difference between the time of the first writing and the least timestamp of events scheduled
  //
  // Status:   Ready.
  //===============================================
  testcase tc_EPTF_Scheduler_Test_schedulerWritingSpeed_decremental_exp() runs on EPTF_Scheduler_Test_CT {
    f_EPTF_Scheduler_Test_init_CT(); 
    var float vl_totalAvgTime := 0.0;
    vl_totalAvgTime:=f_EPTF_Scheduler_Test_StartEvent2List_Average(
      tsp_noOfEvents, tsp_qNoOfEvents, tsp_eventGap, tsp_delayBetweenSchAndGen, decr,exp) 

    log(">>Decremental Writing: Average Time Spent For One Insertion Into The Queue:",vl_totalAvgTime);
    if( vl_totalAvgTime<0.0005) //not too strict, it means 2000/s
    {
      setverdict(pass);
    } else {
      setverdict(fail);
    }
    self.stop;
  }//tc_

  //===============================================
  // Name:     tc_EPTF_Scheduler_Test_schedulerWritingSpeed_random_exp
  // Purpose:  To test the scheduler how quick is to write into the event queue
  //           It measures the full and average insertion time into the event queue time for N elements,
  //           where N=tsp_deltaNoOfEvents, 2*tsp_deltaNoOfEvents,...tsp_noOfEvents
  // Parameters: tsp_noOfEvents: max number of events
  //            tsp_deltaNoOfEvents 
  //            tsp_eventGap   : used eventGap between two event timestamp (used only for incremental and decremental case)
  //            tsp_delayBetweenSchAndGen: the difference between the time of the first writing and the least timestamp of events scheduled
  //
  // Status:   Ready.
  //===============================================
  testcase tc_EPTF_Scheduler_Test_schedulerWritingSpeed_random_exp() runs on EPTF_Scheduler_Test_CT {
    f_EPTF_Scheduler_Test_init_CT(); 
    var float vl_totalAvgTime := 0.0;
    vl_totalAvgTime:=f_EPTF_Scheduler_Test_StartEvent2List_Average(
      tsp_noOfEvents, tsp_qNoOfEvents, tsp_eventGap, tsp_delayBetweenSchAndGen, rand,exp) 

    log(">>Random Writing: Average Time Spent For One Insertion Into The Queue:",vl_totalAvgTime);
    if( vl_totalAvgTime<0.0005) //not too strict, it means 2000/s
    {
      setverdict(pass);
    } else {
      setverdict(fail);
    }
    self.stop;
  }//tc_
  
  //===============================================
  // Name:     tc_EPTF_Scheduler_Test_schedulerWriteThenReadSpeed_incr_exp
  // Purpose:  To test the scheduler as a whole, i.e
  //           functions f_EPTF_SchedulerComp_InitScheduler, f_EPTF_SchedulerComp_scheduleAction, 
  //           and within it e.g as_EPTF_SchedulerComp_ActionHandler
  // Status:   Started
  //===============================================
  testcase tc_EPTF_Scheduler_Test_schedulerWriteThenReadSpeed_incr_exp() runs on EPTF_Scheduler_Test_CT {
    var verdicttype vl_verdict:=pass;

    vl_verdict:=f_EPTF_Scheduler_Test_WriteReadHandleEvents_Average(
      tsp_repeatNo,
      tsp_noOfEvents,
      tsp_qNoOfEvents,
      tsp_eventGap,
      tsp_delayBetweenSchAndGen,
      incr,
      exp)

    setverdict(vl_verdict);

    //cleanup ???

  }//tc_
  
  //===============================================
  // Name:     tc_EPTF_Scheduler_Test_schedulerWriteThenReadSpeed_decr_exp
  // Purpose:  To test the scheduler as a whole, i.e
  //           functions f_EPTF_SchedulerComp_InitScheduler, f_EPTF_SchedulerComp_scheduleAction, 
  //           and within it e.g as_EPTF_SchedulerComp_ActionHandler
  // Status:   Ready
  //===============================================
  testcase tc_EPTF_Scheduler_Test_schedulerWriteThenReadSpeed_decr_exp() runs on EPTF_Scheduler_Test_CT {
    var verdicttype vl_verdict:=pass;

    vl_verdict:=f_EPTF_Scheduler_Test_WriteReadHandleEvents_Average(
      tsp_repeatNo,
      tsp_noOfEvents,
      tsp_qNoOfEvents,
      tsp_eventGap,
      tsp_delayBetweenSchAndGen,
      decr,
      exp)

    setverdict(vl_verdict);

    //log("==TC finished===");
    //cleanup ???

  }//tc_  
  //===============================================
  // Name:     tc_EPTF_Scheduler_Test_schedulerWriteThenReadSpeed_rand_exp
  // Purpose:  To test the scheduler as a whole, i.e
  //           functions f_EPTF_SchedulerComp_InitScheduler, f_EPTF_SchedulerComp_scheduleAction, 
  //           and within it e.g as_EPTF_SchedulerComp_ActionHandler
  // Status:   Ready
  //===============================================
  testcase tc_EPTF_Scheduler_Test_schedulerWriteThenReadSpeed_rand_exp() runs on EPTF_Scheduler_Test_CT {
    var verdicttype vl_verdict:=pass;

    vl_verdict:=f_EPTF_Scheduler_Test_WriteReadHandleEvents_Average(
      tsp_repeatNo,
      tsp_noOfEvents,
      tsp_qNoOfEvents,
      tsp_eventGap,
      tsp_delayBetweenSchAndGen,
      rand,
      exp)

    setverdict(vl_verdict);

    //log("==TC finished===");
    //cleanup ???

  }//tc_
//    testcase tc_bug() runs on EPTF_Scheduler_Test_CT {
//     log("==TC finished===");
//      //cleanup ???
//      mtc.stop
//   }




group Scheduler_Test_dte_handler {

// Test purpose: Schedule several events, some of them generates DTE, some of them generates DTE in the DTE handler,
// and some generates DTE without DTE handler.
// The test case checks whether all events were handled, all DTE were handled and if the corresponding warning message
// was logged.
// The verdict should be pass, no dynamic test case error should occur at test case level.
testcase tc_EPTF_Scheduler_Test_dte_handler()
runs on EPTF_Scheduler_Test_CT
{
  f_EPTF_Scheduler_init_CT("dte_handler");
  f_EPTF_Base_setDTEHandling(true);
  
  v_eventCounter := 0;
  v_actualNofSchEvents := 0;

  f_EPTF_SchedulerComp_refreshSnapshotTime();
  
  f_EPTF_Logging_registerPreambleFn(refers(f_EPTF_Scheduler_Test_checkWarning_DTE_preamble_FN));
  
  var integer vl_eventIdx;
  f_EPTF_SchedulerComp_scheduleAction(
    pl_when := f_EPTF_SchedulerComp_snapshotTime()+0.1,
    pl_actionHandler :=  refers(f_EPTF_Scheduler_Test_generateDTEActionHandler), 
    pl_action := {},
    pl_eventIndex := vl_eventIdx,
    pl_dteHandler := refers(f_EPTF_Scheduler_Test_dteHandler)
  );
  f_EPTF_SchedulerComp_scheduleAction(
    pl_when := f_EPTF_SchedulerComp_snapshotTime()+0.2,
    pl_actionHandler :=  refers(f_EPTF_Scheduler_Test_generateDTEActionHandler), 
    pl_action := {1}, // generate DTE
    pl_eventIndex := vl_eventIdx,
    pl_dteHandler := refers(f_EPTF_Scheduler_Test_dteHandler)
  );
  f_EPTF_SchedulerComp_scheduleAction(
    pl_when := f_EPTF_SchedulerComp_snapshotTime()+0.3,
    pl_actionHandler :=  refers(f_EPTF_Scheduler_Test_generateDTEActionHandler), 
    pl_action := {},
    pl_eventIndex := vl_eventIdx,
    pl_dteHandler := refers(f_EPTF_Scheduler_Test_dteHandler)
  );
  f_EPTF_SchedulerComp_scheduleAction(
    pl_when := f_EPTF_SchedulerComp_snapshotTime()+0.4,
    pl_actionHandler :=  refers(f_EPTF_Scheduler_Test_generateDTEActionHandler), 
    pl_action := {1,1}, // generate DTE and generate DTE in dtehandler as well
    pl_eventIndex := vl_eventIdx,
    pl_dteHandler := refers(f_EPTF_Scheduler_Test_dteHandler)
  );
  f_EPTF_SchedulerComp_scheduleAction(
    pl_when := f_EPTF_SchedulerComp_snapshotTime()+0.5,
    pl_actionHandler :=  refers(f_EPTF_Scheduler_Test_generateDTEActionHandler), 
    pl_action := {},
    pl_eventIndex := vl_eventIdx,
    pl_dteHandler := refers(f_EPTF_Scheduler_Test_dteHandler)
  );
  f_EPTF_SchedulerComp_scheduleAction(
    pl_when := f_EPTF_SchedulerComp_snapshotTime()+0.6,
    pl_actionHandler :=  refers(f_EPTF_Scheduler_Test_generateDTEActionHandler), 
    pl_action := {1}, // generate DTE
    pl_eventIndex := vl_eventIdx,
    pl_dteHandler := null // no dteHandler but DTE is generated
  );
  f_EPTF_SchedulerComp_scheduleAction(
    pl_when := f_EPTF_SchedulerComp_snapshotTime()+0.7,
    pl_actionHandler :=  refers(f_EPTF_Scheduler_Test_generateDTEActionHandler), 
    pl_action := {},
    pl_eventIndex := vl_eventIdx,
    pl_dteHandler := refers(f_EPTF_Scheduler_Test_dteHandler)
  );
 
  timer t_wait := 1.0;
  t_wait.start;
  t_wait.timeout; // event is handled before timeout;
  
  if (v_eventCounter!=2) {
    setverdict(fail,"Expected DTE did not appear")
  }
  
  if (v_actualNofSchEvents!=7) {
    setverdict(fail,"Too few events are handled: ", v_actualNofSchEvents);
  }
  
  f_EPTF_Base_stop(none);
}

} //group Scheduler_Test_dte_handler

}//group Testcases

group PerformanceTests
{

modulepar integer tsp_minClusterSize := 1;
modulepar integer tsp_maxClusterSize := 1000;
modulepar integer tsp_clusterSizeMul := 10;// default cluster sizes: 1, 10, 100, 1000
modulepar integer tsp_clusterSizeInc := 0; // used if tsp_clusterLogIncrement == false

modulepar integer tsp_minDistinctIntervalSize := 1;
modulepar integer tsp_maxDistinctIntervalSize := 1000;
modulepar integer tsp_distinctIntervalSizeMul := 10;// default distinct interval sizes: 1, 10, 100, 1000
modulepar integer tsp_distinctIntervalSizeInc := 0; // used if tsp_distinctIntervalLogIncrement == false

type record of integer ROI;
type record of float ROF;
modulepar ROF tsp_holdingTime := {0.1};
modulepar ROI tsp_cps := {50};
modulepar ROI tsp_nofSchEvents := {2};
modulepar integer tsp_nofMeasurements := 100;

function f_dummy_eventHandler(in EPTF_ScheduledAction pl_action, in integer pl_eventIndex)
runs on EPTF_Scheduler_CT return boolean {
  return true;
}

function f_EPTF_Scheduler_PerfTest_schedule(
  in integer pl_numberOfDistinctIntervals,
  in integer pl_numberOfClustersPerInterval,
  inout EPTF_IntegerList pl_timerIdxList)
runs on EPTF_Scheduler_CT
return float // time taken
{
  var integer vl_interval := 0, vl_cluster := 0, vl_idx := 0;
  var float vl_when := f_EPTF_Base_getAbsTimeInSecs() + 10.0;
  timer T_meas := 100000.0;
  T_meas.start;
  for(vl_interval := 0; vl_interval < pl_numberOfDistinctIntervals; vl_interval := vl_interval + 1) {
    for(vl_cluster := 0; vl_cluster < pl_numberOfClustersPerInterval; vl_cluster := vl_cluster + 1) {
      f_EPTF_SchedulerComp_scheduleAction(
        vl_when,
        refers(f_dummy_eventHandler),
        {0,vl_idx},
        pl_timerIdxList[vl_idx]
      );
      vl_idx := vl_idx + 1;
    }
    vl_when := vl_when + 1.0;
  }
  var float vl_time := T_meas.read;
  T_meas.stop;
  return vl_time;
}

function f_EPTF_Scheduler_PerfTest_cancel(
  in EPTF_IntegerList pl_timerIdxList)
runs on EPTF_Scheduler_CT
return float // time taken
{
  var integer vl_idx := 0;
  timer T_meas := 100000.0;
  T_meas.start;
  for(vl_idx := 0; vl_idx < sizeof(pl_timerIdxList); vl_idx := vl_idx + 1) {
    f_EPTF_SchedulerComp_CancelEvent(pl_timerIdxList[vl_idx]);
  }
  var float vl_time := T_meas.read;
  T_meas.stop;
  return vl_time;
}

testcase tc_EPTF_Scheduler_PerfTest_cluster()
runs on EPTF_Scheduler_CT
{
  var integer vl_interval := 0, vl_cluster := 0;
  var EPTF_IntegerList vl_timerIdxList := {};

  for(vl_interval := tsp_minDistinctIntervalSize;
      vl_interval <= tsp_maxDistinctIntervalSize;
      vl_interval := vl_interval * tsp_distinctIntervalSizeMul + tsp_distinctIntervalSizeInc) {
    for(vl_cluster := tsp_minClusterSize;
        vl_cluster <= tsp_maxClusterSize;
        vl_cluster := vl_cluster * tsp_clusterSizeMul + tsp_clusterSizeInc) {

      f_EPTF_SchedulerComp_initEventQueue(); // empties the queue but not start the sch clock and does not activate its default altstep.

      action("intervals: ", vl_interval, ", clusters: ", vl_cluster);
      var float vl_time := f_EPTF_Scheduler_PerfTest_schedule(
        vl_interval,
        vl_cluster,
        vl_timerIdxList);
      action("scheduling: ", vl_time, " seconds.");
      vl_time := f_EPTF_Scheduler_PerfTest_cancel(vl_timerIdxList);
      action("cancelling: ", vl_time, " seconds.");
      vl_timerIdxList := {};
    }
  }
}

/////////////////////////////

function f_EPTF_Test_PerfTest_eventHandler(in EPTF_ScheduledAction pl_action, in integer pl_eventIndex)
runs on EPTF_Scheduler_Test_CT return boolean
{
  var integer vl_i, vl_n:=sizeof(pl_action.actionId);
  if(sizeof(pl_action.actionId)==0)
  {
    action("A non cancel event occured!");
    return false;
  }
  for(vl_i := 0; vl_i < vl_n; vl_i := vl_i + 1)
  {
    f_EPTF_SchedulerComp_CancelEvent(pl_action.actionId[vl_i]);
  }
  return true;
}

function f_EPTF_Test_PerfTest_generatorEventHandler(in EPTF_ScheduledAction pl_action, in integer pl_eventIndex)
runs on EPTF_Scheduler_Test_CT return boolean
{
  //v_event1DataList stores data of the generator events
  v_event1DataList[pl_action.actionId[0]].eventCounter := v_event1DataList[pl_action.actionId[0]].eventCounter + 1;
  //re-schedule the generator event
  f_EPTF_SchedulerComp_scheduleAction(pl_action.when + v_event1DataList[pl_action.actionId[0]].period, refers(f_EPTF_Test_PerfTest_generatorEventHandler),
    pl_action.actionId, v_event1DataList[pl_action.actionId[0]].eventIdx);

  var EPTF_IntegerList vl_eventIdxList;//to store the scheduled eventIds
  var integer vl_i;
  var float vl_when, vl_whenFirstEvent;  
  vl_whenFirstEvent := pl_action.when + v_actualHoldingTime;  
  vl_when:=vl_whenFirstEvent+0.2;
  
  for(vl_i := 0; vl_i < v_actualNofSchEvents-1; vl_i := vl_i + 1)
  {
    f_EPTF_SchedulerComp_scheduleAction(vl_when, refers(f_EPTF_Test_PerfTest_eventHandler), {}, vl_eventIdxList[vl_i]);
    vl_when := vl_when + 0.2;
  }

  f_EPTF_SchedulerComp_scheduleAction(vl_whenFirstEvent, refers(f_EPTF_Test_PerfTest_eventHandler), vl_eventIdxList, vl_eventIdxList[vl_i]);

  v_eventCounter := v_eventCounter + v_actualNofSchEvents;

  return true;
}

function f_EPTF_Scheduler_Test_Perftest_measEventHandler(in EPTF_ScheduledAction pl_action, in integer pl_eventIndex)
runs on EPTF_Scheduler_Test_CT return boolean
{
  var integer vl_i, vl_burstSize := sizeof(v_event1DataList);
  if(v_event1Data.eventCounter < tsp_nofMeasurements)
  {
    f_EPTF_SchedulerComp_scheduleAction(pl_action.when + 10.0*v_event1Data.period, refers(f_EPTF_Scheduler_Test_Perftest_measEventHandler), {}, v_event1Data.eventIdx);
    v_event1Data.eventCounter := v_event1Data.eventCounter + 1;
    var float vl_cpuLoad := f_EPTF_HostAdmin_Base_getProcessCpuLoad(v_pid);
    var integer vl_memUsg := f_EPTF_HostAdmin_Base_getProcessMemUsage(v_pid);
    action("Measure number: "&log2str(v_event1Data.eventCounter)&" CPU: "&log2str(vl_cpuLoad)&" Memory: "&log2str(vl_memUsg));
    v_loadStatList[sizeof(v_loadStatList)] := {vl_cpuLoad, vl_memUsg};
  }
  return true;
}

function f_EPTF_Scheduler_Test_Perftest_startSchedulingGeneratorEvents()
runs on EPTF_Scheduler_Test_CT
{
  var integer vl_burstSize := (v_actualCPS/10);
  var integer vl_usedCPS := 10 * vl_burstSize; //round the cps to 10
  var float vl_when := f_EPTF_Base_getRelTimeInSecs() + 0.1;//time of first generator event(s)
  var integer vl_i;

  v_result.start_time:=f_TestResultGen_getCurrentTime();

  action("Generating events with CPS: ", log2str(vl_usedCPS));

  v_event1Data.period := 1.0/10.0;//frequency of generator events
  var float vl_wait := 2.0*v_actualHoldingTime;//must be multiple of 0.1
  //start measuring after vl_wait secs with tsp_EPTF_ELEMENTARY_TIMESTEP_PARAM delay from the generator events
  f_EPTF_SchedulerComp_scheduleAction(vl_when + vl_wait + tsp_EPTF_ELEMENTARY_TIMESTEP_PARAM, refers(f_EPTF_Scheduler_Test_Perftest_measEventHandler), {}, v_event1Data.eventIdx); 

  //schedule the generator events
  for(vl_i := 0; vl_i < vl_burstSize; vl_i := vl_i +1)
  {
    v_event1DataList[vl_i] := {0.1, -1, 0, 0};
    v_event1DataList[vl_i].period := 1.0/10.0;
    f_EPTF_SchedulerComp_scheduleAction(vl_when, refers(f_EPTF_Test_PerfTest_generatorEventHandler), {vl_i}, v_event1DataList[vl_i].eventIdx);  
  }
  timer t_wait := 2.0 * v_actualHoldingTime;
  t_wait.start;


  v_result.parameter_list:=
  {
    {name:="Holding time",unit:="sec",base:=log2str(v_actualHoldingTime)},
    {name:="CPS",unit:="call/sec",base:=log2str(v_actualCPS)},
    {name:="Number of scheduled events",unit:="-",base:=log2str(v_actualNofSchEvents)},
    {name:="Number of measurements",unit:="-",base:=log2str(tsp_nofMeasurements)}
  }
  v_result.name:="Scheduler CPU load and memory usage test";
  v_result.purpose:="To get information if we should change the old RBT to the new rafactored one";
  f_TestResultGen_getEnvironment(v_result.environment);

  var float vl_cpuLoad;

  alt{
    [] t_wait.timeout{
        if(v_event1Data.eventCounter < tsp_nofMeasurements)
        {t_wait.start(1.0);repeat}
        else
        {
          v_result.end_time:=f_TestResultGen_getCurrentTime();
          for(vl_i := 0; vl_i < vl_burstSize; vl_i := vl_i +1)
          {
            f_EPTF_SchedulerComp_CancelEvent(v_event1DataList[vl_i].eventIdx);
          }
          vl_cpuLoad:=f_EPTF_Scheduler_Test_Perftest_averageCPULoad();
          v_result.result_list:=
          {
            {name:="CPU load",unit:="%",base:=log2str(vl_cpuLoad)},
            {name:="Memory usage",unit:="kB",base:=log2str(v_loadStatList[sizeof(v_loadStatList)-1].memUsage)}
          }
          action("Average CPU load is: "&log2str(vl_cpuLoad));
        }
      }
  }
}

function f_EPTF_Scheduler_Test_Perftest_averageCPULoad()
runs on EPTF_Scheduler_Test_CT return float
{
  var integer vl_i, vl_n:=sizeof(v_loadStatList), vl_from:=0, vl_to:=vl_n;
  var float vl_tmp, vl_sum := 0.0, vl_avg := 0.0;  
  var boolean swap := true;

  //if the number of measurements count at least 10, then throw away 20% minor and 20% major elements to refine the average
  if(vl_n >= 10)
  {
    while(swap)//bubble sort
    {
      swap:=false;
      for(vl_i:=0; vl_i<vl_n-1;vl_i:=vl_i+1)
      {
        if(v_loadStatList[vl_i].cpuLoad > v_loadStatList[vl_i+1].cpuLoad)
        {
          vl_tmp:=v_loadStatList[vl_i].cpuLoad;
          v_loadStatList[vl_i].cpuLoad:=v_loadStatList[vl_i+1].cpuLoad;
          v_loadStatList[vl_i+1].cpuLoad:=vl_tmp;
          swap:=true;
        }
      }
    }    
    vl_from:=float2int(0.2*int2float(vl_n));
    vl_to:=float2int(0.8*int2float(vl_n))
  }



  for(vl_i:=vl_from; vl_i < vl_to; vl_i:=vl_i+1)
  {    
    action("Calculating average using element "&log2str(vl_i)&". "&log2str(v_loadStatList[vl_i].cpuLoad));
    vl_sum := vl_sum + v_loadStatList[vl_i].cpuLoad;    
  }
  vl_avg := vl_sum / int2float(vl_to-vl_from);
  return vl_avg;
}

function f_EPTF_Scheduler_Test_Perftest(in integer pl_CPS, in float pl_holdingTime, in integer pl_nofSchEvents, in boolean pl_first:=false)
runs on EPTF_Scheduler_Test_CT
{
  f_EPTF_Scheduler_init_CT("PerfTest");
  f_EPTF_HostAdmin_Base_init_CT("PerfTestHostAdminCT");
  v_pid := f_SYS_getpid();
  f_EPTF_HostAdmin_Base_addProcessStat(v_pid);

  v_loadStatList:={};
  v_actualCPS:=pl_CPS;
  v_actualHoldingTime:=pl_holdingTime;
  v_actualNofSchEvents:=pl_nofSchEvents;
  v_event1Data:={period:=0.0, eventIdx:=-1, maxExecutionNum:=0, eventCounter:=0};
  v_event1DataList:={};
  v_eventCounter:=0;
  action("Before generating events! CPU: "&log2str(f_EPTF_HostAdmin_Base_getProcessCpuLoad(v_pid))&"Memory: "&log2str(f_EPTF_HostAdmin_Base_getProcessMemUsage(v_pid)) );
  f_EPTF_Scheduler_Test_Perftest_startSchedulingGeneratorEvents();
  action("Total number of scheduled events: ", log2str(v_eventCounter) );
  if(pl_first)
  {
    f_TestResultGen_writeResult("CPUandMEMUsage.xml", v_result);
  }
  else
  {
    f_TestResultGen_appendResult("CPUandMEMUsage.xml", v_result);
  }
  action("Stopping component...");
  f_EPTF_Base_stop(pass);
}

testcase tc_EPTF_Scheduler_Test_Perftest()
runs on EPTF_Scheduler_Test_CT
{
  f_EPTF_Base_init_CT("MTC");
  var integer vl_i, vl_j, vl_k, vl_numCPS:=sizeof(tsp_cps), vl_numEvents:=sizeof(tsp_nofSchEvents), vl_numHoldingTime:=sizeof(tsp_holdingTime);
  for(vl_i:=0; vl_i<vl_numCPS; vl_i:=vl_i+1)
  {
    for(vl_j:=0; vl_j<vl_numEvents; vl_j:=vl_j+1)
    {
      for(vl_k:=0; vl_k<vl_numHoldingTime; vl_k:=vl_k+1)
      {
        var EPTF_Scheduler_Test_CT newCT:=EPTF_Scheduler_Test_CT.create;
        action("New component created!");
        if(vl_i==0 and vl_j==0 and vl_k==0)
        {
          newCT.start(f_EPTF_Scheduler_Test_Perftest(tsp_cps[vl_i], tsp_holdingTime[vl_k], tsp_nofSchEvents[vl_j], true));
        }
        else
        {

          newCT.start(f_EPTF_Scheduler_Test_Perftest(tsp_cps[vl_i], tsp_holdingTime[vl_k], tsp_nofSchEvents[vl_j]));
        }
        newCT.done;
        action("Component done!");
      }
    }
  }  
}

} // group PerformanceTests

function f_EPTF_Scheduler_Test_failedActionHandler(
  in EPTF_ScheduledAction pl_action, in integer pl_eventIndex) runs on EPTF_Scheduler_Test_CT
  return boolean {

  v_actualNofSchEvents := v_actualNofSchEvents + 1;
  return (v_actualNofSchEvents!=1); // action handling fails only for the first event
}

function f_EPTF_Scheduler_Test_checkWarning_preamble_FN(in charstring pl_message) runs on EPTF_Scheduler_Test_CT {
  if (match(pl_message,pattern "*f_EPTF_SchedulerComp_performActions has failed to handle event:*")) {
    v_eventCounter := 1;
    setverdict(pass,"Expected warning received");
  }
}

testcase tc_EPTF_Scheduler_Test_failedEvent()
runs on EPTF_Scheduler_Test_CT
{
  f_EPTF_Scheduler_init_CT("failedEvent");
  
  v_eventCounter := 0;
  v_actualNofSchEvents := 0;
  
  f_EPTF_SchedulerComp_refreshSnapshotTime();
  
  f_EPTF_Logging_registerPreambleFn(refers(f_EPTF_Scheduler_Test_checkWarning_preamble_FN));
  
  var integer vl_eventIdx;
  f_EPTF_SchedulerComp_scheduleAction(
    pl_when := f_EPTF_SchedulerComp_snapshotTime()+1.0,
    pl_actionHandler :=  refers(f_EPTF_Scheduler_Test_failedActionHandler), 
    pl_action := {},
    pl_eventIndex := vl_eventIdx
  );
   f_EPTF_SchedulerComp_scheduleAction(
    pl_when := f_EPTF_SchedulerComp_snapshotTime()+2.0,
    pl_actionHandler :=  refers(f_EPTF_Scheduler_Test_failedActionHandler), 
    pl_action := {},
    pl_eventIndex := vl_eventIdx
  );
  f_EPTF_SchedulerComp_scheduleAction(
    pl_when := f_EPTF_SchedulerComp_snapshotTime()+3.0,
    pl_actionHandler :=  refers(f_EPTF_Scheduler_Test_failedActionHandler), 
    pl_action := {},
    pl_eventIndex := vl_eventIdx
  );
 
  timer t_wait := 5.0;
  t_wait.start;
  t_wait.timeout; // event is handled before timeout;
  
  if (v_eventCounter!=1) {
    setverdict(fail,"Expected warning did not appear")
  }
  
  if (v_actualNofSchEvents<3) {
    setverdict(fail,"Too few events are handled: ", v_actualNofSchEvents);
  }
  
  f_EPTF_Base_stop(none);
}


testcase tc_EPTF_Scheduler_Test_overloadDetection() runs on EPTF_Scheduler_OverloadTest_CT {
  
  
  f_EPTF_Scheduler_OverloadTest_init("overloadDetection");
  f_BurnCPU_setTargetLoad(0.9);
  f_BurnCPU_setDeltaT(0.05);
  f_EPTF_Scheduler_setMaxLoadThreshold(0.99);
  f_EPTF_Scheduler_setLoadMeasurementPeriod(1.0);
  v_readLoadPeriod := f_EPTF_Scheduler_getLoadMeasurementInterval();
  
  timer t_wait := 62.0;
  t_wait.start;
  t_wait.timeout;
  

  // activate the overload generator altstep  
}


control {
  execute(tc_EPTF_Scheduler_Test_demo());
  execute(tc_EPTF_Scheduler_Test_timerReadSpeed());
  execute(tc_EPTF_Scheduler_Test_schedulerWritingSpeed_incremental_linear());
  execute(tc_EPTF_Scheduler_Test_schedulerWritingSpeed_decremental_linear());
  execute(tc_EPTF_Scheduler_Test_schedulerWritingSpeed_random_linear());
  execute(tc_EPTF_Scheduler_Test_schedulerWritingSpeed_incremental_exp());
  execute(tc_EPTF_Scheduler_Test_schedulerWritingSpeed_decremental_exp());
  execute(tc_EPTF_Scheduler_Test_schedulerWritingSpeed_random_exp());
  execute(tc_EPTF_Scheduler_Test_schedulerWriteThenReadSpeed_incr_exp());
  execute(tc_EPTF_Scheduler_Test_schedulerWriteThenReadSpeed_decr_exp());
  execute(tc_EPTF_Scheduler_Test_schedulerWriteThenReadSpeed_rand_exp());
  execute(tc_EPTF_Scheduler_Test_dte_handler());
  execute(tc_EPTF_Scheduler_Test_failedEvent());
}//control

}//module
