///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
//  Module: EPTF_UIHandler_Test_Definitions
// 
//  Purpose:
//    This module contains tests of EPTF_CLL_UIHandler_CT.
// 
//  Module depends on:
//   TBD
// 
//  Current Owner:
//    Laszlo Skumat (elszsku)
// 
//  Last Review Date:
//    2008-xx-xx
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////////
module EPTF_UIHandler_TestReconnect
{
import from EPTF_CLL_UIHandler_Definitions all;
import from EPTF_CLL_UIHandler_WidgetFunctions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_LoggingServer_Definitions all;
import from EPTF_CLL_LoggingServer_Functions all;
import from EPTF_CLL_LoggingClient_Functions all;

type component UIHandler_Test_CT extends EPTF_UIHandler_CT, EPTF_LoggingServer_CT, EPTF_LoggingClient_CT {}

const charstring c_reconnectGUI := "
<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>
  <tabbox flex='1.0' id='EPTF_Logging_tabbox'>
    <tabs>
      <tab id='v_Tab_Global' label='GLOBAL Logging' />
      <iterator element='SelectionTypes' id='sel' source='Logging'>
        <tab id='v_Tab%sel%' label='%sel%' />
      </iterator>
    </tabs>
    <tabpanels>
      <tabpanel id='v_Tabpanel_Global' orientation='vertical'>
        <tree hidecolumnpicker='true' id='Global_CheckBoxes' >
          <treecols>
            <treecol editable='false' flex='1.0' label='Client Name' widgetType='string' />
            <treecol flex='1.0' label='Enable Logging' widgetType='checkBox' />
          </treecols>
          <treechildren>
            <treeitem>
              <treerow>
                <treecell id='Global_ClientName' label='All Components' tooltiptext='Name of the client component' />
                <treecell label='true'>
                  <externaldata element='LogEnabled' source='Logging' />
                </treecell>
              </treerow>
              <iterator element='Clients' id='client' source='Logging'>
                <treerow>
                  <treecell label='%client%' tooltiptext='Name of the client component' />
                  <treecell label='true'>
                    <externaldata element='LogEnabled' source='LoggingClient' ptcname='%client%'/>
                  </treecell>
                </treerow>
              </iterator>
            </treeitem>
          </treechildren>
        </tree>
      </tabpanel>
      <iterator element='SelectionTypes' id='sel' source='Logging'>
        <tabpanel id='Tabpanel%sel%' orientation='vertical'>
          <tabbox flex='1.0' id='ComponentType_tabbox%sel%'>
            <tabs>
              <iterator element='ComponentTypes' id='type' source='Logging'>
                <params>
                  <dataparam name='Selection' value='%sel%' />
                </params>
                <tab id='%type%.%sel%.Tab' label='%type%' />
              </iterator>
            </tabs>
            <tabpanels>
              <iterator element='ComponentTypes' id='type' source='Logging'>
                <params>
                  <dataparam name='Selection' value='%sel%' />
                </params>
                <tabpanel id='%type%.%sel%.Tab' orientation='vertical'>
                  <hbox flex='1.0' orientation='vertical'>
                    <tree id='%type%.%sel%.ComponentTree' hidecolumnpicker='true'>
                      <treecols>
                        <treecol editable='false' flex='1.0' label='Component Type' widgetType='string' />
                        <treecol flex='1.0' label='Enable Logging' widgetType='checkBox' />
                        <iterator element='Classes' id='class' source='Logging'>
                          <params>
                            <dataparam name='ComponentType' value='%type%' />
                          </params>
                          <treecol flex='1.0' label='%class%' widgetType='checkBox' />
                        </iterator>
                      </treecols>
                      <treechildren>
                        <treeitem>
                          <treerow>
                            <treecell label='%type%' tooltiptext='Name of the component' />
                            <treecell label='true'>
                              <externaldata element='LogEnabled' source='Logging'>
                                <params>
                                  <dataparam name='ComponentType' value='%type%' />
                                </params>
                              </externaldata>
                            </treecell>
                            <iterator element='Classes' id='class' source='Logging'>
                              <params>
                                <dataparam name='ComponentType' value='%type%' />
                              </params>
                              <treecell label='true'>
                                <externaldata element='LogEnabled' source='Logging'>
                                  <params>
                                    <dataparam name='ComponentType' value='%type%' />
                                    <dataparam name='Class' value='%class%' />
                                  </params>
                                </externaldata>
                              </treecell>
                            </iterator>
                          </treerow>
                        </treeitem>
                      </treechildren>
                    </tree>
                    <tree id='%type%.%sel%.ClientTree' hidecolumnpicker='true' >
                      <treecols>
                        <treecol editable='false' flex='1.0' label='Client Name' widgetType='string' />
                        <treecol flex='1.0' label='Enable Logging' widgetType='checkBox' />
                        <iterator element='Classes' id='class' source='Logging'>
                          <params>
                            <dataparam name='ComponentType' value='%type%' />
                          </params>
                          <treecol flex='1.0' label='%class%' widgetType='checkBox' />
                        </iterator>
                        <treecol flex='1.0' label='Empty' widgetType='checkBox' />
                      </treecols>
                      <treechildren>
                        <treeitem>
                          <iterator element='Clients' id='client' source='Logging'>
                            <params>
                              <dataparam name='ComponentType' value='%type%' />
                            </params>
                            <treerow>
                              <treecell label='%client%' tooltiptext='Name of the component' />
                              <treecell label='true'>
                                <externaldata element='LogEnabled' source='LoggingClient' ptcname='%client%'>
                                  <params>
                                    <dataparam name='ComponentType' value='%type%' />
                                  </params>
                                </externaldata>
                              </treecell>
                              <treecell label='' tooltiptext='Empty cell' />
                              <iterator element='Classes' id='class' source='Logging'>
                                <params>
                                  <dataparam name='ComponentType' value='%type%' />
                                </params>
                                <treecell label='true'>
                                  <externaldata element='LogEnabled' source='LoggingClient' ptcname='%client%'>
                                    <params>
                                      <dataparam name='ComponentType' value='%type%' />
                                      <dataparam name='Class' value='%class%' />
                                    </params>
                                  </externaldata>
                                </treecell>
                              </iterator>
                            </treerow>
                            <treerow>
                              <treecell label='bubu' tooltiptext='Empty cell1' />
                            </treerow>
                          </iterator>
                        </treeitem>
                      </treechildren>
                    </tree>
                  </hbox>
                </tabpanel>
              </iterator>
            </tabpanels>
          </tabbox>
        </tabpanel>
      </iterator>
    </tabpanels>
  </tabbox>
</Widgets>
"
modulepar boolean tsp_UIHandler_reconnect_connectGUI := true; 
testcase tc_UIHandler_reconnect() runs on UIHandler_Test_CT{
  f_EPTF_UIHandler_init_CT(%definitionId, tsp_UIHandler_reconnect_connectGUI);
  f_EPTF_LoggingServer_init_CT(%definitionId, self);
  f_EPTF_LoggingClient_init_CT(%definitionId, self, self);
  f_EPTF_UIHandler_clearGUI();
  f_EPTF_UIHandler_addWindow();
  //f_EPTF_UIHandler_addMainTabbox();
  f_EPTF_UIHandler_createGUI(c_reconnectGUI,tsp_EPTF_GUI_Main_Window_WidgetId);
  action("GUI ready ----");
  f_EPTF_Base_wait4Shutdown();
}

}  // end of module
