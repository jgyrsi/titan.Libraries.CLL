///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_UIHandler_GUIVariables_Test_Testcases
// 
//  Purpose:
//    This module contains tests of EPTF_CLL_UIHandler_CT.
// 
//  Module depends on:
//   TBD
// 
//  Current Owner:
//    EKOVIST
// 
//  Last Review Date:
//    2010-xx-xx
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////////
module EPTF_UIHandler_GUIVariables_Test_Testcases
{
//=========================================================================
// Import Part
//=========================================================================
import from EPTF_CLL_UIHandler_Definitions all;
import from EPTF_CLL_UIHandler_WidgetFunctions all;
import from EPTF_CLL_Base_Functions all;
//import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_DataSource_Functions all;
import from EPTF_CLL_DataSource_Definitions all;
import from EPTF_CLL_DataSourceClient_Functions all;
import from EPTF_CLL_Variable_Definitions all;
import from EPTF_CLL_Variable_Functions all;

//=========================================================================
// Component Types
//=========================================================================

type component UIHandler_GUIVariables_Test_CT extends EPTF_UIHandler_CT, EPTF_Var_CT, EPTF_DataSourceClient_CT
{
  	var boolean v_UIHandler_GUIVariables_Test_initialized := false;
    var integer v_startBecauseDSIsReady := 0;
    var charstring v_gui := "";   
}

///////////////////////////////////////////////////////////
// Type: EPTF_GUIVariables_Test_RequestedList
//
// Purpose:
//   Record of charstrings to provide a container for requested datatypes
//
// Elements:
//   record of *charstring*
///////////////////////////////////////////////////////////
type record EPTF_GUIVariables_Test_RequestedItem {
   charstring     requestType,
   boolean 		  testResult
}
type record of EPTF_GUIVariables_Test_RequestedItem EPTF_GUIVariables_Test_RequestedList;
type record EPTF_GUIVariables_Test_Case {
   charstring     originalType,
   EPTF_GUIVariables_Test_RequestedList requestedList
}
type record of EPTF_GUIVariables_Test_Case EPTF_GUIVariables_Test_CaseList;
type record EPTF_GUIVariables_Test_CasePanel {
   charstring     panelName,
   EPTF_GUIVariables_Test_CaseList panelList
}
type record of EPTF_GUIVariables_Test_CasePanel EPTF_GUIVariables_Test_PanelsList;

//=========================================================================
// Constants
//=========================================================================
const charstring cg_UIHandler_GUIVariables_Test_source := "UIHandler_GUIVariables"
const charstring cg_xulmask_treecell := "treecell"

const charstring cg_varNameEnd := "VarGUI";
const charstring cg_varNameStart_integerField := "integerField";
const charstring cg_varNameStart_floatField := "floatField";
const charstring cg_varNameStart_string := "string";
const charstring cg_varNameStart_statusLED := "statusLED";
const charstring cg_varNameStart_statusLEDWithText := "statusLEDWithText";
const charstring cg_varNameStart_checkBox := "checkBox";
const charstring cg_varNameStart_none_ := "none_";
const charstring cg_varNameStart_image := "image";
const charstring cg_varNameStart_pushButton := "pushButton";
const charstring cg_varNameStart_toggleButton := "toggleButton";
const charstring cg_varNameStart_trace := "trace";
const charstring cg_varNameStart_valueList := "valueList";
const charstring cg_varNameStart_nemtommi := "nemtommi";

const float cg_waitForVariables := 60.0; 
const float cg_waitForTest := 10.0; 

//=========================================================================
// Modulepars
//=========================================================================
//modulepar charstring tsp_demoHost := "127.0.0.1";
//modulepar EPTF_CharstringList tsp_uiDemoHostList := {};
modulepar EPTF_GUIVariables_Test_PanelsList tsp_TestedPanelsList := {};


  //=========================================================================
  // Functions
  //=========================================================================
  // Init - cleanup
  // ----------------------------------------------------------------------
  function f_UIHandler_GUIVariables_Test_init_CT(in charstring pl_selfName) runs on UIHandler_GUIVariables_Test_CT {
  	if (v_UIHandler_GUIVariables_Test_initialized) {
    	return;
  	}
  	v_UIHandler_GUIVariables_Test_initialized := true;
  	// call all the init functions of the components that your component extends _explicitly_:  
    f_EPTF_UIHandler_init_CT(pl_selfName, true);
    f_EPTF_Var_init_CT(pl_selfName);
    f_EPTF_DataSourceClient_init_CT(pl_selfName,self);
    f_initVariables();
    f_EPTF_DataSource_registerReadyCallback(refers(f_EPTF_UIHandler_Test_DataSourceClientReady ));
    /* 	in charstring pl_source,
  		in charstring pl_ptcName := "",
  		in fcb_EPTF_DataSourceClient_dataHandler pl_dataHandler,
  		in EPTF_DataSource_CT pl_sourceCompRef := null*/
    f_EPTF_DataSourceClient_registerData(
    	pl_source:=pl_selfName,
    	pl_ptcName:=f_EPTF_Base_selfName(),
    	pl_dataHandler:=refers(f_EPTF_UIHandlerTest_dataHandler)
    );  
  
    f_EPTF_DataSourceClient_sendReady(
    	pl_source:=cg_UIHandler_GUIVariables_Test_source,
    	pl_ptcName:=f_EPTF_Base_selfName()
    );
    
  	// register your cleanup function:
  	f_EPTF_Base_registerCleanup(refers(f_UIHandler_GUIVariables_Test_cleanup_CT));

  	log("----A INIT DONE[UIHandler_GUIVariables_Test]----");
  }


  function f_UIHandler_GUIVariables_Test_cleanup_CT() runs on UIHandler_GUIVariables_Test_CT {
  	if (v_UIHandler_GUIVariables_Test_initialized == false) {
    	return;
  	}  
  	v_UIHandler_GUIVariables_Test_initialized := false;
  }
  // ----------------------------------------------------------------------
  
  // Variables
  // ----------------------------------------------------------------------
  function f_initVariables() runs on UIHandler_GUIVariables_Test_CT{
    // integerField, 
    // floatField,
    // string,  
    // statusLED,
    // statusLEDWithText,
    // checkBox,
    // image,
    // none_,
    // pushButton,
    // toggleButton,
    // trace,
    // valueList
    // nemtommi
    var integer vl_intVarIdx;
    f_EPTF_Var_newInt(cg_varNameStart_integerField & cg_varNameEnd, 123, vl_intVarIdx); 
    f_EPTF_Var_newFloat(cg_varNameStart_floatField & cg_varNameEnd, 234.56, vl_intVarIdx); 
	f_EPTF_Var_newCharstring(cg_varNameStart_string & cg_varNameEnd,"IGEN", vl_intVarIdx); 
	f_EPTF_Var_newStatusLED(cg_varNameStart_statusLED & cg_varNameEnd, {led_green, omit}, vl_intVarIdx);
	f_EPTF_Var_newStatusLED(cg_varNameStart_statusLEDWithText & cg_varNameEnd, {led_red, "ONE"}, vl_intVarIdx);
	f_EPTF_Var_newBool(cg_varNameStart_checkBox & cg_varNameEnd, true, vl_intVarIdx);
	f_EPTF_Var_newCharstring(cg_varNameStart_none_ & cg_varNameEnd, "NONE_", vl_intVarIdx);
	f_EPTF_Var_newCharstring(cg_varNameStart_image & cg_varNameEnd, "IMAGE", vl_intVarIdx);
    f_EPTF_Var_newInt(cg_varNameStart_pushButton & cg_varNameEnd, 11, vl_intVarIdx);
	f_EPTF_Var_newBool(cg_varNameStart_toggleButton  & cg_varNameEnd, true, vl_intVarIdx);
    f_EPTF_Var_newInt(cg_varNameStart_trace & cg_varNameEnd,22, vl_intVarIdx);
	f_EPTF_Var_newIntegerlist(cg_varNameStart_valueList & cg_varNameEnd, {33,44,55,66,77,88,99}, vl_intVarIdx);
	f_EPTF_Var_newCharstring(cg_varNameStart_nemtommi & cg_varNameEnd, "NEMTOMMI", vl_intVarIdx);           
  }
  
  function f_EPTF_UIHandlerTest_dataHandler(
  	out charstring pl_dataVarName,
 	in charstring pl_source,
  	in charstring pl_ptcName,
  	in charstring pl_element,
  	in EPTF_DataSource_Params pl_params) runs on UIHandler_GUIVariables_Test_CT return integer
  {
  	if(pl_source!=cg_UIHandler_GUIVariables_Test_source)
  	{
		return 1;
  	}
  	if(f_EPTF_Var_getId(pl_element)!=-1)
  	{
    	pl_dataVarName := pl_element;
    	return 0;
  	}  
  	return 2;
  }
  

  function f_EPTF_UIHandler_Test_DataSourceClientReady(
    in charstring pl_source,
    in charstring pl_ptcName)
  runs on UIHandler_GUIVariables_Test_CT{
    action("Ready received[",pl_source,"]:",pl_ptcName);
    v_startBecauseDSIsReady := v_startBecauseDSIsReady + 1;
  }
  // ----------------------------------------------------------------------
  
  // Test functions
  // ----------------------------------------------------------------------
  function f_EPTF_UIHandler_treecell_get_XUL( 
	in charstring pl_xulmaskname, 
	in charstring pl_origtype, 
	in charstring pl_testtype, 
	charstring pl_varName) runs on UIHandler_GUIVariables_Test_CT   return charstring
  {
    v_gui := "";
    // treecell
    if(pl_xulmaskname == cg_xulmask_treecell) {
	  v_gui := 
  		"  <Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>"&
  		"  <hbox flex='1.0' id='UIHandler_widgetId_0' orientation='horizontal'>"&
  		"    <tabpages flex='1.0' id='EPTF_GUIVariables_Tabbox'>"&
  		"            <tabpage id='GUIVariables_Tabpanel1_Id' label='UIHandler_GUIVariables' orientation='vertical'>"&
  		"                <tree hidecolumnpicker='true' id='GUIVariablesTreeId'>"&
  		"                    <treecols>"&
  		"                        <treecol editable='true' flex='1.0' id='OrigColID' label='Orig["&pl_origtype&"]' widgetType='"&pl_origtype&"'/>"&
  		"                        <treecol editable='true' flex='1.0' id='TestColID' label='Test["&pl_testtype&"]' widgetType='"&pl_testtype&"'/>"&
  		"                    </treecols>"&
  		"                    <treechildren>"&
  		"                        <treeitem>"&					
  		"								<treerow>"&
  		"									<treecell id='ID1' label=''>"&
  		"										<externaldata element='"&pl_varName&"' source='"&cg_UIHandler_GUIVariables_Test_source&"' />"&
  		"									</treecell>"&		
  		"									<treecell id='ID2' label=''>"&
  		"										<externaldata element='"&pl_varName&"' source='"&cg_UIHandler_GUIVariables_Test_source&"' />"&
  		"									</treecell>"&
  		"								</treerow>"&
  		"                        </treeitem>"&
  		"                    </treechildren>"&
  		"                </tree>"&
  		"           </tabpage>"&
  		"    </tabpages>"&
  		"  </hbox>"&
  		"</Widgets>";    
    } else {
    }
    return(v_gui);  
  }

function f_EPTF_UIHandler_customGUI_test(	
			in charstring pl_panel, 
			in charstring pl_origtype, 
			in charstring pl_testtype, 
			in charstring pl_varname,
			in boolean pl_testresult)
  runs on UIHandler_GUIVariables_Test_CT
  {
    f_UIHandler_GUIVariables_Test_init_CT(cg_UIHandler_GUIVariables_Test_source);
    if(pl_testresult == false)
    {
      	action(log2str("f_EPTF_Base_setNegativeTestMode:", pl_panel,"-", pl_origtype,"-",pl_testtype));
    	f_EPTF_Base_setNegativeTestMode(true);
    	f_EPTF_Base_setExpectedErrorMsg("*The specified *: type incompatibility!");
    } 
 	
 	f_EPTF_UIHandler_clearGUI();
    f_EPTF_UIHandler_addWindow(); 
    
    timer TL_guard, TL_alt;
    TL_guard.start( cg_waitForVariables );
    TL_alt.start( 0.0 );
    alt{
      [] TL_guard.timeout{
        setverdict(fail,"Timeout during config");
        f_EPTF_Base_stopAll();
      }
      [v_startBecauseDSIsReady > 0] TL_alt.timeout{}
    };
 	   
    var charstring vl_lastgui := f_EPTF_UIHandler_treecell_get_XUL(pl_panel,pl_origtype, pl_testtype, pl_varname);
    f_EPTF_UIHandler_createGUI(vl_lastgui, tsp_EPTF_GUI_Main_Window_WidgetId);

	timer TL_wait;
    TL_wait.start( cg_waitForTest );
    TL_wait.timeout;
           
    setverdict(pass);
    f_EPTF_Base_cleanup_CT();
  }
  // ----------------------------------------------------------------------

  ///////////////////////////////////////////////////////////
  //  Testcase: tc_EPTF_UIHandler_customGUI_test_OK
  // 
  //  Purpose:   
  //
  ///////////////////////////////////////////////////////////

  
  testcase tc_EPTF_UIHandler_customGUI_test(in charstring pl_panel, in charstring pl_origtype, in charstring pl_testtype, in boolean pl_testResult)
  	runs on UIHandler_GUIVariables_Test_CT
  {
    action(log2str("UIHandler_customGUI_test:", pl_panel,"-", pl_origtype,"-",pl_testtype));
    f_EPTF_UIHandler_customGUI_test(pl_panel, pl_origtype, pl_testtype, pl_origtype & cg_varNameEnd, pl_testResult);
    setverdict(pass);
  }
 
  testcase tc_EPTF_UIHandler_customGUI_test_integerField(in charstring pl_panel, in charstring pl_origtype, in boolean pl_testResult)
  	runs on UIHandler_GUIVariables_Test_CT
  {
    action(log2str("tc_EPTF_UIHandler_customGUI_test_integerField:", pl_panel,"-", pl_origtype,"-",cg_varNameStart_integerField));
    f_EPTF_UIHandler_customGUI_test(pl_panel, pl_origtype, cg_varNameStart_integerField, pl_origtype & cg_varNameEnd, pl_testResult);
    setverdict(pass);
  }
  testcase tc_EPTF_UIHandler_customGUI_test_floatField(in charstring pl_panel, in charstring pl_origtype, in boolean pl_testResult)
  	runs on UIHandler_GUIVariables_Test_CT
  {
    action(log2str("tc_EPTF_UIHandler_customGUI_test_floatField:", pl_panel,"-", pl_origtype,"-",cg_varNameStart_floatField));
    f_EPTF_UIHandler_customGUI_test(pl_panel, pl_origtype, cg_varNameStart_floatField, pl_origtype & cg_varNameEnd, pl_testResult);
    setverdict(pass);
  }
  testcase tc_EPTF_UIHandler_customGUI_test_string(in charstring pl_panel, in charstring pl_origtype, in boolean pl_testResult)
  	runs on UIHandler_GUIVariables_Test_CT
  {
    action(log2str("tc_EPTF_UIHandler_customGUI_test_string:", pl_panel,"-", pl_origtype,"-",cg_varNameStart_string));
    f_EPTF_UIHandler_customGUI_test(pl_panel, pl_origtype, cg_varNameStart_string, pl_origtype & cg_varNameEnd, pl_testResult);
    setverdict(pass);
  }
  testcase tc_EPTF_UIHandler_customGUI_test_statusLED(in charstring pl_panel, in charstring pl_origtype, in boolean pl_testResult)
  	runs on UIHandler_GUIVariables_Test_CT
  {
    action(log2str("tc_EPTF_UIHandler_customGUI_test_statusLED:", pl_panel,"-", pl_origtype,"-",cg_varNameStart_statusLED));
    f_EPTF_UIHandler_customGUI_test(pl_panel, pl_origtype, cg_varNameStart_statusLED, pl_origtype & cg_varNameEnd, pl_testResult);
    setverdict(pass);
  }
  testcase tc_EPTF_UIHandler_customGUI_test_statusLEDWithText(in charstring pl_panel, in charstring pl_origtype, in boolean pl_testResult)
  	runs on UIHandler_GUIVariables_Test_CT
  {
    action(log2str("tc_EPTF_UIHandler_customGUI_test_statusLEDWithText:", pl_panel,"-", pl_origtype,"-",cg_varNameStart_statusLEDWithText));
    f_EPTF_UIHandler_customGUI_test(pl_panel, pl_origtype, cg_varNameStart_statusLEDWithText, pl_origtype & cg_varNameEnd, pl_testResult);
    setverdict(pass);
  }
  testcase tc_EPTF_UIHandler_customGUI_test_checkBox(in charstring pl_panel, in charstring pl_origtype, in boolean pl_testResult)
  	runs on UIHandler_GUIVariables_Test_CT
  {
    action(log2str("tc_EPTF_UIHandler_customGUI_test_checkBox:", pl_panel,"-", pl_origtype,"-",cg_varNameStart_checkBox));
    f_EPTF_UIHandler_customGUI_test(pl_panel, pl_origtype, cg_varNameStart_checkBox, pl_origtype & cg_varNameEnd, pl_testResult);
    setverdict(pass);
  }
  testcase tc_EPTF_UIHandler_customGUI_test_none_(in charstring pl_panel, in charstring pl_origtype, in boolean pl_testResult)
  	runs on UIHandler_GUIVariables_Test_CT
  {
    action(log2str("tc_EPTF_UIHandler_customGUI_test_none_:", pl_panel,"-", pl_origtype,"-",cg_varNameStart_none_));
    f_EPTF_UIHandler_customGUI_test(pl_panel, pl_origtype, cg_varNameStart_none_, pl_origtype & cg_varNameEnd, pl_testResult);
    setverdict(pass);
  }
  testcase tc_EPTF_UIHandler_customGUI_test_image(in charstring pl_panel, in charstring pl_origtype, in boolean pl_testResult)
  	runs on UIHandler_GUIVariables_Test_CT
  {
    action(log2str("tc_EPTF_UIHandler_customGUI_test_image:", pl_panel,"-", pl_origtype,"-",cg_varNameStart_image));
    f_EPTF_UIHandler_customGUI_test(pl_panel, pl_origtype, cg_varNameStart_image, pl_origtype & cg_varNameEnd, pl_testResult);
    setverdict(pass);
  }
  testcase tc_EPTF_UIHandler_customGUI_test_pushButton(in charstring pl_panel, in charstring pl_origtype, in boolean pl_testResult)
  	runs on UIHandler_GUIVariables_Test_CT
  {
    action(log2str("tc_EPTF_UIHandler_customGUI_test_pushButton:", pl_panel,"-", pl_origtype,"-",cg_varNameStart_pushButton));
    f_EPTF_UIHandler_customGUI_test(pl_panel, pl_origtype, cg_varNameStart_pushButton, pl_origtype & cg_varNameEnd, pl_testResult);
    setverdict(pass);
  }
  testcase tc_EPTF_UIHandler_customGUI_test_toggleButton(in charstring pl_panel, in charstring pl_origtype, in boolean pl_testResult)
  	runs on UIHandler_GUIVariables_Test_CT
  {
    action(log2str("tc_EPTF_UIHandler_customGUI_test_toggleButton:", pl_panel,"-", pl_origtype,"-",cg_varNameStart_toggleButton));
    f_EPTF_UIHandler_customGUI_test(pl_panel, pl_origtype, cg_varNameStart_toggleButton, pl_origtype & cg_varNameEnd, pl_testResult);
    setverdict(pass);
  }
  testcase tc_EPTF_UIHandler_customGUI_test_trace(in charstring pl_panel, in charstring pl_origtype, in boolean pl_testResult)
  	runs on UIHandler_GUIVariables_Test_CT
  {
    action(log2str("tc_EPTF_UIHandler_customGUI_test_trace:", pl_panel,"-", pl_origtype,"-",cg_varNameStart_trace));
    f_EPTF_UIHandler_customGUI_test(pl_panel, pl_origtype, cg_varNameStart_trace, pl_origtype & cg_varNameEnd, pl_testResult);
    setverdict(pass);
  }
  testcase tc_EPTF_UIHandler_customGUI_test_valueList(in charstring pl_panel, in charstring pl_origtype, in boolean pl_testResult)
  	runs on UIHandler_GUIVariables_Test_CT
  {
    action(log2str("tc_EPTF_UIHandler_customGUI_test_valueList:", pl_panel,"-", pl_origtype,"-",cg_varNameStart_valueList));
    f_EPTF_UIHandler_customGUI_test(pl_panel, pl_origtype, cg_varNameStart_valueList, pl_origtype & cg_varNameEnd, pl_testResult);
    setverdict(pass);
  }
  testcase tc_EPTF_UIHandler_customGUI_test_nemtommi(in charstring pl_panel, in charstring pl_origtype, in boolean pl_testResult)
  	runs on UIHandler_GUIVariables_Test_CT
  {
    action(log2str("tc_EPTF_UIHandler_customGUI_test_nemtommi:", pl_panel,"-", pl_origtype,"-",cg_varNameStart_nemtommi));
    f_EPTF_UIHandler_customGUI_test(pl_panel, pl_origtype, cg_varNameStart_nemtommi, pl_origtype & cg_varNameEnd, pl_testResult);
    setverdict(pass);
  }
  testcase tc_EPTF_test_END()
  	runs on UIHandler_GUIVariables_Test_CT
  {
    setverdict(pass);
  }  

//=========================================================================
// Control
//=========================================================================
  control{
  

 // 	execute(tc_EPTF_UIHandler_customGUI_test_treecell_statusledWithText_statusLEDWithText());
 // 	execute(tc_EPTF_UIHandler_customGUI_test_treecell_statusled_statusLED());
 // 	execute(tc_EPTF_UIHandler_customGUI_test_treecell_float_statusLED()); 
 /*
  wtUnknown,
  wtWindow,
  wtTabbox,
  wtTabs,
  wtTab,
  wtTabpanels,
  wtTabpanel,
  wtTabpages,
  wtTabpage,
  wtTabpagePre,
  wtTree,
  wtTreecols,
  wtTreecol,
  wtTreechildren,
  wtTreeitem,
  wtTreerow,
  wtTreecell,
  wtHbox,
  wtLabel,
  wtSpacer,
  wtButton,
  wtTextbox,
  wtChart,
  wtTraceList,
  wtTrace,
  wtToolbar,
  wtToolbarbuttons,
  wtToolbarbutton,
  wtSeparator,
  wtListbox,
  wtListitem,
  wtNumericalwidget,
  wtMenulist,
  wtMenuitem,
  wtDistributionchart,
  wtIntervallimits,
  wtValuelist,
  wtImage,
  wtPushbutton,
  wtTogglebutton
*/
  	
  	if(sizeof(tsp_TestedPanelsList) == 0) { 
      
    } else {
      /*
		type record of charstring EPTF_GUIVariables_Test_RequestedList;
		type record EPTF_GUIVariables_Test_Case {
   			charstring     originalType,
   			EPTF_GUIVariables_Test_RequestedList requestedList
		}
		type record of EPTF_GUIVariables_Test_Case EPTF_GUIVariables_Test_CaseList;
		type record EPTF_GUIVariables_Test_CasePanel {
   			charstring     panelName,
   			EPTF_GUIVariables_Test_CaseList panelList
		}
		type record of EPTF_GUIVariables_Test_CasePanel EPTF_GUIVariables_Test_PanelsList;
		*/
      	for(var integer vl_i:=0; vl_i<sizeof(tsp_TestedPanelsList); vl_i:=vl_i+1) {
        	// tsp_TestedPanelsList[i]
        	// Test
        	if( 0 < sizeof(tsp_TestedPanelsList[vl_i].panelList)) {
        		for(var integer vl_j:=0; vl_j<sizeof(tsp_TestedPanelsList[vl_i].panelList); vl_j:=vl_j+1) {
        		  if( 0 < sizeof(tsp_TestedPanelsList[vl_i].panelList[vl_j].requestedList)){
        		    for(var integer vl_k:=0; vl_k<sizeof(tsp_TestedPanelsList[vl_i].panelList[vl_j].requestedList); vl_k:=vl_k+1) {
        		      var charstring vl_panelName := tsp_TestedPanelsList[vl_i].panelName;
        		      var charstring vl_originalType := tsp_TestedPanelsList[vl_i].panelList[vl_j].originalType;
        		      var charstring vl_testType := tsp_TestedPanelsList[vl_i].panelList[vl_j].requestedList[vl_k].requestType;
        		      var boolean vl_testResult := tsp_TestedPanelsList[vl_i].panelList[vl_j].requestedList[vl_k].testResult;
/*       		      execute(tc_EPTF_UIHandler_customGUI_test(panelName, originalType, vl_testType,vl_testResult)); 	*/
        		      select(vl_testType){
            				case(cg_varNameStart_integerField) {execute(tc_EPTF_UIHandler_customGUI_test_integerField(vl_panelName, vl_originalType, vl_testResult));}
            				case(cg_varNameStart_floatField) {execute(tc_EPTF_UIHandler_customGUI_test_floatField(vl_panelName, vl_originalType, vl_testResult));}
            				case(cg_varNameStart_string) {execute(tc_EPTF_UIHandler_customGUI_test_string(vl_panelName, vl_originalType, vl_testResult));}
            				case(cg_varNameStart_statusLED) {execute(tc_EPTF_UIHandler_customGUI_test_statusLED(vl_panelName, vl_originalType, vl_testResult));}
            				case(cg_varNameStart_statusLEDWithText) {execute(tc_EPTF_UIHandler_customGUI_test_statusLEDWithText(vl_panelName, vl_originalType, vl_testResult));}
            				case(cg_varNameStart_checkBox) {execute(tc_EPTF_UIHandler_customGUI_test_checkBox(vl_panelName, vl_originalType, vl_testResult));}
            				case(cg_varNameStart_none_) {execute(tc_EPTF_UIHandler_customGUI_test_none_(vl_panelName, vl_originalType, vl_testResult));}
            				case(cg_varNameStart_image) {execute(tc_EPTF_UIHandler_customGUI_test_image(vl_panelName, vl_originalType, vl_testResult));}
            				case(cg_varNameStart_pushButton) {execute(tc_EPTF_UIHandler_customGUI_test_pushButton(vl_panelName, vl_originalType, vl_testResult));}
            				case(cg_varNameStart_toggleButton) {execute(tc_EPTF_UIHandler_customGUI_test_toggleButton(vl_panelName, vl_originalType, vl_testResult));}
            				case(cg_varNameStart_trace) {execute(tc_EPTF_UIHandler_customGUI_test_trace(vl_panelName, vl_originalType, vl_testResult));}
            				case(cg_varNameStart_valueList) {execute(tc_EPTF_UIHandler_customGUI_test_valueList(vl_panelName, vl_originalType, vl_testResult));}
            				case(cg_varNameStart_nemtommi) {execute(tc_EPTF_UIHandler_customGUI_test_nemtommi(vl_panelName, vl_originalType, vl_testResult));}
							case else {action(log2str("tsp_TestedPanelsList Unknown testType[", vl_i,"-", vl_j,"-",vl_k,":",vl_testType,"]"));}
            				
        		      }/* EndSelect */       		      
        		    }/* EndFor */
        		    execute(tc_EPTF_test_END());
        		  }/* EndIf */ 
        		}
        	}
      	}
    }
    

  }
}  // end of module
