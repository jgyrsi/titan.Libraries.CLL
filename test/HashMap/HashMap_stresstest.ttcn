///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: HashMap_demo
// 
//  Purpose:
//    Test file for HashMap.
// 
///////////////////////////////////////////////////////////

module HashMap_stresstest
{

//=========================================================================
// Import Part
//=========================================================================

import from EPTF_CLL_HashMapStr2Int_Functions all;
import from EPTF_CLL_HashMapOct2Int_Functions all;
import from EPTF_CLL_HashMapInt2Int_Functions all;
import from TCCEnv_Functions all;
import from EPTF_CLL_HashMap_Definitions all;
import from EPTF_CLL_HashMap_Functions all;
import from EPTF_CLL_Base_Functions all;

//=========================================================================
//Component Types
//=========================================================================

type component MyMTC_CT extends EPTF_HashMap_CT{
}

type record of charstring char2D;
type record of octetstring octet2D;

function returnCharstringKey2() return charstring
{
  var char2D row;
  var integer rndnumber:= float2int(rnd()*100.0);
  row[1]:="key"&int2str(rndnumber);
  var charstring key:= row[1];
  return key;
}

function returnCharstringKey() return charstring
{
  var charstring key:=returnCharstringKey2();
  return key;
}

function returnOctetstringKey2() return octetstring
{
  var octet2D row;
  var integer rndnumber:= float2int(rnd()*255.0);
  row[1]:=int2oct(rndnumber,1);
  var octetstring key:= row[1];
  return key;
}

function returnOctetstringKey() return octetstring
{
  var octetstring key:=returnOctetstringKey2();
  return key;
}


//=========================================================================
// Testcases
//=========================================================================


//=========================================================================
// Testcase: tc_str2int_HashMap_stresstest()
//=========================================================================

testcase tc_str2int_HashMap_stresstest()
  runs on MyMTC_CT
{

  f_EPTF_HashMap_init_CT ("HashMapTest");
  var charstring v_first := "my_first_hashmap";
  var integer v_first_int;
  v_first_int := f_EPTF_str2int_HashMap_New (v_first);

while(true)
{

  for(var integer i:=0;i<1000;i:=i+1)
  {
    var charstring key:=int2str(i);
    f_EPTF_str2int_HashMap_Insert ( v_first_int,key, i);
  }

  //log("Dumping all hashmaps");
  //f_EPTF_str2int_HashMap_DumpAll();

  //f_EPTF_str2int_HashMap_Clear (v_first_int);

  for(var integer i:=0;i<1000;i:=i+1)
  {
    var charstring key:=int2str(i);  
    f_EPTF_str2int_HashMap_Erase (v_first_int, key);
  }

  log("Dumping all hashmaps");
  f_EPTF_str2int_HashMap_DumpAll();
}  
  f_EPTF_str2int_HashMap_Delete (v_first);
  setverdict(pass);  
  f_EPTF_Base_stop(none);
}  

//=========================================================================
// Testcase: tc_oct2int_HashMap_stresstest()
//=========================================================================

testcase tc_oct2int_HashMap_stresstest()
  runs on MyMTC_CT
{

  f_EPTF_HashMap_init_CT ("HashMapTest");
  var charstring v_first := "my_first_hashmap";
  var integer v_first_int;
  v_first_int := f_EPTF_oct2int_HashMap_New (v_first);

while(true)
{

  f_EPTF_oct2int_HashMap_Insert ( v_first_int,'11'O, 11);

  f_EPTF_oct2int_HashMap_Erase (v_first_int, '11'O); 
  //f_EPTF_oct2int_HashMap_Clear (v_first_int);

  log("Dumping all hashmaps");
  f_EPTF_oct2int_HashMap_DumpAll();
}  
  f_EPTF_oct2int_HashMap_Delete (v_first);
  setverdict(pass);  
  f_EPTF_Base_stop(none);
} 

//=========================================================================
// Testcase: tc_str2int_HashMap_assigntest()
//=========================================================================

testcase tc_str2int_HashMap_assigntest()
  runs on MyMTC_CT
{

  f_EPTF_HashMap_init_CT ("HashMapTest");

  for(var integer i:=0;i<100;i:=i+1)
  {
    var charstring key:=int2str(i);
    var integer v_first_int;
    v_first_int := f_EPTF_str2int_HashMap_New (key);

    f_EPTF_str2int_HashMap_Insert ( v_first_int,key, i);

    var charstring key2:=int2str(i+100);
    var integer v_second_int;
    v_second_int := f_EPTF_str2int_HashMap_New (key2);  

    f_EPTF_str2int_HashMap_Assign ( v_second_int,v_first_int);
  }

  //log("Dumping all hashmaps");
  //f_EPTF_str2int_HashMap_DumpAll();

  for(var integer i:=0;i<100;i:=i+1)
  {
    var charstring key:=int2str(i);

    f_EPTF_str2int_HashMap_Delete (key);

  }


  for(var integer i:=500;i<600;i:=i+1)
  {
    var charstring key:=int2str(i);
    var integer v_first_int;
    v_first_int := f_EPTF_str2int_HashMap_New (key);

    f_EPTF_str2int_HashMap_Insert ( v_first_int,key, i);

    var charstring key2:=int2str(i+100);
    var integer v_second_int;
    v_second_int := f_EPTF_str2int_HashMap_New (key2);  

    f_EPTF_str2int_HashMap_Assign ( v_second_int,v_first_int);
  }


  log("Dumping all hashmaps");
  f_EPTF_str2int_HashMap_DumpAll();

  setverdict(pass);  
  f_EPTF_Base_stop(none);
} 

//=========================================================================
// Testcase: tc_oct2int_HashMap_assigntest()
//=========================================================================

testcase tc_oct2int_HashMap_assigntest()
  runs on MyMTC_CT
{

  f_EPTF_HashMap_init_CT ("HashMapTest");

  for(var integer i:=0;i<100;i:=i+1)
  {
    var charstring name:=int2str(i);
    var octetstring key:=int2oct(i,1);
    var integer v_first_int;
    v_first_int := f_EPTF_oct2int_HashMap_New (name);

    f_EPTF_oct2int_HashMap_Insert ( v_first_int,key, i);

    var charstring name2:=int2str(i+100);
    var integer v_second_int;
    v_second_int := f_EPTF_oct2int_HashMap_New (name2);  

    f_EPTF_oct2int_HashMap_Assign ( v_second_int,v_first_int);
  }

  //log("Dumping all hashmaps");
  //f_EPTF_str2int_HashMap_DumpAll();

  for(var integer i:=0;i<100;i:=i+1)
  {
    var charstring name:=int2str(i);

    f_EPTF_oct2int_HashMap_Delete (name);
  }

  for(var integer i:=500;i<600;i:=i+1)
  {
    var charstring name:=int2str(i);
    var octetstring key:=int2oct(i,2);
    var integer v_first_int;
    v_first_int := f_EPTF_oct2int_HashMap_New (name);

    f_EPTF_oct2int_HashMap_Insert ( v_first_int,key, i);

    var charstring name2:=int2str(i+100);
    var integer v_second_int;
    v_second_int := f_EPTF_oct2int_HashMap_New (name2);  

    f_EPTF_oct2int_HashMap_Assign ( v_second_int,v_first_int);
  }


  log("Dumping all hashmaps");
  f_EPTF_oct2int_HashMap_DumpAll();

  setverdict(pass);  
  f_EPTF_Base_stop(none);
}

//=========================================================================
// Testcase: tc_str2int_HashMap_dynamicKeyTest()
//=========================================================================

testcase tc_str2int_HashMap_dynamicKeyTest()
  runs on MyMTC_CT
{

  f_EPTF_HashMap_init_CT ("HashMapTest");
  var charstring v_first := "my_hashmap";
  var integer v_first_int;
  v_first_int := f_EPTF_str2int_HashMap_New (v_first);

//while(true)
//{    
  for(var integer i:=0;i<100;i:=i+1)
  {
    var charstring key:=returnCharstringKey();
    f_EPTF_str2int_HashMap_Insert ( v_first_int,key, i);
  } 

  for(var integer i:=0;i<100;i:=i+1)
  {
    var charstring key:=returnCharstringKey();  
    f_EPTF_str2int_HashMap_Erase (v_first_int, key);
  }

  for(var integer i:=0;i<100;i:=i+1)
  {
    var charstring key:=returnCharstringKey();
    f_EPTF_str2int_HashMap_Insert ( v_first_int,key, i);
  }

  for(var integer i:=0;i<100;i:=i+1)
  {
    var charstring key:=returnCharstringKey();
    f_EPTF_str2int_HashMap_Erase ( v_first_int,key);
  }

  log("Dumping all hashmaps");
  f_EPTF_str2int_HashMap_DumpAll();

  f_EPTF_str2int_HashMap_Clear(v_first_int);

//}
  f_EPTF_str2int_HashMap_Delete (v_first);
  setverdict(pass);  
  f_EPTF_Base_stop(none);
}

//=========================================================================
// Testcase: tc_oct2int_HashMap_dynamicKeyTest()
//=======================================================================

testcase tc_oct2int_HashMap_dynamicKeyTest()
  runs on MyMTC_CT
{

  f_EPTF_HashMap_init_CT ("HashMapTest");
  var charstring v_first := "my_first_hashmap";
  var integer v_first_int;
  v_first_int := f_EPTF_oct2int_HashMap_New (v_first);

//while(true)
//{    
  for(var integer i:=0;i<255;i:=i+1)
  {
    var octetstring key:=returnOctetstringKey();
    f_EPTF_oct2int_HashMap_Insert ( v_first_int,key, i);
  } 

  for(var integer i:=0;i<255;i:=i+1)
  {
    var  octetstring key:=returnOctetstringKey();  
    f_EPTF_oct2int_HashMap_Erase (v_first_int, key);
  }

  log("Dumping all hashmaps");
  f_EPTF_oct2int_HashMap_DumpAll();

  for(var integer i:=0;i<255;i:=i+1)
  {
    var  octetstring key:=returnOctetstringKey();
    f_EPTF_oct2int_HashMap_Insert ( v_first_int,key, i);
  }

  for(var integer i:=0;i<255;i:=i+1)
  {
    var  octetstring key:=returnOctetstringKey();  
    f_EPTF_oct2int_HashMap_Erase (v_first_int, key);
  }

  log("Dumping all hashmaps");
  f_EPTF_oct2int_HashMap_DumpAll();

  f_EPTF_oct2int_HashMap_Clear(v_first_int);

//}
  f_EPTF_oct2int_HashMap_Delete (v_first);
  setverdict(pass);  
  f_EPTF_Base_stop(none);
}


//=========================================================================
// Control
//=========================================================================

control 
{
  select (f_GetEnv("TTCN_TEST_TYPE")) {
    case ("SMOKE") {
	  execute( tc_str2int_HashMap_stresstest() );
	  execute( tc_oct2int_HashMap_stresstest() );
	  execute( tc_str2int_HashMap_assigntest() );
	  execute( tc_oct2int_HashMap_assigntest() );
	  execute( tc_str2int_HashMap_dynamicKeyTest() );
	  execute( tc_oct2int_HashMap_dynamicKeyTest() );
	}
	case else {
	  execute( tc_str2int_HashMap_stresstest() );
	  execute( tc_oct2int_HashMap_stresstest() );
	  execute( tc_str2int_HashMap_assigntest() );
	  execute( tc_oct2int_HashMap_assigntest() );
	  execute( tc_str2int_HashMap_dynamicKeyTest() );
	  execute( tc_oct2int_HashMap_dynamicKeyTest() );
	}
  }
}

}  // end of module
