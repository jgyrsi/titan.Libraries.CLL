<!--Copyright (c) 2000-2023 Ericsson Telecom AB

All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v2.0
which accompanies this distribution, and is available at
http:www.eclipse.org/legal/epl-v10.html                           -->
<!DOCTYPE TITAN_GUI_project_file>
<Project TITAN_version="1.8.pre0 build 1" >
    <General>
        <Project_Name>EPTF_TPE_Test</Project_Name>
        <Executable_Path>bin/EPTF_TPE_Test</Executable_Path>
        <Working_Dir>bin</Working_Dir>
        <Build_Host>alpha</Build_Host>
        <Execution_Mode>Parallel</Execution_Mode>
        <ScriptFile_AfterMake>../Variable/makefile_patch.sh</ScriptFile_AfterMake>
        <Log_Format>yes</Log_Format>
        <Update_Symlinks>yes</Update_Symlinks>
        <Create_Absolute_Symlinks>no</Create_Absolute_Symlinks>
        <Update_Makefile>yes</Update_Makefile>
        <Localhost_Execute>yes</Localhost_Execute>
        <Execute_Command>rsh %host &quot;cd %project_working_dir ; &quot;%executable&quot; %localhost %mctr_port&quot;</Execute_Command>
        <Execute_Hosts>alfa, beta, gamma</Execute_Hosts>
        <UnUsed_List></UnUsed_List>
    </General>
    <Configs>
        <Config>EPTF_TPE_Test.cfg</Config>
        <Config>edited_time_profiles.cfg</Config>
    </Configs>
    <Test_Cases>
        <Test_Case>EPTF_CLL_TimeProfileEditor.control</Test_Case>
        <Test_Case>EPTF_CLL_TimeProfileEditor.tc_TimeProfileEditor</Test_Case>
        <Test_Case>EPTF_TPE_Test_Testcases.control</Test_Case>
        <Test_Case>EPTF_TPE_Test_Testcases.EPTF_TPE_Test_behaviour</Test_Case>
        <Test_Case>EPTF_TPE_Test_Testcases.tc_EPTF_TPE_Test_add_time_sequence</Test_Case>
        <Test_Case>EPTF_TPE_Test_Testcases.tc_EPTF_TPE_Test_change_time_sequence_properties_tree</Test_Case>
        <Test_Case>EPTF_TPE_Test_Testcases.tc_EPTF_TPE_Test_addTimeSequenceRow</Test_Case>
        <Test_Case>EPTF_TPE_Test_Testcases.tc_EPTF_TPE_Test_removeSequenceRow</Test_Case>
        <Test_Case>EPTF_TPE_Test_Testcases.tc_EPTF_TPE_Test_removeSequence</Test_Case>
        <Test_Case>EPTF_TPE_Test_Testcases.tc_EPTF_TPE_Test_addTimeProfile</Test_Case>
        <Test_Case>EPTF_TPE_Test_Testcases.tc_EPTF_TPE_Test_addNewProfileRow</Test_Case>
        <Test_Case>EPTF_TPE_Test_Testcases.tc_EPTF_TPE_Test_removeTimeProfileRow</Test_Case>
        <Test_Case>EPTF_TPE_Test_Testcases.tc_EPTF_TPE_Test_removeTimeProfile</Test_Case>
    </Test_Cases>
    <File_Group name="MainFileGroup" >
        <File_Groups>
            <File_Group path="../../src/Base/EPTF_CLL_Base.grp" />
            <File_Group path="../../src/FreeBusyQueue/EPTF_CLL_FreeBusyQueue.grp" />
            <File_Group path="../../src/HashMap/EPTF_CLL_HashMap.grp" />
            <File_Group path="../../src/Logging/EPTF_CLL_Logging.grp" />
            <File_Group path="../../src/RedBlackTree/EPTF_CLL_RBtree.grp" />
            <File_Group path="../../src/Variable/EPTF_CLL_Variable.grp" />
            <File_Group path="../../src/Common/EPTF_CLL_Common.grp" />
            <File_Group path="../../src/Transport/EPTF_CLL_TransportCommPortIPL4.grp" />
            <File_Group path="../../src/Transport/EPTF_CLL_TransportMessageBufferManager.grp" />
            <File_Group path="../../src/Scheduler/EPTF_CLL_Scheduler.grp" />	
            <File_Group path="../../src/TimeProfileEditor/EPTF_CLL_TimeProfileEditor_Dep.grp" />
            <File_Group name="GeneralTypes" >
                <File path="../../../../ProtocolModules/COMMON/src/General_Types.ttcn" />
            </File_Group>   
            <File_Group name="XTDP" >
                <File path="../../../../ProtocolModules/XTDP_CNL113663/src/XTDP_EncDecFunctions.ttcn" />
                <File path="../../../../ProtocolModules/XTDP_CNL113663/src/../../COMMON/src/UsefulTtcn3Types.ttcn" />
                <File path="../../../../ProtocolModules/XTDP_CNL113663/src/../../COMMON/src/XSD.ttcn" />
                <File path="../../../../ProtocolModules/XTDP_CNL113663/src/generated_files/ttcn_ericsson_se_protocolModules_xtdp_xtdl.ttcn" />
                <File path="../../../../ProtocolModules/XTDP_CNL113663/src/generated_files/ttcn_ericsson_se_protocolModules_xtdp_xtdp.ttcn" />
            </File_Group>
            <File_Group name="AbstractSocket" >
                <File path="../../../../TestPorts/Common_Components/Abstract_Socket_CNL113384/src/Abstract_Socket.cc" />
                <File path="../../../../TestPorts/Common_Components/Abstract_Socket_CNL113384/src/Abstract_Socket.hh" />
            </File_Group>
            <File_Group name="UsefulFunctions" >
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCDateTime.cc" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCDateTime_Functions.ttcn" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCEncoding.cc" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCEncoding_Functions.ttcn" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCFileIO.cc" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCFileIO_Functions.ttcn" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCInterface.cc" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCInterface_Functions.ttcn" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCConversion_Functions.ttcn" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCConversion.cc" />
            </File_Group>
            <File_Group name="EPTF_TPE_Test" >
                <File path="EPTF_TPE_Test_Definitions.ttcn" />
                <File path="EPTF_TPE_Test_Testcases.ttcn" />
                <File path="EPTF_TPE_Test_Functions.ttcn" />
                <File path="EPTF_TPE_Test_Templates.ttcn" />
            </File_Group>
            <File_Group name="IPL4" >
                <File path="../../../../TestPorts/IPL4asp_CNL113531/src/IPL4asp_PT.cc" />
                <File path="../../../../TestPorts/IPL4asp_CNL113531/src/IPL4asp_PT.hh" />
                <File path="../../../../TestPorts/IPL4asp_CNL113531/src/IPL4asp_PortType.ttcn" />
                <File path="../../../../TestPorts/IPL4asp_CNL113531/src/IPL4asp_Types.ttcn" />
                <File path="../../../../TestPorts/IPL4asp_CNL113531/src/IPL4asp_Functions.ttcn" />
                <File path="../../../../TestPorts/IPL4asp_CNL113531/src/IPL4asp_discovery.cc" />	
                <File path="../../../../TestPorts/IPL4asp_CNL113531/src/IPL4asp_protocol_L234.hh" />				
            </File_Group>
        </File_Groups>
    </File_Group>
</Project>
