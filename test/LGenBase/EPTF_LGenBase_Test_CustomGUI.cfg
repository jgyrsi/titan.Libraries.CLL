///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
# This section shall contain the values of all parameters that are defined in your TTCN-3 modules.
[MODULE_PARAMETERS]
EPTF_CLL_UIHandler_WidgetFunctions.tsp_xtdp_listen_addr := "127.0.0.1"
EPTF_CLL_UIHandler_WidgetFunctions.tsp_xtdp_listen_port := 11420

tsp_numEntities := 6
tsp_debug_EPTF_SchedulerFunctions := false
tsp_debugVerbose_EPTF_SchedulerFunctions := false
tsp_EPTF_LGenBaseDebugTraffic := false
tsp_EPTF_LGenBaseDebug := false
#LGenBase Test:
tsp_EPTF_cpsUpdateInterval := 0.01
tsp_cps := 1000.0
tsp_execTime := 60.0
tsp_nrOfEntities := 300
tsp_minNrOfEntities := 100
//Almost all the testcases run on a single component. Don't want to wait for synchronisation
tsp_EPTF_LGenBase_fSMStatsSubscribeMode := realtime
tsp_dummyTemplatePath := "./dummy_templ.txt"
#  for tc_EPTF_LGenBase_Test_cpsTest4WeightedScenario_entityLimit
tsp_maxNrOfEntities := 100000
#& for tc_EPTF_LGenBase_Test_cpsTest4NotWeightedScenario_searchEntityLimit
tsp_LGenBase_Test_timeout := 0.0001
tsp_EPTF_LGenBaseTestDebug := false
tsp_testDuration := 10.0


tsp_burstCalcMode := cs
tsp_tcOfSc2 := {
    tcName := "TrafficCaseName",
    startDelay := 0.0,
    cpsToReach := 20.0,
    enableEntities := true,
    enable := true,
    ranges := { },
    params := {
        {
            aName := "A",
            pName := "P",
            pValue := "V"
        }
    },
    groupFinishConditions := {
        nrOfExecStart := omit,
        nrOfSuccesses := omit,
        nrOfFails := omit,
        nrOfGrpLoop := omit,
        nrOfRangeLoop := omit,
        execTime := omit,
        entitiesFinished := omit,
        customFinishCondition := omit
    },
    entityFinishConditions := {
        nrOfExecStart := omit,
        nrOfSuccesses := omit,
        nrOfFails := omit,
        customFinishCondition := omit
    },
    entityFinishActions := { },
    tcFinishActions := { }
}




tsp_nrOfRepeat := 1
tsp_EPTF_LGenBase_PhaseList_Declarators := {
    {
        name := "BasicPhases",
        phases := {
            {
                name := "preamble",
                enabled := true
            },
            {
                name := "loadgen",
                enabled := true
            },
            {
                name := "postamble",
                enabled := true
            }
        }
    },
    {
        name := "AdvancedPhases",
        phases := {
            {
                name := "init",
                enabled := true
            },
            {
                name := "preamble",
                enabled := true
            },
            {
                name := "loadgen",
                enabled := true
            },
            {
                name := "postamble",
                enabled := true
            },
            {
                name := "delete",
                enabled := true
            }
        }
    }
}



tsp_EPTF_Var_SyncInterval := 3.0
tsp_EPTF_Var_refreshSubscribersPeriod := -1.0
tsp_EPTF_LGenBaseDebugLightList := {
    "DummyFsmName",
    "FSM2bDebug"
}
tsp_LGenBaseDebugLightLogFile := "@tcclab2-MTC.log"
tsp_LGenBaseDebugLightPipeGuard := 180.0

tsp_reregSuccessPercent := 0.3







tsp_modulePar_compactFsmTables := {
  {
    name := "lGenRegFsm",
    //             [0]   
    stateList := {"idle", "busy"}, 
    timerList := {{name:= "dummy", startValue := 0.1}},
    table := {
      {eventToListen := {bName := "EPTF_CLL_LGenBase",iName := "LGenBase: Start_the_traffic_case!",eventType := fsm},
        cellRow := {
          //state[0]==idle 
          {actionList := {{stepOrFunctionName := "LGenBase: StepFunction_timerStart", contextArgs := {timerName := "dummy"}}}, nextStateCalculation := omit, nextState := "busy"},
          //state[1]==busy 
          {actionList := {{stepOrFunctionName := "f_LGenBaseTest_InvalidEvent",contextArgs := {stepContextArgs := {0}}}}, nextStateCalculation := omit, nextState := "busy"}
        }
      },        
      {eventToListen := {bName := "Special behavior for timeout reporting", iName := "dummy",eventType := fsm},
        cellRow := {
          //state[0]==idle 
          {actionList := omit , nextStateCalculation := omit, nextState := omit},
          //state[1]==busy 
          {actionList := {{stepOrFunctionName := "f_LGenBaseTest_register",contextArgs := omit}}, nextStateCalculation := omit,nextState := "idle"}
        }
      }        
    }
  },
  {
    name := "lGenReRegFsm",
    //             [0]   
    stateList := {"idle", "busy"}, 
    timerList := {{name:= "dummy", startValue := 0.5}},
    table := {
      {
        eventToListen := {bName := "EPTF_CLL_LGenBase",iName := "LGenBase: Start_the_traffic_case!",eventType := fsm},
        cellRow := {
          //state[0]==idle 
          {actionList := {{stepOrFunctionName := "LGenBase: StepFunction_timerStart",contextArgs := {timerName := "dummy"}}}, nextStateCalculation := omit, nextState := "busy"},
          //state[1]==busy 
          {actionList := {{stepOrFunctionName := "f_LGenBaseTest_InvalidEvent",contextArgs := {stepContextArgs := {0}}}}, nextStateCalculation := omit, nextState := "busy"}
        }
      },        
      {
        eventToListen := {bName := "Special behavior for timeout reporting",iName := "dummy",eventType := fsm},
        cellRow := {
          //state[0]==idle 
          {actionList := omit , nextStateCalculation := omit, nextState := omit},
          //state[1]==busy 
          {actionList := {{stepOrFunctionName := "f_LGenBaseTest_reRegister",contextArgs := omit}}, nextStateCalculation := omit, nextState := "idle"}
        }
      },        
      {
        eventToListen := {bName := "EPTF_CLL_LGenBase", iName := "LGenBase: Stop_the_traffic_case!",eventType := fsm},
        cellRow := {
          //state[0]==idle 
          {actionList := {{stepOrFunctionName := "f_LGenBaseTest_stopReRegisterIdle",contextArgs := omit}}, nextStateCalculation := omit,nextState := "idle"},
          //state[1]==busy 
          {actionList := {{stepOrFunctionName := "f_LGenBaseTest_stopReRegisterBusy",contextArgs := omit}}, nextStateCalculation := omit,nextState := "idle"}
        }
      }
    }
  },
  {
    name := "lGenTrafficFsm",
    //             [0]   
    stateList := {"idle","running"}, 
    timerList := {{name:= "run", startValue := ${doTrafficDelay, float}}},
    table := {
      {
        eventToListen := {bName := "EPTF_CLL_LGenBase",iName := "LGenBase: Start_the_traffic_case!",eventType := fsm},
        cellRow := {
          //state[0]==idle 
          {actionList := 
            {
              {
                stepOrFunctionName := "LGenBase: StepFunction_timerStart",
                contextArgs := {
                  timerName := "run"
                }
              }
            }, 
            nextStateCalculation := omit, 
            nextState := "running"
          },
          //state[1]==running 
          {actionList := omit , nextStateCalculation := omit, nextState := omit}
        }
      },
      {
        eventToListen := {
          bName := "Special behavior for timeout reporting",
          iName := "run",
          eventType := fsm
        },
        cellRow := {
          //idle
          {actionList := omit , nextStateCalculation := omit, nextState := omit},
          //running
          {actionList := 
            {
              {stepOrFunctionName := "f_LGenBaseTest_doTraffic",contextArgs := omit}
            }, 
            nextStateCalculation := omit, 
            nextState := "idle"
          }
        }
      }
    }
  }
};  

tsp_modulePar_entityTypes := {
    {name := "etBubu0", behaviorTypeIdxList := {"bhTest1","bhTest2"}}
    }
    
tsp_modulePar_entityGroups := {
    {name := "eg0", eType := "etBubu0", eCount := ${num_entities,integer}},
    {name := "eg1", eType := "etBubu0", eCount := ${num_entities,integer}}
}

tsp_modulePar_trafficCases := {
  {
    name := "register",
    fsmName := "lGenRegFsm",
    entityType := "etBubu0",
    customEntitySucc := ""
  },
  {
    name := "reregister",
    fsmName := "lGenReRegFsm",
    entityType := "etBubu0",
    customEntitySucc := ""
  },
  {
    name := "reregisterWieght",
    fsmName := "lGenReRegFsm",
    entityType := "etBubu0",
    customEntitySucc := ""
  },
  {
    name := "doTraffic",
    fsmName := "lGenTrafficFsm",
    entityType := "etBubu0",
    customEntitySucc := ""
  }    
};

tsp_modulePar_scenarios := {
  {
    name := "sc1",
    tcList := {
      {
        tcName := "register",
        startDelay := 0.0,
        cpsToReach := 10.0,
        enableEntities := true,
        enable := true,
        ranges := {},
        params := {},
        groupFinishConditions := {
          nrOfExecStart := omit, 
          nrOfSuccesses := ${num_entities,integer},
          nrOfFails := omit, 
          nrOfGrpLoop := omit, 
          nrOfRangeLoop := omit, 
          execTime := omit,
          entitiesFinished := false,
          customFinishCondition := omit
        },
        entityFinishConditions := {
          nrOfExecStart := omit, 
          nrOfSuccesses := 1, 
          nrOfFails := omit, 
          customFinishCondition := omit
        },
        entityFinishActions := {
          { 
            enableEntity4Tc := {
              tcName := "reregister",
              aMode := available
            }
          },
          { 
            enableEntity4Tc := {
              tcName := "doTraffic",
              aMode := available
            }
          }
        },
        tcFinishActions := {
          {
            actionsType := onGroupFinishCondition,
            actions := {{enableTc := {tcName := "doTraffic",aMode := noChange}}}
          },
          {
            actionsType := onGroupFinishCondition,
            actions := {{customFinishFunction := "successGroupRegister"}}
          }
        }
      },
      {
        tcName := "reregister",
        startDelay := 10.0,
        cpsToReach := 10.0,
        enableEntities := false,
        enable := true,
        ranges := {},
        params := {},
        groupFinishConditions := {
          nrOfExecStart := ${maxReRegisterCount,integer}, 
          nrOfSuccesses := omit, 
          nrOfFails := omit, 
          nrOfGrpLoop := omit, 
          nrOfRangeLoop := omit, 
          execTime := omit, 
          entitiesFinished := false,
          customFinishCondition := omit
        },
        entityFinishConditions := {
          nrOfExecStart := omit, 
          nrOfSuccesses := omit, 
          nrOfFails := omit, 
          customFinishCondition := omit
        },
        entityFinishActions := {},
        tcFinishActions := {}
      },
      {
        tcName := "doTraffic",
        startDelay := 0.0,
        cpsToReach :=12.0,
        enableEntities := false,
        enable := false,
        ranges := {},
        params := {},
        groupFinishConditions := {
          nrOfExecStart := 120, //omit, 
          nrOfSuccesses := omit, 
          nrOfFails := omit, 
          nrOfGrpLoop := omit, 
          nrOfRangeLoop := omit, 
          execTime := omit, 
          entitiesFinished := false,
          customFinishCondition := omit
        },
        entityFinishConditions := {
          nrOfExecStart := omit, 
          nrOfSuccesses := omit, 
          nrOfFails := omit, 
          customFinishCondition := omit
        },
        entityFinishActions := {},
        tcFinishActions := {
          {
            actionsType := onGroupFinishCondition,
            actions := {{customFinishFunction := "doTrafficFinished"}}
          }
          }
      }
    }
  },
  {
    name := "sc2",
    tcList := {
      {
        tcName := "register",
        startDelay := 0.0,
        cpsToReach := 10.0,
        enableEntities := true,
        enable := true,
        ranges := {},
        params := {},
        groupFinishConditions := {
          nrOfExecStart := omit, 
          nrOfSuccesses := ${num_entities,integer},
          nrOfFails := omit, 
          nrOfGrpLoop := omit, 
          nrOfRangeLoop := omit, 
          execTime := omit,
          entitiesFinished := false,
          customFinishCondition := omit
        },
        entityFinishConditions := {
          nrOfExecStart := omit, 
          nrOfSuccesses := 1, 
          nrOfFails := omit, 
          customFinishCondition := omit
        },
        entityFinishActions := {},
        tcFinishActions := {}
      }
    }
  },
  {
    name := "sc3",
    tcList := {
      {
        tcName := "reregister",
        startDelay := 0.0,
        cpsToReach := 10.0,
        enableEntities := false,
        enable := true,
        ranges := {},
        params := {},
        groupFinishConditions := {
          nrOfExecStart := ${maxReRegisterCount,integer}, 
          nrOfSuccesses := omit, 
          nrOfFails := omit, 
          nrOfGrpLoop := omit, 
          nrOfRangeLoop := omit, 
          execTime := 20.0, 
          entitiesFinished := false,
          customFinishCondition := omit
        },
        entityFinishConditions := {
          nrOfExecStart := omit, 
          nrOfSuccesses := omit, 
          nrOfFails := omit, 
          customFinishCondition := omit
        },
        entityFinishActions := {},
        tcFinishActions := {}
      },
      {
        tcName := "doTraffic",
        startDelay := 8.0,
        cpsToReach :=12.0,
        enableEntities := false,
        enable := false,
        ranges := {},
        params := {},
        groupFinishConditions := {
          nrOfExecStart := omit, 
          nrOfSuccesses := omit, 
          nrOfFails := omit, 
          nrOfGrpLoop := omit, 
          nrOfRangeLoop := omit, 
          execTime := omit, 
          entitiesFinished := false,
          customFinishCondition := omit
        },
        entityFinishConditions := {
          nrOfExecStart := omit, 
          nrOfSuccesses := omit, 
          nrOfFails := omit, 
          customFinishCondition := omit
        },
        entityFinishActions := {},
        tcFinishActions := {
          {
            actionsType := onGroupFinishCondition,
            actions := {{customFinishFunction := "doTrafficFinished"}}
          }
          }
      }
    }
  }
}
tsp_modulePar_Scenarios2Grps := {
  {eGrpName := "eg0", scenarioNames := {"sc2"}},
  {eGrpName := "eg0", scenarioNames := {"sc3"}},
  {eGrpName := "eg1", scenarioNames := {"sc1"}},
  {eGrpName := "eg0", scenarioNames := {"sc1"}}
}

//LogFile := "logs/%e.%h-%t%r.%s"
[LOGGING]
LogFile := "%c@%h-%n.log"
FileMask := LOG_ALL | TTCN_ACTION | TTCN_ERROR | TTCN_TESTCASE | TTCN_STATISTICS | TTCN_VERDICTOP | ACTION
//ERROR | STATISTICS | PARALLEL | ACTION | USER
ConsoleMask := TTCN_ACTION | TTCN_ERROR | TTCN_TESTCASE | TTCN_STATISTICS | TTCN_VERDICTOP
//
TimeStampFormat := DateTime
LogEventTypes := Yes
LogEntityName := Yes
SourceInfoFormat := Stack

# In this section you can specify parameters that are passed to Test Ports.
#[DEFINE]
# In this section you can create macro definitions,
# that can be used in other configuration file sections except [INCLUDE].
[DEFINE]
num_entities := 100
maxReRegisterCount := 79
doTrafficCPS := 32.4
doTrafficDelay := 2.0

#[INCLUDE]
# To use configuration settings given in other configuration files,
# the configuration files just need to be listed in this section, with their full or relative pathnames.
[EXTERNAL_COMMANDS]

# This section can define external commands (shell scripts) to be executed by the ETS
# whenever a control part or test case is started or terminated.
#BeginTestCase := ""
#EndTestCase := ""
#BeginControlPart := ""
#EndControlPart := ""
[EXECUTE]
EPTF_LGenBase_Test_CustomGUI.control

# This section consists of rules restricting the location of created PTCs.
[MAIN_CONTROLLER]
UnixSocketsEnabled := No

//saved by GUI
