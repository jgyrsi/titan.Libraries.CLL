///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_LGenBase_PerfTest_FSM_Definitions
//
//  Purpose:
//    This module provides type definitions for testing LGenBase FSM
//
//  Module depends on:
//    -
//
//  Current Owner:
//    Jozsef Gyurusi (ETHJGI)
//
//  Last Review Date:
//    -
//
//  Detailed Comments:
//    -
//
//
///////////////////////////////////////////////////////////////
module EPTF_LGenBase_PerfTest_FSM_Definitions
{

import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_LGenBase_Definitions all;
import from EPTF_CLL_Common_Definitions all;
import from TestResultGen all;
import from ttcn_ericsson_se_TitanSim_Perftest all;

type enumerated EPTF_LGenBase_PerfTest_FSM_PostOrDispatch {
  post,
  dispatch
}



type component EPTF_LGenBase_PerfTest_FSM_CT extends EPTF_LGenBase_CT, TestResultGen_CT {
  var integer v_myBCtxIdx := -1;
  var integer v_EPTF_LGenBase_PerfTest_FSM_nofEventCounter := 0;
  var EPTF_IntegerList v_EPTF_LGenBase_PerfTest_FSM_nofSuccessPerEntity := {};
  var EPTF_IntegerList v_EPTF_LGenBase_PerfTest_FSM_eventIDs := {};
  var integer v_EPTF_LGenBase_Test_nofEvents := 0;
  var EPTF_IntegerList v_EPTF_LGenBase_PerfTest_FSM_nofCyclesPerEntity := {};
  var boolean v_EPTF_LGenBase_PerfTest_FSM_testFinished := false;
  var Testresult v_EPTF_LGenBase_PerfTest_FSM_testResult;
  var charstring v_EPTF_LGenBase_PerfTest_FSM_startTime := "";

}

type record of EPTF_LGenBase_PerfTest_FSM_CT EPTF_LGenBase_PerfTest_FSM_CT_List;

type component EPTF_LGenBase_PerfTest_FSM_Main_CT extends EPTF_Base_CT, TestResultGen_CT {
}

} //end of module
