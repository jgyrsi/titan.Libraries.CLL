///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// 
//  Purpose:
//    This module provides functions for testing R18 functionalities and bugfixes
//    of LGenBase
// 
//  Module depends on:
//    -
// 
//  Current Owner:
//    Jozsef Gyurusi (ethjgi)
// 
//  Last Review Date:
//    -
//
///////////////////////////////////////////////////////////////
module EPTF_LGenBase_Test_TestcasesR18
// [.objid{ itu_t(0) identified_organization(4) etsi(0)
// identified_organization(127) ericsson(5) testing(0)
// <put further nodes here if needed>}]
{
import from EPTF_LGenBase_Test_Definitions all;
import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_LGenBase_Functions all;
import from EPTF_CLL_LGenBase_ConfigFunctions all;
import from EPTF_CLL_LGenBase_ConfigDefinitions all;
import from EPTF_CLL_LGenBase_Definitions all;
import from EPTF_CLL_LGenBase_EventHandlingFunctions all;
import from EPTF_CLL_LGenBase_TrafficFunctions all;
import from EPTF_CLL_LGenBaseStats_Definitions all;
import from EPTF_CLL_Variable_Functions all;
import from EPTF_CLL_LGenBase_StepFunctions all;



const integer c_EPTF_LGenBase_Test_nofEntities := 20000; // number of the entities in the entity group
const integer c_EPTF_LGenBase_Test_nofEvents := 20000;  // number of events each entity posts for itself
const integer c_EPTF_LGenBase_Test_nofSuccesses := 10; // test is executed until nofSuccess reaches this (entity is successful if received event reaches c_EPTF_LGenBase_Test_nofEvents)
const float c_EPTF_LGenBase_Test_maxExecutionTime := 60.0; // the main component shuts down the test if LGen does not finish until this time
const float c_EPTF_LGenBase_Test_tcCPS := 10.0; // the CPS used int the traffic case


// this testcase posts 20000 events this causes a segfault in the CLL version R18A04
// because the function stack becomes too deep and the OP system runs out of memory.
// This test should pass without segfault.
testcase tc_EPTF_LGenBase_Test_longCallStackSegfault_Post() runs on EPTF_Base_CT {

  f_EPTF_Base_init_CT("longCallStackSegfault");
  
  // start an LGenBase component that posts a lot of events
  var EPTF_LGenBase_Test_CT vl_lgen := EPTF_LGenBase_Test_CT.create;
  vl_lgen.start(f_EPTF_LGenBase_Test_longCallStackSegfaultBehaviour(post));
  
  timer t_wait := c_EPTF_LGenBase_Test_maxExecutionTime;
  t_wait.start; 
  
  alt {
    [] t_wait.timeout {
      setverdict(fail, "LGen component did not terminated in time");
      f_EPTF_Base_stop(none);
    }
    [] vl_lgen.done;
  }
  
  f_EPTF_Base_stop(pass);
}

// this testcase dispatches 20000 events this does not cause a segfault in the CLL version R18A04
// because of  too deep function stack.
// This test should pass without segfault.
testcase tc_EPTF_LGenBase_Test_longCallStackSegfault_Dispatch() runs on EPTF_Base_CT {

  f_EPTF_Base_init_CT("longCallStackSegfault");
  
  // start an LGenBase component that posts a lot of events
  var EPTF_LGenBase_Test_CT vl_lgen := EPTF_LGenBase_Test_CT.create;
  vl_lgen.start(f_EPTF_LGenBase_Test_longCallStackSegfaultBehaviour(dispatch));
  
  timer t_wait := c_EPTF_LGenBase_Test_maxExecutionTime;
  t_wait.start; 
  
  alt {
    [] t_wait.timeout {
      setverdict(fail, "LGen component did not terminated in time");
      f_EPTF_Base_stop(none);
    }
    [] vl_lgen.done;
  }
  
  f_EPTF_Base_stop(pass);
}

function f_EPTF_Test_postALotEvents(in EPTF_LGenBase_TestStepArgs pl_ptr) runs on EPTF_LGenBase_Test_CT {
  //action(log2str(%definitionId&"(pl_ptr:= ",pl_ptr,"): called"));
  
  for(var integer i:=0; i<c_EPTF_LGenBase_Test_nofEvents; i:=i+1) {
    f_EPTF_LGenBase_postEvent({
      event := {
        bIdx := v_myBCtxIdx,
        iIdx := v_dummyInt,
        target := {eIdx := pl_ptr.eIdx, fsmCtxIdx := pl_ptr.refContext.fCtxIdx}, 
        source := {eIdx := pl_ptr.eIdx, fsmCtxIdx := pl_ptr.refContext.fCtxIdx} 
      },
      reportedArgs := {i}
    });
  }
}

function f_EPTF_Test_dispatchALotEvents(in EPTF_LGenBase_TestStepArgs pl_ptr) runs on EPTF_LGenBase_Test_CT {
  //action(log2str(%definitionId&"(pl_ptr:= ",pl_ptr,"): called"));
  
  for(var integer i:=0; i<c_EPTF_LGenBase_Test_nofEvents; i:=i+1) {
    f_EPTF_LGenBase_dispatchEvent({
      event := {
        bIdx := v_myBCtxIdx,
        iIdx := v_dummyInt,
        target := {eIdx := pl_ptr.eIdx, fsmCtxIdx := pl_ptr.refContext.fCtxIdx}, 
        source := {eIdx := pl_ptr.eIdx, fsmCtxIdx := pl_ptr.refContext.fCtxIdx} 
      },
      reportedArgs := {i}
    });
  }
}

function f_EPTF_Test_logEvent(in EPTF_LGenBase_TestStepArgs pl_ptr) runs on EPTF_LGenBase_Test_CT {
  //action(log2str(%definitionId&"(pl_ptr:= ",pl_ptr,"): called"));
  v_registerCounterSucc := v_registerCounterSucc + 1 ;
  
  if (not isbound(v_LGenBase_Test_counters[pl_ptr.eIdx])) {
    v_LGenBase_Test_counters[pl_ptr.eIdx] := 0;
  }
  v_LGenBase_Test_counters[pl_ptr.eIdx] := v_LGenBase_Test_counters[pl_ptr.eIdx] + 1;

  //report success if all events were received:
  if (v_LGenBase_Test_counters[pl_ptr.eIdx]==c_EPTF_LGenBase_Test_nofEvents) {
    f_EPTF_LGenBase_step_trafficSuccess(pl_ptr);
  }
}

type enumerated EPTF_LGenBase_Test_PostOrDispatch {
  post,
  dispatch
}

function f_EPTF_LGenBase_Test_longCallStackSegfaultBehaviour(in EPTF_LGenBase_Test_PostOrDispatch pl_postOrDispatch := post) runs on EPTF_LGenBase_Test_CT {
  
  f_EPTF_LGenBase_init("longCallStackSegfaultBehaviour");
  
  v_registerCounterSucc := 0; // count the events
  v_LGenBase_Test_counters := {}; // count the events per entity
  v_myBCtxIdx := f_EPTF_LGenBase_declareBehaviorType("b1", -1, null, null, null);
  f_EPTF_LGenBase_declareEntityType("et1", {"b1"});

  var EPTF_LGenBase_EventArrayDeclaratorList vl_eventArray := {};
  vl_eventArray[0].eventConstant := 0;
  vl_eventArray[0].eventName := "Event";

  v_dummyInt := f_EPTF_LGenBase_declareFsmEvent("b1", "Event"); // the event ID

  
  if (pl_postOrDispatch == post) {
    f_EPTF_LGenBase_declareFunction("reportALotEvents",{testStepFunction := refers(f_EPTF_Test_postALotEvents)})
  } else {
    f_EPTF_LGenBase_declareFunction("reportALotEvents",{testStepFunction := refers(f_EPTF_Test_dispatchALotEvents)})
  }
  f_EPTF_LGenBase_declareFunction("logEvent",{testStepFunction := refers(f_EPTF_Test_logEvent)})

  f_EPTF_LGenBase_declareFSMTable({
    name := "FSM1",
    fsmParams := {
      {stateList := {"idle"}}
    },
    table := {
      classicTable := {
        {eventToListen := {c_EPTF_LGenBase_behavior,c_EPTF_LGenBase_inputName_testMgmt_startTC,fsm},
          cellRow := {{
              {
                {"reportALotEvents", omit}
              },
              omit, omit
            }}
        },       
        {eventToListen := {"b1","Event",fsm},
          cellRow := {{
              {
                {"logEvent", omit}
              },
              omit, omit
            }}
        }        
      }
    }
  });
  

  f_EPTF_LGenBase_createEntityGroup({"eg1", "et1", c_EPTF_LGenBase_Test_nofEntities});

  var EPTF_LGenBase_TcMgmt_tcTypeDeclaratorList vl_tcTypes := {};
  vl_tcTypes[0] := {
    name := "TCType",
    fsmName := "FSM1",
    entityType := "et1",
    customEntitySucc := ""
  }
  f_EPTF_LGenBase_declareTcType2(vl_tcTypes[0]);

  var EPTF_LGenBase_TcMgmt_ScenarioDeclarator2 vl_sc := {
    name:="Sc1",
    tcList := {} 
  }
  vl_sc.tcList[0] := {
    "", //    charstring tcName,
    0.0,//    float startDelay optional,
    0.0,//    float cpsToReach,
    true,//  boolean enableEntities,
    true,//  boolean enable, 
    omit,//   EPTF_LGenBase_TcMgmt_ParamRangeDeclaratorList ranges optional,
    omit,//   EPTF_LGenBase_TcMgmt_tcCustomParamDeclaratorList params optional,
    omit,//   EPTF_LGenBase_TcMgmt_GroupFinishConditions2 groupFinishConditions optional,
    omit,//   EPTF_LGenBase_TcMgmt_EntityFinishConditions entityFinishConditions optional,
    omit,//   EPTF_LGenBase_TcMgmt_EntityActionsList entityFinishActions optional,
    omit//    EPTF_LGenBase_TcMgmt_GroupActions2List tcFinishActions optional
  }
  vl_sc.tcList[0].tcName := "TCType";
  vl_sc.tcList[0].cpsToReach := c_EPTF_LGenBase_Test_tcCPS;
  f_EPTF_LGenBase_declareScenarioType2(vl_sc);  

  f_EPTF_LGenBase_createScenario2EntityGroup({"eg1","Sc1"},false);
  f_EPTF_LGenBase_startTrafficCase("eg1", "Sc1", "TCType");


  // name of var that contains the success counter
  var charstring pl_dataVarName := f_EPTF_LGenBase_getNamePrefix("eg1","Sc1","TCType") & c_EPTF_LGenBaseStats_nameOfTcNrOfSuccesses;

  timer t_wait := 0.0;
  t_wait.start; // wait for c_EPTF_LGenBase_Test_nofSuccesses success
  timer t_maxExecution := 4.0*int2float(c_EPTF_LGenBase_Test_nofSuccesses);
  t_maxExecution.start;
  alt {
    [f_EPTF_Var_getIntValue(f_EPTF_Var_getId(pl_dataVarName))==c_EPTF_LGenBase_Test_nofSuccesses] t_wait.timeout;
    [] t_maxExecution.timeout {
      setverdict(fail, log2str("The number of expected successes (",c_EPTF_LGenBase_Test_nofSuccesses,") was not reached in time. Current number of succ: ", f_EPTF_Var_getIntValue(f_EPTF_Var_getId(pl_dataVarName))));
      f_EPTF_Base_stop(none);
    }
  }
  
//  f_EPTF_LGenBase_stopTrafficCase("eg1", "Sc1", "TCType");
//  t_execution.start(2.0); t_execution.timeout;
  
  
  var integer vl_nofSuccess := f_EPTF_Var_getIntValue(f_EPTF_Var_getId(pl_dataVarName));
  
  action("vl_nofSuccess: ", vl_nofSuccess);
  action("v_registerCounterSucc: ", v_registerCounterSucc);
  action("v_LGenBase_Test_counters: ", v_LGenBase_Test_counters);
  
  
  // check the counters:
  if (v_registerCounterSucc != vl_nofSuccess*c_EPTF_LGenBase_Test_nofEvents) {
    setverdict(fail,log2str("Number of all received events (",v_registerCounterSucc,") is smaller than expected: ",vl_nofSuccess*c_EPTF_LGenBase_Test_nofEvents));
    f_EPTF_Base_stop(none);
  }
  if (sizeof(v_LGenBase_Test_counters) != vl_nofSuccess) {
    setverdict(fail,log2str("Number of entities that received events (",sizeof(v_LGenBase_Test_counters),") is smaller than expected: ",vl_nofSuccess));
    f_EPTF_Base_stop(none);
  }
  for(var integer i:=0; i<sizeof(v_LGenBase_Test_counters); i:=i+1) {
    if (v_LGenBase_Test_counters[i] != c_EPTF_LGenBase_Test_nofEvents) {
      setverdict(fail,log2str("Number of events received by entity #",i," (",v_LGenBase_Test_counters[i],") is smaller than expected: ",c_EPTF_LGenBase_Test_nofEvents));
      f_EPTF_Base_stop(none);
    }
    
  }
    
  f_EPTF_Base_stop(pass);

}


control{
  execute(tc_EPTF_LGenBase_Test_longCallStackSegfault_Post());
  execute(tc_EPTF_LGenBase_Test_longCallStackSegfault_Dispatch());

}

}  // end of module
