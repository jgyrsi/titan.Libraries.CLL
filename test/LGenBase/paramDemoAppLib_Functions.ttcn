///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: paramDemoAppLib_Functions
// 
//  Purpose:
//    This module demonstrates the usage of the LGenBase feature
// 
//  Module Parameters:
//    -
// 
//  Module depends on:
// 
//  Current Owner:
//    ELSZSKU
// 
//  Last Review Date:
//    2008-02-18
// 
// 
///////////////////////////////////////////////////////////
module paramDemoAppLib_Functions{

import from EPTF_CLL_LGenBase_Definitions all;
import from EPTF_CLL_LGenBase_Functions all;
import from EPTF_CLL_LGenBase_ConfigFunctions all;
import from EPTF_CLL_LGenBase_TrafficFunctions all;
import from EPTF_CLL_LGenBase_StepFunctions all;
import from paramDemoAppLib_Definitions all;
import from EPTF_CLL_Base_Functions all;

//modulepar integer tsp_numEntities := 100;
modulepar float tsp_reregSuccessPercent := 0.5;

function f_LGenBaseTest_Init_CT(in charstring pl_selfName, in charstring pl_entityNamePrefix)
runs on LGenBaseTest_CT {
  f_EPTF_LGenBase_init(pl_selfName, 0, pl_entityNamePrefix);  
  v_dummyInt := f_EPTF_LGenBase_declareBehaviorType("bhTest1", -1, null, null, null);
  v_dummyInt := f_EPTF_LGenBase_declareBehaviorType("bhTest2", -1, null, null, null);
  v_dummyInt := f_EPTF_LGenBase_declareFunction("f_LGenBaseTest_InvalidEvent", {testStepFunction := refers(f_LGenBaseTest_InvalidEvent)});
  v_dummyInt := f_EPTF_LGenBase_declareFunction("f_LGenBaseTest_register", {testStepFunction := refers(f_LGenBaseTest_register)});
  v_dummyInt := f_EPTF_LGenBase_declareFunction("f_LGenBaseTest_reRegister", {testStepFunction := refers(f_LGenBaseTest_reRegister)});
  v_dummyInt := f_EPTF_LGenBase_declareFunction("f_LGenBaseTest_doTraffic", {testStepFunction := refers(f_LGenBaseTest_doTraffic)});
  v_dummyInt := f_EPTF_LGenBase_declareFunction("f_LGenBaseTest_stopReRegisterIdle", {testStepFunction := refers(f_LGenBaseTest_stopReRegisterIdle)});
  v_dummyInt := f_EPTF_LGenBase_declareFunction("f_LGenBaseTest_stopReRegisterBusy", {testStepFunction := refers(f_LGenBaseTest_stopReRegisterBusy)});
}

function f_LGenBaseTest_register(in EPTF_LGenBase_TestStepArgs pl_ptr) 
runs on LGenBaseTest_CT {
  var float vl_rnd := rnd();
  if(vl_rnd < 0.7){
    log("--RegisterCounter- Entity#",pl_ptr.eIdx," has been succesfully registered. Successful registrations: ",v_registerCounterSucc);
    f_EPTF_LGenBase_step_trafficSuccess(pl_ptr);
  }else{
    log("--RegisterCounter- Registration of #",pl_ptr.eIdx," failed.");
    f_EPTF_LGenBase_step_trafficFailed(pl_ptr);
  }
} 

function f_LGenBaseTest_InvalidEvent(in EPTF_LGenBase_TestStepArgs pl_ptr) 
runs on LGenBaseTest_CT {
  log("ERROR: ","An invalid event has arrived in busy state!");
  f_EPTF_Base_stop();
}

function f_LGenBaseTest_reRegister(in EPTF_LGenBase_TestStepArgs pl_ptr) 
runs on LGenBaseTest_CT {
  log("--- Reregister ",pl_ptr.eIdx);
  if(vl_checkTraffic and vl_trafficStarted){
    setverdict(fail);
  }
  var float vl_rnd := rnd();
  if(vl_rnd < tsp_reregSuccessPercent){
    f_EPTF_LGenBase_step_trafficSuccess(pl_ptr);
  }else{
    f_EPTF_LGenBase_step_trafficFailed(pl_ptr);
  }
} 

function f_LGenBaseTest_doTraffic(in EPTF_LGenBase_TestStepArgs pl_ptr) 
runs on LGenBaseTest_CT {
  log("--- doTraffic#", pl_ptr.eIdx);
  vl_trafficStarted := true;
  var float vl_rnd := rnd();
  if(vl_rnd < 0.5){
    f_EPTF_LGenBase_step_trafficSuccess(pl_ptr);
  }else{
    f_EPTF_LGenBase_step_trafficFailed(pl_ptr);
  }
} 

  function f_LGenBaseTest_stopReRegisterIdle(in EPTF_LGenBase_TestStepArgs pl_ptr) 
  runs on LGenBaseTest_CT {
    v_reRegisterCounterStopped := v_reRegisterCounterStopped + 1;
    log("---AdminEvent  Stop traffic case has been called in idle state.");
  }
  function f_LGenBaseTest_stopReRegisterBusy(in EPTF_LGenBase_TestStepArgs pl_ptr) 
  runs on LGenBaseTest_CT {
    v_reRegisterCounterStopped := v_reRegisterCounterStopped + 1;
    log("---AdminEvent  Stop traffic case has been called in busy state.");
  }


}