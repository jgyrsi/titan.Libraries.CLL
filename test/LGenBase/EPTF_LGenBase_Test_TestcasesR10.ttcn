///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// 
//  Purpose:
//    This module provides functions for testing R10 functionalities and bugfixes
//    of LGenBase
// 
//  Module depends on:
//    -
// 
//  Current Owner:
//    LÃÂ¡szlÃÂ³ SkumÃÂ¡t (ELSZSKU)
// 
//  Last Review Date:
//    -
//
///////////////////////////////////////////////////////////////
module EPTF_LGenBase_Test_TestcasesR10
// [.objid{ itu_t(0) identified_organization(4) etsi(0)
// identified_organization(127) ericsson(5) testing(0)
// <put further nodes here if needed>}]
{
import from EPTF_LGenBase_Test_Functions all;
import from EPTF_CLL_LGenBase_TrafficFunctions all;
import from EPTF_CLL_LGenBase_ConfigDefinitions all;
import from EPTF_CLL_LGenBase_ConfigFunctions all;
import from EPTF_LGenBase_Test_TestcasesR3 all;
import from EPTF_CLL_LGenBase_EventHandlingFunctions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_LGenBase_Definitions all;

group timer_0point0_tests{


//TODO staticra es gauss timerre is!!!!, kirakni fuggvenybe a belet.

function f_EPTF_LGenBase_ownDispatch(in EPTF_LGenBase_TestStepArgs pl_ptr)
runs on EPTF_LGenBase_VarAndStatTest_CT{
  log("------Dispatch")
  f_EPTF_LGenBase_dispatchEvent({{v_LGenBase_VarAndStatTest_behav,1,{0,0},omit},{}})  
}

function f_LGenBase_Test_timer_0point0_test(in EPTF_LGenBase_FsmTableDeclarator pl_fsm)
runs on EPTF_LGenBase_VarAndStatTest_CT{
    f_LGenBase_Test_fsmVariables_init()
    v_dummyInt := f_EPTF_LGenBase_declareStep(c_EPTF_LGenBase_behavior, {"Finished",refers( f_EPTF_LGenBase_TestFinishedNow)});
    v_dummyInt := f_EPTF_LGenBase_declareStep(c_EPTF_LGenBase_behavior, {"FinishedFail",refers( f_EPTF_LGenBase_TestFinishedFail)});
    v_dummyInt := f_EPTF_LGenBase_declareStep(c_EPTF_LGenBase_behavior, {"checkVarEqual",refers( f_EPTF_LGenBase_checkVarEqualForInt)})
    v_dummyInt := f_EPTF_LGenBase_declareStep(c_EPTF_LGenBase_behavior, {"owndispatch",refers( f_EPTF_LGenBase_ownDispatch)})
    v_dummyInt := f_EPTF_LGenBase_declareFSMTable( pl_fsm )
    v_dummyInt := f_EPTF_LGenBase_declareTrafficCaseType({
        "TC1",
        "fsmtimer0_FSM",
        "et1",
        {}
      })
    v_dummyInt := f_EPTF_LGenBase_declareScenarioType3(
      {  "SC1",
        {
          {"TC1",{
              {target := {cpsToReach := 1.0}}
            }
          }
        },
        {}
      })
    f_EPTF_LGenBase_createScenario2EntityGroup({"eg1","SC1"},false)
    f_EPTF_LGenBase_dispatchEvent({{v_LGenBase_VarAndStatTest_behav,0,{0,0},omit},{}})
    v_dummyBool := f_EPTF_LGenBase_wait4TestFinished()
    f_EPTF_Base_cleanup_CT();
 
}

const EPTF_LGenBase_FsmTableDeclarator c_timer_0point0_fsm1 := {
        name := "fsmtimer0_FSM",
        fsmParams := {
          {timerList := {{"time",3.27},{"time2",0.0}}},
          {stateList := {"start","end"}},
          {varList :=
            { 
              {
                name := "TCvarA",
                initValue := {intVal := 0},
                scope := TC
              }
            }
          },
          {statMeasStatList := { }}
        },
        table := {
          classicTable := {
            {eventToListen := {"b1","input1",fsm},
              cellRow := {
                //state[0]==start
                {
                  {
                    {c_EPTF_LGenBase_stepName_timerStart,{timerName := "time"}},
                    {c_EPTF_LGenBase_stepName_timerStart,{timerName := "time2"}},
                    {"checkVarEqual",{varParams := {"TCvarA",{intVal := 0}}}}
                  }, 
                  omit, "end"
                },
                //state[1]==end 
                {
                  {
                    {"FinishedFail", omit}
                  }, 
                  omit, omit
                }
              }
            },
            {eventToListen := {c_EPTF_LGenBase_specialBName_timerTimeout,"time2",fsm},
              cellRow := {
                {
                  {
                    {"FinishedFail", omit}
                   }, 
                  omit, omit
                },
                {
                  {
                    {"checkVarEqual",{varParams := {"TCvarA",{intVal := 0}}}},
                    {"LGenBase: Increment variable",{varParams := {"TCvarA",{intVal := 1}}}}
                  }, 
                  omit, omit
                }
              }
            },
            {eventToListen := {c_EPTF_LGenBase_specialBName_timerTimeout,"time",fsm},
              cellRow := {
                {
                  {
                    {"FinishedFail", omit}
                  }, 
                  omit, omit
                },
                {  
                  {  //ha ide ertunk, mar a time2 lekattant es allapotatmenet is kellett legyen
                    {"checkVarEqual",{varParams := {"TCvarA",{intVal := 1}}}},
                    {"Finished", omit}
                  }, 
                  omit, omit
                }
              }
            }
            
          }
        }
      }

const EPTF_LGenBase_FsmTableDeclarator c_timer_0point0_fsm2 := {
        name := "fsmtimer0_FSM",
        fsmParams := {
          {timerList := {{"time",3.27},{"time2",0.0}}},
          {stateList := {"start","end"}},
          {varList :=
            { 
              {
                name := "TCvarA",
                initValue := {intVal := 0},
                scope := TC
              }
            }
          },
          {statMeasStatList := { }}
        },
        table := {
          classicTable := {
            {eventToListen := {"b1","input1",fsm},
              cellRow := {
                //state[0]==start
                {
                  {
                    {c_EPTF_LGenBase_stepName_timerStart,{timerName := "time"}},
                    {c_EPTF_LGenBase_stepName_timerStart,{timerName := "time2"}},
                    {"owndispatch", omit },
                    {"checkVarEqual",{varParams := {"TCvarA",{intVal := 1}}}}
                  }, 
                  omit, "end"
                },
                //state[1]==end 
                {
                  {
                    {"FinishedFail", omit}
                  }, 
                  omit, omit
                }
              }
            },
            {eventToListen := {"b1","input2",fsm},
              cellRow := {
                //state[0]==start
                {
                  {
                    {"LGenBase: Increment variable",{varParams := {"TCvarA",{intVal := 1}}}}
                  }, 
                  omit, "end"
                },
                //state[1]==end 
                {
                  {
                    {"FinishedFail", omit}
                  }, 
                  omit, omit
                }
              }
            },
            {eventToListen := {c_EPTF_LGenBase_specialBName_timerTimeout,"time2",fsm},
              cellRow := {
                {
                  {
                    {"FinishedFail", omit}
                   }, 
                  omit, omit
                },
                {
                  {
                    {"checkVarEqual",{varParams := {"TCvarA",{intVal := 1}}}},
                    {"LGenBase: Increment variable",{varParams := {"TCvarA",{intVal := 1}}}}
                  }, 
                  omit, omit
                }
              }
            },
            {eventToListen := {c_EPTF_LGenBase_specialBName_timerTimeout,"time",fsm},
              cellRow := {
                {
                  {
                    {"FinishedFail", omit}
                  }, 
                  omit, omit
                },
                {  
                  {  //ha ide ertunk, mar a time2 lekattant es allapotatmenet is kellett legyen
                    {"checkVarEqual",{varParams := {"TCvarA",{intVal := 2}}}},
                    {"Finished", omit}
                  }, 
                  omit, omit
                }
              }
            }
            
          }
        }
      }

const EPTF_LGenBase_FsmTableDeclarator c_timer_0point0_fsm3 := {
        name := "fsmtimer0_FSM",
        fsmParams := {
          {randomTimerList := {{"time",3.27,3.28},{"time2", 0.0, 1.0 }}},
          {stateList := {"start","end"}},
          {varList :=
            { 
              {
                name := "TCvarA",
                initValue := {intVal := 0},
                scope := TC
              }
            }
          },
          {statMeasStatList := { }}
        },
        table := {
          classicTable := {
            {eventToListen := {"b1","input1",fsm},
              cellRow := {
                //state[0]==start
                {
                  {
                    {c_EPTF_LGenBase_stepName_timerStart,{timerName := "time"}},
                    {c_EPTF_LGenBase_stepName_timerStart,{timerName := "time2"}},
                    {"checkVarEqual",{varParams := {"TCvarA",{intVal := 0}}}}
                  }, 
                  omit, "end"
                },
                //state[1]==end 
                {
                  {
                    {"FinishedFail", omit}
                  }, 
                  omit, omit
                }
              }
            },
            {eventToListen := {c_EPTF_LGenBase_specialBName_timerTimeout,"time2",fsm},
              cellRow := {
                {
                  {
                    {"FinishedFail", omit}
                   }, 
                  omit, omit
                },
                {
                  {
                    {"checkVarEqual",{varParams := {"TCvarA",{intVal := 0}}}},
                    {"LGenBase: Increment variable",{varParams := {"TCvarA",{intVal := 1}}}}
                  }, 
                  omit, omit
                }
              }
            },
            {eventToListen := {c_EPTF_LGenBase_specialBName_timerTimeout,"time",fsm},
              cellRow := {
                {
                  {
                    {"FinishedFail", omit}
                  }, 
                  omit, omit
                },
                {  
                  {  //ha ide ertunk, mar a time2 lekattant es allapotatmenet is kellett legyen
                    {"checkVarEqual",{varParams := {"TCvarA",{intVal := 1}}}},
                    {"Finished", omit}
                  }, 
                  omit, omit
                }
              }
            }
            
          }
        }
      }

const EPTF_LGenBase_FsmTableDeclarator c_timer_0point0_fsm4 := {
        name := "fsmtimer0_FSM",
        fsmParams := {
          {randomTimerList := {{"time",3.27,3.28},{"time2", 0.0, 1.0 }}},
          {stateList := {"start","end"}},
          {varList :=
            { 
              {
                name := "TCvarA",
                initValue := {intVal := 0},
                scope := TC
              }
            }
          },
          {statMeasStatList := { }}
        },
        table := {
          classicTable := {
            {eventToListen := {"b1","input1",fsm},
              cellRow := {
                //state[0]==start
                {
                  {
                    {c_EPTF_LGenBase_stepName_timerStart,{timerName := "time"}},
                    {c_EPTF_LGenBase_stepName_timerStart,{timerName := "time2"}},
                    {"owndispatch", omit },
                    {"checkVarEqual",{varParams := {"TCvarA",{intVal := 1}}}}
                  }, 
                  omit, "end"
                },
                //state[1]==end 
                {
                  {
                    {"FinishedFail", omit}
                  }, 
                  omit, omit
                }
              }
            },
            {eventToListen := {"b1","input2",fsm},
              cellRow := {
                //state[0]==start
                {
                  {
                    {"LGenBase: Increment variable",{varParams := {"TCvarA",{intVal := 1}}}}
                  }, 
                  omit, "end"
                },
                //state[1]==end 
                {
                  {
                    {"FinishedFail", omit}
                  }, 
                  omit, omit
                }
              }
            },
            {eventToListen := {c_EPTF_LGenBase_specialBName_timerTimeout,"time2",fsm},
              cellRow := {
                {
                  {
                    {"FinishedFail", omit}
                   }, 
                  omit, omit
                },
                {
                  {
                    {"checkVarEqual",{varParams := {"TCvarA",{intVal := 1}}}},
                    {"LGenBase: Increment variable",{varParams := {"TCvarA",{intVal := 1}}}}
                  }, 
                  omit, omit
                }
              }
            },
            {eventToListen := {c_EPTF_LGenBase_specialBName_timerTimeout,"time",fsm},
              cellRow := {
                {
                  {
                    {"FinishedFail", omit}
                  }, 
                  omit, omit
                },
                {  
                  {  //ha ide ertunk, mar a time2 lekattant es allapotatmenet is kellett legyen
                    {"checkVarEqual",{varParams := {"TCvarA",{intVal := 2}}}},
                    {"Finished", omit}
                  }, 
                  omit, omit
                }
              }
            }
            
          }
        }
      }

  
///////////////////////////////////////////////////////////
//  Testcase: tc_LGenBase_Test_timer_0point0_statechangebeforetimertimeoff
// 
//  Purpose: Tests the 0.0 static timer handling - before 0.0 timer goes off, the state change should happen.
//
//  Action: The fsm starts a normal and a 0.0 timer in the state "start", checks the value of TCvarA variable 
//          and changes state to end.
//          When the 0.0 timer goes off, the fsm should be in the end state. The fsm checks the value of 
//          TCvarA variable and increases the value.
//          When the second timer goes off, the value of the variable is checked again.
//          The wrong event-state combinations are honored with a fail verdict and a wrong variable value also.
//
//  Expected Result: before 0.0 timer goes off, the state change should happen.
///////////////////////////////////////////////////////////
testcase tc_LGenBase_Test_timer_0point0_statechangebeforetimertimeoff()
  runs on EPTF_LGenBase_VarAndStatTest_CT{
    
       f_LGenBase_Test_timer_0point0_test(c_timer_0point0_fsm1);
}
  
///////////////////////////////////////////////////////////
//  Testcase: tc_LGenBase_Test_timer_0point0_dispatchaftertimerstart
// 
//  Purpose: Tests the 0.0 static timer handling - timer timeout should occur after the dispatch has ended (post semantic).
//
//  Action: The fsm starts a normal and a 0.0 timer in the state "start", an event dispatch("b1","input2") is made and it checks 
//          the value of TCvarA variable and changes state to end.
//          Immediately after the dispatched event is handled with increasing the value of the variable.
//          When the 0.0 timer goes off, the fsm should be in the end state. The fsm checks the value of 
//          TCvarA variable and increases the value.
//          When the second timer goes off, the value of the variable is checked again.
//          The wrong event-state combinations are honored with a fail verdict and a wrong variable value also.
//
//  Expected Result: 0.0 timer timeout should occur after the dispatch has ended.
///////////////////////////////////////////////////////////  
testcase tc_LGenBase_Test_timer_0point0_dispatchaftertimerstart()
  runs on EPTF_LGenBase_VarAndStatTest_CT{
      f_LGenBase_Test_timer_0point0_test(c_timer_0point0_fsm2);
}

///////////////////////////////////////////////////////////
//  Testcase: tc_LGenBase_Test_timer_0point0_statechangebeforetimertimeoff_random
// 
//  Purpose: Tests the 0.0, 0.0 random timer handling - before 0.0 timer goes off, the state change should happen.
//
//  Action: The fsm starts a normal and a 0.0 timer in the state "start", checks the value of TCvarA variable 
//          and changes state to end.
//          When the 0.0 timer goes off, the fsm should be in the end state. The fsm checks the value of 
//          TCvarA variable and increases the value.
//          When the second timer goes off, the value of the variable is checked again.
//          The wrong event-state combinations are honored with a fail verdict and a wrong variable value also.
//
//  Expected Result: before 0.0, 0.0 random timer goes off, the state change should happen.
///////////////////////////////////////////////////////////
testcase tc_LGenBase_Test_timer_0point0_statechangebeforetimertimeoff_random()
  runs on EPTF_LGenBase_VarAndStatTest_CT{
    
       f_LGenBase_Test_timer_0point0_test(c_timer_0point0_fsm3);
}
  
///////////////////////////////////////////////////////////
//  Testcase: tc_LGenBase_Test_timer_0point0_dispatchaftertimerstart_random
// 
//  Purpose: Tests the 0.0, 0.0 random timer handling - timer timeout should occur after the dispatch has ended (post semantic).
//
//  Action: The fsm starts a normal and a 0.0 timer in the state "start", an event dispatch("b1","input2") is made and it checks 
//          the value of TCvarA variable and changes state to end.
//          Immediately after the dispatched event is handled with increasing the value of the variable.
//          When the 0.0 timer goes off, the fsm should be in the end state. The fsm checks the value of 
//          TCvarA variable and increases the value.
//          When the second timer goes off, the value of the variable is checked again.
//          The wrong event-state combinations are honored with a fail verdict and a wrong variable value also.
//
//  Expected Result: 0.0, 0.0 random timer timeout should occur after the dispatch has ended.
///////////////////////////////////////////////////////////  
testcase tc_LGenBase_Test_timer_0point0_dispatchaftertimerstart_random()
  runs on EPTF_LGenBase_VarAndStatTest_CT{
      f_LGenBase_Test_timer_0point0_test(c_timer_0point0_fsm4);
}


}

control{
  execute(tc_LGenBase_Test_timer_0point0_statechangebeforetimertimeoff());
  execute(tc_LGenBase_Test_timer_0point0_dispatchaftertimerstart());
  execute(tc_LGenBase_Test_timer_0point0_statechangebeforetimertimeoff_random());
  execute(tc_LGenBase_Test_timer_0point0_dispatchaftertimerstart_random());

}

}  // end of module
