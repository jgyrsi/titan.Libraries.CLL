///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
EPTF_CLI_Test.tc_EPTF_CLI_Test_registerCommand
EPTF_CLI_Test.tc_EPTF_CLI_Test_executeCustomCommand
EPTF_CLI_Test.tc_EPTF_CLI_Test_executeCommandSetCommands
EPTF_CLI_Test.tc_EPTF_CLI_Test_executeParallelCommands
EPTF_CLI_Test.tc_EPTF_CLI_Test_splitString

EPTF_CLI_Test.tc_EPTF_CLI_Test_stopCommand
EPTF_CLI_Test.tc_EPTF_CLI_Test_Terminal_help
EPTF_CLI_Test.tc_EPTF_CLI_Test_Terminal_exit
EPTF_CLI_Test.tc_EPTF_CLI_Test_Terminal_quit
EPTF_CLI_Test.tc_EPTF_CLI_Test_Terminal_stop
EPTF_CLI_Test.tc_EPTF_CLI_Test_Terminal_stopAll
EPTF_CLI_Test.tc_EPTF_CLI_Test_Terminal_echo
EPTF_CLI_Test.tc_EPTF_CLI_Test_Terminal_echo2
EPTF_CLI_Test.tc_EPTF_CLI_Test_Terminal_comment
EPTF_CLI_Test.tc_EPTF_CLI_Test_Terminal_display
EPTF_CLI_Test.tc_EPTF_CLI_Test_Terminal_echo2PlusNotExistingCmd

