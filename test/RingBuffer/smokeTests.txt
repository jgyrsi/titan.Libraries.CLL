///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
GenericRingBuffer_TestCases.tc_GRB_init
GenericRingBuffer_TestCases.tc_GRB_size
GenericRingBuffer_TestCases.tc_GRB_push_pop
GenericRingBuffer_TestCases.tc_GRB_access
GenericRingBuffer_TestCases.tc_GRB_HK72355

IntegerRingBuffer_TestCases.tc_RB_init
IntegerRingBuffer_TestCases.tc_RB_size
IntegerRingBuffer_TestCases.tc_RB_push_pop
IntegerRingBuffer_TestCases.tc_RB_access
IntegerRingBuffer_TestCases.tc_RB_HK72355
