///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

module GenericRingBuffer_TestCases
{

// declares the ring buffer base type
#define EPTF_BASE_TYPE UserType

// defines the ring buffer base type
type record of integer UserType;

// instantiate the code for managing a ring buffer built from the base type
#include "EPTF_CLL_GenericRingBuffer_Functions.ttcnin"

// undef the base type to avoid error in case of multiple careless "#include"-s
#undef EPTF_BASE_TYPE 

type component MTC_CT {}

// helper method - dumps a ring buffer to simple array and compares it with an expected array
function f_checkBuffer(in UserTypeRingBuffer checkedRingBuffer, in UserTypeList expectedBuffer) 
{
  var UserTypeList dump;
  f_EPTF_UserType_RB_dump(checkedRingBuffer, dump);
  if (dump == expectedBuffer) 
  {
    setverdict(pass);
  }  
  else  
  {
    setverdict(fail);
  };
} 

// test ring buffer initialization
testcase tc_GRB_init() runs on MTC_CT system MTC_CT
{ 
  var UserTypeRingBuffer rb1, rb2, rb3, rb4;

  // initialise a ring buffer and check capacity and empty content
  f_EPTF_UserType_RB_init(rb1, 238);
  if (238 == f_EPTF_UserType_RB_capacity(rb1)) 
  { 
    setverdict(pass); 
  } 
  else 
  { 
    setverdict(fail); 
  }
  f_checkBuffer(rb1, {});

  // initialize to default capacity
  f_EPTF_UserType_RB_init(rb2);
  // initialize to invalid capacity values
  f_EPTF_UserType_RB_init(rb3, 0);
  f_EPTF_UserType_RB_init(rb4, -100);

  // check that ring buffers are initialized to default capacity
  if (rb2 == rb3 and rb2 == rb4 and c_EPTF_RB_default_capacity == f_EPTF_UserType_RB_capacity(rb4)) 
  { 
    setverdict(pass); 
  } 
  else 
  { 
    setverdict(fail); 
  }

  // check that ring buffers are empty
  f_checkBuffer(rb2, {});
}

// test empty and size methods
testcase tc_GRB_size() runs on MTC_CT system MTC_CT 
{
  var UserTypeRingBuffer rb;

  // new ring buffer shall be empty
  f_EPTF_UserType_RB_init(rb, 5);
  if (f_EPTF_UserType_RB_empty(rb) and 0 == f_EPTF_UserType_RB_size(rb))
  { 
    setverdict(pass); 
  } 
  else 
  { 
    setverdict(fail); 
  }

  // after adding 3 items buffer is not empty and shall contain 3 items
  f_EPTF_UserType_RB_push_back( rb, {1,1} );
  f_EPTF_UserType_RB_push_back( rb, {1,1} );
  f_EPTF_UserType_RB_push_back( rb, {1,1} );
  if (not f_EPTF_UserType_RB_empty(rb) and 3 == f_EPTF_UserType_RB_size(rb))
  { 
    setverdict(pass); 
  } 
  else 
  { 
    setverdict(fail); 
  }
  
  // after adding 3 more items buffer shall contain 5 items
  f_EPTF_UserType_RB_push_back( rb, {1,1} );
  f_EPTF_UserType_RB_push_back( rb, {1,1} );
  f_EPTF_UserType_RB_push_back( rb, {1,1} );
  if (5 == f_EPTF_UserType_RB_size(rb))
  { 
    setverdict(pass); 
  } 
  else 
  { 
    setverdict(fail); 
  }

  // after removing 2 items buffer shall contain 3 items
  f_EPTF_UserType_RB_pop_front(rb);
  f_EPTF_UserType_RB_pop_front(rb);
  if (3 == f_EPTF_UserType_RB_size(rb))
  { 
    setverdict(pass); 
  } 
  else 
  { 
    setverdict(fail); 
  }

  // after clear operation buffer shall be empty
  f_EPTF_UserType_RB_clear(rb);
  if (f_EPTF_UserType_RB_empty(rb) and 0 == f_EPTF_UserType_RB_size(rb))
  { 
    setverdict(pass); 
  } 
  else 
  { 
    setverdict(fail); 
  }
}

// test adding and removing elements to/from ring buffer
testcase tc_GRB_push_pop() runs on MTC_CT system MTC_CT 
{ 
  var UserTypeRingBuffer buffer;
  var UserTypeList myData;

  // init buffer with capacity of 5 elements...
  f_EPTF_UserType_RB_init(buffer, 5);

  //////////////////////////////////////////////
  //      Test push_back() and pop_front()    //

  // push 3 items into buffer and check content...
  f_EPTF_UserType_RB_push_back( buffer, {1,1} );
  f_EPTF_UserType_RB_push_back( buffer, {2,2} );
  f_EPTF_UserType_RB_push_back( buffer, {3,3} );
  myData := {{1,1}, {2,2}, {3,3}};
  f_checkBuffer(buffer,myData);

  // pop 2 items from buffer and check content...
  f_EPTF_UserType_RB_pop_front(buffer);
  f_EPTF_UserType_RB_pop_front(buffer);
  myData := {{3,3}};
  f_checkBuffer(buffer,myData);

  // push 6 items into buffer and check content...
  f_EPTF_UserType_RB_push_back( buffer, {4,4} );
  f_EPTF_UserType_RB_push_back( buffer, {5,5} );
  f_EPTF_UserType_RB_push_back( buffer, {6,6} );
  f_EPTF_UserType_RB_push_back( buffer, {7,7} );
  f_EPTF_UserType_RB_push_back( buffer, {8,8} );
  f_EPTF_UserType_RB_push_back( buffer, {9,9} );
  myData := { {5,5}, {6,6}, {7,7}, {8,8}, {9,9} };
  f_checkBuffer(buffer,myData);

  // pop 3 items from buffer and check content...
  f_EPTF_UserType_RB_pop_front(buffer);
  f_EPTF_UserType_RB_pop_front(buffer);
  f_EPTF_UserType_RB_pop_front(buffer);
  myData := {{8,8}, {9,9}};
  f_checkBuffer(buffer,myData);

  // pop other 2 items from buffer and check buffer is empty...
  f_EPTF_UserType_RB_pop_front(buffer);
  f_EPTF_UserType_RB_pop_front(buffer);
  myData := {};
  f_checkBuffer(buffer,myData);

  // pop from empty buffer shall not couse ant effect...
  f_EPTF_UserType_RB_pop_front(buffer);
  myData := {};
  f_checkBuffer(buffer,myData);

  //////////////////////////////////////////////
  //      Test push_front() and pop_back()    //

  // push 3 items into buffer and check content...
  f_EPTF_UserType_RB_push_front(buffer, {2,2});
  f_EPTF_UserType_RB_push_back(buffer, {3,3});
  f_EPTF_UserType_RB_push_front(buffer, {1,1});
  myData := {{1,1}, {2,2}, {3,3}};
  f_checkBuffer(buffer,myData);

  // pop 2 items from buffer and check content...
  f_EPTF_UserType_RB_pop_back(buffer);
  f_EPTF_UserType_RB_pop_back(buffer);
  myData := {{1,1}};
  f_checkBuffer(buffer,myData);

  // push 6 items into buffer and check content...
  f_EPTF_UserType_RB_push_front(buffer, {2,2});
  f_EPTF_UserType_RB_push_front(buffer, {3,3});
  f_EPTF_UserType_RB_push_front(buffer, {4,4});
  f_EPTF_UserType_RB_push_front(buffer, {5,5});
  f_EPTF_UserType_RB_push_front(buffer, {6,6});
  f_EPTF_UserType_RB_push_front(buffer, {7,7});
  myData := {{7,7}, {6,6}, {5,5}, {4,4}, {3,3}};
  f_checkBuffer(buffer,myData);

  // pop 3 items from buffer and check content...
  f_EPTF_UserType_RB_pop_back(buffer);
  f_EPTF_UserType_RB_pop_back(buffer);
  f_EPTF_UserType_RB_pop_back(buffer);
  myData := {{7,7}, {6,6}};
  f_checkBuffer(buffer,myData);

  // pop other 2 items from buffer and check buffer is empty...
  f_EPTF_UserType_RB_pop_back(buffer);
  f_EPTF_UserType_RB_pop_back(buffer);
  myData := {};
  f_checkBuffer(buffer,myData);

  // pop from empty buffer shall not couse ant effect...
  f_EPTF_UserType_RB_pop_back(buffer);
  myData := {};
  f_checkBuffer(buffer,myData);
} 

// test accessing elements of ring buffer
testcase tc_GRB_access() runs on MTC_CT system MTC_CT 
{
  var UserTypeRingBuffer rb;
  var UserType item;

  // init ring buffer
  f_EPTF_UserType_RB_init(rb, 5);

  // checked access to empty buffer shall return false
  if (not f_EPTF_UserType_RB_at(rb, 0, item))
  { 
    setverdict(pass); 
  } 
  else 
  { 
    setverdict(fail); 
  }

  // push an item into buffer...
  f_EPTF_UserType_RB_push_back(rb, {42,43,44} );

  // verify checked access...
  if (f_EPTF_UserType_RB_at(rb, 0, item) and {42,43,44} == item)
  { 
    setverdict(pass); 
  } 
  else 
  { 
    setverdict(fail); 
  }

  // verify checked access at invalid index...
  if ( not f_EPTF_UserType_RB_at(rb, -100, item) and 
       not f_EPTF_UserType_RB_at(rb, 2, item) and 
       not f_EPTF_UserType_RB_at(rb, 5, item) )
  { 
    setverdict(pass); 
  } 
  else 
  { 
    setverdict(fail); 
  }

  // verify unchecked access...
  if ( {42,43,44} == f_EPTF_UserType_RB_front(rb) and 
       {42,43,44} == f_EPTF_UserType_RB_back(rb) and 
       {42,43,44} == f_EPTF_UserType_RB_get(rb, 0))
  { 
    setverdict(pass); 
  } 
  else 
  { 
    setverdict(fail); 
  }

  // push 6 items into buffer and pop 2 item
  // rb.data should contain: {5,6,x,x,4}
  f_EPTF_UserType_RB_push_back(rb, {1,1} );
  f_EPTF_UserType_RB_push_back(rb, {2,2} );
  f_EPTF_UserType_RB_push_back(rb, {3,3} );
  f_EPTF_UserType_RB_push_back(rb, {4,4} );
  f_EPTF_UserType_RB_push_back(rb, {5,5} );
  f_EPTF_UserType_RB_push_back(rb, {6,6} );
  f_EPTF_UserType_RB_pop_front(rb);
  f_EPTF_UserType_RB_pop_front(rb);

  // check access...
  if ( {4,4} == f_EPTF_UserType_RB_front(rb)  and 
       {6,6} == f_EPTF_UserType_RB_back(rb)   and 
       {4,4} == f_EPTF_UserType_RB_get(rb, 0) and 
       {6,6} == f_EPTF_UserType_RB_get(rb, 2) )
  { 
    setverdict(pass); 
  } 
  else 
  { 
    setverdict(fail); 
  }
}

// test solution for HK72355
// - check RB works fine with capacity=1 (head points to valid place after init)
// - check no unbound value at beginnning of data buffer after push_bach if capacity>1
testcase tc_GRB_HK72355() runs on MTC_CT system MTC_CT 
{
  var UserTypeRingBuffer rb;

  // check RB with capacity=1

  // calling front after init and push_back caused dynamic error in case capacity=1
  f_EPTF_UserType_RB_init(rb, 1);
  f_EPTF_UserType_RB_push_back(rb, {42, 42});
  if ( {42, 42} == f_EPTF_UserType_RB_front(rb) and  {42, 42} == f_EPTF_UserType_RB_back(rb) )
  { 
    setverdict(pass); 
  } 
  else 
  { 
    setverdict(fail); 
  }

  // inverse test: back after push_front
  f_EPTF_UserType_RB_init(rb, 1);
  f_EPTF_UserType_RB_push_front(rb, {4242, 4242});
  if ({4242, 4242} == f_EPTF_UserType_RB_back(rb) and {4242, 4242} == f_EPTF_UserType_RB_front(rb))
  { 
    setverdict(pass); 
  } 
  else 
  { 
    setverdict(fail); 
  }

  // adding more values
  f_EPTF_UserType_RB_push_front(rb, {1,1});
  f_EPTF_UserType_RB_push_back(rb, {2,2});
  f_EPTF_UserType_RB_push_back(rb, {3,3});
  if ({3,3} == f_EPTF_UserType_RB_back(rb) and {3,3} == f_EPTF_UserType_RB_front(rb))
  { 
    setverdict(pass); 
  } 
  else 
  { 
    setverdict(fail); 
  }
  // check length of data buffer
  if (lengthof(rb.data) <= 1)
  { 
    setverdict(pass); 
  } 
  else 
  { 
    setverdict(fail); 
  }

  // check RB with capacity=2

  // check that data_buffer[0] is not unbound after first push_back
  // NOTE: data can contain unbound items in other cases, e.g. after push_front!
  f_EPTF_UserType_RB_init(rb, 2);
  f_EPTF_UserType_RB_push_back(rb, {42,42});
  if ( {42,42} == f_EPTF_UserType_RB_front(rb) and {42,42} == f_EPTF_UserType_RB_back(rb) and  //< check content correct
       rb.data[0] == {42,42}  and //< check 0. item not unbound
       lengthof(rb.data) <= 2)
  { 
    setverdict(pass); 
  } 
  else 
  { 
    setverdict(fail); 
  }

  // check RB with capacity>2

  // check that data_buffer[0] is not unbound after first push_back
  // NOTE: data can contain unbound items in other cases, e.g. after push_front!
  f_EPTF_UserType_RB_init(rb, 5);
  f_EPTF_UserType_RB_push_back(rb, {42,42});
  if ( {42,42} == f_EPTF_UserType_RB_front(rb) and {42,42} == f_EPTF_UserType_RB_back(rb) and  //< check content correct
       rb.data[0] == {42,42}  and //< check 0. item not unbound
       lengthof(rb.data) <= 5)
  { 
    setverdict(pass); 
  } 
  else 
  { 
    setverdict(fail); 
  }
}

control
{
  execute(tc_GRB_init());
  execute(tc_GRB_size());
  execute(tc_GRB_push_pop());
  execute(tc_GRB_access());
  execute(tc_GRB_HK72355());
} 

} //module
