///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_SMacro_PerfTest_Functions
//
// Purpose:
//   This module contains functions to test the performance of EPTF SMacro feature.
//
// Module Parameters:
//    -
//  Module depends on:
//    <EPTF_SMacro_PerfTest_Definitions>
//    <EPTF_CLL_SMacro_Functions>
//    <EPTF_CLL_Base_Functions>
//    <EPTF_CLL_Common_Definitions>
//    <TestResultGen>
//    <ttcn_ericsson_se_TitanSim_Perftest>
//    <TCCConversion_Functions>
//
// Current Owner:
//    Balazs Barcsik (ebalbar)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////
module EPTF_SMacro_PerfTest_Functions {

//=========================================================================
// Import Part
//=========================================================================
import from EPTF_SMacro_PerfTest_Definitions all;
import from EPTF_CLL_SMacro_Functions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_Common_Definitions all;
import from TestResultGen all;
import from ttcn_ericsson_se_TitanSim_Perftest all;
import from TCCConversion_Functions all;
import from TCCMaths_Functions all;

//=========================================================================
// Functions
//=========================================================================

function f_EPTF_SMacro_PerfTest_init_CT(
  in charstring pl_selfName
) runs on SMacro_PerfTest_CT {
  
  
  f_EPTF_Base_init_CT(pl_selfName);
  f_EPTF_SMacro_init_CT(pl_selfName);
  
  v_macro_name1 := "macro_name1";
  v_macro_name2 := "macro_name2";
  v_macro_value1 := "macro_value1";
  v_macro_value2 := "macro_function2";
  
  v_start_time := f_TestResultGen_getCurrentTime();

  if(f_EPTF_SMacro_define(v_macro_name1, v_macro_value1) != 0) {
    setverdict(fail, "Macro cannot be defined, but macro is correct");
    f_EPTF_Base_stop(none);
  }
  
  if(f_EPTF_SMacro_define(v_macro_name2, v_macro_value2) != 0) {
    setverdict(fail, "Macro cannot be defined, but macro is correct");
    f_EPTF_Base_stop(none);
  }
  
  f_EPTF_SMacro_registerCalcFn(
    pl_functionName := "macro_function2",
    pl_macro_function := refers(f_EPTF_SMacro_PerfTest_dummyCallback)
  );
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_SMacro_PerfTest_dummyCallback
// 
//  Purpose:
//    dummy callback function
//
//  Parameters:
//    pl_macroArgs - <in EPTF_CharstringList> - macro arguments
//    pl_userArgs - <in EPTF_IntegerList> - user specific macro arguments
//
//  Return Value:
//   charstring - "resolved_value"
//
//  Errors:
//    -
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////

function f_EPTF_SMacro_PerfTest_dummyCallback(
  in EPTF_CharstringList pl_macroArgs,
  in EPTF_IntegerList pl_userArgs := {}
) runs on SMacro_PerfTest_CT return charstring {
  
  return "resolved_value";
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_SMacro_PerfTest_generateStringTemplate
// 
//  Purpose:
//    generates String Templates based on the parameters
//
//  Parameters:
//    pl_macroName - *in charstring* - name of the macro
//    pl_macroPar - *in charstring* - parameter of the macro
//    pl_numOfStringMacros - *in integer* - number of the String Macros within the String Template
//    pl_numOfStringMacroParam - *in integer* - number of the parameters in the String Macros
//    pl_lengthOfConcatString - *in integer* - length of concat string between String Macros
//    pl_embedLevel - *in integer* - defines the level for embedded macro
//
//  Return Value:
//    charstring - the generated String Template
//
//  Errors:
//    -
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////

function f_EPTF_SMacro_PerfTest_generateStringTemplate(
  in charstring pl_macroName := "",
  in charstring pl_macroPar := "",
  in integer pl_numOfStringMacros :=1,
  in integer pl_numOfStringMacroParams := 0,
  in integer pl_lengthOfConcatString := 0,
  in integer pl_embedLevel := 0
  ) runs on SMacro_PerfTest_CT return charstring {

  var charstring vl_stringTemplate := ""; // return value
  var charstring vl_stringMacroName := "$("&pl_macroName;
  var charstring vl_stringMacro := "";
  var charstring vl_stringMacroParams := "";
  var charstring vl_concatString := "";
  
  if(pl_macroName == "" or pl_numOfStringMacros == 0){
    return "";
  }
  
  if(pl_macroPar == ""){
    pl_numOfStringMacroParams := 0;
  }
  
  // generate concat string 
  for(var integer vl_l := 0; vl_l < pl_lengthOfConcatString; vl_l := vl_l+1){
    vl_concatString := vl_concatString&"@";
  }
  
  // create first string macro with params
  for(var integer vl_i := 0; vl_i < pl_numOfStringMacroParams; vl_i := vl_i+1)
  {
    vl_stringMacroParams := vl_stringMacroParams&" , "&log2str(pl_macroPar);
  }
  vl_stringMacro := vl_stringMacroName&vl_stringMacroParams&")";

  // create embedded macro with params  
  for(var integer vl_j := 0; vl_j < pl_embedLevel; vl_j := vl_j+1){   
    
    for(var integer vl_l :=0; vl_l <pl_numOfStringMacroParams; vl_l := vl_l+1){
      
      if(vl_l == 0) {
        vl_stringMacroParams := log2str(vl_stringMacro);
      }
      else{
        vl_stringMacroParams := vl_stringMacroParams&", "&log2str(vl_stringMacro);
      }     
    }
    vl_stringMacro := vl_stringMacroName&" , "&vl_stringMacroParams&")";
  } 
  
  // create string template
  for(var integer vl_k := 0; vl_k < pl_numOfStringMacros; vl_k := vl_k+1){
    vl_stringTemplate := vl_stringTemplate&vl_stringMacro&vl_concatString;
  }

  return vl_stringTemplate;
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_SMacro_PerfTest_generateExpr
// 
//  Purpose:
//    generates String Macro parameter which consists mathematical expressions
//
//  Parameters:
//    pl_numOfOperands - *in integer* - number of the operands in the mathematical expression
//    pl_precedenceZero - *in boolean* - if true each expression will be in brackets
//
//  Return Value:
//    charstring - the generated mathematical expression
//
//  Errors:
//    -
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////
function f_EPTF_SMacro_PerfTest_generateExpr(
  in integer pl_numOfOperands,
  in boolean pl_precedenceZero := false
  ) runs on SMacro_PerfTest_CT return charstring {

  if(pl_numOfOperands == 0){
    return "";
  }
  
  var EPTF_CharstringList vl_operations := {"*","/","%","-","+"};
  var charstring vl_paramEVAL := int2str(3); // return value
  var integer vl_idx := 0;
  
  var integer vl_numOfOperations := sizeof(vl_operations);
  
  for(var integer vl_i := 0; vl_i < pl_numOfOperands; vl_i := vl_i+1){
    vl_idx := vl_i rem vl_numOfOperations;
    
    if(pl_precedenceZero) {
      vl_paramEVAL := "("&vl_paramEVAL&vl_operations[vl_idx]&int2str(vl_i+2)&")";
    }
    else {
      vl_paramEVAL := vl_paramEVAL&vl_operations[vl_idx]&int2str(vl_i+2);
    }
  }
  return vl_paramEVAL;

}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_SMacro_PerfTest_measure
// 
//  Purpose:
//    measure the execution time of f_EPTF_SMacro_resolve function
//
//  Parameters:
//    pl_nameOfTestResult - *in charstring* - name of the test, e.g.: "simple macro resolving time"
//    pl_numOfIteration - *in integer* - number of sampling
//    pl_numOfStringMacros - *in integer* - number of the String Macros within the String Template
//    pl_numOfStringMacroParam - *in integer* - number of the parameters in the String Macros
//    pl_lengthOfConcatString - *in integer* - length of concat string between String Macros
//    pl_embedLevel - *in integer* - defines the level for embedded macro
//    pl_numOfExpr - *in integer* - number of mathematical expressions
//    pl_precedenceZero - *in boolean* - if true bracket will be used for each mathematical operation
//    pl_autoEVAL - *in boolean* - switch for auto EVAL
//
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////

function f_EPTF_SMacro_PerfTest_measure(
  in charstring pl_stringTemplate,
  in charstring pl_nameOfTestResult,
  in integer pl_numOfIteration,
  in integer pl_numOfStringMacros,
  in integer pl_numOfStringMacroParams,
  in integer pl_lengthOfConcatString,
  in integer pl_embedLevel,
  in integer pl_numOfExpr := 0, // only for EVAL
  in boolean pl_precedenceZero := false, // only for EVAL
  in boolean pl_autoEVAL := false // only for Auto EVAL
  ) runs on SMacro_PerfTest_CT {
  
  // init v_testResult
  v_testResult.name := pl_nameOfTestResult;
  v_testResult.purpose := "To measure the "&pl_nameOfTestResult;
  f_TestResultGen_getEnvironment(v_testResult.environment);
  v_testResult.parameter_list := {
      {name := "Number of iteration", unit := "-", base := log2str(pl_numOfIteration) },
      {name := "Number of String Macros in the String Template", unit := "-", base := log2str(pl_numOfStringMacros) },
      {name := "Number of Parameters in the String Macro", unit := "-", base := log2str(pl_numOfStringMacroParams) },
      {name := "Length of concat string between String Macros", unit := "-", base := log2str(pl_lengthOfConcatString) },
      {name := "Embed level", unit := "-", base := log2str(pl_embedLevel) },
      {name := "Number of mathematical expressions in the EVAL macro parameter", unit := "-", base := log2str(pl_numOfExpr) },
      {name := "Brackets are used for mathematical expressions", unit := "-", base := log2str(pl_precedenceZero) }
  };
  
  v_testResult.start_time := v_start_time;
  
  // start resolving String Templates
  var float vl_startTime := f_EPTF_Base_getRelTimeInSecs();

  for(var integer vl_i := 0; vl_i < pl_numOfIteration; vl_i := vl_i+1)
  {
    f_EPTF_SMacro_resolve(pl_stringTemplate, pl_autoEVAL); 
  }
  
  var float vl_endTime := f_EPTF_Base_getRelTimeInSecs();
  // end resolving String Templates
  
  // stores the result
  v_testResult.end_time := f_TestResultGen_getCurrentTime();
  
  // S(n+1)=S(n)*P+1 --> S(n)=1+P+P^2+.....+P^n=(P^n-1)/(P-1) OR if P=1 --> S(n)=n+1
  // n=pl_embedLevel+1, P=pl_numOfStringMacroParams, S(n)=vl_numOfResolveEmbed
  var integer vl_numOfResolveEmbed := pl_embedLevel+1;
  if(pl_numOfStringMacroParams != 1) {
    vl_numOfResolveEmbed := (f_powII(pl_numOfStringMacroParams,pl_embedLevel+1)-1)/(pl_numOfStringMacroParams-1);
  }
   
  v_testResult.result_list[0] := {
      name := "Resolving time of one String Template",
      unit := "microsec",
      base := log2str((vl_endTime-vl_startTime)/int2float(pl_numOfIteration)*1e6)
  };
  
  if(pl_embedLevel > 0){
  v_testResult.result_list[1] :=  {
      name := "Resolving time of one String Macro",
      unit := "microsec",
      base := log2str((vl_endTime-vl_startTime)/int2float(pl_numOfIteration)*1e6/int2float(pl_numOfStringMacros)/int2float(vl_numOfResolveEmbed))
    };
  }
  else {
  v_testResult.result_list[1] :=  {
      name := "Resolving time of one String Macro",
      unit := "microsec",
      base := log2str((vl_endTime-vl_startTime)/int2float(pl_numOfIteration)*1e6/int2float(pl_numOfStringMacros))
    };
  }
  
  f_TestResultGen_appendResult(%testcaseId&"_"&f_unichar2charstr(v_testResult.start_time)&".xml", v_testResult);
  
//   action("*** String Template to resolve: ",pl_stringTemplate);
//   action("*** Resolved value: ", f_EPTF_SMacro_resolve(pl_stringTemplate));
//   action("*** Returning with testResult: ",v_testResult);
  }

} // module
