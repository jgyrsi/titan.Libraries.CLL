///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_SMacro_PerfTest_Definitions
//
// Purpose:
//   This module contains data types to test the performance of EPTF SMacro feature.
//
// Module Parameters:
//    -
//  Module depends on:
//    <EPTF_CLL_SMacro_Definitions>
//    <TestResultGen>
//    <ttcn_ericsson_se_TitanSim_Perftest>
//
// Current Owner:
//    Balazs Barcsik (ebalbar)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////
module EPTF_SMacro_PerfTest_Definitions {
//=========================================================================
// Import Part
//=========================================================================

import from EPTF_CLL_SMacro_Definitions all;
import from TestResultGen all;
import from ttcn_ericsson_se_TitanSim_Perftest all;

///////////////////////////////////////////////////////////
//  Component: EPTF_SMacro_PerfTest_CT
// 
//  Purpose:
//    The component to run Performance Tests of SMacro feature
// 
//  Elements:
//    c_param_string - *charstring* - constant string for macro parameters
//    v_macro_name1 - *charstring* - name of the macro1
//    v_macro_name2 - *charstring* - name of the macro2
//    v_macro_value1 - *charstring* - value of the macro1
//    v_macro_value2 - *charstring* - value of the macro2
//    v_testResult - <Testresult> - stores the test result
//    v_start_time - *charstring* - start time of the test case
//
//  Detailed Comments:
//  
//
///////////////////////////////////////////////////////////
type component SMacro_PerfTest_CT extends EPTF_SMacro_CT, TestResultGen_CT {
  
  const charstring c_param_string := "parameter";
  
  var charstring  v_macro_name1;
  var charstring  v_macro_name2;

  var charstring v_macro_value1;
  var charstring v_macro_value2;
  
  var Testresult v_testResult;
  var charstring v_start_time := "";
}

} //module
