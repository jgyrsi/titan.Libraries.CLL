///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_CS_Test_Functions
//
// Purpose:
//   This module contains function definitions for testing EPTF Central Scheduling.
//
// Module Parameters:
//    -
//  Module depends on:
//
// Current Owner:
//    Balazs Barcsik (ebalbar)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////

module EPTF_CS_Test_Functions
// [.objid{ itu_t(0) identified_organization(4) etsi(0) 
// identified_organization(127) ericsson(5) testing(0)
// <put further nodes here if needed>}]
{

//=========================================================================
// Import Part
//=========================================================================
import from EPTF_CS_Test_Definitions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_CSAdmin_Functions all;
import from EPTF_CLL_CSUIHandler_Functions all;
// Insert imports here if applicable!
import from EPTF_CLL_ExecCtrl_Definitions all;
import from EPTF_CLL_ExecCtrl_Functions all;
// You can use the import_part skeleton!
import from EPTF_CLL_Common_Definitions all;
//nem fog kelleni
import from EPTF_CLL_CS_Definitions all;

// UIHandler
import from EPTF_CLL_UIHandler_Definitions all;
import from EPTF_CLL_UIHandler_WidgetFunctions all;
import from XTDP_PDU_Defs all;

import from EPTF_CLL_DataSource_Functions all;
import from EPTF_CLL_DataSource_Definitions all;

//=========================================================================
// Module Parameters
//=========================================================================

//=========================================================================
// Altstep
//=========================================================================
function f_EPTF_CS_getSelectedLGenIdex() runs on EPTF_CS_ApplAdminBase_CT return integer {
  return v_EPTF_CS_selectedLGen;
}

altstep as_LGen_Test_msgHandler() runs on CS_LGen_Test_CT
{
  []CS_PCO.receive(CS_A_LGenExecuteTrafficCaseList:?) -> value v_execBurst_msg
  {
    v_execBurstResult_msg := {}
    for(v_execBurstIdx := 0;
      v_execBurstIdx < sizeof(v_execBurst_msg);
      v_execBurstIdx := v_execBurstIdx + 1) {

        f_LGen_Test_trafficCaseSelector();
        //echo back the admin, LGen indexes
        v_execBurstResult_msg[v_execBurstIdx].data[2] := v_execBurst_msg[v_execBurstIdx].data[0];
        v_execBurstResult_msg[v_execBurstIdx].data[3] := v_execBurst_msg[v_execBurstIdx].data[1];
      }

    CS_PCO.send(v_execBurstResult_msg);
    repeat;
  }
  []CS_PCO.receive
  {
    log(%definitionId& ": unexpected message received from admin.");
    f_EPTF_Base_stop();
  }
}
///////////////////////////////////////////////////////////////////////////
//ApplAdmin Altsteps
///////////////////////////////////////////////////////////////////////////
altstep as_CS_Test_msgHandler() runs on CS_ApplAdmin_Test_CT
{
  []CS_PCO.receive(CS_A_LGenExecutionResultList:?) -> value v_execBurstResult_msg sender v_lgenCompRef
  {
    var integer vl_LGenIdx := f_EPTF_CS_lookupBySenderAddress(v_lgenCompRef);
      log("####v_execBurstResult_msg: ", v_execBurstResult_msg);
      for(v_execBurstIdx := 0; v_execBurstIdx < sizeof(v_execBurstResult_msg);v_execBurstIdx := v_execBurstIdx + 1) {
         log("")
      f_EPTF_CS_handleExecResult(
        v_execBurstResult_msg[v_execBurstIdx].execResult,
        v_execBurstIdx,
          vl_LGenIdx);

        v_trafficCounter := v_trafficCounter + 1;
        log("v_trafficCounter ", v_trafficCounter);
   }
f_EPTF_CS_LGenBurstFinished(vl_LGenIdx);
    repeat;
  }
  []CS_PCO.receive(CS_A_LGenStatus:?) -> value v_lgenStatus_msg sender v_lgenCompRef
    {
      
    if(not v_lgenStatus_msg.LGenEnabled) {
        v_nofEnabledLGens := v_nofEnabledLGens + 1;
      }
      if (v_nofEnabledLGens == 5) {
        if(v_trafficCounter == 7) {
          setverdict(pass);
        } else {
          log("Error: unixpected number of running traffic cases: ", v_trafficCounter, " expected: 7");
          setverdict(fail);
        }
      }   
    f_EPTF_CS_handleLGenStatusMsg(v_lgenStatus_msg.LGenEnabled, v_lgenCompRef);
    repeat;
  }
  []CS_PCO.receive
  {
    log(%definitionId& ": unexpected message received from LGen.");
    f_EPTF_Base_stop();
  }
}

altstep as_CS_Test_msgHandler2() runs on CS_ApplAdmin_Test_CT
{
  []CS_PCO.receive(CS_A_LGenExecutionResultList:?) -> value v_execBurstResult_msg sender v_lgenCompRef
  {
    var integer vl_LGenIdx := f_EPTF_CS_lookupBySenderAddress(v_lgenCompRef);


    for(v_execBurstIdx := 0;
        v_execBurstIdx < sizeof(v_execBurstResult_msg);
        v_execBurstIdx := v_execBurstIdx + 1) {

      f_EPTF_CS_handleExecResult(
        v_execBurstResult_msg[v_execBurstIdx].execResult,
        v_execBurstIdx,
        vl_LGenIdx);
   }
      f_EPTF_CS_LGenBurstFinished(vl_LGenIdx);

      var integer vl_compIdx := f_EPTF_Base_upcast(v_lgenCompRef);

       for(var integer i := 0; i < sizeof(v_execBurstResult_msg);i := i + 1) {
        if (v_execBurstResult_msg[i].data[3] ==  vl_compIdx) {
          setverdict(pass);
        } else {
          log("Error: Wrong selected LGen: ", vl_compIdx, " expected: ", v_execBurstResult_msg[i].data[3]);
          setverdict(fail);
        }
        if (v_execBurstResult_msg[i].data[2] == v_adminIdx) {
        setverdict(pass);
        } else {
          log("Error: Wrong Admin index: ", v_adminIdx, " expected: ", v_execBurstResult_msg[v_execBurstIdx].data[2]);
          setverdict(fail);
        }
        if (v_execBurstResult_msg[i].data[3] ==  v_execBurstResult_msg[i].data[1]) {
          setverdict(pass);
        } else {
          log("Error: Wrong selected LGen: ", v_execBurstResult_msg[i].data[1], " expected: ", v_execBurstResult_msg[i].data[3]);
          setverdict(fail);
        }
        if (v_execBurstResult_msg[i].data[2] == v_execBurstResult_msg[i].data[0]) {
        setverdict(pass);
        } else {
          log("Error: Wrong Admin index: ", v_execBurstResult_msg[i].data[0], " expected: ", v_execBurstResult_msg[v_execBurstIdx].data[2]);
          setverdict(fail);
        }

      }
    repeat;
  }
  []CS_PCO.receive(CS_A_LGenStatus:?) -> value v_lgenStatus_msg sender v_lgenCompRef
    {
      if(v_lgenStatus_msg.LGenEnabled) {
        v_nofEnabledLGens := v_nofEnabledLGens + 1;
      }
      if(v_nofEnabledLGens == 5) {
        if(v_nofLgens == 5) {
          setverdict(pass);
        } else {
          log("Error: wrong number of the created Lgens: ", v_nofLgens, " expected: 5");
        }
      }

    f_EPTF_CS_handleLGenStatusMsg(v_lgenStatus_msg.LGenEnabled, v_lgenCompRef);
    repeat;
  }
  []CS_PCO.receive
  {
    log(%definitionId& ": unexpected message received from LGen.");
    f_EPTF_Base_stop();
  }
}
//=========================================================================
// Functions
//=========================================================================
///////////////////////////////////////////////////////////////////////////
//LGen Functions
///////////////////////////////////////////////////////////////////////////
function f_CS_Test_LGen_behavior(in charstring pl_selfName, in integer pl_adminIdx, in integer pl_lgenIdx)
runs on CS_LGen_Test_CT
{
  log(%definitionId& ": started");

  f_LGen_Test_init(pl_selfName, pl_adminIdx, pl_lgenIdx);

  CS_PCO.send(CS_A_LGenStatus:{LGenEnabled:=true});

  log(%definitionId& ": entering main alt loop");
  alt {
    []as_LGen_Test_msgHandler();
  }
  log(%definitionId& ": leaving main alt loop");

  f_EPTF_Base_cleanup_CT();

  log(%definitionId& ": finished");
}

function f_LGen_Test_init(in charstring pl_selfName, in integer pl_adminIdx, in integer pl_lgenIdx) runs on CS_LGen_Test_CT
{
  log("f_LGen_A_init() started");
  f_EPTF_CS_LGenBase_init_CT(pl_selfName);
  v_LGenIdx := pl_lgenIdx;
  v_adminIdx := pl_adminIdx;
  f_EPTF_Base_registerCleanup(refers(f_LGen_Test_cleanup));

  log("f_LGen_A_init() finished");
}

function f_LGen_Test_cleanup() runs on CS_LGen_Test_CT
{
  log("f_LGen_A_cleanup() started");
  CS_PCO.send(CS_A_LGenStatus:{LGenEnabled:=false});

//  unmap(self:PIPEasp_PCO, system:PIPEasp_PCO);

  log("f_LGen_A_cleanup() finished");
}


function f_LGen_Test_trafficCaseSelector() runs on CS_LGen_Test_CT
{
  // Select and execute traffic case
  select (v_execBurst_msg[v_execBurstIdx].trafficCaseSelector)
  {
    case (0) {f_LGen_Test_trafficCase(v_execBurst_msg[v_execBurstIdx].trafficCaseSelector);}
    case (1) {f_LGen_Test_trafficCaseConf();}
    case else {
      log(%definitionId& ": unhandled traffic case: ", v_execBurst_msg[v_execBurstIdx].trafficCaseSelector);
      f_EPTF_Base_stop();
    }
  }
}

function f_LGen_Test_ExecCtrlbehavior(in charstring pl_selfName, in integer pl_nofCsAdmins) runs on EPTF_ExecCtrl_CT
{
  f_EPTF_ExecCtrl_behavior(pl_selfName, pl_nofCsAdmins)
}

function f_LGen_Test_trafficCase(in integer pl_value) runs on CS_LGen_Test_CT
{
  log("f_LGen_A_trafficCase ",pl_value," started");

      timer T;
      T.start(1.0);
      T.timeout;
      // Compose the result...
      v_execBurstResult_msg[v_execBurstIdx] := {
        trafficCaseSelector := v_execBurst_msg[v_execBurstIdx].trafficCaseSelector,
        execResult := pass,
        data := {v_adminIdx,v_LGenIdx}//v_execBurst_msg[v_execBurstIdx].data.tcData} // echoing back...
      }

  log("f_LGen_A_trafficCase ",pl_value," finished");
}

function f_LGen_Test_trafficCaseConf() runs on CS_LGen_Test_CT
{
  log("f_LGen_A_trafficCaseConf started");
  
      timer T;
      T.start(1.0);
      T.timeout;
      // Compose the result...
      v_execBurstResult_msg[v_execBurstIdx] := {
        trafficCaseSelector := v_execBurst_msg[v_execBurstIdx].trafficCaseSelector,
        execResult := pass,
        data := {v_adminIdx, v_LGenIdx}
      }
   log("f_LGen_A_trafficCaseConf finished");
}

///////////////////////////////////////////////////////////////////////////
//ApplAdmin Functions
///////////////////////////////////////////////////////////////////////////
function f_CS_Test_sendExecBurst() runs on CS_ApplAdmin_Test_CT
{
  for(var integer i:= 0; i < sizeof(v_execBurst_msg); i := i + 1) {
    v_execBurst_msg[i].data := {v_adminIdx, f_EPTF_Base_upcast(f_EPTF_CS_Admin_getCompRefOfSelectedLGen())}
  }
  CS_PCO.send(v_execBurst_msg) to f_EPTF_CS_Admin_getCompRefOfSelectedLGen();
  v_execBurst_msg := {}
}

function f_CS_Test_createLGen(
  in integer pl_LGenIndex,
  in charstring pl_LGenName,
  in charstring pl_LGenHostName)
runs on CS_ApplAdmin_Test_CT
return EPTF_Base_CT
{
  var integer vl_lgenIdx;
  var CS_LGen_Test_CT vl_lgen;
  if(pl_LGenHostName == "") {
    vl_lgen := CS_LGen_Test_CT.create(pl_LGenName);
  } else {
    vl_lgen := CS_LGen_Test_CT.create(pl_LGenName, pl_LGenHostName);
  }
  //count the number of the created LGen-s
  v_nofLgens := v_nofLgens + 1;
  
  connect(self:CS_PCO, vl_lgen:CS_PCO);
  vl_lgenIdx := f_EPTF_Base_upcast(vl_lgen); //convert to integer then to EPTF_Base_CT
  vl_lgen.start(f_CS_Test_LGen_behavior(pl_LGenName, v_adminIdx, vl_lgenIdx));
  return  f_EPTF_Base_downcast(vl_lgenIdx);
}

function f_CS_Test_addExecBurstElem(
  in integer pl_trafficCaseSelector,
  in EPTF_IntegerList pl_currentRangeItems)
runs on CS_ApplAdmin_Test_CT
{
  v_execBurstIdx := sizeof(v_execBurst_msg);
  v_execBurst_msg[v_execBurstIdx] := {
        trafficCaseSelector := pl_trafficCaseSelector,
        data := { -1, -1}
  }
  
}
function f_CS_Test_cleanup() runs on CS_ApplAdmin_Test_CT {
  if (v_nofEnabledLGens == 5) {
    setverdict(pass);  
  } else {
    log("Not enabled enough LGen: ", v_nofEnabledLGens, " expected: 5");
    setverdict(fail);
  }
}

function f_CS_test_init(
  in charstring pl_adminName,
  in integer pl_adminIdx/*, in boolean pl_loadBalancingAvailable*/)
runs on CS_ApplAdmin_Test_CT
{
  // Init database and get params...
  log(%definitionId& " started");

  v_adminIdx := pl_adminIdx;


  // Registering the functions
  v_EPTF_CS_LGenCreate := refers(f_CS_Test_createLGen);
  v_EPTF_CS_addExecBurstElem := refers(f_CS_Test_addExecBurstElem);
  v_EPTF_CS_sendExecBurst := refers(f_CS_Test_sendExecBurst);

  v_msgHandler := activate(as_CS_Test_msgHandler());

  f_EPTF_CS_Admin_setEntityNameSuffix("_A"); // resulting entity name: CS_Entity_Tc_A
  f_EPTF_CS_Admin_declareTrafficCase("TC_A1", 0);
  f_EPTF_CS_Admin_declareTrafficCase("TC_A2", 0);
  f_EPTF_CS_Admin_declareTrafficCase("TC_A3", 1);
  f_EPTF_Base_registerCleanup(refers(f_CS_Test_cleanup));
  log(%definitionId& " finished");
}

function f_CS_test_declareTrafficCase(
  in charstring pl_adminName,
  in integer pl_adminIdx/*, in boolean pl_loadBalancingAvailable*/)
runs on CS_ApplAdmin_Test_CT
{
   v_adminIdx := pl_adminIdx;


  // Registering the functions
  v_EPTF_CS_LGenCreate := refers(f_CS_Test_createLGen);
  v_EPTF_CS_addExecBurstElem := refers(f_CS_Test_addExecBurstElem);
  v_EPTF_CS_sendExecBurst := refers(f_CS_Test_sendExecBurst);

  v_msgHandler := activate(as_CS_Test_msgHandler());

  f_EPTF_CS_Admin_setEntityNameSuffix("_A"); // resulting entity name: CS_Entity_Tc_A

  f_EPTF_CS_Admin_declareTrafficCase("TC_A1", 0);
  f_EPTF_CS_Admin_declareTrafficCase("TC_A2", 0);
  f_EPTF_CS_Admin_declareTrafficCase("TC_A3", 1);
  f_EPTF_Base_registerCleanup(refers(f_CS_Test_cleanup));
  log(%definitionId& " finished");
}

function f_CS_test_getCompRefOfSelectedLGen(
  in charstring pl_adminName,
  in integer pl_adminIdx/*, in boolean pl_loadBalancingAvailable*/)
runs on CS_ApplAdmin_Test_CT
{
  v_adminIdx := pl_adminIdx;


  // Registering the functions
  v_EPTF_CS_LGenCreate := refers(f_CS_Test_createLGen);
  v_EPTF_CS_addExecBurstElem := refers(f_CS_Test_addExecBurstElem);
  v_EPTF_CS_sendExecBurst := refers(f_CS_Test_sendExecBurst);

  v_msgHandler := activate(as_CS_Test_msgHandler2());

  f_EPTF_CS_Admin_setEntityNameSuffix("_B"); // resulting entity name: CS_Entity_Tc_A
 

  f_EPTF_CS_Admin_declareTrafficCase("TC_B", 1);
  
  
  log(%definitionId& " finished");
}
function f_EPTF_CS_Test_behavior(
  in charstring pl_adminName,
  in integer pl_adminIdx,
  in EPTF_ExecCtrl_CT pl_execCtrlRef,
  in boolean pl_useLoadBalancing,
  in EPTF_DataSource_CT pl_dataSource_Ref := null)
runs on CS_ApplAdmin_Test_CT
{
  f_EPTF_CS_Admin_behavior(
    pl_adminName,
    pl_adminIdx,
    pl_execCtrlRef,
    refers(f_CS_test_init),
    pl_useLoadBalancing,
    pl_dataSource_Ref);
}

function f_EPTF_CS_Test_declareTrafficCase(
  in charstring pl_adminName,
  in integer pl_adminIdx,
  in EPTF_ExecCtrl_CT pl_execCtrlRef,
  in boolean pl_useLoadBalancing)
runs on CS_ApplAdmin_Test_CT
{
  f_EPTF_CS_Admin_behavior(
    pl_adminName,
    pl_adminIdx,
    pl_execCtrlRef,
    refers(f_CS_test_declareTrafficCase),
    pl_useLoadBalancing);
}
function f_EPTF_CS_Test_getCompRefOfSelectedLGen(
  in charstring pl_adminName,
  in integer pl_adminIdx,
  in EPTF_ExecCtrl_CT pl_execCtrlRef,
  in boolean pl_useLoadBalancing)
runs on CS_ApplAdmin_Test_CT
{
  f_EPTF_CS_Admin_behavior(
    pl_adminName,
    pl_adminIdx,
    pl_execCtrlRef,
    refers(f_CS_test_getCompRefOfSelectedLGen),
    pl_useLoadBalancing);
}


function f_EPTF_CS_Test_HL26276_init(
  in charstring pl_adminName,
  in integer pl_adminIdx/*, in boolean pl_loadBalancingAvailable*/)
runs on CS_ApplAdmin_Test_CT
{
  // Init database and get params...
  log(%definitionId& " started");

  v_adminIdx := pl_adminIdx;


  // Registering the functions
  v_EPTF_CS_LGenCreate := refers(f_CS_Test_createLGen);
  v_EPTF_CS_addExecBurstElem := refers(f_CS_Test_addExecBurstElem);
  v_EPTF_CS_sendExecBurst := refers(f_CS_Test_sendExecBurst);

  v_msgHandler := activate(as_CS_Test_msgHandler2());

  f_EPTF_CS_Admin_setEntityNameSuffix("_A"); // resulting entity name: CS_Entity_Tc_A
  f_EPTF_CS_Admin_declareTrafficCase("TC_A1", 0);
  f_EPTF_CS_Admin_declareTrafficCase("TC_A2", 0);
  f_EPTF_CS_Admin_declareTrafficCase("TC_A3", 1);
  f_EPTF_Base_registerCleanup(refers(f_CS_Test_cleanup));
  log(%definitionId& " finished");
}

function f_EPTF_CS_Test_HL26276(
  in charstring pl_adminName,
  in integer pl_adminIdx,
  in EPTF_ExecCtrl_CT pl_execCtrlRef,
  in EPTF_UIHandler_CT pl_UIHandlerRef,
  in boolean pl_useLoadBalancing)
runs on CS_ApplAdmin_Test_UIHandler_CT
{
  f_EPTF_CS_Admin_UIHandler_behavior(
    pl_adminName,
    pl_adminIdx,
    pl_execCtrlRef,
    pl_UIHandlerRef,
    refers(f_EPTF_CS_Test_HL26276_init),
    pl_useLoadBalancing,
    null);
}

/*
function f_EPTF_CS_Test_enableTcs(in EPTF_IntegerList pl_enableTcs) runs on EPTF_ExecCtrl_TC {
  
  f_EPTF_LGenBase_declareScenarioType2(pl_enableTcs)

}
  
function f_EPTF_ExecCtrl_Test_behavior(
  in charstring pl_name,
  in integer pl_clientNum,
  in EPTF_IntegerList pl_enableTcs) 
runs in EPTF_ExecCtrl_CT
{
  f_EPTF_CS_Test_enableTcs(pl_enableTcs);
  f_EPTF_LGenBase_declareScenarioType2(pl_enableTcs)
  f_EPTF_ExecCtrl_behavior(pl_name, pl_clientNum);
}
*/
//=========================================================================
// Testcases
//=========================================================================

// Insert test cases here if applicable!
// You can use the test_case skeleton!

//=========================================================================
// Control
//=========================================================================

// Insert control part here if applicable!
// You can use the control_part skeleton!

}  // end of module
