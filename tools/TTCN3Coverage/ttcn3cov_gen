#!/bin/bash
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////
#Generates coverage info to TTCN3Coverage.log
#and prints out only the overall information.
if [ "$*" ]
then
echo "ttcn3cov_gen version 1.0"
echo "Report generator for TTCN3Coverage"
echo "Usage: Run the script without arguments in the bin directoy."
echo ""
echo "Prints out short summary of the TTCN3 coverage information."
echo "Generates detailed coverage info to TTCN3Coverage.log"
echo "Detailed report on unused functions is written to TTCN3Coverage_unused.log"
echo ""
echo "All coverage information produced by the executable test suite are"
echo "merged into TTCN3Coverage.cov"
exit
fi

TTCN3COV_FNS=""
if [ -r "TTCN3Coverage.fns" ]
then
TTCN3COV_FNS="TTCN3Coverage.fns"
fi

#if [ "$(ls *.cov 2> /dev/null)" == "" ]
#then
#touch xx.cov
#echo "###### TTCN3Coverage is reset #######"
#sleep 1
#exit
#fi

echo "Merging coverage results started at $(date)"
TTCN3COV_INPUTFILES=$(echo *.cov | xargs ls 2> /dev/null)

TTCN3COV_OUTPUTFILE="/dev/null"
if [ "${TTCN3COV_INPUTFILES}" ]
then
TTCN3COV_OUTPUTFILE="TTCN3Coverage.log"
fi

#echo "###### TTCN3Coverage Summary #######"

: | echo ${TTCN3COV_FNS} ${TTCN3COV_INPUTFILES} | xargs $(dirname $0)/ttcn3cov > tmp.cov
: | $(dirname $0)/ttcn3cov ${TTCN3COV_FNS} tmp.cov | tee  ${TTCN3COV_OUTPUTFILE} | grep "coverage\|All calls\|All function calls"

# remove all input files and use the generated log to store data:
rm tmp.cov
if [ "${TTCN3COV_INPUTFILES}" ]
then
echo *.cov | xargs rm;cp TTCN3Coverage.log TTCN3Coverage.cov
fi

if [ "${TTCN3COV_INPUTFILES}" ]
then
TTCN3COV_OUTPUTFILE="TTCN3Coverage_unused.log"
fi

:|$(dirname $0)/ttcn3cov_unusedFns | tee ${TTCN3COV_OUTPUTFILE} | grep "Functions not used"

echo "Detailed TTCN3 coverage info is available in TTCN3Coverage.log and TTCN3Coverage_unused.log"
echo "Merging coverage results finished at $(date)"
#echo "###################################"
