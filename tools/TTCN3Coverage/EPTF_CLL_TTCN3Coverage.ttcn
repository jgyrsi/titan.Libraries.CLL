///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_TTCN3Coverage
// 
//  Purpose:
//    This module contains the TTCN3 interface to the TTCN3Coverage feature.
// 
//  Module depends on:
//    none
//
//  Module Parameters:
//    tsp_EPTF_TTCN3Coverage_usedModules - charstring - sets the file mask
//        used to select modules to generate TTCN3Coverage info.
//       (i.e. the list of modules to enable coverage detection ended by .cc)
//        NOTE: If this module parameter is changed in the config file,
//        the test case <tc_EPTF_TTCN3Coverage_setConfig> should be executed
//        and the test suite should be recompiled.
//        The value "" disables coverage detection. Default: "*Definitions *Functions"
//    tsp_EPTF_TTCN3Coverage_disabled - boolean - disables coverage detection
//       without the need of recompile. Default: false
//
//  Current Owner:
//    Jozsef Gyurusi (ethjgi)
// 
//  Last Review Date:
//    2009-xx-xx
//
//  Detailed Comments:
//    This module contains the TTCN3 interface functions for TTCN3Coverage.
//    It integrates seemlessly into titan and mctr_gui.
//
//    The coverage information provided by this feature can be used
//    to find TTCN3 functions and code lines that are called the most,
//    determine what fraction of the total code lines or functions were
//    executed during the tests, or which functions were not used at all.
//
//    Code line: code line in this context is a TTCN3 statement that can be
//    executed during runtime. Type definitions, module parameters etc.
//    are not takes as code lines. Code lines are determined from the
//    titan-generated cc code, where line information is placed.
//
//    The coverage information automatically accumulated during successive
//    execution of several test cases, or even between different projects,
//    if they use the same bin directory. The collected data can be reset
//    by the corresponding test case provided by this feature.
//    All collected statistics is available via log files for offline use.
//
//  Requirements:
//    i)   The tools gawk and sed has to be installed and accessible via $PATH.
//    ii)  Required shells: sh, bash, tcsh.
//    iii) The line number info should be enabled in the Makefile:
//        COMPILER_FLAGS = -L
//
//  Installation:
//    1) Add the current directory as the first item in the $PATH:
//       set PATH=.:${PATH};export PATH
//
//    2) Start mctr_gui in that shell and open your project
//
//    3) Add the TTCN3Coverage.grp to the project (mctr_gui)
//
//    4) Create symlinks and Makefile, build and run the test cases as usual
//
//    5) Well done! The coverage information will be printed automatically
//       to the mctr_gui's main screen when the test suite terminates.
//       For further customization use the module parameters
//       and the test cases provided.
//
//    Public functions:
//       <f_EPTF_TTCN3Coverage_system>
//       <f_EPTF_TTCN3Coverage_shell>
//       <f_EPTF_TTCN3Coverage_detailed>
//       <f_EPTF_TTCN3Coverage_reset>
//       <f_EPTF_TTCN3Coverage_unusedFns>
//
//    Public test cases:
//       <tc_EPTF_TTCN3Coverage_setConfig>
//       <tc_EPTF_TTCN3Coverage_detailed>
//       <tc_EPTF_TTCN3Coverage_reset>
//       <tc_EPTF_TTCN3Coverage_unusedFns>
//
//    All other functions in this module are private! 
//
///////////////////////////////////////////////////////////////


module EPTF_CLL_TTCN3Coverage {

modulepar charstring tsp_EPTF_TTCN3Coverage_usedModules := "*Definitions *Functions" // for best results this default value should be the same as TTCN3COV_USEDMODULES_DEFAULT in TTCN3COV compiler
modulepar boolean tsp_EPTF_TTCN3Coverage_disabled := false; // disables coverage detection without the need of recompile

type record of integer TTCN3Coverage_IntegerList;

type record EPTF_TTCN3Coverage_Info {
  charstring filename,
  integer codeLines,
  TTCN3Coverage_IntegerList lines
}

type record of EPTF_TTCN3Coverage_Info EPTF_TTCN3CoverageDb;


//function f_EPTF_TTCN3Coverage_init(out EPTF_TTCN3CoverageDb pl_coverageDb, out integer pl_hashmapId) {
//  pl_coverageDb := {};
//}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_TTCN3Coverage_system
// 
//  Purpose:
//    Execute shell command from TTCN
//
//  Parameters:
//    - pl_command - *in charstring* - the name and arguments of the command to execute
//
//  Return Value:
//    -
//
///////////////////////////////////////////////////////////
external function f_EPTF_TTCN3Coverage_system(in charstring pl_command);

///////////////////////////////////////////////////////////
//  Function: f_EPTF_TTCN3Coverage_shell
// 
//  Purpose:
//    Execute shell script from TTCN
//
//  Parameters:
//    - pl_command - *in charstring* - the name of the script to execute
//
//  Return Value:
//    -
//
///////////////////////////////////////////////////////////
external function f_EPTF_TTCN3Coverage_shell(in charstring pl_command);

///////////////////////////////////////////////////////////
//  TestCase: tc_EPTF_TTCN3Coverage_setConfig
// 
//  Purpose:
//    To set the module parameters defined in the configFile
//
//  Parameters:
//
//  Return Value:
//    -
//
//  Detailed Comments:
//    This test case should be executed after the module parameter
//    <tsp_EPTF_TTCN3Coverage_usedModules> was changed in the
//    titan config file. Then the test suite should be rebuilt
//    so that the new settings could take effect.
//
///////////////////////////////////////////////////////////
testcase tc_EPTF_TTCN3Coverage_setConfig() runs on TTCN3Coverage_CT {
  //f_EPTF_TTCN3Coverage_system("./ttcn3cov_gen");
  //f_EPTF_TTCN3Coverage_system("./ttcn3cov *.cov | tee TTCN3Coverage.log | grep \"overage\"");
  var charstring vl_command :="echo 'TTCN3COV_USEDMODULES=\""&tsp_EPTF_TTCN3Coverage_usedModules&"\"' > cov2ttcn3.cfg;"&
    "rm compile;"&
    "echo 'Used modules updated to: \""&tsp_EPTF_TTCN3Coverage_usedModules&"\"';"&
    "echo \\`Rebuild_required_for_the_new_settings_to_take_effect\"'\"!";
  f_EPTF_TTCN3Coverage_system(vl_command);
  setverdict(pass);
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_TTCN3Coverage_detailed
// 
//  Purpose:
//    Generate more detailed coverage report
//
//  Parameters:
//
//  Return Value:
//    -
//
///////////////////////////////////////////////////////////
external function f_EPTF_TTCN3Coverage_detailed();

type component TTCN3Coverage_CT {
}

///////////////////////////////////////////////////////////
//  TestCase: tc_EPTF_TTCN3Coverage_detailed
// 
//  Purpose:
//    Generate more detailed coverage report
//
//  Parameters:
//
//  Return Value:
//    -
//
//  Detailed Comments:
//    This test case is optimized for mctr_gui
//
///////////////////////////////////////////////////////////
testcase tc_EPTF_TTCN3Coverage_detailed() runs on TTCN3Coverage_CT {
  //f_EPTF_TTCN3Coverage_system("./ttcn3cov_gen");
  //f_EPTF_TTCN3Coverage_system("./ttcn3cov *.cov | tee TTCN3Coverage.log | grep \"overage\"");
  f_EPTF_TTCN3Coverage_detailed();
  setverdict(pass);
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_TTCN3Coverage_reset
// 
//  Purpose:
//    Reset coverage info
//
//  Parameters:
//
//  Return Value:
//    -
//
///////////////////////////////////////////////////////////
external function f_EPTF_TTCN3Coverage_reset();

///////////////////////////////////////////////////////////
//  TestCase: tc_EPTF_TTCN3Coverage_reset
// 
//  Purpose:
//    Resets coverage info
//
//  Parameters:
//
//  Return Value:
//    -
//
///////////////////////////////////////////////////////////
testcase tc_EPTF_TTCN3Coverage_reset() runs on TTCN3Coverage_CT {
  //f_EPTF_TTCN3Coverage_system("./ttcn3cov_cleanAll");
  //f_EPTF_TTCN3Coverage_system("rm *.cov TTCN3Coverage.log");
  f_EPTF_TTCN3Coverage_reset();
  setverdict(pass);
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_TTCN3Coverage_unusedFns
// 
//  Purpose:
//    Prints the list of unused functions in the used modules
//
//  Parameters:
//
//  Return Value:
//    -
//
///////////////////////////////////////////////////////////
external function f_EPTF_TTCN3Coverage_unusedFns();

///////////////////////////////////////////////////////////
//  TestCase: tc_EPTF_TTCN3Coverage_unusedFns
// 
//  Purpose:
//    Prints the list of unused functions in the used modules
//
//  Parameters:
//
//  Return Value:
//    -
//
//  Detailed Comments:
//    This test case is optimized for mctr_gui
//    Note, that the generated list can be very long!
//
///////////////////////////////////////////////////////////
testcase tc_EPTF_TTCN3Coverage_unusedFns() runs on TTCN3Coverage_CT {
  //f_EPTF_TTCN3Coverage_system("./ttcn3cov_cleanAll");
  //f_EPTF_TTCN3Coverage_system("rm *.cov TTCN3Coverage.log");
  f_EPTF_TTCN3Coverage_unusedFns();
  setverdict(pass);
}

external function f_EPTF_TTCN3Coverage_printStat(in EPTF_TTCN3CoverageDb pl_coverageDb, in integer pl_codeLines) return charstring;

// Prints the statistics for a given module to a string
// If this function is changed, the code generated by titan should be copy-pasted to ttcn3cov.cc. 
//The string constants should be replaced by #define-s!
/*function f_EPTF_TTCN3Coverage_printStat(in EPTF_TTCN3CoverageDb pl_coverageDb, in integer pl_codeLines) return charstring {
  var charstring vl_coverageStat := "";
  var float vl_usage;
  var integer vl_totalCodeLines := 0;
  var integer vl_totalUsedLines := 0;
  var integer vl_totalSourceLines := 0;
  for(var integer i:=0; i<sizeof(pl_coverageDb);i:=i+1) {
//     if (sizeof(pl_coverageDb[i].lines)==0) {
//       continue;
//     }
    var integer vl_usedLines := 0;
    var charstring vl_filename := pl_coverageDb[i].filename;
    for(var integer l:=0; l<sizeof(pl_coverageDb[i].lines);l:=l+1) {
      var integer vl_cov := pl_coverageDb[i].lines[l];
      if (vl_cov==0) {
        continue; // do not generate output for uncovered lines
      }
      vl_coverageStat := vl_coverageStat
        & int2str(vl_cov) & ":\t"&vl_filename&":"&int2str(l) & "\n";
        
      if (vl_cov>0) {
        vl_usedLines := vl_usedLines + 1;
      }
    }
    var integer vl_codeLines := vl_usedLines; // needed for ttcnin support (will have 100% coverage) FIXME: set code lines for them correctly in the generated cc file
    if(isbound(pl_coverageDb[i].codeLines)) {
      vl_codeLines:=pl_coverageDb[i].codeLines;
    }
    if (vl_codeLines<1) {
      vl_usage := 0.0;
    } else {
      vl_usage := int2float(vl_usedLines)/int2float(vl_codeLines);
    }
    vl_coverageStat := vl_coverageStat &"Total lines    in file "&vl_filename&": "&int2str(sizeof(pl_coverageDb[i].lines));
    vl_coverageStat := vl_coverageStat &"\nCode lines     in file "&vl_filename&": "&int2str(vl_codeLines);
    vl_coverageStat := vl_coverageStat &"\nExecuted lines in file "&vl_filename&": "&int2str(vl_usedLines);
    vl_coverageStat := vl_coverageStat &"\nCode coverage  in file "&vl_filename&": "&float2str(vl_usage)&"\n";
    
    vl_totalSourceLines := vl_totalSourceLines + sizeof(pl_coverageDb[i].lines);
    vl_totalUsedLines := vl_totalUsedLines + vl_usedLines;
    vl_totalCodeLines := vl_totalCodeLines + vl_codeLines;
  }

  if (vl_totalCodeLines==0) {
    vl_usage := 0.0;
  } else {
    vl_usage := int2float(vl_totalUsedLines)/int2float(vl_totalCodeLines);
  }

  vl_coverageStat := vl_coverageStat &"Total source lines   : "&int2str(vl_totalSourceLines);
  vl_coverageStat := vl_coverageStat &"\nTotal code lines     : "&int2str(vl_totalCodeLines);
  vl_coverageStat := vl_coverageStat &"\nTotal executed lines : "&int2str(vl_totalUsedLines);
  vl_coverageStat := vl_coverageStat &"\nOverall code coverage: "&float2str(vl_usage)&"\n";
  
  return vl_coverageStat;
};*/

} // end of module
