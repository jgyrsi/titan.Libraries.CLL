#!/bin/bash
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////
echo "make_ttcn3cov version 1.0"
echo "Makefile patcher and make utility for TTCN3Coverage"

if [ "$MAKE_CMD" == "" ]
then
#store the original make command:
MAKE_CMD="$(cd ..; tcsh -c '/usr/bin/which make')"
fi

if [ "$MAKE_CMD" == "" ]
then
#store the original make command:
MAKE_CMD=$(cd ..; which make)
fi

echo "make_ttcn3cov: Patching makefile..."
# changing only the compiler:
cat Makefile | sed 's/\$(TTCN3_DIR)\/bin\/compiler/compiler/g' > makefile_ttcn3cov_

difference="different"
if [ -r makefile_ttcn3cov ]
then
  difference="$(diff makefile_ttcn3cov_ makefile_ttcn3cov)"
fi
#echo ${difference}
if [ "${difference}" ]
then
  mv makefile_ttcn3cov_ makefile_ttcn3cov
  echo "make_ttcn3cov: Makefile successfully patched."
else
  echo "make_ttcn3cov: Makefile is up-to-date."
  rm makefile_ttcn3cov_
fi

if [ "$*" == "clean" ]
then
  ${MAKE_CMD} -f makefile_ttcn3cov $*
  $(dirname $0)/ttcn3cov_cleanAll
  if [ -r $(dirname $0)/ttcn3prof_cleanAll ]
  then
    $(dirname $0)/ttcn3prof_cleanAll
  fi
  rm makefile_ttcn3cov
  exit
fi

#original make is started by exec (i.e. with no return)
#needed to be able stop make by the RedX button on the mctr_gui
echo "make_ttcn3cov: Invoking "${MAKE_CMD}
exec ${MAKE_CMD} -f makefile_ttcn3cov $*
