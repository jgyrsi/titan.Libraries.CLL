///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_DSHelp_Functions
//
// Purpose:
//   This module contains functions for testing EPTF DsRestAPI.
//
// Module Parameters:
//    -
//  Module depends on:
//    <EPTF_DSHelp_Definitions>
//    <EPTF_CLL_DsRestAPI_Definitions>
//    <EPTF_CLL_DsRestAPI_Functions>
//    <EPTF_CLL_Base_Functions>
//    <EPTF_CLL_Common_Definitions>
//
// Current Owner:
//    Tamas Kis (ekistam)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////
module EPTF_DSHelp_Functions {
  friend module EPTF_DSHelp_Testcases;

  //=========================================================================
  // Import Part
  //=========================================================================
  import from EPTF_CLL_Logging_Functions all;
  import from EPTF_DSHelp_Definitions all;
  import from EPTF_CLL_DsRestAPI_Functions all;
  import from EPTF_CLL_Base_Functions all;
  import from EPTF_CLL_ExecCtrl_Definitions all;
  import from EPTF_CLL_LGenBase_Functions all;
  import from EPTF_CLL_LGenBase_ConfigFunctions all;
  import from EPTF_CLL_ExecCtrlClient_Functions all;
  import from EPTF_CLL_HostAdminServer_Definitions all;
  import from EPTF_CLL_HostAdminServer_Functions all;
  import from EPTF_CLL_LoggingServer_Functions all;
  import from EPTF_CLL_LoadRegulator_Functions all;
  import from EPTF_CLL_StatManager_Functions all;
  import from EPTF_CLL_UIHandler_WidgetFunctions all;
  import from EPTF_CLL_Base_Functions all;
  import from EPTF_CLL_HostAdmin_Functions all;
  import from EPTF_CLL_HostAdminServer_Definitions all;
  import from EPTF_CLL_ExecCtrl_Functions all;
  import from TCCFileIO_Functions all;

  //=========================================================================
  // Functions
  //=========================================================================
  public function f_EPTF_DSHelp_init_CT(in charstring pl_selfName) runs on DSHelp_CT {
    f_EPTF_Base_init_CT(pl_selfName);
    f_EPTF_Base_registerCleanup(refers(f_EPTF_DSHelp_cleanup_CT));
    f_EPTF_DsRestAPI_init_CT(pl_selfName);
    f_EPTF_LoggingServer_init_CT(pl_selfName, self);
    f_EPTF_Logging_init_CT(pl_selfName);
    f_EPTF_LoadRegulator_init_CT(pl_selfName, refers(f_EPTF_DSHelp_getSUTLoad), pl_dataSource_compRef := self);
    f_EPTF_StatManager_init_CT(pl_selfName);
    f_EPTF_UIHandler_init_CT(pl_selfName);
      
    var EPTF_HostAdminServer_CT vl_hostAdminServer_compRef := EPTF_HostAdminServer_CT.create;
    vl_hostAdminServer_compRef.start(f_EPTF_DSHelp_HostAdminServerBehaviour());
    f_EPTF_HostAdmin_init_CT(f_EPTF_Base_selfName(), pl_dataSource_compRef := self/*, pl_server_Ref := v_hostAdminServer_compRef*/);
    
    f_EPTF_ExecCtrl_setManualControl(true);
    
    f_EPTF_ExecCtrl_loadConfig(
        pl_EPTF_LGenBase_ScenarioDeclaratorList := {},
        pl_EPTF_LGenBase_tcTypeDeclaratorList := {},
        pl_EPTF_LGenBase_TcMgmt_ScenarioDeclaratorList := {},
        pl_EPTF_LGenBase_TcMgmt_WeightedScenarioDeclaratorList:= {},
        pl_EPTF_ExecCtrl_TimeProfileDescrList:={},
        pl_EPTF_ExecCtrl_TimeProfileList:={},
        pl_EPTF_ExecCtrl_TimeProfile2TcList:={},
        pl_EPTF_ExecCtrl_ScenarioInstanceTypeList:={},
        pl_EPTF_ExecCtrl_LGenFunction_Entry_List:={{
            name := omit,
            fn := refers(f_EPTF_DSHelp_createDefaultLGen)
          }},
        pl_EPTF_ExecCtrl_PhaseList_Declarators:={},
        pl_EPTF_ExecCtrl_ScenarioGroup_Declarators:={},
        pl_EPTF_ExecCtrl_RegulatorNames:={"LimitMaxRegulator"}
      );
  
    f_EPTF_ExecCtrl_init_CT(f_EPTF_Base_selfName(), 0, pl_dataSource_compRef := self);
    
  }
  
  private function f_EPTF_DSHelp_cleanup_CT() runs on DSHelp_CT {
  }
  
  private function f_EPTF_DSHelp_getSUTLoad() runs on DSHelp_CT return float
  {
    return 11.0;
  }
  
  private function f_EPTF_DSHelp_defaultLGenBehaviour(in charstring pl_name, in EPTF_ExecCtrl_CT pl_execCtrlCompRef) runs on EPTF_ExecCtrlClient_CT {
    f_EPTF_LGenBase_init(pl_name);
    var integer vl_Ret := f_EPTF_LGenBase_declareBehaviorType(cg_EPTF_DSHelp_defaultBehaviorName, -1, null, null, null);
    vl_Ret := f_EPTF_LGenBase_declareEntityType(cg_EPTF_DSHelp_defaultETypeName, {cg_EPTF_DSHelp_defaultBehaviorName});
    vl_Ret := f_EPTF_LGenBase_declareFSMTable(cg_EPTF_DSHelp_defaultFSM);
    f_EPTF_ExecCtrlClient_init_CT(pl_name, pl_execCtrlCompRef);
    f_EPTF_Base_wait4Shutdown();
  }
  
  private function f_EPTF_DSHelp_createDefaultLGen(in charstring pl_hostname, in charstring pl_componentName) runs on EPTF_ExecCtrl_CT return EPTF_ExecCtrlClient_CT {
    pl_hostname := pl_componentName;
    var EPTF_ExecCtrlClient_CT vl_lgen := EPTF_ExecCtrlClient_CT.create;
    vl_lgen.start(f_EPTF_DSHelp_defaultLGenBehaviour("ExecCtrl_DefaultLGen_"&log2str(vl_lgen),self) );
    return vl_lgen;
  }

  private function f_EPTF_DSHelp_HostAdminServerBehaviour() runs on EPTF_HostAdminServer_CT {
    f_EPTF_HostAdminServer_init_CT("MTC", mtc);
    f_EPTF_Base_wait4Shutdown();
  }
  
  friend function f_EPTF_DSHelp_HTTPServer_saveFile(in charstring pl_fileName, in charstring pl_data) return integer {
    var integer vl_retval:=-1;
    if(pl_fileName != "") {
      var integer vl_file := f_FIO_open_trunc_wronly(pl_fileName);
      if(vl_file!=-1) {
          vl_retval := f_FIO_write_text(vl_file, pl_data);
        if (vl_retval != -1) {
          vl_retval := f_FIO_close(vl_file);
        }
      }
    }
    return vl_retval;
  }
} //module
