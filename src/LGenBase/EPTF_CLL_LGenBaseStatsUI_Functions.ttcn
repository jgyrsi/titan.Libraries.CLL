///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_LGenBaseStatsUI_Functions
// 
//  Purpose:
//    This module provides functions for TitanSim load generator statistics display
// 
//  Module Parameters:
//    -
// 
//  Module depends on:
//    <EPTF_CLL_Common_Definitions>
//    <EPTF_CLL_Base_Functions>
//    <EPTF_CLL_LGenBase_Definitions>
//    <EPTF_CLL_LGenBase_Functions>
//    <EPTF_CLL_LGenBase_ConfigFunctions>
//    <EPTF_CLL_LGenBase_TrafficFunctions>
//    <EPTF_CLL_LGenBaseStatsUI_Definitions>
//    <EPTF_CLL_LGenBaseStats_Definitions>
//    <EPTF_CLL_LGenBaseStats_Functions>
//    <EPTF_CLL_UIHandlerClient_Functions>
//    <EPTF_CLL_UIHandler_Definitions>
//    <EPTF_CLL_UIHandler_MsgDefinitions>
//    <EPTF_CLL_Variable_Definitions>
//    <EPTF_CLL_Variable_Functions>
//    <XTDP_PDU_Defs>
//
//  Current Owner:    ELSZSKU
// 
//  Last Review Date:
//    2008-02-26
// 
//  Detailed Comments:
// 
///////////////////////////////////////////////////////////
module EPTF_CLL_LGenBaseStatsUI_Functions

// identified_organization(127) ericsson(5) testing(0)
// <put further nodes here if needed>}]
{
//=========================================================================
// Import Part
//=========================================================================
import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_LGenBase_Definitions all;
import from EPTF_CLL_LGenBase_ConfigFunctions all;
import from EPTF_CLL_LGenBase_ConfigDefinitions all;
import from EPTF_CLL_LGenBase_TrafficFunctions all;
import from EPTF_CLL_LGenBaseStatsUI_Definitions all;
import from EPTF_CLL_LGenBaseStats_Definitions all;
import from EPTF_CLL_LGenBaseStats_Functions all;
import from EPTF_CLL_UIHandlerClient_Functions all;
import from EPTF_CLL_UIHandler_Definitions all;
import from EPTF_CLL_UIHandler_MsgDefinitions all;
import from EPTF_CLL_Variable_Definitions all;
import from EPTF_CLL_Variable_Functions all;

///////////////////////////////////////////////////////////
// Module parameter: 
//   tsp_LGenBaseStatsUI_columnDescriptorList
//
// Detailed description:
//    This parameter describes the contents of the traffic case tree
//    on the GUI if the pl_columnDescriptorList parameter of 
//    the function <f_EPTF_LGenBaseStatsUI_init> has no value.
///////////////////////////////////////////////////////////
modulepar EPTF_LGenBaseStatsUI_columnDescriptorList tsp_LGenBaseStatsUI_columnDescriptorList := {
  {"Tc", c_EPTF_LGenBaseStatsUI_tcColTcName, false},
  {"En.", c_EPTF_LGenBaseStatsUI_tcColEnabled, false},
  {"State", c_EPTF_LGenBaseStatsUI_tcColStateName, false},
  {"Info", c_EPTF_LGenBaseStatsUI_tcColUserData, false},
  {"Start", c_EPTF_LGenBaseStatsUI_tcColStartBtn, true},
  {"Stop", c_EPTF_LGenBaseStatsUI_tcColStopBtn, true},
  {"Pause", c_EPTF_LGenBaseStatsUI_tcColPauseBtn, true},
  {"Abort", c_EPTF_LGenBaseStatsUI_tcColAbortBtn, true},
  {"Single shot", c_EPTF_LGenBaseStatsUI_tcColSingleShotBtn, true},
  {"Traffic type", c_EPTF_LGenBaseStatsUI_tcColTrafficType, false},
  {"CPS", c_EPTF_LGenBaseStatsUI_tcColCpsToReach, true},
  {"Weight", c_EPTF_LGenBaseStatsUI_tcColWeight, true},
  {"Last CPS", c_EPTF_LGenBaseStatsUI_tcColLastCps, false},
  {"Running", c_EPTF_LGenBaseStatsUI_tcColNrOfRunningEntities, false},
  {"Available", c_EPTF_LGenBaseStatsUI_tcColNrOfAvailableEntities, false},
  {"Not finished", c_EPTF_LGenBaseStatsUI_tcColNrOfNotFinishedEntities, false},
  {"Starts",  c_EPTF_LGenBaseStatsUI_tcColNrOfStarts, false},
  {"Success",  c_EPTF_LGenBaseStatsUI_tcColNrOfSuccesses, false},
  {"Fail",  c_EPTF_LGenBaseStatsUI_tcColNrOfFails, false},
  {"Erroneous",  c_EPTF_LGenBaseStatsUI_tcColNrOfErrors, false},
  {"Timeout",  c_EPTF_LGenBaseStatsUI_tcColNrOfTimeouts, false},
  {"Max.running",  c_EPTF_LGenBaseStatsUI_tcColMaxRunning, false},
  {"Min.available",  c_EPTF_LGenBaseStatsUI_tcColMinAvailable, false},
  {"Max.busy",  c_EPTF_LGenBaseStatsUI_tcColMaxBusy, false},
  {"Fin.traffic",  c_EPTF_LGenBaseStatsUI_tcColReceivedAnswers, false},
  {"Range loops",  c_EPTF_LGenBaseStatsUI_tcColRangeLoops, false}
}

///////////////////////////////////////////////////////////
// Module parameter: 
//   tsp_LGenBaseStatsUI_columnDescriptorList
//
// Detailed description:
//    This parameter describes the contents of the scenario panel
//    on the GUI if the pl_scColumnDescriptorList parameter of 
//    the function <f_EPTF_LGenBaseStatsUI_init> has no value.
///////////////////////////////////////////////////////////
modulepar EPTF_LGenBaseStatsUI_scColumnDescriptorList tsp_LGenBaseStatsUI_scColumnDescriptorList := {
  {"Scenario", c_EPTF_LGenBaseStatsUI_scColName, false},
  {"En.", c_EPTF_LGenBaseStatsUI_scColEnabled, true},
  {"State", c_EPTF_LGenBaseStatsUI_scColStateName, false},
  {"Start", c_EPTF_LGenBaseStatsUI_scColStartBtn, true},
  {"Stop", c_EPTF_LGenBaseStatsUI_scColStopBtn, true},
  {"Pause", c_EPTF_LGenBaseStatsUI_scColPauseBtn, true},
  {"Abort", c_EPTF_LGenBaseStatsUI_scColAbortBtn, true},
  {"Restore", c_EPTF_LGenBaseStatsUI_scColRestoreBtn, true},
  {"Single shot", c_EPTF_LGenBaseStatsUI_scColSingleShotBtn, true},
  {"CPS", c_EPTF_LGenBaseStatsUI_scColCPS, true},
  {"Traffic type", c_EPTF_LGenBaseStatsUI_scColTrafficType, false},
  {"Lock CPS", c_EPTF_LGenBaseStatsUI_scColLockCPS, true}
}

group public_functions{
  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_LGenBaseStatsUI_init
  // 
  //  Purpose:
  //    Inits EPTF_LGenBaseStatsUI feature
  //
  //  Parameters:
  //    - pl_selfName - *in charstring* - the name of the component
  //    - entityNamePrefix - *in charstring* -  the name prefix for the entities in the LGenBase
  //    - pl_DefaultUIHandler - *in EPTF_UIHandler_CT* - the UIHandler component to communicate with
  //    - pl_scColumnDescriptorList - *in* <EPTF_LGenBaseStatsUI_scColumnDescriptorList> - descriptor of the scenario tree
  //    - pl_columnDescriptorList - *in* <EPTF_LGenBaseStatsUI_tcColumnDescriptorList> - descriptor of the traffic case tree
  ///////////////////////////////////////////////////////////
  public function f_EPTF_LGenBaseStatsUI_init(
    in charstring pl_selfName,
    in charstring pl_entityNamePrefix,
    in EPTF_UIHandler_CT pl_DefaultUIHandler,
    in EPTF_LGenBaseStatsUI_scColumnDescriptorList pl_scColumnDescriptorList := tsp_LGenBaseStatsUI_scColumnDescriptorList,
    in EPTF_LGenBaseStatsUI_columnDescriptorList pl_columnDescriptorList := tsp_LGenBaseStatsUI_columnDescriptorList
  )
  runs on EPTF_LGenBaseStatsUI_Private_CT{
    if (v_LGenBaseStatsUI_initialized) {return}
    f_EPTF_Base_assert(%definitionId&": The default UIHandler component must not be null.",null != pl_DefaultUIHandler);
    f_EPTF_LGenBaseStats_init(pl_selfName, pl_entityNamePrefix);
    f_EPTF_UIHandlerClient_init_CT(pl_selfName, pl_DefaultUIHandler)
    v_LGenBaseStatsUI_tcColumnDescriptorList := pl_columnDescriptorList;
    v_LGenBaseStatsUI_scColumnDescriptorList := pl_scColumnDescriptorList;
    var integer vl_createdWidgetsCountM := sizeof(c_EPTF_LGenBaseStatsUI_tcVarDescriptor)-1;
    v_LGenBaseStatsUI_createdTcWidgets[vl_createdWidgetsCountM ] := false;
    for ( var integer vl_i := 0; vl_i < vl_createdWidgetsCountM ; vl_i := vl_i+1 )
    {
      v_LGenBaseStatsUI_createdTcWidgets[vl_i] := false;
      v_LGenBaseStatsUI_enabledTcWidgets[vl_i] := false;
    }
    var integer vl_colCount := sizeof(v_LGenBaseStatsUI_tcColumnDescriptorList);
    for ( var integer vl_col := 0; vl_col < vl_colCount ; vl_col := vl_col+1 )
    {
      v_LGenBaseStatsUI_createdTcWidgets[v_LGenBaseStatsUI_tcColumnDescriptorList[vl_col].tcColId] := true;
      v_LGenBaseStatsUI_enabledTcWidgets[v_LGenBaseStatsUI_tcColumnDescriptorList[vl_col].tcColId] := v_LGenBaseStatsUI_tcColumnDescriptorList[vl_col].enableIfAvailable;
    }
    for ( var integer vl_i := 0; vl_i < c_EPTF_LGenBaseStatsUI_scLastColId + 1 ; vl_i := vl_i+1 )
    {
      v_LGenBaseStatsUI_createdScWidgets[vl_i] := false;
      v_LGenBaseStatsUI_enabledScWidgets[vl_i] := false;
    }
    for ( var integer vl_i := 0; vl_i < sizeof(v_LGenBaseStatsUI_scColumnDescriptorList) ; vl_i := vl_i+1 )
    {
      v_LGenBaseStatsUI_createdScWidgets[v_LGenBaseStatsUI_scColumnDescriptorList[vl_i].scColId] := true;
      v_LGenBaseStatsUI_enabledScWidgets[v_LGenBaseStatsUI_scColumnDescriptorList[vl_i].scColId] := v_LGenBaseStatsUI_scColumnDescriptorList[vl_i].enableIfAvailable;
    }
    v_LGenBaseStatsUI_subGUIPrefix := f_EPTF_Base_selfName()&tsp_LGenBase_nameSeparator&c_LGenBaseStatsUI_subGUIPrefix;
    v_LGenBaseStatsUI_initialized := true;
  }
  
  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_LGenBaseStatsUI_prepareGUI
  // 
  //  Purpose:
  //    Creates the statistical variables, adds the widgets to the GUI,
  //    and connects them to the widgets
  //
  //  Parameters:
  //    - pl_parentWidgetId - *in charstring* - the name of the parent widget
  //
  //  Detailed Comments:
  //    The function must be called after creating all the scenarios
  ///////////////////////////////////////////////////////////
  public function f_EPTF_LGenBaseStatsUI_prepareGUI(
    in EPTF_UIHandler_WidgetIdString pl_parentWidgetId
  )
  runs on EPTF_LGenBaseStatsUI_Private_CT{
    f_EPTF_LGenBaseStats_createVars();
    f_EPTF_LGenBaseStatsUI_joinWidgets();
  }
}

public function f_EPTF_LGenBaseStatsUI_scenarioPrefix(
  in charstring pl_eGrpName,
  in charstring pl_scName
)
runs on EPTF_LGenBaseStatsUI_Private_CT
return charstring{
  return f_EPTF_Base_selfName()&c_EPTF_LGenBaseStatsUI_widgetIdSeparator&pl_eGrpName&c_EPTF_LGenBaseStatsUI_widgetIdSeparator&pl_scName&c_EPTF_LGenBaseStatsUI_widgetIdSeparator;
}

private function f_EPTF_LGenBaseStatsUI_treeName(
  in charstring pl_eGrpName,
  in charstring pl_scName
)
runs on EPTF_LGenBaseStatsUI_Private_CT
return charstring{
  return f_EPTF_LGenBaseStatsUI_scenarioPrefix(pl_eGrpName, pl_scName)&"tree";
}

private function f_EPTF_LGenBaseStatsUI_cellNamePrefix(
  in charstring pl_eGrpName,
  in charstring pl_scName,
  in charstring pl_tcName
)
runs on EPTF_LGenBaseStatsUI_Private_CT
return charstring{
  return f_EPTF_LGenBaseStatsUI_scenarioPrefix(pl_eGrpName, pl_scName)&pl_tcName&c_EPTF_LGenBaseStatsUI_widgetIdSeparator;
}

private function f_EPTF_LGenBaseStatsUI_joinWidgets()
runs on EPTF_LGenBaseStatsUI_Private_CT {
  for ( var integer vl_eGrpIdx := 0; vl_eGrpIdx < f_EPTF_LGenBase_getEGrpCount() ; vl_eGrpIdx := vl_eGrpIdx+1 )
  {
    f_EPTF_LGenBaseStatsUI_joinEGroupWidgets(vl_eGrpIdx);
  }
  f_EPTF_LGenBase_registerTcStateChangedCallback(refers(f_EPTF_LGenBaseStatsUI_tcStateChanged));
  f_EPTF_LGenBase_registerScenarioStateChangedCallback(refers(f_EPTF_LGenBaseStatsUI_scenarioStateChanged))
  f_EPTF_LGenBaseStatsUI_initBtnStates();
}


private function f_EPTF_LGenBaseStatsUI_initBtnStates()
runs on EPTF_LGenBaseStatsUI_Private_CT {
  for ( var integer vl_eGrp := 0; vl_eGrp < f_EPTF_LGenBase_getEGrpCount() ; vl_eGrp := vl_eGrp+1 )
  {
    for ( var integer vl_sc := 0; vl_sc < f_EPTF_LGenBase_getScCountOfEGrp(vl_eGrp) ; vl_sc := vl_sc+1 )
    {
      f_EPTF_LGenBaseStatsUI_scenarioStateChanged(vl_eGrp, vl_sc);
      for ( var integer vl_tc := 0; vl_tc < f_EPTF_LGenBase_getTcCountOfSc(vl_eGrp, vl_sc) ; vl_tc := vl_tc+1 )
      {
        f_EPTF_LGenBaseStatsUI_tcStateChanged(f_EPTF_LGenBase_trafficCaseAbsIdx(vl_eGrp, vl_sc, vl_tc));
      }
    }
  }
}
private function f_EPTF_LGenBaseStatsUI_joinEGroupWidgets(
  in integer pl_eGrpIdx
)
runs on EPTF_LGenBaseStatsUI_Private_CT {
  var charstring vl_eGrpName := f_EPTF_LGenBaseStats_eGrpName(pl_eGrpIdx);
  //var charstring vl_prefix := pl_prefix&vl_eGrpName&c_EPTF_LGenBase_widgetIdSeparator;
  for ( var integer vl_sc := 0; vl_sc < f_EPTF_LGenBase_getScCountOfEGrp(pl_eGrpIdx) ; vl_sc := vl_sc+1 )
  {
    f_EPTF_LGenBaseStatsUI_joinScenarioWidgets(pl_eGrpIdx, vl_sc, vl_eGrpName);
  }
}

private function f_EPTF_LGenBaseStatsUI_getScVarNamePrefix(
  in charstring pl_eGrpName,
  in charstring pl_scName
) runs on EPTF_LGenBaseStats_CT return charstring {
  return pl_eGrpName&tsp_LGenBase_nameSeparator&pl_scName&tsp_LGenBase_nameSeparator;
}

private function f_EPTF_LGenBaseStatsUI_joinScenarioWidgets(
  in integer pl_eGrpIdx,
  in integer pl_scIdx,
  in charstring pl_eGrpName
)
runs on EPTF_LGenBaseStatsUI_Private_CT {
  var integer vl_tcCount := f_EPTF_LGenBase_getTcCountOfSc(pl_eGrpIdx, pl_scIdx)
  var charstring vl_scName := f_EPTF_LGenBaseStats_scenarioName(pl_eGrpIdx, pl_scIdx);
  //var charstring vl_tcVarName := f_EPTF_LGenBaseStats_getScNamePrefix(pl_eGrpName,vl_scName)&c_EPTF_LGenBaseStats_nameOfTcRunningTcsInSc;
  //var integer vl_varIdx := f_EPTF_Var_getId(vl_tcVarName);
  //f_EPTF_Base_assert(%definitionId&": Invalid variable index for scenario "&vl_scName&" of entity group "&pl_eGrpName,-1 != vl_varIdx);
  //f_EPTF_Var_addPostProcFn(vl_varIdx, {refers(f_EPTF_LGenBaseStatsUI_tcStartedOrStopped),{pl_eGrpIdx, pl_scIdx}});
  var boolean vl_weighted := f_EPTF_LGenBase_isWeightedScenario(pl_eGrpIdx, pl_scIdx);
  for ( var integer vl_tc := 0; vl_tc < vl_tcCount ; vl_tc := vl_tc+1 )
  {
    var integer vl_tcAbsIdx := f_EPTF_LGenBase_trafficCaseAbsIdx(pl_eGrpIdx, pl_scIdx, vl_tc)
    var charstring vl_tcName := f_EPTF_LGenBase_getTcNameByTcIdx(vl_tcAbsIdx);//f_EPTF_LGenBase_getTcUniqueNameByTcIdx(vl_tcAbsIdx)
    var charstring vl_varNamePrefix := f_EPTF_LGenBaseStats_getNamePrefix(pl_eGrpName,vl_scName,vl_tcName);
    var charstring vl_widgetPrefix := f_EPTF_LGenBaseStatsUI_cellNamePrefix(pl_eGrpName,vl_scName,vl_tcName);
    //var charstring vl_widgetPrefix := f_EPTF_LGenBaseStatsUI_widgetPrefix(pl_prefix, pl_eGrpName, pl_scName);
    //var integer vl_colCounter := 0;
    for ( var integer vl_col := 0; vl_col < sizeof(v_LGenBaseStatsUI_tcColumnDescriptorList) ; vl_col := vl_col+1 )
    {
      if( (vl_weighted and match(c_EPTF_LGenBaseStatsUI_tcVarDescriptor[v_LGenBaseStatsUI_tcColumnDescriptorList[vl_col].tcColId].displayOn, EPTF_LGenBaseStatsUI_displayOnScTypeWeighted))or
        (not vl_weighted and match(c_EPTF_LGenBaseStatsUI_tcVarDescriptor[v_LGenBaseStatsUI_tcColumnDescriptorList[vl_col].tcColId].displayOn, EPTF_LGenBaseStatsUI_displayOnScTypeCps)))
      {
        select( v_LGenBaseStatsUI_tcColumnDescriptorList[vl_col].tcColId )
        {
          case ( c_EPTF_LGenBaseStatsUI_tcColStartBtn )
          {
            var charstring vl_startTcVarName := vl_varNamePrefix&c_EPTF_LGenBaseStatsUI_nameOfStartBtn;
            f_EPTF_UIHandlerClient_subscribeMe(vl_startTcVarName, v_LGenBaseStatsUI_subGUIPrefix&vl_startTcVarName, 
              vl_widgetPrefix&c_EPTF_LGenBaseStatsUI_nameOfStartBtn,
              realtime); 
          }
          case ( c_EPTF_LGenBaseStatsUI_tcColStopBtn )
          {
            var charstring vl_stopTcVarName := vl_varNamePrefix&c_EPTF_LGenBaseStatsUI_nameOfStopBtn;
            f_EPTF_UIHandlerClient_subscribeMe(vl_stopTcVarName, v_LGenBaseStatsUI_subGUIPrefix&vl_stopTcVarName, 
              vl_widgetPrefix&c_EPTF_LGenBaseStatsUI_nameOfStopBtn,
              realtime); 
          }
          case ( c_EPTF_LGenBaseStatsUI_tcColAbortBtn )
          {
            var charstring vl_abortTcVarName := vl_varNamePrefix&c_EPTF_LGenBaseStatsUI_nameOfAbortBtn;
            f_EPTF_UIHandlerClient_subscribeMe(vl_abortTcVarName, v_LGenBaseStatsUI_subGUIPrefix&vl_abortTcVarName, 
              vl_widgetPrefix&c_EPTF_LGenBaseStatsUI_nameOfAbortBtn,
              realtime); 
          }
          case ( c_EPTF_LGenBaseStatsUI_tcColPauseBtn )
          {
            var charstring vl_pauseTcVarName := vl_varNamePrefix&c_EPTF_LGenBaseStatsUI_nameOfPauseBtn;
            f_EPTF_UIHandlerClient_subscribeMe(vl_pauseTcVarName, v_LGenBaseStatsUI_subGUIPrefix&vl_pauseTcVarName, 
              vl_widgetPrefix&c_EPTF_LGenBaseStatsUI_nameOfPauseBtn,
              realtime); 
          }
          case ( c_EPTF_LGenBaseStatsUI_tcColSingleShotBtn )
          {
            var charstring vl_varName := vl_varNamePrefix&c_EPTF_LGenBaseStatsUI_nameOfSingleShotBtn;
            f_EPTF_UIHandlerClient_subscribeMe(vl_varName, v_LGenBaseStatsUI_subGUIPrefix&vl_varName, 
              vl_widgetPrefix&c_EPTF_LGenBaseStatsUI_nameOfSingleShotBtn,
              realtime); 
          }
          case ( c_EPTF_LGenBaseStatsUI_tcColCpsToReach )
          {
            var charstring vl_varName := c_EPTF_LGenBaseStatsUI_tcVarDescriptor[v_LGenBaseStatsUI_tcColumnDescriptorList[vl_col].tcColId].varName;
            f_EPTF_UIHandlerClient_subscribeMe(vl_varNamePrefix&vl_varName, v_LGenBaseStatsUI_subGUIPrefix&vl_varNamePrefix&vl_varName,
              //tcPrefix&c_EPTF_LGenBaseStatsUI_tcVarNames[v_LGenBaseStatsUI_tcColumnDescriptorList[vl_col].tcColId],
              vl_widgetPrefix&vl_varName,
              realtime); 
          }
          case ( c_EPTF_LGenBaseStatsUI_tcColWeight )
          {
            var charstring vl_varName := c_EPTF_LGenBaseStatsUI_tcVarDescriptor[v_LGenBaseStatsUI_tcColumnDescriptorList[vl_col].tcColId].varName;
            f_EPTF_UIHandlerClient_subscribeMe(vl_varNamePrefix&vl_varName, v_LGenBaseStatsUI_subGUIPrefix&vl_varNamePrefix&vl_varName,
              //tcPrefix&c_EPTF_LGenBaseStatsUI_tcVarNames[v_LGenBaseStatsUI_tcColumnDescriptorList[vl_col].tcColId],
              vl_widgetPrefix&vl_varName,
              realtime); 
          }
          case else
          {
            var charstring vl_varName := c_EPTF_LGenBaseStatsUI_tcVarDescriptor[v_LGenBaseStatsUI_tcColumnDescriptorList[vl_col].tcColId].varName;
            f_EPTF_UIHandlerClient_subscribeMe(vl_varNamePrefix&vl_varName, v_LGenBaseStatsUI_subGUIPrefix&vl_varNamePrefix&vl_varName,
              //tcPrefix&c_EPTF_LGenBaseStatsUI_tcVarNames[v_LGenBaseStatsUI_tcColumnDescriptorList[vl_col].tcColId],
              vl_widgetPrefix&vl_varName,
              sampledAtSync)
          }
        }
        //vl_colCounter := vl_colCounter + 1;
      }
    }
  }
  //vl_colCounter := 0;
  var charstring vl_scVarPrefix := f_EPTF_LGenBaseStatsUI_getScVarNamePrefix(pl_eGrpName, vl_scName);
  var charstring vl_scWidgetPrefix := f_EPTF_LGenBaseStatsUI_scenarioPrefix(pl_eGrpName, vl_scName);
  for ( var integer vl_col := 0; vl_col < sizeof(v_LGenBaseStatsUI_scColumnDescriptorList) ; vl_col := vl_col+1 )
  {
    if( (vl_weighted and match(c_EPTF_LGenBaseStatsUI_scVarDescriptor[v_LGenBaseStatsUI_scColumnDescriptorList[vl_col].scColId].displayOn, EPTF_LGenBaseStatsUI_displayOnScTypeWeighted))or
      (not vl_weighted and match(c_EPTF_LGenBaseStatsUI_scVarDescriptor[v_LGenBaseStatsUI_scColumnDescriptorList[vl_col].scColId].displayOn, EPTF_LGenBaseStatsUI_displayOnScTypeCps))){
      var charstring vl_varName := vl_scVarPrefix&c_EPTF_LGenBaseStatsUI_scVarDescriptor[v_LGenBaseStatsUI_scColumnDescriptorList[vl_col].scColId].varName;
      var charstring vl_widgetName := vl_scWidgetPrefix&c_EPTF_LGenBaseStatsUI_scVarDescriptor[v_LGenBaseStatsUI_scColumnDescriptorList[vl_col].scColId].varName;
      select( v_LGenBaseStatsUI_scColumnDescriptorList[vl_col].scColId)
      {
        case ( c_EPTF_LGenBaseStatsUI_scColStartBtn )
        {
          //Start
          f_EPTF_UIHandlerClient_subscribeMe(vl_varName, v_LGenBaseStatsUI_subGUIPrefix&vl_varName,
            vl_widgetName,
            realtime);
        }
        case ( c_EPTF_LGenBaseStatsUI_scColStopBtn )
        {
          //Stop 
          f_EPTF_UIHandlerClient_subscribeMe(vl_varName, v_LGenBaseStatsUI_subGUIPrefix&vl_varName,
            vl_widgetName,
            realtime);
        }
        case ( c_EPTF_LGenBaseStatsUI_scColPauseBtn )
        {
          //Pause 
          f_EPTF_UIHandlerClient_subscribeMe(vl_varName, v_LGenBaseStatsUI_subGUIPrefix&vl_varName,
            vl_widgetName,
            realtime);
        }
        case ( c_EPTF_LGenBaseStatsUI_scColAbortBtn )
        {
          //Abort 
          f_EPTF_UIHandlerClient_subscribeMe(vl_varName, v_LGenBaseStatsUI_subGUIPrefix&vl_varName,
            vl_widgetName,
            realtime);
        }
        case ( c_EPTF_LGenBaseStatsUI_scColRestoreBtn )
        {
          //Restore 
          f_EPTF_UIHandlerClient_subscribeMe(vl_varName, v_LGenBaseStatsUI_subGUIPrefix&vl_varName,
            vl_widgetName,
            realtime);
        }
        case ( c_EPTF_LGenBaseStatsUI_scColSingleShotBtn )
        {
          //SingleShot 
          f_EPTF_UIHandlerClient_subscribeMe(vl_varName, v_LGenBaseStatsUI_subGUIPrefix&vl_varName,
            vl_widgetName,
            realtime);
        }
        case else
        {
          f_EPTF_UIHandlerClient_subscribeMe(vl_varName, v_LGenBaseStatsUI_subGUIPrefix&vl_varName,
            vl_widgetName,
            sampledAtSync)
        }
      }
    }
  }
}

private function f_EPTF_LGenBaseStatsUI_scenarioStateChanged(
  in integer pl_eGrpIdx, 
  in integer pl_scIdx)
runs on EPTF_LGenBaseStatsUI_Private_CT {
  var charstring vl_eGrpName := f_EPTF_LGenBase_getEGrpName(pl_eGrpIdx)
  f_EPTF_Base_assert(%definitionId&": "&"Entity group name "&vl_eGrpName&" contains the separator "&tsp_LGenBase_nameSeparator,f_EPTF_LGenBase_checkName(vl_eGrpName)); 
  var charstring vl_scName := f_EPTF_LGenBase_scenarioName(pl_eGrpIdx, pl_scIdx)
  f_EPTF_Base_assert(%definitionId&": Scenario name "&vl_scName&" contains the separator "&tsp_LGenBase_nameSeparator,f_EPTF_LGenBase_checkName(vl_scName));
  f_EPTF_LGenBaseStatsUI_setScButtonsState(
    f_EPTF_LGenBase_getScState(pl_eGrpIdx, pl_scIdx), 
    f_EPTF_LGenBaseStatsUI_scenarioPrefix(vl_eGrpName,vl_scName),
    f_EPTF_LGenBase_isWeightedScenario(pl_eGrpIdx, pl_scIdx),
    f_EPTF_LGenBase_getScTrafficType(pl_eGrpIdx, pl_scIdx))
}

private function f_EPTF_LGenBaseStatsUI_tcStateChanged(in integer pl_tcIdx)
runs on EPTF_LGenBaseStatsUI_Private_CT {
  var charstring vl_eGrpName;
  var charstring vl_scName;
  var charstring vl_tcName;
  f_EPTF_LGenBase_trafficCaseIdNames(pl_tcIdx, vl_eGrpName, vl_scName, vl_tcName);
  var charstring vl_widgetPrefix := f_EPTF_LGenBaseStatsUI_cellNamePrefix(vl_eGrpName, vl_scName, vl_tcName);//&
  var charstring vl_varPrefix := f_EPTF_LGenBaseStats_getNamePrefix(vl_eGrpName, vl_scName, vl_tcName);
  var integer vl_eGrpIdx := f_EPTF_LGenBase_getEGrpIdxOfTc(pl_tcIdx)
  var integer vl_scIdx := f_EPTF_LGenBase_getScIdxOfTc(pl_tcIdx)
  //"."&int2str(v_LGenBase_trafficCases[pl_tcIdx].eTcRelIdx)&".";
  f_EPTF_LGenBaseStatsUI_setTcButtonsState(
    f_EPTF_LGenBase_getTcState(pl_tcIdx), 
    vl_widgetPrefix, 
    vl_varPrefix,
    f_EPTF_LGenBase_isWeightedScenario(vl_eGrpIdx, vl_scIdx),
    f_EPTF_LGenBase_getTcTrafficType(pl_tcIdx))
}

type enumerated EPTF_LGenBaseStatsUI_tcOrSc {tc, sc}
private function f_EPTF_LGenBaseStatsUI_setTcButtons(
  in EPTF_LGenBaseStatsUI_widgetSetDescriptorList pl_btnDesc,
  in charstring pl_widgetPrefix,
  in charstring pl_varNamePrefix,
  in EPTF_BooleanList pl_createdList,
  in EPTF_BooleanList pl_enabledList,
  in EPTF_LGenBaseStatsUI_tcOrSc pl_tcOrSc,
  in boolean pl_weighted)
runs on EPTF_LGenBaseStatsUI_Private_CT {
  var EPTF_Var_DirectContent vl_false := { boolVal := false };
  var template EPTF_LGenBaseStatsUI_displayOnScType vl_dsTemplate;
  if(pl_weighted){
    vl_dsTemplate := EPTF_LGenBaseStatsUI_displayOnScTypeWeighted;
  }else{
    vl_dsTemplate := EPTF_LGenBaseStatsUI_displayOnScTypeCps;
  }
  for ( var integer vl_i := 0; vl_i < sizeof(pl_btnDesc) ; vl_i := vl_i+1 )
  {
    var EPTF_LGenBaseStatsUI_displayOnScType vl_displayOn;
    if ( tc == pl_tcOrSc ){
      vl_displayOn := c_EPTF_LGenBaseStatsUI_tcVarDescriptor[pl_btnDesc[vl_i].column].displayOn;
    }else{
      vl_displayOn := c_EPTF_LGenBaseStatsUI_scVarDescriptor[pl_btnDesc[vl_i].column].displayOn;
    }
    if(pl_createdList[pl_btnDesc[vl_i].column] and
      match(vl_displayOn,vl_dsTemplate)
    ){
      var charstring vl_varShortName;
      if(tc == pl_tcOrSc){
        vl_varShortName := c_EPTF_LGenBaseStatsUI_tcVarDescriptor[pl_btnDesc[vl_i].column].varName;
      }else{
        vl_varShortName := c_EPTF_LGenBaseStatsUI_scVarDescriptor[pl_btnDesc[vl_i].column].varName;
      }
      var charstring vl_widgetName := pl_widgetPrefix&vl_varShortName;
      if ( pl_btnDesc[vl_i].enable and pl_enabledList[pl_btnDesc[vl_i].column]){
        f_EPTF_UIHandlerClient_enableGUIElement(vl_widgetName);
      }else{
        f_EPTF_UIHandlerClient_disableGUIElement(vl_widgetName);
      }
      if(pl_btnDesc[vl_i].clearIt){
        var integer vl_varIdx := f_EPTF_Var_getId(pl_varNamePrefix&vl_varShortName);
        f_EPTF_Base_assert(%definitionId&": Invalid variable name: "&pl_varNamePrefix&vl_varShortName,-1 < vl_varIdx);
        f_EPTF_Var_adjustContent(vl_varIdx, vl_false);
      }
    }
  }
}
private function f_EPTF_LGenBaseStatsUI_setTcButtonsState(
  in EPTF_LGenBase_tcState pl_state,
  in charstring pl_widgetPrefix,
  in charstring pl_varNamePrefix,
  in boolean pl_weighted,
  in EPTF_LGenBase_TrafficType pl_trafficType)
runs on EPTF_LGenBaseStatsUI_Private_CT {
  /*var charstring vl_tcStartBtnName;
  var charstring vl_tcStopBtnName;
  var charstring vl_tcAbortBtnName;
  var charstring vl_tcRestoreBtnName;
  var charstring vl_tcPauseBtnName;
  
  //vl_tcStartBtnName := pl_widgetPrefix&c_EPTF_LGenBaseStatsUI_nameOfStartBtn;
  //vl_tcStopBtnName := pl_widgetPrefix&c_EPTF_LGenBaseStatsUI_nameOfStopBtn;
  //vl_tcAbortBtnName := pl_widgetPrefix&c_EPTF_LGenBaseStatsUI_nameOfAbortBtn;
  //vl_tcRestoreBtnName := pl_widgetPrefix&c_EPTF_LGenBaseStatsUI_nameOfRestoreBtn;
  //vl_tcPauseBtnName := pl_widgetPrefix&c_EPTF_LGenBaseStatsUI_nameOfPauseBtn;
  */
  var EPTF_LGenBaseStatsUI_widgetSetDescriptorList vl_btnDesc := {}
  if(terminating == pl_trafficType){
    vl_btnDesc[0] := {c_EPTF_LGenBaseStatsUI_tcColStartBtn, false, false}
    vl_btnDesc[1] := {c_EPTF_LGenBaseStatsUI_tcColStopBtn, false, false}
    vl_btnDesc[2] := {c_EPTF_LGenBaseStatsUI_tcColAbortBtn, false, false}
    vl_btnDesc[3] := {c_EPTF_LGenBaseStatsUI_tcColPauseBtn, false, false}
    vl_btnDesc[4] := {c_EPTF_LGenBaseStatsUI_tcColEnabled, false, false}
    vl_btnDesc[5] := {c_EPTF_LGenBaseStatsUI_tcColSingleShotBtn, false, false}
  }else{
    select( pl_state )
    {
      case ( c_EPTF_LGenBase_tcStateIdle )
      {
        vl_btnDesc[0] := {c_EPTF_LGenBaseStatsUI_tcColStartBtn, true, true}
        vl_btnDesc[1] := {c_EPTF_LGenBaseStatsUI_tcColStopBtn, false, true}
        vl_btnDesc[2] := {c_EPTF_LGenBaseStatsUI_tcColAbortBtn, false, true}
        vl_btnDesc[3] := {c_EPTF_LGenBaseStatsUI_tcColPauseBtn, false, true}
        vl_btnDesc[4] := {c_EPTF_LGenBaseStatsUI_tcColEnabled, true, false}
        vl_btnDesc[5] := {c_EPTF_LGenBaseStatsUI_tcColSingleShotBtn, true, true}
      }
      case ( c_EPTF_LGenBase_tcStateRunning )
      {
        vl_btnDesc[0] := {c_EPTF_LGenBaseStatsUI_tcColStartBtn, false, true}
        vl_btnDesc[1] := {c_EPTF_LGenBaseStatsUI_tcColStopBtn, true, true}
        vl_btnDesc[2] := {c_EPTF_LGenBaseStatsUI_tcColAbortBtn, true, true}
        vl_btnDesc[3] := {c_EPTF_LGenBaseStatsUI_tcColPauseBtn, true, true}
        vl_btnDesc[4] := {c_EPTF_LGenBaseStatsUI_tcColEnabled, false, false}
        vl_btnDesc[5] := {c_EPTF_LGenBaseStatsUI_tcColSingleShotBtn, false, true}
      }
      case ( c_EPTF_LGenBase_tcStatePaused )
      {
        vl_btnDesc[0] := {c_EPTF_LGenBaseStatsUI_tcColStartBtn, true, true}
        vl_btnDesc[1] := {c_EPTF_LGenBaseStatsUI_tcColStopBtn, true, true}
        vl_btnDesc[2] := {c_EPTF_LGenBaseStatsUI_tcColAbortBtn, true, true}
        vl_btnDesc[3] := {c_EPTF_LGenBaseStatsUI_tcColPauseBtn, false, true}
        vl_btnDesc[4] := {c_EPTF_LGenBaseStatsUI_tcColEnabled, false, false}
        vl_btnDesc[5] := {c_EPTF_LGenBaseStatsUI_tcColSingleShotBtn, false, true}
      }
      case ( c_EPTF_LGenBase_tcStateStopping )
      {
        vl_btnDesc[0] := {c_EPTF_LGenBaseStatsUI_tcColStartBtn, false, true}
        vl_btnDesc[1] := {c_EPTF_LGenBaseStatsUI_tcColStopBtn, false, false}
        vl_btnDesc[2] := {c_EPTF_LGenBaseStatsUI_tcColAbortBtn, false, true}
        vl_btnDesc[3] := {c_EPTF_LGenBaseStatsUI_tcColPauseBtn, false, true}
        vl_btnDesc[4] := {c_EPTF_LGenBaseStatsUI_tcColEnabled, false, false}
        vl_btnDesc[5] := {c_EPTF_LGenBaseStatsUI_tcColSingleShotBtn, false, true}
      }
      case ( c_EPTF_LGenBase_tcStateAborting )
      {
        vl_btnDesc[0] := {c_EPTF_LGenBaseStatsUI_tcColStartBtn, false, true}
        vl_btnDesc[1] := {c_EPTF_LGenBaseStatsUI_tcColStopBtn, false, true}
        vl_btnDesc[2] := {c_EPTF_LGenBaseStatsUI_tcColAbortBtn, false, false}
        vl_btnDesc[3] := {c_EPTF_LGenBaseStatsUI_tcColPauseBtn, false, true}
        vl_btnDesc[4] := {c_EPTF_LGenBaseStatsUI_tcColEnabled, false, false}
        vl_btnDesc[5] := {c_EPTF_LGenBaseStatsUI_tcColSingleShotBtn, false, true}
      }
      case ( c_EPTF_LGenBase_tcStateStopped, 
        c_EPTF_LGenBase_tcStateAborted, 
        c_EPTF_LGenBase_tcStateTerminated)
      {
        vl_btnDesc[0] := {c_EPTF_LGenBaseStatsUI_tcColStartBtn, true, true}
        vl_btnDesc[1] := {c_EPTF_LGenBaseStatsUI_tcColStopBtn, false, true}
        vl_btnDesc[2] := {c_EPTF_LGenBaseStatsUI_tcColAbortBtn, false, true}
        vl_btnDesc[3] := {c_EPTF_LGenBaseStatsUI_tcColPauseBtn, false, true}
        vl_btnDesc[4] := {c_EPTF_LGenBaseStatsUI_tcColEnabled, true, false}
        vl_btnDesc[5] := {c_EPTF_LGenBaseStatsUI_tcColSingleShotBtn, false, true}
      }
      case ( c_EPTF_LGenBase_tcStateFinished )
      {
        vl_btnDesc[0] := {c_EPTF_LGenBaseStatsUI_tcColStartBtn, false, true}
        vl_btnDesc[1] := {c_EPTF_LGenBaseStatsUI_tcColStopBtn, true, true}
        vl_btnDesc[2] := {c_EPTF_LGenBaseStatsUI_tcColAbortBtn, true, true}
        vl_btnDesc[3] := {c_EPTF_LGenBaseStatsUI_tcColPauseBtn, false, true}
        vl_btnDesc[4] := {c_EPTF_LGenBaseStatsUI_tcColEnabled, false, false}
        vl_btnDesc[5] := {c_EPTF_LGenBaseStatsUI_tcColSingleShotBtn, false, true}
      }
      case else
      {
        f_EPTF_Base_assert(%definitionId&": Invalid state: "&int2str(pl_state),false);
      }
    }
  }
  f_EPTF_LGenBaseStatsUI_setTcButtons(
    vl_btnDesc, 
    pl_widgetPrefix, 
    pl_varNamePrefix,
    v_LGenBaseStatsUI_createdTcWidgets, 
    v_LGenBaseStatsUI_enabledTcWidgets,
    tc,
    pl_weighted);
}

private function f_EPTF_LGenBaseStatsUI_setScButtonsState(
  in EPTF_LGenBase_tcState pl_state,
  in charstring pl_widgetPrefix,
  in boolean pl_weighted,
  in EPTF_LGenBase_TrafficType pl_trafficType)
runs on EPTF_LGenBaseStatsUI_Private_CT {
  //  if ( pl_isSc ){
  /*
  var charstring vl_scStartBtnName := pl_widgetPrefix&c_EPTF_LGenBaseStatsUI_nameOfStartBtn;
  var charstring vl_scStopBtnName := pl_widgetPrefix&c_EPTF_LGenBaseStatsUI_nameOfStopBtn;
  var charstring vl_scAbortBtnName := pl_widgetPrefix&c_EPTF_LGenBaseStatsUI_nameOfAbortBtn;
  var charstring vl_scRestoreBtnName := pl_widgetPrefix&c_EPTF_LGenBaseStatsUI_nameOfRestoreBtn;
  var charstring vl_scPauseBtnName := pl_widgetPrefix&c_EPTF_LGenBaseStatsUI_nameOfPauseBtn;
  var charstring vl_scEnabledName :=  pl_widgetPrefix&c_EPTF_LGenBaseStats_nameOfScEnabled;
  */
  var EPTF_LGenBaseStatsUI_widgetSetDescriptorList vl_btnDesc := {}
  if(terminating == pl_trafficType){
    vl_btnDesc[0] := {c_EPTF_LGenBaseStatsUI_scColStartBtn, false, false}
    vl_btnDesc[1] := {c_EPTF_LGenBaseStatsUI_scColStopBtn, false, false}
    vl_btnDesc[2] := {c_EPTF_LGenBaseStatsUI_scColAbortBtn, false, false}
    vl_btnDesc[3] := {c_EPTF_LGenBaseStatsUI_scColRestoreBtn, false, false}
    vl_btnDesc[4] := {c_EPTF_LGenBaseStatsUI_scColEnabled, false, false}
    vl_btnDesc[5] := {c_EPTF_LGenBaseStatsUI_scColPauseBtn, false, false}
    vl_btnDesc[6] := {c_EPTF_LGenBaseStatsUI_scColSingleShotBtn, false, false}
  }else{
    select( pl_state )
    {
      case ( c_EPTF_LGenBase_tcStateIdle )
      {
        vl_btnDesc[0] := {c_EPTF_LGenBaseStatsUI_scColStartBtn, true, false}
        vl_btnDesc[1] := {c_EPTF_LGenBaseStatsUI_scColStopBtn, false, false}
        vl_btnDesc[2] := {c_EPTF_LGenBaseStatsUI_scColAbortBtn, false, false}
        vl_btnDesc[3] := {c_EPTF_LGenBaseStatsUI_scColRestoreBtn, false, false}
        vl_btnDesc[4] := {c_EPTF_LGenBaseStatsUI_scColEnabled, true, false}
        vl_btnDesc[5] := {c_EPTF_LGenBaseStatsUI_scColPauseBtn, false, false}
        vl_btnDesc[6] := {c_EPTF_LGenBaseStatsUI_scColSingleShotBtn, true, false}
      }
      case ( c_EPTF_LGenBase_tcStateRunning )
      {
        vl_btnDesc[0] := {c_EPTF_LGenBaseStatsUI_scColStartBtn, false, false}
        vl_btnDesc[1] := {c_EPTF_LGenBaseStatsUI_scColStopBtn, true, false}
        vl_btnDesc[2] := {c_EPTF_LGenBaseStatsUI_scColAbortBtn, true, false}
        vl_btnDesc[3] := {c_EPTF_LGenBaseStatsUI_scColRestoreBtn, false, false}
        vl_btnDesc[4] := {c_EPTF_LGenBaseStatsUI_scColEnabled, false, false}
        vl_btnDesc[5] := {c_EPTF_LGenBaseStatsUI_scColPauseBtn, true, false}
        vl_btnDesc[6] := {c_EPTF_LGenBaseStatsUI_scColSingleShotBtn, false, false}
      }
      case ( c_EPTF_LGenBase_tcStatePaused )
      {
        vl_btnDesc[0] := {c_EPTF_LGenBaseStatsUI_scColStartBtn, true, false}
        vl_btnDesc[1] := {c_EPTF_LGenBaseStatsUI_scColStopBtn, true, false}
        vl_btnDesc[2] := {c_EPTF_LGenBaseStatsUI_scColAbortBtn, true, false}
        vl_btnDesc[3] := {c_EPTF_LGenBaseStatsUI_scColRestoreBtn, false, false}
        vl_btnDesc[4] := {c_EPTF_LGenBaseStatsUI_scColEnabled, false, false}
        vl_btnDesc[5] := {c_EPTF_LGenBaseStatsUI_scColPauseBtn, false, false}
        vl_btnDesc[6] := {c_EPTF_LGenBaseStatsUI_scColSingleShotBtn, false, false}
      }
      case ( c_EPTF_LGenBase_tcStateStopping, c_EPTF_LGenBase_tcStateAborting )
      {
        vl_btnDesc[0] := {c_EPTF_LGenBaseStatsUI_scColStartBtn, false, false}
        vl_btnDesc[1] := {c_EPTF_LGenBaseStatsUI_scColStopBtn, false, false}
        vl_btnDesc[2] := {c_EPTF_LGenBaseStatsUI_scColAbortBtn, false, false}
        vl_btnDesc[3] := {c_EPTF_LGenBaseStatsUI_scColRestoreBtn, false, false}
        vl_btnDesc[4] := {c_EPTF_LGenBaseStatsUI_scColEnabled, false, false}
        vl_btnDesc[5] := {c_EPTF_LGenBaseStatsUI_scColPauseBtn, false, false}
        vl_btnDesc[6] := {c_EPTF_LGenBaseStatsUI_scColSingleShotBtn, false, false}
      }
      case ( c_EPTF_LGenBase_tcStateStopped, 
        c_EPTF_LGenBase_tcStateAborted,
        c_EPTF_LGenBase_tcStateTerminated)
      {
        vl_btnDesc[0] := {c_EPTF_LGenBaseStatsUI_scColStartBtn, false, false}
        vl_btnDesc[1] := {c_EPTF_LGenBaseStatsUI_scColStopBtn, false, false}
        vl_btnDesc[2] := {c_EPTF_LGenBaseStatsUI_scColAbortBtn, false, false}
        vl_btnDesc[3] := {c_EPTF_LGenBaseStatsUI_scColRestoreBtn, true, false}
        vl_btnDesc[4] := {c_EPTF_LGenBaseStatsUI_scColEnabled, false, false}
        vl_btnDesc[5] := {c_EPTF_LGenBaseStatsUI_scColPauseBtn, false, false}
        vl_btnDesc[6] := {c_EPTF_LGenBaseStatsUI_scColSingleShotBtn, false, false}
      }
      case ( c_EPTF_LGenBase_tcStateFinished )
      {
        vl_btnDesc := {
          {c_EPTF_LGenBaseStatsUI_scColStartBtn, false, false},
          {c_EPTF_LGenBaseStatsUI_scColStopBtn, true, false},
          {c_EPTF_LGenBaseStatsUI_scColAbortBtn, true, false},
          {c_EPTF_LGenBaseStatsUI_scColRestoreBtn, false, false},
          {c_EPTF_LGenBaseStatsUI_scColEnabled, false, false},
          {c_EPTF_LGenBaseStatsUI_scColPauseBtn, false, false},
          {c_EPTF_LGenBaseStatsUI_scColSingleShotBtn, false, false}
        }
      }
      case else
      {
        f_EPTF_Base_assert(%definitionId&": Invalid state: "&int2str(pl_state),false);
      }
    }
  }
  f_EPTF_LGenBaseStatsUI_setTcButtons(
    vl_btnDesc, 
    pl_widgetPrefix, 
    "", 
    v_LGenBaseStatsUI_createdScWidgets, 
    v_LGenBaseStatsUI_enabledScWidgets,
    sc,
    pl_weighted);
}

template EPTF_LGenBaseStatsUI_displayOnScType EPTF_LGenBaseStatsUI_displayOnScTypeWeighted := (dsBoth, dsWeight);
template EPTF_LGenBaseStatsUI_displayOnScType EPTF_LGenBaseStatsUI_displayOnScTypeCps := (dsBoth, dsCps);

private function f_EPTF_LGenBaseStatsUI_tcAbsIdx(
  in integer pl_eGrpIdx,
  in integer pl_scIdx,
  in integer pl_tcRelIdx
)
runs on EPTF_LGenBaseStatsUI_Private_CT
return integer{
  return f_EPTF_LGenBase_trafficCaseAbsIdx(pl_eGrpIdx, pl_scIdx, pl_tcRelIdx)
}

}
