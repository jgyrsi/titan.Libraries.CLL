///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_LoggingUIClient_Functions
// 
//  Purpose:
//    Creates and manages EPTF_Variables and GUI elements for the LoggingUI Clients logging parameters.
// 
//  Module depends on:
//    <EPTF_CLL_Common_Functions>
//    <EPTF_CLL_LoggingUI_Definitions>
//    <EPTF_CLL_LoggingUI_PrivateFunctions>
//    <EPTF_CLL_Logging_Functions>
//    <EPTF_CLL_Variable_Definitions>
//    <EPTF_CLL_Variable_Functions>
//    <EPTF_CLL_Base_Functions>
//    <EPTF_CLL_UIHandlerClient_Functions>
//    <EPTF_CLL_UIHandler_Definitions>
//    <XTDP_PDU_Defs language>
//
//  Current Owner:
//    Bence Molnar (EBENMOL)
// 
//  Last Review Date:
//    2008-01-15
//
//  Detailed Comments:
//    Creates and manages EPTF_Variables and GUI elements for the LoggingUI Clients logging parameters. 
//
//    <f_EPTF_LoggingUIClient_init_CT> - Function to initialize LoggingUI Client component. 
//
//    <f_EPTF_LoggingUI_enableAllGlobal> - Function to enable EPTF Logging of all features on all components. 
//
//    <f_EPTF_LoggingUI_disableAllGlobal> - Function to disable EPTF User Logging of all features on all components.. 
//
//    <f_EPTF_LoggingUI_enableGlobal> - Function to enable EPTF Logging on the current component type. 
//
//    <f_EPTF_LoggingUI_disableGlobal> - Function to disable EPTF Logging on the current component type. 
//
//    <f_EPTF_LoggingUI_enableGlobalMask> - Function to enable an EPTF Logging  an event class on the current component type.
//
//    <f_EPTF_LoggingUI_disableGlobalMask> - Function to disable an EPTF Logging an event class on the current component type. 
//
//    <f_EPTF_LoggingUIClient_cleanup_CT> - Function to shutdown loggingUI client.
//
//    <f_EPTF_LoggingUI_componentLocalToGui> - Function to create logging GUI elements. 
//
//    <f_EPTF_LoggingUI_removeComponentGui> - Function to remove logging GUI elements.
//
//    <f_EPTF_LoggingUI_componentLocalBindToEPTFVar> - Binds boolean parameters to EPTF_variables. 
//
//    <f_EPTF_LoggingUI_componentLocalBindToGui> - Function to bind EPTF Variables to Gui.
//
//    <f_EPTF_LoggingUI_allComponentLocalToGui> - Function to create logging GUI elements. 
//
//    <f_EPTF_LoggingUI_allComponentLocalBindToEPTFVar> - Binds boolean parameters to EPTF_variables.
//
//    <f_EPTF_LoggingUI_allComponentLocalBindToGui> - Function to bind EPTF Variables to Gui.
//
//    <f_EPTF_LoggingUI_allComponentGlobalSubscribeToEPTFVar> - Function to subscribe variable to the global EPTF Variable. 
//
//    <f_EPTF_LoggingUI_componentGlobalSubscribeToEPTFVar> - Function to subscribe variables to the global EPTF Variable.
//
//    <f_EPTF_LoggingUIClient_loggingUpdate> - Callback function for Logging_CT.
//
//    <f_EPTF_LoggingUIClient_refreshAllRefs> - Callback function for Logging_CT.
//
//    <f_EPTF_LoggingUI_refreshVar> - Callback function for Logging_CT.
//
///////////////////////////////////////////////////////////

module EPTF_CLL_LoggingUIClient_Functions

{
import from EPTF_CLL_Common_Functions all;

// if you use EPTF_LoggingUI_CT you have to import these
import from EPTF_CLL_LoggingUI_Definitions all;
import from EPTF_CLL_LoggingUI_PrivateFunctions all

// if you use EPTF_Logging_CT you have to import these
import from EPTF_CLL_Logging_Functions all;

// if you use EPTF_Var_CT you have to import these
import from EPTF_CLL_Variable_Definitions all;
import from EPTF_CLL_Variable_Functions all;
import from EPTF_CLL_Base_Functions all;

// if you use UIHandler&XUL you have to import these
import from EPTF_CLL_UIHandlerClient_Functions all;
import from EPTF_CLL_UIHandler_Definitions all;

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUIClient_init_CT
// 
//  Purpose:
//    Function to initialize loggingUI Client component.
// 
//  Parameters:
//    pl_selfName - *in* *charstring* - name of LoggingUI client component
//    pl_loggingUI_main - *in* <EPTF_LoggingUI_CT> - the main LoggingUI component
//    pl_subscriber - *in* <EPTF_UIHandler_CT> - UIHandler component
//
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
//    The function initializes LoggingUI client component. Initializes Logging_CT, EPTF_Var_CT, UIHandlerClient_CT.
//    After this sends out <EPTF_LoggingUI_Msg> type request and waits for the response. If the response arrives, the main
//    window created and the component checkboxes can be put to the GUI. 
// 
///////////////////////////////////////////////////////////

public function f_EPTF_LoggingUIClient_init_CT(
  in charstring pl_selfName,
  in EPTF_LoggingUI_CT pl_loggingUI_main,
  in EPTF_UIHandler_CT pl_subscriber) runs on EPTF_LoggingUIClient_CT {

  if (v_EPTF_LoggingUIClient_initialized) {return;}
  v_EPTF_LoggingUIClient_initialized := true;
  
  //log("----- LoggingUI init START -------");
  v_EPTF_LoggingUI_main:=pl_loggingUI_main;

  f_EPTF_Logging_init_CT(pl_selfName);

  //f_EPTF_Var_CT_LogAll("----- EPTF VAR INIT START -------");//log
  f_EPTF_Var_init_CT(pl_selfName);
  //f_EPTF_Var_CT_LogAll("----- EPTF VAR INIT READY -------");//log


  f_EPTF_UIHandlerClient_init_CT(pl_selfName, pl_subscriber);

  //log("-----v_EPRF_Logging_myMaskId: ",v_EPTF_Logging_myMaskId);

  connect(self:EPTF_LoggingUIClientPort_CP, pl_loggingUI_main:EPTF_LoggingUIPort_CP);

  EPTF_LoggingUIClientPort_CP.send(c_EPTF_LoggingUI_hello);
  
  for (var integer i:=0; i<sizeof(v_EPTF_Logging_ComponentMask_List); i:=i+1) {
    var EPTF_LoggingUI_Msg vl_req:={
      LoggingUI_Req:={
        Request:=Init,
        compName:=f_EPTF_Base_selfName(),
        compType:=v_EPTF_Logging_ComponentMask_List[i].componentTypeName,
        selection:=v_EPTF_Logging_ComponentMask_List[i].selection,
        eventClassNames:=v_EPTF_Logging_ComponentMask_List[i].eventClassNames
      }
    }

    EPTF_LoggingUIClientPort_CP.send(vl_req);

    alt {
      [] EPTF_LoggingUIClientPort_CP.receive(c_EPTF_LoggingUI_resp) {}
    }

    f_EPTF_LoggingUI_componentGlobalSubscribeToEPTFVar(i);

    //f_EPTF_LoggingUI_componentLocalToGui(i);
    f_EPTF_LoggingUI_componentLocalBindToEPTFVar(i);
    f_EPTF_LoggingUI_componentLocalBindToGui(i);
  }

  //f_EPTF_LoggingUI_allComponentLocalToGui();
  f_EPTF_LoggingUI_allComponentLocalBindToEPTFVar();
  f_EPTF_LoggingUI_allComponentLocalBindToGui();
  f_EPTF_LoggingUI_allComponentGlobalSubscribeToEPTFVar();


  f_EPTF_Logging_registerGUICallbacks(refers(f_EPTF_LoggingUIClient_loggingUpdate),refers(f_EPTF_LoggingUI_refreshVar));

  f_EPTF_Base_registerCleanup(refers(f_EPTF_LoggingUIClient_cleanup_CT));
  //log("----- LoggingUI init END -------");

}

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_enableAllGlobal
// 
//  Purpose:
//    Function to enable EPTF Logging of all features on all components.
// 
//  Parameters:
//    -
//
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
//    Calling this function will enable EPTF logging on all components.
// 
///////////////////////////////////////////////////////////

public function f_EPTF_LoggingUI_enableAllGlobal() runs on EPTF_LoggingUIClient_CT {
  v_EPTF_Logging_allComponentGlobalEnable := true;
  f_EPTF_Logging_refreshUIVar(v_EPTF_Logging_allComponentGlobalEnable);
}

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_disableAllGlobal
// 
//  Purpose:
//    Function to disable EPTF User Logging of all features on all components.
// 
//  Parameters:
//    -
//
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
//    Calling this function will disable EPTF logging on all components.
// 
///////////////////////////////////////////////////////////

public function f_EPTF_LoggingUI_disableAllGlobal() runs on EPTF_LoggingUIClient_CT {
  v_EPTF_Logging_allComponentGlobalEnable := false;
  f_EPTF_Logging_refreshUIVar(v_EPTF_Logging_allComponentGlobalEnable);
}

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_enableGlobal
// 
//  Purpose:
//    Function to enable EPTF Logging on the current component type.
// 
//  Parameters:
//    pl_compTypeId - *in* *integer* - the ID of the component type
//
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
// 
///////////////////////////////////////////////////////////

public function f_EPTF_LoggingUI_enableGlobal(in integer pl_compTypeId) runs on EPTF_LoggingUIClient_CT {
  v_EPTF_Logging_ComponentMask_List[pl_compTypeId].componentGlobalEnable := true;
  f_EPTF_Logging_refreshUIVar(v_EPTF_Logging_ComponentMask_List[pl_compTypeId].componentGlobalEnable);
}

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_disableGlobal
// 
//  Purpose:
//    Function to disable EPTF Logging on the current component type.
// 
//  Parameters:
//    pl_compTypeId - *in* *integer* - the ID of the component type
//
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
// 
///////////////////////////////////////////////////////////

public function f_EPTF_LoggingUI_disableGlobal(in integer pl_compTypeId) runs on EPTF_LoggingUIClient_CT {
  v_EPTF_Logging_ComponentMask_List[pl_compTypeId].componentGlobalEnable := false;
  f_EPTF_Logging_refreshUIVar(v_EPTF_Logging_ComponentMask_List[pl_compTypeId].componentGlobalEnable);
}

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_enableGlobalMask
// 
//  Purpose:
//    Function to enable an EPTF Logging an event class on the current component type.
// 
//  Parameters:
//    pl_compTypeId - *in* *integer* - the ID of the component type
//    pl_eventClass - *in* *integer* - index of the event class
//
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
//    Calling this function will enable the given EPTF logging event class on this component type.
//    The pl_eventClass is the index of the event class defined by <tsp_EPTF_UserEventClassPrefixList>.
// 
///////////////////////////////////////////////////////////

public function f_EPTF_LoggingUI_enableGlobalMask(in integer pl_compTypeId,in integer pl_eventClass) runs on EPTF_LoggingUIClient_CT {
  v_EPTF_Logging_ComponentMask_List[pl_compTypeId].componentGlobalMask[pl_eventClass] := true;
  f_EPTF_Logging_refreshUIVar(v_EPTF_Logging_ComponentMask_List[pl_compTypeId].componentGlobalMask[pl_eventClass]);
}

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_disableGlobalMask
// 
//  Purpose:
//    Function to disable an EPTF Logging an event class on the current component type.
// 
//  Parameters:
//    pl_compTypeId - *in* *integer* - the ID of the component type
//    pl_eventClass - *in* *integer* - index of the event class
//
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
//    Calling this function will disable the given EPTF logging event class on this component type.
//    The pl_eventClass is the index of the event class defined by <tsp_EPTF_UserEventClassPrefixList>.
// 
///////////////////////////////////////////////////////////

public function f_EPTF_LoggingUI_disableGlobalMask(in integer pl_compTypeId,in integer pl_eventClass) runs on EPTF_LoggingUIClient_CT {
  v_EPTF_Logging_ComponentMask_List[pl_compTypeId].componentGlobalMask[pl_eventClass] := false;
  f_EPTF_Logging_refreshUIVar(v_EPTF_Logging_ComponentMask_List[pl_compTypeId].componentGlobalMask[pl_eventClass]);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
// private:
//////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Group: Private
//
// Purpose:
//   Private functions. These functions must not be used by the user of <EPTF_LoggingUIClient_CT>
//
// Elements:
///////////////////////////////////////////////////////////
group Private {

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUIClient_cleanup_CT
// 
//  Purpose:
//    Function to shutdown loggingUI client.
// 
//  Parameters:
//    -
//
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
//    -
// 
///////////////////////////////////////////////////////////

private function f_EPTF_LoggingUIClient_cleanup_CT() runs on EPTF_LoggingUIClient_CT {
  //log("----- LoggingUIClient cleanup -------");
  if (v_EPTF_LoggingUIClient_initialized == false) {
    return;
  }

  f_EPTF_LoggingUI_removeComponentGui();

  EPTF_LoggingUIClientPort_CP.send(c_EPTF_LoggingUI_bye);

  disconnect(self:EPTF_LoggingUIClientPort_CP, v_EPTF_LoggingUI_main:EPTF_LoggingUIPort_CP);

  v_EPTF_LoggingUIClient_initialized := false;

}

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_removeComponentGui
// 
//  Purpose:
//    Function to remove the logging gui elements for the boolean values mask. (now only disables the mask)
// 
//  Parameters:
//    -
//
//  Return Value:
//    -
// 
//  Errors:
//  - (none) 
// 
//  Detailed Comments:
//    The Function removes the tablerow from the Component Types tab. 
//    The removed row countained checkboxes for the logging boolean values.(now only disables the row)
// 
///////////////////////////////////////////////////////////

private function f_EPTF_LoggingUI_removeComponentGui() runs on EPTF_LoggingUIClient_CT {


  //log("***********f_EPTF_LoggingUI_removeComponentGui***************");

  for(var integer j:=0; j<sizeof(v_EPTF_Logging_ComponentMask_List) ; j:=j+1) {

    var charstring vl_selection:=f_EPTF_LoggingUI_Selection2Str(v_EPTF_Logging_ComponentMask_List[j].selection);
    var charstring vl_compTypeName:=v_EPTF_Logging_ComponentMask_List[j].componentTypeName;
  
    //disable mask checkboxes
    f_EPTF_UIHandlerClient_disableGUIElement(f_EPTF_Base_selfName()&"."&vl_compTypeName&"."&vl_selection&".Component_CheckBoxes_ComponentName");
    f_EPTF_UIHandlerClient_disableGUIElement(f_EPTF_Base_selfName()&"."&vl_compTypeName&"."&vl_selection&".Component_CheckBoxes_Enable");

    for (var integer i := 0; i < sizeof(v_EPTF_Logging_ComponentMask_List[j].eventClassNames); i := i + 1)
    {
       f_EPTF_UIHandlerClient_disableGUIElement(f_EPTF_Base_selfName()&"."&vl_compTypeName&"."&vl_selection&".Component_CheckBoxes_Mask" & int2str(i));     
    }

    //disable component global checkboxes
    f_EPTF_UIHandlerClient_disableGUIElement(f_EPTF_Base_selfName()&".allcomponent.local.enable_ComponentName");
    f_EPTF_UIHandlerClient_disableGUIElement(f_EPTF_Base_selfName()&".allcomponent.local.enable_Enable");
    
    // remove element later!
   // f_EPTF_UIHandlerClient_removeElementFromGUI(f_EPTF_Base_selfName()&"."&vl_selection&".Component_CheckBoxes_ComponentName");
  }

} 

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_componentLocalBindToEPTFVar
// 
//  Purpose:
//    Binds boolean parameters to EPTF_variables.
// 
//  Parameters:
//    pl_logMaskIdx - *in* *integer* - ID of the loggingMask
//
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
//    Binds the boolean parameters to EPTF_variables. Binds <v_EPTF_Logging_ComponentMask_List.componentLocalMask> to EPTF_Var variables.
// 
///////////////////////////////////////////////////////////

private function f_EPTF_LoggingUI_componentLocalBindToEPTFVar(
  in integer pl_logMaskIdx) runs on EPTF_LoggingUIClient_CT {

  //log("***********f_EPTF_LoggingUI_componentLocalBindToEPTFVar***************");
  var charstring boolVarRef;
  var charstring vl_selection:=f_EPTF_LoggingUI_Selection2Str(v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].selection);
  var charstring vl_compTypeName:=v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].componentTypeName;

  // mask bindings
  boolVarRef := "componentLocalEnable."&f_EPTF_Base_selfName()&"."&vl_compTypeName&"."&vl_selection;

  f_EPTF_Var_newBoolRef(boolVarRef, v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].componentLocalEnable,v_EPTF_LoggingUI_componentLocalEnableIdx[pl_logMaskIdx]);

  for(var integer i:=0; i<sizeof(v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].eventClassNames); i:=i+1) {

    //log("EPTF_componentLocalMask: ",v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].componentLocalMask[i]);

    boolVarRef := "componentLocalMask."&int2str(i)&f_EPTF_Base_selfName()&"."&vl_compTypeName&"."&vl_selection;
    f_EPTF_Var_newBoolRef(boolVarRef, v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].componentLocalMask[i],v_EPTF_LoggingUI_componentLocalMaskIdx[pl_logMaskIdx][i]);
  }
}

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_componentLocalBindToGui
// 
//  Purpose:
//    Function to bind EPTF Variables to Gui
// 
//  Parameters:
//    pl_logMaskIdx - *in* *integer* - ID of the loggingMask
//
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
//    Binds <v_EPTF_Logging_ComponentMask_List.componentLocalEnable> and <v_EPTF_Logging_ComponentMask_List.componentLocalMask> 
//    and its EPTF_Var variables with the corresponding checkboxes.
// 
///////////////////////////////////////////////////////////

private function f_EPTF_LoggingUI_componentLocalBindToGui(
  in integer pl_logMaskIdx) runs on EPTF_LoggingUIClient_CT {

  //log("***********f_EPTF_LoggingUI_componentLocalBindToGui***************");
  var charstring boolVarName,boolVarName2,boolVarName3;
  var charstring vl_selection:=f_EPTF_LoggingUI_Selection2Str(v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].selection);
  var charstring vl_compTypeName:=v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].componentTypeName;

  // mask bindings
  boolVarName := f_EPTF_Var_getName(v_EPTF_LoggingUI_componentLocalEnableIdx[pl_logMaskIdx]);
  boolVarName2 := f_EPTF_Base_selfName()&"."&vl_compTypeName&"."&vl_selection&".subscriber_componentLogIsEnabled";
  //boolVarName3 :="Component_CheckBoxes"&int2str(j)&"_Enable";
  boolVarName3 := f_EPTF_Base_selfName()&"."&vl_compTypeName&"."&vl_selection&".Component_CheckBoxes_Enable";
  f_EPTF_UIHandlerClient_subscribeMe(boolVarName, boolVarName2, boolVarName3); 

  for(var integer i:=0; i<sizeof(v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].eventClassNames); i:=i+1) {

    boolVarName := f_EPTF_Var_getName(v_EPTF_LoggingUI_componentLocalMaskIdx[pl_logMaskIdx][i]);
    boolVarName2 := f_EPTF_Base_selfName()&"."&vl_compTypeName&"."&vl_selection&int2str(i)&".subscriber.componentLogMaskList";
    boolVarName3 := f_EPTF_Base_selfName()&"."&vl_compTypeName&"."&vl_selection&".Component_CheckBoxes_Mask"&int2str(i);
    f_EPTF_UIHandlerClient_subscribeMe(boolVarName, boolVarName2, boolVarName3);
  }
} 

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_allComponentLocalBindToEPTFVar
// 
//  Purpose:
//    Binds boolean parameters to EPTF_variables
// 
//  Parameters:
//    -
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
//    Binds the boolean parameters to EPTF_variables. Binds <v_EPTF_Logging_allComponentLocalEnable> to EPTF_Var variables.
// 
///////////////////////////////////////////////////////////

private function f_EPTF_LoggingUI_allComponentLocalBindToEPTFVar() runs on EPTF_LoggingUIClient_CT {

  //log("***********f_EPTF_LoggingUI_AllComponentLoggingBindToEPTFVar***************");
  var charstring boolVarRef;

  // all component local bindings
  boolVarRef := "allcomponentLocalEnable."&f_EPTF_Base_selfName();
  f_EPTF_Var_newBoolRef(boolVarRef, v_EPTF_Logging_allComponentLocalEnable, v_EPTF_LoggingUI_allComponentLocalEnableIdx);  
}

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_allComponentLocalBindToGui
// 
//  Purpose:
//    Function to bind EPTF Variables to Gui
// 
//  Parameters:
//    -
//  Return Value:
//    -
// 
//  Errors:
//  - (none) 
// 
//  Detailed Comments:
//    Binds <v_EPTF_Logging_allComponentLocalEnable> and its EPTF_Var variables with the corresponding checkboxes.
// 
///////////////////////////////////////////////////////////

private function f_EPTF_LoggingUI_allComponentLocalBindToGui() runs on EPTF_LoggingUIClient_CT {

  //log("***********f_EPTF_LoggingUI_AllComponentLoggingBindToGui***************");
  var charstring boolVarName,boolVarName2,boolVarName3;

  // all component local bindings
  boolVarName := f_EPTF_Var_getName(v_EPTF_LoggingUI_allComponentLocalEnableIdx);
  boolVarName2 := f_EPTF_Base_selfName()&".subscriber_allcomponent.local.enable";
  boolVarName3 := f_EPTF_Base_selfName()&".allcomponent.local.enable_Enable";

  f_EPTF_UIHandlerClient_subscribeMe(boolVarName, boolVarName2, boolVarName3);
} 


//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_allComponentGlobalSubscribeToEPTFVar
// 
//  Purpose:
//    Function to subscribe <allComponentGlobalEnable> variable to the global EPTF Variable.
// 
//  Parameters:
//    -
//  Return Value:
//    -
// 
//  Errors:
//  - (none) 
// 
//  Detailed Comments:
//    Main LoggingUI component creates an EPTF_Variable, and a checkbox to the GUI for this boolean variable.
//    This function subscribes <allComponentGlobalEnable> variable to the EPTF Variable created by the main component.
// 
///////////////////////////////////////////////////////////

private function f_EPTF_LoggingUI_allComponentGlobalSubscribeToEPTFVar() runs on EPTF_LoggingUIClient_CT {

  var integer idx;
  //allComponentGlobalEnable
  f_EPTF_Var_subscribeRemoteBoolRef(v_EPTF_Logging_allComponentGlobalEnable,v_EPTF_LoggingUI_main,"allComponentGlobalEnable",realtime,idx);
}

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_componentGlobalSubscribeToEPTFVar
// 
//  Purpose:
//    Function to subscribe <EPTF_Logging_ComponentMask.componentGlobalEnable> , 
//    <EPTF_Logging_ComponentMask.componentGlobalMask>  variables to an EPTF Variable.
// 
//  Parameters:
//    pl_logMaskIdx - *in* *integer* - ID of the loggingMask
//
//  Return Value:
//    -
// 
//  Errors:
//  - (none) 
// 
//  Detailed Comments:
//    Main LoggingUI component creates an EPTF_Variable, and a checkbox to the GUI for this boolean variable.
//    This function subscribes <EPTF_Logging_ComponentMask.componentGlobalEnable> and 
//    <EPTF_Logging_ComponentMask.componentGlobalMask> variables to the EPTF Variable created by the main component.
// 
///////////////////////////////////////////////////////////

private function f_EPTF_LoggingUI_componentGlobalSubscribeToEPTFVar(
  in integer pl_logMaskIdx) runs on EPTF_LoggingUIClient_CT {

  var charstring vl_selection:=f_EPTF_LoggingUI_Selection2Str(v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].selection);
  var charstring vl_compTypeName:=v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].componentTypeName;
  var integer idx;
  
  //componentGlobalEnable
  f_EPTF_Var_subscribeRemoteBoolRef(v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].componentGlobalEnable,
    v_EPTF_LoggingUI_main,
    "EPTF_LoggingUI.componentGlobalEnable."&vl_compTypeName&"."&vl_selection,
    realtime,idx,
    "componentGlobalEnable."&vl_compTypeName&"."&vl_selection);

  for(var integer i:=0; i<sizeof(v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].eventClassNames); i:=i+1) {
    //componentGlobalMask
    f_EPTF_Var_subscribeRemoteBoolRef(v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].componentGlobalMask[i],
    v_EPTF_LoggingUI_main,
    "EPTF_LoggingUI.componentGlobalMask."&int2str(i)&"."&vl_compTypeName&"."&vl_selection,
    realtime,idx,
    "componentGlobalMask."&int2str(i)&"."&vl_compTypeName&"."&vl_selection);
  }

}

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUIClient_loggingUpdate
// 
//  Purpose:
//    Callback function for Logging_CT
// 
//  Parameters:
//    pl_logMaskIdx - *in* *integer* - the the mask ID to be updated
//
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
//    After calling the function <f_EPTF_Logging_registerComponentMasks> EPTF_Logging_CT uses this callback function
//    to put the new mask parameters to the GUI.
// 
///////////////////////////////////////////////////////////

private function f_EPTF_LoggingUIClient_loggingUpdate(
  in integer pl_logMaskIdx) runs on EPTF_LoggingUIClient_CT {

  //log("----- f_EPTF_LoggingUIClient_LogUpdate Callback -------");
  var EPTF_LoggingUI_Msg vl_req:={
    LoggingUI_Req:={
      Request:=Init,
      compName:=f_EPTF_Base_selfName(),
      compType:=v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].componentTypeName,
      selection:=v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].selection,
      eventClassNames:=v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].eventClassNames
    }
  }

  EPTF_LoggingUIClientPort_CP.send(vl_req);

  alt {
    [] EPTF_LoggingUIClientPort_CP.receive(c_EPTF_LoggingUI_resp) {}
  }

  f_EPTF_LoggingUI_componentGlobalSubscribeToEPTFVar(pl_logMaskIdx);
  //f_EPTF_LoggingUI_componentLocalToGui(pl_logMaskIdx);
  f_EPTF_LoggingUI_componentLocalBindToEPTFVar(pl_logMaskIdx);
  f_EPTF_LoggingUI_componentLocalBindToGui(pl_logMaskIdx);

  f_EPTF_LoggingUIClient_refreshAllRefs();
}

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUIClient_refreshAllRefs
// 
//  Purpose:
//    Callback function for Logging_CT. Refreshes component references of all EPTF Variables.
// 
//  Parameters:
//    -
//
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
//    TITAN dynamically allocates memory for list elements. When the list become full memory reallocated.
//    The allocated component references to EPTF Variables go wrong, so we need to refresh the component references.
//    This function refreshes component references of all EPTF Variables.
// 
///////////////////////////////////////////////////////////

private function f_EPTF_LoggingUIClient_refreshAllRefs() runs on EPTF_LoggingUIClient_CT {
  var charstring vl_varName;
  // do this for all comp variables:
  //f_EPTF_Var_CT_LogAll("f_EPTF_LoggingUIClient_refreshAllRefs");

  for (var integer pl_logMaskIdx:= 0; pl_logMaskIdx<sizeof(v_EPTF_Logging_ComponentMask_List); pl_logMaskIdx:=pl_logMaskIdx+1) {

    var charstring vl_selection:=f_EPTF_LoggingUI_Selection2Str(v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].selection);
    var charstring vl_compTypeName:=v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].componentTypeName;


  //componentGlobalEnable
  vl_varName := "componentGlobalEnable."&vl_compTypeName&"."&vl_selection;
  f_EPTF_Var_updateRef(f_EPTF_Var_getId(vl_varName),f_EPTF_Var_getBoolRef(v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].componentGlobalEnable));

  for(var integer i:=0; i<sizeof(v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].eventClassNames); i:=i+1) {
    //componentGlobalMask
    vl_varName := "componentGlobalMask."&int2str(i)&"."&vl_compTypeName&"."&vl_selection;
    f_EPTF_Var_updateRef(f_EPTF_Var_getId(vl_varName),f_EPTF_Var_getBoolRef(v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].componentGlobalMask[i]));
  }

  // mask bindings
  vl_varName := "componentLocalEnable."&f_EPTF_Base_selfName()&"."&vl_compTypeName&"."&vl_selection;
  f_EPTF_Var_updateRef(f_EPTF_Var_getId(vl_varName),f_EPTF_Var_getBoolRef(v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].componentLocalEnable));


  for(var integer i:=0; i<sizeof(v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].eventClassNames); i:=i+1) {

    //log("EPTF_componentLocalMask: ",v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].componentLocalMask[i]);

    vl_varName := "componentLocalMask."&int2str(i)&f_EPTF_Base_selfName()&"."&vl_compTypeName&"."&vl_selection;
    f_EPTF_Var_updateRef(f_EPTF_Var_getId(vl_varName),f_EPTF_Var_getBoolRef(v_EPTF_Logging_ComponentMask_List[pl_logMaskIdx].componentLocalMask[i]));
  }

  } // for pl_logMaskIdx

  //f_EPTF_Var_CT_LogAll("Updated f_EPTF_LoggingUIClient_refreshAllRefs");
}


//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_refreshVar
// 
//  Purpose:
//    Callback function for Logging_CT. Refreshes a component variable value from the EPTF Variable.
// 
//  Parameters:
//    pl_logVar - *inout* *boolean* - the reference of the component variable
//
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
// 
///////////////////////////////////////////////////////////

private function f_EPTF_LoggingUI_refreshVar(
  inout boolean pl_logVar) runs on EPTF_Var_CT {

  if (f_EPTF_Var_boolCompRef2Id(pl_logVar)!=-1) {
    f_EPTF_Var_adjustContent(f_EPTF_Var_boolCompRef2Id(pl_logVar),{boolVal:=pl_logVar});
  } else {
    f_EPTF_Common_user(log2str("f_EPTF_Var_boolCompRef2Id(pl_logVar): ",f_EPTF_Var_boolCompRef2Id(pl_logVar)));
  }
}

} // group Private

}  // end of module
