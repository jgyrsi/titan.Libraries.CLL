///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_LoggingUI_Functions
// 
//  Purpose:
//    Creates and manages EPTF_Variables and GUI elements for the main LoggingUI logging parameters.
// 
//  Module depends on:
//    <EPTF_CLL_Common_Functions>
//    <EPTF_CLL_LoggingUI_Definitions>
//    <EPTF_CLL_LoggingUI_PrivateFunctions>
//    <EPTF_CLL_Logging_Definitions>
//    <EPTF_CLL_Logging_Functions>
//    <EPTF_CLL_Variable_Functions>
//    <EPTF_CLL_Base_Functions>
//    <EPTF_CLL_UIHandlerClient_Functions>
//    <EPTF_CLL_UIHandler_Definitions>
//    <XTDP_PDU_Defs language>
//    <EPTF_CLL_HashMapStr2Int_Functions>
//    <EPTF_CLL_HashMap_Functions>
//
//  Current Owner:
//    Bence Molnar (EBENMOL)
// 
//  Last Review Date:
//    2008-01-15
//
//  Detailed Comments:
//    Creates and manages EPTF_Variables and GUI elements for the main LoggingUI logging parameters. 
//
//    <f_EPTF_LoggingUI_init_CT> - Function to initialize main loggingUI component. 
//
//    <f_EPTF_LoggingUI_cleanup_CT> - Function to shutdown main loggingUI.
//
//    <f_EPTF_LoggingUI_main_CT> - Main function to run main LoggingUI component.. 
//
//    <f_EPTF_LoggingUI_createGlobalGui> - Function to create the main window for the boolean values.
//
//    <f_EPTF_LoggingUI_GlobalBindToEPTFVar> - Binds the global boolean parameters to EPTF_variables. 
//
//    <f_EPTF_LoggingUI_GlobalBindToGui> - Function to bind global EPTF Variables to Gui elements.
//
//    <f_EPTF_LoggingUI_createCompTypeGui> - Function to create the component type Gui elements for the boolean values. 
//
//    <f_EPTF_LoggingUI_CompTypeBindToEPTFVar> - Binds the component type boolean parameters to EPTF_variables.
//
//    <f_EPTF_LoggingUI_CompTypeBindToGui> - Function to bind component type EPTF Variables to Gui.
//
///////////////////////////////////////////////////////////

module EPTF_CLL_LoggingUI_Functions

{
import from EPTF_CLL_Common_Functions all;
// if you use EPTF_LoggingUI_CT you have to import these
import from EPTF_CLL_LoggingUI_Definitions all;
import from EPTF_CLL_LoggingUI_PrivateFunctions all
// if you use EPTF_Logging_CT you have to import these
import from EPTF_CLL_Logging_Definitions all;
import from EPTF_CLL_Logging_Functions all;
// if you use EPTF_Var_CT you have to import these
import from EPTF_CLL_Variable_Functions all;
import from EPTF_CLL_Base_Functions all;
// if you use UIHandler&XUL you have to import these
import from EPTF_CLL_UIHandlerClient_Functions all;
import from EPTF_CLL_UIHandler_Definitions all;
// if you use string to integer HashMaps you have to import these
import from EPTF_CLL_HashMapStr2Int_Functions all;

import from EPTF_CLL_HashMap_Functions all;

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_init_CT
// 
//  Purpose:
//    Function to initialize main LoggingUI component
// 
//  Parameters:
//    pl_selfName - *in* *charstring* - name of the main LoggingUI component
//    pl_subscriber - *in* <EPTF_UIHandler_CT> - UIHandler component
//    pl_tabboxName - *in* *charstring* - name of the tabbox where the LoggingUI has to be created
//
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
//    The function initializes main LoggingUI component. Initializes Logging_CT, EPTF_Var_CT, UIHandlerClient_CT and
//    creates a HashMap to store the types of logged components.
//    After initialization f_EPTF_LoggingUI_main_CT() function called.
//
///////////////////////////////////////////////////////////

public function f_EPTF_LoggingUI_init_CT(
  in charstring pl_selfName, 
  in EPTF_UIHandler_CT pl_subscriber,
  in charstring pl_tabboxName) runs on EPTF_LoggingUI_CT {

  if (v_EPTF_LoggingUI_initialized) {return;}

  //log("----- LoggingUI init START -------");
  v_EPTF_LoggingUI_tabboxName:=pl_tabboxName;

  f_EPTF_Logging_init_CT(pl_selfName);
  f_EPTF_HashMap_init_CT (pl_selfName);


  //f_EPTF_Var_CT_LogAll("----- EPTF VAR INIT START -------");//log
  f_EPTF_Var_init_CT(pl_selfName);
  //f_EPTF_Var_CT_LogAll("----- EPTF VAR INIT READY -------");//log

  f_EPTF_UIHandlerClient_init_CT(pl_selfName, pl_subscriber);

  var boolean vl_tabboxExists:=true;
  if(f_EPTF_UIHandlerClient_widgetExists(v_EPTF_LoggingUI_tabboxName)==wtUnknown)
  {
    vl_tabboxExists:=false;
  }  
  f_EPTF_Base_assert("Wrong tabbox name.",vl_tabboxExists);

  //f_EPTF_LoggingUI_createGlobalGui()
  //f_EPTF_LoggingUI_WriteOut();
  f_EPTF_LoggingUI_GlobalBindToEPTFVar();
  f_EPTF_LoggingUI_GlobalBindToGui();

  
  v_EPTF_LoggingUI_typesLookupTableName:= c_EPTF_LoggingUI_typesLookupTable_HashName;
  v_EPTF_LoggingUI_typesLookupTableID:=f_EPTF_str2int_HashMap_New (v_EPTF_LoggingUI_typesLookupTableName); 
  v_EPTF_LoggingUI_typesID:=0;

  v_EPTF_LoggingUI_initialized := true;
  v_EPTF_LoggingUI_numberOfClients:=0;

  f_EPTF_Base_registerCleanup(refers(f_EPTF_LoggingUI_cleanup_CT));
  //log("----- LoggingUI init END -------");

  v_EPTF_LoggingUI_def:=activate(as_EPTF_LoggingUI_main_CT());
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// private:
//////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Group: Private
//
// Purpose:
//   Private functions. These functions must not be used by the user of <EPTF_LoggingUI_CT>
//
// Elements:
///////////////////////////////////////////////////////////
group Private {

//////////////////////////////////////////////////////////
//  Altstep: as_EPTF_LoggingUI_main_CT
// 
//  Purpose:
//    The main altstep to run main LoggingUI component.
// 
//  Parameters:
//    -
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
//    Waits for <EPTF_LoggingUI_Msg> messages. If a Request arrives, checks whether the arrived component type exists.
//    If component type not exists creates the component mask to GUI, else do nothing. Sends a response message back.
//    Main LoggingUI sends this response message only after the GUI window completed.
// 
///////////////////////////////////////////////////////////

private altstep as_EPTF_LoggingUI_main_CT() runs on EPTF_LoggingUI_CT {

  //log("***********f_EPTF_LoggingUI_main***************");
  var EPTF_LoggingUIClient_CT v_send;
  var EPTF_LoggingUI_Msg vl_req;

    [v_EPTF_LoggingUI_allowReq] EPTF_LoggingUIPort_CP.receive(tr_EPTF_LoggingUI_req) -> value vl_req sender v_send
      {
        var integer vl_compTypeID;


          //find if ID not exist
        if(not f_EPTF_str2int_HashMap_Find(v_EPTF_LoggingUI_typesLookupTableID,vl_req.LoggingUI_Req.compType,vl_compTypeID))
        {
          v_EPTF_LoggingUI_allowReq := false;
          v_EPTF_LoggingUI_typesID:=v_EPTF_LoggingUI_typesID+1;
          f_EPTF_str2int_HashMap_Insert(v_EPTF_LoggingUI_typesLookupTableID,vl_req.LoggingUI_Req.compType,v_EPTF_LoggingUI_typesID);
          //f_EPTF_LoggingUI_createCompTypeGui(vl_req.LoggingUI_Req.compName,vl_req.LoggingUI_Req.compType,vl_req.LoggingUI_Req.selection,vl_req.LoggingUI_Req.eventClassNames);
          f_EPTF_LoggingUI_CompTypeBindToEPTFVar(vl_req.LoggingUI_Req.compType,v_EPTF_LoggingUI_typesID,vl_req.LoggingUI_Req.selection,vl_req.LoggingUI_Req.eventClassNames);
          f_EPTF_LoggingUI_CompTypeBindToGui(vl_req.LoggingUI_Req.compType,v_EPTF_LoggingUI_typesID,vl_req.LoggingUI_Req.selection,vl_req.LoggingUI_Req.eventClassNames);
          v_EPTF_LoggingUI_allowReq := true;

          // to check redefinition:
          f_EPTF_str2int_HashMap_Insert(v_EPTF_LoggingUI_typesLookupTableID,
            vl_req.LoggingUI_Req.compType&log2str(vl_req.LoggingUI_Req.selection,"/",vl_req.LoggingUI_Req.eventClassNames),
            0);
        } else {
          // check redefinitions:
          if(not f_EPTF_str2int_HashMap_Find(v_EPTF_LoggingUI_typesLookupTableID,
            vl_req.LoggingUI_Req.compType&log2str(vl_req.LoggingUI_Req.selection,"/",vl_req.LoggingUI_Req.eventClassNames),
            vl_compTypeID)) {
            f_EPTF_Logging_error(true,"Inconsistent eventClassNames found. Event classes for this component type were already defined. Event class names are redefined on component "&
              vl_req.LoggingUI_Req.compName&" with compType: "&vl_req.LoggingUI_Req.compType&", selection: "&log2str(vl_req.LoggingUI_Req.selection)&
              ", eventClassNames: "&log2str(vl_req.LoggingUI_Req.eventClassNames));
            f_EPTF_Base_stopAll();
          }
        }
        EPTF_LoggingUIPort_CP.send(c_EPTF_LoggingUI_resp) to v_send;
        repeat;
      }
    [] EPTF_LoggingUIPort_CP.receive(c_EPTF_LoggingUI_hello) -> value vl_req sender v_send
      {
        v_EPTF_LoggingUI_numberOfClients:=v_EPTF_LoggingUI_numberOfClients+1;
        f_EPTF_Logging_debug(true,log2str("v_EPTF_LoggingUI_numberOfClients:",v_EPTF_LoggingUI_numberOfClients));
        repeat;
      }
    [] EPTF_LoggingUIPort_CP.receive(c_EPTF_LoggingUI_bye) -> value vl_req sender v_send
      {
        v_EPTF_LoggingUI_numberOfClients:=v_EPTF_LoggingUI_numberOfClients-1;
        f_EPTF_Logging_debug(true,log2str("v_EPTF_LoggingUI_numberOfClients:",v_EPTF_LoggingUI_numberOfClients));
        repeat;
      }

}
 
//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_cleanup_CT
// 
//  Purpose:
//    Function to shutdown main loggingUI component
// 
//  Parameters:
//    -
//
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
//    -
// 
///////////////////////////////////////////////////////////

private function f_EPTF_LoggingUI_cleanup_CT() runs on EPTF_LoggingUI_CT {
  //log("----- LoggingUI cleanup -------");
  if (v_EPTF_LoggingUI_initialized == false) {
    return;
  }  
  
  if (v_EPTF_LoggingUI_typesLookupTableID != -1) {
    f_EPTF_str2int_HashMap_Delete(c_EPTF_LoggingUI_typesLookupTable_HashName);
    v_EPTF_LoggingUI_typesLookupTableID := -1;
  }
  
  var EPTF_LoggingUIClient_CT v_send;
  v_EPTF_LoggingUI_initialized := false;
  if (v_EPTF_LoggingUI_numberOfClients==0) {
    return;
  }
  // wait for missing bye messages
  timer t_max_wait := 60.0; // to prevent deadlock
  t_max_wait.start;
  alt {
    [] t_max_wait.timeout {
        f_EPTF_Common_warning(log2str(%definitionId&": WARNING: MaxWaitTime expired. Not all pending bye messages received in time (still pending: ",v_EPTF_LoggingUI_numberOfClients,"). Exiting anyway..."));
    }
    [] EPTF_LoggingUIPort_CP.receive(c_EPTF_LoggingUI_bye) -> sender v_send
      {
        v_EPTF_LoggingUI_numberOfClients:=v_EPTF_LoggingUI_numberOfClients-1;
        f_EPTF_Logging_debug(true,log2str("v_EPTF_LoggingUI_numberOfClients:",v_EPTF_LoggingUI_numberOfClients));
        if (v_EPTF_LoggingUI_numberOfClients>0) {
          repeat;
        }
        log("LoggingUI: all clients exited");
      }
    [] EPTF_LoggingUIPort_CP.receive(?) -> sender v_send 
      {
        f_EPTF_Common_warning(log2str(%definitionId&": WARNING: Unexpected message received from ",v_send," during cleanup. Message dropped"));
        repeat;
      }
  }  
}

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_GlobalBindToEPTFVar
// 
//  Purpose:
//    Binds the global boolean parameters to EPTF_variables
// 
//  Parameters:
//    -
//
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
//    Binds the boolean parameter <v_EPTF_Logging_allComponentGlobalEnable> to EPTF_variable. 
// 
///////////////////////////////////////////////////////////

private function f_EPTF_LoggingUI_GlobalBindToEPTFVar() runs on EPTF_LoggingUI_CT {

  //log("***********f_EPTF_LoggingUI_GlobalBindToEPTFVar***************");

    var charstring boolVarRef;

    // Binding: v_EPTF_LoggerDataList[j].EPTF_globalLogIsEnabled 
  boolVarRef := "allComponentGlobalEnable";
  f_EPTF_Var_newBoolRef(boolVarRef,v_EPTF_Logging_allComponentGlobalEnable,v_EPTF_LoggingUI_allComponentGlobalEnableIdx);

}

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_GlobalBindToGui
// 
//  Purpose:
//    Function to bind global EPTF Variables to Gui elements
// 
//  Parameters:
//    -
//
//  Return Value:
//    -
// 
//  Errors:
//  - (none) 
// 
//  Detailed Comments:
//    Binds the boolean parameter <v_EPTF_Logging_allComponentGlobalEnable> and its EPTF_variable to the corresponding checkbox.
// 
///////////////////////////////////////////////////////////

private function f_EPTF_LoggingUI_GlobalBindToGui() runs on EPTF_LoggingUI_CT {

  //log("***********f_EPTF_LoggingUI_GlobalBindToGui***************");

  var charstring boolVarName,boolVarName2,boolVarName3;

  boolVarName := f_EPTF_Var_getName(v_EPTF_LoggingUI_allComponentGlobalEnableIdx);

  boolVarName2 := "subscriber_allComponentGlobalEnable"; 

  //boolVarName3 := "MainAdmin_CheckBoxes"&int2str(j)&".0.1"; 
  boolVarName3 :="Global_Enable";

  f_EPTF_UIHandlerClient_subscribeMe(boolVarName, boolVarName2, boolVarName3); 

}     

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_CompTypeBindToEPTFVar
// 
//  Purpose:
//    Binds the component type boolean parameters to EPTF_variables
// 
//  Parameters:
//    pl_compType - *in* *charstring* - name of the component type
//    pl_compTypeID - *in* *integer* - the ID of the component type
//    pl_selection - *in* *integer* - CORE/USER
//    pl_eventClassNames - *in* <EPTF_Logging_EventClassPrefixList> - logmasks names defined for this component type
//
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
//    Binds the boolean parameters to EPTF_variables.
// 
///////////////////////////////////////////////////////////

function f_EPTF_LoggingUI_CompTypeBindToEPTFVar(
  in charstring pl_compType,
  in integer pl_compTypeID,
  in integer pl_selection,
  in EPTF_Logging_EventClassPrefixList pl_eventClassNames) runs on EPTF_LoggingUI_CT {

  //log("***********f_EPTF_LoggingUI_CompTypeBindToEPTFVar***************");

  var charstring boolVarRef;
  var charstring vl_selection:=f_EPTF_LoggingUI_Selection2Str(pl_selection);

  // mask bindings
  boolVarRef := "EPTF_LoggingUI.componentGlobalEnable."&pl_compType&"."&vl_selection;

  f_EPTF_Var_newBool(boolVarRef, true, v_EPTF_LoggingUI_componentGlobalEnableIdx[pl_compTypeID][pl_selection]);

  for(var integer i:=0; i<sizeof(pl_eventClassNames); i:=i+1) {

    boolVarRef := "EPTF_LoggingUI.componentGlobalMask."&int2str(i)&"."&pl_compType&"."&vl_selection;
    f_EPTF_Var_newBool(boolVarRef, true, v_EPTF_LoggingUI_componentGlobalMaskIdx[pl_compTypeID][pl_selection][i]);
  }

}

//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_CompTypeBindToGui
// 
//  Purpose:
//    Function to bind component type EPTF Variables to Gui
// 
//  Parameters:
//    pl_compType - *in* *charstring* - name of the component type
//    pl_compTypeID - *in* *integer* - the ID of the component type
//    pl_selection - *in* *integer* - CORE/USER
//    pl_eventClassNames - *in* <EPTF_Logging_EventClassPrefixList> - logmasks names defined for this component typ
//
//  Return Value:
//    -
// 
//  Errors:
//  - (none) 
// 
//  Detailed Comments:
//    -
// 
///////////////////////////////////////////////////////////

function f_EPTF_LoggingUI_CompTypeBindToGui(
  in charstring pl_compType,
  in integer pl_compTypeID,
  in integer pl_selection,
  in EPTF_Logging_EventClassPrefixList pl_eventClassNames) runs on EPTF_LoggingUI_CT {

  //log("***********f_EPTF_LoggingUI_CompTypeBindToGui***************");
  var charstring boolVarName,boolVarName2,boolVarName3; 
  var charstring vl_selection:=f_EPTF_LoggingUI_Selection2Str(pl_selection);
  
  // mask bindings
  boolVarName := f_EPTF_Var_getName(v_EPTF_LoggingUI_componentGlobalEnableIdx[pl_compTypeID][pl_selection]);
  boolVarName2 := pl_compType&"."&vl_selection&".subscriber_componentLogIsEnabled";
  boolVarName3 := pl_compType&"."&vl_selection&".CompType_CheckBox_Enable";
  f_EPTF_UIHandlerClient_subscribeMe(boolVarName, boolVarName2, boolVarName3); 

  for(var integer i:=0; i<sizeof(pl_eventClassNames); i:=i+1) {

    boolVarName := f_EPTF_Var_getName(v_EPTF_LoggingUI_componentGlobalMaskIdx[pl_compTypeID][pl_selection][i]);
    boolVarName2 := pl_compType&"."&vl_selection&"."&int2str(i)&".subscriber.componentLogMaskList";
    boolVarName3 := pl_compType&"."&vl_selection&".CompType_CheckBox_Mask"&int2str(i);
    f_EPTF_UIHandlerClient_subscribeMe(boolVarName, boolVarName2, boolVarName3);
  }

} 
  
//////////////////////////////////////////////////////////
//  Function: f_EPTF_LoggingUI_WriteOut
// 
//  Purpose:
//    Writes out the boolean parameter values
// 
//  Parameters:
//    -
//  Return Value:
//    -
// 
//  Errors:
//    - (none) 
// 
//  Detailed Comments:
//    Writes out the boolean parameter values. The boolean variables are called with their non EPTF variables names.
//    We use this function for debugging only.
// 
///////////////////////////////////////////////////////////

function f_EPTF_LoggingUI_WriteOut() runs on EPTF_LoggingUI_CT {

  f_EPTF_Common_user("***********f_EPTF_LoggingUI_WriteOut***************");
  f_EPTF_Logging_logAll();

}

} // group Private

} //end of module
