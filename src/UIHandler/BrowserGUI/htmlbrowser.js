var dummy_fn = function dummy() {};
var console = console || window.console || { // Semi-polyfill for old IE, FF versions.
  'log' : dummy_fn,
  'warn' : dummy_fn,
  'trace' : dummy_fn,
  'error' : function (s) {
    alert(s);
  }
};
// Chart databases
var g_$chart = {}; // g_$chart holds reference to all initialized charts on the page.
var chart_line_series = {};
var chart_bar_series = {};

var flot_base_cfg = {
  "legend" : {
    "hideable" : true,
    "margin" : [2, 20]
  },
  "yaxis" : {
    "tickDecimals" : 0,
  },
  "grid" : {
    "autoHighlight" : true,
    "hoverable" : true
  },
  "zoom" : {
    "interactive" : true
  },
  "pan" : {
    "interactive" : true
  }
};

// Use the source, Luke: http://www.flotcharts.org/flot/examples/series-types/index.html
var g_flot_line_cfg = $.extend({
  "lines" : {
    "show" : true
  },
  "points" : {
    "show" : false
  },
  "xaxis" : {
    "mode" : "time"
  }
}, flot_base_cfg);

// Use the source, Luke: http://www.flotcharts.org/flot/examples/series-types/index.html
var g_flot_bar_cfg = $.extend({
  "series" : {
    "bars" : {
      "show" : true,
      "align" : "center",
      "barWidth" : 0.5
    },
    "points" : {
      "show" : true
    }
  },
  "xaxis" : {
    "tickDecimals" : 0
  },
}, flot_base_cfg);

var chart_trace_ids = [];
var charts_initialized = [];
var traces = [];
var barChartIds = [];
var barChartValues = [];
var barChartValues_changed = [];
// Array of default colors' hex codes: hot_pink, firebrick3, green, purple
// light_blue, gold, light_blue3, medium_purple, light_green
// colors added from Ericsson palette also
var defaultLineColor = [
  '#e2001c', '#f08a00', '#601f70', '#246ab3', '#009f80', '#0082b9', '#a1c517', '#3Ca329',
  '#F660AB', '#f4a8a6', '#80699B', '#88c6d5', '#e4d047', '#92A8CD', '#A47D7C', '#B5CA92'
];

var a_element;
var a_element_orig_href;

var errorMsgElement;
var lastErrorType = "";
var lastErrorMsg = "";
var haveErrorMsg = false;
var disabledIDs = [];

var requestSuccess = 0; // no cache counter for cross browser
var updateSuccess = 0; // no cache counter for cross browser
var refreshProcessID = null;
var refreshInterval = 2500; // refresh interval
var refreshIntervalRetFromError = 1000; // refresh interval if returns from error
var crosbrowserProcessID = null;
var crosbrowserInterval = 200; // cross browser interval
var reloadProcessID = null;
var reloadInterval = 100; // browser reload interval
var onAfterRefreshDelegates = []; // Replacement of the eval(callsAfterRefresh) mechanism.
var debugEnabled = false;
var debugInOut = true;
var debugGlobalStr = "";
var refresPageWidth = -1; // width on refresh
var ajaxRefreshRequestCache = [];
var ajaxRefreshRequestNow;
var inPrintProcess = false;

var plotOverlayHandler = function plot_overlay_handler(plot, cvs) {
  if (!plot) {
    return; /* No chart no action */
  }
  cvs.font = "bold 16px Open Sans";
  cvs.fillStyle = "#555555"; // Pretty darkish grey
  cvs.textAlign = 'center';
  // Flot: put Chart Title there. (text, X, Y)
  cvs.fillText(plot.getOptions().title, cvs.canvas.width / 2, 20);
  return cvs;
};

/* Special scroll events for jQuery */
(function (jQuery) {
  "use strict";
  var special = jQuery.event.special,
  uid1 = 'D' + (+new Date()),
  uid2 = 'D' + (+new Date() + 1);

  special.scrollstart = {
    setup : function () {
      var timer,
      handler = function (evt) {

        var _self = this,
        _args = arguments;

        if (timer) {
          clearTimeout(timer);
        } else {
          evt.type = 'scrollstart';
          jQuery.event.dispatch.apply(_self, _args);
        }

        timer = setTimeout(function anon_timeout_scrollstart() {
            timer = null;
          }, special.scrollstop.latency);

      };

      jQuery(this).bind('scroll', handler).data(uid1, handler);

    },
    teardown : function () {
      jQuery(this).unbind('scroll', jQuery(this).data(uid1));
    }
  };

  special.scrollstop = {
    latency : 300,
    setup : function () {

      var timer,
      handler = function (evt) {

        var _self = this,
        _args = arguments;

        if (timer) {
          clearTimeout(timer);
        }

        timer = setTimeout(function anon_timeout_scrollstop() {

            timer = null;
            evt.type = 'scrollstop';
            jQuery.event.dispatch.apply(_self, _args);

          }, special.scrollstop.latency);

      };

      jQuery(this).bind('scroll', handler).data(uid2, handler);

    },
    teardown : function () {
      jQuery(this).unbind('scroll', jQuery(this).data(uid2));
    }
  };
})(jQuery);

window.onbeforeprint = beforePrint;

$(window).resize(function onresize_window() {
  "use strict";
  clearCache(false);
});

(function (jQuery) {
  "use strict";
  jQuery(window).bind('scrollstop', function ononscrollstop_window() {
    clearCache(false);
  });
})(jQuery);

// Automatic refresh method after the page is ready
$(document).ready(function ondocumentready() {
  "use strict";

  // Only jQuery is cross-browser enough.
  createErrorMsgPanel();
  updatePrintSubtitle();
  updateRefreshPageWidth();

  updateVersions(""); // Update versions
  clearCache(false);
  // Set refresh time period(refreshInterval: now 2500ms) and operations
  refreshProcessID = setInterval(processRefresh, refreshInterval);

  var maxBottomHeight = 0;
  var maxTopHeight = 0;
  var i,
  j;
  var visibleDivBottomRightElements,
  visibleDivBottomRightElementsSize;
  var visibleDivBottomLeftElements,
  visibleDivBottomLeftElementsSize;
  var visibleDivTopRightElements,
  visibleDivTopRightElementsSize;
  var visibleDivTopLeftElements,
  visibleDivTopLeftElementsSize;

  if (document.getElementsByClassName) {
    visibleDivBottomRightElements = document.getElementsByClassName('fixed_box_bottom_right');
    visibleDivBottomRightElementsSize = visibleDivBottomRightElements.length;

    for (j = 0; j < visibleDivBottomRightElementsSize; j += 1) {
      i = visibleDivBottomRightElements[j].clientHeight;
      if (maxBottomHeight < i) {
        maxBottomHeight = i;
      }
    }

    visibleDivBottomLeftElements = document.getElementsByClassName('fixed_box_bottom_left');
    visibleDivBottomLeftElementsSize = visibleDivBottomLeftElements.length;

    for (j = 0; j < visibleDivBottomLeftElementsSize; j += 1) {
      i = visibleDivBottomLeftElements[j].clientHeight;
      if (maxBottomHeight < i) {
        maxBottomHeight = i;
      }
    }

    visibleDivTopRightElements = document.getElementsByClassName('fixed_box_top_rigt');
    visibleDivTopRightElementsSize = visibleDivTopRightElements.length;

    for (j = 0; j < visibleDivTopRightElementsSize; j += 1) {
      i = visibleDivTopRightElements[j].clientHeight;
      if (maxTopHeight < i) {
        maxTopHeight = i;
      }
    }

    visibleDivTopLeftElements = document.getElementsByClassName('fixed_box_top_left');
    visibleDivTopLeftElementsSize = visibleDivTopLeftElements.length;

    for (j = 0; j < visibleDivTopLeftElementsSize; j += 1) {
      i = visibleDivTopLeftElements[j].clientHeight;
      if (maxTopHeight < i) {
        maxTopHeight = i;
      }
    }
  } else {
    i = 0;
    var a = document.getElementsByTagName("div");
    var element = a[i++];
    while (element) {
      if (0 <= element.className.indexOf("fixed_box_bottom_right")) {
        visibleDivBottomRightElements = element;
        visibleDivBottomRightElementsSize = visibleDivBottomRightElements.length;

        for (j = 0; j < visibleDivBottomRightElementsSize; j += 1) {
          i = visibleDivBottomRightElements[j].clientHeight;
          if (maxBottomHeight < i) {
            maxBottomHeight = i;
          }
        }
      } else if (0 <= element.className.indexOf("fixed_box_bottom_left")) {
        visibleDivBottomLeftElements = element;
        visibleDivBottomLeftElementsSize = visibleDivBottomLeftElements.length;

        for (j = 0; j < visibleDivBottomLeftElementsSize; j += 1) {
          i = visibleDivBottomLeftElements[j].clientHeight;
          if (maxBottomHeight < i) {
            maxBottomHeight = i;
          }
        }
      } else if (0 <= element.className.indexOf("fixed_box_top_rigt")) {
        visibleDivTopRightElements = element;
        visibleDivTopRightElementsSize = visibleDivTopRightElements.length;

        for (j = 0; j < visibleDivTopRightElementsSize; j += 1) {
          i = visibleDivTopRightElements[j].clientHeight;
          if (maxTopHeight < i) {
            maxTopHeight = i;
          }
        }
      } else if (0 <= element.className.indexOf("fixed_box_top_left")) {
        visibleDivTopLeftElements = element;
        visibleDivTopLeftElementsSize = visibleDivTopLeftElements.length;

        for (j = 0; j < visibleDivTopLeftElementsSize; j += 1) {
          i = visibleDivTopLeftElements[j].clientHeight;
          if (maxTopHeight < i) {
            maxTopHeight = i;
          }
        }
      }
      element = a[i++];
    }
  }

  if (maxBottomHeight > 0) {
    document.getElementById("footer_placeholder").height = maxBottomHeight;
  }
  if (maxTopHeight > 0) {
    document.getElementById("header_placeholder").height = maxTopHeight;
  }

  // Value update method (updating a text field, checkbox)
  $(document).on('change', 'input', function onchange_any_input() {
    //converting html value to ttcn
    var convertedValue = convertElement(this.id, this.value);
    sendAjaxUpdateRequest(this.id, convertedValue);
    return false;
  });

  // Value update method (updating a menulist)
  $(document).on('change', 'select', function onchange_any_select() {
    // Converting html value to ttcn
    var convertedValue = convertElement(this.id, this.value);
    sendAjaxUpdateRequest(this.id, convertedValue);
    return false;
  });

  // Button click update
  $(document).on('click', 'button', function onclick_any_button() {
    var convertedValue = "";
    sendAjaxUpdateRequest(this.name, convertedValue);
    return false;
  });

  // If the document is clicked somewhere
  $(document).on("mousedown click", function onmousedown_to_hide_contextmenu(e) {
    // If the clicked element is not the menu
    if ($(e.target).parents("DIV.custom-context-menu").length <= 0) {
      // Hide it
      $(".custom-context-menu").remove();
    }
    return true; // let it bubble through
  });

  // Show the context menu
  $(document).on("contextmenu", '.hbox_chart, .hbox_distributionchart', function oncontextmenu_show(e) {
    e.preventDefault();
    $("<div class='custom-context-menu'><ul><li data-action='chart_save'>Save Chart as Image</li><li data-action='chart_reset'>Reset Chart</li></ul></div>")
    .appendTo("body")
    .css({
      top : e.pageY + "px",
      left : e.pageX + "px"
    })
    .attr('data-target', e.currentTarget.id);
    return false;
  });

  // If the menu element is clicked
  $(document).on('click', 'DIV.custom-context-menu LI', function oncontextmenu_item_click(e) {
    e.preventDefault();
    var $this = $(this);
    var dataTarget = $this.parent().parent().attr("data-target");
    var chartElementId;
    var chartElementDrawId = document.getElementById(dataTarget).getAttribute('id');

    if (chartElementDrawId.indexOf('_draw', chartElementDrawId.length - 5) !== -1) {
      chartElementId = chartElementDrawId.slice(0, -5);
    }

    // This is the triggered action name
    switch ($this.attr("data-action")) {
      // A case for each action. Your actions here
    case "chart_save":
      html2canvas($(document.getElementById(chartElementDrawId)), {
        "onrendered" : function onrendered_h2c(canvas) {
          var link = document.createElementNS("http://www.w3.org/1999/xhtml", "a");
          var parent = $(document.getElementById(chartElementDrawId)).get(0);
          link.setAttribute('download', 'Chart_' + (new Date()).toISOString() + '.png');
          link.setAttribute('target', '_blank');
          link.setAttribute('href', canvas.toDataURL('image/png'));
          parent.appendChild(link);
          link.click();
          parent.removeChild(link);
          if (parent.scrollIntoViewIfNeeded) {
            parent.scrollIntoViewIfNeeded(true); // cx only
          } else {
            parent.scrollIntoView(true); // align top
          }
        }
      });
      break;
    case "chart_reset":
      var plot = g_$chart[chartElementId];
      if (plot) {
        var axes = plot.getAxes(),
        xaxis = axes.xaxis.options,
        yaxis = axes.yaxis.options;
        xaxis.min = null;
        xaxis.max = null;
        yaxis.min = null;
        yaxis.max = null;

        // Don't forget to redraw the plot
        repaintChart(chartElementId);
      }
      break;
    }
    // Hide it AFTER the action was triggered
    $("DIV.custom-context-menu").remove();
    return false;
  });
});

// Page reload function
function reloadPage() {
  "use strict";
  clearCache(true);
  if (reloadProcessID) {
    clearInterval(reloadProcessID);
    reloadProcessID = null;
  }
  reloadProcessID = setTimeout(function ontimeout_reloadPage() {
      window.location.reload(true);
    }, reloadInterval);
  return true;
}

// Before Print
function beforePrint() {
  "use strict";
  inPrintProcess = true;
  // Clear refresh time
  if (refreshProcessID) {
    clearInterval(refreshProcessID);
    refreshProcessID = null;
  }
  drawCharts(true);
  processRefresh();
  inPrintProcess = false;
  // Set refresh time period(refreshInterval: now 2500ms) and operations
  if (refreshProcessID === null) {
    refreshProcessID = setInterval(processRefresh, refreshInterval);
  }
  return true;
}

// Main refresh process
function processRefresh() {
  "use strict";
  onAfterRefreshDelegates = []; // refreshElement() is gonna fill it anyway.
  // Create the ajax refresh string in json format for refreshing
  // the elements of the form that the page contains
  var ajaxRefreshRequest = "ajaxrefresh_";
  ajaxRefreshRequest += requestSuccess + (new Date()).getTime();

  var i = 0,
  j;
  var escapedId = "";
  var debug_processRefresh = "";
  var dateNow = new Date();
  var timeStart = dateNow.getTime();
  var timeRequest = dateNow.getTime();
  var timeUpdate = dateNow.getTime();
  var numberOfRequested = 0;
  // Number of forms
  var numberOfForms = document.forms.length;
  var element;
  var formElementsNum;
  var ajaxRefreshRequestInPrint;

  if (refreshProcessID) {
    clearInterval(refreshProcessID);
    refreshProcessID = null;
  }
  for (j = 0; j < numberOfForms; j += 1) {
    // Number of elements on the form
    formElementsNum = document.forms[j].elements.length;

    if (haveErrorMsg) {
      // If Error pending
      ajaxRefreshRequest += '{"ajaxReloadVersionCount"' + ":" + '\n' + '"' + '"}';
    } else if (inPrintProcess) {
      // Before printing, request all data
      ajaxRefreshRequestInPrint = "{";
      debug_processRefresh += "INPRINT!";
      for (i = 0; i < formElementsNum; i += 1) {
        element = document.forms[j].elements[i];
        if (element.id) {
          // Escape the special characters from the element ids (",/,\)
          escapedId = escapeElement(element.id);
          // Get the values of only the visible elements.
          if ($($(element).parent()).prop('class') === "chart") {
            ajaxRefreshRequestInPrint += '"' + escapedId + '"' + ":" + '\n' + '"chartInit",';
            numberOfRequested++;
          } else
            if ($($(element).parent()).prop('class') === "chart_ready" ||
              $($(element).parent()).prop('class') === "distributionchart_ready" ||
              $($(element).parent()).prop('class') === "distributionchart") {
              ajaxRefreshRequestInPrint += '"' + escapedId + '"' + ":" + '\n' + '"' + '",';
              numberOfRequested++;
            } else {
              ajaxRefreshRequestInPrint += '"' + escapedId + '"' + ":" + '\n' + '"' + '",';
              numberOfRequested++;
            }
        } // endif
      } // endfor
      ajaxRefreshRequestInPrint += "}";
      ajaxRefreshRequestInPrint = ajaxRefreshRequestInPrint.replace(",}", "}");
      ajaxRefreshRequest += ajaxRefreshRequestInPrint;
    } else if (j < ajaxRefreshRequestCache.length && 0 < ajaxRefreshRequestCache[j].length) {
      debug_processRefresh += "CACHE!";
      ajaxRefreshRequest += ajaxRefreshRequestCache[j];
    } else {
      // Build up the cache string
      ajaxRefreshRequestCache[j] = "{";
      ajaxRefreshRequestNow = "{";
      // Creating the json string from the ids of the form elements, where
      // the format of a json string is: {"element id":""}
      debug_processRefresh += "NEWREFRESH!";
      var docViewTop = $(window).scrollTop();
      var docViewBottom = docViewTop + $(window).height();
      var docViewLeft = $(window).scrollLeft();
      var docViewRigth = docViewLeft + $(window).width();
      for (i = 0; i < formElementsNum; i += 1) {
        element = document.forms[j].elements[i];
        if (element.id) {
          // Escape the special characters from the element ids (",/,\)
          escapedId = escapeElement(element.id);
          // Get the values of only the visible elements.
          if ($($(element).parent()).prop('class') === "chart" && isScrolledIntoViewWindow(($(element).parent()).prop('id') + "_draw", docViewTop, docViewBottom, docViewLeft, docViewRigth)) {
            ajaxRefreshRequestNow += '"' + escapedId + '"' + ":" + '\n' + '"chartInit",';
            ajaxRefreshRequestCache[j] += '"' + escapedId + '"' + ":" + '\n' + '"' + '",';
            numberOfRequested++;
          } else
            if (isScrolledIntoViewWindow(element.id, docViewTop, docViewBottom, docViewLeft, docViewRigth) || ((($($(element).parent()).prop('class') === "chart_ready") || ($($(element).parent()).prop('class') === "distributionchart_ready") || ($($(element).parent()).prop('class') === "distributionchart")) && isScrolledIntoViewWindow(element.id + "_draw", docViewTop, docViewBottom, docViewLeft, docViewRigth))) {
              ajaxRefreshRequestNow += '"' + escapedId + '"' + ":" + '\n' + '"' + '",';
              ajaxRefreshRequestCache[j] += '"' + escapedId + '"' + ":" + '\n' + '"' + '",';
              numberOfRequested++;
            }
        } // endif
      } // endfor
      ajaxRefreshRequestNow += "}";
      ajaxRefreshRequestCache[j] += "}";
      ajaxRefreshRequestNow = ajaxRefreshRequestNow.replace(",}", "}");
      ajaxRefreshRequestCache[j] = ajaxRefreshRequestCache[j].replace(",}", "}");
      ajaxRefreshRequest += ajaxRefreshRequestNow;
    }

    // Ajax request, where the data contains the json string,
    // which contains the element ids that have to be updated
    dateNow = new Date();
    timeRequest = dateNow.getTime();
    $.ajax({
      url : "",
      type : 'GET',
      data : ajaxRefreshRequest,
      dataType : 'json',
      success : function onAjaxSuccess_in_processRefresh(data) {
        // called when successful
        // For every element of the response the refreshElement
        // funtction has to be called
        // alert("ajaxRefreshRequest -> " + ajaxRefreshRequest);
        // hide error message
        var returnfromerror = hideErrorMsgPanel();
        // connected:
        if (document.title.search(" - Disconnected") !== -1) {
          document.title = document.title.replace(" - Disconnected", "");
        }
        requestSuccess++;
        dateNow = new Date();
        timeUpdate = dateNow.getTime();
        // Process data
        debugGlobalStr = "[processRefresh]:";
        if (data) {
          $.each(data, function anon_each_refr(key, val) {
            refreshElement(key, val);
          });

          drawCharts(false);
        } else {
          setErrorMsg("warning", "data==null!");
          showErrorMsgPanel();
        }
        dateNow = new Date();
        var timeStop = dateNow.getTime();
        debug_processRefresh += " [" + formElementsNum + " | " + numberOfRequested + "]: ";
        debug_processRefresh += "{ BUILD; REQUEST; UPDATE} {" + (timeRequest - timeStart) + "; " + (timeUpdate - timeRequest) + "; " + (timeStop - timeUpdate) + "}->" + ajaxRefreshRequest;
        //                       alert(debug_processRefresh);
        // if have -> call it
        for (var i = 0; i < onAfterRefreshDelegates.length; i++) {
          /* Note: You wonder what the hell is this?
           * This is a replacement for the eval-loop that was previously built as a string by the pioneers of the golden age.
           * Now, it is an array of functions, executed sequentially.
           */
          onAfterRefreshDelegates[i]();
        }
        onAfterRefreshDelegates = [];

        updatePrintSubtitle();
        // If not under printing
        if (!inPrintProcess) {
          // Set refresh time period(refreshInterval: now 2500ms) and operations
          if (refreshProcessID === null) {
            if (returnfromerror) {
              refreshProcessID = setInterval(processRefresh, refreshIntervalRetFromError);
            } else {
              refreshProcessID = setInterval(processRefresh, refreshInterval);
            }
          }
        }
      },
      error : function onerror_ajax_processRefresh(jqXHR, textStatus, errorThrown) {
        console.warn("Refreshing process encountered an error in the $.ajax part. (Might be simply disconnected.) |", textStatus, "|", errorThrown, "|", jqXHR);
        // alert(textStatus+" , "+ errorThrown);
        // disconnected => change title:
        if (document.title.search(" - Disconnected") === -1) {
          document.title = document.title + " - Disconnected";
        }
        // show error message
        setErrorMsgIfNoError("error", "Connection to server lost! - " + textStatus);
        showErrorMsgPanel();
        // Set refresh time period(refreshInterval: now 2500ms) and operations
        if (refreshProcessID === null) {
          refreshProcessID = setInterval(processRefresh, refreshInterval);
        }
      }
    });
  }
}

// Refreshing values on the form in case of the periodic refresh.
// The values that came in the response are compared to the
// actual values and if these two values are equal, than the
// field is not updated to avoid the unnecessary renderings
function refreshElement(elementId, refreshedValue) {
  "use strict";
  var elementNotFound = "";
  var parentNotFound = "";
  var i;
  var widgetIds;
  var chartTabElement;
  var chartTabpagesId;
  var element;
  var str_class;
  var freeze;

  refreshedValue = restoreEscapedString(refreshedValue);
  // Check if a hidden tab refresh is received, by checking the
  // elementId, if it is set to the "ajaxActivate" string

  // The server tells the browser, which tabpage is activated from code,
  // please set the given tabpage active ( download it from server and make it visible)
  // timeElapsed
  if (elementId === "ajaxActivate") {
    widgetIds = refreshedValue.split(",");
    var size = widgetIds.length;
    for (i = 0; i < size; i += 1) {
      chartTabElement = document.getElementById("tabpage_" + widgetIds[i]);
      // If other browser on new active tab -> reload?
      if (chartTabElement) {
        chartTabpagesId = chartTabElement.getAttribute('parenttab');
        if (document.getElementById("data_" + chartTabpagesId)) {
          // Set the tab to that one, which id was in the refreshedValue
          // setTab(chartTabpagesId, widgetIds[i]);
          freeze = widgetIds[i];
          onAfterRefreshDelegates.push(function onafterrefresh_refreshElement_setTab() {
            setTab(chartTabpagesId, freeze);
          });
        } else {
          parentNotFound = "parent_" + chartTabpagesId;
        }
      } else {
        elementNotFound = "tabpage_" + widgetIds[i];
      }
    }
    // If not set => do nothing
    //if(elementNotFound != "" || parentNotFound != "") {} // nothing inside and no side effects. Why is this here?
  } else if (elementId === "ajaxReload") {
    var versionStr = "";
    var idx = refreshedValue.indexOf("_");
    if (0 <= idx) {
      versionStr = refreshedValue.substr(0, idx);
      refreshedValue = refreshedValue.substr(idx + 1);
    }
    if (versionStr) {
      element = document.getElementById("ajaxReloadVersionCount");
      if (element && element.type === "hidden") {
        element.value = versionStr;
      }
    }
    if (refreshedValue === "") {
      // The server tells the browser, the whole window needs to be reloaded.

      onAfterRefreshDelegates.push(reloadPage);
    } else {
      // The server tells the browser, which tab(s) to be reloaded.
      widgetIds = refreshedValue.split(",");
      for (i = 0; i < widgetIds.length; i += 1) {
        if (widgetIds[i]) {
          chartTabElement = document.getElementById("tabpage_" + widgetIds[i]);
          // If other browser on new active tab -> reload?
          if (chartTabElement) {
            chartTabpagesId = chartTabElement.getAttribute('parenttab');
            if (document.getElementById("data_" + chartTabpagesId)) {
              // Set the tab to that one, which id was in the refreshedValue
              // setTab(chartTabpagesId, widgetIds[i]);
              freeze = widgetIds[i];
              onAfterRefreshDelegates.push(function onafterrefresh_refreshElement_setTab() {
                setTab(chartTabpagesId, freeze);
              });
            } else {
              parentNotFound = "parent_" + chartTabpagesId;
            }
          } else {
            elementNotFound = "tabpage_" + widgetIds[i];
          }
        }
      }
    }
  } else if (elementId === "ajaxReloadVersionCount") {
    element = document.getElementById(elementId);
    if (element && element.type === "hidden") {
      if (element.id === "ajaxReloadVersionCount" && element.value && element.value !== refreshedValue) {
        // alert(element.id + "=" +refreshedValue);
        // The server tells the browser, the whole window needs to be reloaded.

        onAfterRefreshDelegates.push(reloadPage);
      }
      element.value = refreshedValue;
    }
  } else if (elementId.indexOf("ajaxTabVersionCount_") === 0) {
    element = document.getElementById(elementId);
    elementNotFound = "";
    parentNotFound = "";
    if (element && element.type === "hidden") {
      if (element.value !== refreshedValue) {
        // The server tells the browser, the Tab needs to be reloaded.
        chartTabElement = document.getElementById("tabpage_" + elementId);
        // If other browser on new active tab -> reload?
        if (chartTabElement) {
          chartTabpagesId = chartTabElement.getAttribute('parenttab');
          if (document.getElementById("data_" + chartTabpagesId)) {
            // Set the tab to that one, which id was in the refreshedValue
            // setTab(chartTabpagesId, elementId);

            onAfterRefreshDelegates.push(function onafterrefresh_refreshElement_setTab() {
              setTab(chartTabpagesId, elementId);
            });
          } else {
            parentNotFound = "parent_" + chartTabpagesId;
          }
        } else {
          elementNotFound = "tabpage_" + elementId;
        }
      }
      element.value = refreshedValue;
    } else {
      //console.log("Unhandled branch in refreshElement(), notable values:", elementId, refreshedValue);
    }
  } else {
    element = document.getElementById(elementId);
    // The first character of the refreshedValue is a 0 or 1.
    var refreshedEnable = parseInt(refreshedValue.charAt(0), 10);

    // Check if the incoming first character was a 0 or 1, if not,
    // then send an alert message.
    if (refreshedEnable === 0 || refreshedEnable === 1) {
      refreshedValue = refreshedValue.substring(1);
    } else {
      console.warn("refreshedValue string is not correct (first char should be 0 or 1): \n\"" + refreshedValue + "\"\n" + refreshedEnable);
      alert("refreshedValue string is not correct (first char should be 0 or 1): \n\"" + refreshedValue + "\"\n" + refreshedEnable);
    }

    if (element) {

      // The refreshedEnable value means that if the incoming integer is 1, then the
      // element is enabled, otherwise it is disabled. (it is important
      // in case of checkboxes, buttons and other editable elements)
      if (refreshedEnable === 0 || refreshedEnable === 1) {
        if (refreshedEnable === 1 && 0 <= ($(element).prop('class').indexOf("textbox_read"))) {
          str_class = $(element).prop('class');
          str_class = str_class.replace("textbox_read", "textbox_editable");
          $(element).prop('class', str_class);
          $(element).prop('readonly', "");
        } else if (refreshedEnable === 0 && 0 <= ($(element).prop('class').indexOf("textbox_editable"))) {
          str_class = $(element).prop('class');
          str_class = str_class.replace("textbox_editable", "textbox_read");
          $(element).prop('class', str_class);
          $(element).prop('readonly', "readonly");
        }

        if (element.disabled && refreshedEnable === 1) {
          element.disabled = false;
        } else if (!element.disabled && refreshedEnable === 0) {
          element.disabled = true;
        }

        var element_upButtonSpan = document.getElementById("upButtonSpan_" + elementId);
        var element_downButtonSpan = document.getElementById("downButtonSpan_" + elementId);
        if (element_upButtonSpan) {
          if (refreshedEnable == 1 && $(element_upButtonSpan).prop('class') == "upButtonReadOnly") {
            $(element_upButtonSpan).prop('class', "upButton");
          } else if (refreshedEnable === 0 && $(element_upButtonSpan).prop('class') === "upButton") {
            $(element_upButtonSpan).prop('class', "upButtonReadOnly");
          }
        }

        if (element_downButtonSpan) {
          if (refreshedEnable === 1 && $(element_downButtonSpan).prop('class') === "downButtonReadOnly") {
            $(element_downButtonSpan).prop('class', "downButton");
          } else if (refreshedEnable === 0 && $(element_downButtonSpan).prop('class') === "downButton") {
            $(element_downButtonSpan).prop('class', "downButtonReadOnly");
          }
        }
      }

      // In case of checkboxes, the element.checked attribute also
      // have to be set to a different attribute and value than the refreshedValue.
      if (element.type == "checkbox") {

        if (refreshedValue == "true") {

          // When the incoming value for the checkbox is true, the checked attribute
          // of the checkbox has to be monitored. If it is empty, the element.checked
          // attribute has to be set to "checked".
          if (element.checked === "") {
            element.checked = "checked";
          }
        } else {

          // If the incoming value for the checkbox is false, the checked attribute
          // of the checkbox has to be monitored. If it is not empty, the element.checked
          // attribute has to be set to empty string.
          if (element.checked) {
            element.checked = "";
          }
        }

      } else if (element.type === "button") {
        if (element.disabled && $($(element).parent()).prop('class').indexOf("greyButton") < 0) {
          str_class = $($(element).parent()).prop('class');
          str_class = replace_class_str(str_class, "blueButton", "greyButton");
          $($(element).parent()).prop('class', str_class);
        } else if (element.disabled === false && $($(element).parent()).prop('class').indexOf("blueButton") < 0) {
          str_class = $($(element).parent()).prop('class');
          str_class = replace_class_str(str_class, "greyButton", "blueButton");
          $($(element).parent()).prop('class', str_class);
        }

        // Check the class attribute of the element. If it is led_text, the incoming message has
        // to be sliced. The first part is the class value (led_green, ...) of the parent div of
        // the input element, the second part is the value of the element (Running, ...).
      } else if (0 <= $(element).prop('class').indexOf("led_text")) {
        var classInputString = refreshedValue.substring(refreshedValue.indexOf("[") + 1, refreshedValue.indexOf("]"));
        var valueString = refreshedValue.substring(refreshedValue.indexOf("]") + 1);
        // Replace the : in the class value to _
        var classString = classInputString.replace(":", "_");

        // Check the class attribute of the parent of the refreshed element. If it is
        // different from the incoming value, it has to be updated and the value of
        // the element also has to be updated.
        if ($($(element).parent()).prop('class').indexOf(classString) < 0 || element.value != valueString) {
          str_class = $($(element).parent()).prop('class');
          str_class = replace_class_str(str_class, "led_", classString);
          $($(element).parent()).prop('class', str_class);
          element.value = valueString;
        }

      } else if ($($(element).parent()).prop('class') == "chart") {
        initChartDatabase($($(element).parent()).prop('id'), element.id, refreshedValue);
      } else if ($($(element).parent()).prop('class') == "chart_ready") {
        updateChartDatabase($($(element).parent()).prop('id'), element.id, refreshedValue);
      } else if ($($(element).parent()).prop('class') == "distributionchart" || $($(element).parent()).prop('class') == "distributionchart_ready") {
        //initialise the database with values given in the xml

        if (initBarChartDatabaseIfNecessary(element)) {
          refreshedValue = refreshedValue.replace("{", "");
          refreshedValue = refreshedValue.replace("}", "");

          var tempId = [];
          tempId = element.id.split(".");
          var endId = element.id;
          if (4 < tempId.length) {
            endId = tempId[tempId.length - 4] + "." + tempId[tempId.length - 3];
          }

          var temp = [];
          temp = refreshedValue.split(",");
          for (i = 0; i < temp.length; i++) {
            temp[i] = parseInt(temp[i], 10);
          }
          updateBarChartDatabase($($(element).parent()).prop('id'), element.id, temp);
        } else {
          console.warn("DATABASE CANNOT BE INITIALIZED : UPDATE FAILED");
        }

      } else {
        // Boolean variable for storing if the element is a menulist
        var isNotSelect = true;

        // Getting the elements with select tag and the number of them
        var selects = document.getElementsByTagName("select");
        var numberOfSelects = selects.length;

        // Iterate through the select elements
        for (var j = 0; j < numberOfSelects; j++) {

          // Checks if any of the select elements is the
          // same as the element that needs to be updated
          if (selects[j] == element) {
            // If the element is a menulist the boolean value is set to true
            isNotSelect = false;

            if (0 <= $(element).prop('class').indexOf("listbox")) {
              refreshedValue = refreshedValue.replace("{", "");
              refreshedValue = refreshedValue.replace("}", "");

              var selectedIndexes = [];
              selectedIndexes = refreshedValue.split(",");
              var indexesSize = selectedIndexes.length;

              for (i = 0; i < indexesSize; i++) {
                if (selectedIndexes[i]) {
                  selectedIndexes[i] = parseInt(selectedIndexes[i], 10);
                }
              }

              var listboxOptions = element.options;
              var optionSize = listboxOptions.length;

              var isSelected = false;
              for (i = 0; i < optionSize; i++) {
                for (var k = 0; k < indexesSize; k++) {
                  if (selectedIndexes[k] == i) {
                    isSelected = true;
                    if (!listboxOptions[i].selected) {
                      listboxOptions[i].selected = true;
                    }
                  }
                }
                if (!isSelected) {
                  listboxOptions[i].selected = false;
                }
                isSelected = false;
              }
            } else {
              // Getting the options of the menulist and the index of the
              // current selected item
              var optionsOfSelects = selects[j].options;
              var selectedOptionId = selects[j].selectedIndex;

              // Check if the incoming value is null or empty string
              if (refreshedValue === null || refreshedValue === "") {
                // If the actual index is not -1, it is set to be
                if (selectedOptionId != -1) {
                  selects[j].selectedIndex = -1;
                }
              } else {

                // If the incoming value is not empty the actual index has to
                // tested whether it is -1 or not or the value of the actual selected
                // item is different from the incoming value or not. If
                // any of these two variants is true, then the actual index has to be set
                // to the item, which has the incoming value in its text attribute.
                var numberOfOptions = optionsOfSelects.length;
                if (selectedOptionId == -1 || optionsOfSelects[selectedOptionId].text != refreshedValue) {
                  for (var l = 0; l < numberOfOptions; l++) {
                    if (optionsOfSelects[l].text == refreshedValue) {
                      selects[j].selectedIndex = l;
                    }
                  }
                }
              }
            }
          }
        }
        /* endFor */

        // If the refreshed element is not checkbox, led, active readonly element or menulist
        // and not an element, which is edited at the moment of the refresh (text field),
        // then its value is updated.
        if (isNotSelect && element.value != refreshedValue && (document.activeElement != element || (document.activeElement == element && ($(element).prop('readonly'))))) {
          element.value = refreshedValue;
        }
        //If textarea, check focusafterchange
        if (element.type == "textarea") {
          if (($($(element).parent()).prop('class')).toUpperCase() == "FOCUSAFTERCHANGE_BOTTOM") {
            element.scrollTop = element.scrollHeight;
          } else if (($($(element).parent()).prop('class')).toUpperCase() == "FOCUSAFTERCHANGE_BOTTOMWITHTRUNC") {
            element.scrollTop = element.scrollHeight;
            element.style.overflow = "hidden";
          } else if (($($(element).parent()).prop('class')).toUpperCase() == "FOCUSAFTERCHANGE_TOP") {
            // alert("TextArea refresh Top!!!! No need to do anything, works this way by default");
          }
        }
      }
    }
  }
}

/* =================================================================================================================== */
// Update version strings process
function updateVersions(versionIds) {
  onAfterRefreshDelegates = [];
  // Create the ajax refresh string in json format for refreshing
  // the elements of the form that the page contains
  var ajaxVersionRequest = "ajaxversion_";
  ajaxVersionRequest += (new Date()).getTime() + "{" + versionIds + "}";
  // Ajax request, where the data contains the json string,
  // which contains the element ids that have to be updated
  $.ajax({
    url : "",
    type : 'GET',
    data : ajaxVersionRequest,
    dataType : 'json',
    success : function onsuccess_ajax_updateVersions(data) {
      var keyValues = "[updateVersions]:" + ajaxVersionRequest + " -> ";
      if (data) {
        $.each(data, function (key, val) {
          keyValues += key + "=" + val + "; ";
          var element = document.getElementById(key);
          if (element && element.type === "hidden") {
            element.value = val;
          } else {
            // Tab not on the actual page: subtab or its added
            // If MAIN page
            if (!document.getElementById("browserguiTabID")) {
              // The server tells the browser, the whole window needs to be reloaded.

              onAfterRefreshDelegates.push(function onafterrefresh_refreshElement_reload() {
                window.location.reload(true);
              });
            }
          }
        });
      }
    },
    error : function (jqXHR, textStatus, errorThrown) {
      console.warn("Unhandled $.ajax error in updateVersions().", jqXHR, textStatus, errorThrown);
    }
  });
}

// creates the error message panel
function createErrorMsgPanel() {
  if (!document.getElementById("error_msg_panel")) {
    //create disconn notif:

    //var input = document.createElement("input"),
    //    type = document.createAttribute("type");
    //
    //type.nodeValue = "text";
    //input.setAttributeNode(type);
    //container.appendChild(input);
    // -------------------------------------------
    //var input = document.createElement("input");
    //
    //input.type = "text";
    //container.appendChild(input);

    errorMsgElement = document.createElement("div");
    if (errorMsgElement) {
      // add id:
      errorMsgElement.id = "error_msg_panel";

      // OLD
      //        var attr = document.createAttribute("id");
      //        attr.nodeValue = "error_msg_panel";
      //        errorMsgElement.setAttributeNode(attr);

      // add red led:
      setErrorMsg("error", "Connection to server lost!");
      // add connection state:
      errorMsgElement.style.display = "none";

      // add the <div> to the document body: - Cross browser
      document.body.appendChild(errorMsgElement);
    }
  }
}

function setErrorMsgIfNoError(typeTxt, msgTxt) {
  if (errorMsgElement && (lastErrorMsg === "" || lastErrorType === "")) {
    setErrorMsg(typeTxt, msgTxt);
  }
}

function setErrorMsg(typeTxt, msgTxt) {
  if (errorMsgElement && (lastErrorMsg != msgTxt || lastErrorType != typeTxt)) {
    lastErrorType = typeTxt;
    lastErrorMsg = msgTxt;
    // clear last data
    while (errorMsgElement.childNodes.length >= 1) {
      errorMsgElement.removeChild(errorMsgElement.firstChild);
    }
    // add led:
    var line = document.createElement("P");
    if (line) {
      //var attr; // OLD
      if (typeTxt && (typeTxt == "error" || typeTxt == "warning")) {
        var ledPict = "led_red_22x22.png";
        if (typeTxt == "warning") {
          ledPict = "led_yellow_22x22.png";
        }
        //OLD
        //          attr = document.createAttribute("src");
        //          attr.nodeValue = ledPict;
        var img = document.createElement("img");
        if (img) {
          //          img.setAttributeNode(attr);
          img.src = ledPict;
          line.appendChild(img);
        }
      }
      if (msgTxt) {
        // add text:
        var txt = document.createTextNode(msgTxt);
        if (txt) {
          line.appendChild(txt);
        }
      }
      errorMsgElement.appendChild(line);
      // alert(line.innerHTML);
    }
  }
}

// printpage is referenced from a hardcoded js string in a HTML element.
function printpage() {
  try {
    window.print();
  } catch (e) {
    alert("print-error:" + e.toString());
  }
}

function updatePrintSubtitle() {
  var subtitle = document.getElementById("browsergui_print_subtitle");
  if (subtitle) {
    while (subtitle.childNodes.length >= 1) {
      subtitle.removeChild(subtitle.firstChild);
    }
    //create dsubtitlef:
    var subTitleElement = document.createElement("div");
    if (subTitleElement) {
      // add id:
      // OLD
      //        var attr = document.createAttribute("id");
      //        attr.nodeValue = "subTitlePanel";
      //        subTitleElement.setAttributeNode(attr);
      subTitleElement.id = "subTitlePanel";

      // add text:
      var line = document.createElement("P");
      var msgTxt = "";
      var menus = document.getElementsByTagName("li");
      var numberOfMenus = menus.length;
      // Iterate through the select elements
      for (var j = 0; j < numberOfMenus; j++) {
        if (0 <= menus[j].className.indexOf("tabpage_selected")) {
          if (msgTxt) {
            msgTxt += " >> ";
          }
          msgTxt += menus[j].title;
        }
      }
      if (line) {
        var txt = document.createTextNode(msgTxt);
        if (txt) {
          line.appendChild(txt);
        }
      }
      subTitleElement.appendChild(line);
      subtitle.appendChild(subTitleElement);
    }
  }
}

function hideErrorMsgPanel() {
  var returnfromerror = false;
  if (errorMsgElement) {
    lastErrorType = "";
    lastErrorMsg = "";
    errorMsgElement.style.display = "none";
    $("#page_holder_id").css({
      opacity : 1.0
    });
    $("#page_holder_id").removeAttr('disabled');
    var numberOfDisabled = disabledIDs.length;
    for (var i = 0; i < numberOfDisabled; i++) {
      var sourceTab = "";
      if (disabledIDs[i]) {
        $("#" + disabledIDs[i]).removeAttr('disabled');
        var idx = disabledIDs[i].indexOf("tabpage_");
        if (0 === idx) {
          var a_element = document.getElementById(disabledIDs[i]);
          if (a_element) {
            var urlStr = "/?tabrequest_" + sourceTab;
            $(a_element).prop('href', urlStr);
          }
        }
      }
    }
    disabledIDs = [];
    if (0 < numberOfDisabled) {
      clearCache(false);
      returnfromerror = true;
    }
  }
  haveErrorMsg = false;
  return (returnfromerror);
}

function showErrorMsgPanel() {
  var element;
  if (errorMsgElement && haveErrorMsg === false) {
    haveErrorMsg = true;
    errorMsgElement.style.display = "block";
    $("#page_holder_id").css({
      opacity : 0.5
    });
    $("#page_holder_id").attr('disabled', '');
    // Number of forms
    var numberOfForms = document.forms.length;
    var dbgStr = "";
    disabledIDs = [];
    var idx = 0;
    for (var j = 0; j < numberOfForms; j++) {
      // Number of elements on the form
      var formElementsNum = document.forms[j].elements.length;
      for (i = 0; i < formElementsNum; i++) {
        element = document.forms[j].elements[i];
        if (element.id) {
          if (element.disabled) {
            dbgStr = dbgStr + "; " + element.id + "++{" + element.disabled + "}";
          } else {
            dbgStr = dbgStr + "; " + element.id + "--{" + element.disabled + "}";
            disabledIDs[idx] = element.id;
            $("#" + disabledIDs[idx]).attr('disabled', 'true');
            element.disabled = true;
            idx++;
          }
        }
      }
    }
    var listElements = document.getElementsByTagName("li");
    var listElementsNum = listElements.length;
    for (i = 0; i < listElementsNum; i++) {
      element = listElements[i];
      if (element.id) {
        if (!(element.disabled)) {
          disabledIDs[idx] = element.id;
          $("#" + disabledIDs[idx]).attr('disabled', 'true');
          element.disabled = true;
          idx++;
        }
        var sourceTab = "";
        idx = element.id.indexOf("_");
        if (0 <= idx) {
          sourceTab = element.id.substr(idx + 1);
        }
        if (sourceTab) {
          var a_element = document.getElementById("tabpage_" + sourceTab);
          if (!(a_element && a_element.disabled)) {
            //var a_element_href_int = a_element.getAttribute("href");
            //$(a_element).prop('href', "javascript:"); // $.click, maybe?!
            disabledIDs[idx] = a_element.id;
            $("#" + disabledIDs[idx]).attr('disabled', 'true');
            a_element.disabled = true;
            idx++;
          }
        }
      }
    }

    var numberOfDisabled = disabledIDs.length;
    dbgStr = dbgStr + " - ";
    for (var i = 0; i < numberOfDisabled; i++) {}
    //alert(dbgStr);
  }
  resetDebug();
}

function isArrayAllTheSame(arr) {
  "use strict";
  return !!arr.reduce(function (a, b) {
    return (a === b) ? a : NaN;
  });
}

function autoCutLabels(db) {
  "use strict";
  var pivot = [];
  var unpivot = [];
  var i;
  var j;

  // pivot the chart labels (uses the fixed traceLabel, not the volatile label)
  for (i = 0; i < db.length; i++) {
    var t = db[i].traceLabel.split('.');
    t.forEach(function (curr, idx) {
      if (!pivot[idx])
        pivot[idx] = [];
      pivot[idx][i] = curr;
    });
  }

  // Cut common prefixes
  while (pivot && pivot[0] && isArrayAllTheSame(pivot[0])) {
    pivot.shift();
  }

  // Cut common suffixes
  while (pivot && pivot[pivot.length - 1] && isArrayAllTheSame(pivot[pivot.length - 1])) {
    pivot.pop();
  }

  // Recreate labels / unpivot
  for (i = 0; i < pivot.length; i++) {
    for (j = 0; j < pivot[i].length; j++) {
      if (!unpivot[j])
        unpivot[j] = [];
      unpivot[j] += pivot[i][j] + ' ';
    }
  }

  // Set the real labelz
  for (i = 0; i < db.length; i++) {
    db[i].label = unpivot[i] ? unpivot[i].trim() : "N/A"; // Thank god the objects come through by-reference.
  }
}

function updateChartDatabase(chartElementId, traceLabel, fresh) {
  "use strict";
  var unique_id = chartElementId + traceLabel;
  //var serieLabel = traceLabel.split('.').pop(); // Not needed anymore.
  var size = chart_trace_ids.length;
  var k = -1;
  var freshData;

  for (var i = 0; i < size; i++) {
    if (chart_trace_ids[i] === unique_id) {
      k = i;
      break;
    }
  }

  // unique id not found in the set, insert it
  if (k == -1) {
    chart_trace_ids.push(unique_id);
    k = size;
  }

  if (!traces || !traces[k]) {
    if (traces) {
      traces[k] = [];
    } else {
      console.error("In updateChartDatabase(), `traces` or `traces[" + k + "]` turned out to be undefined, which only happens if there were an error during initialization.", traces, traces[k]);
    }
  }

  // Store for each trace, if it is changed during the last refresh or not
  traces[k][2] = true; // true for changed

  if (typeof fresh == "string") {
    freshData = fresh.split(":");
    freshData[0] = parseInt(freshData[0], 10); // Je Suis *nix Timestamp
    freshData[1] = parseFloat(freshData[1]);
  } else { // We hope it's preprocessed by initChartDatabase()...
    freshData = fresh;
  }
  freshData[0] *= 1000; // convert to millisec precision *nix timestamp

  if (!chart_line_series[chartElementId]) {
    chart_line_series[chartElementId] = [];
  }

  if (traces[k][0] === freshData[0]) {
    traces[k][2] = false; // Last not changed
  } else {
    var v_serie = chart_line_series[chartElementId].find(function anon_find_serie(c) {
        return c.id === unique_id;
      });
    traces[k][0] = freshData[0];
    traces[k][1] = freshData[1];
    if (v_serie) { // contains label
      v_serie.data.push([traces[k][0], traces[k][1]]);
      v_serie.data = v_serie.data
        .sort(function anon_sort_vseriedata(a, b) {
          return ((a[0] < b[0]) ? -1 : ((a[0] > b[0]) ? 1 : 0));
        });
      if (v_serie.data.length > v_serie.maxPoints) {
        v_serie.data.splice(0, v_serie.data.length - v_serie.maxPoints); // Cut overflown elements from the beginning.
      }
    } else { // new
      // $configElement: complex selector needed, as it is not portable to select an ID that contains dots. (They tend to be treated as '.class' selectors.)
      // Oh wait, that's not even standards compliant!
      var $configElement = $('INPUT[id="' + traceLabel + '"]');
      var serie_color = $configElement.attr('color');
      var serie_label = $configElement.attr('name');
      var serie_maxpoints = parseInt($configElement.attr('maxpoints'), 10);
      chart_line_series[chartElementId].push({
        "id" : unique_id,
        "traceLabel" : traceLabel,
        "label" : serie_label ? serie_label : "N/A", // Also check the function autoCutLabels().
        "data" : [
          [traces[k][0], traces[k][1]]
        ],
        "color" : serie_color ? countHexTraceColor(serie_color) : defaultLineColor[chart_line_series[chartElementId].length % defaultLineColor.length],
        "maxPoints" : serie_maxpoints ? serie_maxpoints : 100
      });

      if (!serie_label) {
        // automagically make labels if there are missing ones.
        autoCutLabels(chart_line_series[chartElementId]);
      }
    }

    if (g_$chart[chartElementId]) {
      g_$chart[chartElementId].setData(chart_line_series[chartElementId]);
      repaintChart(chartElementId);
    }
  }
}

function initChartDatabase(chartElementId, traceLabel, refreshedValue) {
  "use strict";
  var unique_id = chartElementId + traceLabel;
  var size = chart_trace_ids.length;
  var k = -1;
  for (var i = 0; i < size; i++) {
    if (chart_trace_ids[i] == unique_id) {
      k = i;
      break;
    }
  }

  //////unique id not found, insert it
  if (k == -1) {
    chart_trace_ids[size] = unique_id;
    k = size;
  }

  traces[k] = [undefined, undefined, false];

  refreshedValue.split("_")
  .map(function anon_map_iva(curr) {
    var c = curr.split(":");
    return [parseInt(c[0], 10), parseFloat(c[1])];
  })
  .filter(function anon_filter_iva(c, i, a) {
    if (i && c[0] === a[i - 1][0])
      return false; // Skip duplicates
    updateChartDatabase(chartElementId, traceLabel, c); // if seemingly unique, display it.
    return true;
  });
}

function countHexTraceColor(traceColor) {
  "use strict";
  var hexColorValue = '';

  if (!traceColor) {
    return hexColorValue;
  }

  switch (traceColor) {
  case "blue":
    hexColorValue = '#0000FF';
    break;
  case "cyan":
    hexColorValue = '#00FFFF';
    break;
  case "dark_gray":
    hexColorValue = '#202020';
    break;
  case "gray":
    hexColorValue = '#505050';
    break;
  case "light_gray":
    hexColorValue = '#B0B0B0';
    break;
  case "green":
    hexColorValue = '#00FF00';
    break;
  case "red":
    hexColorValue = '#FF0000';
    break;
  case "magenta":
    hexColorValue = '#FF00FF';
    break;
  case "pink":
    hexColorValue = '#F52887';
    break;
  case "orange":
    hexColorValue = '#FF7000';
    break;
  case "yellow":
    hexColorValue = '#F0F000';
    break;
  case "white":
    hexColorValue = '#FFFFFF';
    break;
  default:
    if (traceColor) {
      var rgb = traceColor.split(":");

      // RGB to Hex
      if (rgb[0].toUpperCase() == "RGB") {
        hexColorValue = '#' + ('0' + parseInt(rgb[1], 10).toString(16)).slice(-2) +
          ('0' + parseInt(rgb[2], 10).toString(16)).slice(-2) +
          ('0' + parseInt(rgb[3], 10).toString(16)).slice(-2);
      }
    }
  }
  return hexColorValue;
}

function drawChart(chartElementId) {
  "use strict";
  var $chartElement = $(document.getElementById(chartElementId));
  var title = $chartElement.attr('title');

  if ($chartElement.hasClass('chart')) {
    // This branch happens later than initChartDatabase:
    // Save Chart state as Image (PNG)
    var $chartElementDraw = $(document.getElementById(chartElementId + '_draw'));
    var v_$me = $.plot($chartElementDraw, chart_line_series[chartElementId], $.extend({
          'title' : title
        }, g_flot_line_cfg));
    v_$me.hooks.drawOverlay.push(plotOverlayHandler);
    g_$chart[chartElementId] = v_$me;
    repaintChart(chartElementId);

    $chartElement.prop('class', "chart_ready");
  }
}

function initBarChartDatabase(chartElementId, traceLabel, refreshedValue) {
  "use strict";
  var unique_id = chartElementId + traceLabel;
  var size = charts_initialized.length;
  var k = -1;
  var i;
  for (i = 0; i < size; i++) {
    if (charts_initialized[i] == chartElementId) {
      k = i;
      break;
    }
  }
  if (k == -1) {
    charts_initialized[size] = chartElementId;
  }

  size = barChartIds.length;
  k = -1;
  for (i = 0; i < size; i++) {
    if (barChartIds[i] == unique_id) {
      k = i;
      return;
    }
  }

  if (document.getElementById(traceLabel)) {
    if (document.getElementById(traceLabel).getAttribute('class') == "intervallimits") {
      return;
    }
  }

  if (k == -1) {
    barChartIds[size] = unique_id;
    k = size;
  }

  barChartValues[k] = refreshedValue;
  barChartValues_changed[k] = true;
}

function updateBarChartDatabase(chartElementId, traceLabel, refreshedValue) {
  "use strict";
  var unique_id = chartElementId + traceLabel;
  var size = barChartIds.length;
  var i;
  var k = -1;
  var freshData = [];
  var serieLabel = traceLabel.split('.');
  serieLabel.pop();
  serieLabel = serieLabel.join('.');

  for (i = 0; i < size; i++) {
    if (barChartIds[i] == unique_id) {
      k = i;
      break;
    }
  }

  if (document.getElementById(traceLabel)) {
    if (document.getElementById(traceLabel).getAttribute('class') == "intervallimits") {
      return;
    }
  }

  if (k == -1) {
    barChartIds[size] = unique_id;
    k = size;
  }

  if (barChartValues[k] != refreshedValue) {
    barChartValues[k] = refreshedValue;
    barChartValues_changed[k] = true;
  }

  for (i = 0; i < refreshedValue.length; i += 1) {
    freshData.push([i, refreshedValue[i]]);
  }

  if (!chart_bar_series[chartElementId]) {
    chart_bar_series[chartElementId] = [];
  }

  var v_bar_serie = chart_bar_series[chartElementId].find(function anon_find_bar_serie(c) {
      return c.label === serieLabel;
    });
  if (v_bar_serie) { // exists
    v_bar_serie.data = freshData;
  } else { // new
    chart_bar_series[chartElementId].push({
      "label" : serieLabel,
      "data" : freshData,
      "color" : defaultLineColor[chart_bar_series[chartElementId].length % defaultLineColor.length]
    });
  }

  if (g_$chart[chartElementId]) {
    g_$chart[chartElementId].setData(chart_bar_series[chartElementId]);
    repaintChart(chartElementId);
  }
}

function initBarChartDatabaseIfNecessary(element) {
  "use strict";
  //    console.log("initBarChartDatabaseIfNecessary  " + $($(element).parent()).prop('id'));
  var size = charts_initialized.length;
  var k = -1;
  var i;
  var j;
  for (i = 0; i < size; i++) {
    if (charts_initialized[i] == $($(element).parent()).prop('id')) {
      k = i;
      break;
    }
  }
  if (k == -1) {
    //console.log("Chart not initialized yet, do it.");
    var temp = [];
    //Draw distribution charts as barcharts
    var barChartElements = document.getElementsByClassName('distributionchart');
    var barChartElementsSize = barChartElements.length;

    for (i = 0; i < barChartElementsSize; i++) {
      if (barChartElements[i] && barChartElements[i].id) {
        //alert(barChartElements[i].id);
        //initialise the database with values given in the xml
        var storeThisID = "";
        $($(barChartElements[i]).children()).each(function anon_each() {
          if ($(this).prop('class') == "distributionchart_valuelist") {
            if (temp.length > 0) {
              for (j = 0; j < temp.length; j++) {
                temp[j] = parseInt(temp[j], 10);
              }
              initBarChartDatabase($(barChartElements).prop('id'), storeThisID, temp);
            }
            storeThisID = $(this).prop('id');
            temp = [];
          } else {
            if ($(this).prop('class') == "distributionchart_value") {
              temp.push(this.innerHTML);
            }
          }
        });

        if (temp.length > 0) {
          //for the last valuelist
          for (j = 0; j < temp.length; j++) {
            temp[j] = parseInt(temp[j], 10);
          }
          initBarChartDatabase($(barChartElements).prop('id'), storeThisID, temp);
        }
      }
    }

    temp = [];

    size = charts_initialized.length;
    k = -1;
    for (i = 0; i < size; i++) {
      if (charts_initialized[i] == $($(element).parent()).prop('id')) {
        k = i;
        break;
      }
    }
    if (k == -1) {
      //            console.log("First Init was not working, try the other way!!!!!!");

      //initialise the database with values given in the xml
      $($(barChartElements).children()).each(function () {
        if ($(this).prop('class') != "intervallimits") {

          var temp = [];
          $($(this).children()).each(function () {
            temp.push(this.innerHTML);
          });

          for (j = 0; j < temp.length; j++) {
            temp[j] = parseInt(temp[j], 10);
          }
          initBarChartDatabase($(barChartElements).prop('id'), $(this).prop('id'), temp);
        }

      });

    }
    size = charts_initialized.length;
    k = -1;
    for (i = 0; i < size; i++) {
      if (charts_initialized[i] == $($(element).parent()).prop('id')) {
        k = i;
        break;
      }
    }
    if (k == -1) {
      //            console.log("very big problem occured, init phase not working!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
      return false;
    } else {
      return true;
    }

  } else {
    // console.log("Chart already initialized, do nothing.");
    return true;
  }

}

function drawBarChart(chartElementId) {
  "use strict";
  var $chartElement = $("#" + chartElementId);
  var title = $chartElement.prop('title');

  if ($chartElement.hasClass('distributionchart')) {
    var chartElementDrawId = "#" + chartElementId + "_draw";

    // This happens later than initBarChartDatabase:
    g_$chart[chartElementId] = $.plot(chartElementDrawId, chart_bar_series[chartElementId], $.extend({
          'title' : title
        }, g_flot_bar_cfg));
    g_$chart[chartElementId].hooks.drawOverlay.push(plotOverlayHandler);
    repaintChart(chartElementId);

    $chartElement.prop('class', "distributionchart_ready");
  }

  //$("#" + chartElementId + "_draw").html("<p style='font-size:20px;'>This |feature| is currently disabled.</p>");
}

function drawCharts(drawflag) {
  "use strict";
  var i;
  if (document.getElementsByClassName) {
    var chartElements = document.getElementsByClassName('chart');
    var chartElementsSize = chartElements.length;

    for (i = 0; i < chartElementsSize; i++) {
      if (chartElements[i] && chartElements[i].id && (drawflag === true || isScrolledIntoView(chartElements[i].id + "_draw"))) {
        drawChart(chartElements[i].id);
      }
    }

    //Draw distribution charts as barcharts
    var barChartElements = document.getElementsByClassName('distributionchart');
    var barChartElementsSize = barChartElements.length;

    for (i = 0; i < barChartElementsSize; i++) {
      if (barChartElements[i] && barChartElements[i].id && (drawflag === true || isScrolledIntoView(barChartElements[i].id + "_draw"))) {
        //initialise the database with values given in the xml
        $($(barChartElements).children()).each(function anon_each() {
          if ($(this).prop('class') != "intervallimits") {
            initBarChartDatabaseIfNecessary(this);
          }
        });
        drawBarChart(barChartElements[i].id);
      }
    }
  } else {
    // for IE: - Cross browser
    var a = document.getElementsByTagName("div");
    var element = a[0];
    i = 0;
    while (element) {
      if (0 <= element.className.indexOf("chart")) {
        if (drawflag === true || isScrolledIntoView(element.id + "_draw")) {
          drawChart(element.id);
        }
      } else if (0 <= element.className.indexOf("distributionchart")) {
        if (drawflag === true || isScrolledIntoView(element.id + "_draw")) {
          drawBarChart(element.id);
        }
      }

      i++;
      element = a[i];
    }
  }
}

function repaintChart(chartElementId) {
  var v_$me = g_$chart[chartElementId];
  var exists = !!document.getElementById(chartElementId);
  if (exists && v_$me) {
    try {
      v_$me.resize(); // Automagically resize to fit the plot placeholder again.
      v_$me.setupGrid();
      v_$me.draw();
    } catch (ex) {
      // chart iz gone.
      exists = false;
    }
  }
  if (!exists) {
    // chartElementId nonexistent, remove it.
    g_$chart[chartElementId] = null;
    /* Remove this `delete` line below if performance really, REALLY matters.
     * Not in the ratfucking "just because I can" way, but the real performance tuning scenario.
     */
    delete g_$chart[chartElementId];
  }
}

// repaint, resize, &c the charts to be fancy again.
function repaintCharts() {
  for (var chartElementId in g_$chart) {
    if (g_$chart.hasOwnProperty(chartElementId))
      repaintChart(chartElementId);
  }
}

function resetAllCharts() {
  chart_line_series = {};
  chart_bar_series = {};
  repaintCharts();
}

// This function converts the values of the html elements to
// ttcn values. EG.: checked attribute of the checkbox into true-false.
function convertElement(elementId, elementValue) {
  var i;
  var element = document.getElementById(elementId);

  if (element) {

    // In case of checkboxes the value should be converted to true or false
    if (element.type == "checkbox") {
      if (element.checked == true) {
        return "true";
      } else {
        return "false";
      }
    } else {
      // It has to be checked if the updated element is a menulist.
      // If it is, the text of the selected option has to be returned
      // as the value of the element update.
      var selects = document.getElementsByTagName("select");
      var numberOfSelects = selects.length;
      var isNotSelect = true;

      // Check if the element is equal with any of the select elements
      // of the form.
      for (i = 0; i < numberOfSelects; i++) {

        // If it is, the isNotSelect variable has to be set to false, to
        // avoid that the last return overwrites this value.
        if (element == selects[i]) {
          isNotSelect = false;

          if (0 <= $(element).prop('class').indexOf("listbox")) {
            var listboxOptions = element.options;
            var size = listboxOptions.length;
            var selectedOptions = "{";

            for (i = 0; i < size; i++) {
              if (listboxOptions[i].selected) {
                selectedOptions = selectedOptions + i.toString() + ",";
              }
            }
            selectedOptions = selectedOptions + "}";
            selectedOptions = selectedOptions.replace(",}", "}");

            return selectedOptions;
          } else {
            // Return the text of the selected option.
            return element.options[element.selectedIndex].text;
          }
        }
      }

      // If the element is not checkbox or menulist, the original
      // elementValue is returned.
      if (isNotSelect) {
        return elementValue;
      }
    }
  }
}

// This function is escaping the special characters as ",/,\, by placing
// an extra \ before each character by using the .replace() built-in function
// and returns the escaped string
function escapeElement(elementString) {
  var escapedString = elementString.replace('"', '\"');
  escapedString = escapedString.replace("\\", "\\\\");
  escapedString = escapedString.replace("/", "\/");

  return escapedString;
}

// Function to generate and send the Ajax request, which
// contains the id and value of the updated element
function sendAjaxUpdateRequest(elementId, elementValue) {
  // If Error pending
  if (haveErrorMsg) {
    return;
  }
  // Escape the special characters from the element id
  var escapedId = escapeElement(elementId);
  // Escape the special characters from the element value
  var escapedValue = escapeElement(elementValue);

  // Creating the json string for the element that has to be updated,
  // the string contains the element id and value in the following format:
  // {"element id":"element value"}
  var ajaxUpdate = "ajaxupdate_" + updateSuccess + (new Date()).getTime() + '{"' + escapedId + '"' + ":" + '"' + escapedValue + '"' + "}";
  // Send the ajax request to the server
  $.ajax({
    url : "",
    type : 'GET',
    data : ajaxUpdate,
    dataType : 'text',
    success : function (data) {
      // called when successful
      // The response contains a success message and two error codes.
      // In case of an error code is received an alert window pops up
      // on the page with the proper message (Wrong value type, unexisting variable)
      if (data == "ajaxupdate_NOK_WRONG_VALUE_GIVEN") {
        alert("Wrong value type was given...");
        // show error message
        setErrorMsg("error", "sendAjaxUpdateRequest: Wrong value type was given...");
        showErrorMsgPanel();
      } else if (data == "ajaxupdate_ERROR_NOVAR") {
        alert("No variable exists with this id: " + elementId);
        // show error message
        setErrorMsg("error", "sendAjaxUpdateRequest: No variable exists with this id: " + elementId);
        showErrorMsgPanel();
      } else if (data != "ajaxupdate_OK") {
        alert("sendAjaxUpdateRequest: error = " + data);
        // show error message
        setErrorMsg("error", "sendAjaxUpdateRequest: error = " + data);
        showErrorMsgPanel();
      }
      updateSuccess++;
    },
    error : function (e) {
      // show error message
      setErrorMsg("error", "sendAjaxUpdateRequest: " + e.toString());
      showErrorMsgPanel();
      //alert("error");
    }
  });
}

/* TAB | MENU Handling */
var destTabID = "";
var xslDoc = null;
var xslDocBase = null;
var xmlDoc = null;
var xmlDoc2 = null;
var xmlDocBase = null;

var g_destId = null;
var g_sourceTab = null;

function loadXMLDoc(fname) {
  var xmlDocLoc = null;
  if (window.ActiveXObject) {
    // ...otherwise, use the ActiveX control for IE.
    xmlDocLoc = new window.ActiveXObject('Microsoft.XMLHTTP');
    xmlDocLoc.open("GET", fname, false);
    xmlDocLoc.send("");
  } else if (window.XMLHttpRequest) {
    // If Mozilla, Safari, and so on: Use native object.
    xmlDocLoc = new XMLHttpRequest();
    xmlDocLoc.open("GET", fname, false);
    xmlDocLoc.send("");
  } else {
    alert('Your browser cannot handle this script');
  }
  return (xmlDocLoc);
}

function setTab(destId, sourceTab) {
  // If Error pending
  if (haveErrorMsg) {
    return;
  }
  clearCache(false);
  g_destId = destId;
  g_sourceTab = sourceTab;
  destTabID = "data_" + destId;
  var urlStr = "?tabrequest_samepage_{" + (new Date()).getTime() + "}" + sourceTab;

  a_element = document.getElementById("tabpage_" + sourceTab);
  a_element_orig_href = a_element.getAttribute("href");
  // Note: Yes, this HREF hack is ugly here --v below, but don't play with click handlers and e.preventDefault(), as the a_element already has event handlers, and you would just screw them up. Let the dead rest in peace.
  $(a_element).prop('href', "javascript:;"); // Leave the ;, it's good. Ignore linter/checker warnings.

  xmlDoc = null;
  try {
    if (typeof window.ActiveXObject != 'undefined') {
      xmlDoc = new window.ActiveXObject("Microsoft.XMLHTTP");
    } else if (window.XMLHttpRequest) {
      xmlDoc = new window.XMLHttpRequest();
      xmlDoc.onreadystatechange = function anon_ajax_succ_settab() {
        if (xmlDoc.readyState == 4 && xmlDoc.status == 200) {
          process();
        }
      };
    } else {
      alert('Your browser cannot handle this script');
    }
    if (xmlDoc) {
      xmlDoc.open("GET", urlStr, true); // If you rework this to async (3rd to true), you have to completely rewrite the process() function.
      xmlDoc.send(null);
      //process();                             // If async, it is called in the event handler
    }
  } catch (e) {
    alert("setTab-error:" + e.toString());
    // show error message
    setErrorMsg("error", "setTab: " + e.toString());
    showErrorMsgPanel();
  }
}

function process() {
  //if ( xmlDoc.readyState != 4 ) return ;
  //if ( xmlDoc.status != 200 ) return ;
  var element = document.getElementById(destTabID);
  var requestCount = 0;
  var i;
  var len;
  var str_class;
  if (element && element.childNodes) {
    while (element.childNodes.length >= 1) {
      element.removeChild(element.firstChild);
    }
    while (xmlDoc.responseXML === null && requestCount < 100) {
      var urlStr = "/?tabrequest_samepage_{" + (new Date()).getTime() + "}" + g_sourceTab;
      requestCount++;
      try {
        xmlDocBase = loadXMLDoc(urlStr);
        xmlDoc = xmlDocBase.responseXML;
      } catch (e) {
        alert("ZZ " + e.toString());
        // show error message
        setErrorMsg("error", "process: " + e.toString());
        showErrorMsgPanel();
      }
    }
    if (requestCount < 100) {
      while (/*!xslDocBase || xslDocBase.parseError.errorCode != 0 || */
        !xslDoc) {
        for (i = 0; i < xmlDoc.responseXML.childNodes.length; i++) {
          var child = xmlDoc.responseXML.childNodes[i];
          if (child.nodeType == 7) {
            if (child.target == "xml-stylesheet") {
              var match = /href="(.*?)"/gi.exec(child.data);
              if (match.index >= 0) {
                xslDocBase = loadXMLDoc(match[1]);
              } else {
                alert("NoXSLdownloaded, using tab.xsl:" + child.data);
              }
              xslDoc = xslDocBase.responseXML;
            }
          }
        }
      }
      xmlDoc2 = xmlDoc.responseXML;
      if (window.ActiveXObject) {
        // support Windows/ActiveX enabled browsers
        element.innerHTML = xmlDoc.responseXML.transformNode(xslDoc);
      } else if (window.XMLHttpRequest) {
        // code for Mozilla, Firefox, Opera, etc.
        var xsltProcessor;
        try {
          xsltProcessor = new window.XSLTProcessor();
          xsltProcessor.importStylesheet(xslDoc);
        } catch (e) {
          alert("X" + e.toString());
          alert(xmlDoc2.responseText);
          alert(xslDoc.responseText);
          // show error message
          setErrorMsg("error", "process1: " + e.toString());
          showErrorMsgPanel();
        }

        var resultDocument;
        try {
          resultDocument = xsltProcessor.transformToFragment(xmlDoc2, document);
        } catch (e) {
          alert("Y" + e.toString());
          alert(xmlDoc2.responseText);
          alert(xslDoc.responseText);
          // show error message
          setErrorMsg("error", "process2: " + e.toString());
          showErrorMsgPanel();
        }
        element.appendChild(resultDocument);
      }
      /* Select tabpage and deselect the sibling tabpages */
      var elementTabpage = document.getElementById("tabpage_" + g_sourceTab);
      var li_element = elementTabpage.parentNode;
      var li_elements = elementTabpage.parentNode.parentNode.getElementsByTagName("li");
      for (i = 0, len = li_elements.length; i < len; i++) {
        str_class = $(li_elements[i]).prop('class');
        str_class = replace_class_str(str_class, "tabpage_selected", "");
        str_class = replace_class_str(str_class, "hidetabpage_selected", "");
        $(li_elements[i]).prop('class', str_class);
      }

      if (0 <= $(li_element.parentNode).prop('class').indexOf("tabs_menu") || 0 <= $(li_element.parentNode).prop('class').indexOf("vert_menu")) {
        str_class = $(li_element).prop('class');
        str_class = "tabpage_selected " + str_class;
        $(li_element).prop('class', str_class);
      } else {
        str_class = $(li_element).prop('class');
        str_class = "hidetabpage_selected " + str_class;
        $(li_element).prop('class', str_class);
      }
    } else {
      alert("process: 100 < requestCount");
      // show error message
      setErrorMsg("error", "process4: 100 < requestCount");
      showErrorMsgPanel();
    }

  } else {
    alert("process:ERROR");
    // show error message
    setErrorMsg("error", "process3: ERROR");
    showErrorMsgPanel();
  }
  // Cross browser
  if (crosbrowserProcessID === null) {
    crosbrowserProcessID = setInterval(function () {
        crossBrowserDelay();
      }, crosbrowserInterval);
  }

  repaintCharts();
}

function crossBrowserDelay() {
  if (crosbrowserProcessID) {
    clearInterval(crosbrowserProcessID);
    crosbrowserProcessID = null;
  }
  if (a_element && a_element_orig_href) {
    $(a_element).prop('href', a_element_orig_href);
    a_element_orig_href = "";
  }
}

function isScrolledIntoView(elemId) {
  var docViewTop = $(window).scrollTop();
  var docViewBottom = docViewTop + $(window).height();
  var docViewLeft = $(window).scrollLeft();
  var docViewRigth = docViewLeft + $(window).width();

  return isScrolledIntoViewWindow(elemId, docViewTop, docViewBottom, docViewLeft, docViewRigth);
}

function isScrolledIntoViewWindow(elemId, docViewTop, docViewBottom, docViewLeft, docViewRigth) {
  if (elemId == "ajaxReloadVersionCount") {
    return true;
  }

  if (elemId.indexOf("ajaxTabVersionCount_") === 0) {
    return true;
  }
  // No refresh
  if (elemId == "browserguiTabID") {
    return false;
  }

  var elem;
  if (elemId) {
    elem = document.getElementById(elemId);
  }

  if (!elem) {
    return (false);
  }

  var elemTop = $(elem).offset().top;
  var elemBottom = elemTop + $(elem).height();
  var elemLeft = $(elem).offset().left;
  var elemRight = elemLeft + $(elem).width();

  var retVal = ((elemBottom >= docViewTop) && (elemTop <= docViewBottom)) &&
  ((elemRight >= docViewLeft) && (elemLeft <= docViewRigth));
  return (retVal);
}

function setCookie(cookieName, cookieValue) {
  var date = new Date();
  date.setTime(date.getTime() + (24 * 60 * 60 * 1000));
  document.cookie = cookieName + "=" + cookieValue /*+ "; expires=" + date.toGMTString() + "; path=/"*/;
}

function restoreEscapedString(escapedString) {
  var restoredString = escapedString.replace("\\n", "\n");
  restoredString = restoredString.replace("\\r", "\r");
  restoredString = restoredString.replace("\\b", "\b");
  restoredString = restoredString.replace("\\t", "\t");
  restoredString = restoredString.replace("\\f", "\f");
  restoredString = restoredString.replace("//", "/");
  restoredString = restoredString.replace('\\\"', '\"');
  //restoredString = restoredString.replace('\"','"');

  return restoredString;
}

function toggleTabpages(id) {
  var divImg = document.getElementById("expandCollapseImg" + id).parentNode;
  var divData = document.getElementById("expand_collapse_data" + id);
  if ($(divImg).prop('class') == "expand_collapse_up") {
    divData.style.display = "none";
    $(divImg).prop('class', "expand_collapse_down");
  } else if ($(divImg).prop('class') == "expand_collapse_down") {
    divData.style.display = "block";
    $(divImg).prop('class', "expand_collapse_up");
  } else if ($(divImg).prop('class') == "expand_collapse_left") {
    divData.parentNode.style.display = "none";
    divData.style.display = "none";
    $(divImg).prop('class', "expand_collapse_right");
  } else {
    divData.parentNode.style.display = "";
    divData.style.display = "block";
    $(divImg).prop('class', "expand_collapse_left");
  }
}

function changeNumericalWidget(id, minvalue, maxvalue, stepsize, widgetType, add) {
  var numWidget = document.getElementById(id);
  if ($(numWidget).prop('readonly') === false) {
    var widgetVal,
    stepSizeVal,
    minvalueVal,
    maxvalueVal;
    var numWidgetDigLength = numWidget.value.length - numWidget.value.indexOf(".") - 1;

    if (widgetType == "integerField") {
      widgetVal = parseInt(numWidget.value, 10);
      stepSizeVal = parseInt(stepsize, 10);
      minvalueVal = parseInt(minvalue, 10);
      maxvalueVal = parseInt(maxvalue, 10);
    } else {
      widgetVal = parseFloat(numWidget.value);
      stepSizeVal = parseFloat(stepsize);
      minvalueVal = parseFloat(minvalue);
      maxvalueVal = parseFloat(maxvalue);
    }

    if (add == "true") {
      widgetVal = widgetVal + stepSizeVal;
    } else {
      widgetVal = widgetVal - stepSizeVal;
    }

    if (minvalueVal > widgetVal) {
      widgetVal = minvalueVal;
    }
    if (maxvalueVal < widgetVal) {
      widgetVal = maxvalueVal;
    }

    if (widgetType == "integerField") {
      numWidget.value = widgetVal;
    } else {
      numWidget.value = widgetVal.toFixed(numWidgetDigLength);
    }

    //converting html value to ttcn
    var convertedValue = convertElement(id, numWidget.value);
    sendAjaxUpdateRequest(id, convertedValue);
  }
}

function insertDebug(txt) {
  if (debugEnabled && debugInOut) {
    var element = document.getElementById("debugStr");
    if (element && element.type === "hidden") {
      element.value = element.value + " - " + txt;
    } else {}
  }
}

function getDebug() {
  var txt = "";
  if (debugEnabled && debugInOut) {
    var element = document.getElementById("debugStr");
    if (element && element.type === "hidden") {
      txt = element.value;
    }
  }
  debugInOut = false;
  return (txt);
}

function resetDebug() {
  var txt = "";
  if (debugEnabled && debugInOut) {
    var element = document.getElementById("debugStr");
    if (element && element.type === "hidden") {
      element.value = "";
    }
  }
  debugInOut = true;
  return (txt);
}

function updateRefreshPageWidth() {
  if (refresPageWidth == -1) {
    var main_holder,
    page_holder;
    main_holder = document.getElementById("main_holder_id");
    page_holder = document.getElementById("page_holder_id");
    if (main_holder) {
      //alert($(main_holder).width() +"-"+ $(page_holder).width());
      if ($(main_holder).width() == $(page_holder).width()) {
        refresPageWidth = 0;
      } else {
        refresPageWidth = $(main_holder).width();
        var divData = document.getElementById("full_size_id");
        if (divData) {
          divData.style.display = "block";
        }
      }
    }
  }
}

function full_size() {
  var main_holder;
  main_holder = document.getElementById("main_holder_id");
  if (main_holder) {
    main_holder.style.width = "100%";
    var divData = document.getElementById("full_size_id");
    if (divData) {
      divData.style.display = "none";
    }
    divData = document.getElementById("orig_size_id");
    if (divData) {
      divData.style.display = "block";
    }
  }
}

function orig_size() {
  var main_holder;
  main_holder = document.getElementById("main_holder_id");
  if (main_holder) {
    main_holder.style.width = refresPageWidth + "px";
    var divData = document.getElementById("orig_size_id");
    if (divData) {
      divData.style.display = "none";
    }
    divData = document.getElementById("full_size_id");
    if (divData) {
      divData.style.display = "block";
    }
  }
}

function replace_class_str(str_class, str_from_part, str_to_class) {
  var idx_from_class_part = str_class.indexOf(str_from_part);
  var str_classNew = "";
  if (0 <= idx_from_class_part) {
    // change split
    var classesList = str_class.split(" ");
    for (var i = 0; i < classesList.length; i++) {
      if (0 <= classesList[i].indexOf(str_from_part)) {
        if (str_to_class) {
          str_classNew = str_to_class + " " + str_classNew;
        }
      } else {
        if (classesList[i] && str_to_class != classesList[i]) {
          str_classNew = str_classNew + " " + classesList[i];
        }
      }
    }
  } else {
    if (str_to_class) {
      if (str_class.indexOf(str_to_class) < 0) {
        str_classNew = str_to_class + " " + str_class;
      }
    }
  }
  return (str_classNew);
}

function clearCache(reset) {
  ajaxRefreshRequestCache = [];
  repaintCharts();
  if (reset)
    resetAllCharts(); // The Viewstate changed, a new chart will be drawn, so reset.
}

function helppage(url) {
  var win = window.open(url, '_blank');
  win.focus();
}

function aboutpage(url) {
  window.open(url, "About...", "status = 1, height = 300, width = 400, resizable = 0");
}
