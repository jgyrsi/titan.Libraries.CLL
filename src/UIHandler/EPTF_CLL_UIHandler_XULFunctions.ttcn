
///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_UIHandler_XULFunctions
// 
//  Purpose:
//    This module contains widget manipulation functions which
//    use the obsolete XTDPAsp testport based structures and functions.
// 
//  Current Owner:
//    ELSZSKU
// 
//  Last Review Date:
//    2011-xx-xx
///////////////////////////////////////////////////////////
module EPTF_CLL_UIHandler_XULFunctions
{
import from EPTF_CLL_CLI_Definitions all;
//import from EPTF_CLL_UIHandler_ConvertFunctions all;
import from EPTF_CLL_UIHandler_WidgetFunctions all;
import from ttcn_ericsson_se_protocolModules_xtdp_xtdl all;
import from EPTF_CLL_UIHandler_Definitions all;

friend module EPTF_CLL_UIHandlerClient_Functions;

///////////////////////////////////////////////////////////
//  Function: f_EPTF_UIHandler_init_CT
// 
//  Purpose:
//    Initialises the EPTF_UIHandler_CT
// 
//  Parameters:
//    pl_selfName - *in* *charstring* - the name used in the initialization
//      of the f_EPTF_Base_CT
//    pl_CLI_compRef - *in* <EPTF_CLI_CT> - the CLI to use.
//      Default: null (== local CLI ports are disabled)
//    pl_connectGUI - *in* *boolean* - default: true. If it is true
//      UIHandler will try to connect to the RuntimeGUI and will perform
//      the XTDP handshake and authentication. The init function will
//      exit after the XTDP session is connected. If the transport connection
//      failed but the headless mode is true, the init function will exit
//      with XTDP session not established and the UIHandler will enter
//      into headless mode.
//    pl_windowLayout - *in* *charstring* - This parameter defines the initial
//      gui layout. If it is empty (default value), no initial layout is
//      sent to the RuntimeGUI. The charstring has to be able to be decoded by
//      the XTDP TestPort decode function
//    pl_prefix - *in* *charstring* - (default:tsp_EPTF_UIHandler_CLIPrefix) -
//      common prefix for the UIHandler Command-Line Interface commands
// 
//  Return Value:
//    -
// 
//  Errors:
//    Error occurs in client mode (i.e. if tsp_EPTF_UIHandler_servermode is false)
//    if
//     - pl_connectGUI true, headless mode is false, and it is not possible
//       to connect to the RuntimeGUI.
//     - pl_connectGUI false and headless mode is false
//     - pl_connectGUI true but XTDP handshake&authentication is not
//       performed in time (during tsp_EPTF_UIHandler_maxGUIWaitTime seconds)
//
//  Detailed Comments:
//    This function should be called before using the EPTF_UIHandler_CT
//    component.
//
//    It is possible to start the RuntimeGUI with a XUL file. The layout
//    defined in that XUL will be used by UIHandler as an initial layout
//    only if all of the following is true:
//      - this RuntimeGUI is the first GUI connected to the UIHandler
//         and established XDTP session successfully (handshake+authentication)
//      - pl_windowLayout is empty
//      - tsp_EPTF_GUI_DefaultWindow is empty
//
///////////////////////////////////////////////////////////
public function f_EPTF_UIHandlerXUL_init_CT(
  in charstring pl_selfName, 
  in boolean pl_connectGUI := true, 
  in charstring pl_windowLayout := "",
  in EPTF_CLI_CT pl_CLI_compRef := null,
  in charstring pl_prefix := tsp_EPTF_UIHandler_CLIPrefix)
runs on EPTF_UIHandler_Private_CT {
  if (not v_UIHandler_initialized) {
    v_UIHandler_windowLayout := pl_windowLayout;
    v_EPTF_UIHandler_createLayoutCallback := refers(f_EPTF_UIHandler_createInitialLayout);
    v_EPTF_UIHandler_createLayoutCallback := null;
    f_EPTF_UIHandler_initComponent(pl_selfName, pl_CLI_compRef, pl_prefix);
    if(f_EPTF_UIHandler_layoutReady()) {
      f_EPTF_UIHandler_createInitialLayout(); // initial window is created in internal DB before trying to connect to GUI
    }
    f_EPTF_UIHandler_connect(pl_connectGUI);
  }
}


///////////////////////////////////////////////////////////
//  Function: f_EPTF_UIHandler_createInitialLayout
//
//  Purpose:
//    Create initial layout based on settings
///////////////////////////////////////////////////////////
private function f_EPTF_UIHandler_createInitialLayout() 
runs on EPTF_UIHandler_Private_CT 
{
  var boolean vl_ret := false;
  if("" == v_UIHandler_windowLayout and "" == tsp_EPTF_GUI_DefaultWindow){
    // no window is given -> create a simple window and a main tabbox
    f_EPTF_UIHandler_debug(%definitionId&": Creating a window and a main tabbox");
    vl_ret := f_EPTF_UIHandler_addWindow();
    vl_ret := vl_ret and f_EPTF_UIHandler_addMainTabbox();
  }
  else {
    var Widgets vl_wndXul;
    var boolean vl_widgetExists := false;
    if ("" != v_UIHandler_windowLayout) {
      // create window given in parameter
      f_EPTF_UIHandler_debug(%definitionId&": Creating window specified by function parameter");
      //f_EPTF_UIHandler_convertWidgets(f_EPTF_UIHandler_decodeXUL(v_UIHandler_windowLayout),vl_wndXul);
      vl_wndXul := f_EPTF_UIHandler_XSD_decodeXUL(v_UIHandler_windowLayout);
    } 
    else if ("" != tsp_EPTF_GUI_DefaultWindow) {
      // create window from specified in tsp
      f_EPTF_UIHandler_debug(%definitionId&": Creating window specified by tsp_EPTF_GUI_DefaultWindow");
      //f_EPTF_UIHandler_convertWidgets(f_EPTF_UIHandler_decodeXUL(tsp_EPTF_GUI_DefaultWindow),vl_wndXul);
      vl_wndXul := f_EPTF_UIHandler_XSD_decodeXUL(tsp_EPTF_GUI_DefaultWindowXTDL);
    }
    vl_ret := f_EPTF_UIHandler_XSD_addElementToGui(vl_wndXul, "", vl_widgetExists, true);
  }
  if (not vl_ret) {
    f_EPTF_UIHandler_debug(%definitionId&": f_EPTF_UIHandler_addElementToGui failed, could not create initial GUI");
  }
}




}  // end of module
