///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_UIHandler_ChartDataTypeRingBuffer_Functions
// 
//  Purpose:
//    This module contains type definitions, include statements and macro definitions 
//    for a ChartDataType type ringbuffer
// 
//  Module depends on:
//    <EPTF_CLL_GenericRingBuffer_Functions.ttcnin>
// 
//  Current Owner:
//   Balazs Lugossy (eballug)
// 
//  Last Review Date:
//    2012-08-02
// 
///////////////////////////////////////////////////////////

module EPTF_CLL_UIHandler_ChartDataTypeRingBuffer_Functions
{
  type record UIHandler_ChartDataType{
    float data,
    float timestamp
  }
#define EPTF_BASE_TYPE UIHandler_ChartDataType
#include "EPTF_CLL_GenericRingBuffer_Functions.ttcnin"
#undef EPTF_BASE_TYPE
}
