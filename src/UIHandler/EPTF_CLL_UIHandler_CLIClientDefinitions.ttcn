///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_UIHandler_CLIClientDefinitions
// 
//  Purpose:
//    This module contains constants related to the CLI functions of the UIHandler
//
//  Module Parameters:
//    *boolean* tsp_debug_EPTF_UIHandler_CLI_Functions
//    *float* ts_CLI_refreshRate - sets the frequency of the display of variables
//    on the display terminal
// 
//  Module depends on:
//
//  Current Owner:
//    Laszlo Skumat (ELSZSKU)
// 
//  Last Review Date:
//    2007-
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////////
module EPTF_CLL_UIHandler_CLIClientDefinitions
{
import from EPTF_CLL_Common_Definitions all;

friend module EPTF_CLL_UIHandler_WidgetFunctions;

modulepar boolean tsp_debug_EPTF_UIHandler_CLI_Functions := false;
modulepar float ts_CLI_refreshRate := 5.0
///////////////////////////////////////////////////////////
// Private Template: t_UIHandler_CLIClient_errEmptyVarName
// Purpose:
//   The name of the variable is empty in the CLI command. 
///////////////////////////////////////////////////////////////
friend function f_UIHandler_CLIClient_errEmptyVarName(charstring pl_command) 
return charstring{ 
  return "The name of the variable mustn't be empty! Command: " & pl_command;
}
///////////////////////////////////////////////////////////
// Private Template: t_UIHandler_CLIClient_errInvalidVarName
// Purpose:
//   There is no variable with the name specified in the CLI command. 
///////////////////////////////////////////////////////////////
friend function f_UIHandler_CLIClient_errInvalidVarName(charstring pl_varName, charstring pl_command) 
return charstring{ 
  return "The variable """&pl_varName&""" doesn't exist! Command: " & pl_command;
}
///////////////////////////////////////////////////////////
// Private Template: t_UIHandler_CLIClient_errEmptyContent
// Purpose:
//   The new content of the variable is empty in the CLI command. 
///////////////////////////////////////////////////////////////
friend function f_UIHandler_CLIClient_errEmptyContent(charstring pl_command) 
return charstring{ 
  return "The new content of the variable mustn't be empty! Command: " & pl_command;
}
///////////////////////////////////////////////////////////
// Private Template: t_UIHandler_CLIClient_errInvalidContent
// Purpose:
//   The new content of the variable in the CLI command is invalid. 
///////////////////////////////////////////////////////////////
friend function f_UIHandler_CLIClient_errInvalidContent(charstring pl_newContent, charstring pl_command)
return charstring{ 
  return "The new content of the variable is invalid! Content: "&pl_newContent&" Command: "&pl_command;
}
///////////////////////////////////////////////////////////
// Private Template: t_UIHandler_CLIClient_errVarIsReadonly
// Purpose:
//   The variable is read-only.
///////////////////////////////////////////////////////////////
friend function f_UIHandler_CLIClient_errVarIsReadonly(charstring pl_varName)
return charstring{ 
  return "Cannot set variable " & pl_varName & "\nType lswritable to list variables that can be set.";
}
///////////////////////////////////////////////////////////
// Private Template: t_UIHandler_CLIClient_setContentOK
// Purpose:
//   The new content was set. 
///////////////////////////////////////////////////////////////
friend function f_UIHandler_CLIClient_setContentOK(charstring vl_varName,charstring vl_newContent)
return charstring{ 
  return "Set content OK. Variable: """&vl_varName&""" content: "&vl_newContent;
}
///////////////////////////////////////////////////////////
// Private Template: t_UIHandler_CLIClient_varContentAnswer
// Purpose:
//   Answer message of a succesful content change command. 
///////////////////////////////////////////////////////////////
friend function f_UIHandler_CLIClient_varContentAnswer(charstring pl_varName, charstring pl_newContent)
return charstring{ 
  return pl_varName&":="&pl_newContent;
}

///////////////////////////////////////////////////////////
// Private Template: t_UIHandler_CLIClient_hideAnswer
// Purpose:
//   Answer message of a succesful variable hide command. 
///////////////////////////////////////////////////////////////
friend function f_UIHandler_CLIClient_hideAnswer(charstring pl_varName) 
return charstring{ 
  return pl_varName&" has been hidden.";
}
///////////////////////////////////////////////////////////
// Private Template: t_UIHandler_CLIClient_unknownCommand
// Purpose:
//   Error message when the command can not be recognized. 
///////////////////////////////////////////////////////////////
friend function f_UIHandler_CLIClient_unknownCommand(charstring pl_command)
return charstring{ 
  return "Unknown command: "&pl_command;
}
///////////////////////////////////////////////////////////
// Private Const: c_UIHandler_CLIClient_notImplemented
// Purpose:
//   "Not implemented." 
///////////////////////////////////////////////////////////////
friend const charstring c_UIHandler_CLIClient_notImplemented := "Not implemented.";

friend const charstring c_UIHandler_CLIClient_cmdUsage := "Usage: \n"; 
friend const charstring c_UIHandler_CLIClient_setUsage := "set floatVar := 3.41 \n set charstringVar :=  value \n set \"Variable with space in the name\" := \"Value with space in it\" \n set \"Variable with \\\" in the name\" := \"Value with \\\" in it\"";
friend const charstring c_UIHandler_CLIClient_getUsage := "get floatVar";
friend const charstring c_UIHandler_CLIClient_displayUsage := "display floatVar";
friend const charstring c_UIHandler_CLIClient_hideUsage1 := "hide floatVar";
friend const charstring c_UIHandler_CLIClient_hideUsage2 := "hide";

friend const EPTF_CharstringList c_UIHandler_CLIClient_commandList:={
  "GET",
  "SET",
  "LS",
  "LSREADONLY",
  "LSWRITABLE",
  "DISPLAY",
  "HIDE",
  "QUIT"//,
  //"STOP",
  //"HELP"
}

friend const integer c_UIHandler_CLIClient_commandGETIdx :=0
friend const integer c_UIHandler_CLIClient_commandSETIdx :=1
friend const integer c_UIHandler_CLIClient_commandLSIdx :=2
friend const integer c_UIHandler_CLIClient_commandLSREADONLYIdx :=3
friend const integer c_UIHandler_CLIClient_commandLSWRITABLEIdx :=4
friend const integer c_UIHandler_CLIClient_commandDISPLAYIdx :=5
friend const integer c_UIHandler_CLIClient_commandHIDEIdx :=6
friend const integer c_UIHandler_CLIClient_commandQUITIdx :=7
//friend const integer c_UIHandler_CLIClient_commandSTOPIdx :=8
//friend const integer c_UIHandler_CLIClient_commandHELPIdx :=9

//command(\S*)(\s*):= \"(.*)"
//command\1 := c_UIHandler_CLIClient_commandList\[c_UIHandler_CLIClient_command\1Idx\]
friend const charstring c_UIHandler_CLIClient_commandGET := c_UIHandler_CLIClient_commandList[c_UIHandler_CLIClient_commandGETIdx]
friend const charstring c_UIHandler_CLIClient_commandSET := c_UIHandler_CLIClient_commandList[c_UIHandler_CLIClient_commandSETIdx]
friend const charstring c_UIHandler_CLIClient_commandLS := c_UIHandler_CLIClient_commandList[c_UIHandler_CLIClient_commandLSIdx]
friend const charstring c_UIHandler_CLIClient_commandLSREADONLY := c_UIHandler_CLIClient_commandList[c_UIHandler_CLIClient_commandLSREADONLYIdx]
friend const charstring c_UIHandler_CLIClient_commandLSWRITABLE := c_UIHandler_CLIClient_commandList[c_UIHandler_CLIClient_commandLSWRITABLEIdx]
friend const charstring c_UIHandler_CLIClient_commandDISPLAY := c_UIHandler_CLIClient_commandList[c_UIHandler_CLIClient_commandDISPLAYIdx]
friend const charstring c_UIHandler_CLIClient_commandHIDE := c_UIHandler_CLIClient_commandList[c_UIHandler_CLIClient_commandHIDEIdx]
friend const charstring c_UIHandler_CLIClient_commandQUIT := c_UIHandler_CLIClient_commandList[c_UIHandler_CLIClient_commandQUITIdx]
//friend const charstring c_UIHandler_CLIClient_commandSTOP := c_UIHandler_CLIClient_commandList[c_UIHandler_CLIClient_commandSTOPIdx]
//friend const charstring c_UIHandler_CLIClient_commandHELP := c_UIHandler_CLIClient_commandList[c_UIHandler_CLIClient_commandHELPIdx]

friend const charstring c_UIHandler_CLIClient_commandPrefix := "UI_"
///////////////////////////////////////////////////////////
// Private Const: c_UIHandler_CLIClient_help
// Purpose:
//   The answer message of the "HELP" command. 
///////////////////////////////////////////////////////////////
friend const charstring c_UIHandler_CLIClient_help :=
"\n"//&


friend const charstring c_UIHandler_CLIClient_commandGETHelp := c_UIHandler_CLIClient_commandGET&" - Retrieves the value of the specified variable \n"&
"ex: "&c_UIHandler_CLIClient_getUsage
friend const charstring c_UIHandler_CLIClient_commandSETHelp := c_UIHandler_CLIClient_commandSET&" - Sets the value of the specified variable \n"&
"ex: "&c_UIHandler_CLIClient_setUsage
friend const charstring c_UIHandler_CLIClient_commandLSHelp := c_UIHandler_CLIClient_commandLS&" - Lists all GUI variables and their current values"
friend const charstring c_UIHandler_CLIClient_commandLSREADONLYHelp := c_UIHandler_CLIClient_commandLSREADONLY&" - Lists read-only GUI variables\n"
friend const charstring c_UIHandler_CLIClient_commandLSWRITABLEHelp := c_UIHandler_CLIClient_commandLSWRITABLE&" - Lists writable GUI variables (that can be SET)"
friend const charstring c_UIHandler_CLIClient_commandDISPLAYHelp := c_UIHandler_CLIClient_commandDISPLAY&" - Periodically displays the value of the specified variable on the display consol\n"&
"ex: \n"&c_UIHandler_CLIClient_displayUsage
friend const charstring c_UIHandler_CLIClient_commandHIDEHelp := c_UIHandler_CLIClient_commandHIDE&" - Stops the periodical display of the specified variable or all the variables\n"&
"ex: "&c_UIHandler_CLIClient_hideUsage1&"\n"&
"ex: "&c_UIHandler_CLIClient_hideUsage2
//c_UIHandler_CLIClient_commandINITGUI&" - Connects the UIHandler to the runtime GUI - not implemented yet \n\n"&
//c_UIHandler_CLIClient_commandCLOSEGUI&" - Disconnects the UIHandler from the runtime GUI - not implemented yet \n\n"&
//friend const charstring c_UIHandler_CLIClient_commandQUITHelp := c_UIHandler_CLIClient_commandQUIT&" - Quit the telnet session\n"&
//"ex: quit"
//friend const charstring c_UIHandler_CLIClient_commandSTOPHelp := c_UIHandler_CLIClient_commandSTOP&" - Controlled stop of the execution\n"&
//"ex: stop"//&
//c_UIHandler_CLIClient_commandHELP&" - Prints this message\n"&
//"ex: help\n\n";

///////////////////////////////////////////////////////////
// Private Const: c_UIHandler_CLIClient_help
// Purpose:
//   The answer message of the "HELP" command. 
///////////////////////////////////////////////////////////////
friend const charstring c_UIHandler_CLIClient_usage :=
"\n\
usage: \n\
"&c_UIHandler_CLIClient_help;


}  // end of module
