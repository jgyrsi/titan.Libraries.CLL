///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_UIHandlerCLI_Functions
// 
//  Purpose:
//    This module contains CLI interface functions for the UIHandler.
// 
//  Module depends on:
//    <EPTF_CLL_Logging_Definitions>
//    <EPTF_CLL_Logging_Functions>
// 
//  Current Owner:
//    Laszlo Skumat (ELSZSKU)
// 
//  Last Review Date:
//    2007-xx-xx
//
//  Detailed Comments:
//    This module contains CLI connection functions. 
//
///////////////////////////////////////////////////////////////
module EPTF_CLL_UIHandlerCLI_Functions
// [.objid{ itu_t(0) identified_organization(4) etsi(0) 
// identified_organization(127) ericsson(5) testing(0)
// <put further nodes here if needed>}]
{
import from EPTF_CLL_UIHandler_Definitions all;

import from EPTF_CLL_UIHandler_WidgetFunctions all;
import from EPTF_CLL_UIHandlerCLI_Definitions all;
import from EPTF_CLL_CLI_Definitions all;
import from EPTF_CLL_CLI_Functions all;
import from EPTF_CLL_UIHandler_XULFunctions all;


//=========================================================================
// Import Part
//=========================================================================

///////////////////////////////////////////////////////////
// Function: f_EPTF_UIHandler_CLI_init_CT
// Purpose:
//   The initialization function. 
///////////////////////////////////////////////////////////////
public function f_EPTF_UIHandler_CLI_init_CT(
  in charstring pl_selfName,
  in boolean pl_connectGUI := true, 
  in charstring pl_windowLayout := "",
  in EPTF_CLI_CT pl_CLI_compRef := null,
  in charstring pl_prefix := tsp_EPTF_UIHandler_CLIPrefix)
runs on EPTF_UIHandler_CLI_CT {
  if (pl_CLI_compRef==null) {
    pl_CLI_compRef := self; // null is interpreted as self (TITAN: self cannot be default!!)
  }
  // enable to telnet ports
  f_EPTF_CLI_init_CT(pl_selfName);
  f_EPTF_UIHandlerXUL_init_CT(pl_selfName, pl_connectGUI, pl_windowLayout, pl_CLI_compRef,pl_prefix);
}

///////////////////////////////////////////////////////////
// Function: f_EPTF_UIHandler_CLI_sendUserMessage
// Purpose:
//   Send a custom message to be displayed to the user. 
///////////////////////////////////////////////////////////////
public function f_EPTF_UIHandler_CLI_sendUserMessage(
  in charstring pl_msg)
runs on EPTF_UIHandler_CLI_CT {
  f_EPTF_UIHandler_CLIClient_sendUserMessage(pl_msg)
}

///////////////////////////////////////////////////////////
// Function: f_EPTF_UIHandler_CLI_logAll
//
// Purpose:
//   Log all internal variables in EPTF_UIHandler_CLI_CT component.
//
// Detailed Comments:
//   
///////////////////////////////////////////////////////////////
public function f_EPTF_UIHandler_CLI_logAll() runs on EPTF_UIHandler_CLI_CT {
  f_EPTF_UIHandler_CLIClient_logAll()
}//f_

}  // end of module
