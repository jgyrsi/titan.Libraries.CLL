///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_UIHandler_MsgDefinitions
// 
//  Purpose:
//    This module contains definitions of messages between UIHandler and UIHandlerClient components.
// 
//  Module depends on:
//    <XTDP_PDU_Defs>
//    <EPTF_CLL_Variable_Definitions>
// 
//  Current Owner:
//    Laszlo Skumat (ELSZSKU)
// 
//  Last Review Date:
//    2007-xx-xx
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////////
module EPTF_CLL_UIHandler_MsgDefinitions
// [.objid{ itu_t(0) identified_organization(4) etsi(0) 
// identified_organization(127) ericsson(5) testing(0)
// <put further nodes here if needed>}]
{

//=========================================================================
// Import Part
//=========================================================================
import from EPTF_CLL_Variable_Definitions all;
//import from EPTF_CLL_UIHandler_Definitions all;
import from ttcn_ericsson_se_protocolModules_xtdp_xtdl all;

//=========================================================================
// Data Types
//=========================================================================

///////////////////////////////////////////////////////////
//  Type: EPTF_UIHandler_HandledWidgetType
// 
//  Purpose:
//    List of widget types
///////////////////////////////////////////////////////////
type enumerated EPTF_UIHandler_HandledWidgetType{
  wtUnknown,
  wtWindow,
  wtTabbox,
  wtTabs,
  wtTab,
  wtTabpanels,
  wtTabpanel,
  wtTabpages,
  wtTabpage,
  wtTree,
  wtTreecols,
  wtTreecol,
  wtTreechildren,
  wtTreeitem,
  wtTreerow,
  wtTreecell,
  wtHbox,
  wtLabel,
  wtSpacer,
  wtButton,
  wtTextbox,
  wtChart,
  wtTraceList,
  wtTrace,
  wtToolbar,
  wtToolbarbuttons,
  wtToolbarbutton,
  wtSeparator,
  wtListbox,
  wtListitem,
  wtNumericalwidget,
  wtMenulist,
  wtMenuitem,
  wtDistributionchart,
  wtIntervallimits,
  wtValuelist,
  wtImage,
  wtPushbutton,
  wtTogglebutton,
  wtHtmlcode,
  wtMetaIterator
}

//type charstring EPTF_UIHandler_WidgetIdString;
///////////////////////////////////////////////////////////
//  Type: EPTF_UIHandler_WidgetIdString
//
//  Purpose:
//    Type alias of *charstring*.
///////////////////////////////////////////////////////////
type charstring EPTF_UIHandler_WidgetIdString;

type union EPTF_UIHandler_AnyWidget {
	Tabpages tabpages,
	Tree tree,
	Hbox hbox,
	Label label_,
	Spacer spacer,
	Button button,
	Textbox textbox,
	Numericalwidget numericalwidget,
	Chart chart,
	Distributionchart distributionchart,
	Toolbar toolbar,
	Toolbarbutton toolbarbutton,
	Listbox listbox,
	Menulist menulist,
	Pushbutton pushbutton,
	Togglebutton togglebutton,
	Htmlcode htmlcode
}

type record of EPTF_UIHandler_AnyWidget EPTF_UIHandler_WidgetList;

group SubscriptionMessages{
  ///////////////////////////////////////////////////////////
  //  Type: EPTF_CLL_UISubscribeNote
  // 
  //  Purpose:
  //   This record defines the EPTF_CLL_UISubscribeNote element of the <EPTF_CLL_UISubscMgmtMsg>
  // 
  //  Elements:
  //    providedParamName - <charstring> - the name of the variable to subscribe
  //    idx - <integer> - the index of the variable in the provider
  // 
  //  Detailed Comments:
  //    The provider can order the CLL_UIHandler to:
  //    1) subscribe a component to a variable, create a new Var in its EPTF_Vars, and connect
  //       its data to a widget. Then all the members have to be filled.
  //    2) subscribe a component to a variable, create a new Var in its EPTF_Vars, but not connect
  //       its data to a widget. Then providedParamName and destParamName has to be filled,
  //       widgetId has to be omit
  //    3) Connect a previously subscribed variable to a widget. Then the providedParamName and 
  //       widgetId has to be filled, destParamName has to be omit.
  //
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_SubscribeNoteMsg {
    EPTF_Var_CT remoteCompRef,
    charstring providedParamName,
    charstring destParamName optional,
    EPTF_UIHandler_WidgetIdString widgetId optional,
    EPTF_Var_SubscriptionMode subsMode,
    integer messageId,
    integer refreshRate
  }
  
  ///////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_SubscribeAck
  // 
  //  Purpose:
  //   Ackowledgement message about a EPTF_UIHandler_SubscribeNoteMsg message
  // 
  //  Elements:
  //    requestId - *integer* - The messageId of the request
  //    varIdx - *integer* - The index of the created variable in the UIHandler
  // 
  //  Detailed Comments:
  //
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_SubscribeAck {
    integer requestId,
    integer varIdx
  }
  
  ///////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_SubscribeNAck
  // 
  //  Elements:
  //    requestId - *integer* - The messageId of the request
  // 
  //  Purpose:
  //   Ackowledgement message about a EPTF_UIHandler_SubscribeNoteMsg message
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_SubscribeNAck {
  integer requestId
  }
}
group WidgetMessages{
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminEnableWidgetByWidgetName
  // 
  //  Purpose:
  //   Record type for messages to enable a GUI widget by an explicit
  //   widget name
  // 
  //  Elements:
  //   widgetName - *EPTF_UIHandler_WidgetIdString* - the name of the widget to enable
  //   messageID - *integer* - the message ID
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminEnableWidgetByWidgetName
  {
    EPTF_UIHandler_WidgetIdString widgetName,
    integer messageID
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminDisableWidgetByWidgetName
  // 
  //  Purpose:
  //   Record type for messages to disable a GUI widget by an explicit
  //   widget name
  //   messageID - *integer* - the message ID
  // 
  //  Elements:
  //   widgetName - *EPTF_UIHandler_WidgetIdString* - the name of the widget to disable
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminDisableWidgetByWidgetName
  {
    EPTF_UIHandler_WidgetIdString widgetName,
    integer messageID
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminDisableWidgetByIdx
  // 
  //  Purpose:
  //   Record type for messages to disable all the GUI widgets
  //	 connected to the given variable
  // 
  //  Elements:
  //   index - *integer* - the index of the variable 
  //   messageID - *integer* - the message ID
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminDisableWidgetByIdx
  {
    integer index,
    integer messageID
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminEnableWidgetByIdx
  // 
  //  Purpose:
  //   Record type for messages to disable all the GUI widgets
  //	 connected to the given variable
  // 
  //  Elements:
  //   index - *integer* - the index of the variable
  //   messageID - *integer* - the message ID
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminEnableWidgetByIdx
  {
    integer index,
    integer messageID
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminEnableWidgetAck
  // 
  //  Purpose:
  //   -
  // 
  //  Elements:
  //   messageID - *integer* - the ID of the message from the enableWidget request
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminEnableWidgetAck {
    integer           messageID
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminEnableWidgetNAck
  // 
  //  Purpose:
  //   -
  // 
  //  Elements:
  //   messageID - *integer* - the ID of the message from the enableWidget request
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminEnableWidgetNAck {
    integer           messageID
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminDisableWidgetAck
  // 
  //  Purpose:
  //   -
  // 
  //  Elements:
  //   messageID - *integer* - the ID of the message from the enableWidget request
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminDisableWidgetAck {
    integer           messageID
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminDisableWidgetNAck
  // 
  //  Purpose:
  //   -
  // 
  //  Elements:
  //   messageID - *integer* - the ID of the message from the enableWidget request
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminDisableWidgetNAck {
    integer           messageID
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminSetFocusToWidget
  // 
  //  Purpose:
  //   Record type for messages to set the focus to a GUI widget by an explicit
  //   widget name
  // 
  //  Elements:
  //   widgetName - *EPTF_UIHandler_WidgetIdString* - the name of the widget being focused
  //   messageID - *integer* - the message ID
  //   tabIndex - the index of the tab being focused. Can be used only if widget is a tabbox
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminSetFocusToWidget
  {
    EPTF_UIHandler_WidgetIdString widgetName,
    integer messageID,
    integer tabIndex optional
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminSetFocusToWidgetAck
  // 
  //  Purpose:
  //   -
  // 
  //  Elements:
  //   messageID - *integer* - the ID of the message from the setFocusToWidget request
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminSetFocusToWidgetAck {
    integer           messageID
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminSetFocusToWidgetNAck
  // 
  //  Purpose:
  //   -
  // 
  //  Elements:
  //   messageID - *integer* - the ID of the message from the enableWidget request
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminSetFocusToWidgetNAck {
    integer           messageID
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminAddElementToGUI
  // 
  //  Purpose:
  //   To add an element to the Runtime GUI
  // 
  //  Elements:
  //   parentWidgetId - *EPTF_UIHandler_WidgetIdString* - optional the widget id of the element
  //                                   to add the new element to
  //   messageID - *integer* - the ID of the message to identify the acknowledgement
  //   xul - *XTDP_XML_Tag* - the added element
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminAddElementToGUI
  {
    EPTF_UIHandler_WidgetIdString    parentWidgetId optional,
    integer           messageID,
    Widgets      xul
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminAddElementToGUIAck
  // 
  //  Purpose:
  //   Positive akcnowledgement of an addElementToGUI message.
  // 
  //  Elements:
  //   messageID - *integer* - the ID of the message from the addElementToGUI request
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminAddElementToGUIAck {
    integer         messageID,
    boolean			widgetExists
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminAddElementToGUINAck
  // 
  //  Purpose:
  //   The negative acknowledgement of an addElementToGUI message
  // 
  //  Elements:
  //   messageID - *integer* - the ID of the message from the addElementToGUI request
  //                                   to add the new element to
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminAddElementToGUINAck {
    integer           messageID
  }
  
    ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminCreateGUI
  // 
  //  Purpose:
  //   To add a gui part to the GUI
  // 
  //  Elements:
  //   xul - *charstring* - the added element
  //   parentWidgetId - *EPTF_UIHandler_WidgetIdString* - optional the widget id of the element
  //                                   to add the new element to
  //   messageID - *integer* - the ID of the message to identify the acknowledgement
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminCreateGUI
  {
    charstring                       xul,
    EPTF_UIHandler_WidgetIdString    parentWidgetId optional,
    integer                          messageID
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminCreateGUIAck
  // 
  //  Purpose:
  //   Positive akcnowledgement of an createGUI message.
  // 
  //  Elements:
  //   messageID - *integer* - the ID of the message from the createGUI request
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminCreateGUIAck {
    integer         messageID
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminCreateGUINAck
  // 
  //  Purpose:
  //   The negative acknowledgement of a createGUI message
  // 
  //  Elements:
  //   messageID - *integer* - the ID of the message from the createGUI request
  //                                   to add the new element to
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminCreateGUINAck {
    integer           messageID
  }
  
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminRemoveElementFromGUI
  // 
  //  Purpose:
  //   Record type for messages to remove an element from the Runtime GUI
  // 
  //  Elements:
  //   widgetId - *EPTF_UIHandler_WidgetIdString* - the widget id of the element
  //                             to remove
  //   messageID - *integer* - the message ID
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminRemoveElementFromGUI
  {
    EPTF_UIHandler_WidgetIdString        widgetId,
    integer               messageID
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminRemoveElementFromGUIAck
  // 
  //  Purpose:
  //   To Positive akcnowledgement of an removeElementFromGUI message
  // 
  //  Elements:
  //   widgetId - *EPTF_UIHandler_WidgetIdString* - the widget id of the element
  //                             to remove
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminRemoveElementFromGUIAck
  {
    EPTF_UIHandler_WidgetIdString        widgetId
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminRemoveElementFromGUINAck
  // 
  //  Purpose:
  //   The negative acknowledgement of an removeElementFromGUI message
  // 
  //  Elements:
  //   widgetId - *EPTF_UIHandler_WidgetIdString* - the widget id of the element
  //                             to remove
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminRemoveElementFromGUINAck
  {
    EPTF_UIHandler_WidgetIdString        widgetId
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminWidgetExists
  // 
  //  Purpose:
  //   Retrieves whether the widget exists on the GUI
  // 
  //  Elements:
  //   widgetName - *EPTF_UIHandler_WidgetIdString* - the name of the widget to disable
  //   messageID - *integer* - the message ID
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminWidgetExists
  {
    EPTF_UIHandler_WidgetIdString widgetName,
    integer messageID
  }
  
  ////////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_AdminWidgetExistsResp
  // 
  //  Purpose:
  //   Retrieves whether the widget exists on the GUI
  // 
  //  Elements:
  //   widgetName - *EPTF_UIHandler_WidgetIdString* - the name of the widget to disable
  //   messageID - *integer* - the message ID
  //
  //  Detailed Comments:
  //    Response message.
  ///////////////////////////////////////////////////////////
  type record EPTF_UIHandler_AdminWidgetExistsResp
  {
    EPTF_UIHandler_WidgetIdString widgetName,
    integer messageID,
    EPTF_UIHandler_HandledWidgetType widgetType
  }
  
  ///////////////////////////////////////////////////////////
  //  Type: EPTF_UIHandler_WidgetMsg
  // 
  //  Purpose:
  //     Union type for MainAdmin-Interface messages.
  // 
  //  Elements:
  //   enableWidgetByIdx,
  //   disableWidgetByIdx,
  //   enableWidgetByWidgetName,
  //   disableWidgetByWidgetName,
  //   addElementToGUI,
  //   addElementToGUIAck,
  //   addElementToGUINAck,
  //   removeElementFromGUI,
  //   removeElementFromGUIAck,
  //   removeElementFromGUINAck,
  // 
  //   EPTF_UIHandler_SubscribeMeNoteMsg subscribeMeNote, // the provider orders the subscriber to subscribe to the given parameter
  //   EPTF_UIHandler_SubscribeMeAck subscribeMeAck,   //The subscriber accepts the note
  //   EPTF_UIHandler_SubscribeMeAck subscribeMeNAck   //The subscriber refuses the note
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////  
  type union EPTF_UIHandler_WidgetMsg
  {
    EPTF_UIHandler_AdminEnableWidgetByIdx		    enableWidgetByIdx,
    EPTF_UIHandler_AdminDisableWidgetByIdx         disableWidgetByIdx,
    EPTF_UIHandler_AdminEnableWidgetByWidgetName   enableWidgetByWidgetName,
    EPTF_UIHandler_AdminDisableWidgetByWidgetName  disableWidgetByWidgetName,
    EPTF_UIHandler_AdminEnableWidgetAck            enableWidgetAck,
    EPTF_UIHandler_AdminDisableWidgetAck           disableWidgetAck,
    EPTF_UIHandler_AdminEnableWidgetNAck           enableWidgetNAck,
    EPTF_UIHandler_AdminDisableWidgetNAck          disableWidgetNAck,
    EPTF_UIHandler_AdminSetFocusToWidget           setFocusToWidget,
    EPTF_UIHandler_AdminSetFocusToWidgetAck        setFocusToWidgetAck,
    EPTF_UIHandler_AdminSetFocusToWidgetNAck       setFocusToWidgetNAck,
    EPTF_UIHandler_AdminAddElementToGUI            addElementToGUI,
    EPTF_UIHandler_AdminAddElementToGUIAck         addElementToGUIAck,
    EPTF_UIHandler_AdminAddElementToGUINAck        addElementToGUINAck,
    EPTF_UIHandler_AdminRemoveElementFromGUI       removeElementFromGUI,
    EPTF_UIHandler_AdminRemoveElementFromGUIAck    removeElementFromGUIAck,
    EPTF_UIHandler_AdminRemoveElementFromGUINAck   removeElementFromGUINAck,
    EPTF_UIHandler_AdminWidgetExists				widgetExists,
    EPTF_UIHandler_AdminWidgetExistsResp			widgetExistsResp,
    
    EPTF_UIHandler_SubscribeNoteMsg subscribeNoteMsg, // the provider orders the subscriber to subscribe to the given parameter
    EPTF_UIHandler_SubscribeAck subscribeAck,   //The subscriber accepts the note
    EPTF_UIHandler_SubscribeAck subscribeNAck,  //The subscriber refuses the note
    EPTF_UIHandler_AdminCreateGUI                  createGUI,
    EPTF_UIHandler_AdminCreateGUIAck               createGUIAck,
    EPTF_UIHandler_AdminCreateGUINAck              createGUINAck
  }

  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminWidgetExists
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) wdgetExists.
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      EPTF_UIHandler_WidgetIdString pWidgetName,
  //      integer pMessageID)  := {
  //      disableWidgetByWidgetName := {  - the message type
  //  	  widgetName       := pWidgetName, - the name of the widget to disable on GUI
  //      messageID        := pMessageID
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminWidgetExists(
    template EPTF_UIHandler_WidgetIdString pWidgetName,
    template integer pMessageID) :=
  {
    widgetExists := {
      widgetName        := pWidgetName,
      messageID         := pMessageID
    }
  }
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminWidgetExistsResp
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) wdgetExists.
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      EPTF_UIHandler_WidgetIdString pl_WidgetName,
  //      integer pl_MessageID,
  //    template EPTF_UIHandler_HandledWidgetType pl_WidgetType) :=
  //      widgetExistsResp 	:= {  - the message type
  //  	  widgetName       	:= pl_WidgetName, - the name of the widget to disable on GUI
  //      messageID        	:= pl_MessageID,
  //      widgetType		:= pl_WidgetType
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminWidgetExistsResp(
    template EPTF_UIHandler_WidgetIdString pl_WidgetName,
    template integer pl_MessageID,
    template EPTF_UIHandler_HandledWidgetType pl_WidgetType) :=
  {
    widgetExistsResp := {
      widgetName        := pl_WidgetName,
      messageID         := pl_MessageID,
      widgetType		:= pl_WidgetType
    }
  }
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminEnableWidgetByWidgetName
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) enableWidgetByWidgetName.
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      template EPTF_UIHandler_WidgetIdString pl_WidgetName,
  //      template integer pl_MessageID)  := {
  //      enableWidgetByWidgetName := {  - the message type
  //  	  widgetName       := pl_WidgetName, - the name of the widget to enable on GUI
  //      messageID        := pl_MessageID
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminEnableWidgetByWidgetName(
    template EPTF_UIHandler_WidgetIdString pl_WidgetName,
    template integer pl_MessageID) :=
  {
    enableWidgetByWidgetName := {
      widgetName        := pl_WidgetName,
      messageID         := pl_MessageID
    }
  }
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminEnableWidgetAck
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) enableWidget...
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      template integer pl_MessageID) :=
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminEnableWidgetAck(
    template integer pl_MessageID) :=
  {
    enableWidgetAck := {
      messageID         := pl_MessageID
    }
  }
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminEnableWidgetNAck
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) enableWidget...
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      template integer pl_MessageID) :=
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminEnableWidgetNAck(
    template integer pl_MessageID) :=
  {
    enableWidgetNAck := {
      messageID         := pl_MessageID
    }
  }
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminDisableWidgetNAck
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) enableWidget...
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      template integer pl_MessageID) :=
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminDisableWidgetNAck(
    template integer pl_MessageID) :=
  {
    disableWidgetNAck := {
      messageID         := pl_MessageID
    }
  }
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminDisableWidgetAck
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) enableWidget...
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      template integer pl_MessageID) :=
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminDisableWidgetAck(
    template integer pl_MessageID) :=
  {
    disableWidgetAck := {
      messageID         := pl_MessageID
    }
  }
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminDisableWidgetByWidgetName
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) disableWidgetByWidgetName.
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      template EPTF_UIHandler_WidgetIdString pl_WidgetName)  := {
  //      disableWidgetByWidgetName := {  - the message type
  //  	  widgetName       := pl_WidgetName, - the name of the widget to disable on GUI
  //      messageID        := pl_MessageID
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminDisableWidgetByWidgetName(
    template EPTF_UIHandler_WidgetIdString pl_WidgetName,
    template integer pl_MessageID) :=
  {
    disableWidgetByWidgetName := {
      widgetName        := pl_WidgetName,
      messageID         := pl_MessageID
    }
  }
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminEnableWidgetByIdx
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) enableWidgetByWidgetName.
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      template integer pl_Idx,
  //      template integer pl_MessageID) :=
  //    {
  //      enableWidgetByIdx := {
  //        index        := pl_Idx,
  //        messageID         := pl_MessageID
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminEnableWidgetByIdx(
    template integer pl_Idx,
    template integer pl_MessageID) :=
  {
    enableWidgetByIdx := {
      index        := pl_Idx,
      messageID         := pl_MessageID
    }
  }
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminDisableWidgetByIdx
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) disableWidgetByWidgetName.
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //   template integer pl_Idx,
  //   template integer pl_MessageID) :=
  //      disableWidgetByIdx := {
  //        index        := pl_Idx,
  //        messageID    := pl_MessageID
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminDisableWidgetByIdx(
    template integer pl_Idx,
    template integer pl_MessageID) :=
  {
    disableWidgetByIdx := {
      index        := pl_Idx,
      messageID         := pl_MessageID
    }
  }
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminSetFocusToWidget
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) setFocusToWidget.
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      template EPTF_UIHandler_WidgetIdString pl_WidgetName)  := {
  //      setFocusToWidget := {  - the message type
  //  	  widgetName       := pl_WidgetName, - the name of the widget being focused on GUI
  //      messageID        := pl_MessageID
  //      tabIndex         := pl_TabIndex
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminSetFocusToWidget(
    template EPTF_UIHandler_WidgetIdString pl_WidgetName,
    template integer pl_MessageID,
    template integer pl_TabIndex := omit) :=
  {
    setFocusToWidget := {
      widgetName        := pl_WidgetName,
      messageID         := pl_MessageID,
      tabIndex          := pl_TabIndex 
    }
  }
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminSetFocusToWidgetNAck
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) setFocusToWidget...
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      template integer pl_MessageID) :=
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminSetFocusToWidgetNAck(
    template integer pl_MessageID) :=
  {
    setFocusToWidgetNAck := {
      messageID         := pl_MessageID
    }
  }
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminSetFocusToWidgetAck
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) setFocusToWidget...
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      template integer pl_MessageID) :=
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminSetFocusToWidgetAck(
    template integer pl_MessageID) :=
  {
    setFocusToWidgetAck := {
      messageID         := pl_MessageID
    }
  }
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminAddElementToGUI
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) addElementToGUI.
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      template EPTF_UIHandler_WidgetIdString pl_ParentWidgetId,
  //      template integer pl_MessageID,
  //      template XTDP_XML_Tag pl_Xul)  := {
  //        addElementToGUI := {  - the message type
  //  	    parentWidgetId       := pl_ParentWidgetId, - the id of the widget to add elements to on the GUI
  //          messageID := pl_MessageID,
  //          xul := pl_Xul - the element to add to the Runtime GUI
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminAddElementToGUI(
    template EPTF_UIHandler_WidgetIdString pl_ParentWidgetId,
    template integer pl_MessageID,
    template Widgets pl_Xul) :=
  {
    addElementToGUI := {
      parentWidgetId := pl_ParentWidgetId,
      messageID := pl_MessageID,
      xul := pl_Xul
    }
  }

  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminAddElementToGUIAck
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) addElementToGUIAck.
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      template integer pl_MessageID)  := {
  //        addElementToGUIAck := {  - the message type
  //  	    messageID := pl_MessageID
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminAddElementToGUIAck(
    template integer pl_MessageID,
    template boolean pl_WidgetExists) :=
  {
    addElementToGUIAck := {
      messageID := pl_MessageID,
      widgetExists := pl_WidgetExists
    }
  }
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminAddElementToGUINAck
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) addElementToGUINAck.
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      template integer pl_MessageID)  := {
  //        addElementToGUINAck := {  - the message type
  //  	    messageID := pl_MessageID
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminAddElementToGUINAck(
    template integer pl_MessageID) :=
  {
    addElementToGUINAck:= {
      messageID := pl_MessageID
    }
  }
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminCreateGUI
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) createGUI.
  //
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminCreateGUI(
    template charstring pl_xul,
    template EPTF_UIHandler_WidgetIdString pl_ParentWidgetId,
    template integer pl_MessageID ) :=
  {
    createGUI := {
      xul := pl_xul,
      parentWidgetId := pl_ParentWidgetId,
      messageID := pl_MessageID
    }
  }

  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminCreateGUIAck
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) CreateGUIAck.
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      template integer pl_MessageID)  := {
  //        addElementToGUIAck := {  - the message type
  //        messageID := pl_MessageID
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminCreateGUIAck(
    template integer pl_MessageID) :=
  {
    createGUIAck := {
      messageID := pl_MessageID
    }
  }
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminCreateGUINAck
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) CreateGUINAck.
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      template integer pl_MessageID)  := {
  //        addElementToGUINAck := {  - the message type
  //        messageID := pl_MessageID
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminCreateGUINAck(
    template integer pl_MessageID) :=
  {
    createGUINAck:= {
      messageID := pl_MessageID
    }
  }
  
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminRemoveElementFromGUI
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) removeElementFromGUI.
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      template EPTF_UIHandler_WidgetIdString pl_WidgetId)  := {
  //        removeElementFromGUI := {  - the message type
  //  	    widgetId       := pl_WidgetId - the id of the widget to remove from the GUI
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminRemoveElementFromGUI(
    template EPTF_UIHandler_WidgetIdString pl_WidgetId,
    template integer               pl_MessageID) :=
  {
    removeElementFromGUI := {
      widgetId := pl_WidgetId,
      messageID := pl_MessageID
    }
  }
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminRemoveElementFromGUIAck
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) removeElementFromGUIAck.
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      template EPTF_UIHandler_WidgetIdString pl_WidgetId)  := {
  //        removeElementFromGUIAck := {  - the message type
  //  	    widgetId       := pl_WidgetId - the id of the widget to remove from the GUI
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminRemoveElementFromGUIAck(
    template EPTF_UIHandler_WidgetIdString pl_WidgetId) :=
  {
    removeElementFromGUIAck := {
      widgetId := pl_WidgetId
    }
  }
  
  ///////////////////////////////////////////////////////////
  // Template: t_UIHandler_AdminRemoveElementFromGUINAck
  //
  // Purpose:
  //    Send/receive template EPTF Admin Interface (AdmIf) removeElementFromGUINAck.
  //
  // References:
  //   <EPTF_UIHandler_WidgetMsg> (    
  //      template EPTF_UIHandler_WidgetIdString pl_WidgetId)  := {
  //        removeElementFromGUINAck := {  - the message type
  //  	    widgetId       := pl_WidgetId - the id of the widget to remove from the GUI
  //      }
  //    }
  ///////////////////////////////////////////////////////////
  template EPTF_UIHandler_WidgetMsg t_UIHandler_AdminRemoveElementFromGUINAck(
    template EPTF_UIHandler_WidgetIdString pl_WidgetId) :=
  {
    removeElementFromGUINAck := {
      widgetId := pl_WidgetId
    }
  }
  
  //=========================================================================
  //Port Types
  //=========================================================================
  
  ///////////////////////////////////////////////////////////
  //  Type: EPTF_adminPort_PT
  // 
  //  Purpose:
  //     Internal message port type for exchanging configuration messages.
  // 
  //  Elements:
  // 
  //  Detailed Comments:
  //    Can bothway send messages type <EPTF_UIHandler_WidgetMsg>. 
  ///////////////////////////////////////////////////////////  
  type port EPTF_adminPort_PT message {
    inout EPTF_UIHandler_WidgetMsg
  } with {extension "internal"}
  
} //group WidgetMessages

}  // end of module
