///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_PTCDeployment_Functions
// 
//  Purpose:
//    This module contains the implementation of the EPTF_PTCDeployment functions.
//
//  Module Parameters:
//    tsp_debug_EPTF_PTCDeployment_Functions - boolean
// 
//  Module depends on:
//    <EPTF_CLL_Common_Definitions>
//    <EPTF_CLL_Base_Functions>
//    <EPTF_CLL_Logging_Definitions>
//    <EPTF_CLL_Logging_Functions>
//    <EPTF_CLL_PTCDeployment_Definitions>
//    <EPTF_CLL_PTCDeployment_PrivateFunctions>
// 
//  Current Owner:
//    Zsolt Szalai (EZSOSZA)
// 
//  Last Review Date:
//    2007-11-13
//
//  Detailed Comments:
//    This module contains the interface functions for the EPTF_PTCDeployment. 
//
///////////////////////////////////////////////////////////////

module EPTF_CLL_PTCDeployment_Functions {

  import from EPTF_CLL_Base_Functions all;
  import from EPTF_CLL_Common_Definitions all;
  import from EPTF_CLL_Logging_Definitions all;
  import from EPTF_CLL_Logging_Functions all;
  import from EPTF_CLL_PTCDeployment_Definitions all;
  import from EPTF_CLL_PTCDeployment_PrivateFunctions all;

  modulepar boolean tsp_debug_EPTF_PTCDeployment_Functions := false;

  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_PTCD_init_CT
  // 
  //  Purpose:
  //    Initialises the EPTF_PTCD_CT component
  //
  //  Parameters:
  //    pl_selfName - *in* *charstring*  - name of the component
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    This function should be called before using the EPTF PTCD
  //    component.
  //
  ///////////////////////////////////////////////////////////
  public function f_EPTF_PTCD_init_CT(in charstring pl_selfName) runs on EPTF_PTCD_CT {
    if (v_PTCD_initialized) {
      return;
    }
    f_EPTF_Base_init_CT(pl_selfName);
    
    f_EPTF_Logging_init_CT(pl_selfName);
    v_PTCDeployment_loggingMaskId := f_EPTF_Logging_registerComponentMasks(tsp_EPTF_PTCDeployment_loggingComponentMask, c_EPTF_PTCDeployment_loggingEventClasses, EPTF_Logging_CLL);
    if(tsp_debug_EPTF_PTCDeployment_Functions) {
      f_EPTF_Logging_enableLocalMask(v_PTCDeployment_loggingMaskId, c_EPTF_PTCDeployment_loggingClassIdx_Debug);
    } else {
      f_EPTF_Logging_disableLocalMask(v_PTCDeployment_loggingMaskId, c_EPTF_PTCDeployment_loggingClassIdx_Debug);
    }

    v_roledatabase := tsp_PTCD_database;
    v_currentBurst := {};
    v_PTCD_initialized := true;

    f_EPTF_Base_registerCleanup(refers(f_EPTF_PTCD_cleanup_CT));
    //log("----PTCD INIT DONE----");
    f_EPTF_PTCDeployment_debug("---- EPTF_PTCD INIT "&pl_selfName&" DONE ----");
  }
  
  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_PTCD_GetHost
  // 
  //  Purpose:
  //    This function queries a host from a database, according to a role.
  //
  //  Parameters:
  //    pl_role - *in* *charstring* - the role
  //    pl_method - *in* *integer* - for switching between deterministic or random methods, default is random(else than 1)
  //
  //  Return Value:
  //    *charstring* the host to execute the role on
  //
  //  Errors:
  //    On invalid role the function returs the empty string
  //    After filtering the hostlist for invalid hosts, it returns the empty 
  //    string in case it is filterd to an empty list
  //
  //  Detailed Comments:
  //    -
  //    
  ///////////////////////////////////////////////////////////
  public function f_EPTF_PTCD_GetHost(in charstring pl_role, in integer pl_method := 0) runs on EPTF_PTCD_CT return charstring {
    var integer vl_roleindex;
  
    if (f_PTCD_role_in_database(pl_role, vl_roleindex)) {
      return f_PTCD_choose_host(v_roledatabase[vl_roleindex].deployment,pl_method);
    } else {
      return "";
    }
  }
 
  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_PTCD_AddHosts
  // 
  //  Purpose:
  //    This function appends the database with the give hosts to the given role.
  //    If there is no such rule, it appends it too.
  //
  //  Parameters:
  //    pl_role - *in* *charstring* - the role
  //    pl_hostlist - *in* <EPTF_HostDeploymentEntryList> the hosts to append
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    -
  //    
  ///////////////////////////////////////////////////////////
  public function f_EPTF_PTCD_AddHosts(in charstring pl_role, in EPTF_HostDeploymentEntryList pl_hostlist) runs on EPTF_PTCD_CT {
    var integer vl_roleindex := 0;
    if (f_PTCD_role_in_database(pl_role, vl_roleindex)) {
      for (var integer i:=0;i<sizeof(pl_hostlist);i:=i+1){
	v_roledatabase[vl_roleindex].deployment[sizeof(v_roledatabase[vl_roleindex].deployment)] := pl_hostlist[i];
      }
    } else {
      f_PTCD_AddRole({pl_role,pl_hostlist});
    }
  }

  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_PTCD_RemoveHosts
  // 
  //  Purpose:
  //    This function removes the hosts listed from the database
  //
  //  Parameters:
  //    pl_role - *in* *charstring* - the role
  //    pl_hostlist - *in* <Charstrings> the hosts to append
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    -
  //    
  ///////////////////////////////////////////////////////////
  public function f_EPTF_PTCD_RemoveHosts(in charstring pl_role, in EPTF_CharstringList pl_hosts) runs on EPTF_PTCD_CT {
    var integer vl_roleindex := 0;
    if (f_PTCD_role_in_database(pl_role, vl_roleindex)){
      for (var integer i:=0;i<sizeof(pl_hosts);i:=i+1){
	f_PTCD_RemoveHost(vl_roleindex, pl_hosts[i]);
      }
    }
  }

  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_PTCD_cleanup_CT
  // 
  //  Purpose:
  //    This function cleans up the component
  //
  //  Parameters:
  //    -
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    -
  //    
  ///////////////////////////////////////////////////////////
  private function f_EPTF_PTCD_cleanup_CT() runs on EPTF_PTCD_CT {
    if (v_PTCD_initialized == false) {
      return;
    }
    v_PTCD_initialized := false;
    //log("----PTCD CLEANUP DONE----");
    f_EPTF_PTCDeployment_debug("---- EPTF_PTCD CLEANUP DONE ----");    
  }

}
