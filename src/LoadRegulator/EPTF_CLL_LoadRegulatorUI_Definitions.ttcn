///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_CLL_LoadRegulator_Definitions
//
// Purpose:
//   This module contains data type definitions for EPTF LoadRegulatorUI
//
// Module Parameters:
//    tsp_EPTF_LoadRegulatorUI_loggingComponentMask - *charstring* - component-type mask for logging, default value: "EPTF_LoadRegulatorUI"
//
//  Module depends on:
//    <EPTF_CLL_Logging_Definitions>
//    <EPTF_CLL_LoggingUI_Definitions>
//    <EPTF_CLL_LoadRegulator_Definitions>
//    <EPTF_CLL_UIHandler_Definitions>
//    <EPTF_CLL_UIHandlerVariableUI_Definitions>
// 
// Current Owner:
//    Zsolt Szalai (ezsosza)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////

module EPTF_CLL_LoadRegulatorUI_Definitions{

  import from EPTF_CLL_LoadRegulator_Definitions all;
  import from EPTF_CLL_UIHandler_Definitions all;
  import from EPTF_CLL_UIHandlerVariableUI_Definitions all;
  import from EPTF_CLL_Logging_Definitions all;
  import from EPTF_CLL_LoggingUI_Definitions all;

  modulepar charstring tsp_EPTF_LoadRegulatorUI_loggingComponentMask := "EPTF_LoadRegulatorUI";

/////////////////////////////////////////////////////////////////////////
// Constants: c_LoadRegulatorUI_NameId
//
// Purpose:
//    -
//
// Other values:
//    - 
/////////////////////////////////////////////////////////////////////////
  const charstring c_LoadRegulatorUI_NameId := "EPTF_LoadRegulatorUI_NameId";
  
/////////////////////////////////////////////////////////////////////////
// Constants: c_LoadRegulatorUI_enabledId
//
// Purpose:
//    -
//
// Other values:
//    - 
/////////////////////////////////////////////////////////////////////////
  const charstring c_LoadRegulatorUI_enabledId := "EPTF_LoadRegulatorUI_enabledId";

/////////////////////////////////////////////////////////////////////////
// Constants: c_LoadRegulatorUI_cpsToReachId
//
// Purpose:
//    -
//
// Other values:
//    - 
/////////////////////////////////////////////////////////////////////////
  const charstring c_LoadRegulatorUI_cpsToReachId := "EPTF_LoadRegulatorUI_cpsToReachId";

/////////////////////////////////////////////////////////////////////////
// Constants: c_LoadRegulatorUI_loadToReachId
//
// Purpose:
//    -
//
// Other values:
//    - 
/////////////////////////////////////////////////////////////////////////
  const charstring c_LoadRegulatorUI_loadToReachId := "EPTF_LoadRegulatorUI_loadToReachId";

/////////////////////////////////////////////////////////////////////////
// Constants: c_LoadRegulatorUI_currentLoadId
//
// Purpose:
//    -
//
// Other values:
//    - 
/////////////////////////////////////////////////////////////////////////
  const charstring c_LoadRegulatorUI_currentLoadId := "EPTF_LoadRegulatorUI_currentLoadId";

/////////////////////////////////////////////////////////////////////////
// Constants: c_LoadRegulatorUI_loadIsStableId
//
// Purpose:
//    -
//
// Other values:
//    - 
/////////////////////////////////////////////////////////////////////////
  const charstring c_LoadRegulatorUI_loadIsStableId := "EPTF_LoadRegulatorUI_loadIsStableId";

/////////////////////////////////////////////////////////////////////////
//
//  Component: EPTF_LoadRegulatorUI_CT
//
//  Purpose:
//    Component type for EPTF_LoadRegulatorUI
//
//  Elements:
//    v_LoadRegulatorUI_initialized - *boolean* - regulator initialized
//    v_LoadRegulatorUI_UIHandler - <EPTF_UIHandler_CT> - UIHandler component reference
//    v_LoadRegulatorUI_enabledVar - *integer* - enabled Variable reference
//    v_LoadRegulatorUI_cpsToReachVar - *integer* - enabled Variable reference
//    v_LoadRegulatorUI_loadToReachVar - *integer* - loadToReach Variable reference
//    v_LoadRegulatorUI_currentLoadVar - *integer* - currentLoad Variable reference
//    v_LoadRegulatorUI_loadIsStableVar - *integer* - loadIsStable Variable reference
//    v_LoadRegulatorUI_altstep - *default* - altstep
//    T_LoadRegulatorUI_refresh - *timer* - timer to go off to refresh data
//    v_LoadRegulatorUI_loggingMaskId - *integer* - logging mask id
//
//  Detailed Comments:
//    Extends EPTF_LoadRegulator_CT and EPTF_UIHandler_VariableUI_CT and EPTF_LoggingUIClient_CT
///////////////////////////////////////////////////////////////////////// 
  type component EPTF_LoadRegulatorUI_CT extends EPTF_LoadRegulator_CT, EPTF_UIHandler_VariableUI_CT, EPTF_LoggingUIClient_CT{
    private var boolean v_LoadRegulatorUI_initialized := false;
    private var EPTF_UIHandler_CT v_LoadRegulatorUI_UIHandler;
    private var integer v_LoadRegulatorUI_enabledVar;
    private var integer v_LoadRegulatorUI_cpsToReachVar;
    private var integer v_LoadRegulatorUI_loadToReachVar;
    private var integer v_LoadRegulatorUI_currentLoadVar;
    private var integer v_LoadRegulatorUI_loadIsStableVar;
    private var default v_LoadRegulatorUI_altstep;
    
    // logging
    private var integer v_LoadRegulatorUI_loggingMaskId := c_EPTF_Logging_invalidMaskId;
  }
}


