///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_CLL_LoadRegulator_Functions
//
// Purpose:
//   This module contains function definitions for EPTF LoadRegulatorUI
//
// Module Parameters:
//
//
//  Module depends on:
//    <EPTF_CLL_UIHandler_Definitions>
//    <EPTF_CLL_UIHandlerVariableUI_Functions>
//    <EPTF_CLL_Logging_Definitions>
//    <EPTF_CLL_Logging_Functions>
//    <EPTF_CLL_LoggingUI_Definitions>
//    <EPTF_CLL_LoggingUIClient_Functions>
//    <EPTF_CLL_Base_Functions>
//    <EPTF_CLL_LoadRegulatorUI_Definitions>
//    <EPTF_CLL_LoadRegulator_Functions>
//    <EPTF_CLL_LoadRegulator_Definitions>
// 
// Current Owner:
//    Zsolt Szalai (ezsosza)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////

module EPTF_CLL_LoadRegulatorUI_Functions{

  import from EPTF_CLL_UIHandler_Definitions all;
  import from EPTF_CLL_UIHandlerVariableUI_Functions all;
  import from EPTF_CLL_Logging_Definitions all;
  import from EPTF_CLL_Logging_Functions all;
  import from EPTF_CLL_LoggingUI_Definitions all;
  import from EPTF_CLL_LoggingUIClient_Functions all;
  import from EPTF_CLL_Base_Functions all;
  import from EPTF_CLL_LoadRegulatorUI_Definitions all;
  import from EPTF_CLL_LoadRegulator_Functions all;
  import from EPTF_CLL_LoadRegulator_Definitions all;

  modulepar boolean tsp_debug_EPTF_LoadRegulatorUI := false;

  private function f_swapspace(in charstring pl_str) return charstring {
    for (var integer i:=0; i<lengthof(pl_str); i:=i+1){
      if (pl_str[i]==" "){
	pl_str[i]:="_"
      }
    }
    return pl_str;
  }

  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_LoadRegulatorUI_init_CT
  //
  //  Purpose:
  //    Initialize the component
  //
  //  Parameters:
  //    pl_getSutLoad - *in* <ft_EPTF_LoadRegulator_getSUTLoad> - getSUTLoad function pointer
  //    pl_calcNextCps - *in* <ft_EPTF_LoadRegulator_calculateNextCps> - calculateNextCps function pointer
  //    pl_parentid - *in* *charstring* -  The parent widgetID, where to put LoadRegulator
  //    pl_UIHandler - *in* <EPTF_UIHandler_CT> -  The UIHandler component of the application
  //    pl_loggingUi_compRef - *in* <EPTF_LoggingUI_CT> - LoggingUI component reference
  //
  //  Detailed Comments:
  //    Initializes LoadRegulator and sets up a default behavior
  //
  ///////////////////////////////////////////////////////////
  public function f_EPTF_LoadRegulatorUI_init_CT(in charstring pl_selfName,
                                          in ft_EPTF_LoadRegulator_getSUTLoad pl_getSutLoad,
					  in ft_EPTF_LoadRegulator_calculateNextCps pl_calcNextCps := null,
					  in charstring pl_parentid,
                                          in EPTF_UIHandler_CT pl_UIHandler,
                                          in EPTF_LoggingUI_CT pl_loggingUi_compRef := null)
  runs on EPTF_LoadRegulatorUI_CT {
    if(pl_loggingUi_compRef != null) {
      f_EPTF_LoggingUIClient_init_CT(pl_selfName, pl_loggingUi_compRef, pl_UIHandler);
    } else {
      f_EPTF_Logging_init_CT(pl_selfName);
    }
    v_LoadRegulatorUI_loggingMaskId := f_EPTF_Logging_registerComponentMasks(tsp_EPTF_LoadRegulatorUI_loggingComponentMask, c_EPTF_LoadRegulator_loggingEventClasses, EPTF_Logging_CLL);
    if(tsp_debug_EPTF_LoadRegulatorUI) {
      f_EPTF_Logging_enableLocalMask(v_LoadRegulatorUI_loggingMaskId, c_EPTF_LoadRegulator_loggingClassIdx_Debug);
    } else {
      f_EPTF_Logging_disableLocalMask(v_LoadRegulatorUI_loggingMaskId, c_EPTF_LoadRegulator_loggingClassIdx_Debug);
    }
    
    if (not v_LoadRegulatorUI_initialized) {
      f_EPTF_UIHandler_VariableUI_init_CT(pl_selfName, pl_UIHandler);
      f_EPTF_LoadRegulator_init_CT(pl_selfName, pl_getSutLoad, pl_calcNextCps);
      //var charstring pl_prefix := f_swapspace(f_EPTF_Base_selfName());
      //f_EPTF_LoadRegulator_createVarRef_enabled(pl_prefix&".v_EPTF_LoadRegulatorUI_enabled", v_LoadRegulatorUI_enabledVar);
      //f_EPTF_LoadRegulator_createVarRef_cpsToReach(pl_prefix&".v_EPTF_LoadRegulatorUI_cpsToReach", v_LoadRegulatorUI_cpsToReachVar);
      //f_EPTF_LoadRegulator_createVarRef_loadToReach(pl_prefix&".v_EPTF_LoadRegulatorUI_loadToReach", v_LoadRegulatorUI_loadToReachVar);
      //f_EPTF_LoadRegulator_createVarRef_currentLoad(pl_prefix&".v_EPTF_LoadRegulatorUI_currentLoad", v_LoadRegulatorUI_currentLoadVar);
      //f_EPTF_LoadRegulator_createVarRef_loadIsStable(pl_prefix&".v_EPTF_LoadRegulatorUI_loadIsStable", v_LoadRegulatorUI_loadIsStableVar);
      v_LoadRegulatorUI_enabledVar := f_EPTF_LoadRegulator_getVarRef_enabled();
      v_LoadRegulatorUI_cpsToReachVar := f_EPTF_LoadRegulator_getVarRef_cpsToReach();
      v_LoadRegulatorUI_loadToReachVar := f_EPTF_LoadRegulator_getVarRef_loadToReach();
      v_LoadRegulatorUI_currentLoadVar := f_EPTF_LoadRegulator_getVarRef_currentLoad();
      v_LoadRegulatorUI_loadIsStableVar := f_EPTF_LoadRegulator_getVarRef_loadIsStable();
      v_LoadRegulatorUI_initialized := true;
      f_EPTF_Base_registerCleanup(refers(f_EPTF_LoadRegulatorUI_cleanup_CT));
    }
  }

  ///////////////////////////////////////////////////////////
  // Group: Private
  //
  // Purpose:
  //   Private functions. These functions must not be called by the user of <EPTF_LoadRegulatorUI_CT>
  //
  // Elements:
  ///////////////////////////////////////////////////////////  
  group Private {

  private function f_EPTF_LoadRegulatorUI_cleanup_CT() runs on EPTF_LoadRegulatorUI_CT{
    v_LoadRegulatorUI_initialized := false;
    // how to destroy variable???
  }

  group Logging {

  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_LoadRegulatorUI_warning
  // 
  //  Purpose:
  //    Function to log a warning from LoadRegulatorUI feature.
  //
  //  Parameters:
  //    - pl_message - *in* *charstring* - the message to log
  //
  //  Return Value:
  //    -
  //
  //  Errors & assertions:
  //    - 
  //
  //  Detailed Comments:
  //    -
  //
  ///////////////////////////////////////////////////////////
  private function f_EPTF_LoadRegulatorUI_warning(in @lazy charstring pl_message)
  runs on EPTF_LoadRegulatorUI_CT
  {
    f_EPTF_Logging_warningV2(pl_message, v_LoadRegulatorUI_loggingMaskId, {c_EPTF_LoadRegulator_loggingClassIdx_Warning});
  }
  
  } // group Logging
  } // group Private
  
  
}

