///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_Statreplay_Definitions
// 
//  Purpose:
//    This module contains definitions of generic EPTF_CLL_Statreplay functions.
//
//  Module Parameters:
//    tsp_EPTF_StatReplay_loggingComponentMask - *charstring* - component-type mask for logging, default value: "EPTF_StatReplay"
// 
//  Module depends on:
//	  <EPTF_CLL_UIHandler_Definitions>
//    <EPTF_CLL_UIHandlerClient_Definitions>
//	  <EPTF_CLL_Semaphore_Definitions>
//    <EPTF_CLL_Logging_Definitions>
// 
//  Current Owner:
//    Norbert PintÃ¯Â¿Â½r (ENORPIN)
// 
//  Last Review Date:
//    2007-xx-xx
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////////
module EPTF_CLL_StatReplay_Definitions{
  
//=========================================================================
// Import Part
//=========================================================================
import from EPTF_CLL_UIHandler_Definitions all;
import from EPTF_CLL_UIHandlerClient_Definitions all;
import from EPTF_CLL_Semaphore_Definitions all;
import from EPTF_CLL_Logging_Definitions all;

modulepar charstring tsp_EPTF_StatReplay_loggingComponentMask := "EPTF_StatReplay";

//=========================================================================
// Data Types
//=========================================================================
///////////////////////////////////////////////////////////
//  Type: CaptureInfoType
// 
//  Purpose:
//    Defines the base type the ring buffer is built from.
///////////////////////////////////////////////////////////
type record EPTF_StatReplay_CaptureInfoType
{
  float timestamp,
  float variableValue
}

// defines the ring buffer base type
#define EPTF_BASE_TYPE EPTF_StatReplay_CaptureInfoType

//instantiate the code for managing a ring buffer built from the base type
#include "EPTF_CLL_GenericRingBuffer_Functions.ttcnin"

//undef the base type to avoid error in case of multiple careless "#include"-s
#undef EPTF_BASE_TYPE 

//=========================================================================
//Constants
//=========================================================================
const charstring c_EPTF_CLL_StatReplay_shiftLeftButtonWidgetId := "shift_left";
const charstring c_EPTF_CLL_StatReplay_shiftLeftFastButtonWidgetId := "shift_left_fast";
const charstring c_EPTF_CLL_StatReplay_shiftRightButtonWidgetId := "shift_right";
const charstring c_EPTF_CLL_StatReplay_shiftRightFastButtonWidgetId := "shift_right_fast";

//=========================================================================
//Component Types
//=========================================================================
///////////////////////////////////////////////////////////
//  Type: EPTF_CLL_StatReplay_CT
// 
//  Purpose:
//    The component implements the Statistic Replay features.
//    It extends the EPTF_CLL_UIHandlerClient_CT, EPTF_CLL_UIHandler_CT 
//    and the EPTF_Semaphore_CT components.
// 
//  Elements:
//    v_EPTF_CLL_StatReplay_ringBuffer - <EPTF_StatReplay_CaptureInfoTypeRingBuffer> - contain the visualized elements
//	  v_EPTF_CLL_StatReplay_semaphoreIdx - <integer> - contain the index of the semaphore
//	  v_EPTF_CLL_StatReplay_headFileDescriptor - <integer> - the file descriptor of the head of the visualized windows
//    v_EPTF_CLL_StatReplay_tailFileDescriptor - <integer> - the file descriptor of the tail of the visualized windows
//    v_EPTF_CLL_StatReplay_capturedVariablePos - <integer> - the position of the visualized variable in the capture group
//	  v_EPTF_CLL_StatReplay_initialized - <boolean> - prevents multiple init by calling <f_EPTF_CLL_StatReplay_init_CT> several times
//    v_EPTF_CLL_StatReplay_fastLeftButtonIdx - <integer> - the index of the assigned new fast 
//															left button variable (EPTF_Var)
//    v_EPTF_CLL_StatReplay_leftButtonIdx - <integer> - the index of the assigned new  
//														left button variable (EPTF_Var)
//    v_EPTF_CLL_StatReplay_rightButtonIdx - <integer> - the index of the assigned new 
//														 right button variable (EPTF_Var)
//    v_EPTF_CLL_StatReplay_fastRightButtonIdx - <integer> - the index of the assigned new fast 
//															 right button variable (EPTF_Var)
//    v_EPTF_CLL_StatReplay_chartTraceIdx - <integer> - the index of the assigned new trace variable (EPTF_Var) 
//    
//    v_StatReplay_loggingMaskId - *integer* - logging component mask ID
//
//  Detailed Comments:
//   Gives a surface for visualization of a variable in the given log file.
//
///////////////////////////////////////////////////////////
type component EPTF_CLL_StatReplay_CT extends EPTF_UIHandlerClient_CT, EPTF_Semaphore_CT, EPTF_UIHandler_CT, EPTF_Logging_CT {
  var EPTF_StatReplay_CaptureInfoTypeRingBuffer v_EPTF_CLL_StatReplay_ringBuffer;
  var integer v_EPTF_CLL_StatReplay_semaphoreIdx;
  var integer v_EPTF_CLL_StatReplay_headFileDescriptor;
  var integer v_EPTF_CLL_StatReplay_tailFileDescriptor;
  var integer v_EPTF_CLL_StatReplay_capturedVariablePos;
  var boolean v_EPTF_CLL_StatReplay_initialized := false;
  var integer v_EPTF_CLL_StatReplay_fastLeftButtonIdx;
  var integer v_EPTF_CLL_StatReplay_leftButtonIdx;
  var integer v_EPTF_CLL_StatReplay_rightButtonIdx;
  var integer v_EPTF_CLL_StatReplay_fastRightButtonIdx;
  var integer v_EPTF_CLL_StatReplay_chartTraceIdx;

  // logging
  var integer v_StatReplay_loggingMaskId := c_EPTF_Logging_invalidMaskId;
}


///////////////////////////////////////////////////////////
//  Constant: c_EPTF_StatReplay_loggingEventClasses
// 
//  Purpose:
//    list of logging event class names used on the StatReplay
// 
//  Detailed Comments:
//    <EPTF_Logging_EventClassPrefixList> { "Warning", "Debug" }
///////////////////////////////////////////////////////////
const EPTF_Logging_EventClassPrefixList c_EPTF_StatReplay_loggingEventClasses := { "Warning", "Debug" };

///////////////////////////////////////////////////////////
//  Constant: c_EPTF_StatReplay_loggingClassIdx_Warning
// 
//  Purpose:
//    logging class index for Error
// 
//  Detailed Comments:
//    *0*
///////////////////////////////////////////////////////////
const integer c_EPTF_StatReplay_loggingClassIdx_Warning := 0;
///////////////////////////////////////////////////////////
//  Constant: c_EPTF_StatReplay_loggingClassIdx_Debug
// 
//  Purpose:
//    logging class index for Error
// 
//  Detailed Comments:
//    *1*
///////////////////////////////////////////////////////////
const integer c_EPTF_StatReplay_loggingClassIdx_Debug := 1;



}
