///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_DsRestAPI_Functions
// 
//  Purpose:
//    This module contains the implementation of generic EPTF_CLL_DsRestAPI functions.
// 
//  Module depends on:
//    <EPTF_CLL_DsRestAPI_Definitions>
//    <EPTF_CLL_DsRestAPI_HTTPServerFunctions>
//    <EPTF_CLL_DsRestAPI_HTTPServer_Definitions>
//    <EPTF_CLL_Base_Functions>
//    <EPTF_CLL_Logging_Functions>
//    <EPTF_CLL_Logging_Definitions>
//    <TCCFileIO_Functions>
//    <TCCConversion_Functions>
//    <IPL4asp_Types>
// 
//  Current Owner:
//    Tamas Kis (ekistam)
// 
//  Last Review Date:
//    -
//
//  Detailed Comments:
//    This module contains the interface functions for the EPTF_CLL_DsRestAPI.
//    Public functions:
//      <f_EPTF_DsRestAPI_init_CT>
//      <f_EPTF_DsRestAPI_start>
//      <f_EPTF_DsRestAPI_stop>
//
///////////////////////////////////////////////////////////////

module EPTF_CLL_DsRestAPI_Functions {

import from EPTF_CLL_DsRestAPI_HTTPServer_Functions all;
import from EPTF_CLL_DsRestAPI_Timeline_Functions all;
import from EPTF_CLL_DsRestAPI_Definitions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_Logging_Definitions all;
import from EPTF_CLL_Logging_Functions all;
import from EPTF_CLL_Variable_Definitions all;
import from TCCFileIO_Functions all;
import from TCCConversion_Functions all;
import from IPL4asp_Types all;

///////////////////////////////////////////////////////////
//  Function: f_EPTF_DsRestAPI_init_CT
// 
//  Purpose:
//    Initialises the EPTF_DsRestAPI_CT component
//
//  Parameters:
//    pl_selfName - *in charstring* - name of the component
//
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//    This function should be called before using the EPTF DsRestAPI
//    component.
//
///////////////////////////////////////////////////////////
public function f_EPTF_DsRestAPI_init_CT(in charstring pl_selfName) runs on EPTF_DsRestAPI_CT {
  if (v_EPTF_DsRestAPI_initialized) {
	f_EPTF_DsRestAPI_debug(%definitionId &": already initialized");
    return;
  }
  f_EPTF_Base_init_CT(pl_selfName);
  f_EPTF_DsRestAPI_HTTPServer_init_CT(pl_selfName);
  f_EPTF_DsRestAPI_Timeline_init_CT(pl_selfName);
  f_EPTF_Logging_init_CT(pl_selfName);
  v_DsRestAPI_loggingMaskId := f_EPTF_Logging_registerComponentMasks(tsp_EPTF_DsRestAPI_loggingComponentMask, c_EPTF_DsRestAPI_loggingEventClasses, EPTF_Logging_CLL);
  v_EPTF_DsRestAPI_started := false;
  v_EPTF_DsRestAPI_initialized := true;
  f_EPTF_Base_registerCleanup(refers(f_EPTF_DsRestAPI_cleanup_CT));
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_DsRestAPI_start
// 
//  Purpose:
//    Starts DsRestAPI as HTTP server.
//
//  Parameters:
//    pl_hostIPAddress - *in charstring* - the IP address of the server
//    pl_hostPort - *in integer* - the port number of the server
//    pl_HTTPServer_dir - *in charstring* - the path of HTTP server directory
//    pl_customizableApp_dir - *in charstring* - the path of customizableApp directory
//    pl_API_dir - *in charstring* - the path of DsRestAPI API directory
//    pl_proto - *in <ProtoTuple> - the protocol to be used for IPL4, default: { tcp := {} }. For ssl use: { ssl := {} }
//
//  Return Value:
//    Integer which represents the success of the function.
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//    
///////////////////////////////////////////////////////////
public function f_EPTF_DsRestAPI_start(
  in charstring pl_hostIPAddress,
  in integer pl_hostPort,
  in charstring pl_HTTPServer_dir := "",
  in charstring pl_customizableApp_dir := "",
  in charstring pl_API_dir := "",
  in ProtoTuple pl_proto := { tcp := {} }
) runs on EPTF_DsRestAPI_CT return integer {
  var integer vl_result := -1;
  if(v_EPTF_DsRestAPI_initialized) {
    f_EPTF_DsRestAPI_HTTPServer_setDir(pl_HTTPServer_dir);
    f_EPTF_DsRestAPI_createSymlinks(pl_customizableApp_dir, pl_API_dir, pl_HTTPServer_dir);
    vl_result := f_EPTF_DsRestAPI_HTTPServer_listen(pl_hostIPAddress, pl_hostPort, pl_proto);
    if (vl_result == 0) {
      v_EPTF_DsRestAPI_started := true;
    } else {
      f_EPTF_DsRestAPI_warning(%definitionId & ": DsRestAPI cannot listen on port: " & int2str(pl_hostPort));
    }
    
    if (pl_customizableApp_dir != "") {
      var charstring vl_customizableApp_dir_abs := "";
      if (f_EPTF_DsRestAPI_getAbsPath(pl_customizableApp_dir, vl_customizableApp_dir_abs)) {
        var EPTF_Var_DirectContent vl_content;
        var charstring vl_fileName := vl_customizableApp_dir_abs & "/TimelineRequest.json";
        if (f_FIO_fileOrDirExists(vl_fileName)) {
          if (f_EPTF_DsRestAPI_Timeline_AppendFromFile(vl_fileName, vl_content) != 0) {
            if (ischosen(vl_content.charstringVal)) {
              f_EPTF_DsRestAPI_warning("Setting default timelie request failed: " & vl_content.charstringVal);
            } else {
              f_EPTF_DsRestAPI_warning("Setting default timelie request failed.");
            }
          }
        }
      } else {
        f_EPTF_DsRestAPI_warning("CustomizableApp directory does not exist: " & pl_customizableApp_dir);
      }
    }
  }
  
  f_EPTF_DsRestAPI_makeCreatedSymlinksWritableToAll();
  
  return vl_result;
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_DsRestAPI_stop
//
//  Purpose:
//    Stops DsRestAPI's HTTP server.
//
//  Parameters:
//    -
//
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
public function f_EPTF_DsRestAPI_stop() runs on EPTF_DsRestAPI_CT {
  if(v_EPTF_DsRestAPI_started){
    var integer vl_res := f_EPTF_DsRestAPI_HTTPServer_close();
    if (vl_res != 0) {
      f_EPTF_DsRestAPI_warning(%definitionId & " unsuccessful!");
    }
    v_EPTF_DsRestAPI_started := false;
    f_EPTF_DsRestAPI_removeSymlinks();
  }
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_DsRestAPI_cleanup_CT
// 
//  Purpose:
//    Cleans up DsRestAPI feature.
//
//  Parameters:
//    -
//
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//    
///////////////////////////////////////////////////////////
private function f_EPTF_DsRestAPI_cleanup_CT() runs on EPTF_DsRestAPI_CT {
  if (v_EPTF_DsRestAPI_initialized) {
    f_EPTF_DsRestAPI_stop();
    v_EPTF_DsRestAPI_initialized := false;
  }
}

private external function ef_EPTF_DsRestAPI_FIO_readlink(in charstring pl_relPath, out charstring pl_absPath) return integer;
private external function ef_EPTF_DsRestAPI_FIO_symlink(in charstring pl_path1, in charstring pl_path2) return integer;

///////////////////////////////////////////////////////////
//  Function: f_EPTF_DsRestAPI_debug
//
//  Purpose:
//    Function to log a debug message from DsRestAPI feature.
//
//  Parameters:
//    pl_message - *in* *charstring* - message to be logged
//
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
private function f_EPTF_DsRestAPI_debug(in @lazy charstring pl_msg) runs on EPTF_DsRestAPI_CT {
  f_EPTF_Logging_debugV2(pl_msg, v_DsRestAPI_loggingMaskId, {cg_EPTF_DsRestAPI_loggingClassIdx_Debug});
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_DsRestAPI_warning
//
//  Purpose:
//    Function to log a warning from DsRestAPI feature.
//
//  Parameters:
//    pl_msg - *in* *charstring* - message to be logged
//
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
private function f_EPTF_DsRestAPI_warning(in @lazy charstring pl_msg) runs on EPTF_DsRestAPI_CT {
  f_EPTF_Logging_warningV2(pl_msg, v_DsRestAPI_loggingMaskId, {cg_EPTF_DsRestAPI_loggingClassIdx_Warning});
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_DsRestAPI_error
//
//  Purpose:
//    Function to log an error from DsRestAPI feature.
//
//  Parameters:
//    pl_msg - *in* *charstring* - message to be logged
//
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
private function f_EPTF_DsRestAPI_error(in charstring pl_msg) runs on EPTF_DsRestAPI_CT {
  f_EPTF_Logging_error(true, pl_msg);
  f_EPTF_Base_stopAll();
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_DsRestAPI_createSymlinks
//
//  Purpose:
//    creates symlinks for App specific and DsRestAPI API directories
//
//  Parameters:
//    pl_customizableApp_dir - *in charstring* - the path of customizableApp directory
//    pl_API_dir - *in charstring* - the path of DsRestAPI API directory
//    pl_HTTPServer_dir - *in charstring* - the path of HTTP server directory
//
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
private function f_EPTF_DsRestAPI_createSymlinks(in charstring pl_customizableApp_dir, in charstring pl_API_dir, in charstring pl_HTTPServer_dir) runs on EPTF_DsRestAPI_CT {

  if (pl_HTTPServer_dir == "") {
    // this mode corresponds to the RESTAPI
    return;
  }
  
  if (pl_HTTPServer_dir[lengthof(pl_HTTPServer_dir) - 1] != "/") {
    pl_HTTPServer_dir := pl_HTTPServer_dir & "/";
  }

  var charstring vl_customApp_dirSymlinkRel := pl_HTTPServer_dir&"CustomizableContent/";
  var charstring vl_customAppSymlinkName := "CustomizableApp";
  var charstring vl_API_dirSymlinkRel := pl_HTTPServer_dir&"Utils/";
  var charstring vl_API_dirSymlinkName := "DsRestAPI";

  // get absolute path of symlinks
  if(not f_EPTF_DsRestAPI_getAbsPath(vl_customApp_dirSymlinkRel, v_customApp_dir_symlink) or not f_EPTF_DsRestAPI_getAbsPath(vl_API_dirSymlinkRel, v_API_dir_symlink)){
    f_EPTF_DsRestAPI_warning("HTTPServer_directory does not exist: " & pl_HTTPServer_dir);
    return;
  }

  // concat absolute paths with names
  v_config_symlink := v_customApp_dir_symlink&"/"&"config.cfg";
  v_customApp_dir_symlink := v_customApp_dir_symlink&"/"&vl_customAppSymlinkName;
  v_API_dir_symlink := v_API_dir_symlink&"/"&vl_API_dirSymlinkName;

  // remove if symlinks already exist
  f_EPTF_DsRestAPI_removeSymlinks();

  if (pl_customizableApp_dir != "") {
    // get absolute path of module parameters
    var charstring vl_customizableApp_directory_abs := "";
    if (not f_EPTF_DsRestAPI_getAbsPath(pl_customizableApp_dir, vl_customizableApp_directory_abs)) {
      f_EPTF_DsRestAPI_warning("CustomizableApp directory does not exist: "&pl_customizableApp_dir);
	  if (not f_FIO_mkdir(v_customApp_dir_symlink)) {
        f_EPTF_DsRestAPI_warning("Directory creation was unsuccessful for CustomizableApp directory");
      }
    } else {
      var charstring vl_customizableApp_directory_rel := f_EPTF_DsRestAPI_relativizePath(vl_customizableApp_directory_abs, v_customApp_dir_symlink);
      if (ef_EPTF_DsRestAPI_FIO_symlink(vl_customizableApp_directory_rel, v_customApp_dir_symlink) != 0 ) {
        f_EPTF_DsRestAPI_warning("Symlink creation was unsuccessful for CustomizableApp directory");
      }
	}
	f_EPTF_DsRestAPI_createCustomizableAppDirectories(v_customApp_dir_symlink);
  }

//   if (pl_API_dir != "") {
//     var charstring vl_DsRestAPI_API_directory_abs := "";
//     if(not f_EPTF_DsRestAPI_getAbsPath(pl_API_dir, vl_DsRestAPI_API_directory_abs)){
//       f_EPTF_DsRestAPI_warning("DsRestAPI API directory does not exist: "&pl_API_dir);
//     } else {
//       var charstring vl_DsRestAPI_API_directory_rel := f_EPTF_DsRestAPI_relativizePath(vl_DsRestAPI_API_directory_abs, v_API_dir_symlink);
//       if(ef_EPTF_DsRestAPI_FIO_symlink(vl_DsRestAPI_API_directory_rel, v_API_dir_symlink) != 0 ){
//         f_EPTF_DsRestAPI_warning("Symlink creation was unsuccessful for DsRestAPI_API directory");
//       }
// 	}
//   }

  //f_EPTF_DsRestAPI_createConfigSymlink(v_config_symlink);
}

private function f_EPTF_DsRestAPI_createCustomizableAppDirectories(in charstring pl_customizableApp_dir) runs on EPTF_DsRestAPI_CT {
  var charstring vl_setupsDir := pl_customizableApp_dir & "/Setups";
  if (not f_FIO_fileOrDirExists(vl_setupsDir) and not f_FIO_mkdir(vl_setupsDir)) {
    f_EPTF_DsRestAPI_warning("Setup directory does not exist and could not be created it in GUICustomization location");
  }
  
  var charstring vl_viewModelsDir := pl_customizableApp_dir & "/ViewModels";
  if (not f_FIO_fileOrDirExists(vl_viewModelsDir) and not f_FIO_mkdir(vl_viewModelsDir)) {
    f_EPTF_DsRestAPI_warning("ViewModels directory does not exist and could not be created it in GUICustomization location");
  }
  
  var charstring vl_viewsDir := pl_customizableApp_dir & "/Views";
  if (not f_FIO_fileOrDirExists(vl_viewsDir) and not f_FIO_mkdir(vl_viewsDir)) {
    f_EPTF_DsRestAPI_warning("Views directory does not exist and could not be created it in GUICustomization location");
  }
}

private function f_EPTF_DsRestAPI_createConfigSymlink(in charstring pl_symlinkLocation) runs on EPTF_DsRestAPI_CT {
  var charstring vl_executable;
  var EPTF_CharstringList vl_testCases;
  var charstring vl_symlinkTarget_relToPwd;
  var charstring vl_symlinkTarget_abs;
  var charstring vl_symlinkTarget_relToHttpDir;
  
  f_EPTF_Base_getStartCommand(vl_executable, vl_symlinkTarget_relToPwd, vl_testCases);
  if (not f_EPTF_DsRestAPI_getAbsPath(vl_symlinkTarget_relToPwd, vl_symlinkTarget_abs)) {
    f_EPTF_DsRestAPI_warning("Symlink creation to the config file was unsuccessful: file not found: " & vl_symlinkTarget_relToPwd);
    return;
  }
  
  vl_symlinkTarget_relToHttpDir := f_EPTF_DsRestAPI_relativizePath(vl_symlinkTarget_abs, pl_symlinkLocation);
  
  if(ef_EPTF_DsRestAPI_FIO_symlink(vl_symlinkTarget_relToHttpDir, pl_symlinkLocation) != 0 ){
    f_EPTF_DsRestAPI_warning("Symlink creation to the config file was unsuccessful");
  }
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_DsRestAPI_relativizePath
//
//  Purpose:
//    creates a relative path from a given absolute path relative to the second argument.
//
//  Parameters:
//    pl_absPath - the absolute path that will become relative
//    pl_relativeToAbsPath - the path to which it will be relative
//
//  Return Value:
//    *charstring* - the relative path created
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
private function f_EPTF_DsRestAPI_relativizePath(in charstring pl_absPath, in charstring pl_relativeToAbsPath) runs on EPTF_DsRestAPI_CT return charstring {

  var integer vl_absPathLength := lengthof(pl_absPath);
  var integer vl_absRelativeToLength := lengthof(pl_relativeToAbsPath);
  
  var integer vl_indexOfLastDash := 0;
  
  var integer vl_i := 0;
  
  // check the common part of the paths
  while (vl_i < vl_absPathLength and vl_i < vl_absRelativeToLength) {
    if (pl_absPath[vl_i] == pl_relativeToAbsPath[vl_i]) {
      if (pl_absPath[vl_i] == "/") {
        vl_indexOfLastDash := vl_i;
      }
      vl_i := vl_i + 1;
    } else {
      break;
    }
  }
  
  // the number of directories we have to go down the structure
  var integer vl_numberOfDirsDown := -1;
  vl_i := vl_indexOfLastDash;
  while (vl_i < vl_absRelativeToLength) {
    if (pl_relativeToAbsPath[vl_i] == "/") {
      vl_numberOfDirsDown := vl_numberOfDirsDown + 1;
    }
    vl_i := vl_i + 1;
  }
  
  var charstring vl_relPath := "";
  for (var integer vl_j := 0; vl_j < vl_numberOfDirsDown; vl_j := vl_j + 1) {
    vl_relPath := vl_relPath & "../";
  }
  if (lengthof(pl_absPath) > 0) { //DTE prevention
    vl_relPath := vl_relPath & substr(pl_absPath, vl_indexOfLastDash + 1, lengthof(pl_absPath) - vl_indexOfLastDash - 1);
  }
  return vl_relPath;
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_DsRestAPI_makeCreatedSymlinksWritableToAll
//
//  Purpose:
//    makes the symlinks writable to all users
//
//  Parameters:
//    -
//
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
private function f_EPTF_DsRestAPI_makeCreatedSymlinksWritableToAll() runs on EPTF_DsRestAPI_CT {
  if (v_customApp_dir_symlink != "" and f_FIO_fileOrDirExists(v_customApp_dir_symlink) and f_EPTF_Base_system("chmod 0777 "&v_customApp_dir_symlink) != 0) {
    //f_EPTF_DsRestAPI_warning("Symlink cannot be made writable: "&v_customApp_dir_symlink);
  }
//   if (v_API_dir_symlink != "" and f_FIO_fileOrDirExists(v_API_dir_symlink) and f_EPTF_Base_system("chmod 0777 "&v_API_dir_symlink) != 0) {
//     //f_EPTF_DsRestAPI_warning("Symlink cannot be made writable: "&v_API_dir_symlink);
//   }
  if (v_config_symlink != "" and f_FIO_fileOrDirExists(v_config_symlink) and f_EPTF_Base_system("chmod 0777 "&v_config_symlink) != 0) {
    //f_EPTF_DsRestAPI_warning("Symlink cannot be made writable: "&v_config_symlink);
  }
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_DsRestAPI_removeSymlinks
//
//  Purpose:
//    removes symlinks for App specific and DsRestAPI API directories during the cleanup
//
//  Parameters:
//    -
//
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
private function f_EPTF_DsRestAPI_removeSymlinks() runs on EPTF_DsRestAPI_CT {
  if(v_customApp_dir_symlink != "" and f_FIO_fileOrDirExists(v_customApp_dir_symlink)){
    if(ef_EPTF_DsRestAPI_FIO_deleteResource(v_customApp_dir_symlink) != 0){
      f_EPTF_DsRestAPI_warning("Symlink cannot be removed: "&v_customApp_dir_symlink);
    }
  }
//   if(v_API_dir_symlink != "" and f_FIO_fileOrDirExists(v_API_dir_symlink)){
//     if(ef_EPTF_DsRestAPI_FIO_deleteResource(v_API_dir_symlink) != 0){
//       f_EPTF_DsRestAPI_warning("Symlink cannot be removed: "&v_API_dir_symlink);
//     }
//   }
  if(v_config_symlink != "" and f_FIO_fileOrDirExists(v_config_symlink)){
    if(ef_EPTF_DsRestAPI_FIO_deleteResource(v_config_symlink) != 0){
      f_EPTF_DsRestAPI_warning("Symlink cannot be removed: "&v_config_symlink);
    }
  }
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_DsRestAPI_getAbsPath
//
//  Purpose:
//    returns with the absolute path of the specified directory
//
//  Parameters:
//   pl_relPath - *in* *charstring* - relative path which needs to be resolved
//   pl_absPath - *out* *charstring* - resolved absolute path
//
//  Return Value:
//    *boolean* - true if the directory exists (absoulte path is valid)
//
//  Errors:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
private function f_EPTF_DsRestAPI_getAbsPath(
  in charstring pl_relPath,
  out charstring pl_absPath) runs on EPTF_DsRestAPI_CT return boolean {
  var boolean vl_retVal := false;
  pl_absPath := "";
  if(f_FIO_fileOrDirExists(pl_relPath)) {
    if(ef_EPTF_DsRestAPI_FIO_readlink(pl_relPath, pl_absPath) != 0) {
      f_EPTF_DsRestAPI_warning("Failed to get absolute path of " & pl_relPath);
    } else {
      pl_absPath := f_stripWhitespaces(pl_absPath); // remove "\n" from the end of strings
      vl_retVal := true;
    }
  } else {
    f_EPTF_DsRestAPI_warning("Failed to get absolute path of " & pl_relPath & ": resource does not exist");
  }
  
  return vl_retVal;
}

} // ~ module EPTF_CLL_DsRestAPI_Functions
