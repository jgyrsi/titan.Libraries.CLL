///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_DsRestAPI_Filter_Definitions
// 
//  Purpose:
//    This module contains definitions of EPTF_CLL_DsRestAPI_Filter.
// 
//  Module depends on:
//    <EPTF_CLL_DataSource_Definitions>
//    <EPTF_CLL_Base_Definitions>
//    <EPTF_CLL_Common_Definitions>
//    <EPTF_CLL_FBQ_Definitions>
// 
//  Current Owner:
//    Tamas Kis (ekistam)
// 
//  Last Review Date:
//    -
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////////
module EPTF_CLL_DsRestAPI_Filter_Definitions {
  
  import from EPTF_CLL_DataSource_Definitions all;
  import from EPTF_CLL_Base_Definitions all;
  import from EPTF_CLL_Common_Definitions all;
  import from EPTF_CLL_FBQ_Definitions all;
  import from EPTF_CLL_Logging_Definitions all;
  import from EPTF_CLL_DsRestAPI_Storage_Definitions all;
  
  modulepar charstring tsp_EPTF_DsRestAPI_Filter_loggingComponentMask := "EPTF_DsRestAPI_Filter";
  
  ///////////////////////////////////////////////////////////
  //  Constant: c_EPTF_DsRestAPI_Filter_loggingEventClasses
  // 
  //  Purpose:
  //    list of logging event class names used on DsRestAPI
  // 
  //  Detailed Comments:
  //    <EPTF_Logging_EventClassPrefixList> { "Warning", "Debug" }
  ///////////////////////////////////////////////////////////
  const EPTF_Logging_EventClassPrefixList c_EPTF_DsRestAPI_Filter_loggingEventClasses := { "Warning", "Debug" };
  
  ///////////////////////////////////////////////////////////
  //  Constant: cg_EPTF_DsRestAPI_Filter_loggingClassIdx_Warning
  // 
  //  Purpose:
  //    logging class index for Warning
  // 
  //  Detailed Comments:
  //    *0*
  ///////////////////////////////////////////////////////////
  const integer cg_EPTF_DsRestAPI_Filter_loggingClassIdx_Warning := 0;
  
  ///////////////////////////////////////////////////////////
  //  Constant: cg_EPTF_DsRestAPI_Filter_loggingClassIdx_Debug
  // 
  //  Purpose:
  //    logging class index for Debug
  // 
  //  Detailed Comments:
  //    *1*
  ///////////////////////////////////////////////////////////
  const integer cg_EPTF_DsRestAPI_Filter_loggingClassIdx_Debug := 1;
  
  ///////////////////////////////////////////////////////////////////////////////
  // Type: EPTF_DsRestAPI_FilterRequest
  //
  // Purpose:
  //   Type for filtering Request
  //
  // Elements:
  //   - source - *charstring* - the name of source
  //   - ptcname - *charstring* *optional* - the name of PTC
  //   - element - *charstring* - the name of the element
  //   - params - *EPTF_DataSource_Params* *optional* - the record of DS parameters
  //       
  ///////////////////////////////////////////////////////////////////////////////
  type record EPTF_DsRestAPI_FilterRequest {
    charstring source,
    charstring ptcname optional,
    charstring element,
    EPTF_DsRestAPI_Filter_Params params optional
  }
  
  ///////////////////////////////////////////////////////////////////////////////
  // Type: EPTF_DsRestAPI_Filter_Param
  //
  // Purpose:
  //   Type for filtering params
  //
  // Elements:
  //   - paramName - *charstring* - the param name
  //   - paramValue - *EPTF_DsRestAPI_Filter* - the param value which is either another getData or a value
  //       
  ///////////////////////////////////////////////////////////////////////////////
  type record EPTF_DsRestAPI_Filter_Param {
    charstring paramName,
    EPTF_DsRestAPI_Filter paramValue
  }
  
  ///////////////////////////////////////////////////////////
  // Type: EPTF_DsRestAPI_Filter_Params
  //
  // Purpose:
  //   Type for storing list of <EPTF_DsRestAPI_Filter_Param> items
  //
  // Elements:
  //   <EPTF_DsRestAPI_Filter_Param>
  ///////////////////////////////////////////////////////////
  type record of EPTF_DsRestAPI_Filter_Param EPTF_DsRestAPI_Filter_Params;
  
  ///////////////////////////////////////////////////////////////////////////////
  // Type: EPTF_DsRestAPI_Filter
  //
  // Purpose:
  //   Type for wrapping a filter
  //
  // Elements:
  //   - dataValue - <charstring> - a value
  //   - request - <EPTF_DsRestAPI_FilterRequest> - a getData request
  //       
  ///////////////////////////////////////////////////////////////////////////////
  type union EPTF_DsRestAPI_Filter {
    charstring dataValue,
    EPTF_DsRestAPI_FilterRequest request
  } with {  // for support TITAN/4 R2A :
    variant(dataValue) "JSON:name as dataValue";
    variant(request) "JSON:name as request";
  }
  
  ///////////////////////////////////////////////////////////////////////////////
  //  Function: fcb_EPTF_DsRestAPI_FilterEvaluatedCallback
  // 
  //  Purpose:
  //    This function is called when the filter of the getData request is evaluated
  //
  //  Parameters:
  //    *in boolean pl_source* - the result of the filter evaluation
  //    *in* <EPTF_IntegerList> *pl_userData* - user specific data given at the request
  //
  //  Return Value:
  //    -
  //
  //  Detailed Comments:
  //    -
  ///////////////////////////////////////////////////////////////////////////////
  type function fcb_EPTF_DsRestAPI_FilterEvaluatedCallback(in boolean pl_result, in EPTF_IntegerList pl_userData) runs on self;
  
  ///////////////////////////////////////////////////////////////////////////////
  // Type: EPTF_DsRestAPI_EvaluatedFilterHandlerItem
  //
  // Purpose:
  //   Type for storing filter handler items that are used after the filter is evaluated
  //
  // Elements:
  //   - userData - *EPTF_IntegerList* - user specific data given at the request
  //   - filter - *EPTF_DsRestAPI_Filter* - the filter
  //   - filterEvaluatedCallback - *fcb_EPTF_DsRestAPI_FilterEvaluatedCallback* - the function to call when the filter is evaluated
  //       
  ///////////////////////////////////////////////////////////////////////////////
  type record EPTF_DsRestAPI_EvaluatedFilterHandlerItem {
    EPTF_IntegerList userData,
    EPTF_DsRestAPI_Filter filter,
    fcb_EPTF_DsRestAPI_FilterEvaluatedCallback filterEvaluatedCallback,
    integer storageId
  }
  
  ///////////////////////////////////////////////////////////////////////////////
  // Type: EPTF_DsRestAPI_FiltersToBeEvaluated
  //
  // Purpose:
  //   Type for storing list of <EPTF_DsRestAPI_EvaluatedFilterHandlerItem> items
  //
  // Elements:
  //   <EPTF_DsRestAPI_EvaluatedFilterHandlerItem>
  ///////////////////////////////////////////////////////////////////////////////
  type record of EPTF_DsRestAPI_EvaluatedFilterHandlerItem EPTF_DsRestAPI_FiltersToBeEvaluated;
  
  ///////////////////////////////////////////////////////////////////////////////
  // Type: EPTF_DsRestAPI_FilterHandlerItem
  //
  // Purpose:
  //   Type for storing filter handler items that are used during the filter evaluation
  //
  // Elements:
  //   - filterId - *integer* - the filter id
  //   - filterPath - *EPTF_IntegerList* - the filter path
  //       
  ///////////////////////////////////////////////////////////////////////////////
  type record EPTF_DsRestAPI_FilterHandlerItem {
    integer filterId,
    EPTF_IntegerList filterPath,
    charstring storageKey
  }
  
  ///////////////////////////////////////////////////////////////////////////////
  // Type: EPTF_DsRestAPI_OutstandingFilters
  //
  // Purpose:
  //   Type for storing list of <EPTF_DsRestAPI_FilterHandlerItem> items
  //
  // Elements:
  //   <EPTF_DsRestAPI_FilterHandlerItem>
  ///////////////////////////////////////////////////////////////////////////////
  type record of EPTF_DsRestAPI_FilterHandlerItem EPTF_DsRestAPI_OutstandingFilters;
  
  /////////////////////////////////////////////////////////////////////////////  
  //  Type: EPTF_DsRestAPI_Filter_CT
  //
  //  Purpose:
  //    The DataSource Server UI Model component
  //
  //  Extensions:
  //    EPTF_Base_CT, EPTF_Var_CT, EPTF_Logging_CT, EPTF_DataSource_CLI_CT, EPTF_DataSourceClient_CT
  //
  //  Variables:    
  //    - v_DsRestAPI_Filter_initialized - *boolean* - represents if Filter is initialized
  //    - v_DsRestAPI_Filter_loggingMaskId - *integer* - logging component mask ID
  //
  //    - v_DsRestAPI_Filter_filtersToBeEvaluatedDB - *EPTF_DsRestAPI_RequestHandlerDB* - the list for the filters to be evaluated
  //    - v_DsRestAPI_Filter_outstandingFiltersDB - *EPTF_DsRestAPI_ResponseHandlerDB* - the list for the outstanding DataSource requests
  //
  //    - v_DsRestAPI_Filter_filtersToBeEvaluatedQueue - *EPTF_FreeBusyQueue* - the FBQ for the filters to be evaluated
  //    - v_DsRestAPI_Filter_outstandingFiltersQueue - *EPTF_FreeBusyQueue* - the FBQ for the outstanding DataSource requests
  //
  //  Ports:  
  //    -
  //  Function types:
  //    -
  //
  //  Detailed Comments:
  //      -
  /////////////////////////////////////////////////////////////////////////////
  type component EPTF_DsRestAPI_Filter_CT extends
    EPTF_Base_CT,
    EPTF_DataSource_CT,
    EPTF_DsRestAPI_Storage_CT {
    private var boolean v_DsRestAPI_Filter_initialized := false;
    private var integer v_DsRestAPI_Filter_loggingMaskId := c_EPTF_Logging_invalidMaskId;
   
    private var EPTF_FreeBusyQueue v_DsRestAPI_Filter_filtersToBeEvaluatedQueue := -1;
    private var EPTF_FreeBusyQueue v_DsRestAPI_Filter_outstandingFiltersQueue := -1;
    private var EPTF_DsRestAPI_FiltersToBeEvaluated v_DsRestAPI_Filter_filtersToBeEvaluatedDB := {};
    private var EPTF_DsRestAPI_OutstandingFilters v_DsRestAPI_Filter_outstandingFiltersDB := {};
  }

} with {encode "JSON"}  // ~ module EPTF_CLL_DsRestAPI_Filter_Definitions
