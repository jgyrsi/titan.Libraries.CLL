///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_Rendezvous_Definitions
// 
//  Purpose:
//    This module contains definitions of EPTF_CLL_Rendezvous feature.
//    
//  Module depends on:
//    <EPTF_CLL_Base_Definitions>
//    <EPTF_CLL_Common_Definitions>
//    <EPTF_CLL_Semaphore_Definitions>
//    <EPTF_CLL_Logging_Definitions>
//    <EPTF_CLL_FBQ_Definitions>
//    <EPTF_CLL_HashMap_Definitions>
//
//  Current Owner:
//    Bence Molnar (EBENMOL)
// 
//  Last Review Date:
//    2009-06-08
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////

module EPTF_CLL_Rendezvous_Definitions
{

import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_Common_Definitions all;

import from EPTF_CLL_Semaphore_Definitions all;
import from EPTF_CLL_Logging_Definitions all;
import from EPTF_CLL_FBQ_Definitions all;
import from EPTF_CLL_HashMap_Definitions all;


modulepar charstring tsp_EPTF_Rendezvous_loggingComponentMask := "EPTF_Rendezvous";
modulepar charstring tsp_EPTF_RendezvousClient_loggingComponentMask := "EPTF_RendezvousClient";

///////////////////////////////////////////////////////////
//  Template: t_EPTF_Rendezvous_WaitForATrigger 
// 
//  Purpose:
//   An <EPTF_Rendezvous_Msg> type template. 
//   This is a WaitForATrigger request message.
// 
//
//////////////////////////////////////////////////////////

template EPTF_Rendezvous_Msg t_EPTF_Rendezvous_WaitForATrigger:={
  Rendezvous_Client_Req:={
    rendezvousType:=WaitForATrigger,
    semaphoreID:=?,
    rendezvousID:=?,
    rendezvousParam:=*,
    extendedRendezvousID := *
  }
}

///////////////////////////////////////////////////////////
//  Template: t_EPTF_Rendezvous_WaitForNTrigger 
// 
//  Purpose:
//   An <EPTF_Rendezvous_Msg> type template. 
//   This is a WaitForNTrigger request message.
// 
//
//////////////////////////////////////////////////////////

template EPTF_Rendezvous_Msg t_EPTF_Rendezvous_WaitForNTrigger:={
  Rendezvous_Client_Req:={
    rendezvousType:=WaitForNTrigger,
    semaphoreID:=?,
    rendezvousID:=?,
    rendezvousParam:=*,
    extendedRendezvousID := *
  }
}

template EPTF_Rendezvous_Msg t_EPTF_Rendezvous_StateTrigger:={
  Rendezvous_Client_Req:={
    rendezvousType:=StateTrigger,
    semaphoreID:=?,
    rendezvousID:=?,
    rendezvousParam:=?,
    extendedRendezvousID := ?
  }
}


///////////////////////////////////////////////////////////
//  Template: t_EPTF_Rendezvous_WaitForATrigger_Resp 
// 
//  Purpose:
//   An <EPTF_Rendezvous_Msg> type template. 
//   This is a WaitForATrigger response message.
// 
//
//////////////////////////////////////////////////////////

template EPTF_Rendezvous_Msg t_EPTF_Rendezvous_WaitForATrigger_Resp:={
  Rendezvous_Client_Resp:={
    rendezvousType:=WaitForATrigger,
    semaphoreID:=?,      
    rendezvousID:=?,
    rendezvousParam:=*,
    extendedRendezvousID := *
  }
}

///////////////////////////////////////////////////////////
//  Template: t_EPTF_Rendezvous_WaitForNTrigger_Resp 
// 
//  Purpose:
//   An <EPTF_Rendezvous_Msg> type template. 
//   This is a WaitForNTrigger response message.
// 
//
//////////////////////////////////////////////////////////

template EPTF_Rendezvous_Msg t_EPTF_Rendezvous_WaitForNTrigger_Resp:={
  Rendezvous_Client_Resp:={
    rendezvousType:=WaitForNTrigger,
    semaphoreID:=?,      
    rendezvousID:=?,
    rendezvousParam:=*,
    extendedRendezvousID := *
  }
}

template EPTF_Rendezvous_Msg t_EPTF_Rendezvous_StateTrigger_Resp :={
  Rendezvous_Client_Resp:={
    rendezvousType:=StateTrigger,
    semaphoreID:=-1,      
    rendezvousID:=?,
    rendezvousParam:=?,
    extendedRendezvousID := ?
  }
}

///////////////////////////////////////////////////////////
//  Type: EPTF_Rendezvous_WaitForNTriggerClients 
// 
//  Purpose:
//   Record for storing WaitForNTrigger type clients
// 
//  Elements:
//    maxClients - *integer* - the maximum number of clients
//    registeredClients - *integer* - the clients already registered for the rendezvous
//    clientIDs - <EPTF_IntegerList> - the IDs of the registered clients
//    semaphoreIDs - <EPTF_IntegerList> - the semaphore IDs of the registered clients
// 
//  Detailed Comments:
//
//////////////////////////////////////////////////////////

type record EPTF_Rendezvous_WaitForNTriggerClients
{
  integer maxClients,
  integer registeredClients,
  EPTF_IntegerList clientIDs,
  EPTF_IntegerList semaphoreIDs
}

type record EPTF_Rendezvous_StateTriggerClients{
  integer fired,
  integer registeredClients,
  EPTF_IntegerList clientIDs,
  EPTF_IntegerList actionIDs,
  EPTF_IntegerList paramList
}

///////////////////////////////////////////////////////////
//  Type: EPTF_Rendezvous_WaitForNTriggerClientsList
// 
//  Purpose:
//   A list for storing WaitForNTrigger type clients
///////////////////////////////////////////////////////////

type record of EPTF_Rendezvous_WaitForNTriggerClients EPTF_Rendezvous_WaitForNTriggerClientsList
type record of EPTF_Rendezvous_StateTriggerClients EPTF_Rendezvous_StateTriggerClientsList

///////////////////////////////////////////////////////////
//  Type: Rendezvous_Client_Req_Type 
// 
//  Purpose:
//   Request type message
// 
//  Elements:
//    rendezvousType - <EPTF_Rendezvous_types> - the type of rendezvous
//    semaphoreID - *integer* - the ID of the locking semaphore
//    rendezvousID - *integer* - the ID of the rendezvous type
//    rendezvousParam - <EPTF_IntegerList> - rendezvous type parameters
//    extendedRendezvousID - *charstring* - charstring ID of the rendezvous type
// 
//  Detailed Comments:
//
//////////////////////////////////////////////////////////

type record Rendezvous_Client_Req_Type
{
  EPTF_Rendezvous_types rendezvousType,
  integer semaphoreID,
  integer rendezvousID,
  EPTF_IntegerList rendezvousParam optional,
  charstring extendedRendezvousID optional
}

///////////////////////////////////////////////////////////
//  Type: Rendezvous_Client_Resp_Type 
// 
//  Purpose:
//   Response type message
// 
//  Elements:
//    rendezvousType - <EPTF_Rendezvous_types> - the type of rendezvous
//    semaphoreID - *integer* - the ID of the locking semaphore
//    rendezvousID - *integer* - the ID of the rendezvous type
//    rendezvousParam - <EPTF_IntegerList> - rendezvous type parameters
//    extendedRendezvousID - *charstring* - charstring ID of the rendezvous type
// 
//  Detailed Comments:
//
//////////////////////////////////////////////////////////

type record Rendezvous_Client_Resp_Type
{
  EPTF_Rendezvous_types rendezvousType,
  integer semaphoreID,
  integer rendezvousID,
  EPTF_IntegerList rendezvousParam optional,
  charstring extendedRendezvousID optional
}

///////////////////////////////////////////////////////////
//  Type: EPTF_Rendezvous_Msg
// 
//  Purpose:
//     Rendezvous message type what can be sent on <EPTF_RendezvousPort_PT>
// 
//  Elements:
//    Rendezvous_Client_Req - <Rendezvous_Client_Req_Type> - request type message.
//    Rendezvous_Client_Resp - <Rendezvous_Client_Resp_Type> - response type message.
// 
//  Detailed Comments:
//    There are two types of messages Request type and Response type. Request can be sent by rendezvous clients.
//    After receiving the Request, Rendezvous servers send Responses back to the clients.
//    Rendezvous Messages can be sent on <EPTF_RendezvousPort_PT>.
//
/////////////////////////////////////////////////////////// 

type union EPTF_Rendezvous_Msg
{
  Rendezvous_Client_Req_Type Rendezvous_Client_Req,
  Rendezvous_Client_Resp_Type Rendezvous_Client_Resp
}

///////////////////////////////////////////////////////////
//  Type: EPTF_RendezvousPort_PT
// 
//  Purpose:
//     Internal message port type for exchanging configuration messages.
// 
//  Elements:
//    -
// 
//  Detailed Comments:
//    Can bothway send messages type <EPTF_Rendezvous_Msg>. 
///////////////////////////////////////////////////////////  

type port EPTF_RendezvousPort_PT message {
    inout EPTF_Rendezvous_Msg;
  } with {extension "internal"}

///////////////////////////////////////////////////////////
//  Type: EPTF_Rendezvous_types
// 
//  Purpose:
//     Enumerated type for chosing rendezvous types.
// 
//  Elements:
//    WaitForATrigger - WaitForATrigger type rendezvous for 2 clients
//    WaitForNTrigger - WaitForNTrigger type rendezvous for n clients
// 
//  Detailed Comments:
//    Enumerated type for chosing possible rendezvous types. 
///////////////////////////////////////////////////////////  

type enumerated EPTF_Rendezvous_types {
  WaitForATrigger(0), 
  WaitForNTrigger(1),
  StateTrigger (2)
}

type function ft_EPTF_Rendezvous_NotifyAction(in charstring rendezvousID, in integer rendezvousID_int) runs on self;
type record of ft_EPTF_Rendezvous_NotifyAction EPTF_Rendezvous_ActionList;

///////////////////////////////////////////////////////////
//  Type: EPTF_Rendezvous_CT
// 
//  Purpose:
//     Rendezvous Server component.
// 
//  Elements:
//    v_EPTF_RendezvousServer_def - *default* - default altstep in RendezvousServer component
//    v_EPTF_RendezvousServer_initialized - *boolean* - if the server is initialized
//    vg_EPTF_Rendezvous_Wait4TriggerLookupTable - *charstring* - the name of the lookup table for WaitForATrigger type rendezvous.
//    vg_EPTF_Rendezvous_Wait4TriggerLookupTableID - *integer* - the ID of the lookup table for WaitForATrigger type rendezvous.
//    vg_EPTF_Rendezvous_Wait4NTriggerLookupTable - *charstring* - the name of the lookup table for WaitForNTrigger type rendezvous.
//    vg_EPTF_Rendezvous_Wait4NTriggerLookupTableID - *integer* - the ID of the lookup table for WaitForNTrigger type rendezvous.
//    vg_EPTF_Rendezvous_freeBusyQueue - <EPTF_FreeBusyQueue> - A FreeBusyQueue, storing client list IDs for WaitForNTrigger type rendezvous.
//    vg_WaitForNTriggerClients - <EPTF_Rendezvous_WaitForNTriggerClientsList> - List, storing clientIDs for WaitForNTrigger type rendezvous.
// 
//  Detailed Comments:
//    This is the Rendezvous Server component. The rendezvous server supports a set of rendezvous services with 
//    predefined types. Supported rendezvous types are "Wait for a trigger", "Wait for N trigger" rendezvous.
//
/////////////////////////////////////////////////////////// 

type component EPTF_Rendezvous_CT extends EPTF_Base_CT, EPTF_Logging_CT, EPTF_HashMap_CT, EPTF_FBQ_CT {
  private port EPTF_RendezvousPort_PT EPTF_RendezvousServer_Port_CP;

  private var default v_EPTF_RendezvousServer_def := null; 
  private var boolean v_EPTF_RendezvousServer_initialized:=false;
  private var charstring vg_EPTF_Rendezvous_Wait4TriggerLookupTable;
  private var integer vg_EPTF_Rendezvous_Wait4TriggerLookupTableID;
  private var charstring vg_EPTF_Rendezvous_Wait4NTriggerLookupTable;
  private var integer vg_EPTF_Rendezvous_Wait4NTriggerLookupTableID;
  private var charstring vg_EPTF_Rendezvous_StateTriggerLookupTable;
  private var integer vg_EPTF_Rendezvous_StateTriggerLookupTableID;
  private var EPTF_FreeBusyQueue vg_EPTF_Rendezvous_freeBusyQueue;
  private var EPTF_FreeBusyQueue vg_EPTF_Rendezvous_StateTriggerQueue;
  private var EPTF_Rendezvous_WaitForNTriggerClientsList vg_WaitForNTriggerClients; 
  private var EPTF_Rendezvous_StateTriggerClientsList vg_StateTriggerClients; 

  // logging
  private var integer v_Rendezvous_loggingMaskId := c_EPTF_Logging_invalidMaskId;  
}

///////////////////////////////////////////////////////////
//  Type: EPTF_RendezvousClient_CT
// 
//  Purpose:
//     Rendezvous Client component.
// 
//  Elements:
//    v_EPTF_RendezvousClient_def - *default* - the default receive altstep
//    v_EPTF_RendezvousClient_initialized - *boolean* - if the server is initialized
//    v_EPTF_RendezvousClient_Server - <EPTF_Rendezvous_CT> - the rendezvous server component
// 
//  Detailed Comments:
//    This is the Rendezvous Client component. The rendezvous client sends the rendezvous requests and
//    wait for the response.
//
/////////////////////////////////////////////////////////// 

type component EPTF_RendezvousClient_CT extends EPTF_Base_CT, EPTF_Semaphore_CT, EPTF_Logging_CT {
  private port EPTF_RendezvousPort_PT EPTF_RendezvousClient_Port_CP; 

  private var default v_EPTF_RendezvousClient_def:=null
  private var boolean v_EPTF_RendezvousClient_initialized := false;
  private var EPTF_Rendezvous_CT v_EPTF_RendezvousClient_Server;

  private var EPTF_FreeBusyQueue v_EPTF_RendezvousClient_ActionQueue;
  private var EPTF_Rendezvous_ActionList v_EPTF_RendezvousClient_Actions := {};
  // logging
  private var integer v_RendezvousClient_loggingMaskId := c_EPTF_Logging_invalidMaskId;
}

const integer c_StateTrigger_OPERATIONIDX := 0;
const integer c_StateTrigger_SUBSCRIBE := 0;
const integer c_StateTrigger_NEWSTATE := 1;
const integer c_StateTrigger_NOTIFY := 2;
const integer c_StateTrigger_UNSUBS := 3;

const integer c_StateTrigger_ACTIONIDX := 1;

const integer c_StateTrigger_PARAMIDX := 2;
const integer c_StateTrigger_NOPARAM := 0;
const integer c_StateTrigger_PERMANENTSUBS := 1;

const EPTF_IntegerList c_StateTrigger_InitinalParams := {-1,-1,0};

///////////////////////////////////////////////////////////
//  Constant: c_EPTF_Rendezvous_loggingEventClasses
// 
//  Purpose:
//    list of logging event class names used on the Rendezvous
// 
//  Detailed Comments:
//    <EPTF_Logging_EventClassPrefixList> { "Warning", "Debug" }
///////////////////////////////////////////////////////////
const EPTF_Logging_EventClassPrefixList c_EPTF_Rendezvous_loggingEventClasses := { "Warning", "Debug" };

///////////////////////////////////////////////////////////
//  Constant: c_EPTF_Rendezvous_loggingClassIdx_Warning
// 
//  Purpose:
//    logging class index for Error
// 
//  Detailed Comments:
//    *0*
///////////////////////////////////////////////////////////
const integer c_EPTF_Rendezvous_loggingClassIdx_Warning := 0;
///////////////////////////////////////////////////////////
//  Constant: c_EPTF_Rendezvous_loggingClassIdx_Debug
// 
//  Purpose:
//    logging class index for Error
// 
//  Detailed Comments:
//    *1*
///////////////////////////////////////////////////////////
const integer c_EPTF_Rendezvous_loggingClassIdx_Debug := 1;

}  // end of module
