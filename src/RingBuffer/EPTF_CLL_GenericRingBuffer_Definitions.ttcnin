///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_GenericRingBuffer_Definitions
// 
//  Purpose:
//    This module provides macro type definitions for generic ring buffer container.
// 
//  Module Parameters:
//      (none)
//
//  Module depends on:
//    <EPTF_CLL_RingBuffer_Definitions>
// 
//  Current Owner:
//    Istvan Falusi (eistfal)
// 
//  Last Review Date:
//    2007-11-19
// 
//  Detailed Comments:
//    This module contains types for generic ring buffer.
//    This file should be included into the module, where the base type 
//    of the ring buffer is known and the ring buffer is to be created.
// 
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Type: EPTF_BASE_TYPE
// 
//  Purpose:
//    The user defined TTCN-3 type that the generic ring buffer is built from.
//    This type shall be defined by the user of EPTF ring buffer.
// 
//  Elements:
//    User defined.
// 
// Detailed Comments:
//   To create a ring buffer from any user defined type <EPTF_BASE_TYPE> the
//   following TTCN-3 code should be inserted into one of the the user modules:
//
//   #define EPTF_BASE_TYPE <EPTF_BASE_TYPE>
//
//   //instantiate the code
//
//   #include "EPTF_CLL_GenericRingBuffer_Functions.ttcnin"
//
//   //undef the type to avoid error in case of multiple careless "#include"-s
//
//   #undef EPTF_BASE_TYPE
//
//
///////////////////////////////////////////////////////////

import from EPTF_CLL_RingBuffer_Definitions all;

///////////////////////////////////////////////////////////
//  Type: EPTF_BASE_TYPE##List
// 
//  Purpose:
//    Provides generic data list.
// 
//  Elements:
//    *record of* <EPTF_BASE_TYPE>
// 
///////////////////////////////////////////////////////////
#define EPTF_RINGBUFFER_DATA_LIST_TYPE_(TYPEARG) type record of TYPEARG TYPEARG##List;
#define EPTF_RINGBUFFER_DATA_LIST_TYPE(TYPEARG) EPTF_RINGBUFFER_DATA_LIST_TYPE_(TYPEARG)
EPTF_RINGBUFFER_DATA_LIST_TYPE(EPTF_BASE_TYPE)

///////////////////////////////////////////////////////////
//  Type: EPTF_BASE_TYPE##RingBuffer
// 
//  Purpose:
//    This datastructure the generic ring buffer type
// 
//  Elements:
//    - status - <EPTF_RingBufferStatus> - contains the status information of the ring buffer.
//
//    - data- <EPTF_BASE_TYPE##List> - contains the data to be buffered. 
// 
///////////////////////////////////////////////////////////
#define EPTF_RINGBUFFER_TYPE_(TYPEARG) type record TYPEARG##RingBuffer {EPTF_RingBufferStatus status, TYPEARG##List data}
#define EPTF_RINGBUFFER_TYPE(TYPEARG) EPTF_RINGBUFFER_TYPE_(TYPEARG)
EPTF_RINGBUFFER_TYPE(EPTF_BASE_TYPE)

