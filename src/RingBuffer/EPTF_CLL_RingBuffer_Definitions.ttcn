///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_CLL_RingBuffer_Definitions
//
// Purpose:
//   This module contains the types and constants of ring buffer
//
// Module Parameters:
//     -
//
//  Module depends on:
//     -
// 
// Current Owner:
//   Istvan Falusi (EISTFAL)
//
// Last Review Date:
//   2007-11-19
//
///////////////////////////////////////////////////////////
module EPTF_CLL_RingBuffer_Definitions
{

//=========================================================================
// Data Types
//=========================================================================

///////////////////////////////////////////////////////////
// Type: EPTF_RingBufferStatus
//
// Purpose:
//   Record type to describe status information of ring buffer
//
// Elements:
//   head - *integer* - index of first data item
//
//   tail - *integer* - index of last data item
//
//   content_size - *integer* number of buffered items
//
//   capacity  - *integer* possible maximum of buffered items
//
// Detailed Comments:
//   NOTE: If (content_size==0) then both head and tail are invalid
//
///////////////////////////////////////////////////////////
type record EPTF_RingBufferStatus
{
  integer head,
  integer tail,
  integer content_size,
  integer capacity
}

///////////////////////////////////////////////////////////
// Type: EPTF_RingBufferDataItem
//
// Purpose:
//   Type of buffered items. It is a simple alaias to integer.
//
///////////////////////////////////////////////////////////
type integer EPTF_RingBufferDataItem;

///////////////////////////////////////////////////////////
// Type: EPTF_RingBufferDataList
//
// Purpose:
//   Type for define a fixed length array for buffering.
//
// Elements:
//   record of <EPTF_RingBufferDataItem>
//
///////////////////////////////////////////////////////////
type record of EPTF_RingBufferDataItem EPTF_RingBufferDataList;

///////////////////////////////////////////////////////////
// Type: EPTF_IntegerRingBuffer
//
// Purpose:
//   This is the definition of EPTF_FreeBusyQueue, which is record of 
//   the queue's status and list of the queue items
//
// Elements:
//   status - <EPTF_RingBufferStatus> - status info of ring buffer 
//
//   itemList  - <EPTF_RingBufferDataList> - list of buffered elements
//
///////////////////////////////////////////////////////////
type record EPTF_IntegerRingBuffer 
{
  EPTF_RingBufferStatus   status,
  EPTF_RingBufferDataList data
}

//=========================================================================
// Constants
//=========================================================================

///////////////////////////////////////////////////////////
// Constant: c_EPTF_emptyRingBufferStatus
//
// Purpose:
//   useful constant to initiate the status info of an empty ring buffer
//
///////////////////////////////////////////////////////////
const EPTF_RingBufferStatus c_EPTF_emptyRingBufferStatus 
  := { head :=0, tail := c_EPTF_RB_default_capacity - 1, content_size := 0, capacity := c_EPTF_RB_default_capacity };

///////////////////////////////////////////////////////////
// Constant: c_EPTF_RB_default_capacity
//
// Purpose:
//   defines the default capacity of ring buffers
//
///////////////////////////////////////////////////////////
const integer c_EPTF_RB_default_capacity := 100;

}  // end of module
