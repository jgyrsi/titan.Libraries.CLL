///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_StatHandlerUI_Definitions
// 
//  Purpose:
//    This module contains type definitions for StatHandlerUI.
// 
//  Module depends on:
//    <EPTF_CLL_LoggingUI_Definitions>
//    <EPTF_CLL_StatHandler_Definitions>
//    <EPTF_CLL_StatHandlerClient_Definitions>
//
//  Module Parameters:
//    -
//
//  Current Owner:
//    Gabor Tatarka (EGBOTAT)
// 
//  Last Review Date:
//    -
//
//  Detailed Comments:
//    This module contains type definitions for StatHandlerUI.
//
///////////////////////////////////////////////////////////////

module EPTF_CLL_StatHandlerUI_Definitions {

import from EPTF_CLL_LoggingUI_Definitions all;
import from EPTF_CLL_StatHandler_Definitions all;
import from EPTF_CLL_StatHandlerClient_Definitions all;

/////////////////////////////////////////////////////////////////////////
//  Component: EPTF_StatHandlerUI_CT
//
//  Purpose:
//    Component type for StatHandlerUI
//
//  Elements:
//    v_EPTF_StatHandlerUI_initialized - *boolean*
//    v_EPTF_StatHandlerUI_resetButtonContent - *integer*
//
//  Detailed Comments:
//    Extends EPTF_LoggingUIClient_CT, EPTF_StatHandler_CT
/////////////////////////////////////////////////////////////////////////
type component EPTF_StatHandlerUI_CT extends EPTF_LoggingUIClient_CT, EPTF_StatHandler_CT
{
  private var boolean v_EPTF_StatHandlerUI_initialized := false;
  private var integer v_EPTF_StatHandlerUI_resetButtonContent := 0;
}

/////////////////////////////////////////////////////////////////////////
//  Component: EPTF_StatHandlerClientUI_CT
//
//  Purpose:
//    Component type for StatHandlerClientUI
//
//  Elements:
//    v_EPTF_StatHandlerClientUI_initialized - *boolean*
//
//  Detailed Comments:
//    Extends EPTF_LoggingUIClient_CT, EPTF_StatHandlerClient_CT
/////////////////////////////////////////////////////////////////////////
type component EPTF_StatHandlerClientUI_CT extends EPTF_LoggingUIClient_CT, EPTF_StatHandlerClient_CT
{
  private var boolean v_EPTF_StatHandlerClientUI_initialized := false;
}

/////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_StatHandlerUI_guiVarNamePrefix
//
// Purpose:
//    Prefix used in generation variable names for UIHandler
/////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_StatHandlerUI_guiVarNamePrefix := "GUI";

/////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_StatHandlerUI_tabpanel
//
// Purpose:
//    Widget ID for the tab panel of StatHandlerUI
/////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_StatHandlerUI_tabpanel := "EPTF_StatHandlerUI_tabpanel";

/////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_StatHandlerUI_tabpanel
//
// Purpose:
//    Widget ID for the tree of StatHandlerUI
/////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_StatHandlerUI_tree := "EPTF_StatHandlerUI_tree";

/////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_StatHandlerUI_guiVarNamePrefix
//
// Purpose:
//    Widget ID for the reset button of StatHandlerUI
/////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_StatHandlerUI_resetButton := "EPTF_StatHandler_resetButton";

/////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_StatHandlerUI_ResetVarNameLocal
//
// Purpose:
//    Local reset button Var name - depricated, only for backward compability
/////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_StatHandlerUI_ResetVarNameLocal := c_EPTF_StatHandler_ResetVarNameLocal;
/////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_StatHandlerUI_ResetVarNameGlobal
//
// Purpose:
//    Global reset button Var name - depricated, only for backward compability
/////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_StatHandlerUI_ResetVarNameGlobal := c_EPTF_StatHandler_ResetVarNameGlobal;

} // end of module
