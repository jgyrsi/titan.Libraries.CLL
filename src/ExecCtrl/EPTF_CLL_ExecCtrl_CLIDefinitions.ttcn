///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_ExecCtrl_CLIDefinitions
// 
//  Purpose:
//    This module contains the type definitions of EPTF_CLL_ExecCtrl
//    Command Line Interface.
// 
//  Module depends on:
//    <EPTF_CLL_ExecCtrl_Definitions>
//    <EPTF_CLL_CLI_Definitions>
//
//  Module Parameters:
//
//  Current Owner:
//    Jozsef Gyurusi (ethjgi)
// 
//  Last Review Date:
//    2010-03-30
//
//  Detailed Comments:
//    This module contains the Command Line Interface type definitions
//    for the EPTF_CLL_ExecCtrl.
//
///////////////////////////////////////////////////////////////
module EPTF_CLL_ExecCtrl_CLIDefinitions {

import from EPTF_CLL_ExecCtrl_Definitions all;
import from EPTF_CLL_CLI_Definitions all;

type component EPTF_ExecCtrl_CLI_CT extends EPTF_ExecCtrl_CT, EPTF_CLI_Client_CT {
  var charstring v_ExecCtrl_CLI_commandPrefix := "";
  var boolean v_ExecCtrl_CLI_initialized := false;
}

const charstring c_EPTF_ExecCtrl_CLI_start := "startExec";
const charstring c_EPTF_ExecCtrl_CLI_stop := "stopExec";
const charstring c_EPTF_ExecCtrl_CLI_terminate := "terminateExec";
const charstring c_EPTF_ExecCtrl_CLI_setCPS := "setCPS";
const charstring c_EPTF_ExecCtrl_CLI_getTargetCPS := "getTargetCPS";
const charstring c_EPTF_ExecCtrl_CLI_getCurrentCPS := "getCurrentCPS";
const charstring c_EPTF_ExecCtrl_CLI_getWeight := "getWeight";
const charstring c_EPTF_ExecCtrl_CLI_setWeight := "setWeight";
const charstring c_EPTF_ExecCtrl_CLI_getTotalCounter := "getTotalCounter";
const charstring c_EPTF_ExecCtrl_CLI_exitTTCN := "exitTTCN";

} //~module EPTF_CLL_ExecCtrl_CLIDefinitions
