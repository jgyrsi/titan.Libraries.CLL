///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Module: EPTF_CLL_ExecCtrlUIHandler_Definitions 
//
// Purpose:
//    This module contains type definitions for EPTF_CLL_ExecCtrl_UIHandler implementation.
//
// Module Parameters:
//    tsp_EPTF_ExecCtrl_UIHandler_loggingComponentMask - *charstring* - component-type mask for logging, default value: "EPTF_ExecCtrl_UIhandler"
//    tsp_EPTF_ExecCtrlClient_UIHandler_loggingComponentMask - *charstring* - component-type mask for logging, default value: "EPTF_ExecCtrlClient_UIhandler"
//    tsp_EPTF_ExecCtrl_UIHandler_refreshRate_ScGrpStatus - *integer* - refresh rate for the Scenario group status LEDs, default: use syncInterval
//    tsp_EPTF_ExecCtrl_UIHandler_refreshRate_CPSChart - *integer* - refresh rate for the CPS Chart, default: use syncInterval
//    tsp_EPTF_ExecCtrl_UIHandler_refreshRate_TCStats - *integer* - refresh rate for the Traffic Case stats, default: use syncInterval
//    tsp_EPTF_ExecCtrl_UIHandler_refreshRate_TimeElapsed - *integer* - refresh rate for the Time Elapsed text, default: use syncInterval
//    tsp_EPTF_ExecCtrl_UIHandler_refreshRate_EntityResourcesTab - *integer* - refresh rate for the Entity resource data on the ClientResources tab, default: use syncInterval
//    tsp_EPTF_ExecCtrl_UIHandler_refreshRate_targetCPS - *integer* - refresh rate for the target CPS values, default: use syncInterval
//    tsp_EPTF_ExecCtrl_UIHandler_refreshRate_currentCPS - *integer* - refresh rate for the Current CPS values, default: use syncInterval
//    tsp_EPTF_ExecCtrl_UIHandler_refreshRate_regulatedItem - *integer* - refresh rate for the RegulatedItems table on the Regulator tab, default: use syncInterval
//    tsp_EPTF_ExecCtrlClient_UIHandler_refreshRate_FSMStats - *integer* -  refresh rate for the Test case FSM statistics, default: use syncInterval
//
//  Module depends on:
//    <XTDPasp_PortType>
//    <XTDP_PDU_Defs>
//    <EPTF_CLL_UIHandlerClient_Definitions>
//    <EPTF_CLL_UIHandler_MsgDefinitions>
//    <EPTF_CLL_ExecCtrl_Definitions>
//    <EPTF_CLL_ExecCtrl_CLIDefinitions>
//    <EPTF_CLL_Common_Definitions>
//    <EPTF_CLL_Logging_Definitions>
//    <EPTF_CLL_LoggingUI_Definitions>
//    <EPTF_CLL_StatHandlerUI_Definitions>
//
//  Current Owner:
//    EFLOATT
//
// Last Review Date:
//   2010-xx-xx
///////////////////////////////////////////////////////////////////////////////
module EPTF_CLL_ExecCtrlUIHandler_Definitions 
{
//===========================================================================
// Import part
//===========================================================================
import from EPTF_CLL_UIHandlerClient_Definitions all;
import from EPTF_CLL_UIHandler_MsgDefinitions all;
import from EPTF_CLL_ExecCtrl_Definitions all;
import from EPTF_CLL_ExecCtrl_CLIDefinitions all;

import from EPTF_CLL_LGenBaseStatsUI_Definitions all;

import from EPTF_CLL_Logging_Definitions all;
import from EPTF_CLL_LoggingUI_Definitions all;

import from EPTF_CLL_StatHandlerUI_Definitions all;

modulepar charstring tsp_EPTF_ExecCtrl_UIHandler_loggingComponentMask := "EPTF_ExecCtrl_UIHandler";
modulepar charstring tsp_EPTF_ExecCtrlClient_UIHandler_loggingComponentMask := "EPTF_ExecCtrlClient_UIHandler";

//refresh rates:
modulepar integer tsp_EPTF_ExecCtrl_UIHandler_refreshRate_ScGrpStatus := 0; // refresh rate for the Scenario group status LEDs, default: use syncInterval
modulepar integer tsp_EPTF_ExecCtrl_UIHandler_refreshRate_CPSChart := 0; // refresh rate for the CPS Chart, default: use syncInterval
modulepar integer tsp_EPTF_ExecCtrl_UIHandler_refreshRate_TCStats := 0; // refresh rate for the Traffic Case stats, default: use syncInterval
modulepar integer tsp_EPTF_ExecCtrl_UIHandler_refreshRate_TimeElapsed := 0; // refresh rate for the Time Elapsed text, default: use syncInterval
modulepar integer tsp_EPTF_ExecCtrl_UIHandler_refreshRate_EntityResourcesTab := 0; // refresh rate for the Entity resource data on the ClientResources tab, default: use syncInterval
modulepar integer tsp_EPTF_ExecCtrl_UIHandler_refreshRate_targetCPS := 0; // refresh rate for the target CPS values, default: use syncInterval
modulepar integer tsp_EPTF_ExecCtrl_UIHandler_refreshRate_currentCPS := 0; // refresh rate for the Current CPS values, default: use syncInterval
modulepar integer tsp_EPTF_ExecCtrl_UIHandler_refreshRate_regulatedItem := 0; // refresh rate for the RegulatedItems table on the Regulator tab, default: use syncInterval
modulepar integer tsp_EPTF_ExecCtrlClient_UIHandler_refreshRate_FSMStats := 0; //  refresh rate for the Test case FSM statistics, default: use syncInterval

//===========================================================================
// Component types
//===========================================================================

/////////////////////////////////////////////////////////////////////////////  
//  Type: EPTF_ExecCtrl_UIHandler_CT
//
//  Purpose:
//    The Execution Control UI Handler component
//
//  Extensions:
//    EPTF_ExecCtrl_CT, EPTF_UIHandlerClient_CT, EPTF_LoggingUIClient_CT
//
//  Variables:    
//    -
//  Ports:  
//    -
//
//  Function types:
//    -
//
//  Detailed Comments:
//      -
/////////////////////////////////////////////////////////////////////////////
type component EPTF_ExecCtrl_UIHandler_CT 
extends EPTF_ExecCtrl_CT, EPTF_UIHandlerClient_CT, EPTF_LoggingUIClient_CT,
  EPTF_StatHandlerUI_CT, EPTF_StatHandlerClientUI_CT, EPTF_ExecCtrl_CLI_CT {
  private var boolean v_EPTF_ExecCtrl_UIHandler_initialized := false;
  private var default v_EPTF_ExecCtrl_UIHandler_def;
  private var EPTF_UIHandler_WidgetIdString v_EPTF_GUI_Main_Tabbox_WidgetId := "";

  private timer t_ExecCtrl_UIHandler_readyToRun := 0.0;
  private var boolean v_EPTF_ExecCtrl_UIHandler_guiDone := false; // set to true when the creation of the gui is finished.
  private var integer v_ExecCtrl_pendingGuiDoneCounter := 0; // counter for pending client gui dones
  
  //refresh rates:
  private var integer v_ExecCtrl_UIHandler_refreshRate_ScGrpStatus := 0; // default
  private var integer v_ExecCtrl_UIHandler_refreshRate_CPSChart := 0; // default
  private var integer v_ExecCtrl_UIHandler_refreshRate_TCStats := 0; // default
  private var integer v_ExecCtrl_UIHandler_refreshRate_TimeElapsed := 0; // default
  private var integer v_ExecCtrl_UIHandler_refreshRate_EntityResourcesTab := 0; // default
  private var integer v_ExecCtrl_UIHandler_refreshRate_targetCPS := 0; // default
  private var integer v_ExecCtrl_UIHandler_refreshRate_currentCPS := 0; // default
  private var integer v_ExecCtrl_UIHandler_refreshRate_regulatedItem := 0; // default

  // logging
  private var integer v_ExecCtrl_UIHandler_loggingMaskId := c_EPTF_Logging_invalidMaskId;
}

/////////////////////////////////////////////////////////////////////////////  
//  Type: EPTF_ExecCtrlClient_UIHandler_CT
//
//  Purpose:
//    The Execution Control UI Handler Client component
//
//  Extensions:
//    EPTF_ExecCtrlClient_CT, EPTF_LGenBaseStatsUI_CT, EPTF_LoggingUIClient_CT
//
//  Variables:    
//    -
//  Ports:  
//    -
//
//  Function types:
//    -
//
//  Detailed Comments:
//      -
/////////////////////////////////////////////////////////////////////////////
type component EPTF_ExecCtrlClient_UIHandler_CT 
extends EPTF_ExecCtrlClient_CT, EPTF_LGenBaseStatsUI_CT, EPTF_LoggingUIClient_CT, EPTF_StatHandlerClientUI_CT {

  private var boolean v_ExecCtrl_UIHandlerClient_initialized := false;

  //refresh rates:
  private var integer v_ExecCtrlClient_UIHandler_refreshRate_FSMStats := 0; // default

  // logging
  private var integer v_ExecCtrlClient_UIHandler_loggingMaskId := c_EPTF_Logging_invalidMaskId;
}

//===========================================================================
// Types
//===========================================================================

//===========================================================================
// Constants
//===========================================================================

///////////////////////////////////////////////////////////////////////////////
// Constant: Execution control widget id constants
//
// Purpose:
//   Used to reach widget id of certain elements on the runtime GUI 
//
// Detailed comments:
//    Available constants:
//    - c_EPTF_ExecCtrl_EntityGrpTabId
//    - c_EPTF_ExecCtrl_ResourceTabId
//    - c_EPTF_ExecCtrl_TrafficTabId_Scenario
//    - c_EPTF_ExecCtrl_TrafficTabId_TC
//    - c_EPTF_ExecCtrl_Start
//    - c_EPTF_ExecCtrl_StatReset
//    - c_EPTF_ExecCtrl_Stop
//    - c_EPTF_ExecCtrl_Exit
//    - c_EPTF_ExecCtrl_RunTest
//    - c_EPTF_ExecCtrl_Snapshot
//    - c_EPTF_ExecCtrl_CPS
//    
///////////////////////////////////////////////////////////////////////////////

//===========================================================================
// Constants
//===========================================================================

///////////////////////////////////////////////////////////////////////////////
// Constant: General widget id constants
//
// Purpose:
//   Used to reach widget id of certain elements on the runtime GUI 
//
// Detailed comments:
//    Available constants:
//    - c_EPTF_GUI_LoggingUI_Tabpanel_WidgetId
//    - c_EPTF_GUI_Main_Window_WidgetId
//    - c_EPTF_GUI_Main_Tabbox_WidgetId
//    - c_EPTF_GUI_Main_hbox_WidgetId
//    - c_EPTF_runtimeGuiStartTestButtonWidgetId
//    - c_EPTF_runtimeGuiStopTestButtonWidgetId
//    - c_EPTF_runtimeGuiRunTestButtonWidgetId
//    - c_EPTF_runtimeGuiExitButtonWidgetId
//    - c_EPTF_runtimeGuiSnapshotButtonWidgetId
//
//    - c_EPTF_ExecCtrl_EntityGrpTabId
//    - c_EPTF_ExecCtrl_ResourceTabId
//    - c_EPTF_ExecCtrl_GroupDistributionTabId
//    - c_EPTF_ExecCtrl_TrafficTabId_Scenario
//    - c_EPTF_ExecCtrl_TrafficTabId_TC
//    
///////////////////////////////////////////////////////////////////////////////

// widget id of LoggingUI main tabpanel
const EPTF_UIHandler_WidgetIdString c_EPTF_GUI_LoggingUI_Tabpanel_WidgetId := 
"EPTF_LoggingUI_tabpanel";

// widget id of main windows  
const EPTF_UIHandler_WidgetIdString c_EPTF_GUI_Main_Window_WidgetId := "EPTF_Main_Window";

// widget id of main tabbox
const EPTF_UIHandler_WidgetIdString c_EPTF_GUI_Main_Tabbox_WidgetId := "EPTF_Main_tabbox";

// widget id of main hbox
const EPTF_UIHandler_WidgetIdString c_EPTF_GUI_Main_hbox_WidgetId := "EPTF_Main_hbox";

// widget id of start test button
const EPTF_UIHandler_WidgetIdString c_EPTF_runtimeGuiStartTestButtonWidgetId := 
"EPTF_start_test_button"

// widget id of stop test button
const EPTF_UIHandler_WidgetIdString c_EPTF_runtimeGuiStopTestButtonWidgetId := 
"EPTF_stop_test_button"

// widget id of terminate test button
const EPTF_UIHandler_WidgetIdString c_EPTF_runtimeGuiTerminateTestButtonWidgetId := 
"EPTF_terminate_test_button"

// widget id of runtie GUI run test button
const EPTF_UIHandler_WidgetIdString c_EPTF_runtimeGuiRunTestButtonWidgetId := 
"EPTF_run_test_button";

// widget id of runtime GUI exit ttcn button
const EPTF_UIHandler_WidgetIdString c_EPTF_runtimeGuiExitButtonWidgetId := 
"EPTF_exit_ttcn_button";

// widget id of runtime GUI snapshot button 
const EPTF_UIHandler_WidgetIdString c_EPTF_runtimeGuiSnapshotButtonWidgetId := 
"EPTF_snapshot_button";

// widget id of time elapsed
const EPTF_UIHandler_WidgetIdString c_EPTF_runtimeGuiTimeElapsedWidgetId := 
"EPTF_timeElapsed"


const EPTF_UIHandler_WidgetIdString c_EPTF_ExecutionControlWidgetId := 
"Execution_Control.tab";

const EPTF_UIHandler_WidgetIdString c_EPTF_ExecutionControlButtonBarWidgetId := 
"Execution_Control.buttonBar";

const EPTF_UIHandler_WidgetIdString c_EPTF_ExecCtrl_EntityGrpTabId := 
"EntityGroup_tab";
const EPTF_UIHandler_WidgetIdString c_EPTF_ExecCtrl_ResourceTabId := 
"Resource_tab";
const EPTF_UIHandler_WidgetIdString c_EPTF_ExecCtrl_GroupDistributionTabId :=
"EntityGroupDistribution_tab";
const EPTF_UIHandler_WidgetIdString c_EPTF_ExecCtrl_TrafficTabId_Scenario := 
"Traffic_Scenario_tab";
const EPTF_UIHandler_WidgetIdString c_EPTF_ExecCtrl_TrafficTab_HBox :=
"Traffic_Scenario_hbox";
const EPTF_UIHandler_WidgetIdString c_EPTF_ExecCtrl_TrafficTabId_TC := 
"Traffic_TC_tab";

const EPTF_UIHandler_WidgetIdString c_EPTF_ExecCtrl_RegulatedItemsTabId :=
"RegulatedItems_tab";
const EPTF_UIHandler_WidgetIdString c_EPTF_ExecCtrl_RegulatorsTotalValueTabId :=
"RegulatorsTotalValue_tab"

const EPTF_UIHandler_WidgetIdString c_EPTF_ExecCtrl_PhaseListsTabboxId :=
"Execution_Control.PhaseLists.tabbox";

const octetstring c_EPTF_UIHandler_XTDP_Image_Terminate := '89504E470D0A1A0A0000000D4948445200000016000000160806000000C4B46C'O &
                                          '3B000000017352474200AECE1CE900000006624B474400FF00FF00FFA0BDA793'O &
                                          '000000097048597300000B0B00000B0B016D0484B70000000774494D4507DA05'O &
                                          '1C09001E5880F452000003984944415438CBA595CD6B545718C67FE79D73EFCC'O &
                                          '752671628DE91032935629A6A1A00B176E8A8850DCB445240E22DD08AE5BBAE8'O &
                                          '3F50F00FA8E02A9BD2D2602D14BA684570A76DA92832AD8A929049C887131375'O &
                                          '321F77E6CE7D4F17C9D8549B34B60F3C8BF370CEEFBCBC87738E610B4D140A1E'O &
                                          'D0EF9CCB29EC588F9B0A8BCEB9CA47B3B3EDCDD69A2DA0878131E00090174803'O &
                                          'C4CE351466813BB173977D91EBC57239FE57F044A1B017F8D8C2E9DDD666733B'O &
                                          '778A3F3C8CBF6F1F924AD19E9EA6F5F0218F9696F45114359BCE5D77CE7DEE8B'O &
                                          'FC5C2C973BFF089E28140AC0C5C098E3238383EC3A7386E0D021BCC1418CE73D'O &
                                          '9F172D2CD0BA778FA71313FC7EEB1655D57B0A9F79C6FCD8859B172AFDE2756B'O &
                                          'DFDB7BFCB864C7C6F00606B63A02E27A9DD52B57981C1F77D3EDF6C3D8B9B3BE'O &
                                          'C88D62B9AC89EEA493D9ECF9C098E2DBC78E99BED3A711DFC785E14BD666136D'O &
                                          '36716108718C3F3C4C3A084CB5547AADAEEA258CB97A329B8D2CC037F9FC610B'O &
                                          'A746723932478FA2B51A2B972EB11D65C7C6080E1EE4AD5289FAEDDBEFD7E3F8'O &
                                          '3B5FE427FB753EEF298CEDB2B63773E408743AC4CBCB000C8D8F6F099D3D7B16'O &
                                          'ADD5D046836074943DA552CF8CEA8950F5170BF40307729E2792C9D05959E155'O &
                                          'F4F8C20500B45A0510855185BC05720279BB670FDA68A08D06AFAA670B0B0024'O &
                                          '8D4120E71993B7C08E843169934CAEB5400449A5B6056C4F4DBD94258C0970AE'O &
                                          'C77603ADD588E6E6309E87F4F6BE52B5D265FC158B059AB1738DD6F232AED502'O &
                                          '55FE8F22E742A065151681D92771FCC6EE7AFD3FC13C6330C650ED7470CE553A'O &
                                          'B028CEB90A70A7A1AA622D1BBD6DB0E761AD255455E081850503F0E5D0D0BBE9'O &
                                          '44E2FB9164B2CFEBEF0791ED972B029D0ED1D21277C370B5A6FA694AE4070BE0'O &
                                          '8B5C0F55BF9D8BA2736F66329864726D41B7DFDD8D36F67F43E65A2DE6E6E769'O &
                                          'A85E4B89DC00561300979F3D731FF6F64E359D1B4957AB85F4D090914C060982'O &
                                          '35A7526BEE8E37640658B97B5767A2E8660C17AD317F3C07039CEAEB5B8EE1E6'O &
                                          'D3381E954A65381D04D881018CEF6FEACEDC1CF3A51293EDF6CDC8B9F32991DF'O &
                                          '80C7C572397EF13D4E44CEBDE39CFBA42F91F8606F32D993DABF5FFC7C1EBA87'O &
                                          'D9E9D09E9921BC7F5F275BADFA9338BEA6F0554AE457A0522C975B9BFD2002A4'O &
                                          'DBAA4713C69C5018F58CC9F58A040055D5B0AD5A011E285C5DEFE923E0E9A63F'O &
                                          'C806B801FC5035AB90F78CC90BF4AC3F32ADC8B9450B0BBEC80AB00A348BE5F2'O &
                                          'DF6ED69FFB67AD2D0BCCC17F0000000049454E44AE426082FFFFFFFF'O;


const charstring c_EPTF_ExecCtrl_StatReset := "Reset";


const charstring c_EPTF_ExecCtrl_RunTest := "EPTF_ExecCtrl_RunTest";


const charstring c_EPTF_ExecCtrl_CPS := "CPS";






//===========================================================================
// Templates
//===========================================================================


}
