<!--Copyright (c) 2000-2023 Ericsson Telecom AB

All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v2.0
which accompanies this distribution, and is available at
http:www.eclipse.org/legal/epl-v10.html                           -->
<!DOCTYPE TITAN_GUI_project_file>
<Project TITAN_version="1.8.pre0 build 2" >
    <General>
        <Project_Name>UI_Handler_Test</Project_Name>
        <Executable_Path>../../../../../../../home/gui:getenv(USER)/Tests/UIHandler/UIHandler_Test/UIHandler_Test</Executable_Path>
        <Working_Dir>../../../../../../../home/gui:getenv(USER)/Tests/UIHandler/UIHandler_Test</Working_Dir>
        <Build_Host>alpha</Build_Host>
        <Execution_Mode>Parallel</Execution_Mode>
        <ScriptFile_AfterMake>Makefile_script</ScriptFile_AfterMake>
        <Log_Format>yes</Log_Format>
        <GNU_Make>yes</GNU_Make>
        <Update_Symlinks>yes</Update_Symlinks>
        <Create_Absolute_Symlinks>no</Create_Absolute_Symlinks>
        <Update_Makefile>yes</Update_Makefile>
        <Log_Dir>../../../../../../../home/gui:getenv(USER)/TTCN3/bin/logs</Log_Dir>
        <Log_Dir>../../../../../../../home/gui:getenv(USER)/TTCN3/EPTFr2/WorkDir/logs</Log_Dir>
        <Localhost_Execute>yes</Localhost_Execute>
        <Execute_Command>rsh %host &quot;cd %project_working_dir ; &quot;%executable&quot; %localhost %mctr_port&quot;</Execute_Command>
        <Execute_Hosts>alfa, beta, gamma</Execute_Hosts>
        <UnUsed_List></UnUsed_List>
    </General>
    <Modules>
        <Module>../../src/HostAdmin/EPTF_CLL_HostAdminUI_Definitions.ttcn</Module>
        <Module>../../src/HostAdmin/EPTF_CLL_HostAdminUI_Functions.ttcn</Module>
        <Module>../../src/LoadRegulator/EPTF_CLL_LoadRegulatorUI_Definitions.ttcn</Module>
        <Module>../../src/LoadRegulator/EPTF_CLL_LoadRegulatorUI_Functions.ttcn</Module>
        <Module>../../src/StatCapture/EPTF_CLL_StatCaptureUI_Definitions.ttcn</Module>
        <Module>../../src/StatCapture/EPTF_CLL_StatCaptureUI_Functions.ttcn</Module>
        <Module>../../../../TestPorts/LOADMEASasp_CNL113585/src/LOADMEASasp_PortType.ttcn</Module>
        <Module>../../../../TestPorts/LOADMEASasp_CNL113585/src/LOADMEASasp_Types.ttcn</Module>
    </Modules>
    <TestPorts>
        <TestPort>../../../../TestPorts/LOADMEASasp_CNL113585/src/LOADMEASasp_PT.cc</TestPort>
        <TestPort>../../../../TestPorts/LOADMEASasp_CNL113585/src/LOADMEASasp_PT.hh</TestPort>
    </TestPorts>
    <Configs>
        <Config>cfg.cfg</Config>
    </Configs>
    <Test_Cases>
        <Test_Case>UI_Handler_Test.control</Test_Case>
        <Test_Case>UI_Handler_Test.tc_InitGUI</Test_Case>
        <Test_Case>UI_Handler_Test.tc_Messages</Test_Case>
        <Test_Case>UI_Handler_Test.tc_load</Test_Case>
        <Test_Case>UI_Handler_Test.tc_VarDoesNotExist</Test_Case>
        <Test_Case>UI_Handler_Test.tc_WidgetDoesNotExist</Test_Case>
        <Test_Case>UI_Handler_Test.tc_snapshot</Test_Case>
        <Test_Case>UI_Handler_Test.tc_Btn</Test_Case>
        <Test_Case>UI_Handler_Test.tc_Check</Test_Case>
        <Test_Case>UI_Handler_Test.tc_remove</Test_Case>
        <Test_Case>UI_Handler_Test.tc_multi</Test_Case>
        <Test_Case>UI_Handler_Test.tc_dynTree</Test_Case>
        <Test_Case>UI_Handler_Test.tc_errResp</Test_Case>
        <Test_Case>UI_Handler_Test.tc_subsTypes</Test_Case>
        <Test_Case>UI_Handler_Test.tc_CLI</Test_Case>
    </Test_Cases>
    <File_Group name="MainFileGroup" >
        <File_Groups>
            <File_Group path="../../src/Variable/EPTF_CLL_Variable.grp" />
            <File_Group path="../../src/Base/EPTF_CLL_Base.grp" />
            <File_Group path="../../src/Common/EPTF_CLL_Common.grp" />
            <File_Group path="../../src/FreeBusyQueue/EPTF_CLL_FreeBusyQueue.grp" />
            <File_Group path="../../src/HashMap/EPTF_CLL_HashMap.grp" />
            <File_Group path="../../src/Logging/EPTF_CLL_Logging.grp" />
            <File_Group path="../../src/RedBlackTree/EPTF_CLL_RBtree.grp" />
            <File_Group name="XTDP" >
                <File path="../../../../TestPorts/XTDPasp_CNL113494/src/xtdp.l" />
                <File path="../../../../TestPorts/Common_Components/Abstract_Socket_CNL113384/src/Abstract_Socket.cc" />
                <File path="../../../../TestPorts/Common_Components/Abstract_Socket_CNL113384/src/Abstract_Socket.hh" />
            </File_Group>
            <File_Group name="TELNETAsp" >
                <File path="../../../../TestPorts/TELNETasp_CNL113320/src/TELNETasp_PortType.ttcn" />
                <File path="../../../../TestPorts/TELNETasp_CNL113320/src/TELNETasp_PT.cc" />
                <File path="../../../../TestPorts/TELNETasp_CNL113320/src/TELNETasp_PT.hh" />
            </File_Group>
            <File_Group name="Useful" >
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCFileIO.cc" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCFileIO_Functions.ttcn" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCDateTime_Functions.ttcn" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCDateTime.cc" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCMaths.cc" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCMaths_Functions.ttcn" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCMaths_GenericTypes.ttcn" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCEncoding.cc" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCEncoding_Functions.ttcn" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCInterface.cc" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCInterface_Functions.ttcn" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCConversion_Functions.ttcn" />
		<File path="../../../TCCUsefulFunctions_CNL113472/src/TCCConversion.cc" />
            </File_Group>
            <File_Group name="IPL4" >
                <File path="../../../../TestPorts/IPL4asp_CNL113531/src/IPL4asp_PT.cc" />
                <File path="../../../../TestPorts/IPL4asp_CNL113531/src/IPL4asp_PT.hh" />
                <File path="../../../../TestPorts/IPL4asp_CNL113531/src/IPL4asp_PortType.ttcn" />
                <File path="../../../../TestPorts/IPL4asp_CNL113531/src/IPL4asp_Types.ttcn" />
                <File path="../../../../TestPorts/IPL4asp_CNL113531/src/IPL4asp_Functions.ttcn" />
                <File path="../../../../TestPorts/IPL4asp_CNL113531/src/IPL4asp_discovery.cc" />	
                <File path="../../../../TestPorts/IPL4asp_CNL113531/src/IPL4asp_protocol_L234.hh" />				
            </File_Group>
            <File_Group name="UI_Handler_Test" >
                <File path="UI_Handler_Test.ttcn" />
            </File_Group>
            <File_Group path="../../src/UIHandler/EPTF_CLL_UIHandler.grp" />
            <File_Group name="EPTF_HostAdmin" >
                <File path="../../src/HostAdmin/EPTF_CLL_HostAdmin_BaseDefinitions.ttcn" />
                <File path="../../src/HostAdmin/EPTF_CLL_HostAdmin_BaseFunctions.ttcn" />
                <File path="../../src/HostAdmin/EPTF_CLL_HostAdmin_Definitions.ttcn" />
                <File path="../../src/HostAdmin/EPTF_CLL_HostAdmin_Functions.ttcn" />
                <File path="../../src/HostAdmin/EPTF_CLL_HostAdmin_DSFunctions.ttcn" />
            </File_Group>
            <File_Group name="EPTF_StatCapture" >
                <File path="../../src/StatCapture/EPTF_CLL_StatCapture_Definitions.ttcn" />
                <File path="../../src/StatCapture/EPTF_CLL_StatCapture_ExternalFunctions.cc" />
                <File path="../../src/StatCapture/EPTF_CLL_StatCapture_Functions.ttcn" />
                <File path="../../src/StatCapture/EPTF_CLL_StatCaptureControl_Definitions.ttcn" />
                <File path="../../src/StatCapture/EPTF_CLL_StatCaptureControl_Functions.ttcn" />
            </File_Group>
            <File_Group name="EPTF_StatMeasure" >
                <File path="../../src/StatMeasure/EPTF_CLL_StatMeasure_Definitions.ttcn" />
                <File path="../../src/StatMeasure/EPTF_CLL_StatMeasure_Functions.ttcn" />
            </File_Group>
            <File_Group name="EPTF_LoadRegulator" >
                <File path="../../src/LoadRegulator/EPTF_CLL_LoadRegulator_Definitions.ttcn" />
                <File path="../../src/LoadRegulator/EPTF_CLL_LoadRegulator_Functions.ttcn" />
            </File_Group>
            <File_Group name="EPTF_CentralScheduling" >
                <File path="../../src/CentralScheduling/EPTF_CLL_CSAdmin_Functions.ttcn" />
                <File path="../../src/CentralScheduling/EPTF_CLL_CS_Definitions.ttcn" />
                <File path="../../src/CentralScheduling/EPTF_CLL_CSLB_Functions.ttcn" />
                <File path="../../src/CentralScheduling/EPTF_CLL_CS_DSFunctions.ttcn" />
            </File_Group>
            <File_Group name="EPTF_NameService" >
                <File path="../../src/NameService/EPTF_CLL_NameService_Definitions.ttcn" />
                <File path="../../src/NameService/EPTF_CLL_NameService_Functions.ttcn" />
                <File path="../../src/NameService/EPTF_CLL_NameServiceClient_Functions.ttcn" />
                <File path="../../src/NameService/EPTF_CLL_NameService_ExternalFunctions.cc" />
            </File_Group>
            <File_Group name="EPTF_CentralScheduling_UIHandler" >
                <File path="../../src/CentralScheduling/EPTF_CLL_CSUIHandler_Definitions.ttcn" />
                <File path="../../src/CentralScheduling/EPTF_CLL_CSUIHandler_Functions.ttcn" />
            </File_Group>
            <File_Group name="ExecCtrlUIHandler" >
                <File path="../../src/ExecCtrl/EPTF_CLL_ExecCtrlUIHandler_Definitions.ttcn" />
                <File path="../../src/ExecCtrl/EPTF_CLL_ExecCtrlUIHandler_Functions.ttcn" />
            </File_Group>
            <File_Group path="../../src/Transport/EPTF_CLL_TransportCommPortIPL4.grp" />
            <File_Group path="../../src/Transport/EPTF_CLL_TransportMessageBufferManager.grp" />
            <File_Group path="../../src/Transport/EPTF_CLL_TransportRouting.grp" />
            <File_Group path="../../src/Scheduler/EPTF_CLL_Scheduler.grp" />
            <File_Group path="../../src/Logging/EPTF_CLL_LoggingUI.grp" />
            <File_Group path="../../src/LGenBase/EPTF_CLL_LGenBase_All.grp" />
            <File_Group path="../../src/ExecCtrl/EPTF_CLL_ExecCtrl.grp" />
            <File_Group path="../../src/StatHandler/EPTF_CLL_StatHandler.grp" />
            <File_Group path="../../src/RandomNArray/EPTF_CLL_RNA.grp" />
            <File_Group path="../../src/Semaphore/EPTF_CLL_Semaphore.grp" />
            <File_Group path="../../src/StatHandler/EPTF_CLL_StatHandlerUI.grp" />
	    <File_Group name="HTTPmsg_CNL113312" >
        	<File path="../../../../TestPorts/HTTPmsg_CNL113312/src/HTTPmsg_MessageLen.ttcn"/>
        	<File path="../../../../TestPorts/HTTPmsg_CNL113312/src/HTTPmsg_MessageLen_Function.cc"/>
        	<File path="../../../../TestPorts/HTTPmsg_CNL113312/src/HTTPmsg_PT.cc"/>
        	<File path="../../../../TestPorts/HTTPmsg_CNL113312/src/HTTPmsg_PT.hh"/>
        	<File path="../../../../TestPorts/HTTPmsg_CNL113312/src/HTTPmsg_PortType.ttcn"/>
        	<File path="../../../../TestPorts/HTTPmsg_CNL113312/src/HTTPmsg_Types.ttcn"/>
	    </File_Group>
	    
        </File_Groups>
    </File_Group>
</Project>
