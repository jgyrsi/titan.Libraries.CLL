///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_LGenBaseDemo_CallerDefinitions
//
//  Purpose:
//    This module contains data structures of the Caller component.
//
//  Module Parameters:
//    -
//
//  Module depends on:
//
//  Current Owner:
//    ELSZSKU
//
//  Last Review Date:
//    20 - -
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////
module EPTF_LGenBaseDemo_CallerDefinitions
{
import from EPTF_LGenBaseDemo_TransportDefinitions all
import from EPTF_CLL_LGenBase_Definitions all

//Count of the users to be simulated
modulepar integer tsp_LGenBaseDemo_userCount := 100
//The required CPS
modulepar float tsp_LGenBaseDemo_targetCPS := 40.0

//Component type of the Caller
type component LGenBaseDemo_Caller_CT extends LGenBaseDemo_Transport_CT, EPTF_LGenBase_CT{
  private var integer v_LGenBaseDemo_Caller_myBehavId := -1
  private var integer v_LGenBaseDemo_Caller_msgRegisterTemplTypeIdx := -1
  private var integer v_LGenBaseDemo_Caller_msgReRegisterTemplTypeIdx := -1
  private var integer v_LGenBaseDemo_Caller_msgDoMsgTemplTypeIdx := -1
  private var integer v_LGenBaseDemo_Caller_msgReRegisterTemplIdx := -1
  private var integer v_LGenBaseDemo_Caller_msgUserDataMarkLen := 0
  private var integer v_LGenBaseDemo_Caller_msgUserDataSeparatorLen := 0
  private var integer v_LGenBaseDemo_Caller_registerTcIdx := -1
}

const float c_LGenBaseDemo_Caller_Watchdog := 0.5

const charstring c_LGenBaseDemo_Caller_BehaviorName := "Caller"

const charstring c_LGenBaseDemo_Caller_fsmNameRegister := "Caller_fsmRegister"
const charstring c_LGenBaseDemo_Caller_fsmNameReRegister := "Caller_fsmReRegister"
const charstring c_LGenBaseDemo_Caller_fsmNameDoMsg := "Caller_fsmDoMsg"

const charstring c_LGenBaseDemo_Caller_entityTypeName := "Caller_EntityType"
const charstring c_LGenBaseDemo_Caller_entityGroupName := "Caller_EntityGroup"
const charstring c_LGenBaseDemo_Caller_scenarioName := "Caller_Scenario"
const charstring c_LGenBaseDemo_Caller_tcRegisterName := "Caller_TcRegister"
const charstring c_LGenBaseDemo_Caller_tcReRegisterName := "Caller_TcReRegister"
const charstring c_LGenBaseDemo_Caller_tcDoMsgName := "Caller_TcDoMsg"

const charstring c_LGenBaseDemo_Caller_StepName_sendMessage := "SendMessage"

const charstring c_LGenBaseDemo_Caller_msgRegisterTemplType := "Register"
const charstring c_LGenBaseDemo_Caller_msgReRegisterTemplType := "ReRegister"
const charstring c_LGenBaseDemo_Caller_msgDoMsgTemplType := "DoMsg"

const charstring c_LGenBaseDemo_Caller_msgUserDataMark := "UserData: "
const charstring c_LGenBaseDemo_Caller_msgUserDataSeparator := ":"
const charstring c_LGenBaseDemo_Caller_msgUserDataTerm := ";"

const charstring c_LGenBaseDemo_Caller_msgRegisterExtTemplateName := "LGenBaseDemo_msgRegisterTemplate"
const charstring c_LGenBaseDemo_Caller_msgReRegisterExtTemplateName := "LGenBaseDemo_msgReRegisterTemplate"
const charstring c_LGenBaseDemo_Caller_msgDoMsgExtTemplateName := "LGenBaseDemo_msgDoMsgTemplate"

const charstring c_LGenBaseDemo_Caller_InputNameRespSuccess := "ResponseSuccess"
const charstring c_LGenBaseDemo_Caller_InputNameRespFailed := "ResponseFailed"

const integer c_LGenBaseDemo_Caller_InputIdxRespSuccess := 0
const integer c_LGenBaseDemo_Caller_InputIdxRespFailed := 1

const integer c_LGenBaseDemo_Caller_MsgRegister := 0
const integer c_LGenBaseDemo_Caller_MsgReRegister := 1
const integer c_LGenBaseDemo_Caller_MsgDoMsg := 2
type integer LGenBaseDemo_Caller_MsgTypes (c_LGenBaseDemo_Caller_MsgRegister..c_LGenBaseDemo_Caller_MsgDoMsg)

const charstring c_LGenBaseDemo_Caller_registerFinishedName := "Register Finished Fn"
const charstring c_LGenBaseDemo_Caller_entityFinishedName := "Entity Finished Fn"

}  // end of module
