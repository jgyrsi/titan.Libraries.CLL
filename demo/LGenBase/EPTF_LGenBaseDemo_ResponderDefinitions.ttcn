///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_LGenBaseDemo_ResponderDefinitions
//
//  Purpose:
//    This module contains the data types of the Responder
//
//  Module Parameters:
//    -
//
//  Module depends on:
//
//  Current Owner:
//    ELSZSKU
//
//  Last Review Date:
//    20 - -
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////
module EPTF_LGenBaseDemo_ResponderDefinitions
{
import from EPTF_CLL_Common_Definitions all
import from EPTF_LGenBaseDemo_TransportDefinitions all
import from EPTF_CLL_LGenBase_ConfigDefinitions all
import from EPTF_CLL_LGenBase_Definitions all

//The component type
type component LGenBaseDemo_Responder_CT extends LGenBaseDemo_Transport_CT, EPTF_LGenBase_CT{
  private var float v_LGenBaseDemo_Responder_maxDelay := 0.5
  private var float v_LGenBaseDemo_Responder_registerSuccessRate := 0.3
  private var float v_LGenBaseDemo_Responder_reRegisterSuccessRate := 0.8
  private var float v_LGenBaseDemo_Responder_doMsgSuccessRate := 0.96
  private var integer v_dummyInt
  private var integer v_LGenBaseDemo_Responder_myBehavId := -1
  private var EPTF_BooleanList v_LGenBaseDemo_Responder_userStates := {}
  private var integer v_LGenBaseDemo_Responder_responderTcIdx := -1
  private var integer v_LGenBaseDemo_Responder_msgHashMap := -1
  private var integer v_LGenBaseDemo_Responder_msgCounter := 0
  
  private var integer v_LGenBaseDemo_Responder_msgTemplateIdx := -1
  private var integer v_LGenBaseDemo_Responder_msgTemplateSetId := -1
}

const charstring c_LGenBaseDemo_Responder_fsmName := "ResponderFSM"
const charstring c_LGenBaseDemo_Responder_tcName := "TC_Responder"
const charstring c_LGenBaseDemo_Responder_entityTypeName := "Responder_EntityType"
const charstring c_LGenBaseDemo_Responder_entityGroupName := "Responder_EntityGroup"
const charstring c_LGenBaseDemo_Responder_scenarioName := "Responder_Scenario"
const charstring c_LGenBaseDemo_Responder_BehaviorName := "Responder_Behavior"

const charstring c_LGenBaseDemo_Responder_msgHashMapName := "MsgIdHashMap"
const charstring c_LGenBaseDemo_Responder_msgTemplateName := "MsgTemplate"

const charstring c_LGenBaseDemo_Responder_StepName_reply2Register := "LGenBaseDemo_Responder_reply2Register"
const charstring c_LGenBaseDemo_Responder_StepName_reply2ReRegister := "LGenBaseDemo_Responder_reply2ReRegister"
const charstring c_LGenBaseDemo_Responder_StepName_reply2DoMsg := "LGenBaseDemo_Responder_reply2DoMsg"
const charstring c_LGenBaseDemo_Responder_StepName_inputInInvalidState := "LGenBaseDemo_Responder_inputInInvalidState"
const charstring c_LGenBaseDemo_Responder_StepName_watchdogTimerTimedOut := "LGenBaseDemo_Responder_watchdogTimerTimedOut"
const charstring c_LGenBaseDemo_Responder_StepName_delay := "LGenBaseDemo_Responder_delay"
const charstring c_LGenBaseDemo_Responder_StepName_storeMsg := "LGenBaseDemo_Responder_StepName_storeMsg"
const charstring c_LGenBaseDemo_Responder_StepName_ackReceived := "LGenBaseDemo_Responder_StepName_ackReceived"
const charstring c_LGenBaseDemo_Responder_NextStateCalcName := "LGenBaseDemo_Responder_NextStateCalcFn"

const charstring c_LGenBaseDemo_Responder_InputRegisterReceived := "RegisterReceived"
const charstring c_LGenBaseDemo_Responder_InputReRegisterReceived := "ReRegisterReceived"
const charstring c_LGenBaseDemo_Responder_InputDoMsgReceived := "DoMsgReceived"
const charstring c_LGenBaseDemo_Responder_InputAckReceived := "AckReceived"

const integer c_LGenBaseDemo_Responder_InputIdxRegisterReceived := 0
const integer c_LGenBaseDemo_Responder_InputIdxReRegisterReceived := 1
const integer c_LGenBaseDemo_Responder_InputIdxDoMsgReceived := 2
const integer c_LGenBaseDemo_Responder_InputIdxAckReceived := 3

const EPTF_LGenBase_FsmLocalStateList c_LGenBaseDemo_Responder_fsmStateList := {"idle", "inRegister", "inReRegister", "inDoMsg", "waiting4Ack"}

const integer c_LGenBaseDemo_Responder_fsmStateIdxIdle := 0
const integer c_LGenBaseDemo_Responder_fsmStateIdxInRegister := 1
const integer c_LGenBaseDemo_Responder_fsmStateIdxInReRegister := 2
const integer c_LGenBaseDemo_Responder_fsmStateIdxInDoMsg := 3
const integer c_LGenBaseDemo_Responder_fsmStateIdxWaiting4Ack := 4

const EPTF_IntegerList c_LGenBaseDemo_Responder_fsmStateList4Events := {
  c_LGenBaseDemo_Responder_fsmStateIdxInRegister,
  c_LGenBaseDemo_Responder_fsmStateIdxInReRegister,
  c_LGenBaseDemo_Responder_fsmStateIdxInDoMsg,
  c_LGenBaseDemo_Responder_fsmStateIdxWaiting4Ack
}

//Structure of the behavior context data
const integer c_LGenBaseDemo_Responder_behavCtxDataIdxInputId := 0
const integer c_LGenBaseDemo_Responder_behavCtxDataIdxNextStateIdx := 1

const charstring c_LGenBaseDemo_Responder_varNameOfLastMsg := "lastMsg"

const integer c_LGenBaseDemo_ResponseType_register := 0
const integer c_LGenBaseDemo_ResponseType_reRegister := 1
const integer c_LGenBaseDemo_ResponseType_doMsg := 2

type integer LGenBaseDemo_ResponseType (c_LGenBaseDemo_ResponseType_register..c_LGenBaseDemo_ResponseType_doMsg)

}  // end of module
