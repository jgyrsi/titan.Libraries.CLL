///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: LoggingUI_Demo
// 
//  Purpose:
//    Demo module for LoggingUI feature.
// 
///////////////////////////////////////////////////////////

module LoggingUI_Demo
{

//=========================================================================
// Import Part
//=========================================================================

// if you use EPTF_Generic_Logger_CT you have to import these
import from EPTF_CLL_Logging_Functions all;

import from EPTF_CLL_LoggingUI_Definitions all;
import from EPTF_CLL_LoggingUI_Functions all;
import from EPTF_CLL_LoggingUIClient_Functions all;

import from EPTF_CLL_UIHandler_Definitions all;
import from EPTF_CLL_UIHandler_WidgetFunctions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_Base_Definitions all;

//=========================================================================
//Component Types
//=========================================================================


type component Server_CT extends EPTF_UIHandler_CT, EPTF_LoggingUI_CT{
}

type component MAIN_CT extends EPTF_Base_CT {
}

type component SYSTEM_CT extends EPTF_Base_CT {
}


function f_EPTF_LoggingUIClient1_Behaviour(
  in charstring pl_selfName, 
  in EPTF_LoggingUI_CT pl_loggingUI_main,
  in EPTF_UIHandler_CT pl_subscriber) runs on EPTF_LoggingUIClient_CT{

  f_EPTF_LoggingUIClient_init_CT(pl_selfName, pl_loggingUI_main, pl_subscriber);
  f_EPTF_Logging_debug(true,"----- LoggingUIClient START -------");

  var integer v_logMaskIdx;
  v_logMaskIdx := f_EPTF_Logging_registerComponentMasks("EPTF_SomeClient", {"Error", "Warning", "Debug"});

  f_EPTF_Logging_log(true,v_logMaskIdx,{2},"----- ComponentMask registered -------");

  timer t_waitProv1 := 0.1;

  t_waitProv1.start;
  alt {
    [] t_waitProv1.timeout{

        f_EPTF_Logging_error(true, "--EPTF_Logging Mask:0 (Error)--");
        f_EPTF_Logging_warning(true,"--EPTF_Logging Mask:1 (Warning)--");
        f_EPTF_Logging_debug(true,"--EPTF_Logging Mask:2 (Debug)--");
        f_EPTF_Logging_debugM(true,"--EPTF_Logging Mask:3 (DebugM)--");
        f_EPTF_Logging_debugV(true,"--EPTF_Logging Mask:4 (DebugV)--");
        f_EPTF_Logging_log(true,v_logMaskIdx,{0},"--EPTF_SomeClient Mask:0 (Error)--");
        f_EPTF_Logging_log(true,v_logMaskIdx,{1},"--EPTF_SomeClient Mask:1 (Warning)--");
        f_EPTF_Logging_log(true,v_logMaskIdx,{2},"--EPTF_SomeClient Mask:2 (Debug)--");       

        t_waitProv1.start;repeat;
    }
  };
  
  f_EPTF_Logging_debug(true,"----- LoggingUIClient shutdown -------");
  f_EPTF_Base_cleanup_CT(); 
}

function f_EPTF_LoggingUIClient2_Behaviour(
  in charstring pl_selfName, 
  in EPTF_LoggingUI_CT pl_loggingUI_main,
  in EPTF_UIHandler_CT pl_subscriber) runs on EPTF_LoggingUIClient_CT{

  f_EPTF_LoggingUIClient_init_CT(pl_selfName, pl_loggingUI_main, pl_subscriber);
  f_EPTF_Logging_debug(true,"----- LoggingUIClient START -------");

  var integer v_logMaskIdx;
  v_logMaskIdx := f_EPTF_Logging_registerComponentMasks("EPTF_SomeClient2", {"Error", "Debug"});
  f_EPTF_Logging_log(true,v_logMaskIdx,{1},"----- ComponentMask registered -------");


  timer t_waitProv1 := 0.1;

  t_waitProv1.start;
  alt {
    [] t_waitProv1.timeout{

        f_EPTF_Logging_error(true, "--EPTF_Logging Mask:0 (Error)--");
        f_EPTF_Logging_warning(true,"--EPTF_Logging Mask:1 (Warning)--");
        f_EPTF_Logging_debug(true,"--EPTF_Logging Mask:2 (Debug)--");
        f_EPTF_Logging_debugM(true,"--EPTF_Logging Mask:3 (DebugM)--");
        f_EPTF_Logging_debugV(true,"--EPTF_Logging Mask:4 (DebugV)--");
        f_EPTF_Logging_log(true,v_logMaskIdx,{0},"--EPTF_SomeClient2 Mask:0 (Error)--");
        f_EPTF_Logging_log(true,v_logMaskIdx,{1},"--EPTF_SomeClient2 Mask:1 (Debug)--");       

        t_waitProv1.start;repeat;
    }
  };
  
  /*  timer t_waitProv1 := 3.0;
  t_waitProv1.start;
  alt {
    [] t_waitProv1.timeout{
        if(v_EPTF_Logging_ComponentMask_List[0].componentLocalMask[0])
        {
          //disabling mask 0.0 element (error,eptf core)
          f_EPTF_Logging_disableLocalMask(0,0);
          f_EPTF_LoggingUI_disableAllGlobal();
        }
        else 
        {
          //enabling mask 0.0 element (error,eptf core)
          f_EPTF_Logging_enableLocalMask(0,0);
          f_EPTF_LoggingUI_enableAllGlobal();
        }       

        t_waitProv1.start;repeat;
    }
  };*/

  f_EPTF_Logging_debug(true,"----- LoggingUIClient shutdown -------");
  f_EPTF_Base_cleanup_CT(); 
}

function f_EPTF_LoggingUI_Behaviour(
  in charstring pl_selfName, 
  in EPTF_UIHandler_CT pl_subscriber,
  in charstring pl_tabboxName) runs on EPTF_LoggingUI_CT{

  log("----- LoggingUI START -------"); 
  f_EPTF_LoggingUI_init_CT(pl_selfName, pl_subscriber,pl_tabboxName)
  
  timer t_waitProv := 2.0;

  t_waitProv.start;
  alt {
    [] t_waitProv.timeout{
      t_waitProv.start;repeat;
    }
  }
  f_EPTF_Base_cleanup_CT();
  log("----- LoggingUI shutdown -------");  
}

function f_EPTF_UIHandler_Behaviour(charstring pl_selfName) runs on EPTF_UIHandler_CT{
  log("----- Subscriber START -------");
  f_EPTF_UIHandler_init_CT(pl_selfName);
  f_EPTF_UIHandler_clearGUI();

  f_EPTF_UIHandler_addWindow();

  //Creates Layout:
  var boolean isExists;

/*  alt {
    [] EPTF_guiPortIf.receive(tr_LoggingUI_ASP_XTDP_exitTTCNButtonPressed){
      log("----- Exit button had been pressed ----");
    };
  };*/
  f_EPTF_UIHandler_exitButtonPressed(60.0);
  f_EPTF_Base_stopRemote(mtc);
  log("----- Subscriber shutdown -------");
}

function f_Server_behaviour(charstring pl_selfName, 
  in EPTF_UIHandler_CT pl_subscriber,
  in charstring pl_tabboxName) runs on Server_CT {
  log("----- Subscriber START -------");
  f_EPTF_UIHandler_init_CT(pl_selfName);
  f_EPTF_UIHandler_clearGUI();
  f_EPTF_UIHandler_addWindow();

  //Creates Layout:
  var boolean isExists;
  log("----- LoggingUI START -------"); 
  f_EPTF_LoggingUI_init_CT(pl_selfName, pl_subscriber,pl_tabboxName)


  //as_EPTF_UIHandler_exitButtonPressed();
  f_EPTF_UIHandler_exitButtonPressed(60.0);
  f_EPTF_Base_stopRemote(mtc);
  log("----- Server shutdown -------");  
}

//=========================================================================
// Testcases
//=========================================================================

testcase tc_RunLoggingUI() runs on MAIN_CT system SYSTEM_CT{

  f_EPTF_Base_init_CT("MainCT");
  //Handler (subscriber)
//  var EPTF_UIHandler_CT ct_subscriber := EPTF_UIHandler_CT.create;
  var Server_CT ct_subscriber := Server_CT.create;

  //Starting UIHandler
  ct_subscriber.start(f_Server_behaviour("Main", ct_subscriber,"EPTF_Main_tabbox"));
  var EPTF_LoggingUI_CT ct_loggingUI_handler  := ct_subscriber;

//  ct_subscriber.start(f_EPTF_UIHandler_Behaviour("Subscriber"));
//  //Starting main LoggingUI
//  var EPTF_LoggingUI_CT ct_loggingUI_handler := EPTF_LoggingUI_CT.create;
//  ct_loggingUI_handler.start(f_EPTF_LoggingUI_Behaviour("Main", ct_subscriber,"EPTF_Main_tabbox"));



  //LoggingUI Client with HandlerClient & Starting Client
  var EPTF_LoggingUIClient_CT ct_client1 := EPTF_LoggingUIClient_CT.create;
  ct_client1.start(f_EPTF_LoggingUIClient1_Behaviour("Client1",ct_loggingUI_handler, ct_subscriber));

 //LoggingUI Client with HandlerClient & Starting Client
  var EPTF_LoggingUIClient_CT ct_client2 := EPTF_LoggingUIClient_CT.create;
  ct_client2.start(f_EPTF_LoggingUIClient2_Behaviour("Client2",ct_loggingUI_handler, ct_subscriber)); 

 //LoggingUI Client with HandlerClient & Starting Client
  var EPTF_LoggingUIClient_CT ct_client3 := EPTF_LoggingUIClient_CT.create;
  ct_client3.start(f_EPTF_LoggingUIClient2_Behaviour("Client3",ct_loggingUI_handler, ct_subscriber)); 

  for (var integer i:=0; i<0; i:=i+1) {
    //LoggingUI Client with HandlerClient & Starting Client
    var EPTF_LoggingUIClient_CT ct_client4 := EPTF_LoggingUIClient_CT.create;
    ct_client4.start(f_EPTF_LoggingUIClient2_Behaviour("Client"&int2str(4+i),ct_loggingUI_handler, ct_subscriber)); 
  }

  timer T_main := 60.0;
  T_main.start;
  alt {
    [] ct_subscriber.done {}
    [] T_main.timeout{}
  }
  all component.done;
  f_EPTF_Base_cleanup_CT();
}

//=========================================================================
// Control
//=========================================================================

control
{
  execute(tc_RunLoggingUI());
}

}  // end of module
