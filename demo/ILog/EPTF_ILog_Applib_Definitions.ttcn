///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_ILog_Applib_Definitions
//
// Purpose:
//   This module contains type definitions for demo of EPTF ILog
//   Applib layer
//
// Module Parameters:
//    -
//
//  Module depends on:
//    <EPTF_CLL_ILogBase_Functions>
// 
// Current Owner:
//    Jozsef Gyurusi (ethjgi)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////

module EPTF_ILog_Applib_Definitions {

import from EPTF_ILog_Transport_Definitions all;
import from EPTF_CLL_ILog_Definitions all;
import from EPTF_CLL_LGenBase_Definitions all;


type record of record of EPTF_LGenBase_TestStepArgs EPTF_ILog_Applib_Transactions; // indexed by connId,transactionId

///////////////////////////////////////////////////////////
//  Type: EPTF_ILog_Applib_CT
// 
//  Purpose:
//     Defines a demonstrative AppLib layer for ILog Logging
// 
//  Elements:    
//    v_ILog_Applib_bidx                 - *integer* - behaviour index of the Applib
//    v_EPTF_ILog_Applib_eventIdx_OK     - *integer* - id of the OK event
//    v_EPTF_ILog_Applib_eventIdx_NOK    - *integer* - id of the NOK event    (=Not OK)
//    v_EPTF_ILog_Applib_eventIdx_ERR    - *integer* - id of the ERR event    (=Erroneous msg arrived)
//    v_EPTF_ILog_Applib_eventIdx_TERR   - *integer* - id of the CERR event   (=Connection ID is erroneous in msg)
//    v_EPTF_ILog_Applib_eventIdx_CERR   - *integer* - id of the TERR event   (=Transaction ID is erroneous in msg)
//    v_ILog_Applib_Transactions         - <EPTF_ILog_Applib_Transactions> - transaction database
//    v_EPTF_ILog_Applib_logItem_List        - <EPTF_ILog_Applib_LogItem_List> - database of logged item
//    v_EPTF_ILog_Applib_logItem_List_dbId   - *integer* - id of the emergency log
//    
//  Detailed Comments:
//     
///////////////////////////////////////////////////////////
type component EPTF_ILog_Applib_CT extends EPTF_ILog_Transport_CT, EPTF_LGenBase_CT {
  var integer v_ILog_Applib_bidx;
  
  var integer v_EPTF_ILog_Applib_eventIdx_OK;
  var integer v_EPTF_ILog_Applib_eventIdx_NOK;
  var integer v_EPTF_ILog_Applib_eventIdx_ERR;
  var integer v_EPTF_ILog_Applib_eventIdx_TERR;
  var integer v_EPTF_ILog_Applib_eventIdx_CERR;
  
  var EPTF_ILog_Applib_Transactions v_ILog_Applib_Transactions;

  // ILog DB:
  private var EPTF_ILog_Applib_LogItem_List v_EPTF_ILog_Applib_logItem_List;
  private var integer v_EPTF_ILog_Applib_logItem_List_dbId;
}

const integer c_EPTF_ILog_appDataIdForConnectionId := 0;


// behav:

///////////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_ILog_Applib_behavior
//
// Purpose:
//   BehaviourName of the Applib
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_ILog_Applib_behavior := "ILog_Applib";

// steps
///////////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_ILog_Applib_stepName_connect
//
// Purpose:
//   stepName to initiate a new connection
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_ILog_Applib_stepName_connect := "connect";

///////////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_ILog_Applib_stepName_sendBegin
//
// Purpose:
//   stepName to send a Begin message
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_ILog_Applib_stepName_sendBegin := "sendBegin";

///////////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_ILog_Applib_stepName_sendInvoke
//
// Purpose:
//   stepName to send an Invoke message
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_ILog_Applib_stepName_sendInvoke := "sendInvoke";

///////////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_ILog_Applib_stepName_sendEnd
//
// Purpose:
//   stepName to send an End message
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_ILog_Applib_stepName_sendEnd := "sendEnd";

///////////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_ILog_Applib_stepName_timeout
//
// Purpose:
//   stepName to notify the Applib about the timeout
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_ILog_Applib_stepName_timeout := "timeout";

///////////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_ILog_Applib_stepName_timeout
//
// Purpose:
//   stepName to notify the Applib about the timeout of a transaction
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_ILog_Applib_stepName_disconnect := "disconnect";

// events:
///////////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_ILog_Applib_eventName_OK
//
// Purpose:
//   event to notify the FSM about a received OK msg
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_ILog_Applib_eventName_OK := "OK";

///////////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_ILog_Applib_eventName_NOK
//
// Purpose:
//   event to notify the FSM about a received NOK msg
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_ILog_Applib_eventName_NOK := "NOK";

///////////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_ILog_Applib_eventName_ERR
//
// Purpose:
//   event to notify the FSM about a received ERR msg
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_ILog_Applib_eventName_ERR := "ERR";

///////////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_ILog_Applib_eventName_TERR
//
// Purpose:
//   event to notify the FSM about a received TERR msg
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_ILog_Applib_eventName_TERR := "TERR";

///////////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_ILog_Applib_eventName_CERR
//
// Purpose:
//   event to notify the FSM about a received CERR msg
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_ILog_Applib_eventName_CERR := "CERR";


type record EPTF_ILog_Applib_Connection {
  EPTF_LGenBase_TestStepArgs pl_ptr
}

type EPTF_ILog_Applib_Connection EPTF_ILog_Applib_SendBegin;
type EPTF_ILog_Applib_Connection EPTF_ILog_Applib_SendInvoke;
type EPTF_ILog_Applib_Connection EPTF_ILog_Applib_SendEnd;
type EPTF_ILog_Applib_Connection EPTF_ILog_Applib_Timedout;

type record EPTF_ILog_Applib_Resp {
  integer connId,
  integer transactionId,
  EPTF_ILog_Transport_ResponseMsg msg
}

type union EPTF_ILog_Applib_LogItemInfo {
  EPTF_ILog_Applib_Connection connection,
  EPTF_ILog_Applib_SendBegin sendBegin,
  EPTF_ILog_Applib_SendInvoke sendInvoke,
  EPTF_ILog_Applib_SendEnd sendEnd,
  EPTF_ILog_Applib_Timedout timedout,
  EPTF_ILog_Applib_Resp resp
}

type record EPTF_ILog_Applib_LogItem {
  charstring msg,
  EPTF_ILog_Applib_LogItemInfo logInfo
}

type record of EPTF_ILog_Applib_LogItem EPTF_ILog_Applib_LogItem_List;

} // module EPTF_ILog_Applib_Definitions
