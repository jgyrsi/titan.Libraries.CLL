///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_ILog_Applib_Functions
//
// Purpose:
//   This module contains type definitions for demo of EPTF ILog
//   Applib layer
//
// Module Parameters:
//    -
//
//  Module depends on:
//    <EPTF_CLL_ILogBase_Functions>
// 
// Current Owner:
//    Jozsef Gyurusi (ethjgi)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
//
//    Public functions:
//      <f_EPTF_ILog_Applib_init_CT>
//
///////////////////////////////////////////////////////////

module EPTF_ILog_Applib_Functions {

import from EPTF_ILog_Transport_Definitions all;
import from EPTF_ILog_Transport_Functions all;
import from EPTF_ILog_Applib_Definitions all;
import from EPTF_CLL_LGenBase_Definitions all;
import from EPTF_CLL_LGenBase_Functions all;
import from EPTF_CLL_LGenBase_EventHandlingFunctions all;
import from EPTF_CLL_LGenBase_ConfigFunctions all;
import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_ILog_Definitions all;
import from EPTF_CLL_ILog_Functions all;
import from EPTF_CLL_LGenBase_ILog_Functions all;

///////////////////////////////////////////////////////////
//  Function: f_EPTF_ILog_Applib_init_CT
// 
//  Purpose:
//    Initializes the <EPTF_ILog_Applib_CT> component
//
//  Parameters:
//    pl_selfName - *in* *charstring* - name of the component
//
//  Return Value:
//   -
//
///////////////////////////////////////////////////////////
public function f_EPTF_ILog_Applib_init_CT(in charstring pl_selfName) runs on EPTF_ILog_Applib_CT {
  f_EPTF_ILog_Transport_init_CT(pl_selfName);
  f_EPTF_LGenBase_init(pl_selfName);

  v_ILog_Applib_Transactions := {};

  //declare transport resp handler:
  f_EPTF_ILog_Transport_addRespHandlerFn(refers(f_EPTF_ILog_Applib_transportRespHandler));

  // declare behaviour:
  v_ILog_Applib_bidx := f_EPTF_LGenBase_declareBehaviorType(c_EPTF_ILog_Applib_behavior, 0, null,null,null);

  // declare steps:
  var integer vl_ret;
  vl_ret := f_EPTF_LGenBase_declareStep(c_EPTF_ILog_Applib_behavior,{c_EPTF_ILog_Applib_stepName_connect,refers(f_EPTF_ILog_Applib_step_connect)});
  vl_ret := f_EPTF_LGenBase_declareStep(c_EPTF_ILog_Applib_behavior,{c_EPTF_ILog_Applib_stepName_sendBegin,refers(f_EPTF_ILog_Applib_step_sendBegin)});
  vl_ret := f_EPTF_LGenBase_declareStep(c_EPTF_ILog_Applib_behavior,{c_EPTF_ILog_Applib_stepName_sendInvoke,refers(f_EPTF_ILog_Applib_step_sendInvoke)});
  vl_ret := f_EPTF_LGenBase_declareStep(c_EPTF_ILog_Applib_behavior,{c_EPTF_ILog_Applib_stepName_sendEnd,refers(f_EPTF_ILog_Applib_step_sendEnd)});
  vl_ret := f_EPTF_LGenBase_declareStep(c_EPTF_ILog_Applib_behavior,{c_EPTF_ILog_Applib_stepName_timeout,refers(f_EPTF_ILog_Applib_step_timeout)});
  vl_ret := f_EPTF_LGenBase_declareStep(c_EPTF_ILog_Applib_behavior,{c_EPTF_ILog_Applib_stepName_disconnect,refers(f_EPTF_ILog_Applib_step_disconnect)});

  // declare events:
  v_EPTF_ILog_Applib_eventIdx_OK := f_EPTF_LGenBase_declareFsmEvent(c_EPTF_ILog_Applib_behavior,c_EPTF_ILog_Applib_eventName_OK);
  v_EPTF_ILog_Applib_eventIdx_NOK := f_EPTF_LGenBase_declareFsmEvent(c_EPTF_ILog_Applib_behavior,c_EPTF_ILog_Applib_eventName_NOK);
  v_EPTF_ILog_Applib_eventIdx_ERR := f_EPTF_LGenBase_declareFsmEvent(c_EPTF_ILog_Applib_behavior,c_EPTF_ILog_Applib_eventName_ERR);
  v_EPTF_ILog_Applib_eventIdx_TERR := f_EPTF_LGenBase_declareFsmEvent(c_EPTF_ILog_Applib_behavior,c_EPTF_ILog_Applib_eventName_TERR);
  v_EPTF_ILog_Applib_eventIdx_CERR := f_EPTF_LGenBase_declareFsmEvent(c_EPTF_ILog_Applib_behavior,c_EPTF_ILog_Applib_eventName_CERR);


  // ILog log DB:
  v_EPTF_ILog_Applib_logItem_List := {};
  v_EPTF_ILog_Applib_logItem_List_dbId := f_EPTF_ILog_register_DB(refers(f_EPTF_ILog_Applib_logItem), {0});
}


private function f_EPTF_ILog_Applib_step_connect(in EPTF_LGenBase_TestStepArgs pl_ptr)
runs on EPTF_ILog_Applib_CT
{
  var EPTF_ILog_chainType vl_chainType := detailed; //???
  var integer vl_detailedChainId := f_EPTF_LGenBase_ILog_getChainId(
    pl_eAbsIdx := pl_ptr.eIdx,
    pl_fsmCtx := pl_ptr.refContext.fCtxIdx,
    pl_type := vl_chainType
  );

  var EPTF_IntegerList vl_chainIds;
  if (vl_detailedChainId==-1) {
    // detailed chain does not exist
    vl_chainIds := {};
  } else {
    vl_chainIds := {vl_detailedChainId};
  }

  var integer vl_connId;
  if (f_EPTF_LGenBase_isvalueAppDataItemOfFsmCtx(
      pl_ptr.eIdx,
      pl_ptr.refContext.fCtxIdx,
      v_ILog_Applib_bidx,
      c_EPTF_ILog_appDataIdForConnectionId
    )) {
    vl_connId := f_EPTF_LGenBase_getAppDataItemOfFsmCtx(
      pl_ptr.eIdx,
      pl_ptr.refContext.fCtxIdx,
      v_ILog_Applib_bidx,
      c_EPTF_ILog_appDataIdForConnectionId
    );

    if (vl_connId!=-1) {
      // connection already exists
      var integer vl_iLogElementId := f_EPTF_LGenBase_ILog_addToChains(pl_ptr, v_EPTF_ILog_Applib_logItem_List_dbId);
      if (vl_iLogElementId!=-1) {
        v_EPTF_ILog_Applib_logItem_List[vl_iLogElementId] := { msg := "Connection already exists", logInfo := {connection:={pl_ptr}}};
      }
      return;
    }

  }

  var EPTF_ILog_Transport_CT vl_remoteCompref := self;
  vl_connId := f_EPTF_ILog_Transport_connect(vl_chainIds, vl_remoteCompref);

  f_EPTF_LGenBase_setAppDataItemOfFsmCtx(
    pl_ptr.eIdx,
    pl_ptr.refContext.fCtxIdx,
    v_ILog_Applib_bidx,
    c_EPTF_ILog_appDataIdForConnectionId,
    vl_connId
  );

  var integer vl_iLogElementId := f_EPTF_LGenBase_ILog_addToChains(pl_ptr, v_EPTF_ILog_Applib_logItem_List_dbId);
  if (vl_iLogElementId!=-1) {
    v_EPTF_ILog_Applib_logItem_List[vl_iLogElementId] := { msg := "Connection established", logInfo := {connection:={pl_ptr}}};
  }
}

private function f_EPTF_ILog_Applib_step_disconnect(in EPTF_LGenBase_TestStepArgs pl_ptr)
runs on EPTF_ILog_Applib_CT
{
  var EPTF_ILog_chainType vl_chainType := detailed; //???
  var integer vl_detailedChainId := f_EPTF_LGenBase_ILog_getChainId(
    pl_eAbsIdx := pl_ptr.eIdx,
    pl_fsmCtx := pl_ptr.refContext.fCtxIdx,
    pl_type := vl_chainType
  );

  var EPTF_IntegerList vl_chainIds;
  if (vl_detailedChainId==-1) {
    // detailed chain does not exist
    vl_chainIds := {};
  } else {
    vl_chainIds := {vl_detailedChainId};
  }

  var integer vl_connId;
  if (f_EPTF_LGenBase_isvalueAppDataItemOfFsmCtx(
      pl_ptr.eIdx,
      pl_ptr.refContext.fCtxIdx,
      v_ILog_Applib_bidx,
      c_EPTF_ILog_appDataIdForConnectionId
    )) {
    vl_connId := f_EPTF_LGenBase_getAppDataItemOfFsmCtx(
      pl_ptr.eIdx,
      pl_ptr.refContext.fCtxIdx,
      v_ILog_Applib_bidx,
      c_EPTF_ILog_appDataIdForConnectionId
    );

    if (vl_connId==-1) {
      // connection already closed
      var integer vl_iLogElementId := f_EPTF_LGenBase_ILog_addToChains(pl_ptr, v_EPTF_ILog_Applib_logItem_List_dbId);
      if (vl_iLogElementId!=-1) {
        v_EPTF_ILog_Applib_logItem_List[vl_iLogElementId] := { msg := "Connection already closed", logInfo := {connection:={pl_ptr}}};
      }
      return;
    }

  }

  f_EPTF_ILog_Transport_disconnect(vl_chainIds, vl_connId);
  vl_connId := -1;
  f_EPTF_LGenBase_setAppDataItemOfFsmCtx(
    pl_ptr.eIdx,
    pl_ptr.refContext.fCtxIdx,
    v_ILog_Applib_bidx,
    c_EPTF_ILog_appDataIdForConnectionId,
    vl_connId
  );

  var integer vl_iLogElementId := f_EPTF_LGenBase_ILog_addToChains(pl_ptr,  v_EPTF_ILog_Applib_logItem_List_dbId);
  if (vl_iLogElementId!=-1) {
    v_EPTF_ILog_Applib_logItem_List[vl_iLogElementId] := { msg := "Connection closed", logInfo := {connection:={pl_ptr}}};
  }
}


private function f_EPTF_ILog_Applib_step_sendBegin(in EPTF_LGenBase_TestStepArgs pl_ptr)
runs on EPTF_ILog_Applib_CT
{
  var EPTF_ILog_chainType vl_chainType := detailed; //???

  var integer vl_detailedChainId := f_EPTF_LGenBase_ILog_getChainId(
    pl_eAbsIdx := pl_ptr.eIdx,
    pl_fsmCtx := pl_ptr.refContext.fCtxIdx,
    pl_type := vl_chainType
  );

  var EPTF_IntegerList vl_chainIds;
  if (vl_detailedChainId==-1) {
    // detailed chain does not exist
    vl_chainIds := {};
  } else {
    vl_chainIds := {vl_detailedChainId};
  }

  var integer vl_connId := f_EPTF_LGenBase_getAppDataItemOfFsmCtx(
    pl_ptr.eIdx,
    pl_ptr.refContext.fCtxIdx,
    v_ILog_Applib_bidx,
    c_EPTF_ILog_appDataIdForConnectionId
  );
  if (vl_connId >=0 ) {
    var integer vl_transactionId := f_EPTF_ILog_Transport_sendReq(vl_chainIds, vl_connId, {Begin:={}});
    if (vl_transactionId!=-1) {
      // send successful:
      var integer vl_iLogElementId := f_EPTF_LGenBase_ILog_addToChains(pl_ptr, v_EPTF_ILog_Applib_logItem_List_dbId);
      if (vl_iLogElementId!=-1) {
        v_EPTF_ILog_Applib_logItem_List[vl_iLogElementId] := { msg := "Message sent", logInfo := {sendBegin:={pl_ptr}}};
      }

      v_ILog_Applib_Transactions[vl_connId][vl_transactionId] := pl_ptr;
    } else {
      // transaction unsuccessful
      var integer vl_iLogElementId := f_EPTF_LGenBase_ILog_addToChains(pl_ptr, v_EPTF_ILog_Applib_logItem_List_dbId);
      if (vl_iLogElementId!=-1) {
        v_EPTF_ILog_Applib_logItem_List[vl_iLogElementId] := { msg := "Sending message failed: invalid connection", logInfo := {sendBegin:={pl_ptr}}};
      }

      // dispatch event
      var EPTF_LGenBase_ReportedEventDescriptor vl_event := c_EPTF_LGenBase_emptyReportedEventDescriptor;
      vl_event.event.bIdx := v_ILog_Applib_bidx;
      vl_event.event.iIdx := v_EPTF_ILog_Applib_eventIdx_TERR;
      vl_event.event.target := {
        eIdx := pl_ptr.eIdx,
        fsmCtxIdx := pl_ptr.refContext.fCtxIdx
      };
      vl_event.reportedArgs := pl_ptr.reportedEvent.reportedArgs;
      f_EPTF_LGenBase_dispatchEvent(vl_event);
    }
  } else {
    // invalid connectionID, cannot send message
    var integer vl_iLogElementId := f_EPTF_LGenBase_ILog_addToChains(pl_ptr, v_EPTF_ILog_Applib_logItem_List_dbId);
    if (vl_iLogElementId!=-1) {
      v_EPTF_ILog_Applib_logItem_List[vl_iLogElementId] := { msg := "Sending message failed: no connection", logInfo := {sendBegin:={pl_ptr}}};
    }

    // dispatch event:
    var EPTF_LGenBase_ReportedEventDescriptor vl_event := c_EPTF_LGenBase_emptyReportedEventDescriptor;
    vl_event.event.bIdx := v_ILog_Applib_bidx;
    vl_event.event.iIdx := v_EPTF_ILog_Applib_eventIdx_CERR;
    vl_event.event.target := {
      eIdx := pl_ptr.eIdx,
      fsmCtxIdx := pl_ptr.refContext.fCtxIdx
    };
    vl_event.reportedArgs := pl_ptr.reportedEvent.reportedArgs;
    f_EPTF_LGenBase_dispatchEvent(vl_event);
  }
}

private function f_EPTF_ILog_Applib_step_sendInvoke(in EPTF_LGenBase_TestStepArgs pl_ptr)
runs on EPTF_ILog_Applib_CT
{
  var EPTF_ILog_chainType vl_chainType := detailed; //???

  var integer vl_detailedChainId := f_EPTF_LGenBase_ILog_getChainId(
    pl_eAbsIdx := pl_ptr.eIdx,
    pl_fsmCtx := pl_ptr.refContext.fCtxIdx,
    pl_type := vl_chainType
  );

  var EPTF_IntegerList vl_chainIds;
  if (vl_detailedChainId==-1) {
    // detailed chain does not exist
    vl_chainIds := {};
  } else {
    vl_chainIds := {vl_detailedChainId};
  }

  var integer vl_connId := f_EPTF_LGenBase_getAppDataItemOfFsmCtx(
    pl_ptr.eIdx,
    pl_ptr.refContext.fCtxIdx,
    v_ILog_Applib_bidx,
    c_EPTF_ILog_appDataIdForConnectionId
  );
  if (vl_connId >=0 ) {
    var integer vl_transactionId := f_EPTF_ILog_Transport_sendReq(vl_chainIds, vl_connId, {Invoke:={}});
    if (vl_transactionId!=-1) {
      // send successful:
      var integer vl_iLogElementId := f_EPTF_LGenBase_ILog_addToChains(pl_ptr, v_EPTF_ILog_Applib_logItem_List_dbId);
      if (vl_iLogElementId!=-1) {
        v_EPTF_ILog_Applib_logItem_List[vl_iLogElementId] := { msg := "Message sent", logInfo := {sendInvoke:={pl_ptr}}};
      }

      v_ILog_Applib_Transactions[vl_connId][vl_transactionId] := pl_ptr;
    } else {
      // transaction unsuccessful
      var integer vl_iLogElementId := f_EPTF_LGenBase_ILog_addToChains(pl_ptr, v_EPTF_ILog_Applib_logItem_List_dbId);
      if (vl_iLogElementId!=-1) {
        v_EPTF_ILog_Applib_logItem_List[vl_iLogElementId] := { msg := "Sending message failed: invalid connection", logInfo := {sendInvoke:={pl_ptr}}};
      }

      // dispatch event
      var EPTF_LGenBase_ReportedEventDescriptor vl_event := c_EPTF_LGenBase_emptyReportedEventDescriptor;
      vl_event.event.bIdx := v_ILog_Applib_bidx;
      vl_event.event.iIdx := v_EPTF_ILog_Applib_eventIdx_TERR;
      vl_event.event.target := {
        eIdx := pl_ptr.eIdx,
        fsmCtxIdx := pl_ptr.refContext.fCtxIdx
      };
      vl_event.reportedArgs := pl_ptr.reportedEvent.reportedArgs;
      f_EPTF_LGenBase_dispatchEvent(vl_event);
    }
  } else {
    // invalid connectionID, cannot send message
    var integer vl_iLogElementId := f_EPTF_LGenBase_ILog_addToChains(pl_ptr, v_EPTF_ILog_Applib_logItem_List_dbId);
    if (vl_iLogElementId!=-1) {
      v_EPTF_ILog_Applib_logItem_List[vl_iLogElementId] := { msg := "Sending message failed: no connection", logInfo := {sendInvoke:={pl_ptr}}};
    }

    // dispatch event:
    var EPTF_LGenBase_ReportedEventDescriptor vl_event := c_EPTF_LGenBase_emptyReportedEventDescriptor;
    vl_event.event.bIdx := v_ILog_Applib_bidx;
    vl_event.event.iIdx := v_EPTF_ILog_Applib_eventIdx_CERR;
    vl_event.event.target := {
      eIdx := pl_ptr.eIdx,
      fsmCtxIdx := pl_ptr.refContext.fCtxIdx
    };
    vl_event.reportedArgs := pl_ptr.reportedEvent.reportedArgs;
    f_EPTF_LGenBase_dispatchEvent(vl_event);

  }
}

private function f_EPTF_ILog_Applib_step_sendEnd(in EPTF_LGenBase_TestStepArgs pl_ptr)
runs on EPTF_ILog_Applib_CT
{
  var EPTF_ILog_chainType vl_chainType := detailed; //???

  var integer vl_detailedChainId := f_EPTF_LGenBase_ILog_getChainId(
    pl_eAbsIdx := pl_ptr.eIdx,
    pl_fsmCtx := pl_ptr.refContext.fCtxIdx,
    pl_type := vl_chainType
  );

  var EPTF_IntegerList vl_chainIds;
  if (vl_detailedChainId==-1) {
    // detailed chain does not exist
    vl_chainIds := {};
  } else {
    vl_chainIds := {vl_detailedChainId};
  }

  var integer vl_connId := f_EPTF_LGenBase_getAppDataItemOfFsmCtx(
    pl_ptr.eIdx,
    pl_ptr.refContext.fCtxIdx,
    v_ILog_Applib_bidx,
    c_EPTF_ILog_appDataIdForConnectionId
  );
  if (vl_connId >=0 ) {
    var integer vl_transactionId := f_EPTF_ILog_Transport_sendReq(vl_chainIds, vl_connId, {End:={}});
    if (vl_transactionId!=-1) {
      // send successful:
      var integer vl_iLogElementId := f_EPTF_LGenBase_ILog_addToChains(pl_ptr, v_EPTF_ILog_Applib_logItem_List_dbId);
      if (vl_iLogElementId!=-1) {
        v_EPTF_ILog_Applib_logItem_List[vl_iLogElementId] := { msg := "Message sent", logInfo := {sendEnd:={pl_ptr}}};
      }

      v_ILog_Applib_Transactions[vl_connId][vl_transactionId] := pl_ptr;
    } else {
      // transaction unsuccessful
      var integer vl_iLogElementId := f_EPTF_LGenBase_ILog_addToChains(pl_ptr, v_EPTF_ILog_Applib_logItem_List_dbId);
      if (vl_iLogElementId!=-1) {
        v_EPTF_ILog_Applib_logItem_List[vl_iLogElementId] := { msg := "Sending message failed: invalid connection", logInfo := {sendEnd:={pl_ptr}}};
      }

      // dispatch event
      var EPTF_LGenBase_ReportedEventDescriptor vl_event := c_EPTF_LGenBase_emptyReportedEventDescriptor;
      vl_event.event.bIdx := v_ILog_Applib_bidx;
      vl_event.event.iIdx := v_EPTF_ILog_Applib_eventIdx_TERR;
      vl_event.event.target := {
        eIdx := pl_ptr.eIdx,
        fsmCtxIdx := pl_ptr.refContext.fCtxIdx
      };
      vl_event.reportedArgs := pl_ptr.reportedEvent.reportedArgs;
      f_EPTF_LGenBase_dispatchEvent(vl_event);
    }
  } else {
    // invalid connectionID, cannot send message
    var integer vl_iLogElementId := f_EPTF_LGenBase_ILog_addToChains(pl_ptr, v_EPTF_ILog_Applib_logItem_List_dbId);
    if (vl_iLogElementId!=-1) {
      v_EPTF_ILog_Applib_logItem_List[vl_iLogElementId] := { msg := "Sending message failed: no connection", logInfo := {sendEnd:={pl_ptr}}};
    }

    // dispatch event:
    var EPTF_LGenBase_ReportedEventDescriptor vl_event := c_EPTF_LGenBase_emptyReportedEventDescriptor;
    vl_event.event.bIdx := v_ILog_Applib_bidx;
    vl_event.event.iIdx := v_EPTF_ILog_Applib_eventIdx_CERR;
    vl_event.event.target := {
      eIdx := pl_ptr.eIdx,
      fsmCtxIdx := pl_ptr.refContext.fCtxIdx
    };
    vl_event.reportedArgs := pl_ptr.reportedEvent.reportedArgs;
    f_EPTF_LGenBase_dispatchEvent(vl_event);
  }
}

private function f_EPTF_ILog_Applib_step_timeout(in EPTF_LGenBase_TestStepArgs pl_ptr)
runs on EPTF_ILog_Applib_CT
{
  // timeout detected:
  var integer vl_iLogElementId := f_EPTF_LGenBase_ILog_addToChains(pl_ptr, v_EPTF_ILog_Applib_logItem_List_dbId);
  if (vl_iLogElementId!=-1) {
    v_EPTF_ILog_Applib_logItem_List[vl_iLogElementId] := { msg := "TIMEOUT", logInfo := {timedout:={pl_ptr}}};
  }
}

private function f_EPTF_ILog_Applib_transportRespHandler(
  in EPTF_IntegerList pl_iLogChainIds,
  in integer pl_connId,
  in integer pl_transactionId,
  in EPTF_ILog_Transport_ResponseMsg pl_msg
) runs on EPTF_ILog_Applib_CT {

  if (pl_transactionId<0
    or pl_connId<0 or
    not isbound(v_ILog_Applib_Transactions[pl_connId])
    or not isbound(v_ILog_Applib_Transactions[pl_connId][pl_transactionId])

  ) {
    // report ERR event:
    var integer vl_iLogElementId := f_EPTF_ILog_addToChains(v_EPTF_ILog_Applib_logItem_List_dbId,{});
    if (vl_iLogElementId!=-1) {
      v_EPTF_ILog_Applib_logItem_List[vl_iLogElementId] := { msg := "Erroneous response received", logInfo := {resp:={pl_connId,pl_transactionId,pl_msg}}};
    }

    var EPTF_LGenBase_ReportedEventDescriptor vl_event := c_EPTF_LGenBase_emptyReportedEventDescriptor;
    vl_event.event.bIdx := v_ILog_Applib_bidx;
    vl_event.event.iIdx := v_EPTF_ILog_Applib_eventIdx_ERR;
    f_EPTF_LGenBase_dispatchEvent(vl_event);
  } else if(ischosen(pl_msg.OK)) {
    var EPTF_LGenBase_TestStepArgs vl_ptr := v_ILog_Applib_Transactions[pl_connId][pl_transactionId];
    var integer vl_iLogElementId := f_EPTF_LGenBase_ILog_addToChains(vl_ptr, v_EPTF_ILog_Applib_logItem_List_dbId);
    if (vl_iLogElementId!=-1) {
      v_EPTF_ILog_Applib_logItem_List[vl_iLogElementId] := { msg := "Message received", logInfo := {resp:={pl_connId,pl_transactionId,pl_msg}}};
    }

    // report OK event:
    var EPTF_LGenBase_ReportedEventDescriptor vl_event := c_EPTF_LGenBase_emptyReportedEventDescriptor;
    vl_event.event.bIdx := v_ILog_Applib_bidx;
    vl_event.event.iIdx := v_EPTF_ILog_Applib_eventIdx_OK;
    vl_event.event.target := {
      eIdx := vl_ptr.eIdx,
      fsmCtxIdx := vl_ptr.refContext.fCtxIdx
    };
    vl_event.reportedArgs := vl_ptr.reportedEvent.reportedArgs;
    f_EPTF_LGenBase_dispatchEvent(vl_event);
  } else if (ischosen(pl_msg.NOK)) {
    var EPTF_LGenBase_TestStepArgs vl_ptr := v_ILog_Applib_Transactions[pl_connId][pl_transactionId];
    var integer vl_iLogElementId := f_EPTF_LGenBase_ILog_addToChains(vl_ptr, v_EPTF_ILog_Applib_logItem_List_dbId);
    if (vl_iLogElementId!=-1) {
      v_EPTF_ILog_Applib_logItem_List[vl_iLogElementId] := { msg := "Message received", logInfo := {resp:={pl_connId,pl_transactionId,pl_msg}}};
    }

    // report NOK event:
    var EPTF_LGenBase_ReportedEventDescriptor vl_event := c_EPTF_LGenBase_emptyReportedEventDescriptor;
    vl_event.event.bIdx := v_ILog_Applib_bidx;
    vl_event.event.iIdx := v_EPTF_ILog_Applib_eventIdx_NOK;
    vl_event.event.target := {
      eIdx := vl_ptr.eIdx,
      fsmCtxIdx := vl_ptr.refContext.fCtxIdx
    };
    vl_event.reportedArgs := vl_ptr.reportedEvent.reportedArgs;
    f_EPTF_LGenBase_dispatchEvent(vl_event);
  }

}


private function f_EPTF_ILog_Applib_logItem(in integer pl_elementIdx, in integer pl_dbId, in integer pl_chainId, in EPTF_IntegerList pl_param)
runs on EPTF_ILog_Applib_CT {
  var charstring vl_strLog := "";
  select( true )
  {
    case ( ischosen(v_EPTF_ILog_Applib_logItem_List[pl_elementIdx].logInfo.connection)){
      vl_strLog := v_EPTF_ILog_Applib_logItem_List[pl_elementIdx].msg;
    }
    case ( ischosen(v_EPTF_ILog_Applib_logItem_List[pl_elementIdx].logInfo.sendBegin)){
      vl_strLog := "Begin sent";
    }
    case ( ischosen(v_EPTF_ILog_Applib_logItem_List[pl_elementIdx].logInfo.sendInvoke)){
      vl_strLog := "Invoke sent";
    }
    case ( ischosen(v_EPTF_ILog_Applib_logItem_List[pl_elementIdx].logInfo.sendEnd)){
      vl_strLog := "End sent";
    }
    case ( ischosen(v_EPTF_ILog_Applib_logItem_List[pl_elementIdx].logInfo.timedout)){
      vl_strLog := "Timeout received";
    }
    case ( ischosen(v_EPTF_ILog_Applib_logItem_List[pl_elementIdx].logInfo.resp)){
      var integer vl_transactionId := v_EPTF_ILog_Applib_logItem_List[pl_elementIdx].logInfo.resp.transactionId,
      vl_connId := v_EPTF_ILog_Applib_logItem_List[pl_elementIdx].logInfo.resp.connId;
      select( true )
      {
        case (vl_transactionId<0
          or vl_connId<0 or
          not isbound(v_ILog_Applib_Transactions[vl_connId])
          or not isbound(v_ILog_Applib_Transactions[vl_connId][vl_transactionId])) {
          vl_strLog := "Unidentified message";
        }
        case ( ischosen(v_EPTF_ILog_Applib_logItem_List[pl_elementIdx].logInfo.resp.msg.OK) ){
          vl_strLog := "Response: OK";
        }
        case else{
          vl_strLog := "Response: NOK";
        }
      }

    }
    case else
    {
      
    }
  }

  action("EPTF_ILog_AppLib: " ,vl_strLog);
}

} // module EPTF_ILog_Applib_Functions
