///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: CS_A_Definitions
//
// Purpose:
//   Example user module containing used defined protocol-specific definitions for
//   protol "A" used in EPTF central scheduling
//
// Module Parameters:
//    -
//
//  Module depends on:
//    <EPTF_CLL_Common_Definitions>
//    <EPTF_CLL_CS_Definitions>
//    <EPTF_CLL_CSUIHandler_Definitions>
//    <EPTF_CLL_UIHandlerClient_Definitions>
//    <EPTF_CLL_UIHandlerClient_Definitions>
//    <EPTF_CLL_LGenBase_Definitions>
//    <EPTF_CLL_LGenBaseStatsUI_Definitions>
//    <Common_Definitions>
//    <A_defs>
//    <EPTF_CLL_ExecCtrl_Definitions>
//    <PIPEasp_PortType>
// 
// Current Owner:
//    Gabor Tatarka (egbotat)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
// 
///////////////////////////////////////////////////////////

module CS_A_Definitions
{

//=========================================================================
// Imports
//=========================================================================

import from EPTF_CLL_Common_Definitions all; // EPTF_IntegerList
import from EPTF_CLL_CS_Definitions all;
import from EPTF_CLL_CSUIHandler_Definitions all;
import from EPTF_CLL_UIHandlerClient_Definitions all;
import from EPTF_CLL_LGenBase_Definitions all;
import from EPTF_CLL_LGenBaseStatsUI_Definitions all;

import from Common_Definitions all;
import from A_defs all;
import from EPTF_CLL_ExecCtrl_Definitions all;

import from PIPEasp_PortType all; // example port used in LGen_A


//=========================================================================
// Data Types
//=========================================================================


///////////////////////////////////////////////////////////
//  Type: CS_A_PT
// 
//  Purpose:
//     Port type for communication between Admin and LGen
//
//  Elements:
//     -
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type port CS_A_PT message {
  inout CS_A_LGenExecuteTrafficCaseList;
  inout CS_A_LGenExecutionResultList;
  inout CS_A_LGenStatus;
} with { extension "internal" }



/////////////////////////////////////////////////////////////////////////
//
//  Component type : CS_LGen_A_CT
//
//  Purpose:
//    Example component type collecting user specefic CS_LGen features for
//    protocol type 'A'
//
//  Elements:
//    -
//
//  Detailed Comments:
//    -
/////////////////////////////////////////////////////////////////////////
type component CS_LGen_A_CT extends EPTF_CS_LGenBase_CT, EPTF_UIHandlerClient_CT
{
  // Append this component type definition with the necessary variables, timers, ports, etc.

  // port A_PT A_PCO;
  port PIPEasp_PT PIPEasp_PCO; // just as example

  port CS_A_PT CS_PCO;

  var CS_A_LGenExecuteTrafficCaseList   v_execBurst_msg := {}
  var CS_A_LGenExecutionResultList      v_execBurstResult_msg := {}
  var integer                           v_execBurstIdx := 0;
  var integer                           v_trafficCaseIdx := 0;
}

/////////////////////////////////////////////////////////////////////////
//
//  Component type : CS_ApplAdmin_A_CT
//
//  Purpose:
//    Example component type collecting user specefic CS_Admin features for
//    protocol type 'A'
//
//  Elements:
//    -
//
//  Detailed Comments:
//    -
/////////////////////////////////////////////////////////////////////////
type component CS_ApplAdmin_A_CT extends EPTF_CS_ApplAdminBase_UIHandler_CT
{
  var integer v_adminIdx := -1;

  // User-defined, traffic type dependent extension of traffic case data
  var CS_A_TrafficCaseDataExtensionList v_A_trafficCaseData := {};

  var CS_A_LGenExecuteTrafficCaseList   v_execBurst_msg := {}
  var CS_A_LGenExecutionResultList      v_execBurstResult_msg := {}
  var CS_A_LGenStatus                   v_lgenStatus_msg := {false};
  var integer                           v_execBurstIdx := 0;
  var integer                           v_trafficCaseIdx := 0;
  var EPTF_IntegerList                  v_currentRangeItems := {};

  var CS_ExecPhase                      v_execPhase := execCTRL_PREEXEC;

  var default v_msgHandler;
  port CS_A_PT CS_PCO;

  var EPTF_CS_LGenBase_CT v_lgenCompRef;
}

} // module

