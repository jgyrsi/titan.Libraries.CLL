///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: LoadRegulator_Functions
//
// Purpose:
//   Example LoadRegulator for EPTF Central Scheduling demo
//
// Module Parameters:
//
// Module depends on:
//    <EPTF_CLL_LoadRegulator_Functions>
//    <EPTF_CLL_HostAdmin_BaseFunctions>
//    <EPTF_CLL_Variable_Definitions>
//    <LoadRegulator_Definitions>
// 
// Current Owner:
//    Gabor Tatarka (egbotat)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////

module LoadRegulator_Functions
{

import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_LoadRegulator_Functions all;
import from EPTF_CLL_HostAdmin_BaseFunctions all;
import from EPTF_CLL_Variable_Definitions all;
import from LoadRegulator_Definitions all;

function f_LoadRegulator_getSutLoad()
runs on LoadRegulator_CT
return float
{
  f_EPTF_HostAdmin_Base_update();
  return v_EPTF_HostAdmin_testerHostLoad;
}

function f_LoadRegulator_behavior(
  in charstring pl_selfName,
  in EPTF_Var_CT pl_execCtrlCompRef,
  in float pl_loadToReach)
runs on LoadRegulator_CT
{
  f_EPTF_HostAdmin_Base_init_CT(pl_selfName);
  f_EPTF_LoadRegulator_init_CT(pl_selfName, refers(f_LoadRegulator_getSutLoad));
  //v_EPTF_LoadRegulator_loadToReach := pl_loadToReach;
  f_EPTF_LoadRegulator_registerToExecCtrl(pl_execCtrlCompRef);
  f_EPTF_LoadRegulator_adjustTargetLoadInExecCtrl(pl_loadToReach);
  timer t_wait := 100.0;
  t_wait.start;
  alt {
    []t_wait.timeout {
      t_wait.start;
      repeat;
    }
  }
  if(t_wait.running) { t_wait.stop; }
  f_EPTF_Base_cleanup_CT();
}

}  // end of module
