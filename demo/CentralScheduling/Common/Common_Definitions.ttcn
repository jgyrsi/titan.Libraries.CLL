///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
// Module: Common_Definitions
//
// Purpose:
//   Common type and constant definitions file for CentralScheduling demo.
//
// Module Parameters:
//    tsp_CS_preExec_Suffix
//    tsp_CS_loadGen_Suffix
//    tsp_CS_postExec_Suffix
//    tsp_LoadRegulators
//
//  Module depends on:
//    -
// 
///////////////////////////////////////////////////////////
module Common_Definitions
{

modulepar charstring tsp_CS_preExec_Suffix := "_Pre";
modulepar charstring tsp_CS_loadGen_Suffix := "";
modulepar charstring tsp_CS_postExec_Suffix := "_Post";
modulepar CBE_LoadRegList tsp_LoadRegulators := {}; // note: starting of the load regulators is out of the scope of this module


type enumerated CS_ExecPhase {
  execCTRL_PREEXEC (0),
  execCTRL_LOADGEN (1),
  execCTRL_POSTEXEC (2)
} 

type record CBE_LoadReg {
  charstring host,
  charstring name,
  float loadToReach
}

type record of CBE_LoadReg CBE_LoadRegList;

const integer c_CS_TcIdx_TcMultiplierFactor := 3,
              c_CS_TcIdx_Remainder_PREEXEC := 0,
              c_CS_TcIdx_Remainder_LOADGEN := 1,
              c_CS_TcIdx_Remainder_POSTEXEC := 2;

} // end of module
