///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: B_defs
//
// Purpose:
//   Example user module containing used defined protocol-specific definitions for
//   protol "B" used in EPTF central scheduling
//
// Module Parameters:
//    tsp_debug_B_CS_SchedulerFunctions - *booelan* - Enable logging of scheduler functions in B
//
//  Module depends on:
//    <EPTF_CLL_CS_Definitions>
//    <Common_Definitions>
// 
// Current Owner:
//    Gabor Tatarka (egbotat)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
// 
///////////////////////////////////////////////////////////

module B_defs
{

import from EPTF_CLL_CS_Definitions all;
import from Common_Definitions all;

///////////////////////////////////////////////////////////
//  Type: B_tcdata
// 
//  Purpose:
//     Common traffic case data type for for protocol type B.
//     Used to compose traffic type dependent data which sent
//     from Admin to LGen
// 
//  Elements:
//      intField - *integer* - integer data field
//      floatField - *float* - float data field

// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type record B_tcdata
{
  integer intField,
  float   floatField
}


///////////////////////////////////////////////////////////
//  Type: Bdata
// 
//  Purpose:
//     Record type for composing traffic type dependent data
//     which sent from Admin to LGen
// 
//  Elements:
//      execPhase - <CS_ExecPhase> - the current execution phase
//      tcData - <B_tcdata> - traffic case data
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type record Bdata
{
  CS_ExecPhase execPhase,
  B_tcdata tcData
}

///////////////////////////////////////////////////////////
//  Type: BdataReply
// 
//  Purpose:
//     Record type for composing traffic type dependent data
//     which sent from LGen to Admin
//
//  Elements:
//      tcData - <B_tcdata> - traffic case data
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type record BdataReply
{
  B_tcdata tcData
}

///////////////////////////////////////////////////////////
//  Type: CS_A_LGenExecuteTrafficCase
// 
//  Purpose:
//     Record type for triggering a traffic case
//
//  Elements:
//      trafficCaseSelector - *integer*
//      data - <Adata>
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type record CS_B_LGenExecuteTrafficCase
{
  integer   trafficCaseSelector,
  Bdata     data
}

///////////////////////////////////////////////////////////
//  Type: CS_B_LGenExecuteTrafficCaseList
// 
//  Purpose:
//     Record type for triggering a list of traffic cases
//
//  Elements:
//      record of CS_B_LGenExecuteTrafficCase
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type record of CS_B_LGenExecuteTrafficCase CS_B_LGenExecuteTrafficCaseList

///////////////////////////////////////////////////////////
//  Type: CS_B_LGenExecutionResult
// 
//  Purpose:
//     Record type for result of a traffic case
//
//  Elements:
//      trafficCaseSelector - *integer*
//      execResult - *verdicttype*
//      data - AdataReply
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type record CS_B_LGenExecutionResult
{
  integer     trafficCaseSelector,
  verdicttype execResult,
  BdataReply  data
}

///////////////////////////////////////////////////////////
//  Type: CS_B_LGenExecutionResultList
// 
//  Purpose:
//     Record type for result of a list of traffic cases
//
//  Elements:
//      record of TcResult_A
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type record of CS_B_LGenExecutionResult CS_B_LGenExecutionResultList

///////////////////////////////////////////////////////////
//  Type: CS_B_LGenStatus
// 
//  Purpose:
//   Record type used by LGen to set its status/availability in the admin's
//   pool.
// 
//  Elements:
//   LGenEnabled - *boolean* - status of the LGen
// 
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////
type record CS_B_LGenStatus
{
  boolean        LGenEnabled
}

//=========================================================================
// Data Types
//=========================================================================

///////////////////////////////////////////////////////////
//  Type: CS_B_TrafficCaseDataExtension
// 
//  Purpose:
//     User-defined, traffic type dependent extension of traffic case data.
// 
//  Elements:
//   -
//
//  Detailed Comments:
//   It is just an example to demonstrate how to extend the 
//   standard, library-defined traffic case data with protocol-
//   type specific data
///////////////////////////////////////////////////////////
type record CS_B_TrafficCaseDataExtension
{
 // Put your definition here
}

///////////////////////////////////////////////////////////
// Type: CS_B_TrafficCaseDataExtensionList
//
// Purpose:
//   Record of CS_B_TrafficCaseDataExtension elements to provide a container
//   for arbitrary number of user-defined, traffic type dependent traffic case
//   data
//
// Elements:
//   record of <CS_B_TrafficCaseDataExtension>
//
//  Detailed Comments:
//   See also <EPTF_CS_trafficCaseDataList>
///////////////////////////////////////////////////////////
type record of CS_B_TrafficCaseDataExtension CS_B_TrafficCaseDataExtensionList;

} // module

