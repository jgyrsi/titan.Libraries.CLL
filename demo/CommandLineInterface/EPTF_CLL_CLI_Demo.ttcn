///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_CLI_Demo
// 
//  Purpose:
//    This module contains the implementation of generic EPTF_CLL_CLI functions.
// 
//  Module depends on:
//    <EPTF_CLL_Base_Definitions>
//    <EPTF_CLL_Commmon_Functions>
// 
//  Current Owner:
//    ethbaat
// 
//  Last Review Date:
//    -
//
//  Detailed Comments:
//    This module contains the functions for testing EPTF_CLL_CLI
//    It can be useful for testing components extension of EPTF_CLL_CLI_CT and/or EPTF_CLL_CLI_client_CT
//
///////////////////////////////////////////////////////////////

module EPTF_CLL_CLI_Demo {
import from EPTF_CLL_CLI_Definitions all;
import from EPTF_CLL_CLI_Functions all;
import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_Common_Definitions all;
import from TELNETasp_PortType all;

modulepar charstring tsp_EPTF_CLI_Demo_prompt :="TTCN> ";
//============================================
//  Datatypes
//============================================
//Command and the expected answer
type record CmdAndAnswer { 
  charstring command,
  charstring expectedAnswer 
};

type record of CmdAndAnswer CmdAndAnswerList;

//============================================
//  Component definitions
//============================================
type component EPTF_CLI_Demo_CT extends EPTF_CLI_CT, EPTF_CLI_Client_CT {
}  

type component EPTF_CLI_Client_Demo_Display_ShowTime_CT extends EPTF_CLI_Demo_CT {
  timer t1 := 1.0;
  var float v_period := 1.0;
  var boolean displayIsOn := false;
}
type component EPTF_CLI_Demo_MTC  extends EPTF_Base_CT { };

type component EPTF_CLI_Demo_terminal extends EPTF_Base_CT{  
  private port TELNETasp_PT EPTF_CLI_Demo_terminal_PCO;
  private var charstring v_EPTF_CLI_Demo_terminal_rcvdMsg:="";
  private var integer v_EPTF_CLI_Demo_terminal_rcvdInt:=-1;
  private var boolean v_EPTF_CLI_Demo_terminal_initiated:=false;
  private var default vd_alt:=null;
  private var charstring v_EPTF_CLI_Demo_terminal_prompt:="";

  //private var 
};

type component EPTF_CLI_Demo_display extends EPTF_Base_CT {
  private port TELNETasp_PT EPTF_CLI_Demo_display_PCO;    
  private var charstring v_EPTF_CLI_Demo_display_rcvdMsg:="";
  private var integer v_EPTF_CLI_Demo_display_rcvdInt:=-1;
  private var boolean v_EPTF_CLI_Demo_display_initiated:=false;
  private var default vd_alt:=null;
  private var charstring v_EPTF_CLI_Demo_display_prompt:="";
};
//============================================
// f_EPTF_CLI_Client_echoHandler
//============================================
function f_EPTF_CLI_Client_echoHandler(in charstring pl_commandArgs, inout charstring pl_result) runs on EPTF_CLI_Client_CT return integer {
  if (pl_commandArgs=="help") {
    pl_result := "echo - prints out its parameters\n"&
    "This is the detailed help message of the echo command\n"&
    "If called with ""ERROR!"" argument it exits with code 1\n"&
    "If called with -t <duration> as the first argument\n"&
    "the result is printed after that time has passed (in seconds).";
    return 0; // OK
  }
  if (pl_commandArgs=="ERROR!") {
    pl_result := "Syntax error!"
    return 1; // set nonzero exit code
  }
  if (regexp(pl_commandArgs,c_EPTF_CLI_anyWord&c_EPTF_CLI_everything,0)=="-t") {
    // -t option is given as the first argument
    // extract the duration value:
    var float vl_duration:=str2float(regexp(pl_commandArgs,c_EPTF_CLI_anyWord&c_EPTF_CLI_anyWord&c_EPTF_CLI_everything,1));
    // delay response:
    timer t_wait := vl_duration;
    t_wait.start; t_wait.timeout;
    // set the result to the remaining string:
    pl_result := regexp(pl_commandArgs,c_EPTF_CLI_anyWord&c_EPTF_CLI_anyWord&c_EPTF_CLI_everything,2);
    return 0; //OK
  }
  pl_result := pl_commandArgs;
  return 0; // OK
}

function f_EPTF_CLI_Client_Demo_Display_ShowTime_init_CT() runs on EPTF_CLI_Client_Demo_Display_ShowTime_CT {
  f_EPTF_CLI_init_CT("ShowTime_Demo");
  f_EPTF_CLI_Client_init_CT("ShowTime_Demo", self);

  t1.start(1.0);
  activate(as_EPTF_CLI_Client_Demo_updateTime());
  f_EPTF_CLI_Client_registerCommand(
    "showTime",
    "displays seconds elapsed",
    refers(f_EPTF_CLI_Client_showTimeHandler)
  );
}
//============================================
// f_EPTF_CLI_Client_showTimeHandler
//============================================
function f_EPTF_CLI_Client_showTimeHandler(in charstring pl_commandArgs, inout charstring pl_result) runs on EPTF_CLI_Client_Demo_Display_ShowTime_CT return integer {
  if (pl_commandArgs=="help") {
    pl_result := " showTime - displays time periodically\n"&
    "Displays elapsed time periodically on the display terminal\n"&
    "Usage: showTime [stop|period <lenght>]\n"&
    "  stop: stops time display\n"&
    "  period <lenght>: sets the period of the display, <lenght> is the period in seconds (float number)\n"&
    "If called without arguments the time display is started";
    return 0; // OK
  }
  if (pl_commandArgs=="stop") {
    displayIsOn := false;
    pl_result := "showTime: Displaying of time is stopped"
    f_EPTF_CLI_Client_sendCommandDisplay("showTime","showTime stopped.");
    return 0; // set nonzero exit code
  }
  var charstring vl_argument := regexp(pl_commandArgs,c_EPTF_CLI_anyWord&c_EPTF_CLI_everything,0);
  if (vl_argument=="period") {
    var charstring periodString := regexp(pl_commandArgs,c_EPTF_CLI_anyWord&c_EPTF_CLI_anyWord,1);
    if (periodString=="") {
      pl_result := "showTime: syntax error\n"&
      " period should have a float argument: showTime period 1.0\n"&
      "For help try: showTime help";
      return 2; // syntax error for setting period
    }
    v_period := str2float(periodString);
    pl_result := "showTime: Display period is set to "&periodString&" seconds."
    f_EPTF_CLI_Client_sendCommandDisplay("showTime","Period is changed to "&periodString&" seconds.");
    // start the timer with the remaining seconds:
    var float vl_remaining := v_period-t1.read;
    if (vl_remaining<0.0) {
      vl_remaining:=0.0;
    }
    t1.stop; t1.start(vl_remaining);
    return 0; // set nonzero exit code
  }
  if (pl_commandArgs=="") {
    pl_result := "Display of elapsed time is started on the Display terminal.\n"&
    "Period of display: "&float2str(v_period);
    f_EPTF_CLI_Client_sendCommandDisplay("showTime","showTime started with period "&float2str(v_period)&" seconds.");
    displayIsOn := true;
    return 0; // OK
  } else {
    pl_result := "showTime: syntax error: Unknown argument: "&vl_argument&"\n"&
    "For help try: showTime help";
    return 1; // syntax error
  }
  pl_result := "Display of elapsed time is started on the Display terminal.\n"&
  "Period of display: "&float2str(v_period);
  return 0; // OK
}

//============================================
// as_EPTF_CLI_Client_Demo_updateTime
//============================================
altstep as_EPTF_CLI_Client_Demo_updateTime()  runs on EPTF_CLI_Client_Demo_Display_ShowTime_CT {
  [displayIsOn] t1.timeout {
      f_EPTF_CLI_Client_sendCommandDisplay("showTime","Elapsed time: "&float2str(f_EPTF_Base_getRelTimeInSecs()));
      t1.start(v_period);
      repeat;
    }
}

//====For automated Terminal ====

//============================================
// f_EPTF_CLI_Demo_CLI_server_behavior
//============================================
function f_EPTF_CLI_Demo_CLI_server_behavior(in charstring pl_selfName) runs on EPTF_CLI_CT {
  f_EPTF_CLI_init_CT(pl_selfName);
  f_EPTF_Base_wait4Shutdown();
};

//============================================
// f_EPTF_CLI_Demo_CLI_client_behavior
//============================================
function f_EPTF_CLI_Demo_CLI_client_behavior(
  in charstring pl_selfName, 
  in EPTF_CLI_CT pl_CLI_compRef := null
) runs on EPTF_CLI_Client_CT {
  f_EPTF_CLI_Client_init_CT( pl_selfName,pl_CLI_compRef);
  f_EPTF_CLI_Client_registerCommand(
    "echo",
    "echoes its arguments",
    refers(f_EPTF_CLI_Client_echoHandler));
  f_EPTF_Base_wait4Shutdown();
}

group CLI_Terminal {
  //============================================
  // f_EPTF_CLI_Demo_terminal_init_CT
  //============================================
  function f_EPTF_CLI_Demo_terminal_init_CT(in charstring pl_selfName, in charstring pl_prompt:="") runs on EPTF_CLI_Demo_terminal {
    if(v_EPTF_CLI_Demo_terminal_initiated) { return};
    f_EPTF_Base_init_CT(pl_selfName);
    v_EPTF_CLI_Demo_terminal_rcvdMsg:="";
    v_EPTF_CLI_Demo_terminal_rcvdInt:=-1;
    v_EPTF_CLI_Demo_terminal_prompt:= pl_prompt;
    log(">>>map is coming");
    map(self:EPTF_CLI_Demo_terminal_PCO,system:EPTF_CLI_Demo_terminal_PCO);
    f_EPTF_Base_registerCleanup(refers(f_EPTF_CLI_Demo_terminal_cleanup_CT));
    log("registered");
    v_EPTF_CLI_Demo_terminal_initiated:=true;
  }
  //============================================
  // f_EPTF_CLI_Demo_terminal_cleanup_CT
  //============================================
  function f_EPTF_CLI_Demo_terminal_cleanup_CT() runs on EPTF_CLI_Demo_terminal {
    if(not v_EPTF_CLI_Demo_terminal_initiated) {return};
    if(vd_alt!=null) {
      deactivate(vd_alt);
      vd_alt:=null;
    }
    unmap(self:EPTF_CLI_Demo_terminal_PCO,system:EPTF_CLI_Demo_terminal_PCO);
    v_EPTF_CLI_Demo_terminal_initiated:=false;
  }
  //============================================
  // f_EPTF_CLI_Demo_CLI_terminal_behavior
  //============================================
  function f_EPTF_CLI_Demo_CLI_terminal_behavior(in charstring pl_selfName,in template CmdAndAnswerList pl_cmdAndAnswerList, in charstring pl_prompt:="", in float pl_timeout:=3.0) 
  runs on EPTF_CLI_Demo_terminal {

    f_EPTF_CLI_Demo_terminal_init_CT(pl_selfName, pl_prompt);
    log(">>>Terminal:Init done");

    var boolean vl_stopped:=false, vl_exit:=false;
    timer T; T.start(pl_timeout);
    for(var integer i:=0;i<sizeof(pl_cmdAndAnswerList);i:=i+1) {
      EPTF_CLI_Demo_terminal_PCO.send(valueof(pl_cmdAndAnswerList[i].command));
      select(valueof(pl_cmdAndAnswerList[i].command)) {
        case("exit") { vl_exit:=true; break;}
        case("quit") { vl_exit:=true;break;}
        case("stop") { vl_stopped:=true;break;}
        case("stopAll"){ vl_stopped:=true;break;}
      }
      alt {
        [] EPTF_CLI_Demo_terminal_PCO.receive(charstring:*) ->value v_EPTF_CLI_Demo_terminal_rcvdMsg {
            if( v_EPTF_CLI_Demo_terminal_rcvdMsg == "\n"&pl_prompt or v_EPTF_CLI_Demo_terminal_rcvdMsg == pl_prompt) {
              log("Prompt received")
              repeat;
            } else if( match(v_EPTF_CLI_Demo_terminal_rcvdMsg,pl_cmdAndAnswerList[i].expectedAnswer) ) {
              setverdict(pass);
              alt {
                [] EPTF_CLI_Demo_terminal_PCO.receive(charstring:*) ->value v_EPTF_CLI_Demo_terminal_rcvdMsg {
                    if( v_EPTF_CLI_Demo_terminal_rcvdMsg == "\n"&pl_prompt or v_EPTF_CLI_Demo_terminal_rcvdMsg == pl_prompt) {
                      log("Prompt received")
                    }
                  }
                [] EPTF_CLI_Demo_terminal_PCO.receive(integer:*)-> value v_EPTF_CLI_Demo_terminal_rcvdInt { }
                [] T.timeout { setverdict(fail, "timeout occured"); }
              }
              //>>>>EXIT
            } else if( v_EPTF_CLI_Demo_terminal_rcvdMsg=="\n") {
              log("\n received and accepted"); repeat;
            } else {
              log(">>>> Unexpected answer received: ",v_EPTF_CLI_Demo_terminal_rcvdMsg);
              log(">>>> Expected                  : ",pl_cmdAndAnswerList[i].expectedAnswer);
              setverdict(fail);
            }
          }
        [] EPTF_CLI_Demo_terminal_PCO.receive(integer:*)-> value v_EPTF_CLI_Demo_terminal_rcvdInt { 
            log(">>>>>Integer received",v_EPTF_CLI_Demo_terminal_rcvdInt); 
            repeat };
        [] T.timeout { 
            setverdict(fail, "timeout occured");
          }
      }//alt

    }//for

    if( vl_exit) {
      alt{
        [] EPTF_CLI_Demo_terminal_PCO.receive(integer:*)-> value v_EPTF_CLI_Demo_terminal_rcvdInt { 
            log(">>>>>Integer received at exit: ",v_EPTF_CLI_Demo_terminal_rcvdInt); 
            f_EPTF_Base_stop(pass);
          };
        [] T.timeout { 
            setverdict(fail, "timeout occured");
          }
      }
    } else if( vl_stopped ) {
      T.stop;
      f_EPTF_Base_wait4Shutdown();
    } else {
      T.timeout;
      log(">>>Terminal successfully exits");
      f_EPTF_Base_stop(pass);
    }
  }//f_
}//group CLI_Terminal

group CLI_display {
  //============================================
  // f_EPTF_CLI_Demo_display_init_CT
  //============================================
  function f_EPTF_CLI_Demo_display_init_CT(in charstring pl_selfName, in charstring pl_prompt:="") runs on EPTF_CLI_Demo_display {
    if(v_EPTF_CLI_Demo_display_initiated) { return};
    f_EPTF_Base_init_CT(pl_selfName);
    v_EPTF_CLI_Demo_display_rcvdMsg:="";
    v_EPTF_CLI_Demo_display_rcvdInt:=-1;
    v_EPTF_CLI_Demo_display_prompt:= pl_prompt;
    log(">>>map is coming");
    map(self:EPTF_CLI_Demo_display_PCO,system:EPTF_CLI_Demo_display_PCO);
    f_EPTF_Base_registerCleanup(refers(f_EPTF_CLI_Demo_display_cleanup_CT));
    log("registered");
    v_EPTF_CLI_Demo_display_initiated:=true;
  }
  //============================================
  // f_EPTF_CLI_Demo_display_cleanup_CT
  //============================================
  function f_EPTF_CLI_Demo_display_cleanup_CT() runs on EPTF_CLI_Demo_display {
    if(not v_EPTF_CLI_Demo_display_initiated) {return};
    if(vd_alt!=null) {
      deactivate(vd_alt);
      vd_alt:=null;
    }
    unmap(self:EPTF_CLI_Demo_display_PCO,system:EPTF_CLI_Demo_display_PCO);
    v_EPTF_CLI_Demo_display_initiated:=false;
  }
  //============================================
  // f_EPTF_CLI_Demo_CLI_display_behavior
  //============================================
  function f_EPTF_CLI_Demo_CLI_display_behavior(in charstring pl_selfName,in charstring pl_prompt:="") 
  runs on EPTF_CLI_Demo_display {
    f_EPTF_CLI_Demo_display_init_CT(pl_selfName, pl_prompt);
    log(">>>display:Init done");
    timer T; T.start(15.0)
    template charstring tl_elapsedTimePattern:= pattern "Elapsed time: " & "[\d]#(1,)\.[\d]#(1,6)"; //1.012268"
    template charstring tl_elapsedTimeAndPromptPattern := pattern "Elapsed time: " & "[\d]#(1,)\.[\d]#(1,6)" & "\n" & pl_prompt;

    alt {
      [] EPTF_CLI_Demo_display_PCO.receive(charstring:*) ->value v_EPTF_CLI_Demo_display_rcvdMsg {
          if( v_EPTF_CLI_Demo_display_rcvdMsg == "\n"&pl_prompt or v_EPTF_CLI_Demo_display_rcvdMsg == pl_prompt) {
            log("Prompt received");
            repeat;
          } else if( match(v_EPTF_CLI_Demo_display_rcvdMsg,tl_elapsedTimePattern) or
            match(v_EPTF_CLI_Demo_display_rcvdMsg,tl_elapsedTimeAndPromptPattern)) {
            setverdict(pass);
            repeat;
          } else if( v_EPTF_CLI_Demo_display_rcvdMsg=="\n") {
            log("\n received and accepted"); 
            repeat;
          } else {
            log(">>>> Unexpected answer received: ",v_EPTF_CLI_Demo_display_rcvdMsg);
          }
        }
      [] EPTF_CLI_Demo_display_PCO.receive(integer:*)-> value v_EPTF_CLI_Demo_display_rcvdInt { 
          log(v_EPTF_CLI_Demo_display_rcvdInt); 
          repeat };
      [] T.timeout { 
          setverdict(fail, "timeout occured");
        }
    }//alt

    //}//for
    log(">>>display successfully exits");
    f_EPTF_Base_stop(pass);

  }//f_

}//group CLI_display

  //This is almost the same as f_EPTF_CLI_Client_Demo_Display_ShowTime_init_CT but CLI is not used
  function f_EPTF_CLI_Demo_client_ShowTime_init_CT(in charstring pl_selfName, in EPTF_CLI_CT pl_CLI_compRef := null) runs on EPTF_CLI_Client_Demo_Display_ShowTime_CT {
    //f_EPTF_CLI_init_CT("ShowTime_Demo");
    f_EPTF_CLI_Client_init_CT(pl_selfName, pl_CLI_compRef);
    displayIsOn:=true; //dangerous, refine it!!!!
    activate(as_EPTF_CLI_Client_Demo_updateTime());
    t1.start(1.0);
    f_EPTF_CLI_Client_registerCommand(
      "showTime",
      "displays seconds elapsed",
      refers(f_EPTF_CLI_Client_showTimeHandler)
    );    
    f_EPTF_CLI_Client_registerCommand(
      "echo",
      "echoes its arguments",
      refers(f_EPTF_CLI_Client_echoHandler));
  }

  function f_EPTF_CLI_Demo_client_behavior_ShowTime(in charstring pl_selfName, in EPTF_CLI_CT pl_CLI_compRef := null) runs on EPTF_CLI_Client_Demo_Display_ShowTime_CT {
    f_EPTF_CLI_Demo_client_ShowTime_init_CT(pl_selfName,pl_CLI_compRef);
    f_EPTF_Base_wait4Shutdown();
  }

///////////////////////////////////////////////////////////
//  Testcase:  tc_EPTF_CLI_Demo_executeCustomCommandManually
// 
//  Purpose:
//    This testcase demonstrate how to use CLI manually via real telnet terminal window and display window
//    The user registers two commands "echo" as case insensitive and "EcHo2" as case sensitive
//    These commands can also be used from terminal
//
//  Detailed Comments:
//    1. Start the testcase. It runs on a CLI-CLIClient
//    2. Open a terminal 
//    3. telnet into the test
//    4. Try commands, e.g."help help", "echo AAA", "EcHo2"
//    5. type exit, quit, stop or stopAll  in the telnet terminal
///////////////////////////////////////////////////////////

testcase tc_EPTF_CLI_Demo_executeCustomCommandManually() runs on EPTF_CLI_Demo_CT {
  f_EPTF_CLI_init_CT("CLI_Demo");
  f_EPTF_CLI_Client_init_CT("CLI_Demo", self);

  f_EPTF_CLI_Client_registerCommand(
    "echo",
    "echoes its arguments",
    refers(f_EPTF_CLI_Client_echoHandler)
  );

  f_EPTF_CLI_Client_registerCommand(
    "EcHo2",
    "echoes its arguments",
    refers(f_EPTF_CLI_Client_echoHandler),
    -,
    false
  );
  f_EPTF_Base_wait4Shutdown();
}

///////////////////////////////////////////////////////////
//  Testcase:  tc_EPTF_CLI_Demo_displayManually
// 
//  Purpose:
//    This testcase demonstrate how to use CLI manually via real telnet terminal window and display window
//    The user registers two commands "echo" as case insensitive and "EcHo2" as case sensitive
//    These commands can also be used from terminal
//
//  Detailed Comments:
//    1. Start the testcase. It runs on a CLI-CLIClient
//    2. Open a terminal 
//    3. telnet into the test
//    4. Start a display with telnet connection
//    5. Try commands, e.g."help help", "echo AAA", "EcHo2"
//    6. type exit, quit, stop or stopAll  in the telnet terminal
///////////////////////////////////////////////////////////
// this is manual test!
testcase tc_EPTF_CLI_Demo_displayManually() runs on EPTF_CLI_Client_Demo_Display_ShowTime_CT {

  f_EPTF_CLI_Client_Demo_Display_ShowTime_init_CT();

  f_EPTF_CLI_Client_registerCommand(
    "echo",
    "echoes its arguments",
    refers(f_EPTF_CLI_Client_echoHandler)
  );

  // case sensitive:
  f_EPTF_CLI_Client_registerCommand(
    "EcHo2",
    "echoes its arguments",
    refers(f_EPTF_CLI_Client_echoHandler),
    -,
    false
  );

  setverdict(pass);

  f_EPTF_Base_wait4Shutdown();

  f_EPTF_Base_stop(inconc);
}
//===============================================
// tc_EPTF_CLI_Demo_AutomatedTerminalAndDisplay
//===============================================
testcase tc_EPTF_CLI_Demo_AutomatedTerminalAndDisplay() runs on EPTF_CLI_Demo_MTC {
  f_EPTF_Base_init_CT("MTC");
  var EPTF_CLI_CT vc_EPTF_CLI_Demo_CLI_server := EPTF_CLI_CT.create;
  var EPTF_CLI_Client_Demo_Display_ShowTime_CT vc_EPTF_CLI_Demo_CLI_client := EPTF_CLI_Client_Demo_Display_ShowTime_CT.create;
  var EPTF_CLI_Demo_terminal vc_EPTF_CLI_Demo_CLI_terminal := EPTF_CLI_Demo_terminal.create;
  var EPTF_CLI_Demo_display vc_EPTF_CLI_Demo_CLI_display := EPTF_CLI_Demo_display.create;
  timer T1;

  vc_EPTF_CLI_Demo_CLI_server.start(f_EPTF_CLI_Demo_CLI_server_behavior("CLI_server"));
  T1.start(1.0); T1.timeout;

  vc_EPTF_CLI_Demo_CLI_display.start(f_EPTF_CLI_Demo_CLI_display_behavior("CLI display",tsp_EPTF_CLI_Demo_prompt));
  T1.start(1.0); T1.timeout;

  vc_EPTF_CLI_Demo_CLI_client.start(f_EPTF_CLI_Demo_client_behavior_ShowTime("CLI_client",vc_EPTF_CLI_Demo_CLI_server));
  T1.start(1.0); T1.timeout;

  template CmdAndAnswerList vl_cmdAndAnswerList := { {command:= "echo haho haliho", expectedAnswer:= "haho haliho"} };
  vc_EPTF_CLI_Demo_CLI_terminal.start(f_EPTF_CLI_Demo_CLI_terminal_behavior("CLI_terminal",vl_cmdAndAnswerList,tsp_EPTF_CLI_Demo_prompt, 10.0));

  any component.done;
  setverdict(pass);
  f_EPTF_Base_cleanup_CT();
}//tc_

control{
  execute(tc_EPTF_CLI_Demo_executeCustomCommandManually());
  execute(tc_EPTF_CLI_Demo_displayManually());
  execute(tc_EPTF_CLI_Demo_AutomatedTerminalAndDisplay());
}

}//module
