///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
module EPTF_Semaphore_test {

import from EPTF_CLL_Semaphore_Definitions all;
import from EPTF_CLL_Semaphore_Functions all;
import from EPTF_CLL_Base_Functions all;

///////////////////////////////////////////
// timer test
///////////////////////////////////////////

type component SemaphoreTest_CT extends EPTF_Semaphore_CT {
  timer t_wait := 1.0;
  var boolean blockingFlag := false;
  var integer v_semaphoreIdx; // will be unlocked
  var integer v_semaphoreIdx2; // will not be unlocked
}

altstep as_Semaphore_test_default() runs on SemaphoreTest_CT {
  [] t_wait.timeout {
    // wait for unlock, that will not happen:
    if (not f_EPTF_Semaphore_waitForUnlock(v_semaphoreIdx2,1.0)) {
      setverdict(fail, log2str("Semaphore could not unlock:", v_semaphoreIdx2));
      f_EPTF_Base_stop();
    }
    // unlock the other semaphore:
    f_EPTF_Semaphore_unlock(v_semaphoreIdx);
    log("Semaphore ",v_semaphoreIdx," unlocked.");
    repeat;
  }
}

testcase tc_Semaphore_test() runs on SemaphoreTest_CT {
  f_EPTF_Semaphore_init_CT("Semaphore_Test");
  v_semaphoreIdx := f_EPTF_Semaphore_new();
  v_semaphoreIdx2 := f_EPTF_Semaphore_new();
  t_wait.start;
  var default v_def := activate(as_Semaphore_test_default());
  
  log("Waiting for unlock......");
  if (f_EPTF_Semaphore_waitForUnlock(v_semaphoreIdx)) {
    setverdict(fail, log2str("Semaphore could not unlock:", v_semaphoreIdx2));
  } else {
    setverdict(pass)
  }
  log("blocking finished");
  f_EPTF_Base_cleanup_CT();
}

} // end of module
