function CViewModel_HtmlTester(p_viewmodel, p_options) {
    "use strict";

    /** constructor */
    
    var v_editedText = "";
    var v_html = "";
    
    this.setSelectionToControl = function() {};
    this.setReponseDataPath = function() {};
    this.setBinder = function() {};
    
    this.getTextData = function(callback) {
        callback(v_editedText);
    };
    
    this.setTextData = function(text) {
        v_editedText = text;
    };
    
    this.getListWithElementInfo = function() {
        return {
            "values": [{
                "element": "Show result",
                "val": "0"
            }]
        }
    };
    
    this.setValue = function() {
        v_html = v_editedText;
    };
    
    this.getHtml = function() {
        return v_html;
    };
}

CViewModel_HtmlTester.getHelp = function() {
    return "A small viewmodel that allows editing a html file and showing the results.";
};

CViewModel_HtmlTester.providesInterface = function() {
    return ["setTextData", "getTextData", "getHtml", "setValue", "getListWithElementInfo"];
};

//# sourceURL=CustomizableContent\CustomizableApp\ViewModels\ViewModel_HtmlTester.js