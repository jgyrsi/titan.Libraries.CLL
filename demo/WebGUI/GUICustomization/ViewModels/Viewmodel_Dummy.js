function CViewModel_Dummy(p_viewmodel, p_options) {
    "use strict";

    /** constructor */
    
    var v_viewmodel = p_viewmodel;
    var v_options = p_options;
    
    var v_binder;
    var v_dataPaths = [];
    var v_selections = [];

    /** public functions - interface for parent */ 

    this.setSelectionToControl = function(p_selection) {
        v_selections.push(p_selection);
    };
    
    this.setReponseDataPath = function(p_index, p_path) {
        v_dataPaths[p_index] = p_path;
    };
    
    this.setBinder = function(p_binder) {
        v_binder = p_binder;
    };
	
	/** public functions - interface for views */ 
    
    this.select = function(p_index) {
        for (var i = 0; i < v_selections.length; ++i) {
            v_selections[i].selection[0] = p_index;
        }
        v_binder.notifyChange();
    };
    
    this.getHeader = function() {
        return ["alma", "korte", "eper"];
    };

    this.getName = function() {
        return "ALMA";
    };

    this.getTable = function() {
        var tableData = [[1,2,3], [4,5,6], [7,8,9]];
        return {
            "table": tableData,
            "selection": v_selections[0] != undefined ? v_selections[0].selection : undefined
        };
    };

    this.setValue = function() {
        alert("Value set");
    };
    
    this.getList = function() {
        return [[1, [2,3]]];
    };
    
    this.getListWithElementInfo = function() {
        return [{
            "element": "alma",
            "val": "1",
            "children": [{
                "element": "korte",
                "val": 2
            },
            {
                "element": "eper",
                "val": 3
            }]
        }];
    }
}

CViewModel_Dummy.getHelp = function() {
    return "A dummy viewmodel";
};

CViewModel_Dummy.providesInterface = function() {
    return ["select", "getName", "getHeader", "getTable", "setValue", "getList", "getListWithElementInfo"];
};

CViewModel_Dummy.getCustomDataSchema = function() {
    return {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CViewModel_Dummy",
        "type": "object",
        "properties": {
            "name": {
                "description": "The title",
                "type": "string"
            },
            "header": {
                "description": "The header",
                "type": "array",
                "format": "table",
                "items": {
                    "type": "string",
                    "title": "heading"
                }
            }
        },
        "additionalProperties": false
    };
}

//# sourceURL=CustomizableContent\CustomizableApp\ViewModels\ViewModel_Dummy.js