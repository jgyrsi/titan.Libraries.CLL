///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_WebGUI_Demo_Functions
//
// Purpose:
//   This module contains functions for testing EPTF DsRestAPI.
//
// Module Parameters:
//    -
//  Module depends on:
//    <EPTF_WebGUI_Demo_Definitions>
//    <EPTF_CLL_DsRestAPI_Definitions>
//    <EPTF_CLL_DsRestAPI_Functions>
//    <EPTF_CLL_Base_Functions>
//    <EPTF_CLL_Common_Definitions>
//
// Current Owner:
//    Tamas Kis (ekistam)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////
module EPTF_WebGUI_Demo_Functions {
  //=========================================================================
  // Import Part
  //=========================================================================
  import from EPTF_WebGUI_Demo_Definitions all;
  import from EPTF_CLL_DsRestAPI_Functions all;
  import from EPTF_CLL_Base_Functions all;
  import from EPTF_CLL_ExecCtrl_Definitions all;
  import from EPTF_CLL_LGenBase_Functions all;
  import from EPTF_CLL_LGenBase_ConfigFunctions all;
  import from EPTF_CLL_ExecCtrlClient_Functions all;
  import from EPTF_CLL_HostAdminServer_Definitions all;
  import from EPTF_CLL_HostAdminServer_Functions all;
  import from EPTF_CLL_StatManager_Functions all;
  import from EPTF_CLL_DsRestAPI_Timeline_Functions all;
  
  //=========================================================================
  // Functions
  //=========================================================================
  function f_EPTF_WebGUI_Demo_init_CT(in charstring pl_selfName) runs on WebGUI_Demo_CT {
    f_EPTF_Base_init_CT(pl_selfName);
    f_EPTF_Base_registerCleanup(refers(f_EPTF_WebGUI_Demo_cleanup_CT));
    f_EPTF_DsRestAPI_init_CT(pl_selfName);
    f_EPTF_StatManager_init_CT(pl_selfName);
  }
  
  private function f_EPTF_WebGUI_Demo_cleanup_CT() runs on WebGUI_Demo_CT {}
  
  function f_EPTF_WebGUI_Demo_checkReady(in charstring pl_source, in charstring pl_ptcName) runs on WebGUI_Demo_CT {
    if (pl_source == "ExecCtrl") {
      v_execCtrlReady := true;
    }
    
    if (pl_source == "HostAdminServer") {
      v_HostAdminServerReady := true;
    }
    
    if (v_execCtrlReady and v_HostAdminServerReady) {
      timer t := 3.0;
      t.start;
      t.timeout;
      f_EPTF_DsRestAPI_Timeline_start();
    }
  }
  
  function f_EPTF_ExecCtrl_CLITest_defaultLGenBehaviour(in charstring pl_name, in EPTF_ExecCtrl_CT pl_execCtrlCompRef) runs on EPTF_ExecCtrlClient_CT {
    f_EPTF_LGenBase_init(pl_name);
    var integer vl_Ret := f_EPTF_LGenBase_declareBehaviorType(cg_EPTF_ExecCtrl_Test_defaultBehaviorName, -1, null, null, null);
    vl_Ret := f_EPTF_LGenBase_declareEntityType(cg_EPTF_ExecCtrl_Test_defaultETypeName, {cg_EPTF_ExecCtrl_Test_defaultBehaviorName});
    vl_Ret := f_EPTF_LGenBase_declareFSMTable(cg_EPTF_ExecCtrl_Test_defaultFSM);
    f_EPTF_ExecCtrlClient_init_CT(pl_name, pl_execCtrlCompRef);
    f_EPTF_Base_wait4Shutdown();
  }
  
  function f_ExecCtrl_Test_createDefaultLGen(in charstring pl_hostname, in charstring pl_componentName) runs on EPTF_ExecCtrl_CT return EPTF_ExecCtrlClient_CT {
    pl_hostname := pl_componentName;
    var EPTF_ExecCtrlClient_CT vl_lgen := EPTF_ExecCtrlClient_CT.create;
    vl_lgen.start( f_EPTF_ExecCtrl_CLITest_defaultLGenBehaviour("ExecCtrl_DefaultLGen_"&log2str(vl_lgen),self) );
    return vl_lgen;
  }

  function f_EPTF_HostDminServerBehaviour() runs on EPTF_HostAdminServer_CT {
    f_EPTF_HostAdminServer_init_CT("MTC", mtc);
    f_EPTF_Base_wait4Shutdown();
  }
  
} //module
