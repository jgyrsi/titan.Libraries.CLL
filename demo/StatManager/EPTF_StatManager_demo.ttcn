///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
module EPTF_StatManager_Test_Testcases {

import from EPTF_CLL_StatManager_Definitions all;
import from EPTF_CLL_StatManager_Functions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_UIHandler_WidgetFunctions all;
import from EPTF_CLL_Variable_Functions all;
import from EPTF_CLL_DataSourceClient_Functions all;
import from EPTF_CLL_DataSource_Functions all;
import from EPTF_CLL_UIHandler_Definitions all;
import from EPTF_CLL_DataSource_Definitions all;

const charstring c_StatManager_Test_DataSource_sourceId := "StatManagerTest";
const charstring c_StatManager_Test_dataElementAbsSourceVar := "absSourceVar";
const charstring c_StatManager_Test_dataElementSourceVar := "sourceVar";
const charstring c_StatManager_Test_dataElementRefVar := "refVar";
const charstring c_StatManager_Test_dataElementButtonPress := "btnPress";

type component EPTF_StatManager_Test_CT extends EPTF_StatManager_CT, EPTF_UIHandler_CT 
{
  var boolean v_ready:=false;
}

public function f_EPTF_StatManager_Test_DSProcessData(
  out charstring pl_dataVarName,
  in charstring pl_source,
  in charstring pl_ptcName,
  in charstring pl_element,
  in EPTF_DataSource_Params pl_params)
runs on EPTF_StatManager_Test_CT return integer
{
  select(pl_element){
  	case(c_StatManager_Test_dataElementAbsSourceVar){
  	  pl_dataVarName := c_StatManager_Test_dataElementAbsSourceVar;
  	  return 0;
  	}
  	case(c_StatManager_Test_dataElementSourceVar){
  	  pl_dataVarName := c_StatManager_Test_dataElementSourceVar;
  	  return 0;
  	}
  	case(c_StatManager_Test_dataElementRefVar){
  	  pl_dataVarName := c_StatManager_Test_dataElementRefVar;
  	  return 0;	  
  	}
  	case(c_StatManager_Test_dataElementButtonPress){
  	  pl_dataVarName := c_StatManager_Test_dataElementButtonPress;
  	  return 0;
  	}
  }
  return -1;
}

function f_EPTF_StatManager_Test_checkReady(
  in charstring pl_source,
  in charstring pl_ptcName) runs on EPTF_StatManager_Test_CT{
    if(pl_source=="StatManagerTest"){
    	v_ready:=true;    
    }
  }

function f_EPTF_StatManager_Test_exitBtnPostProc(in integer pl_idx, in EPTF_IntegerList pl_argList)runs on EPTF_StatManager_Test_CT{
  f_EPTF_Base_stop(none);  
}

testcase tc_EPTF_StatManager_Test_base()runs on EPTF_StatManager_Test_CT{
  f_EPTF_StatManager_init_CT("StatManagerTest");
  f_EPTF_UIHandler_init_CT("StatManagerTest");
  v_ready:=false;

  var integer vl_idx;
  f_EPTF_Var_newFloat(c_StatManager_Test_dataElementSourceVar, 0.0, vl_idx);  
  f_EPTF_DataSource_registerReadyCallback(refers(f_EPTF_StatManager_Test_checkReady));
  f_EPTF_DataSourceClient_registerData(c_StatManager_Test_DataSource_sourceId, "StatManagerTest", refers(f_EPTF_StatManager_Test_DSProcessData), self);
  f_EPTF_DataSourceClient_sendReady(c_StatManager_Test_DataSource_sourceId, "StatManagerTest",  self);  
  const charstring c_gui := "
  <Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>
  	<window height='300.000000' id='Window' orientation='vertical' title='tc_EPTF_StatManager_Test_base' width='800.000000'>
  		<hbox flex='1.000000' orientation='horizontal'>
          <tree flex='1.0' hidecolumnpicker='true' id='myTable'>
            <treecols>
              <treecol label='Status' widgetType='statusLEDWithText'/>
              <treecol label='Value' widgetType='string'/>
            </treecols>
          	<treechildren>
          		<treeitem>
          			<treerow>
     					<externalvalue id='srcVar' element='sourceVar' source='StatManagerTest'>
            				<treecell>
            					<externaldata element='LEDlimit' source='StatManager' ptcname='StatManagerTest'>
            						<params>
            							<dataparam name='VarId' value='%srcVar::ref%'/>
            							<dataparam name='redLimit' value='25.0'/>
            							<dataparam name='yellowLimit' value='50.0'/>
            							<dataparam name='greenLimit' value='75.0'/>            							
            							<dataparam name='defaultColor' value='black'/>
            							<dataparam name='enableValueInLEDText' value='yes'/>
            						</params>
            					</externaldata>
            				</treecell>
            				<treecell label='value0'/>
            			</externalvalue>
          			</treerow>
          		</treeitem>
          	</treechildren>
          </tree>
        </hbox>
  	</window>
  </Widgets>";
f_EPTF_UIHandler_clearGUI();
timer t_wait := 0.0;
t_wait.start;
alt{
  [v_ready]t_wait.timeout{}
}
f_EPTF_UIHandler_createGUI(c_gui);
t_wait.start(2.0); t_wait.timeout;
f_EPTF_Var_adjustContent(vl_idx, {floatVal:=26.0});
t_wait.start(2.0); t_wait.timeout;
f_EPTF_Var_adjustContent(vl_idx, {floatVal:=52.0});
t_wait.start(2.0); t_wait.timeout;
f_EPTF_Var_adjustContent(vl_idx, {floatVal:=78.0});
t_wait.start(2.0); t_wait.timeout;
f_EPTF_Base_stop(pass);
}

testcase tc_EPTF_StatManager_demo()runs on EPTF_StatManager_Test_CT{
  f_EPTF_StatManager_init_CT("StatManagerTest");
  f_EPTF_UIHandler_init_CT("StatManagerTest");
  v_ready:=false;

  var integer vl_absSource, vl_source, vl_reference, vl_btnPrs;
  f_EPTF_Var_newFloat(c_StatManager_Test_dataElementAbsSourceVar, 0.0, vl_absSource);
  f_EPTF_Var_newFloat(c_StatManager_Test_dataElementSourceVar, 0.0, vl_source);  
  f_EPTF_Var_newFloat(c_StatManager_Test_dataElementRefVar, 100.0, vl_reference); 
  f_EPTF_Var_newInt(c_StatManager_Test_dataElementButtonPress, 0, vl_btnPrs);
  f_EPTF_Var_addPostProcFn(vl_btnPrs, {refers(f_EPTF_StatManager_Test_exitBtnPostProc), {}});
  f_EPTF_Var_setSubsCanAdjust(vl_absSource, true);
  f_EPTF_Var_setSubsCanAdjust(vl_source, true);
  f_EPTF_Var_setSubsCanAdjust(vl_reference, true);
  f_EPTF_DataSource_registerReadyCallback(refers(f_EPTF_StatManager_Test_checkReady));
  f_EPTF_DataSourceClient_registerData(c_StatManager_Test_DataSource_sourceId, "StatManagerTest", refers(f_EPTF_StatManager_Test_DSProcessData), self);
  f_EPTF_DataSourceClient_sendReady(c_StatManager_Test_DataSource_sourceId, "StatManagerTest",  self);  
  const charstring c_gui := "
  <Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>
  	<window height='300.000000' id='Window' orientation='vertical' title='tc_EPTF_StatManager_Test_base' width='800.000000'>
  		    <image id='image_stop'
            imagedata='iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAYAAADEtGw7AAAABmJLR0QA/wD/AP+gvaeTAAAACXBI
                      WXMAAAsLAAALCwFtBIS3AAAAB3RJTUUH0woJEwkMqdJ5QQAAA+9JREFUeNqdlduPU0Ucxz8z59Ke
                      bk/pXrgUtSyi2ZgVgq2Y8EKMiRok+oAJUUz0aQm8mfjgk0+a8BcYHk5CfONBTDQmQoSYGEW8LN2s
                      iBICyC1Lt7vttqe0p+05Z8aHssuCgIvf5JvJby6f+eU3mRnBQ1Q4MG2BXg0qh9Yp0IAO0KpsKb/y
                      i/dy70FrxUOg24E9oLciyCOsAQBUt42OrwPTQkVHC/rwKc/z4v8EFw5MbwLxPoa1Nz2Qyq4byci1
                      w0nGRpo4VsylWporFcX83JyqNYJARcEpS9/6ZIs+ctrzvOi+4MKB6Q0gDmFndm590uG1bS6bn9Cs
                      yVoYsj9VAwvNiMvlkGOlDj+cXaAX+H8lVO3DZ/XRY4tw455MP01nMq++sX2VePdFh6dzBgnLIIyh
                      F+klG1IwkjHZvMFi7aDF3xU94nfE81W9sbSz6NwolUp6CZzbtv8gtvvWrhey4vWihW1JepGmG97j
                      nqIbKrqRRmvN+iELN2WJP2/Ew52ettYzdaJYLIain+2Z7cJIfrPlqaHs3h0OriP5dipgJXrlOYcw
                      0nz1a8D3U5WG0Z15bzNfHDcL+ycttN6THkhmCptsYqVotDQAH7295qHQj49UaHcVnZ5iLBfz+6WE
                      W6umd1fC0Z9NYDWwNZNZJbPJCL8leRR99mMCAD9I0SQtFfVxX+TzJpBDyPyIKwh6iqCn6Oe7sg2E
                      6nGxlukHpgnCyGHYeRNIIRMDKSuk3lJIAbYpVgS9NJ+8exMhQCYcoXDNxU6/YzHb0BgSXEesuBQX
                      axnE4nS9eDmUNEEHqG57tuXQipNLgxrxSLXuwzXEnY7QUddEqzJwvdFWG2Ph8H9kGv0TuRV0QccV
                      qVplaSm/AkzTrSnL0NjLvFIlzRjbjBG9qjLpXMjqqzcFQHHf6R3CTn+Zy60bHHTtRyqCISFSsNDs
                      UZ6ZaTqd8x+MyZNfmwAFffjUmWji82Z9bt/o8CAJU/SfJ33PU6XvNMsPrBtprtUXUL3Gd2Py5E9A
                      0wAolUr68cI7l7shzygVb3hs2BCphCBh6b7Nu9ukfScWhFy45qtqrTE5HJ87lBUz55bAALsKVrWq
                      85ONwBz329Gom7IYck1sS2Kb4l+2DJhvhJy93KA850+ujqYO5mXpN2De87x4CVwqlfTOYqpW1aNT
                      jcBIzTXC0WY7spNmJAYcE1NKpACtY6oLLf640lbnrzVv1ev+8eH43KHb0IrneeF9f5CJiQkJDJzl
                      zZcimd6tsMalFDnXMRyAZhB3VKwqJp0LyXj2xO2azgL1B/4gy+ACsCvxaNYX+TyGnRfgCpQUOupK
      				  1Spn9dWbq4z5GtAEAs/z1HLGP0K45L8u7KzsAAAAAElFTkSuQmCC' />
  		<hbox flex='1.000000' orientation='horizontal'>
  		  <hbox flex='1.000000' orientation='vertical'>
  		    <tree flex='1.0' hidecolumnpicker='true' id='myTable1'>
              <treecols>
                <treecol label='Absolute' widgetType='statusLEDWithText'/>                
                <treecol label='Data' widgetType='floatField'/>                
              </treecols>
            	<treechildren>
            		<treeitem>
            			<treerow>
            			  <externalvalue id='absSrcVar' element='absSourceVar' source='StatManagerTest'>
            			    <treecell>
              					<externaldata element='LEDlimit' source='StatManager' ptcname='StatManagerTest'>
              						<params>
              							<dataparam name='VarId' value='%absSrcVar::ref%'/>
              							<dataparam name='redLimit' value='25.0'/>
              							<dataparam name='yellowLimit' value='50.0'/>
              							<dataparam name='greenLimit' value='75.0'/>
              							<dataparam name='defaultColor' value='black'/>
              							<dataparam name='enableValueInLEDText' value='yes'/>
              						</params>
              					</externaldata>
              				</treecell>
              				<treecell label='%absSourceVar%'>
                  				<externaldata element='absSourceVar' source='StatManagerTest'/>                						
              				</treecell>	
              			  </externalvalue>
            			</treerow>
            		</treeitem>
            	</treechildren>          
            </tree>
            <tree flex='1.0' hidecolumnpicker='true' id='myTable2'>
              <treecols>
                <treecol label='Reference' widgetType='statusLEDWithText'/>
                <treecol label='Data' widgetType='floatField'/>
                <treecol label='Reference data' widgetType='floatField'/>
              </treecols>
            	<treechildren>
            		<treeitem>
            			<treerow>
       					<externalvalue id='srcVar' element='sourceVar' source='StatManagerTest'>
       					<externalvalue id='refVar' element='refVar' source='StatManagerTest'>              				
              				<treecell>
              					<externaldata element='LEDlimit' source='StatManager' ptcname='StatManagerTest'>
              						<params>
              							<dataparam name='VarId' value='%srcVar::ref%'/>
              							<dataparam name='refVarId' value='%refVar::ref%'/>
              							<dataparam name='redLimit' value='-75.0'/>
              							<dataparam name='yellowLimit' value='-50.0'/>
              							<dataparam name='greenLimit' value='-25.0'/>
              							<dataparam name='yellowLimit' value='25.0'/>
              							<dataparam name='redLimit' value='50.0'/>
              							<dataparam name='defaultColor' value='black'/>
              							<dataparam name='enableValueInLEDText' value='yes'/>
              						</params>
              					</externaldata>
              				</treecell>
              				<treecell label='%srcVar%'>
                  				<externaldata element='sourceVar' source='StatManagerTest'/>                						
              				</treecell>	
              				<treecell label='%refVar%'>
              					<externaldata element='refVar' source='StatManagerTest'/>                						
              				</treecell>	
              			</externalvalue>
              			</externalvalue>
            			</treerow>
            		</treeitem>
            	</treechildren>          
            </tree>            
          </hbox>
          <button disabled='false' flex='0.100000' id='ExitButton' imageid='image_stop' label='Exit'>
          	<externaldata element='btnPress' source='StatManagerTest'/>
          </button>
        </hbox>
  	</window>
  </Widgets>";
f_EPTF_UIHandler_clearGUI();
timer t_wait := 0.0;
t_wait.start;
alt{
  [v_ready]t_wait.timeout{}
}
f_EPTF_UIHandler_createGUI(c_gui);

f_EPTF_Base_wait4Shutdown();
}

control {  
  execute(tc_EPTF_StatManager_Test_base());
}

} // end of module
