///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: AppLib_A_Definitions
// 
//  Purpose:
//    This module demonstrates the usage of the LGenBase feature through
//    a basic applib
// 
//  Module Parameters:
//    -
// 
//  Module depends on:
//    <EPTF_CLL_LGenBase_Definitions>
//    <EPTF_CLL_LGenBase_Functions>
//    <AppLib_A_Definitions>
//    <EPTF_CLL_Common_Definitions>
//    <EPTF_CLL_Scheduler_Definitions>
//    <EPTF_CLL_RBTScheduler_Functions> 
//    <EPTF_CLL_HashMapInt2Int_Functions>
// 
//  Current Owner:
//    ELSZSKU
// 
//  Last Review Date:
//    2008-02-18
// 
// 
///////////////////////////////////////////////////////////
module AppLib_A_Functions {
import from EPTF_CLL_LGenBase_Definitions all;
import from EPTF_CLL_LGenBase_Functions all;
import from EPTF_CLL_LGenBase_StepFunctions all;
import from EPTF_CLL_LGenBase_ConfigFunctions all;
import from EPTF_CLL_LGenBase_TrafficFunctions all;
import from EPTF_CLL_LGenBase_EventHandlingFunctions all;
import from AppLib_A_Definitions all;
import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_Scheduler_Definitions all;
import from EPTF_CLL_RBTScheduler_Functions all; 
import from EPTF_CLL_HashMapInt2Int_Functions all;

function f_EPTF_LGenBase_demoNextStateCalc(
  in integer eIdx,
  in integer fIdx,
  EPTF_IntegerList contextArgs, 
  in EPTF_LGenBase_FsmTableCellReference pl_cell,
  in EPTF_IntegerList pl_stepArgs //supply args to actions
) runs on EPTF_LGenBase_CT return integer {
  return contextArgs[0];
}

//FSM definitions
const integer c_A_fsmState_BOC_idle:=0,
c_A_fsmState_BOC_connecting:=1,
c_A_fsmState_BOC_connected:=2,
c_A_fsmState_BOC_disconnected:=3,
c_A_fsmTimer_BOC_TP:=0,
c_A_fsmTimer_BOC_TH:=1;
function f_A_return_compactFsm_BasicOutgoingCall() 
runs on LGen_A_CT return EPTF_LGenBase_CompactFsmTable 
{
  return {
    name := c_A_fsmName_basicOutgoingCall,
    //             [0]   [1]          [2]         [3]
    stateList := {"idle","connecting","connected","disconnecting"}, 
    //              [0]                           [1]
    timerList := {{"T_Progress",tsp_T_progress},{"T_Hold",tsp_T_Hold}},
    table := {
      {eventToListen := {v_A_myBIdx,c_A_inputIdx_sendFoo,fsm},
        cellRow := {
          //state[0]==idle 
          {{
              {refers(f_A_step_sendFoo),{}}, 
              //{refers(f_A_step_startTP),{}}
              {refers(f_EPTF_LGenBase_step_timerStart),{c_A_fsmTimer_BOC_TP}}
            },omit,1}
          //state[1]==connecting
          ,{omit,omit,omit}
          //state[2]==connected
          ,{omit,omit,omit}
          //state[3]==disconnecting		
          ,{omit,omit,omit}
        }
      },{eventToListen := {v_A_myBIdx,c_A_inputIdx_bar,fsm},
        cellRow := {
          //state=idle
          {omit,omit,omit}
          //state==connecting
          ,{{
              //{refers(f_A_step_cancelTP),{}}, 
              //{refers(f_A_step_startTH),{}}
              {refers(f_EPTF_LGenBase_step_timerCancel),{c_A_fsmTimer_BOC_TP}},
              {refers(f_EPTF_LGenBase_step_timerStart),{c_A_fsmTimer_BOC_TH}}
            },{refers(f_EPTF_LGenBase_demoNextStateCalc),{2}},omit} //note the calculated next state
          //state==connected
          ,{omit,omit,omit}
          //state==disconnecting		
          ,{omit,omit,omit}
        }
      },{eventToListen := {c_EPTF_LGenBase_specialBIdx_timerTimeout,c_A_fsmTimer_BOC_TP,fsm},
        cellRow := {
          //state=idle
          {omit,omit,omit}
          //state==connecting
          ,{{
              {refers(f_A_step_sendFoo),{}}, 
              //{refers(f_A_step_startTP),{}}
              {refers(f_EPTF_LGenBase_step_timerStart),{c_A_fsmTimer_BOC_TP}}
            }, omit,omit}
          //state==connected
          ,{omit,omit,omit}
          //state==disconnecting		
          ,{{
              {refers(f_A_step_sendBye),{}}, 
              //{refers(f_A_step_startTP),{}}
              {refers(f_EPTF_LGenBase_step_timerStart),{c_A_fsmTimer_BOC_TP}}
            },omit, 3}
        }
      },{eventToListen := {c_EPTF_LGenBase_specialBIdx_timerTimeout,c_A_fsmTimer_BOC_TH,fsm},
        cellRow := {
          //state=idle
          {omit,omit,omit}
          //state==connecting
          ,{omit,omit,omit}
          //state==connected
          ,{{
              {refers(f_A_step_sendBye),{}}, 
              //{refers(f_A_step_startTP),{}}
              {refers(f_EPTF_LGenBase_step_timerStart),{c_A_fsmTimer_BOC_TP}}
            },omit, 3}
          //state==disconnecting		
          ,{omit,omit,omit}
        }
      },{eventToListen := {v_A_myBIdx,c_A_inputIdx_byeBye,fsm},
        cellRow := {
          //state=idle
          {omit,omit,omit}
          //state==connecting
          ,{omit,omit,omit}
          //state==connected
          ,{omit,omit,omit}
          //state==disconnecting		
          ,{{
              //{refers(f_A_step_cancelTP),{}},
              {refers(f_EPTF_LGenBase_step_timerCancel),{c_A_fsmTimer_BOC_TP}},
              {refers(f_A_step_generateEntityEventsAboutFsm),{c_A_inputIdx_finishedBOC}}
            },omit, 0}
        }
      }
    }//table
  }//fsm
}
//FSM definitons 
const integer c_A_fsmState_BIC_idle:=0,
c_A_fsmState_BIC_connected:=1;
function f_A_return_compactIndexedFsm_BasicIncomingCall()
runs on LGen_A_CT return EPTF_LGenBase_IndexedCompactFsmTable 
{
  return {
    name := c_A_fsmName_basicIncomingCall,
    stateList := {"idle","connected"},
    timerList := {},
    table := {
      {eventToListen := {v_A_myBIdx,c_A_inputIdx_foo,fsm},
        cellRow := {
          //state=idle
          {
            { {{v_A_myBIdx,c_A_stepIdx_sendBar},{}} },omit, 1}
          //state==connected
          ,{omit,omit,omit}
        }
      },{eventToListen := {v_A_myBIdx,c_A_inputIdx_bye,fsm},
        cellRow := {
          //state=idle
          {omit,omit,omit}
          //state==connected
          ,{
            { {{v_A_myBIdx,c_A_stepIdx_sendByeBye}, {}},
              {{v_A_myBIdx,c_A_stepIdx_generateEntityEventsAboutFsm},{c_A_inputIdx_finishedBIC}}
            },omit,  0}
        }
      }//row
    }//table
  }//fsm
}
const integer c_A_fsmState_waiting4Call:=0;
function f_A_return_compactFsm_waiting4Call() 
runs on LGen_A_CT return EPTF_LGenBase_CompactFsmTable 
{
  return {
    name := c_A_fsmName_waiting4Call,
    //             [0]   
    stateList := {"waiting for calls"}, //note: state==idle is unnecessary
    timerList := {},
    table := {
      {eventToListen := {v_A_myBIdx,c_A_inputIdx_sendFoo,entity},
        cellRow := {
          //state[0]==idle 
          {{{refers(f_A_step_startBOC),{}}}, omit,omit}
        }
      }, 
      {eventToListen := {v_A_myBIdx,c_A_inputIdx_foo,entity},
        cellRow := {
          //state[0]==idle 
          {{{refers(f_A_step_startBIC),{}}}, omit,omit}
        }
      },      
      {eventToListen := {v_A_myBIdx,c_A_inputIdx_finishedBOC,entity},
        cellRow := {
          //state[0]==idle 
          {{{refers(f_A_step_stopBOC),{}}}, omit,omit}
        }
      },      {eventToListen := {v_A_myBIdx,c_A_inputIdx_finishedBIC,entity},
        cellRow := {
          //state[0]==idle 
          {{{refers(f_A_step_stopBIC),{/*no context args*/}}}, omit,omit}
        }
      }        
    }
  }
}

function f_LGen_A_bindEntity2bCtx(
  in integer pl_eIdx) 
runs on LGen_A_CT 
return EPTF_IntegerList{
  log("----- f_LGen_A_bindEntity2bCtx has been called. eIdx: ",pl_eIdx);
  return {pl_eIdx};  
}

function f_LGen_A_init(in integer pl_numCtxs)
runs on LGen_A_CT {
  var integer i;
  v_A_myBIdx := f_EPTF_LGenBase_declareBehaviorType(c_A_behaviorType, c_A_maxEntityCount, null, null, null);
  // should be declared as many as the max supported value by the applib      ^^^^^
  // orig: v_A_myBIdx := f_EPTF_LGenBase_declareBehaviorType(c_A_behaviorType, pl_numCtxs,null,refers(f_LGen_A_bindEntity2bCtx),null //FIXME
  //);
  log(%definitionId,": myBIdx is ", v_A_myBIdx);
  
  for (i:=0;i<pl_numCtxs; i:=i+1) {
    v_A_contexts[i]:={
      oCalls:=0,
      iCalls:=0,
      oCallsHashMapRef:= f_EPTF_int2int_HashMap_New("Outgoing calls of entity#"&int2str(i)),
      iCallsHashMapRef:= f_EPTF_int2int_HashMap_New("Incoming calls of entity#"&int2str(i)),
      fooMessage:= "foo:",
      barMessage:="bar:",
      byeMessage:="bye:", 
      byeByeMessage:="bye-bye:"};
  }
  //declare metadata
  if(//steps
    c_A_stepIdx_sendFoo !=f_EPTF_LGenBase_declareStep(c_A_behaviorType,{c_A_stepName_sendFoo,refers(f_A_step_sendFoo)}) //0
    //or c_A_stepIdx_startTP  !=f_EPTF_LGenBase_declareStep(c_A_behaviorType,{c_A_stepName_startTP, refers(f_A_step_startTP)})//1
    //or c_A_stepIdx_cancelTP !=f_EPTF_LGenBase_declareStep(c_A_behaviorType,{c_A_stepName_cancelTP,refers(f_A_step_cancelTP)})//2
    //or c_A_stepIdx_startTH  !=f_EPTF_LGenBase_declareStep(c_A_behaviorType,{c_A_stepName_startTH,refers(f_A_step_startTH)})//3
    //or c_A_stepIdx_cancelTH !=f_EPTF_LGenBase_declareStep(c_A_behaviorType,{c_A_stepName_cancelTH,refers(f_A_step_cancelTH)})//4
    or c_A_stepIdx_sendBye !=f_EPTF_LGenBase_declareStep(c_A_behaviorType,{c_A_stepName_sendBye,refers(f_A_step_sendBye)})//5
    or c_A_stepIdx_sendBar != f_EPTF_LGenBase_declareStep(c_A_behaviorType,{c_A_stepName_sendBar,refers(f_A_step_sendBar)})//6
    or c_A_stepIdx_sendByeBye !=f_EPTF_LGenBase_declareStep(c_A_behaviorType,{c_A_stepName_sendByeBye,refers(f_A_step_sendByeBye)})//7
    or c_A_stepIdx_startBOC !=f_EPTF_LGenBase_declareStep(c_A_behaviorType,{c_A_stepName_startBOC,refers(f_A_step_startBOC)})//8
    or c_A_stepIdx_stopBOC !=f_EPTF_LGenBase_declareStep(c_A_behaviorType,{c_A_stepName_stopBOC,refers(f_A_step_stopBOC)})//9
    or c_A_stepIdx_startBIC !=f_EPTF_LGenBase_declareStep(c_A_behaviorType,{c_A_stepName_startBIC,refers(f_A_step_startBIC)})//10
    or c_A_stepIdx_stopBIC !=f_EPTF_LGenBase_declareStep(c_A_behaviorType,{c_A_stepName_stopBIC,refers(f_A_step_stopBIC)})//11
    or c_A_stepIdx_generateEntityEventsAboutFsm !=
    f_EPTF_LGenBase_declareStep(c_A_behaviorType,{c_A_stepName_generateEntityEventsAboutFsm,refers(f_A_step_generateEntityEventsAboutFsm)})//12
    
    //inputs
    or c_A_inputIdx_sendFoo != f_EPTF_LGenBase_declareFsmEvent(c_A_behaviorType,c_A_inputName_sendFoo)//0
    or c_A_inputIdx_bar != f_EPTF_LGenBase_declareFsmEvent(c_A_behaviorType,c_A_inputName_bar)//1
    //or c_A_inputIdx_timeoutTP != f_EPTF_LGenBase_declareFsmEvent(c_A_behaviorType,
    //  c_A_inputName_timeoutTP) //2
    //or c_A_inputIdx_timeoutTH != f_EPTF_LGenBase_declareFsmEvent(c_A_behaviorType,c_A_inputName_timeoutTH)//3
    or c_A_inputIdx_byeBye != f_EPTF_LGenBase_declareFsmEvent(c_A_behaviorType,c_A_inputName_byeBye)//4
    or c_A_inputIdx_foo != f_EPTF_LGenBase_declareFsmEvent(c_A_behaviorType,c_A_inputName_foo)//5
    or c_A_inputIdx_bye != f_EPTF_LGenBase_declareFsmEvent(c_A_behaviorType,c_A_inputName_bye)//6
    or c_A_inputIdx_finishedBOC != f_EPTF_LGenBase_declareFsmEvent(c_A_behaviorType,c_A_inputName_finishedBOC)//7
    or c_A_inputIdx_finishedBIC != f_EPTF_LGenBase_declareFsmEvent(c_A_behaviorType,c_A_inputName_finishedBIC)//8
    
    //states
    //or c_A_stateIdx_Idle != f_EPTF_LGenBase_declareFsmState(c_A_behaviorType,c_A_stateName_Idle)//0
    //or c_A_stateIdx_Connecting != f_EPTF_LGenBase_declareFsmState(c_A_behaviorType,c_A_stateName_Connecting)
    //or c_A_stateIdx_Connected!= f_EPTF_LGenBase_declareFsmState(c_A_behaviorType,c_A_stateName_Connected)
    //or c_A_stateIdx_Disconnecting != f_EPTF_LGenBase_declareFsmState(c_A_behaviorType,c_A_stateName_Disconnecting)
    
    //fsms
    or c_A_fsmIdx_basicOutgoingCall != f_EPTF_LGenBase_declareCompactFsmTable(
      f_A_return_compactFsm_BasicOutgoingCall()) 
    or c_A_fsmIdx_basicIncomingCall != f_EPTF_LGenBase_declareIndexedCompactFsmTable(
      f_A_return_compactIndexedFsm_BasicIncomingCall()) 
    or c_A_fsmIdx_waiting4Call != f_EPTF_LGenBase_declareCompactFsmTable(
      f_A_return_compactFsm_waiting4Call()) 
    //or c_A_fsmIdx_indexedBasicCall != f_EPTF_LGenBase_declareIndexedFsmTable(
    //v_A_myBIdx,f_A_return_fsm_IndexedBasicCall()) 
  ) {
    //fixme
    f_EPTF_LGenBase_log();
    log("error"); mtc.stop
  }
}

function f_A_debugStep(
  in charstring pl_name, 
  in EPTF_LGenBase_TestStepArgs pl_ptr  
) runs on LGen_A_CT {
  var integer e:=pl_ptr.eIdx;
  //  if (ispresent(pl_ptr.fsm)) {
  //  var integer b:= pl_ptr.fsm.behaviorIdx,
  //  t:=pl_ptr.fsm.tableIdx, 
  //  r:=pl_ptr.fsm.tableRowIdx, 
  //  c:=pl_ptr.fsm.tableColIdx;
  //  log (pl_name, 
  //    ": entity:=",v_LGenBase_entities[e],
  //    ", declared FSM[",b,"][",t,"]:=",v_LGenBase_fsmTables[b][t].name,
  //    ", table row index:=", r,
  //    ", table col. index:=", c,
  //    ", table cell:=", 
  //    v_LGenBase_fsmTables[b][t].table[r][c],
  //    ", behavior context[",
  //    v_LGenBase_entities[e].bCtxList[c_A_behaviorType],"]:={",
  //    v_A_contexts[v_LGenBase_entities[e].bCtxList[c_A_behaviorType][0]],
  //    "}" )
  //  } else {
  log (pl_name, ": args:=", pl_ptr);  
  //  }
}
function f_A_step_startBOC(in EPTF_LGenBase_TestStepArgs pl_ptr) 
runs on LGen_A_CT {
  f_A_debugStep(%definitionId, pl_ptr);
  var integer fCtx, eCtx:=pl_ptr.eIdx ;
  var integer myCtx := f_EPTF_LGenBase_getBehaviorCtxItem(eCtx,v_A_myBIdx,0);
  if (c_EPTF_Common_debugSwitch) {
    log(%definitionId,": instructed to start BOC with args:=",pl_ptr,
      ", will create new FSM ctx for entity:", f_EPTF_LGenBase_getEntityName(eCtx))
  }
  fCtx := 
  f_EPTF_LGenBase_activateFsm(pl_ptr.eIdx, c_A_fsmIdx_basicOutgoingCall ,c_A_fsmState_BOC_connecting, -1);
  v_A_msg4entity:=eCtx;
  v_A_msg4call:=v_A_contexts[myCtx].oCalls;
  v_A_contexts[myCtx].oCalls:=v_A_contexts[myCtx].oCalls+1;
  f_EPTF_LGenBase_setAppDataItemOfFsmCtx(eCtx, fCtx,  c_A_fsmCtx_callDataRow, c_A_fsmCtx_callData_Id, v_A_msg4call)
  
  f_EPTF_int2int_HashMap_Insert(v_A_contexts[myCtx].oCallsHashMapRef,v_A_msg4call,fCtx)
  var EPTF_LGenBase_TestStepArgs vl_args := c_EPTF_LGenBase_emptyTestStepArgs
  vl_args.eIdx := eCtx
  vl_args.refContext.fCtxIdx := fCtx
  vl_args.stepArgs := pl_ptr.stepArgs
  f_A_step_sendFoo(vl_args);
  vl_args.refContext.fRefArgs := {c_A_fsmTimer_BOC_TP}
  vl_args.stepArgs := {}
  //f_A_step_startTP({eCtx,{fCtx,{}},pl_ptr.stepArgs});
  f_EPTF_LGenBase_step_timerStart(vl_args);
}
const integer c_A_stepArgsIdx_fsmCtx2stop:=0;
function f_A_step_stopBOC(in EPTF_LGenBase_TestStepArgs pl_ptr) 
runs on LGen_A_CT {
  f_A_debugStep(%definitionId, pl_ptr);
  var integer fCtx:=pl_ptr.stepArgs[c_A_stepArgsIdx_fsmCtx2stop], eCtx:=pl_ptr.eIdx ;
  var integer myCtx := f_EPTF_LGenBase_getBehaviorCtxItem(eCtx,v_A_myBIdx,0); 
  
  f_EPTF_int2int_HashMap_Erase(v_A_contexts[myCtx].oCallsHashMapRef,v_A_msg4call);
  f_EPTF_LGenBase_deactivateFsm(pl_ptr.eIdx, fCtx);
}

function f_A_step_startBIC(in EPTF_LGenBase_TestStepArgs pl_ptr) 
runs on LGen_A_CT { 
  f_A_debugStep(%definitionId, pl_ptr);
  var integer fCtx ,eCtx:=pl_ptr.eIdx ;
  var integer myCtx := f_EPTF_LGenBase_getBehaviorCtxItem(eCtx,v_A_myBIdx,0);
  if (c_EPTF_Common_debugSwitch) {
    log(%definitionId,": instructed to start BIC, will create new FSM ctx for entity:", f_EPTF_LGenBase_getEntityName(eCtx))
  }
  fCtx :=     
  f_EPTF_LGenBase_activateFsm(eCtx, c_A_fsmIdx_basicIncomingCall ,c_A_fsmState_BIC_connected, -1);
  
  f_EPTF_LGenBase_setAppDataItemOfFsmCtx(eCtx, fCtx,  c_A_fsmCtx_callDataRow, c_A_fsmCtx_callData_Id, v_A_msg4call)
  
  f_EPTF_int2int_HashMap_Insert(v_A_contexts[myCtx].iCallsHashMapRef,v_A_msg4call,fCtx)
  v_A_contexts[myCtx].iCalls := v_A_contexts[myCtx].iCalls+1;  
  var EPTF_LGenBase_TestStepArgs vl_args := c_EPTF_LGenBase_emptyTestStepArgs
  vl_args.eIdx := eCtx
  vl_args.refContext.fCtxIdx := fCtx
  f_A_step_sendBar(vl_args);
}
function f_A_step_stopBIC(in EPTF_LGenBase_TestStepArgs pl_ptr) 
runs on LGen_A_CT { 
  f_A_debugStep(%definitionId, pl_ptr);
  var integer fCtx:=pl_ptr.stepArgs[c_A_stepArgsIdx_fsmCtx2stop] ,eCtx:=pl_ptr.eIdx ;
  var integer myCtx := f_EPTF_LGenBase_getBehaviorCtxItem(eCtx,v_A_myBIdx,0); 
  f_EPTF_LGenBase_deactivateFsm(eCtx, fCtx);
  
  f_EPTF_int2int_HashMap_Erase(v_A_contexts[myCtx].iCallsHashMapRef,v_A_msg4call)  
}


function f_A_step_sendFoo(in EPTF_LGenBase_TestStepArgs pl_ptr) 
runs on LGen_A_CT { 
  f_A_debugStep(%definitionId, pl_ptr);
  var integer fCtx:=pl_ptr.refContext.fCtxIdx, eCtx:=pl_ptr.eIdx ;  
  var integer myCtx := f_EPTF_LGenBase_getBehaviorCtxItem(eCtx,v_A_myBIdx,0); 
  port_A.send(v_A_contexts[myCtx].fooMessage & int2str(eCtx)& ",CallId:"&int2str(
      f_EPTF_LGenBase_getAppDataItemOfFsmCtx(eCtx,fCtx,c_A_fsmCtx_callDataRow,c_A_fsmCtx_callData_Id)));
  
  //  f_EPTF_LGenBase_testStepIsOver(pl_ptr)
}
function f_A_step_sendBar(in EPTF_LGenBase_TestStepArgs pl_ptr) 
runs on LGen_A_CT { 
  var integer fCtx:=pl_ptr.refContext.fCtxIdx, eCtx:=pl_ptr.eIdx ;
  var integer myCtx := f_EPTF_LGenBase_getBehaviorCtxItem(eCtx,v_A_myBIdx,0); 
  f_A_debugStep(%definitionId, pl_ptr);
  port_A.send(v_A_contexts[myCtx].barMessage & int2str(eCtx)& ",CallId:"&int2str(
      f_EPTF_LGenBase_getAppDataItemOfFsmCtx(eCtx,fCtx,c_A_fsmCtx_callDataRow,c_A_fsmCtx_callData_Id)));
  //  f_EPTF_LGenBase_testStepIsOver(pl_ptr)
}
function f_A_step_sendBye(in EPTF_LGenBase_TestStepArgs pl_ptr) 
runs on LGen_A_CT { 
  var integer fCtx:=pl_ptr.refContext.fCtxIdx, eCtx:=pl_ptr.eIdx ;
  var integer myCtx := f_EPTF_LGenBase_getBehaviorCtxItem(eCtx,v_A_myBIdx,0); 
  f_A_debugStep(%definitionId, pl_ptr);
  port_A.send(v_A_contexts[myCtx].byeMessage & int2str(eCtx)& ",CallId:"&int2str(
      f_EPTF_LGenBase_getAppDataItemOfFsmCtx(eCtx,fCtx,c_A_fsmCtx_callDataRow,c_A_fsmCtx_callData_Id)));
  //  f_EPTF_LGenBase_testStepIsOver(pl_ptr)
}
function f_A_step_sendByeBye(in EPTF_LGenBase_TestStepArgs pl_ptr) 
runs on LGen_A_CT { 
  var integer fCtx:=pl_ptr.refContext.fCtxIdx, eCtx:=pl_ptr.eIdx ;
  var integer myCtx := f_EPTF_LGenBase_getBehaviorCtxItem(eCtx,v_A_myBIdx,0); 
  f_A_debugStep(%definitionId, pl_ptr);
  port_A.send(v_A_contexts[myCtx].byeByeMessage & int2str(eCtx)& ",CallId:"&int2str(
      f_EPTF_LGenBase_getAppDataItemOfFsmCtx(eCtx,fCtx,c_A_fsmCtx_callDataRow,c_A_fsmCtx_callData_Id)));
  //  f_EPTF_LGenBase_testStepIsOver(pl_ptr)
}
function f_A_step_generateEntityEventsAboutFsm(in EPTF_LGenBase_TestStepArgs pl_ptr) 
runs on LGen_A_CT { 
  f_A_debugStep(%definitionId, pl_ptr);
  for (var integer i:=0; i<sizeof(pl_ptr.refContext.fRefArgs); i:=i+1) {
    f_EPTF_LGenBase_dispatchEvent({
        {v_A_myBIdx,pl_ptr.refContext.fRefArgs[i], {pl_ptr.eIdx,omit},omit},
        {pl_ptr.refContext.fCtxIdx}
      })
  }
}


//Args position for event handlers
const integer 
c_A_action_bIdx:=0, //behavior type idx
c_A_action_iIdx:=1, //input Idx
c_A_action_eIdx:=2, // entity Idx
c_A_action_fCtxIdx:=3; // fsm Context Idx

//needs only input Idx
function f_A_ActionHandler4Generic(
  in EPTF_ScheduledAction pl_action, in integer pl_eventIndex) runs on LGen_A_CT
return boolean{
  log(%definitionId,": action:=", pl_action,", eventIndex:", pl_eventIndex);
  var integer bIdx:= pl_action.actionId[c_A_action_bIdx],iIdx := pl_action.actionId[c_A_action_iIdx];
  f_EPTF_LGenBase_dispatchEvent({{bIdx,iIdx,omit,omit},{}});
  return true
}
//needs input Idx, entity Idx, behavior Context Idx
function f_A_ActionHandler4Entity(
  in EPTF_ScheduledAction pl_action, in integer pl_eventIndex) runs on LGen_A_CT
return boolean{
  log(%definitionId,": action:=", pl_action,", eventIndex:", pl_eventIndex);
  var integer bIdx:= pl_action.actionId[c_A_action_bIdx],iIdx := pl_action.actionId[c_A_action_iIdx],
  eIdx := pl_action.actionId[c_A_action_eIdx];
  f_EPTF_LGenBase_dispatchEvent({{bIdx,iIdx,{eIdx,omit},omit},{}});
  return true;
}
//needs input Idx, entity Idx, behavior Context Idx
function f_A_ActionHandler4Fsm(
  in EPTF_ScheduledAction pl_action, in integer pl_eventIndex) runs on LGen_A_CT
return boolean{
  log(%definitionId,": action:=", pl_action,", eventIndex:", pl_eventIndex);
  var integer bIdx:= pl_action.actionId[c_A_action_bIdx],iIdx := pl_action.actionId[c_A_action_iIdx],
  eIdx := pl_action.actionId[c_A_action_eIdx], fIdx:=pl_action.actionId[c_A_action_fCtxIdx];
  
  f_EPTF_LGenBase_dispatchEvent({{bIdx,iIdx,{eIdx,fIdx},omit},{}}); 
  return true;
}


altstep as_A_catchAll() runs on LGen_A_CT {
  [] port_A.receive(charstring:?) -> value v_A_msg {
    f_EPTF_SchedulerComp_refreshSnapshotTime(); //update scheduler data
    log (%definitionId," has received ", v_A_msg, " ignoring...")
  }
}
altstep as_A_input_PrefixPlusAddress4Fsm(
  in template charstring pl_pt,
  in charstring pl_pr,
  in integer pl_iIdx,
  in A_CallTypes pl_type) 
runs on LGen_A_CT {
  var integer fIdx, hashMapRef; 
  [] port_A.receive(pl_pt) -> value v_A_msg {
    f_EPTF_SchedulerComp_refreshSnapshotTime(); //update scheduler data
    log (%definitionId," has received ", v_A_msg);
    v_A_msg4entity:=str2int(regexp(v_A_msg,pl_pr, 0));
    var integer myCtx := f_EPTF_LGenBase_getBehaviorCtxItem(v_A_msg4entity,v_A_myBIdx,0); 
    v_A_msg4call:=str2int(regexp(v_A_msg,pl_pr, 1));
    select (pl_type) {
      case(BOC){hashMapRef:= v_A_contexts[myCtx].oCallsHashMapRef}
      case(BIC){hashMapRef:= v_A_contexts[myCtx].iCallsHashMapRef}
    }
    
    if (0 > f_EPTF_LGenBase_getEGrpOfEntity(v_A_msg4entity)
      or not f_EPTF_int2int_HashMap_Find(hashMapRef,v_A_msg4call,fIdx)
      or v_A_msg4call<0 
      //FIXME or v_A_msg4call>=sizeof(v_LGenBase_entities[v_A_msg4entity].fsmCtxList)
      ) {
      log(%definitionId,": invalid eIDx, or fIdx calculated from msg. ", v_A_msg, ", with call Id ", v_A_msg4call,
        ",calculated entity is ", v_A_msg4call,",calculated FSM ctx is is ", fIdx)
    } else {
      f_EPTF_LGenBase_dispatchEvent({{v_A_myBIdx,pl_iIdx,{v_A_msg4entity,fIdx},omit},{}});
    }
  }
}
altstep as_A_input_PrefixPlusAddress4Entity(
  in template charstring pl_pt,
  in charstring pl_pr,
  in integer pl_iIdx) 
runs on LGen_A_CT {
  var integer fIdx, hashMapRef;  
  [] port_A.receive(pl_pt) -> value v_A_msg {
    f_EPTF_SchedulerComp_refreshSnapshotTime(); //update scheduler data
    log (%definitionId," has received ", v_A_msg);
    v_A_msg4entity:=str2int(regexp(v_A_msg,pl_pr, 0));
    v_A_msg4call:=str2int(regexp(v_A_msg,pl_pr, 1));
    f_EPTF_LGenBase_dispatchEvent({{v_A_myBIdx,pl_iIdx,{v_A_msg4entity,omit},omit},{}});
  }
}


altstep as_LibA() runs on LGen_A_CT {
  [] as_A_input_PrefixPlusAddress4Entity(tr_A_foo,c_A_regex_foo,c_A_inputIdx_foo) {repeat}//catch bar
  [] as_A_input_PrefixPlusAddress4Fsm(tr_A_bar,c_A_regex_bar,c_A_inputIdx_bar,BOC) {repeat}//catch bar
  [] as_A_input_PrefixPlusAddress4Fsm(tr_A_bye,c_A_regex_bye,c_A_inputIdx_bye,BIC) {repeat}//catch bar
  [] as_A_input_PrefixPlusAddress4Fsm(tr_A_byeBye,c_A_regex_byeBye,c_A_inputIdx_byeBye,BOC) {repeat}//catch bye-bye
  [] as_A_catchAll()  {repeat}
}
function f_LGen_A_log() runs on LGen_A_CT {
  log(%definitionId, ": ",
    "myBIdx:=",v_A_myBIdx,
    ", portA:={", port_A,
    ", v_A_contexts:=",v_A_contexts 
  );
}
}
