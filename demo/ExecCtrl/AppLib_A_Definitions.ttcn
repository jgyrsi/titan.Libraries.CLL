///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: AppLib_A_Definitions
// 
//  Purpose:
//    This module demonstrates the usage of the LGenBase feature through
//    a basic applib
// 
//  Module Parameters:
//    -
// 
//  Module depends on:
//    <EPTF_CLL_LGenBase_Definitions>
//    <EPTF_CLL_Common_Definitions>
//    <EPTF_CLL_Scheduler_Definitions>
//    <EPTF_CLL_RBTScheduler_Definitions>
// 
//  Current Owner:
//    ELSZSKU
// 
//  Last Review Date:
//    2008-02-18
// 
// 
///////////////////////////////////////////////////////////
module AppLib_A_Definitions {
import from EPTF_CLL_LGenBase_Definitions all;
import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_Scheduler_Definitions all;
import from EPTF_CLL_RBTScheduler_Definitions all;

modulepar float tsp_T_progress := 3.0;
modulepar float tsp_T_Hold := 2.0;

const charstring c_A_behaviorType := "Behavior_A";
const integer c_A_maxEntityCount := 100; // if negative: unlimited

type port Port_A_PT message {
  inout charstring;
} with {extension "internal"}

type record A_Ctx {
  integer oCalls,
  integer iCalls,
  integer oCallsHashMapRef,
  integer iCallsHashMapRef,
  charstring fooMessage,
  charstring barMessage,
  charstring byeMessage,
  charstring byeByeMessage
}
type record of A_Ctx A_CtxList;
type enumerated A_CallTypes { 
  BOC, //basic outg. call
  BIC //basic inco. call
}

type component LGen_A_CT extends EPTF_LGenBase_CT, EPTF_Scheduler_CT {
  var integer v_A_myBIdx;
  port Port_A_PT port_A; 
  var A_CtxList v_A_contexts; 
  var charstring v_A_msg:="";
  var integer v_A_msg4entity :=-1, v_A_msg4call:=-1;
} 

//stuff for message "foo"
template charstring tr_A_foo :=pattern "foo:\d#(1,),CallId:\d#(1,)";
const charstring c_A_regex_foo :="foo:([0-9]+),CallId:([0-9]+)";  
//stuff for message "bar""
template charstring tr_A_bar :=pattern "bar:\d#(1,),CallId:\d#(1,)";
const charstring c_A_regex_bar :="bar:([0-9]+),CallId:([0-9]+)";  
//stuff for message "bye""
template charstring tr_A_bye :=pattern "bye:\d#(1,),CallId:\d#(1,)";
const charstring c_A_regex_bye :="bye:([0-9]+),CallId:([0-9]+)";  
//stuff for message "bye-bye""
template charstring tr_A_byeBye :=pattern "bye-bye:\d#(1,),CallId:\d#(1,)";
const charstring c_A_regex_byeBye :="bye-bye:([0-9]+),CallId:([0-9]+)";  


//inputs ------------------------------------------------
const integer c_A_inputIdx_sendFoo := 0;
const charstring c_A_inputName_sendFoo := "Trigger has occured for making a call"
const integer c_A_inputIdx_bar := 1;
const charstring c_A_inputName_bar := "Message bar has arrived"
/*
const integer c_A_inputIdx_timeoutTP := 2;
const charstring c_A_inputName_timeoutTP  := "Call progress timeout ";
const integer c_A_inputIdx_timeoutTH := 3;
const charstring c_A_inputName_timeoutTH  := "Call hold time is over";
*/
const integer c_A_inputIdx_byeBye := 2;
const charstring c_A_inputName_byeBye  := "Message bye-bye has arrived";
const integer c_A_inputIdx_foo := 3;
const charstring c_A_inputName_foo := "A call connect request"
const integer c_A_inputIdx_bye := 4;
const charstring c_A_inputName_bye := "A call disconnect request"
const integer c_A_inputIdx_finishedBOC := 5;
const charstring c_A_inputName_finishedBOC := "Basic outgoing call is over"
const integer c_A_inputIdx_finishedBIC := 6;
const charstring c_A_inputName_finishedBIC := "Basic incoming call is over"



//steps ------------------------------------------------
const integer c_A_stepIdx_sendFoo := 0; 
const charstring c_A_stepName_sendFoo := "Sends foo over A";
/*
const integer c_A_stepIdx_startTP := 1;
const charstring c_A_stepName_startTP := "Start the guard timer with a latency of tsp_T_progress";
const integer c_A_stepIdx_cancelTP := 2;
const charstring c_A_stepName_cancelTP := "Cancel the guard timer";
const integer c_A_stepIdx_startTH := 3;
const charstring c_A_stepName_startTH := "Start the hold timer with a latency of tsp_T_progress";
const integer c_A_stepIdx_cancelTH := 4;
const charstring c_A_stepName_cancelTH := "Cancel the hold timer";
*/
const integer c_A_stepIdx_sendBye := 1; 
const charstring c_A_stepName_sendBye := "Sends bye over A";
const integer c_A_stepIdx_sendBar := 2; 
const charstring c_A_stepName_sendBar := "Sends bar over A";
const integer c_A_stepIdx_sendByeBye := 3; 
const charstring c_A_stepName_sendByeBye := "Sends bye-bye over A";
const integer c_A_stepIdx_startBOC := 4; 
const charstring c_A_stepName_startBOC := "Starts Basic Outgoing Call";
const integer c_A_stepIdx_stopBOC := 5; 
const charstring c_A_stepName_stopBOC:= "Stops Basic Outgoing Call";
const integer c_A_stepIdx_startBIC := 6; 
const charstring c_A_stepName_startBIC := "Starts Basic Incoming Call";
const integer c_A_stepIdx_stopBIC := 7; 
const charstring c_A_stepName_stopBIC := "Stops Basic Incoming Call";
const integer c_A_stepIdx_generateEntityEventsAboutFsm := 8; 
const charstring c_A_stepName_generateEntityEventsAboutFsm := "Generate entity events about an FSM";

//FSM-s -------------------------
const integer c_A_fsmIdx_basicOutgoingCall := 0;
const charstring c_A_fsmName_basicOutgoingCall := "FSM for outgoing call"; 
const integer c_A_fsmIdx_basicIncomingCall := 1;
const charstring c_A_fsmName_basicIncomingCall := "FSM for incoming call"; 
//const integer c_A_fsmIdx_indexedBasicCall := 2;
//const charstring c_A_fsmName_indexedBasicCall := "Indexed FSM for Basic outgoing call"; 
const integer c_A_fsmIdx_waiting4Call := 2;
const charstring c_A_fsmName_waiting4Call := "Entity of type A waiting for calls"; 

//Appdata indices for fsm contexts------------------------
//const integer c_A_fsmCtx_timerDataRow :=0, c_A_fsmCtx_timer_TP:=0,c_A_fsmCtx_timer_TH:=1,
const integer c_A_fsmCtx_callDataRow :=0, c_A_fsmCtx_callData_Id:=0;

}
