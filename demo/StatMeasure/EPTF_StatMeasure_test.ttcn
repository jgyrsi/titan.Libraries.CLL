///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
module EPTF_StatMeasure_test {

import from EPTF_CLL_StatMeasure_Definitions all;
import from EPTF_CLL_StatMeasure_Functions all;
import from EPTF_CLL_Variable_Definitions all;
import from EPTF_CLL_Variable_Functions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_Common_Definitions all; // EPTF_IntegerList


type component StatMeasure_test_CT extends EPTF_Var_CT, EPTF_StatMeasure_CT {
  var integer v_myVar := 1;
  var integer v_myVar2 := 10;
  timer t_wait := 0.5;
  var integer vl_intValue;
  var integer v_myVarIdx;
  var integer vl_intVarIdx;
  var integer vl_intSumIdx;
  var integer vl_remoteIntSubscrIdx;
  var integer vl_remoteIntRefSubscrIdx;
  var integer vl_remoteRemoteIntRefSubscrIdx;
}

function f_sumInt(in integer pl_idx, in EPTF_IntegerList pl_argList, in EPTF_IntegerList pl_nonVarArgList, inout EPTF_Var_DirectContent pl_retVal) runs on EPTF_Var_CT {
  var integer vl_sum :=0;
  for (var integer i:=0; i<sizeof(pl_argList); i:=i+1) {
    vl_sum := vl_sum + f_EPTF_Var_getIntValue(pl_argList[i]);
  }
  pl_retVal:= {intVal:=vl_sum};
}

// check if value is even
function f_guard_parity_Int(in integer pl_idx, in EPTF_IntegerList pl_argList, in EPTF_Var_DirectContent pl_newContent) runs on EPTF_Var_CT return boolean {
  var integer vl_value := pl_newContent.intVal;
  return (vl_value rem 2 == 0);
}

function f_postProc_logValue(in integer pl_idx, in EPTF_IntegerList pl_argList) runs on EPTF_Var_CT {
  var EPTF_Var_DirectContent vl_currentContent;
  f_EPTF_Var_getContent(pl_idx, vl_currentContent);
  log("Value of variable ", pl_idx, ": ", vl_currentContent);
}

function f_setverdictFail(in boolean pl_condition, in charstring pl_reason := "") runs on StatMeasure_test_CT {
  if (pl_condition) {
    setverdict(fail, pl_reason);
    f_EPTF_Base_stop();
  }
}


function f_createConfig1(in EPTF_Var_SubscriptionMode pl_subscriptionMode) runs on StatMeasure_test_CT {
  log("Ref assigned to component var");
  //var integer v_myVarIdx;
  f_EPTF_Var_newIntRef("myVar",v_myVar,v_myVarIdx);
  f_EPTF_Var_CT_LogAll("----- newIntRef -------");
  f_EPTF_Var_Log("--var--",v_myVarIdx);
  
  log("Dereferencing the ref");
//  var integer vl_intValue;
  vl_intValue := f_EPTF_Var_getIntValue(v_myVarIdx);
  log("getIntValue: ", vl_intValue);
  log("v_myVar: ", v_myVar);
  f_setverdictFail((v_myVar!=1)or(v_myVar!=vl_intValue), log2str("v_myVar != 1 or v_myVar != ", vl_intValue, ". v_myVar = ", v_myVar));
  
  log("Modifying value of referenced var to 2...");
//  var octetstring vl_varRef := f_EPTF_Var_getValueRef(v_myVarIdx);
//  f_EPTF_Var_modifyIntRefValue(vl_varRef,2);
  f_EPTF_Var_setContent(v_myVarIdx,{intVal:=2});
  vl_intValue := f_EPTF_Var_getIntValue(v_myVarIdx);
  log("getIntValue: ", vl_intValue);
  log("v_myVar: ", v_myVar);
  f_setverdictFail((v_myVar!=2)or(v_myVar!=vl_intValue), log2str("v_myVar != 2 or v_myVar != ", vl_intValue, ". v_myVar = ", v_myVar));
  
  
  log("Creating intVar with initial value: 10");
//  var integer vl_intVarIdx;
  f_EPTF_Var_newInt("intVar",10, vl_intVarIdx);
  f_EPTF_Var_CT_LogAll("----- newInt -------");
  f_EPTF_Var_Log("--intVar--",vl_intVarIdx);
  vl_intValue := f_EPTF_Var_getIntValue(vl_intVarIdx);
  log("getIntValue: ", vl_intValue);
  f_setverdictFail((vl_intValue!=10), log2str("vl_intValue != 10. vl_intValue = ", vl_intValue));

  log("Modifying value of intVar to 20...");
  f_EPTF_Var_setContent(vl_intVarIdx,{intVal:=20});
  vl_intValue := f_EPTF_Var_getIntValue(vl_intVarIdx);
  log("getIntValue: ", vl_intValue);
  log("v_myVar: ", v_myVar);
  f_setverdictFail((vl_intValue!=20)or(v_myVar!=2), log2str("v_myVar != 2 or vl_intValue != 20. vl_intValue = ", vl_intValue, ". v_myVar = ", v_myVar));
  f_EPTF_Var_CT_LogAll("----- END MODIFY -------");
  
// SUBSCRIBE local
  // create a variable that contains the sum of the two above:
//  var integer vl_intSumIdx;
  f_EPTF_Var_newInt("intSum",0, vl_intSumIdx);
  f_EPTF_Var_CT_LogAll("----- newInt -------");
  // set its local provider:
  var EPTF_Var_ProviderLocal vl_providerLocal := {calcFn:={
    funcRef := refers(f_sumInt),
    argList := {v_myVarIdx,vl_intVarIdx},
    nonVarArgList := {}
  }};
  f_EPTF_Var_subscribeLocal(vl_intSumIdx,vl_providerLocal);
  f_setverdictFail((f_EPTF_Var_getIntValue(vl_intSumIdx)!=22), log2str("int value of vl_intValue variable != 22"));
  f_EPTF_Var_CT_LogAll("----- LOCAL PROVIDER SET -------");
  
  f_EPTF_Var_addPostProcFn(vl_intSumIdx, {refers(f_postProc_logValue),{}});
  f_EPTF_Var_CT_LogAll("----- POSTPROC SET -------");

  //f_EPTF_Var_callCalcFn(vl_intSumIdx);
  //f_EPTF_Var_Log("--callCalcFn--",vl_intSumIdx);
  
// SUBSCRIBE remote for new EPTF var
  // create remote buffered subscriber for the intSum variable
//  var integer vl_remoteIntSubscrIdx;
  f_EPTF_Var_subscribeRemote(self,"intSum",pl_subscriptionMode, vl_remoteIntSubscrIdx);
  f_EPTF_Var_subscribeRemote(self,"intSum",pl_subscriptionMode, vl_remoteIntSubscrIdx, "remoteInt"); // test already subscribed checking with different local name
  f_EPTF_Var_CT_LogAll("----- REMOTE PROVIDER SET -------");
  
  t_wait.start;
  alt {
    [] t_wait.timeout {
      f_EPTF_Var_CT_LogAll("----- REMOTE PROVIDER SET TIMEOUT -------");
    }
  }
  f_setverdictFail((f_EPTF_Var_getIntValue(vl_remoteIntSubscrIdx)!=22), log2str("int value of vl_remoteIntSubscrIdx variable != 22"));

// SUBSCRIBE remote for referenced var
  // create remote buffered v_myVar2-referenced subscriber for the intRef variable
  log("v_myVar2: ", v_myVar2);
//  var integer vl_remoteIntRefSubscrIdx;
  f_EPTF_Var_subscribeRemoteIntRef(v_myVar2,self,"myVar",pl_subscriptionMode, vl_remoteIntRefSubscrIdx,"remoteIntRef"); // subscribe-ref with local name
  f_EPTF_Var_CT_LogAll("----- REMOTE PROVIDER SET FOR REF -------");
  
  t_wait.start;
  alt {
    [] t_wait.timeout {
      f_EPTF_Var_CT_LogAll("----- REMOTE PROVIDER SET FOR REF TIMEOUT -------");
    }
  }
  log("v_myVar2: ", v_myVar2);
  f_setverdictFail((v_myVar2!=2), log2str("v_myVar2 != 2. v_myVar2 = ", v_myVar2));
  
  
  // if this line is not commented out, the subscription after this one will fail in a name clash (name already exist):
  //f_EPTF_Var_newInt("nonBuffered.remoteIntRef",0,vl_remoteRemoteIntRefSubscrIdx);

  // if this line is not commented out, the subscription will fail because the given name does not exist:
  //f_EPTF_Var_subscribeRemote(self,"xxxxx",pl_buffered, vl_remoteRemoteIntRefSubscrIdx);

// SUBSCRIBE remote-remote for referenced var
  // create remote buffered subscriber for the vl_remoteIntRefSubscrIdx variable
//  var integer vl_remoteRemoteIntRefSubscrIdx;
  f_EPTF_Var_subscribeRemote(self,"remoteIntRef",pl_subscriptionMode, vl_remoteRemoteIntRefSubscrIdx);
  f_EPTF_Var_CT_LogAll("----- REMOTE PROVIDER SET FOR REF -------");
  
  f_EPTF_Var_addPostProcFn(vl_remoteRemoteIntRefSubscrIdx, {refers(f_postProc_logValue),{}});
  f_EPTF_Var_CT_LogAll("----- POSTPROC SET FOR REMOTE INT REF -------");

  t_wait.start;
  alt {
    [] t_wait.timeout {
      f_EPTF_Var_CT_LogAll("----- REMOTE REMOTE PROVIDER SET FOR REF TIMEOUT -------");
    }
  }
  vl_intValue := f_EPTF_Var_getIntValue(vl_remoteRemoteIntRefSubscrIdx);
  log("getIntValue remoteRemote: ", vl_intValue);
  f_setverdictFail((vl_intValue!=2));
}


testcase tc_EPTF_StatMeasure_test_nonBuffered_WithAllStat() runs on StatMeasure_test_CT {

  f_EPTF_Var_CT_LogAll("----- START -------");
  f_EPTF_StatMeasure_init_CT("WithAllStat");
  f_EPTF_Var_CT_LogAll("----- INIT READY -------");
  action("The execution takes approx 15 sec to finish...");

  f_createConfig1(realtime);
  // register stats:
  var EPTF_IntegerList vl_statListFor_remoteRemoteIntRefSubscrIdx;
  f_EPTF_StatMeasure_createAllStats(v_myVarIdx);
  f_EPTF_StatMeasure_createAllStats(vl_intVarIdx);
  f_EPTF_StatMeasure_createAllStats(vl_intSumIdx);
  f_EPTF_StatMeasure_createAllStats(vl_remoteIntSubscrIdx);
  f_EPTF_StatMeasure_createAllStats(vl_remoteIntRefSubscrIdx);
  vl_statListFor_remoteRemoteIntRefSubscrIdx:=f_EPTF_StatMeasure_createAllStats(vl_remoteRemoteIntRefSubscrIdx);
  
  // create var for stat:
  var EPTF_IntegerList vl_statListFor_remoteRemoteIntRefSubscr_statIdx
  for (var integer i:=0; i<sizeof(vl_statListFor_remoteRemoteIntRefSubscrIdx); i:=i+1) {
    vl_statListFor_remoteRemoteIntRefSubscr_statIdx[i] := f_EPTF_StatMeasure_createVarFromStat(vl_statListFor_remoteRemoteIntRefSubscrIdx[i]);
    
    // if density: set boundaries to linear scale:
    if (f_EPTF_StatMeasure_getStatType(vl_statListFor_remoteRemoteIntRefSubscrIdx[i]) == density) {
      f_EPTF_StatMeasure_createVarFromStat_boundaries_density(vl_statListFor_remoteRemoteIntRefSubscrIdx[i])
      f_EPTF_StatMeasure_setScale_density(vl_statListFor_remoteRemoteIntRefSubscrIdx[i],0.0,10.0,11);
    }
  }
  
  f_EPTF_Var_CT_LogAll("----- REGISTER STATS -------");
  f_EPTF_StatMeasure_LogAll("----- REGISTER STATS -------");

// REFRESH
  v_myVar := 5;
  f_EPTF_Var_refreshContent(v_myVarIdx);
  t_wait.start;
  alt {
    [] t_wait.timeout {
      f_EPTF_Var_CT_LogAll("----- REFRESH CONTENT TIMEOUT -------");
    }
  }
  log("v_myVar: ", v_myVar);
  log("v_myVar2: ", v_myVar2);
  vl_intValue := f_EPTF_Var_getIntValue(vl_remoteRemoteIntRefSubscrIdx);
  log("getIntValue remoteRemote: ", vl_intValue);
  f_setverdictFail((vl_intValue!=5)or(v_myVar!=v_myVar2)or(v_myVar!=vl_intValue)or(f_EPTF_Var_getIntValue(vl_remoteIntSubscrIdx)!=25));

// ADJUST local.REF.param
  f_EPTF_Var_adjustContent(v_myVarIdx,{intVal:=6});
  t_wait.start;
  alt {
    [] t_wait.timeout {
      f_EPTF_Var_CT_LogAll("----- ADJUST CONTENT LOCAL REF TIMEOUT -------");
    }
  }
  log("v_myVar: ", v_myVar);
  vl_intValue := f_EPTF_Var_getIntValue(vl_remoteRemoteIntRefSubscrIdx);
  log("getIntValue remoteRemote: ", vl_intValue);
  f_setverdictFail((vl_intValue!=6)or(v_myVar!=v_myVar2)or(v_myVar!=vl_intValue)or(f_EPTF_Var_getIntValue(vl_remoteIntSubscrIdx)!=26));

// ADJUST local.DIRECT.param
  f_EPTF_Var_adjustContent(vl_intVarIdx,{intVal:=30});
  t_wait.start;
  alt {
    [] t_wait.timeout {
      f_EPTF_Var_CT_LogAll("----- ADJUST CONTENT LOCAL TIMEOUT -------");
    }
  }
  vl_intValue := f_EPTF_Var_getIntValue(vl_intVarIdx);
  log("getIntValue: ", vl_intValue);
  log("v_myVar: ", v_myVar);
  vl_intValue := f_EPTF_Var_getIntValue(vl_remoteRemoteIntRefSubscrIdx);
  log("getIntValue remoteRemote: ", vl_intValue);
  f_setverdictFail((vl_intValue!=6)or(v_myVar!=v_myVar2)or(v_myVar!=vl_intValue)or(f_EPTF_Var_getIntValue(vl_remoteIntSubscrIdx)!=36)or(f_EPTF_Var_getIntValue(vl_intVarIdx)!=30));

  f_EPTF_StatMeasure_resetStats(vl_statListFor_remoteRemoteIntRefSubscrIdx);
  f_EPTF_Var_CT_LogAll("----- RESET STATS FOR " & int2str(vl_remoteRemoteIntRefSubscrIdx) & " -------");
// ADJUST remote
  f_EPTF_Var_adjustContent(vl_remoteIntRefSubscrIdx,{intVal:=7});
  t_wait.start;
  alt {
    [] t_wait.timeout {
      f_EPTF_Var_CT_LogAll("----- ADJUST CONTENT REMOTE TIMEOUT -------");
    }
  }
  vl_intValue := f_EPTF_Var_getIntValue(vl_intVarIdx);
  log("getIntValue: ", vl_intValue);
  log("v_myVar: ", v_myVar);
  log("v_myVar2: ", v_myVar2);
  vl_intValue := f_EPTF_Var_getIntValue(vl_remoteRemoteIntRefSubscrIdx);
  log("getIntValue remoteRemote: ", vl_intValue);
  f_setverdictFail((vl_intValue!=7)or(v_myVar!=v_myVar2)or(v_myVar!=vl_intValue)or(f_EPTF_Var_getIntValue(vl_remoteIntSubscrIdx)!=37)or(f_EPTF_Var_getIntValue(vl_intVarIdx)!=30));

// ADJUST remote remote
  f_EPTF_Var_adjustContent(vl_remoteRemoteIntRefSubscrIdx,{intVal:=8});
  t_wait.start;
  alt {
    [] t_wait.timeout {
      f_EPTF_Var_CT_LogAll("----- ADJUST CONTENT REMOTE REMOTE TIMEOUT -------");
    }
  }
  log("v_myVar: ", v_myVar);
  log("v_myVar2: ", v_myVar2);
  vl_intValue := f_EPTF_Var_getIntValue(vl_remoteRemoteIntRefSubscrIdx);
  log("getIntValue remoteRemote: ", vl_intValue);
  f_setverdictFail((vl_intValue!=8)or(v_myVar!=v_myVar2)or(v_myVar!=vl_intValue)or(f_EPTF_Var_getIntValue(vl_remoteIntSubscrIdx)!=38)or(f_EPTF_Var_getIntValue(vl_intVarIdx)!=30));

// ADJUST local.DIRECT.localProv
// this causes an error:
//  f_EPTF_Var_adjustContent(vl_intSumIdx,{intVal:=0});

// ADJUST remote.DIRECT.localProv
// this causes an error:
//  f_EPTF_Var_adjustContent(vl_remoteIntSubscrIdx,{intVal:=0});


// LOG StatStrings for vl_remoteIntSubscrIdx:

  f_EPTF_Var_Log("Stat2Str for variable",vl_remoteIntSubscrIdx);
  f_EPTF_StatMeasure_LogAll("----- CURRENT STATS -------");

// SAMPLEDAVERAGE:
  
  // create a new EPS stat without a variable:
  var integer vl_EPSIdx := f_EPTF_StatMeasure_newStat_EPS();
  var integer vl_chronoIdx := f_EPTF_StatMeasure_newStat_chrono();
  
  var integer vl_EPS_varIdx := f_EPTF_StatMeasure_createVarFromStat(vl_EPSIdx);
  var integer vl_chrono_varIdx := f_EPTF_StatMeasure_createVarFromStat(vl_chronoIdx);
  var EPTF_Var_DirectContent vl_tmpContent;
  //f_EPTF_StatMeasure_start_chrono(vl_chronoIdx);
  
  // create density stat for chrono variable:
  var integer vl_chronoDensityStatIdx := f_EPTF_StatMeasure_newStat_density({-1.0,11.0,12.0,13.0,20.0},vl_chrono_varIdx);
  var integer vl_chronoDensityStatVarIdx := f_EPTF_StatMeasure_createVarFromStat(vl_chronoDensityStatIdx);
  var integer vl_chronoDensityStatBoundariesVarIdx := f_EPTF_StatMeasure_createVarFromStat_boundaries_density(vl_chronoDensityStatIdx);
  
  f_EPTF_StatMeasure_addData_density(vl_chronoDensityStatIdx,11.0);
  f_EPTF_StatMeasure_addData_density(vl_chronoDensityStatIdx,-2.0);
  
  f_EPTF_StatMeasure_LogAll("----- CREATE DENSITY STAT FOR CHRONO VARIABLE -------");
  f_EPTF_Var_CT_LogAll("----- CREATE DENSITY STAT FOR CHRONO VARIABLE -------");

  // adjust boundary variable (will be sorted automatically):
  f_EPTF_Var_adjustContent(vl_chronoDensityStatBoundariesVarIdx,{floatlistVal := {2.0,0.0,3.0,1.0}});

  f_EPTF_StatMeasure_addData_density(vl_chronoDensityStatIdx,3.0);
  f_EPTF_StatMeasure_addData_density(vl_chronoDensityStatIdx,-2.0);

  f_EPTF_StatMeasure_LogAll("----- ADJUST BOUNDARY VARIABLE -------");
  f_EPTF_Var_CT_LogAll("----- ADJUST BOUNDARY VARIABLE -------");
  f_EPTF_Var_Log("----- ADJUST BOUNDARY VARIABLE -------",vl_chronoDensityStatBoundariesVarIdx);
  
  // setscale to log:
  f_EPTF_StatMeasure_setScale_density(vl_chronoDensityStatIdx,0.1,10.0,11, logarithmic);
  f_EPTF_StatMeasure_LogAll("----- SETSCALE TO LOG -------");
  f_EPTF_Var_CT_LogAll("----- SETSCALE TO LOG -------");
  f_EPTF_Var_Log("----- SETSCALE TO LOG -------",vl_chronoDensityStatBoundariesVarIdx);

  
  var float vl_sampleTime := 0.5; // the period of the calculation of the EPSs (CPS)
  timer t_sample := vl_sampleTime;
  timer t_refresh := 0.11;
  timer t_end := 10.0;
  t_sample.start; t_refresh.start; t_end.start;
  
  alt {
    [] t_end.timeout {
      f_EPTF_StatMeasure_LogAll("----- PeriodicUPDATESAMPLEDAVERAGE STATS END-------");
    }
    [] t_refresh.timeout {
      f_EPTF_StatMeasure_addData_EPS(vl_EPSIdx); //(new call is generated)
      f_EPTF_StatMeasure_addData_chrono(vl_chronoIdx);
      f_EPTF_Var_adjustContent(v_myVarIdx,{intVal:=v_myVar}); // also: adjust the variable, but do not change the value
      t_refresh.start;
      repeat;
    }
    [] t_sample.timeout {
      f_EPTF_StatMeasure_update_EPS(vl_EPSIdx); // uses automatic time measure
      f_EPTF_StatMeasure_update_chrono(vl_chronoIdx); // update the measured time
      log("Measurement time: ", f_EPTF_StatMeasure_getMeasurementLength_EPS(vl_EPSIdx));
      log("Elapsed time    : ", f_EPTF_StatMeasure_getTime_EPS(vl_EPSIdx));
      log("Chrono meas time: ", f_EPTF_StatMeasure_getMeasurementLength_chrono(vl_chronoIdx));
      log("ChronoElapsed   : ", f_EPTF_StatMeasure_getTime_chrono(vl_chronoIdx));
//      f_EPTF_Var_refreshContent(vl_EPS_varIdx);
      f_EPTF_Var_getContent(vl_EPS_varIdx,vl_tmpContent);
      log("Content of EPS variable   : ", vl_tmpContent);
//      f_EPTF_Var_refreshContent(vl_chrono_varIdx);
      f_EPTF_Var_getContent(vl_chrono_varIdx,vl_tmpContent);
      log("Content of chrono variable: ", vl_tmpContent);
      f_setverdictFail(vl_tmpContent.floatVal != f_EPTF_StatMeasure_getMeasurementLength_chrono(vl_chronoIdx));
      f_EPTF_StatMeasure_update_EPS(vl_statListFor_remoteRemoteIntRefSubscrIdx[7],vl_sampleTime);
      f_EPTF_StatMeasure_LogAll("----- PeriodicUPDATE_SAMPLEDAVERAGE STATS -------");
       t_sample.start;
      repeat;
    }
  }
  
// GUARD FN

  // add guard Fn to myVar
  log("v_myVar: ", v_myVar);
  f_EPTF_Var_addGuardFn(v_myVarIdx,{refers(f_guard_parity_Int),{}});
  f_EPTF_Var_Log("----- GUARDFN SET -------",v_myVarIdx);
  log("v_myVar with guard: ", v_myVar);
// ADJUST remote remote
  f_EPTF_Var_adjustContent(vl_remoteRemoteIntRefSubscrIdx,{intVal:=1});
  t_wait.start;
  alt {
    [] t_wait.timeout {
      f_EPTF_Var_CT_LogAll("----- GUARDCHECK 1: ADJUST CONTENT REMOTE REMOTE TIMEOUT -------");
    }
  }
  log("v_myVar: ", v_myVar);
  log("v_myVar2: ", v_myVar2);
  vl_intValue := f_EPTF_Var_getIntValue(vl_remoteRemoteIntRefSubscrIdx);
  log("getIntValue remoteRemote: ", vl_intValue);
  f_setverdictFail((vl_intValue!=8)or(v_myVar!=v_myVar2)or(v_myVar!=vl_intValue)or(f_EPTF_Var_getIntValue(vl_remoteIntSubscrIdx)!=38)or(f_EPTF_Var_getIntValue(vl_intVarIdx)!=30));
// ADJUST remote remote
  f_EPTF_Var_adjustContent(vl_remoteRemoteIntRefSubscrIdx,{intVal:=2});
  t_wait.start;
  alt {
    [] t_wait.timeout {
      f_EPTF_Var_CT_LogAll("----- GUARDCHECK 2: ADJUST CONTENT REMOTE REMOTE TIMEOUT -------");
    }
  }
  log("v_myVar: ", v_myVar);
  log("v_myVar2: ", v_myVar2);
  vl_intValue := f_EPTF_Var_getIntValue(vl_remoteRemoteIntRefSubscrIdx);
  log("getIntValue remoteRemote: ", vl_intValue);
  f_setverdictFail((vl_intValue!=2)or(v_myVar!=v_myVar2)or(v_myVar!=vl_intValue)or(f_EPTF_Var_getIntValue(vl_remoteIntSubscrIdx)!=32)or(f_EPTF_Var_getIntValue(vl_intVarIdx)!=30));
// ADJUST remote remote
  f_EPTF_Var_adjustContent(vl_remoteRemoteIntRefSubscrIdx,{intVal:=3});
  f_EPTF_Var_unsubscribe(vl_remoteRemoteIntRefSubscrIdx);
  t_wait.start;
  alt {
    [] t_wait.timeout {
      f_EPTF_Var_CT_LogAll("----- GUARDCHECK 3: ADJUST CONTENT AND UNSUBSCRIBE REMOTE REMOTE TIMEOUT -------");
    }
  }
  log("v_myVar: ", v_myVar);
  log("v_myVar2: ", v_myVar2);
  vl_intValue := f_EPTF_Var_getIntValue(vl_remoteRemoteIntRefSubscrIdx);
  log("getIntValue remoteRemote: ", vl_intValue);
  f_setverdictFail((vl_intValue!=2)or(v_myVar!=v_myVar2)or(v_myVar!=vl_intValue)or(f_EPTF_Var_getIntValue(vl_remoteIntSubscrIdx)!=32)or(f_EPTF_Var_getIntValue(vl_intVarIdx)!=30));
// ADJUST remote remote
  f_EPTF_Var_adjustContent(vl_remoteRemoteIntRefSubscrIdx,{intVal:=4});
  t_wait.start;
  alt {
    [] t_wait.timeout {
      f_EPTF_Var_CT_LogAll("----- GUARDCHECK 4: ADJUST CONTENT AND UNSUBSCRIBE REMOTE REMOTE TIMEOUT -------");
    }
  }
  log("v_myVar: ", v_myVar);
  log("v_myVar2: ", v_myVar2);
  vl_intValue := f_EPTF_Var_getIntValue(vl_remoteRemoteIntRefSubscrIdx);
  log("getIntValue remoteRemote: ", vl_intValue);
  f_setverdictFail((vl_intValue!=4)or(v_myVar!=2)or(v_myVar!=v_myVar2)or(f_EPTF_Var_getIntValue(vl_remoteIntSubscrIdx)!=32)or(f_EPTF_Var_getIntValue(vl_intVarIdx)!=30));

// END

  f_EPTF_StatMeasure_update_EPS(vl_statListFor_remoteRemoteIntRefSubscrIdx[7], 10.0);
  f_EPTF_StatMeasure_LogAll("----- UPDATE_SAMPLEDAVERAGE STATS -------");

  f_EPTF_Var_unsubscribe(vl_remoteIntSubscrIdx);
  f_EPTF_Var_CT_LogAll("----- REMOTE PROVIDER REMOVED -------");
  
  t_wait.start;
  alt {
    [] t_wait.timeout {
      f_EPTF_Var_CT_LogAll("----- REMOTE PROVIDER REMOVE TIMEOUT -------");
    }
  }

  f_EPTF_Var_unsubscribe(vl_intSumIdx);
  f_EPTF_Var_CT_LogAll("----- LOCAL PROVIDER REMOVED -------");
  for (var integer i:=0; i<sizeof(vl_statListFor_remoteRemoteIntRefSubscr_statIdx); i:=i+1) {
    if (vl_statListFor_remoteRemoteIntRefSubscr_statIdx[i] == -1) {
      continue;
    }
    var EPTF_Var_DirectContent vl_content;
    f_EPTF_Var_getContent(vl_statListFor_remoteRemoteIntRefSubscr_statIdx[i], vl_content);
    f_EPTF_Var_Log("Value of Var ", vl_statListFor_remoteRemoteIntRefSubscr_statIdx[i]);
    log(": ", vl_content);
  }
  

  // chrono stop:
  var float vl_timeBeforeStop := f_EPTF_StatMeasure_getTime_chrono(vl_chronoIdx);
  var float vl_valueBeforeStop;
  f_EPTF_StatMeasure_getStat_chrono(vl_chronoIdx, vl_valueBeforeStop)
  // stop the chrono:
  f_EPTF_StatMeasure_stop_chrono(vl_chronoIdx);
  var float vl_valueAfterStop;
  f_EPTF_StatMeasure_getStat_chrono(vl_chronoIdx, vl_valueAfterStop);
  log("vl_valueBeforeStop: ", vl_valueBeforeStop);
  log("vl_timeBeforeStop: ", vl_timeBeforeStop);
  log("vl_valueAfterStop: ", vl_valueAfterStop);
  f_setverdictFail(vl_valueBeforeStop>=vl_timeBeforeStop);
  f_setverdictFail(vl_valueAfterStop<=vl_valueBeforeStop);
  
  var float vl_timeAfterStop := f_EPTF_StatMeasure_getTime_chrono(vl_chronoIdx);
  log("vl_timeAfterStop: ", vl_timeAfterStop);
  f_setverdictFail(vl_valueAfterStop!=vl_timeAfterStop);

  // stop the chrono again:
  f_EPTF_StatMeasure_stop_chrono(vl_chronoIdx);
  vl_timeAfterStop := f_EPTF_StatMeasure_getTime_chrono(vl_chronoIdx);
  // time and value should not change:
  f_setverdictFail(vl_valueAfterStop!=vl_timeAfterStop);
  f_EPTF_StatMeasure_getStat_chrono(vl_chronoIdx, vl_valueAfterStop);
  f_setverdictFail(vl_valueAfterStop!=vl_timeAfterStop);
  
  // reset chrono:
  f_EPTF_StatMeasure_resetStat_chrono(vl_chronoIdx);
  f_EPTF_StatMeasure_stop_chrono(vl_chronoIdx);
  vl_timeAfterStop := f_EPTF_StatMeasure_getTime_chrono(vl_chronoIdx);
  // time and value should be zero:
  f_setverdictFail(0.0!=vl_timeAfterStop);
  f_EPTF_StatMeasure_getStat_chrono(vl_chronoIdx, vl_valueAfterStop);
  f_setverdictFail(vl_valueAfterStop!=vl_timeAfterStop);

  f_EPTF_Base_cleanup_CT();
  
  setverdict(pass);
}

testcase tc_EPTF_StatMeasure_test_ChangeToFloat()  runs on StatMeasure_test_CT {
  f_EPTF_StatMeasure_init_CT("ChangeToFloat");
  var integer vl_statIdx_delta := f_EPTF_StatMeasure_newStat_delta();
  var integer vl_statIdx_deltaSum := f_EPTF_StatMeasure_newStat_deltaSum();
  var integer vl_statIdx_min := f_EPTF_StatMeasure_newStat_min();
  var integer vl_statIdx_max := f_EPTF_StatMeasure_newStat_max();
  
  f_EPTF_StatMeasure_changeToFloat_delta(vl_statIdx_delta);
  f_EPTF_StatMeasure_changeToFloat_deltaSum(vl_statIdx_deltaSum);
  f_EPTF_StatMeasure_changeToFloat_min(vl_statIdx_min);
  f_EPTF_StatMeasure_changeToFloat_max(vl_statIdx_max);
  
  f_EPTF_StatMeasure_createVarFromStat(vl_statIdx_delta);
  f_EPTF_StatMeasure_createVarFromStat(vl_statIdx_deltaSum);
  f_EPTF_StatMeasure_createVarFromStat(vl_statIdx_min);
  f_EPTF_StatMeasure_createVarFromStat(vl_statIdx_max);

  f_EPTF_StatMeasure_addData_delta(vl_statIdx_delta,{floatVal := 1.0});
  f_EPTF_StatMeasure_addData_deltaSum(vl_statIdx_deltaSum,{floatVal := 1.0});
  f_EPTF_StatMeasure_addData_min(vl_statIdx_min,{floatVal := 1.0});
  f_EPTF_StatMeasure_addData_max(vl_statIdx_max,{floatVal := 1.0});

  // the type can be changed after reset if no variable is assigned:
  var integer vl_statIdx_min2 := f_EPTF_StatMeasure_newStat_min();
  f_EPTF_StatMeasure_changeToFloat_min(vl_statIdx_min2); // type is changed here to float
  f_EPTF_StatMeasure_addData_min(vl_statIdx_min2,{floatVal := 1.0});
  // now change the type back to int:
  f_EPTF_StatMeasure_resetStat_min(vl_statIdx_min2);
  f_EPTF_StatMeasure_addData_min(vl_statIdx_min2,{intVal := 2}); // here the type changes to int
  // change to float again:
  f_EPTF_StatMeasure_resetStat_min(vl_statIdx_min2);
  f_EPTF_StatMeasure_changeToFloat_min(vl_statIdx_min2); // type is changed here to float
  f_EPTF_StatMeasure_addData_min(vl_statIdx_min2,{floatVal := 3.0});
//  f_EPTF_StatMeasure_changeToFloat_min(vl_statIdx_min2); // this fails because type can only be changed after reset


  // to test if the type can be changed if caraible was created from the stat:
  f_EPTF_StatMeasure_resetStat_delta(vl_statIdx_delta); 
//  f_EPTF_StatMeasure_changeToFloat_delta(vl_statIdx_delta); // fails because variable is created from the stat

  // test if dataType can be changed after reset:
  f_EPTF_StatMeasure_resetStat_deltaSum(vl_statIdx_deltaSum);
//  f_EPTF_StatMeasure_addData_deltaSum(vl_statIdx_deltaSum,{intVal := 2}); // this results in an error, because the created variable type cannot change
  
  // test if wrong data type can be used:
//  f_EPTF_StatMeasure_addData_max(vl_statIdx_max,{intVal := 2}); // this should result in an error because the data type is wrong

  // test if the type can be changed if the stat is not standalone:
  var integer vl_dataVaridx;
  f_EPTF_Var_newInt("dataVar",10, vl_dataVaridx);
  var integer vl_statIdx_deltaSumFromVar := f_EPTF_StatMeasure_newStat_deltaSum(vl_dataVaridx);
//  f_EPTF_StatMeasure_changeToFloat_deltaSum(vl_statIdx_deltaSumFromVar); // this fails because the type of a non-standalone stat cannot be changed
  f_EPTF_StatMeasure_resetStat_deltaSum(vl_statIdx_deltaSumFromVar);
//  f_EPTF_StatMeasure_changeToFloat_deltaSum(vl_statIdx_deltaSumFromVar); // this fails because the type of a non-standalone stat cannot be changed

  setverdict(pass);
}

// demonstrates how the variables containing auxiliary data of mean stat can be created:
testcase tc_AuxDataForMean_test() runs on StatMeasure_test_CT {
  // init features:
  f_EPTF_Var_init_CT("AuxDataForMean");
  f_EPTF_StatMeasure_init_CT("AuxDataForMean");
  
  // create data source (float variable)
  var integer vl_dataSourceVarId;
  f_EPTF_Var_newFloat("DataSource", 0.0, vl_dataSourceVarId);
  
  // create mean from datasource:
  var integer vl_meanStatId := f_EPTF_StatMeasure_newStat_mean(vl_dataSourceVarId);
  // create a var from mean (this is not necessary, because f_StatMeasure_createAuxVarsForMean will do it if needed)
  var integer vl_meanVarId := f_EPTF_StatMeasure_createVarFromStat(vl_meanStatId);
  
  // just reset the stat
  f_EPTF_StatMeasure_resetStat(vl_meanStatId);
  
  // create the aux vars:
  var integer vl_nDataVarIdx := f_EPTF_StatMeasure_createVarFromStat_N_mean(vl_meanStatId);
  
  // add data to stat by adjusting the datasource var:
  f_EPTF_Var_adjustContent(vl_dataSourceVarId,{floatVal := 1.0});
  f_EPTF_Var_adjustContent(vl_dataSourceVarId,{floatVal := 10.0});
  f_EPTF_Var_adjustContent(vl_dataSourceVarId,{floatVal := 100.0});
  f_EPTF_Var_adjustContent(vl_dataSourceVarId,{floatVal := 1000.0});
  
  // n is 4, statValue is 1111/4. Let's check it:
  f_setverdictFail(4!=f_EPTF_Var_getIntValue(vl_nDataVarIdx),
    log2str("Current N is: ", f_EPTF_Var_getIntValue(vl_nDataVarIdx), ", expected: 4"));
  var float vl_meanValue;
  if(f_EPTF_StatMeasure_getStat_mean(vl_meanStatId,vl_meanValue)) {/*no nothing*/}
  f_setverdictFail(1111.0/4.0!=vl_meanValue, 
    log2str("Current statValue is: ", vl_meanValue, ", expected: ",1111.0/4.0));
  f_setverdictFail((1111.0/4.0)!=f_EPTF_Var_getFloatValue(vl_meanVarId),
    log2str("Current value of statVar is: ", f_EPTF_Var_getFloatValue(vl_meanVarId), ", expected: ",1111.0/4.0));

  // exit with pass:
  f_EPTF_Base_stop(pass);
}

// demonstrates how the variables containing auxiliary data of standardDev stat can be created:
testcase tc_AuxDataForStandardDev_test() runs on StatMeasure_test_CT {
  // init features:
  f_EPTF_Var_init_CT("AuxDataForStandardDev");
  f_EPTF_StatMeasure_init_CT("AuxDataForStandardDev");
  
  // create data source (float variable)
  var integer vl_dataSourceVarId;
  f_EPTF_Var_newFloat("DataSource", 0.0, vl_dataSourceVarId);
  
  // create standardDev from datasource:
  var integer vl_standardDevStatId := f_EPTF_StatMeasure_newStat_standardDev(vl_dataSourceVarId);
  var integer vl_standardDevVarId := f_EPTF_StatMeasure_createVarFromStat(vl_standardDevStatId);
  
  // just reset the stat
  f_EPTF_StatMeasure_resetStat(vl_standardDevStatId);
  
  // create the aux vars:
  var integer vl_meanDataVarIdx := f_EPTF_StatMeasure_createVarFromStat_Mean_standardDev(vl_standardDevStatId);
  var integer vl_nDataVarIdx := f_EPTF_StatMeasure_createVarFromStat_N_standardDev(vl_standardDevStatId);
  var integer vl_sDataVarIdx := f_EPTF_StatMeasure_createVarFromStat_S_standardDev(vl_standardDevStatId);
  
  // add data to stat by adjusting the datasource var:
  f_EPTF_Var_adjustContent(vl_dataSourceVarId,{floatVal := 1.0});
  f_EPTF_Var_adjustContent(vl_dataSourceVarId,{floatVal := 10.0});
  f_EPTF_Var_adjustContent(vl_dataSourceVarId,{floatVal := 100.0});
  f_EPTF_Var_adjustContent(vl_dataSourceVarId,{floatVal := 1000.0});
  
  // sum is: 1111, n is 4, statValue is 1111/4,
  // sum of deviation squares (1-1111/4)^2+(10-1111/4)^2+(100-1111/4)^2+(1000-1111/4)^2= 701520.75
  // standard deviation: 701520.75/4 =175380.1875:
  // Let's check them:
  f_setverdictFail(701520.75!=f_EPTF_Var_getFloatValue(vl_sDataVarIdx),
    log2str("Current sum of deviation squares is: ", f_EPTF_Var_getFloatValue(vl_sDataVarIdx), ", expected: 701520.75"));
  f_setverdictFail(4!=f_EPTF_Var_getIntValue(vl_nDataVarIdx),
    log2str("Current N is: ", f_EPTF_Var_getIntValue(vl_nDataVarIdx), ", expected: 4"));
  var float vl_meanValue;
  if(f_EPTF_StatMeasure_getStat_standardDev_mean(vl_standardDevStatId,vl_meanValue)) {/*no nothing*/}
  f_setverdictFail(1111.0/4.0!=vl_meanValue,
    log2str("Current meanValue is: ", vl_meanValue, ", expected: ",1111.0/4.0));
  f_setverdictFail(1111.0/4.0!=f_EPTF_Var_getFloatValue(vl_meanDataVarIdx),
    log2str("Current value of statVar of mean is: ", f_EPTF_Var_getFloatValue(vl_meanDataVarIdx), ", expected: ",1111.0/4.0));
  var float vl_statValue;
  if(f_EPTF_StatMeasure_getStat_standardDev(vl_standardDevStatId,vl_statValue)) {/*no nothing*/}
  f_setverdictFail(701520.75/4.0!=vl_statValue,
    log2str("Current value of standardDev is: ", vl_statValue, ", expected: ",701520.75/4.0));
  f_setverdictFail(701520.75/4.0!=f_EPTF_Var_getFloatValue(vl_standardDevVarId),
    log2str("Current value of statVar is: ", f_EPTF_Var_getFloatValue(vl_standardDevVarId), ", expected: ",701520.75/4.0));

  // exit with pass:
  f_EPTF_Base_stop(pass);
}

} // end of module
