////////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
/////////////////////////////////////////////////////////////////////////////// 

The demo directory is empty. The demo ttcn file is in the src directory (EPTF_CLL_StatReplay_Execution), because
this feature can be run as standalone application. This demo directory contain only the 
project file (mctr_gui) and one example log file.

Running the tests you must start the runtime GUI.
