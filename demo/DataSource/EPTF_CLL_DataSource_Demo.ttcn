///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_Datasource_Demo
// 
//  Purpose:
//    This module contains the implementation of EPTF_CLL_Datasource_Demo functions.
// 
//  Module depends on:
//    <EPTF_CLL_DataSource_Definitions>
//    <EPTF_CLL_DataSourceClient_Functions>
//    <EPTF_CLL_DataSource_Functions>
//    <EPTF_CLL_Common_Definitions>
//    <EPTF_CLL_Variable_Functions>
//    <EPTF_CLL_Variable_Definitions>
//    <EPTF_CLL_Base_Functions>
//    <EPTF_CLL_CLI_Definitions>
//    <EPTF_CLL_CLI_Functions>
//
//  Module Parameters:
//    tsp_debug_EPTF_DataSource_Functions - boolean - enable/disable debug logs
//
//  Current Owner:
//    Jozsef Gyurusi (ethjgi)
// 
//  Last Review Date:
//    2011-02-03
//
//  Detailed Comments:
//    This module contains the demo for the EPTF_CLL_DataSource.
//    Demonstrates the usage of EPTF_CLL_DataSource and EPTF_CLL_DataSource_CLI
//
//  Testcases:
//       <tc_EPTF_CLL_Datasource_Demo>
//
//    All other functions in this module are private! 
//
///////////////////////////////////////////////////////////////

module EPTF_CLL_Datasource_Demo {

import from EPTF_CLL_DataSource_Definitions all;
import from EPTF_CLL_DataSourceClient_Functions all;
import from EPTF_CLL_DataSource_Functions all;
import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_Variable_Functions all;
import from EPTF_CLL_Variable_Definitions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_CLI_Definitions all;
import from EPTF_CLL_CLI_Functions all;

type component EPTF_CLL_DataSource_Demo_CT extends EPTF_DataSource_CT,EPTF_DataSourceClient_CT,EPTF_Var_CT {
}

type component EPTF_CLL_DataSourceClient_Demo_CT extends EPTF_DataSourceClient_CT,EPTF_Var_CT {
}

function f_EPTF_DataSourceClient_demoIteratorHandler(
  out charstring pl_dataVarName,
  in charstring pl_source,
  in charstring pl_ptcName,
  in charstring pl_element,
  in EPTF_DataSource_Params pl_params
) runs on EPTF_DataSourceClient_CT return integer {
  pl_dataVarName := "IteratorVarName"&pl_element&log2str(pl_params);
  var integer vl_varIdx;
  //create iterator var if not exist
  if (f_EPTF_Var_getId(pl_dataVarName)==-1) {
    f_EPTF_Var_newCharstringlist(pl_dataVarName,{"3","4"},vl_varIdx);
  }
  return 0;
}


function f_EPTF_DataSourceClient_behaviour(in EPTF_DataSource_CT pl_sourceCompRef) runs on EPTF_CLL_DataSourceClient_Demo_CT {
  f_EPTF_DataSourceClient_init_CT("EPTF_CLL_DataSourceClient_Demo",pl_sourceCompRef);
  f_EPTF_DataSourceClient_registerData(
    pl_source := "DataSourceClient_Demo",
    pl_ptcName := f_EPTF_Base_selfName(),
    pl_dataHandler := refers(f_EPTF_DataSourceClient_demoIteratorHandler)
  );
  f_EPTF_Base_wait4Shutdown();
}

// demonstrates how to register an iterator, and how to get the iterator value
testcase tc_EPTF_CLL_Datasource_Demo() runs on EPTF_CLL_DataSource_Demo_CT {

  f_EPTF_DataSource_init_CT("EPTF_CLL_Datasource_Demo");
  f_EPTF_DataSourceClient_init_CT("EPTF_CLL_Datasource_Demo",self);
  var EPTF_CLL_DataSourceClient_Demo_CT vl_EPTF_CLL_DataSourceClient_Demo_CT := EPTF_CLL_DataSourceClient_Demo_CT.create;
  vl_EPTF_CLL_DataSourceClient_Demo_CT.start(f_EPTF_DataSourceClient_behaviour(self));
  
  // create iterator var with different value on this component:
  var integer vl_varIdx;
  f_EPTF_Var_newCharstringlist("IteratorVarName"&"MyData"&log2str(valueof(EPTF_CharstringList:{"MyParam"})),{"1","2"},vl_varIdx);
  
  f_EPTF_DataSourceClient_registerData(
    pl_source := "DataSource_Demo",
    pl_ptcName := f_EPTF_Base_selfName(),
    pl_dataHandler := refers(f_EPTF_DataSourceClient_demoIteratorHandler)
  );
  
  timer t_wait := 2.0;
  t_wait.start; t_wait.timeout;
  var  charstring vl_iteratorVarName;
  if (0!=f_EPTF_DataSource_getData(
    pl_dataVarName := vl_iteratorVarName,
    pl_source := "DataSource_Demo",
    pl_ptcName := f_EPTF_Base_selfName(),
    pl_element := "MyData",
    pl_params := {{paramName := "Param1", paramValue := "MyParam"}}
  )) {
    setverdict(fail,"Cannot get iterator for dataSource := DataSource_Demo, ptcName :=", f_EPTF_Base_selfName());
    f_EPTF_Base_stop(fail);
  };
  
  var  charstring vl_clientIteratorVarName;
  if(0!=f_EPTF_DataSource_getData(
    pl_dataVarName := vl_clientIteratorVarName,
    pl_source := "DataSourceClient_Demo",
    pl_ptcName := "EPTF_CLL_DataSourceClient_Demo",
    pl_element := "MyData",
    pl_params := {{paramName := "Param1", paramValue := "MyParam"}}
  )) {
    setverdict(fail,"Cannot get iterator for dataSource := DataSourceClient_Demo, ptcName := EPTF_CLL_DataSourceClient_Demo");
    f_EPTF_Base_stop(fail);
  };
  
  var  charstring vl_clientIteratorVarName2;
  if(0!=f_EPTF_DataSource_getData(
    pl_dataVarName := vl_clientIteratorVarName2,
    pl_source := "DataSourceClient_Demo",
    pl_ptcName := "EPTF_CLL_DataSourceClient_Demo",
    pl_element := "MyData",
    pl_params := {{paramName := "Param1", paramValue := "MyParam"}}
  )) {
    setverdict(fail,"Cannot get iterator for dataSource := DataSourceClient_Demo, ptcName := EPTF_CLL_DataSourceClient_Demo");
    f_EPTF_Base_stop(fail);
  };
  
  if (vl_clientIteratorVarName != vl_clientIteratorVarName2) {
    setverdict(fail,"The GetIterator function returns different variables for the same arguments.")
    f_EPTF_Base_stop(fail);
  }
  
  timer t_endTest := 10.0;
  t_endTest.start; t_endTest.timeout;

  var EPTF_Var_DirectContent vl_currentContent;

  var integer vl_iteratorVarIdx := f_EPTF_Var_getId(vl_iteratorVarName);
  if (vl_iteratorVarIdx!=-1) {
    f_EPTF_Var_getContent(vl_iteratorVarIdx,vl_currentContent);
    action("Value of the iterator: ",vl_currentContent);
    if (vl_currentContent != {charstringlistVal:={"1","2"}}) {
      setverdict(fail,"Iterator value on MTC is incorrect: Should be {""1"",""2""}");
      f_EPTF_Base_stop(fail);
    }
  }
  
  var integer vl_clientIteratorVarIdx := f_EPTF_Var_getId(vl_clientIteratorVarName);
  if (vl_clientIteratorVarIdx!=-1) {
    f_EPTF_Var_getContent(vl_clientIteratorVarIdx,vl_currentContent);
    action("Value of the client iterator: ",vl_currentContent);
    if (vl_currentContent != {charstringlistVal:={"3","4"}}) {
      setverdict(fail,"Iterator value on PTC is incorrect: Should be {""3"",""4""}");
      f_EPTF_Base_stop(fail);
    }
  }
  
  f_EPTF_Base_stop(pass);
}

type component EPTF_DataSource_CLI_Demo_CT extends EPTF_DataSource_CLI_CT, EPTF_CLI_CT {
}

///////////////////////////////////////////////////////////
//  Testcase: tc_EPTF_CLL_DataSource_CLI_Demo
// 
//  Purpose:   
//    Demonstrate the usage of the DataSource CLI.
//    DataSource CLI command can be executed from the telnet interface.
// 
//    Command syntax for DataSource CLI commands:
//    ds getVarName <datadescription xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' element='elementName' ptcname='ptcName' source='sourceName'>   <params>     <dataparam name='param1Name' value='param1Value'/>     <dataparam name='param2Name' value='param2Value'/>   </params> </datadescription>  
// 
//    Example commands:
// 
//    ds help
// 
//    ds getVarName <datadescription xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' element='Sources' source='DataSource'> </datadescription>
//    ds getVarName <datadescription xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' element='PTCs' source='DataSource'>   <params>     <dataparam name='Source' value='StatManager'/> </params> </datadescription>
//    ds getVarName <datadescription xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' element='PTCs' source='DataSource'>   <params>     <dataparam name='Source' value='DummySource'/> </params> </datadescription>
//    ds getVarName <datadescription xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' element='MyData' source='DummySource'>   <params>     <dataparam name='p1' value='2.0'/> <dataparam name='p2' value='1.0'/> </params> </datadescription>
// 
//    ds get <datadescription xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' element='Sources' source='DataSource'> </datadescription>
//    ds get <datadescription xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' element='PTCs' source='DataSource'>   <params>     <dataparam name='Source' value='StatManager'/> </params> </datadescription>
//    ds get <datadescription xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' element='PTCs' source='DataSource'>   <params>     <dataparam name='Source' value='DummySource'/> </params> </datadescription>
//    ds get <datadescription xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' element='MyData' source='DummySource'>   <params>     <dataparam name='p1' value='2.0'/> <dataparam name='p2' value='1.0'/> </params> </datadescription>
// 
//    ds getCondition <datadescription xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' element='match' source='DataSource'>   <params>     <dataparam name='p1' value='10'/> <dataparam name='p2' value='1*'/> </params> </datadescription>
//    ds getCondition <datadescription xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' element='&gt;' source='DataSource'>   <params>     <dataparam name='p1' value='1.0'/> <dataparam name='p2' value='2.0'/> </params> </datadescription>
//    ds getCondition <datadescription xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' element='&gt;' source='DataSource'>   <params>     <dataparam name='p1' value='2.0'/> <dataparam name='p2' value='1.0'/> </params> </datadescription>
// 
//    ds get <datadescription xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' element='Sources' source='DataSource'> </datadescription>
//    ds set <datadescription xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' element='Sources' source='DataSource'> </datadescription> { "A" }
//    ds get <datadescription xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' element='MyData' source='DummySource'>   <params>     <dataparam name='p1' value='2.0'/> <dataparam name='p2' value='1.0'/> </params> </datadescription>
//    ds set <datadescription xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' element='MyData' source='DummySource'>   <params>     <dataparam name='p1' value='2.0'/> <dataparam name='p2' value='1.0'/> </params> </datadescription> {"A","B"}
///////////////////////////////////////////////////////////
testcase tc_EPTF_CLL_DataSource_CLI_Demo() 
  runs on EPTF_DataSource_CLI_Demo_CT {
    
    f_EPTF_CLI_init_CT("DataSource_CLI_Demo")
    f_EPTF_DataSource_CLI_init_CT("DataSource_CLI_Demo",self);
        
    f_EPTF_Base_wait4Shutdown();
}

} // module EPTF_CLL_Datasource_Demo
