///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2023 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: HashMap_demo
// 
//  Purpose:
//    Demo file for HashMap.
// 
///////////////////////////////////////////////////////////

module HashMap_demo
{

//=========================================================================
// Import Part
//=========================================================================

import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_HashMap_Definitions all;
import from EPTF_CLL_HashMap_Functions all;
import from EPTF_CLL_HashMapStr2Int_Functions all;

//=========================================================================
//Component Types
//=========================================================================

type component MyMTC_CT extends EPTF_HashMap_CT {
  }

//=========================================================================
// Testcases
//=========================================================================

testcase tc_HashMap1()
  runs on MyMTC_CT
{

  f_EPTF_HashMap_init_CT ("tc_HashMap1");
  var charstring v_first := "my_first_hashmap";
  var integer v_first_int;
  v_first_int := f_EPTF_str2int_HashMap_New (v_first);

  var integer v_first_check;
  var boolean is_first;
  is_first := f_EPTF_str2int_HashMap_GetID(v_first,v_first_check);
  log("The ID of ",v_first,"is ",v_first_check);
  
  f_EPTF_str2int_HashMap_Delete (v_first);
  f_EPTF_Base_stop(pass);  
}  


testcase tc_HashMap2()
  runs on MyMTC_CT
{

  f_EPTF_HashMap_init_CT ("tc_HashMap2");
  var charstring v_first := "my_first_hashmap";
  var integer v_first_int;
  v_first_int := f_EPTF_str2int_HashMap_New (v_first);

  f_EPTF_str2int_HashMap_Insert ( v_first_int,"hello", 11 );
  f_EPTF_str2int_HashMap_Insert ( v_first_int,"world", 22 );

  var integer v_hm_data;
  if ( f_EPTF_str2int_HashMap_Find (v_first_int,"hello", v_hm_data ) )
  {
    log("Reading data from hashmap first, key = hello data: \n", v_hm_data );
  }

  f_EPTF_str2int_HashMap_Erase (v_first_int, "hello" );

  f_EPTF_str2int_HashMap_Delete (v_first);
  f_EPTF_Base_stop(pass);  
}  


testcase tc_HashMap3()
  runs on MyMTC_CT
{

  f_EPTF_HashMap_init_CT ("tc_HashMap3");
  var charstring v_first := "my_first_hashmap";
  var integer v_first_int;
  v_first_int := f_EPTF_str2int_HashMap_New (v_first);

  f_EPTF_str2int_HashMap_Insert ( v_first_int,"hello", 11 );
  f_EPTF_str2int_HashMap_Insert ( v_first_int,"world", 22 );

  var integer v_hm_size;
  v_hm_size := f_EPTF_str2int_HashMap_Size (v_first_int);
  log( "Number of elements in hashmap: \n", v_hm_size );

  var float v_max_size;
  v_max_size := f_EPTF_str2int_HashMap_MaxSize (v_first_int);
  log( "Maximum size of hashmap: \n", v_max_size );

  log( "Resizing the hashmap to countain at least 500 empty buckets");
  f_EPTF_str2int_HashMap_Resize (v_first_int,500);


  f_EPTF_str2int_HashMap_Delete (v_first);
  f_EPTF_Base_stop(pass);
}


testcase tc_HashMap4()
  runs on MyMTC_CT
{

  f_EPTF_HashMap_init_CT ("tc_HashMap4");
  var charstring v_first := "my_first_hashmap";
  var integer v_first_int;
  v_first_int := f_EPTF_str2int_HashMap_New (v_first);

  f_EPTF_str2int_HashMap_Insert ( v_first_int,"hello", 11 );
  f_EPTF_str2int_HashMap_Insert ( v_first_int,"world", 22 );

  var charstring v_readkey:="";
  var boolean v_cycleIsOver:= f_EPTF_str2int_HashMap_Begin(v_first_int, v_readkey);
  if( not v_cycleIsOver )
  {
    log("No elements in hashmap" );
  };

  while (v_cycleIsOver)
  {
    var integer v_hm_data;
    if ( f_EPTF_str2int_HashMap_Find (v_first_int, v_readkey, v_hm_data ) )
    {
      log("key,data pairs: \n", v_readkey," ", v_hm_data );
    }
    v_cycleIsOver := f_EPTF_str2int_HashMap_Next(v_first_int, v_readkey);
  }  

  f_EPTF_str2int_HashMap_Delete (v_first);
  f_EPTF_Base_stop(pass);
}  


testcase tc_HashMap5()
  runs on MyMTC_CT
{

  f_EPTF_HashMap_init_CT ("tc_HashMap5");
  var charstring v_first := "my_first_hashmap";
  var integer v_first_int;
  v_first_int := f_EPTF_str2int_HashMap_New (v_first);
  f_EPTF_str2int_HashMap_Init ();
  var charstring v_second := "my_second_hashmap";
  var integer v_second_int;
  v_second_int := f_EPTF_str2int_HashMap_New (v_second);

  f_EPTF_str2int_HashMap_Insert ( v_first_int,"hello", 11 );
  f_EPTF_str2int_HashMap_Insert ( v_first_int,"world", 22 );
  f_EPTF_str2int_HashMap_Insert ( v_second_int,"good", 88 );
  f_EPTF_str2int_HashMap_Insert ( v_second_int,"day", 99 );

  log("Dumping first hashmap by name");
  f_EPTF_str2int_HashMap_Dump(v_first);

  log("Dumping first hashmap by ID");
  f_EPTF_str2int_HashMap_DumpByID(v_first_int);

  log("Dumping all hashmaps");
  f_EPTF_str2int_HashMap_DumpAll();

  f_EPTF_str2int_HashMap_Delete (v_first);
  f_EPTF_str2int_HashMap_Delete (v_second);
  f_EPTF_Base_stop(pass);
}



//=========================================================================
// Control
//=========================================================================

control 
{
  execute( tc_HashMap1() );
  execute( tc_HashMap2() );
  execute( tc_HashMap3() );
  execute( tc_HashMap4() );
  execute( tc_HashMap5() );
  
}




}  // end of module
